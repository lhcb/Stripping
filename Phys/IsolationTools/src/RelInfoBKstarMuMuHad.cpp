/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/ToolFactory.h"
#include "RelInfoBKstarMuMuHad.h"
#include "Kernel/RelatedInfoNamed.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RelInfoBKstarMuMuHad
// Converted from ConeVariables by A. Shires
//
// 2018-01-20 : Marcin Chrzaszcz
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_TOOL_FACTORY( RelInfoBKstarMuMuHad )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RelInfoBKstarMuMuHad::RelInfoBKstarMuMuHad( const std::string& type,
                                                                    const std::string& name,
                                                                    const IInterface* parent) : GaudiTool ( type, name , parent )
{
  declareInterface<IRelatedInfoTool>(this);

  declareProperty( "Variables", m_variables,
                   "List of variables to store (store all if empty)");

  declareProperty("Name", m_transformName = "BDTvalue_KstarMuMu" );



  declareProperty
    ( "WeightsFile" , m_weightsName = "B2Kstarmumu_v2.xml",
      "weights parameter file");
  declareProperty(    "PVInputLocation"
                      , m_PVInputLocation = LHCb::RecVertexLocation::Primary
                      , " PV input location"
                      );


  m_keys.clear();

  m_keys.push_back( RelatedInfoNamed::K_SLL_ISO_HAD);
  m_keys.push_back( RelatedInfoNamed::PI_SLL_ISO_HAD);

  m_ParticlePath="/Event/Phys/StdAllNoPIDsPions/Particles";
  m_tracktype=3;

  //std::cout<<"Initialized: RelInfoBKstarMuMuHad"<<std::endl;


}

//=============================================================================
// Destructor
//=============================================================================
RelInfoBKstarMuMuHad::~RelInfoBKstarMuMuHad() {}


//=============================================================================
// Initialize
//=============================================================================
StatusCode RelInfoBKstarMuMuHad::initialize() {
  StatusCode sc = GaudiTool::initialize() ;
  if ( sc.isFailure() ) return sc ;

  m_descend = tool<IParticleDescendants> ( "ParticleDescendants", this );
  if( ! m_descend ) {
    fatal() << "Unable to retrieve ParticleDescendants tool "<< endmsg;
    return StatusCode::FAILURE;
  }



  m_angle  = 0.27;
  m_fc  = 0.60;
  m_doca_iso  = 0.13;
  m_ips  =  3.0;
  m_svdis  = -0.15;
  m_svdis_h  = 30.;
  m_pvdis  = 0.5;
  m_pvdis_h  = 40. ;


  return StatusCode::SUCCESS;

}

//=============================================================================
// Fill Extra Info structure
//=============================================================================
StatusCode RelInfoBKstarMuMuHad::calculateRelatedInfo( const LHCb::Particle *part,
                                                                   const LHCb::Particle *top_bis )
{



  if ( msgLevel(MSG::DEBUG) ) debug() << "Calculating TrackIso extra info" << endmsg;
  m_bdt1 = -1000 ;
  m_bdt2 = -1000 ;

  if ( part->isBasicParticle() || part!=top_bis)
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "top != top_bis" << endmsg;
    return StatusCode::SUCCESS ;
  }

  // -- The vector m_decayParticles contains all the particles that belong to the given decay
  // -- according to the decay descriptor.

  // -- Clear the vector with the particles in the specific decay


  LHCb::Particle::ConstVector decprod;

  is_kst=0;

  const SmartRefVector< LHCb::Particle > Bdaughters = part->daughters();
  int countDaughters =0;

  for( SmartRefVector< LHCb::Particle >::const_iterator idau = Bdaughters.begin() ; idau != Bdaughters.end() ; ++idau)
  {
    if(abs((*idau)->particleID().pid())==313) is_kst++;

    const SmartRefVector< LHCb::Particle > Bgddaughters = (*idau)->daughters();
    for( SmartRefVector< LHCb::Particle >::const_iterator igddau = Bgddaughters.begin() ; igddau != Bgddaughters.end() ; ++igddau)
    {
      const LHCb::Particle* track = *(igddau);
      decprod.push_back(track);
      if (  abs((*igddau)->particleID().pid()) == 13 ) countDaughters++;
    }

  }

  if  (countDaughters ==2 && (abs(part->particleID().pid()) >500) && (is_kst==1) )
    {
      StatusCode fillIsolation_mu = fillIsolation(part);

      if (!fillIsolation_mu) return fillIsolation_mu;

      return StatusCode::SUCCESS;

    }
  else
  {
    //store
    m_map.clear();
    std::vector<short int>::const_iterator ikey;
    m_count_mum=-1.;
    m_count_mup=-1.;
    
    for (ikey = m_keys.begin(); ikey != m_keys.end(); ikey++)
    {
      double value;
      
      switch (*ikey)
      {
      case RelatedInfoNamed::K_SLL_ISO_HAD : value =m_count_mum;        break;
      case RelatedInfoNamed::PI_SLL_ISO_HAD: value =m_count_mup;        break;
      }
      m_map.insert( std::make_pair( *ikey, value) );
      

    }//not K*mumu





  }

  
    return StatusCode::SUCCESS;

}

StatusCode RelInfoBKstarMuMuHad::fillIsolation(const LHCb::Particle *top)
{


  //set PV and SV of the mother
  m_dva = Gaudi::Utils::getIDVAlgorithm( contextSvc() ) ;

  if ( !m_dva )
  {
    return Error("Could not get parent DVAlgorithm");
  }

  const LHCb::VertexBase* goodPV = m_dva->bestVertex(top);

  //const SmartRefVector< LHCb::Particle > & daughters = top->daughters();
  
  m_dist = m_dva->distanceCalculator ();


  // now fillinf the isolations
  m_count_mum = 0;
  m_count_mup = 0;
  m_count_mum_f = 0;
  m_count_mup_f = 0;

  Gaudi::XYZVector PV_pos;
  if (NULL==goodPV)
  {
    warning() << "No best PV for " << top->particleID().pid() << endmsg ;
    return StatusCode::SUCCESS;
  }
  const LHCb::Particle::ConstVector pv = top->daughtersVector();

  int idx = 0;

  for (LHCb::Particle::ConstVector::const_iterator ipart=pv.begin();ipart!=pv.end();++ipart)
  {
    debug() << "daughter ID " << (*ipart)->particleID().pid() << endmsg ;
    //      if ( NULL==(*ipart)->proto() ) continue;
    idx++;
  }
  if(idx != 2 )
  {
    return StatusCode::SUCCESS;
  }
  //bool test =true;
  std::string prefix1, prefix2;
  //debug()<<"  just before calling getIso "<<endreq;

  std::vector<double> iso5 = getIso(top);

  // part =B
  //std::cout<<"Got back the isolation"<<std::endl;
  //std::cout<<"Checking :"<<iso5[0]<<"  "<<iso5[1]<<std::endl;



  //debug()<<"  just after calling getIso "
  //       <<"  iso "<<iso5[0]<<"  "<<iso5[1]<<endreq;


  if (iso5[0]==-999) return StatusCode::FAILURE;
  const LHCb::Particle* dau1 = top->daughtersVector().at(0);
  const LHCb::Particle* dau2 = top->daughtersVector().at(1);
  //  const LHCb::Particle* jpsi ;
  const LHCb::Particle* kstar ;


  if (abs((dau1)->particleID().pid()) == 443 )
  {
    //jpsi = (dau1);
    kstar = (dau2);
  }
  else
  {
    //    jpsi = (dau2);
    kstar = (dau1);
  }
  const LHCb::Particle::ConstVector parts = kstar->daughtersVector();
  
  prefix1 = "kaon";
  prefix2 = "pion";

  double m_count_kaon, m_count_pion;
  

  if (abs(kstar->daughtersVector().at(1)->particleID().pid()) == 211 ) 
  {
    m_count_kaon = static_cast<float>( iso5[0] );
    m_count_pion = static_cast<float>( iso5[1] );
  }
  else
  {
    m_count_kaon = static_cast<float>( iso5[1] );
    m_count_pion = static_cast<float>( iso5[0] );
    
  }
  

    // now my isolations
  
  //std::cout<<"Checking what is really needed: "<<iso5[i*2+6] <<"  "<<iso5[i*2+1+6]<<std::endl;


  //store
  m_map.clear();

  std::vector<short int>::const_iterator ikey;
  for (ikey = m_keys.begin(); ikey != m_keys.end(); ikey++)
  {
    double value;
    switch (*ikey)
    {
    case RelatedInfoNamed::K_SLL_ISO_HAD : value =m_count_kaon;      break;
    case RelatedInfoNamed::PI_SLL_ISO_HAD : value =m_count_pion;      break;
    }
    m_map.insert( std::make_pair( *ikey, value) );
    

  }
  //std::cout<<"Filled isolations: "<<m_count_kaon<<" "<<m_count_pion<<std::endl;
  

  return StatusCode::SUCCESS;

}



//=============================================================================
// method to calculate  isolation
//=============================================================================
std::vector<double>  RelInfoBKstarMuMuHad::getIso(const LHCb::Particle* B)
{
  const LHCb::VertexBase *PV = m_dva->bestVertex(B);
  const LHCb::VertexBase *SV = B->endVertex();
  //  const LHCb::Particle::ConstVector parts = B->daughtersVector();

  std::vector<double>  iso_tmp(2,-9999);



  //  std::cout<<mclink1<<std::endl;
  //    std::vector<int> iso(2, -9999);
  //double mass= B->momentum().M();

  const LHCb::Particle* dau1 =  B->daughtersVector().at(0);
  const LHCb::Particle* dau2 = B->daughtersVector().at(1);
  const LHCb::Particle* jpsi ;
  const LHCb::Particle* kstar ;


  if (abs((dau1)->particleID().pid()) == 443 )
  {
    jpsi = (dau1);
    kstar = (dau2);
  }
  else
  {
    jpsi = (dau2);
    kstar = (dau1);
  }
  const LHCb::Particle::ConstVector parts = jpsi->daughtersVector();
  const LHCb::Particle::ConstVector kpi = kstar->daughtersVector();
   //Loop over kstar particles, get their simple kinematics
  LHCb::Particle::ConstVector::const_iterator imumu_part;
  ROOT::Math::SMatrix<double, 3, 2> p_mumu;
  ROOT::Math::SVector<double, 2> ptmumu;
  int j=0;
  //cout<<"filling41"<<endl;                                                  
  for ( imumu_part = parts.begin(); imumu_part != parts.end(); imumu_part++) 
  {
    const LHCb::ProtoParticle * proto =  (*imumu_part)->proto();
    const LHCb::Track* track = proto->track();
    p_mumu(0,j) = track->momentum().x();
    p_mumu(1,j) = track->momentum().y();
    p_mumu(2,j) = track->momentum().z();
    ptmumu[j] = sqrt(pow(p_mumu(0,j),2)+pow(p_mumu(1,j),2));
    j++;
  }
  if(parts.size() !=2) 
  {
    return iso_tmp;
  }
  
  StatusCode sc = StatusCode::SUCCESS;
  LHCb::Particles* allparts = get<LHCb::Particles>(m_ParticlePath);
  //LHCb::Particles* allparts = get<LHCb::Particles>(m_TracksPath);
  if (!allparts)
  {
    error() << " Failed to get particles container "
            <<  m_ParticlePath << endmsg;

    return iso_tmp;
  }



  Gaudi::XYZPoint PosPV = PV->position();
  Gaudi::XYZPoint PosSV = SV->position();
  int i = 0;
  ROOT::Math::SVector<int, 2> iso5;
  iso5[0]=0;
  iso5[1]=0;
  ROOT::Math::SMatrix<double, 3, 2> o_mu;
  ROOT::Math::SMatrix<double, 3, 2> p_mu;
  ROOT::Math::SVector<double, 2> ptmu;
  //Loop over input particles, get their simple kinematics
  LHCb::Particle::ConstVector::const_iterator ip_part;

  //int itrack=0;


  for ( ip_part = kpi.begin(); ip_part != kpi.end(); ip_part++) 
  {
    const LHCb::ProtoParticle * proto =  (*ip_part)->proto();
    const LHCb::Track* track = proto->track();
    o_mu(0,i) = track->position().x();
    o_mu(1,i) = track->position().y();
    o_mu(2,i) = track->position().z();
    p_mu(0,i) = track->momentum().x();
    p_mu(1,i) = track->momentum().y();
    p_mu(2,i) = track->momentum().z();
    ptmu[i] = sqrt(pow(p_mu(0,i),2)+pow(p_mu(1,i),2));
    i++;
  }
  if (i>2)
  {
    error()<<"more than 2 daughters of the Jpsi" <<endmsg;
    return iso_tmp;

  }
  //int B_ID=0;



  



  std::vector<double> iso(2, -9999);
  
  int ntracks=0;
  for ( int i2 = 0; i2 < 2; i2++)
  {
    bool hltgood = false;
    double fc = 0.;
    Gaudi::XYZPoint omu(o_mu(0,i2),o_mu(1,i2),o_mu(2,i2));
    Gaudi::XYZVector pmu(p_mu(0,i2),p_mu(1,i2),p_mu(2,i2));

    //Loop over all particles
    LHCb::Particles::const_iterator ip;
    int j=0;
    // for counting ntracks
    for ( ip = allparts->begin(); ip != allparts->end() ; ++ip)
    {
      ntracks++;


      const LHCb::ProtoParticle * proto =  (*ip)->proto();
      const LHCb::Track* track = proto->track();
      const LHCb::Particle*  cand = (*ip);
      Gaudi::XYZPoint o(track->position());
      Gaudi::XYZVector p(track->momentum());

      double pt = p.Rho();
      //double ptot = p.R();
      //int charge= proto->charge();
      //float clone = proto->track()->info(LHCb::Track::CloneDist,100000);
      //float ghost = proto->track()->ghostProbability();

      if (track->type()!=m_tracktype)   continue;
      j++;
      // skip if other particle is in input list
      if (ratio(pt, ptmu[0]) < 0.0001 || ratio(pt,ptmu[1]) <0.0001 || ratio(pt, ptmumu[0]) < 0.0001 || ratio(pt,ptmumu[1]) <0.0001) 
      {
        continue;
      }
      IsHltGood(o, p, omu, pmu ,PosPV, hltgood, fc);

      // find doca and angle between input and other tracks
      Gaudi::XYZPoint vtx(0.,0.,0.);
      double doca(-1.);
      double angle(-1.);
      InCone(omu,pmu, o, p, vtx, doca, angle);
      // find impact parameters, distances from secondary and primary verte
      double imp = 0.;
      double impchi2 = 0.;
      double IP,ips,pvdis,svdis;
      ips=100000.;
      LHCb::RecVertex::Container::const_iterator iv;
      LHCb::RecVertex::Container* verts = NULL;
      if(exist<LHCb::RecVertex::Container>(m_PVInputLocation))
      {
        verts = get<LHCb::RecVertex::Container>(m_PVInputLocation);
      }
      for ( iv = verts->begin(); iv != verts->end(); iv++)
      {
        m_dist->distance(&(*cand),(*iv),imp,impchi2);
        if (impchi2<ips) ips = impchi2;
      }

      ips=sqrt(ips);
      IP=imp;
      double trkchi2=cand->proto()->track()->chi2PerDoF();
      //double deltaZvtx = (vtx.z()-PosPV.z());
      pvdis = (vtx.z()-PosPV.z())/fabs(vtx.z()-PosPV.z())*(vtx-PosPV).R();
      svdis = (vtx.z()-PosSV.z())/fabs(vtx.z()-PosSV.z())*(vtx-PosSV).R();

      // non-isolating criteria #5
      if (angle <m_angle && fc<m_fc && doca<m_doca_iso && ips>m_ips &&
          svdis>m_svdis && svdis<m_svdis_h && pvdis>m_pvdis && pvdis<m_pvdis_h
          && track->type()==m_tracktype)
      {
        iso5[i2] += 1;
      }
      // if
      // variables for BDT

      tv_angle=angle;
      tv_fc=fc;
      tv_doca=doca;
      tv_svdis=svdis;
      tv_pvdis=pvdis;
      tv_pt=pt;
      tv_ips=ips;
      tv_ip=IP;
      tv_trkchi2=trkchi2;
      m_varmap.clear()    ;


      m_varmap.insert( "angle", tv_angle);
      m_varmap.insert( "fc", tv_fc);
      m_varmap.insert( "doca", tv_doca);
      m_varmap.insert( "svdis", tv_svdis);
      m_varmap.insert( "pvdis", tv_pvdis);
      m_varmap.insert( "pt", tv_pt);
      m_varmap.insert( "ips", tv_ips);
      m_varmap.insert( "ip", tv_ip);
      //      m_varmap.insert( "@#!@#@!trkchi2", tv_trkchi2);

    }// allparts   
    

  }//i2
  
  

  //  old iso
  iso[0] = iso5[0] ;
  iso[1] = iso5[1] ;


  return iso;

}
//=============================================================================
// IsHLTGood method,used by isolation calculation
//=============================================================================
void  RelInfoBKstarMuMuHad::IsHltGood(Gaudi::XYZPoint o,Gaudi::XYZVector p,
                                  Gaudi::XYZPoint o_mu,Gaudi::XYZVector
                                  p_mu, Gaudi::XYZPoint PV, bool& hltgood,
                                  double& fc)
{
  Gaudi::XYZVector rv;
  Gaudi::XYZPoint vtx;
  Gaudi::XYZPoint close;
  Gaudi::XYZPoint close_mu;
  bool fail(false);
  closest_point(o,p,o_mu,p_mu,close,close_mu,vtx,fail);
  if (fail)
  {
    fc = -1.;
    hltgood = -1;
  }

  else
  {
    double pete = p.Rho();
    rv = vtx - PV;
    double DOCA_b = (close-close_mu).R();
    double ZZ = rv.z();
    fc = pointer(rv,p,p_mu);
    hltgood=( (DOCA_b<0.2) && (ZZ>0.) && (ZZ<30.) && (fc<0.4) && (pete>2.) );
  }
}



double RelInfoBKstarMuMuHad::pointer (Gaudi::XYZVector vertex,
                                  Gaudi::XYZVector p, Gaudi::XYZVector
                                  p_mu)
{

  double pt=p.Rho()+p_mu.Rho();
  Gaudi::XYZVector ptot(p+p_mu);
  double temp = arcosine(vertex,ptot);
  double  num=ptot.R()*sin(temp);
  double  den=(num+pt);
  double fc = num/den;

  return fc;
}

//=============================================================================
// Other functions needed by isolation
//=============================================================================

double RelInfoBKstarMuMuHad::getphi(const LHCb::Particle* vdau1, const LHCb::Particle* vdau2)
{
  double dphi = vdau1->momentum().Phi() - vdau2->momentum().Phi();
  return dphi;
}



//=============================================================================
double RelInfoBKstarMuMuHad::gettheta(const LHCb::Particle* vdau1, const LHCb::Particle* vdau2)
{
  double dtheta = vdau1->momentum().Eta() -  vdau2->momentum().Eta();
  return dtheta;

}

//=============================================================================
double RelInfoBKstarMuMuHad::ratio( double p1, double p2)
{
  return fabs(p1 -p2)*(1./fabs(p1+p2));
}




//=============================================================================
double RelInfoBKstarMuMuHad::IsClose(const LHCb::Particle* p1,const LHCb::Particle* p2)
{
  double deta = gettheta(p1,p2);
  double dphi = getphi(p1,p2);
  return sqrt(deta*deta+dphi*dphi);
}
//=============================================================================
void RelInfoBKstarMuMuHad::closest_point(Gaudi::XYZPoint o,Gaudi::XYZVector p,
                                     Gaudi::XYZPoint o_mu,Gaudi::XYZVector p_mu,
                                     Gaudi::XYZPoint& close1,
                                     Gaudi::XYZPoint& close2,
                                     Gaudi::XYZPoint& vertex, bool& fail)
{

  Gaudi::XYZVector v0(o - o_mu);
  Gaudi::XYZVector v1(p.unit());
  Gaudi::XYZVector v2(p_mu.unit());
  Gaudi::XYZPoint temp1(0.,0.,0.);
  Gaudi::XYZPoint temp2(0.,0.,0.);
  fail = false;
  double  d02 = v0.Dot(v2);
  double  d21 = v2.Dot(v1);
  double  d01 = v0.Dot(v1);
  double  d22 = v2.Dot(v2);
  double  d11 = v1.Dot(v1);
  double  denom = d11 * d22 - d21 * d21;

  if (fabs(denom) <= 0.)
  {
    close1 = temp1;
    close2 = temp2;
    fail = true;
  }

  else
  {
    double numer = d02 * d21 - d01 * d22;
    double mu1 = numer / denom;
    double mu2 = (d02 + d21 * mu1) / d22;
    close1 = o+v1*mu1;
    close2 = o_mu+v2*mu2;
  }

  vertex = (close1+(close2-close1)*0.5);

}

double RelInfoBKstarMuMuHad::arcosine(Gaudi::XYZVector p1,Gaudi::XYZVector p2)
{

  double num    = (p1.Cross(p2)).R();
  double den    = p1.R()*p2.R();
  double seno   = num/den;
  double coseno = p1.Dot(p2)/den;
  double alpha  = asin(fabs(seno));
  if (coseno < 0 )
  {
    alpha = ROOT::Math::Pi() - alpha;
  }

  return alpha;

}
//=============================================================================
void RelInfoBKstarMuMuHad::InCone(Gaudi::XYZPoint o1,
                              Gaudi::XYZVector p1,Gaudi::XYZPoint o2,
                              Gaudi::XYZVector p2,
                              Gaudi::XYZPoint& vtx, double&
                              doca, double& angle)
{

  Gaudi::XYZPoint rv;
  Gaudi::XYZPoint close;
  Gaudi::XYZPoint close_mu;

  bool fail(false);
  closest_point(o1,p1,o2,p2,close,close_mu,vtx, fail);
  if (fail)
  {
    doca =-1.;
    angle=-1.;
  }

  else
  {
    doca = (close-close_mu).R();
    angle = arcosine(p1,p2);
  }

}


//rel infor methods 

LHCb::RelatedInfoMap* RelInfoBKstarMuMuHad::getInfo(void) {
  return &m_map;
}

std::string RelInfoBKstarMuMuHad::infoPath(void) {
  std::stringstream ss;
  ss << std::string("Particle2TrackIsolationRelations");
  return ss.str();
}

//=============================================================================
// Save the particles in the decay chain (recursive function)
//=============================================================================
void RelInfoBKstarMuMuHad::saveDecayParticles( const LHCb::Particle *top)
{

  // -- Get the daughters of the top particle
  const SmartRefVector< LHCb::Particle > & daughters = top->daughters();

  // -- Fill all the daugthers in m_decayParticles
  for( SmartRefVector< LHCb::Particle >::const_iterator idau = daughters.begin() ; idau != daughters.end() ; ++idau){

    // -- If the particle is stable, save it in the vector, or...
    if( (*idau)->isBasicParticle() ){
      if ( msgLevel(MSG::DEBUG) ) debug() << "Filling particle with ID " << (*idau)->particleID().pid() << endmsg;
      m_decayParticles.push_back( (*idau) );
    }else{
      // -- if it is not stable, call the function recursively
      m_decayParticles.push_back( (*idau) );
      if ( msgLevel(MSG::DEBUG) ) debug() << "Filling particle with ID " << (*idau)->particleID().pid() << endmsg;
      saveDecayParticles( (*idau) );
    }

  }

}

//=============================================================================
// Check if the track is already in the decay
//=============================================================================
bool RelInfoBKstarMuMuHad::isTrackInDecay(const LHCb::Track* track){

  bool isInDecay = false;

  for(  std::vector<const LHCb::Particle*>::iterator it = m_decayParticles.begin() ; it != m_decayParticles.end() ; ++it ){

    const LHCb::ProtoParticle* proto = (*it)->proto();
    if(proto){
      const LHCb::Track* myTrack = proto->track();

      if(myTrack){

        if(myTrack == track){
          if ( msgLevel(MSG::DEBUG) ) debug() << "Track is in decay, skipping it" << endmsg;
          isInDecay = true;
        }
      }
    }
  }
  return isInDecay;
}

