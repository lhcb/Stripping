###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

def execInSandbox(fun, *args, **kwargs):
    from processing import Pipe, Process, Condition
    (r, w) = Pipe()

    # bind worker to the 'write' side of the pipe, and to 'fun'
    def worker(*args, **kwargs):
        try:
            # TODO: catch errors, and signal parent about them...
            w.send(fun(*args, **kwargs))
        except Exception, e:  # noqa
            w.send(e)

    p = Process(target=worker, args=args, kwargs=kwargs)
    p.start()
    result = r.recv()
    if type(result) is Exception:
        raise result
    # check if child exited OK..    .
    return result
