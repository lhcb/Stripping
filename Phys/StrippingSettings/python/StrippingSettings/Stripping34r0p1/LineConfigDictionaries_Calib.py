###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
K23PiForDownstreamTrackEff = {
    'WGs'         : ['Calib'],
    'BUILDERTYPE' : 'K23PiForDownstreamTrackEffConf',
    'CONFIG'      : {
        'PionIPCHI2'                 : 16.0,
        'PionMinP'                   : 1500,
        'PionMinPT'                  : 125,
        'PionProbNNghost'            : 0.25,
        'PionProbNNpi'               : 0.6,
        'PionProbNNp'                : 0.1, # against Lambda decays
        'DipionMinPT'                : 300,
        'DipionMinP'                 : 3000,
        'DipionMinMassNarrow'        : 275,
        'DipionMaxMassNarrow'        : 500,
        'DipionMinMassWide'          : 260,
        'DipionMaxMassWide'          : 600,
        'DipionMaxMCorrNarrow'       : 600,
        'DipionMaxMCorrWide'         : 700,
        'DipionMCorrPseudoErrNarrow' : 30,
        'DipionMCorrPseudoErrWide'   : 100,
        'DipionVertexChi2PerDoF'     : 2, # for a two-track vertex, it peaks at 0
        'DipionFlightDistanceChi2'   : 100,
        'DipionMinVertexZ'           : 100.0,
        'DipionMaxVertexZ'           : 2200.0,
        'DipionDira'                 : 0.9995,
        'TripionMinMass'             : 450,
        'TripionMaxMass'             : 550,
        'TripionVertexChi2PerDoF'    : 5,
        'TripionFlightDistanceChi2'  : 100,
        'TripionMinVertexZ'          : 0,
        'TripionMaxVertexZ'          : 2200,
        'DipionSSNarrowDownstreamPrescale'  : 1.0,
        'DipionSSNarrowLongPrescale'        : 0.2,
        'DipionSSWidePrescale'              : 0.05,
        'DipionOSNarrowDownstreamPrescale'  : 0.4,
        'DipionOSNarrowLongPrescale'        : 0.2,
        'DipionOSWidePrescale'              : 0.05,
        'TripionSSPrescale'                 : 1.0,
        'TripionOSPrescale'                 : 1.0
    },
    'STREAMS'     :  [ 'BhadronCompleteEvent' ]
}


DstarD2hhGammaCalib = {
    "BUILDERTYPE": "DstarD2hhGammaCalibLines", 
    "CONFIG": {
        "CombMassHigh": 2270.0, 
        "CombMassHigh_HH": 1820.0, 
        "CombMassLow": 1280.0, 
        "CombMassLow_HH": 500.0, 
        "Daug_TRCHI2DOF_MAX": 3, 
        "Dstar_AMDiff_MAX": 180.0, 
        "Dstar_MDiff_MAX": 165.0, 
        "Dstar_VCHI2VDOF_MAX": 15.0, 
        "HighPIDK": -1, 
        "Hlt1Filter": None, 
        "Hlt1Tos": {
            "Hlt1.*Track.*Decision%TOS": 0
        }, 
        "Hlt2Filter": None, 
        "Hlt2Tos": {
            "Hlt2CharmHadInclDst2PiD02HHXBDTDecision%TOS": 0, 
            "Hlt2PhiIncPhiDecision%TOS": 0
        }, 
        "LowPIDK": 5, 
        "MassHigh": 2250.0, 
        "MassHigh_HH": 1810.0, 
        "MassLow": 1300.0, 
        "MassLow_HH": 600.0, 
        "MaxADOCACHI2": 10.0, 
        "MaxIPChi2": 15, 
        "MaxVCHI2NDOF": 10.0, 
        "MaxVCHI2NDOF_HH": 10.0, 
        "MinBPVDIRA": 0.995, 
        "MinBPVTAU": 0.0001, 
        "MinCombPT": 2000.0, 
        "MinPT": 2500.0, 
        "MinTrkIPChi2": 10, 
        "MinTrkPT": 500.0, 
        "MinVDCHI2_HH": 1000.0, 
        "MinVDCHI2_HHComb": 1000.0, 
        "PiSoft_PT_MIN": 250.0, 
        "PrescaleDstarD2KKGamma": 1, 
        "PrescaleDstarD2KPiGamma": 1, 
        "TrChi2": 4, 
        "TrGhostProb": 0.5, 
        "photonPT": 200.0
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Calib" ]
}

TrackEffDownMuon = {
    "BUILDERTYPE": "StrippingTrackEffDownMuonConf", 
    "CONFIG": {
        "DataType": "2011", 
        "HLT1PassOnAll": True, 
        "HLT1TisTosSpecs": {
            "Hlt1SingleMuonNoIPDecision%TOS": 0, 
            "Hlt1TrackMuonDecision%TOS": 0
        }, 
        "HLT2PassOnAll": False, 
        "HLT2TisTosSpecs": {
            "Hlt2SingleMuon.*Decision%TOS": 0, 
            "Hlt2TrackEffDiMuonDownstream.*Decision%TOS": 0
        }, 
        "JpsiDoca": 5.0, 
        "JpsiHlt1Filter": "Hlt1.*Decision", 
        "JpsiHlt2Filter": "Hlt2.*Decision", 
        "JpsiLinePostscale": 1.0, 
        "JpsiLinePrescale": 1.0, 
        "JpsiMassPostComb": 200.0, 
        "JpsiMassPreComb": 2000.0, 
        "JpsiProbeP": 5.0, 
        "JpsiProbePt": 0.5, 
        "JpsiProbeTrackChi2": 10.0, 
        "JpsiPt": 0.0, 
        "JpsiTagMinIP": 0.5, 
        "JpsiTagP": 5.0, 
        "JpsiTagPID": -2.0, 
        "JpsiTagPt": 0.7, 
        "JpsiTagTrackChi2": 10.0, 
        "JpsiVertexChi2": 5.0, 
        "MuMom": 2.0, 
        "MuTMom": 0.2, 
        "NominalLinePostscale": 1.0, 
        "NominalLinePrescale": 0.2, 
        "SeedingMinP": 1500.0, 
        "TrChi2": 10.0, 
        "UpsilonDoca": 5.0, 
        "UpsilonHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "UpsilonHLT2TisTosSpecs": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        }, 
        "UpsilonLinePostscale": 1.0, 
        "UpsilonLinePrescale": 1.0, 
        "UpsilonMassPostComb": 1500.0, 
        "UpsilonMassPreComb": 100000.0, 
        "UpsilonMuMom": 0.0, 
        "UpsilonMuTMom": 0.5, 
        "UpsilonVertexChi2": 25.0, 
        "ValidationLinePostscale": 1.0, 
        "ValidationLinePrescale": 0.0015, 
        "ZDoca": 5.0, 
        "ZHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZHLT2TisTosSpecs": {
            "Hlt2SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZLinePostscale": 1.0, 
        "ZLinePrescale": 1.0, 
        "ZMassPostComb": 1500.0, 
        "ZMassPreComb": 100000.0, 
        "ZMuMaxEta": 4.5, 
        "ZMuMinEta": 2.0, 
        "ZMuMom": 0.0, 
        "ZMuTMom": 20.0, 
        "ZVertexChi2": 25.0
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingTrackEffDownMuonNominalLine", 
            "StrippingTrackEffDownMuonValidationLine", 
            "StrippingTrackEffDownMuonLine1", 
            "StrippingTrackEffDownMuonLine2", 
            #            "StrippingTrackEffDownMuonZLine", 
            "StrippingTrackEffDownMuonUpsilonLine"
        ]
    }, 
    "WGs": [ "Calib" ]
}

TrackEffMuonTT = {
    "BUILDERTYPE": "StrippingTrackEffMuonTTConf", 
    "CONFIG": {
        "BJpsiKHlt2TriggersTOS": {
            "Hlt2TopoMu2BodyDecision%TOS": 0
        }, 
        "BJpsiKHlt2TriggersTUS": {
            "Hlt2TopoMu2BodyDecision%TUS": 0
        }, 
        "BJpsiKMinIP": 10000, 
        "BJpsiKPrescale": 1.0, 
        "BJpsiVertexChi2": 5, 
        "BMassWin": 500, 
        "Hlt1PassOnAll": True, 
        "Hlt2PassOnAllLowMult": False, 
        "JpsiHlt1Filter": "Hlt1.*Decision", 
        "JpsiHlt1Triggers": {
            "Hlt1TrackMuonDecision%TOS": 0
        }, 
        "JpsiHlt2Filter": "Hlt2.*Decision", 
        "JpsiHlt2Triggers": {
            "Hlt2SingleMuon.*Decision%TOS": 0, 
            "Hlt2TrackEffDiMuonMuonTT.*Decision%TOS": 0
        }, 
        "JpsiLowMultHlt1Filter": "Hlt1.*Decision", 
        "JpsiLowMultHlt1Triggers": {
            "Hlt1LowMultMuonDecision%TOS": 0
        }, 
        "JpsiLowMultHlt2Filter": "Hlt2.*Decision", 
        "JpsiLowMultHlt2Triggers": {
            "Hlt2LowMultMuonDecision%TOS": 0
        }, 
        "JpsiLowMultMassWin": 500.0, 
        "JpsiLowMultMinIP": 9999, 
        "JpsiLowMultPrescale": 1.0, 
        "JpsiLowMultProbeP": 0.0, 
        "JpsiLowMultProbePt": 0.5, 
        "JpsiLowMultPt": 0.0, 
        "JpsiLowMultTagMinIP": 9999, 
        "JpsiLowMultTagP": 0.0, 
        "JpsiLowMultTagPID": -9999, 
        "JpsiLowMultTagPt": 0.5, 
        "JpsiLowMultTagTrackChi2": 5.0, 
        "JpsiLowMultVertexChi2": 9999, 
        "JpsiMassWin": 500.0, 
        "JpsiMinIP": 0.8, 
        "JpsiPrescale": 1.0, 
        "JpsiProbeP": 5.0, 
        "JpsiProbePt": 0.5, 
        "JpsiPt": 1.0, 
        "JpsiTagMinIP": 0.0, 
        "JpsiTagP": 10.0, 
        "JpsiTagPID": -2.0, 
        "JpsiTagPt": 1.3, 
        "JpsiTagTrackChi2": 5.0, 
        "JpsiVertexChi2": 2, 
        "Postscale": 1.0, 
        "UpsilonHlt1Triggers": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "UpsilonHlt2Triggers": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        }, 
        "UpsilonMassWin": 1500, 
        "UpsilonMinIP": 10000, 
        "UpsilonPrescale": 1.0, 
        "UpsilonProbeP": 0.0, 
        "UpsilonProbePt": 0.5, 
        "UpsilonPt": 0.0, 
        "UpsilonTagMinIP": 0.0, 
        "UpsilonTagP": 0.0, 
        "UpsilonTagPID": 2.0, 
        "UpsilonTagPt": 1.0, 
        "UpsilonTagTrackChi2": 5.0, 
        "UpsilonVertexChi2": 5.0, 
        "ZHlt1Triggers": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZHlt2Triggers": {
            "Hlt2EWSingleMuonVHighPtDecision%TOS": 0
        }, 
        "ZMassWin": 40000, 
        "ZMinIP": 10000, 
        "ZPrescale": 1.0, 
        "ZProbeP": 0.0, 
        "ZProbePt": 0.5, 
        "ZPt": 0.0, 
        "ZTagMinIP": 0.0, 
        "ZTagP": 0.0, 
        "ZTagPID": 2.0, 
        "ZTagPt": 10.0, 
        "ZTagTrackChi2": 5.0, 
        "ZVertexChi2": 5.0
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingTrackEffMuonTT_JpsiLine1", 
            "StrippingTrackEffMuonTT_JpsiLine2", 
            "StrippingTrackEffMuonTT_JpsiLowMultLine1", 
            "StrippingTrackEffMuonTT_JpsiLowMultLine2", 
            "StrippingTrackEffMuonTT_UpsilonLine1", 
            "StrippingTrackEffMuonTT_UpsilonLine2", 
            "StrippingTrackEffMuonTT_ZLine1", 
            "StrippingTrackEffMuonTT_ZLine2", 
            "StrippingTrackEffMuonTT_BJpsiKLine1", 
            "StrippingTrackEffMuonTT_BJpsiKLine2"
        ]
    }, 
    "WGs": [ "Calib" ]
}

TrackEffVeloMuon = {
    "BUILDERTYPE": "StrippingTrackEffVeloMuonConf", 
    "CONFIG": {
        "HLT1PassOnAll": True, 
        "HLT1TisTosSpecs": {
            "Hlt1SingleMuonNoIPDecision%TOS": 0, 
            "Hlt1TrackMuonDecision%TOS": 0
        }, 
        "HLT2PassOnAll": False, 
        "HLT2TisTosSpecs": {
            "Hlt2SingleMuon.*Decision%TOS": 0, 
            "Hlt2TrackEffDiMuonVeloMuon.*Decision%TOS": 0
        }, 
        "JpsiHlt1Filter": "Hlt1.*Decision", 
        "JpsiHlt2Filter": "Hlt2.*Decision", 
        "JpsiMassPostComb": 500.0, 
        "JpsiMassPreComb": 1000.0, 
        "JpsiProbeP": 5.0, 
        "JpsiProbePt": 0.5, 
        "JpsiProbeTrChi2": 5.0, 
        "JpsiPt": 0.5, 
        "JpsiTagMinIP": 0.2, 
        "JpsiTagMinIPChi2": 0.0, 
        "JpsiTagMuDLL": -1.0, 
        "JpsiTagP": 7.0, 
        "JpsiTagPt": 0.0, 
        "JpsiTagTrChi2": 3.0, 
        "JpsiVertChi2": 2.0, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "UpsilonHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "UpsilonHLT2TisTosSpecs": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        }, 
        "UpsilonMassPostComb": 1500.0, 
        "UpsilonMassPreComb": 100000.0, 
        "UpsilonPostscale": 1.0, 
        "UpsilonPrescale": 1.0, 
        "UpsilonProbeP": 0.0, 
        "UpsilonProbePt": 0.5, 
        "UpsilonProbeTrChi2": 9999.0, 
        "UpsilonPt": 0.5, 
        "UpsilonTagMinIP": 0.0, 
        "UpsilonTagMinIPChi2": 0.0, 
        "UpsilonTagMuDLL": -9999.0, 
        "UpsilonTagP": 0.0, 
        "UpsilonTagPt": 0.5, 
        "UpsilonTagTrChi2": 9999.0, 
        "UpsilonVertChi2": 10000.0, 
        "ZHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZHLT2TisTosSpecs": {
            "Hlt2SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZMassPostComb": 40000.0, 
        "ZMassPreComb": 100000.0, 
        "ZPostscale": 1.0, 
        "ZPrescale": 1.0, 
        "ZProbeP": 0.0, 
        "ZProbePt": 20.0, 
        "ZProbeTrChi2": 9999.0, 
        "ZPt": 0.5, 
        "ZTagMaxEta": 4.5, 
        "ZTagMinEta": 2.0, 
        "ZTagMinIP": 0.0, 
        "ZTagMinIPChi2": 0.0, 
        "ZTagMuDLL": -9999.0, 
        "ZTagP": 0.0, 
        "ZTagPt": 20.0, 
        "ZTagTrChi2": 9999.0, 
        "ZVertChi2": 10000.0
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingTrackEffVeloMuonLine1", 
            "StrippingTrackEffVeloMuonLine2", 
            "StrippingTrackEffVeloMuonZLine1", 
            "StrippingTrackEffVeloMuonZLine2", 
            "StrippingTrackEffVeloMuonUpsilonLine1", 
            "StrippingTrackEffVeloMuonUpsilonLine2"
        ]
    }, 
    "WGs": [ "Calib" ]
}
ElectronRecoEff = {
    "BUILDERTYPE": "StrippingElectronRecoEffLines", 
    "CONFIG": {
        "DetachedEEK": {
            "AMTAP": 6000.0, 
            "MHIGH": 5700.0, 
            "MLOW": 5000.0, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2TAP": 22, 
            "bmass_ip_constraint": -2.5, 
            "overlapCut": 0.95, 
            "probePcutMax": 150000.0, 
            "probePcutMin": 750.0
        }, 
        "DetachedEEKstar": {
            "AMTAP": 6000.0, 
            "MHIGH": 5700.0, 
            "MLOW": 5000.0, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2TAP": 22, 
            "bmass_ip_constraint": -2.0, 
            "overlapCut": 0.95, 
            "probePcutMax": 150000.0, 
            "probePcutMin": 750.0
        }, 
        "DetachedEEPhi": {
            "AMTAP": 6000.0, 
            "MHIGH": 5700.0, 
            "MLOW": 5000.0, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2TAP": 22, 
            "bmass_ip_constraint": -2.0, 
            "overlapCut": 0.95, 
            "probePcutMax": 150000.0, 
            "probePcutMin": 750.0
        }, 
        "DetachedEK": {
            "AM": 5500.0, 
            "DIRA": 0.95, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2": 10, 
            "VDCHI2": 36, 
            "bCandFlightDist": 4.0
        }, 
        "DetachedEKstar": {
            "AM": 5500.0, 
            "DIRA": 0.95, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2": 10, 
            "VDCHI2": 36, 
            "bCandFlightDist": 3.0
        }, 
        "DetachedEPhi": {
            "AM": 5500.0, 
            "DIRA": 0.9, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2": 10, 
            "VDCHI2": 36, 
            "bCandFlightDist": 3.0
        }, 
        "DetachedKstar": {
            "AMMAX": 1050.0, 
            "AMMIN": 600.0, 
            "APTMIN": 500, 
            "KST_IPCHI2": 4, 
            "K_IPCHI2": 8, 
            "K_PROBNNk": 0.1, 
            "K_PT": 250.0, 
            "MMAX": 1000.0, 
            "MMIN": 750.0, 
            "Pi_IPCHI2": 8, 
            "Pi_PROBNNpi": 0.1, 
            "Pi_PT": 250.0, 
            "VCHI2": 20
        }, 
        "DetachedMuK": {
            "AM": 5400.0, 
            "DIRA": 0.95, 
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            }, 
            "VCHI2": 10, 
            "VDCHI2": 100, 
            "bCandFlightDist": 4.0
        }, 
        "DetachedMuKstar": {
            "AM": 5400.0, 
            "DIRA": 0.95, 
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            }, 
            "VCHI2": 10, 
            "VDCHI2": 100, 
            "bCandFlightDist": 3.0
        }, 
        "DetachedMuMuK": {
            "AMTAP": 6000.0, 
            "MHIGH": 5700.0, 
            "MLOW": 5000.0, 
            "POINTINGMASSMAX": 7200.0, 
            "POINTINGMASSMIN": 3200.0, 
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            }, 
            "VCHI2TAP": 20, 
            "bmass_ip_constraint": -2.75, 
            "probePcutMax": 150000.0, 
            "probePcutMin": 750.0
        }, 
        "DetachedMuMuKstar": {
            "AMTAP": 6000.0, 
            "MHIGH": 5700.0, 
            "MLOW": 5000.0, 
            "POINTINGMASSMAX": 7000.0, 
            "POINTINGMASSMIN": 3500.0, 
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            }, 
            "VCHI2TAP": 20, 
            "bmass_ip_constraint": -2.5, 
            "probePcutMax": 150000.0, 
            "probePcutMin": 750.0
        }, 
        "DetachedMuMuPhi": {
            "AMTAP": 6000.0, 
            "MHIGH": 5700.0, 
            "MLOW": 5000.0, 
            "POINTINGMASSMAX": 7000.0, 
            "POINTINGMASSMIN": 3500.0, 
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            }, 
            "VCHI2TAP": 20, 
            "bmass_ip_constraint": -1.0, 
            "probePcutMax": 150000.0, 
            "probePcutMin": 750.0
        }, 
        "DetachedMuPhi": {
            "AM": 5400.0, 
            "DIRA": 0.95, 
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            }, 
            "VCHI2": 10, 
            "VDCHI2": 100, 
            "bCandFlightDist": 3.0
        }, 
        "DetachedPhi": {
            "AMMAX": 1100, 
            "AMMIN": 900, 
            "K_IPCHI2": 7, 
            "K_PIDK": 1, 
            "K_PT": 200, 
            "MMAX": 1050, 
            "MMIN": 980, 
            "PHI_IPCHI2": 4, 
            "PHI_PT": 400, 
            "VCHI2": 100
        }, 
        "DoVeloDecoding": False, 
        "EtaMaxVelo": 5.1, 
        "EtaMinVelo": 1.9, 
        "Hlt1Req": {
            "DetachedEK": "Hlt1TrackMVA.*Decision", 
            "DetachedEKstar": "Hlt1TrackMVA.*Decision", 
            "DetachedEPhi": "Hlt1TrackMVA.*Decision", 
            "DetachedMuK": "Hlt1Track*MVA.*Decision", 
            "DetachedMuKstar": "Hlt1Track*MVA.*Decision", 
            "DetachedMuPhi": "Hlt1Track*MVA.*Decision"
        }, 
        "Hlt2Req": {
            "DetachedEK": "Hlt2Topo(E)?2BodyDecision", 
            "DetachedEKstar": "Hlt2Topo(E)?2BodyDecision", 
            "DetachedEPhi": "Hlt2Topo(E)?2BodyDecision", 
            "DetachedMuK": "Hlt2Topo(Mu)?2BodyDecision", 
            "DetachedMuKstar": "Hlt2Topo(Mu)?2BodyDecision", 
            "DetachedMuPhi": "Hlt2Topo(Mu)?2BodyDecision"
        }, 
        "L0Req": {
            "DetachedEK": "L0_CHANNEL_RE('Electron')", 
            "DetachedEKstar": "L0_CHANNEL_RE('Electron')", 
            "DetachedEPhi": "L0_CHANNEL_RE('Muon|Electron|Hadron')", 
            "DetachedMuK": "L0_CHANNEL_RE('Muon|Hadron')", 
            "DetachedMuKstar": "L0_CHANNEL_RE('Muon|Hadron')", 
            "DetachedMuPhi": "L0_CHANNEL_RE('Muon|Hadron')"
        }, 
        "L0TOS": {
            "DetachedEK": "L0ElectronDecision", 
            "DetachedMuK": "L0MuonDecision"
        }, 
        "LooseSharedChild": {
            "EtaMaxEle": 5.1, 
            "EtaMaxMu": 5.1, 
            "EtaMinEle": 1.8, 
            "EtaMinMu": 1.8, 
            "IPChi2Ele": 8, 
            "IPChi2Mu": 8, 
            "IPEle": 0.0, 
            "IPKaon": 0.0, 
            "IPMu": 0.0, 
            "IPPion": 0.0, 
            "ProbNNe": 0.2, 
            "ProbNNk": 0.1, 
            "ProbNNmu": 0.2, 
            "PtEle": 1000.0, 
            "PtKaon": 500.0, 
            "PtMu": 1000.0, 
            "TrChi2Ele": 5, 
            "TrChi2Kaon": 5, 
            "TrChi2Mu": 5, 
            "TrChi2Pion": 5
        }, 
        "SharedChild": {
            "EtaMaxEle": 5.1, 
            "EtaMaxMu": 5.1, 
            "EtaMinEle": 1.8, 
            "EtaMinMu": 1.8, 
            "IPChi2Ele": 12, 
            "IPChi2Kaon": 12, 
            "IPChi2Mu": 12, 
            "IPChi2Pion": 36, 
            "IPEle": 0.0, 
            "IPKaon": 0.0, 
            "IPMu": 0.0, 
            "IPPion": 0.0, 
            "ProbNNe": 0.2, 
            "ProbNNk": 0.2, 
            "ProbNNmu": 0.5, 
            "ProbNNpi": 0.8, 
            "PtEle": 1200.0, 
            "PtKaon": 500.0, 
            "PtMu": 1200.0, 
            "PtPion": 1000.0, 
            "TrChi2Ele": 5, 
            "TrChi2Kaon": 5, 
            "TrChi2Mu": 5, 
            "TrChi2Pion": 5
        }, 
        "TrackGEC": 150, 
        "VeloFitter": "SimplifiedGeometry", 
        "VeloMINIP": 0.04, 
        "VeloTrackChi2": 5.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "Calib" ]
}

Ccbar2PpbarFromB = {
    'BUILDERTYPE'       : 'Ccbar2PpbarConf',
    'CONFIG'    : {
        'LinePrescale'     :     1.  ,
        'LinePostscale'    :     1.  ,
        
        'SpdMult'          :   750.  , # dimensionless, Spd Multiplicy cut                                                                                                                                    
        'ProtonPT'         :   500.  , # MeV                                                                                                                                                                  
        'ProtonP'          :    7.5  , # GeV                                                                                                                                                                  
        'ProtonTRCHI2DOF'  :     5.  , # no cut                                                                                                                                                               
        'ProtonPIDppi'     :    5.  , # CombDLL(p-pi)                                                                                                                                                         
        'ProtonPIDpK'      :    5.  , # CombDLL(p-K)                                                                                                                                                          
        'ProtonIPCHI2Cut'  : " & (BPVIPCHI2()>9)",
        'CombMaxMass'      :  3600 , # MeV, before Vtx fit                                                                                                                                                    
        'CombMinMass'      :  2650.  , # MeV, before Vtx fit                                                                                                                                                  
        'MaxMass'          :  3400 , # MeV, after Vtx fit                                                                                                                                                     
        'MinMass'          :  2700.  , # MeV, after Vtx fit                                                                                                                                                   
        'VtxCHI2'          :     9.  , # dimensionless                                                                                                                                                        
        'CCCut'            :  " & (BPVDLS>5) & (BPVCORRM>4000) & (BPVCORRM<5700)"
        },
    'STREAMS' : [ 'CharmCompleteEvent' ] ,
    'WGs'    : [ 'Calib' ]
    }
