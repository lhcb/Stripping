###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                          S T R I P P I N G  34r0p1                         ##
##                                                                            ##
##  Configuration for B&Q WG                                                  ##
##  Contact person: andrii.usachov@cern.ch                                    ##
################################################################################

from GaudiKernel.SystemOfUnits import *

######################################################################
#
# Lb->eta_c K p
# Author:  Mengzhen Wang, Liming Zhang
#
#####################################################################

Lb2EtacKp = {
    'BUILDERTYPE'       :  'Lb2EtacKpConf',
    'CONFIG'    : {
        'KaonCuts'      : "(PROBNNk > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        'ProtonCuts'    : "(PROBNNp > 0.1) & (PT > 300*MeV) & (P > 10*GeV) & (TRGHOSTPROB<0.4)",        
        'PionCuts'      : "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.4)",
        'EtacComAMCuts' : "(AM<3.35*GeV)",
        'EtacComN4Cuts' : """
                          (in_range(2.65*GeV, AM, 3.35*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.5 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>30)
                          """,
        'EtacMomN4Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 9.) 
                           & (in_range(2.7*GeV, MM, 3.3*GeV)) 
                           & (MIPCHI2DV(PRIMARY) > 0.) 
                           & (BPVVDCHI2>10) 
                           & (BPVDIRA>0.9)
                           """,
        'EtacComCuts'   : "(in_range(2.65*GeV, AM, 3.35*GeV))",
        'LambdaSComCuts': "(ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 4.0 *GeV) & (ADOCACHI2CUT(20., ''))",
        'LambdaSMomCuts': """
                          (MIPCHI2DV(PRIMARY) > 0.)
                          & (BPVVDCHI2 > 10.)
                          & (VFASPF(VCHI2) < 9.)
                          & (BPVDIRA>0.9)
                          """,
        'KsCuts'        : "(ADMASS('KS0') < 30.*MeV) & (BPVDLS>5)",
        'LbComCuts'     : "(ADAMASS('Lambda_b0') < 500 *MeV)",
        'LbMomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 10.) 
                          & (BPVDIRA> 0.9999) 
                          & (BPVIPCHI2()<25) 
                          & (BPVVDCHI2>250)
                          & (BPVVDRHO>0.1*mm) 
                          & (BPVVDZ>2.0*mm)
                          """,
        'Prescale'      : 1.,
        'RelatedInfoTools': [{
                      'Type'              : 'RelInfoVertexIsolation',
                      'Location'          : 'RelInfoVertexIsolation'
                  }, {
                      'Type'              : 'RelInfoVertexIsolationBDT',
                      'Location'          : 'RelInfoVertexIsolationBDT'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.0'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.5,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.5'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 2.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_2.0'
                  }]        
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['BandQ'],
    }



###############################################################################
#
#
# Module for selecting B0->Chic0 K pi, B+->Chic0 K phi, Lb->Chic0 K p
#
# Author: Yanxi Zhang, Andrii Usachov
#
#
###############################################################################

B2Chic0KPi = {
    'BUILDERTYPE'       :  'B2Chic0KPiConf',
    'CONFIG'    : {
        'KaonCuts'      : "(PROBNNk  > 0.2) & (PT > 300*MeV) & (MIPCHI2DV()>9) & (TRGHOSTPROB<0.4)",
        'ProtonCuts'    : "(PROBNNp  > 0.2) & (PT > 300*MeV) & (MIPCHI2DV()>9) & (P > 10*GeV) & (TRGHOSTPROB<0.4)",
        'PionCuts'      : "(PROBNNpi > 0.2) & (PT > 300*MeV) & (MIPCHI2DV()>9) & (TRGHOSTPROB<0.4)",
        'KaonCuts4h'    : "(PROBNNk  > 0.2) & (PROBNNp < 0.9) & (PROBNNpi < 0.9) & (MIPCHI2DV()>6) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        'PionCuts4h'    : "(PROBNNpi > 0.2) & (PROBNNk < 0.9) & (PROBNNp  < 0.9) & (MIPCHI2DV()>6) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        'Chic0ComAMCuts' : "(AM<4.2*GeV)",
        'Chic0ComN4Cuts' : """
                           (in_range(2.6*GeV, AM, 4.1*GeV))
                           & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.0 *GeV)
                           & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>50)
                           """,
        'Chic0MomN4Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 5.)
                           & (in_range(2.7*GeV, MM, 4.1*GeV))
                           & (MIPCHI2DV(PRIMARY) > 0.)
                           & (BPVVDCHI2>16)
                           & (BPVDIRA>0.9)
                           """,
        'Chic0ComCuts'   : """
                           (in_range(2.6*GeV, AM, 4.2*GeV))
                           & ( (ACHILD(PT,1)+ACHILD(PT,2))> 1.5 *GeV)
                           """,
        'Chic0MomN2Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 9.)
                           & (in_range(2.7*GeV, MM, 4.1*GeV))
                           & (MIPCHI2DV(PRIMARY) > 0.)
                           & (BPVVDCHI2>16)
                           & (BPVDIRA>0.9)
                           """,
        'KstarComCuts': """
                        (ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 4.0 *GeV) & (ADOCACHI2CUT(10., ''))
                        & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2))>20)
                        """,
        'KstarMomCuts': """
                          (MIPCHI2DV(PRIMARY) > 0.)
                          & (BPVVDCHI2 > 9.)
                          & (VFASPF(VCHI2) < 9.)
                          & (BPVDIRA>0.9)
                        """,
        'PhiComCuts': "(ACHILD(PT,1)+ACHILD(PT,2) > 400.*MeV) & (AM>970.*MeV) &(AM < 1070 *MeV) & (ADOCACHI2CUT(10., ''))",
        'PhiMomCuts': """
                          (MIPCHI2DV(PRIMARY) > 0.)
                          & (BPVVDCHI2 > 9.)
                          & (VFASPF(VCHI2) < 9.)
                          & (BPVDIRA>0.9)
                        """,
        'BComCuts'     : "(ADAMASS('B0') < 300 *MeV)",
        'BMomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 8.)
                          & (BPVDIRA> 0.9999)
                          & (BPVIPCHI2()<16)
                          & (BPVVDCHI2>49)
                         """,
        'LbComCuts'     : "(ADAMASS('Lambda_b0') < 200 *MeV)",
        'LbMomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 5.)
                          & (BPVDIRA> 0.9999)
                          & (BPVIPCHI2()<16)
                          & (BPVVDCHI2>49)
                         """,
        'Prescale'      : 1.,
        'RelatedInfoTools': [{
                      'Type'              : 'RelInfoVertexIsolation',
                      'Location'          : 'RelInfoVertexIsolation'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                      'Location'          : 'RelInfoConeVariables_1.0'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.5,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                      'Location'          : 'RelInfoConeVariables_1.5'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 2.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                      'Location'          : 'RelInfoConeVariables_2.0'
                  }]       
        },
    'STREAMS'           : ['Bhadron'],
    'WGs'               : ['BandQ'],
    }



###############################################################################
#
#
# Ds+-> pi+etap(mumupipi), and Bu->eta_prime(mumupipi)K+, and norm channels
# Ds+-> phi(mumu)pipipi and Bu->J/PsiK+
#
# Author: Xabier Cid Vidal
#
#
###############################################################################

Etap2pipimumu = {
    'BUILDERTYPE' : 'StrippingEtap2pipimumuConf',
    'CONFIG':{
        ## eta' daughters
        'EtapMuonP'         : 0.,    #MeV
        'EtapMuonPT'        : 0.,    #MeV
        'EtapMuonMINIPCHI2' : 5 ,    #adminensional
        'EtapMuonPIDmu'     : -5,    #adimensional    
       
        'EtapPionP'         : 0.,    #MeV
        'EtapPionPT'        : 0.,    #MeV
        'EtapPionMINIPCHI2' : 5 ,    #adminensional
       
        ## Ds decay
        'DsPionP'         : 200.,    #MeV
        'DsPionPT'        : 25. ,    #MeV
        'DsPionMINIPCHI2' : 5   ,    #adminensional
       
        ## B+ decay
        'BuKaonP'         : 2000.,   #MeV
        'BuKaonPT'        : 300. ,   #MeV
        'BuKaonMINIPCHI2' : 5    ,   #adminensional
        'BuKaonPIDk'      : -5   ,   #adimensional
       
        #eta' -> pipimumu
        'Etap_PT'         : 100 ,    #MeV                  
        'Etap_VCHI2DOF'   : 10  ,    #adminensional         
        'Etap_MAXDOCA'   : 1    ,    #mm
        'Etap_FDCHI2'     : 5   ,    #adimensional
        'Etap_IPCHI2'     : 5   ,    #adimensional
        'Etap_MASSWIN'    : 50  ,    #MeV
       
        #Ds+ -> pi+eta and pi+pi-pi+phi
        'Ds_PT'         : 300,        #MeV                  
        'Ds_VCHI2DOF'   : 15 ,        #adminensional         
        'Ds_FDCHI2'     : 10 ,        #adimensional
        'Ds_IPCHI2'     : 10 ,        #adimensional, should be close to 0
        'Ds_MASSWIN'    : 100,        #MeV
                   
        #B+->eta'K+ and J/PsiK+
        'Bu_PT'         : 300,        #MeV                  
        'Bu_VCHI2DOF'   : 8  ,        #adminensional         
        'Bu_FDCHI2'     : 36 ,        #adimensional
        'Bu_IPCHI2'     : 20 ,        #adimensional
        'Bu_MASSWIN'    : 100,        #MeV
        
        #normalization
        #phi -> mumu
        'Phi_PT'         : 100,       #MeV                  
        'Phi_VCHI2DOF'   : 10 ,       #adminensional         
        'Phi_MAXDOCA'    : 1  ,       #mm
        'Phi_FDCHI2'     : 5  ,       #adimensional
        'Phi_IPCHI2'     : 5  ,       #adimensional
        'Phi_MASSWIN'    : 75 ,       #MeV
       
        #J/Psi -> mumu
        'JPsi_PT'        : 100,       #MeV                  
        'JPsi_VCHI2DOF'  : 10 ,       #adminensional         
        'JPsi_MAXDOCA'   : 1  ,       #mm
        'JPsi_FDCHI2'    : 5  ,       #adimensional
        'JPsi_IPCHI2'    : 5  ,       #adimensional
        'JPsi_MASSWIN'   : 75 ,       #MeV

        'Ds2Etap2PiPiMuMuLinePrescale'     : 1,
        'Ds2Etap2PiPiMuMuLinePostscale'    : 1,
        
        'Ds2Phi3PiLineForEtapPrescale'     : 1,
        'Ds2Phi3PiLineForEtapPostscale'    : 1,
       
        'Bu2Etap2PiPiMuMuLinePrescale'     : 1,
        'Bu2Etap2PiPiMuMuLinePostscale'    : 1,
       
        'Bu2JPsiKForEtapLinePrescale'      : 1,
        'Bu2JPsiKForEtapLinePostscale'     : 1,
       
        'RelatedInfoTools': [{
                      'Type'              : 'RelInfoVertexIsolation',
                      'Location'          : 'RelInfoVertexIsolation'
                     }, {
                      'Type'              : 'RelInfoVertexIsolationBDT',
                      'Location'          : 'RelInfoVertexIsolationBDT'
                     }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.0,
                      'Location'          : 'RelInfoConeVariables_1.0',
                     }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.5,
                      'Location'          : 'RelInfoConeVariables_1.5',
                     }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 2.0,
                      'Location'          : 'RelInfoConeVariables_2.0',
                     }, {
                      'Type'              : 'RelInfoTrackIsolationBDT',
                      'Location'          : 'RelInfoTrackIsolationBDT',
                     }, {
                      'Type'              : 'RelInfoBstautauCDFIso',
                      'Location'          : 'RelInfoBstautauCDFIso'
                     }]
            },
    'STREAMS' : [ 'Charm' ],
    'WGs'     : [ 'BandQ' ]
    }



###############################################################################
#
#
# Module for construction of Omegab Decays Stripping Selections and StrippingLines.
# Provides functions to build Xic, Omegac, Omegab selections.
# Provides class OmegabDecaysConf, which constructs the Selections and
# StrippingLines given a configuration dictionary.
#
# Author: Marco Pappagallo
#
#
###############################################################################

OmegabDecays = {
    'BUILDERTYPE'       : 'OmegabDecaysConf',
    'CONFIG'    : {
                   # Omegab
                   'MinOmegabMass'       : 5500.     # MeV
                   ,'MaxOmegabMass'       : 6700.     # MeV
                   ,'Omegab_PT'           : 3500.     # MeV 
                   ,'OmegabVertChi2DOF'   : 10        # Dimensionless 
                   ,'OmegabLT'            : 0.2       # ps
                   ,'OmegabIPCHI2'        : 25        # Dimensionless
                   ,'OmegabDIRA'          : 0.999     # Dimensionless
                   ,'KaonProbNN'          : 0.1       # Dimensionless
                   ,'PionProbNN'          : 0.1       # Dimensionless
                   ,'ProtProbNN'          : 0.1       # Dimensionless
                   ,'TrackMinIPCHI2'      : 4         # Dimensionless  
                   ,'XicPT'               : 1800      # MeV
                   ,'XicBPVVDCHI2'        : 36
                   ,'XicDIRA'             : 0.        # Dimensionless
                   ,'XicDeltaMassWin'     : 100       # MeV
                   ,'XicprDeltaMassWin'   : 250       # MeV
                   ,'OmegacstDeltaMassWin': 500       # MeV
                   ,'MaxXicVertChi2DOF'   : 10        # Dimensionless
                   # Pre- and postscales
                   ,'OmegabDecaysPreScale'   : 1.0
                   ,'OmegabDecaysPostScale'  : 1.0
                  },
    'STREAMS' : [ 'Bhadron' ],
    'WGs'     : [ 'BandQ' ]
    }




###############################################################################
#
#
# Inclusive multi open charm lines
#
# Author: Marian Stahl
#
#
###############################################################################

InclusiveCharmBaryons = {
  'BUILDERTYPE' : 'InclusiveCharmBaryonsConf',
  'CONFIG' : {    
    'pi' : {
      'TES'    : 'Phys/StdAllNoPIDsPions/Particles',
      'Filter' : "(P>1000*MeV) & (MIPCHI2DV(PRIMARY)>4.0)"
    },
    'K' : {
      'TES'    : 'Phys/StdAllNoPIDsKaons/Particles',
      'Filter' : "(P>1000*MeV) & (MIPCHI2DV(PRIMARY)>4.0)"
    },
    'p' : {
      'TES'    : 'Phys/StdAllNoPIDsProtons/Particles',
      'Filter' : "(P>1000*MeV) & (MIPCHI2DV(PRIMARY)>4.0)"
    },
    'CharmHadrons': {
      'Lc' : {
        'Daughters'       : {'p' : 1, 'K' : 2, 'pi' : 3},
        'Parent'          : 'Lc',
        'DecayDescriptors' : ['[Lambda_c+ -> p+ K- pi+]cc'],
        'xmlFile'         : '$TMVAWEIGHTSROOT/data/DfromB/LcPi_2018_GBDT.weights.xml',
        'MVACut'          : '0.45',
        'Comb12Cut'       : "(ADOCA(1,2)<0.5*mm)",
        'CombCut'         : """(ASUM(PT)>1800*MeV) & (ADAMASS('Lambda_c+') < 50*MeV) &
                               (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))|
                                ((ABSID=='KS0') & (PT > 500*MeV) & (P > 5000*MeV) & (BPVVDCHI2 > 1000)))) &
                               (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)""",
        'MotherCut'       : "(CHI2VXNDF<10) & (BPVVDCHI2>36) & (BPVDIRA>0) & (ADMASS('Lambda_c+') < 32*MeV)",
      },
      'Xic' : {
        'Daughters'       : {'p' : 1, 'K' : 2, 'pi' : 3},
        'Parent'          : 'Xic',
        'DecayDescriptors' : ['[Xi_c+ -> p+ K- pi+]cc'],
        'xmlFile'         : '$TMVAWEIGHTSROOT/data/DfromB/XicPi_Run2_GBDT.weights.xml',
        'MVACut'          : '0.5',
        'Comb12Cut'       : "(ADOCA(1,2)<0.5*mm)",
        'CombCut'         : """(ASUM(PT)>1800*MeV) & (ADAMASS('Xi_c+') < 50*MeV) &
                               (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))|
                                ((ABSID=='KS0') & (PT > 500*MeV) & (P > 5000*MeV) & (BPVVDCHI2 > 1000)))) &
                               (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)""",
        'MotherCut'       : "(CHI2VXNDF<10) & (BPVVDCHI2>36) & (BPVDIRA>0) & (ADMASS('Xi_c+') < 32*MeV)",
      },
      'Xic0' : {
        'Daughters'       : {'p' : 1, 'K1' : 2, 'K2' : 3, 'pi' : 4},
        'Parent'          : 'Xic0',
        'DecayDescriptors' : ['[Xi_c0 -> p+ K- K- pi+]cc'],
        'xmlFile'         : '$TMVAWEIGHTSROOT/data/DfromB/Xic0Pi_Run2_GBDT.weights.xml',
        'MVACut'          : '0.5',
        'Comb12Cut'       : "(ADOCA(1,2)<0.5*mm)",
        'Comb123Cut'      : "(ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        'CombCut'         : """(ASUM(PT)>1800*MeV) & (ADAMASS('Xi_c0') < 40*MeV) &
                               (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))|
                                ((ABSID=='KS0') & (PT > 500*MeV) & (P > 5000*MeV) & (BPVVDCHI2 > 1000)))) &
                               (ADOCA(1,4)<0.5*mm) & (ADOCA(2,4)<0.5*mm) & (ADOCA(3,4)<0.5*mm)""",
        'MotherCut'       : "(CHI2VXNDF<10) & (BPVVDCHI2>36) & (BPVDIRA>0) & (ADMASS('Xi_c0') < 24*MeV)",
      },
      'Omegac0' : {
        'Daughters'       : {'p' : 1, 'K1' : 2, 'K2' : 3, 'pi' : 4},
        'Parent'          : 'Xic0', # the name in the weights file
        'DecayDescriptors' : ['[Omega_c0 -> p+ K- K- pi+]cc'],
        'xmlFile'         : '$TMVAWEIGHTSROOT/data/DfromB/Xic0Pi_Run2_GBDT.weights.xml',
        'MVACut'          : '0.5',
        'Comb12Cut'       : "(ADOCA(1,2)<0.5*mm)",
        'Comb123Cut'      : "(ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        'CombCut'         : """(ASUM(PT)>1800*MeV) & (ADAMASS('Omega_c0') < 40*MeV) &
                               (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))|
                                ((ABSID=='KS0') & (PT > 500*MeV) & (P > 5000*MeV) & (BPVVDCHI2 > 1000)))) &
                               (ADOCA(1,4)<0.5*mm) & (ADOCA(2,4)<0.5*mm) & (ADOCA(3,4)<0.5*mm)""",
        'MotherCut'       : "(CHI2VXNDF<10) & (BPVVDCHI2>36) & (BPVDIRA>0) & (ADMASS('Omega_c0') < 24*MeV)",
      },
    },    
  },
  'STREAMS' : {
    'BhadronCompleteEvent':[
      'StrippingInclusiveCharmBaryons_LcLine',
      'StrippingInclusiveCharmBaryons_XicLine',
      'StrippingInclusiveCharmBaryons_Xic0Line',
      'StrippingInclusiveCharmBaryons_Omegac0Line',
    ]
  },
  'WGs' : [ 'BandQ' ]
}


InclusiveDoubleD = {
  'BUILDERTYPE' : 'InclusiveDoubleDConf',
  'CONFIG' : {    
    'pi' : {
      'TES'    : 'Phys/StdAllNoPIDsPions/Particles',
      'Filter' : "(P>1000*MeV) & (MIPCHI2DV(PRIMARY)>4.0)"
    },
    'K' : {
      'TES'    : 'Phys/StdAllNoPIDsKaons/Particles',
      'Filter' : "(P>1000*MeV) & (MIPCHI2DV(PRIMARY)>4.0)"
    },
    'p' : {
      'TES'    : 'Phys/StdAllNoPIDsProtons/Particles',
      'Filter' : "(P>1000*MeV) & (MIPCHI2DV(PRIMARY)>4.0)"
    },
    'CharmHadrons': {
      'D0' : {
        'Daughters'        : {'K' : 1, 'pi' : 2},
        'Parent'           : 'D0',
        'DecayDescriptors' : ['[D0 -> K- pi+]cc'],
        'xmlFile'          : '$TMVAWEIGHTSROOT/data/DfromB/D0Pi_2018_GBDT.weights.xml',
        'MVACut'           : '0.0',
        'CombCut'          : """(ASUM(PT)>1800*MeV) & (ADAMASS('D0') < 60*MeV) & (ADOCA(1,2)<0.5*mm) &
                                (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))))""",
        'MotherCut'        : "(CHI2VXNDF<10) & (BPVVDCHI2>36) & (BPVDIRA>0) & (ADMASS('D0') < 42*MeV)",
      },
      'D' : {
        'Daughters'        : {'K' : 1, 'pi1' : 2, 'pi2' : 3},
        'Parent'           : 'D',
        'DecayDescriptors' : ['[D+ -> K- pi+ pi+]cc'],
        'xmlFile'          : '$TMVAWEIGHTSROOT/data/DfromB/DPi_2018_GBDT.weights.xml',
        'MVACut'           : '0.2',
        'Comb12Cut'        : "(ADOCA(1,2)<0.5*mm)",
        'CombCut'          : """(ASUM(PT)>1800*MeV) & (ADAMASS('D+') < 50*MeV) &
                                (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))|
                                 ((ABSID=='KS0') & (PT > 500*MeV) & (P > 5000*MeV) & (BPVVDCHI2 > 1000)))) &
                                (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)""",
        'MotherCut'        : "(CHI2VXNDF<10) & (BPVVDCHI2>36) & (BPVDIRA>0) & (ADMASS('D+') < 32*MeV)",
      },
      'Ds' : {
        'Daughters'        : {'Kp' : 1, 'Km' : 2, 'pi' : 3},
        'Parent'           : 'Ds',
        'DecayDescriptors' : ['[D_s+ -> K+ K- pi+]cc'],
        'xmlFile'          : '$TMVAWEIGHTSROOT/data/DfromB/DsPi_2018_GBDT.weights.xml',
        'MVACut'           : '0.4',
        'Comb12Cut'        : "(ADOCA(1,2)<0.5*mm)",
        'CombCut'          : """(ASUM(PT)>1800*MeV) & (ADAMASS('D_s+') < 50*MeV) &
                                (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))|
                                 ((ABSID=='KS0') & (PT > 500*MeV) & (P > 5000*MeV) & (BPVVDCHI2 > 1000)))) &
                                (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)""",
        'MotherCut'        : "(CHI2VXNDF<10) & (BPVVDCHI2>36) & (BPVDIRA>0) & (ADMASS('D_s+') < 32*MeV)",
      },
    },
    'DecayDescriptors' : ["B0 -> D0 D~0", "[B0 -> D0 D0]cc", "[B0 -> D0 D+]cc", "[B0 -> D0 D-]cc",
                          "[B0 -> D0 D_s+]cc", "[B0 -> D0 D_s-]cc", "B0 -> D+ D-", "[B0 -> D+ D+]cc",
                          "[B0 -> D+ D_s+]cc", "[B0 -> D+ D_s-]cc", "B0 -> D_s+ D_s-", "[B0 -> D_s+ D_s+]cc"],
    'CombCut'          : "AM > 0",
    'MotherCut'        : "(CHI2VXNDF<10)",
  },
  'STREAMS' : {
    'CharmCompleteEvent':[
      'StrippingInclusiveDoubleDLine',
    ]
  },
  'WGs' : [ 'BandQ' ]
}



###############################################################################
#
#
# Module for selecting Ccbar->LambdaLambda detached line, with loose PT, PID cuts. 
#
# Author: Andrii Usachov
#
#
###############################################################################

Ccbar2LambdaLambda = {
     'BUILDERTYPE' : 'Ccbar2LambdaLambdaConf',
     'CONFIG' : {
         'TRCHI2DOF'        :  5.   ,
         'TRIPCHI2'         :  4.   , 
         'ProtonProbNNp'    :  0.3  ,
         'ProtonP'          :  5000 ,
         'ProtonPTSec'      :  250. , # MeV
         'PionProbNNpi'     :  0.1  ,
         'PionPTSec'        :  250. , # MeV
         'LambdaMassW'      :  30 ,
         'LambdaVCHI2DOF'   :  9 ,
         'CombMaxMass'      :  15100., # MeV, before Vtx fit
         'CombMinMass'      :  2700., # MeV, before Vtx fit
         'MaxMass'          :  15000., # MeV, after Vtx fit
         'MinMass'          :  2750. # MeV, after Vtx fit
         },
     'STREAMS' : [ 'Charm'],
     'WGs'     : [ 'BandQ']
     }

