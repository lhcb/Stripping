###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                          S T R I P P I N G  2 9 r 2 p 2                    ##
##                                                                            ##
##  Configuration for B&Q WG                                                  ##
##  Contact person: valeriia.zhovkovska@cern.ch                               ##
################################################################################

from GaudiKernel.SystemOfUnits import *




###############################################################################
#                                                                             #
#                                                                             #
# Module for selecting B_c->eta_c l nu                                        #
# with eta_c->ppbar, eta_c->KsKpi                                             #
# with mu, tau->mu 2nu, tau-> 3pi                                             #
# Provides functions to build B_c, eta_c, tau selections.                     #
# Provides class Bc2EtacSLConf, which constructs the Selections and           #
# StrippingLines given a configuration dictionary.                            #
#                                                                             #
# Author: Valeriia Zhovkovska                                                 #
#                                                                             #
#                                                                             #
###############################################################################


Bc2EtacSL = {
    "BUILDERTYPE": "Bc2EtacSLConf", 
    "CONFIG": {
        "Bc4KsKpiMuMomCuts": "in_range(3.2*GeV, MM, 7.5*GeV)  & in_range(4.0*GeV, BPVCORRM, 12.0*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 5000)", 
        "Bc4KsKpiTauMomCuts": "in_range(0.75*GeV, MM, 7.5*GeV) & in_range(1.0*GeV, BPVCORRM, 12.0*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 5000)", 
        "Bc4PpbarMuMomCuts": "in_range(3.2*GeV, MM, 7.5*GeV)  & in_range(4.0*GeV, BPVCORRM, 12.0*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 3000)", 
        "Bc4PpbarTauMomCuts": "in_range(0.75*GeV, MM, 7.5*GeV) & in_range(1.0*GeV, BPVCORRM, 12.0*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 3000)", 
        "BcLooseCombCuts": "in_range(0.75*GeV, AM, 7.5*GeV)", 
        "BcTightCombCuts": "in_range(3.2*GeV, AM, 7.5*GeV)", 
        "CCCut": "", 
        "EtacCombCuts": "in_range(2.7*GeV, AM, 3.3*GeV)", 
        "EtacMomCuts": "in_range(2.7*GeV, MM, 3.3*GeV) & (VFASPF(VCHI2/VDOF) < 9.)", 
        "FakeMuonCuts": "(PROBNNmu > 0.1) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 16)", 
        "KaonCuts": "(PROBNNk > 0.65)  & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 4)", 
        "KsCuts": "(ADMASS('KS0') < 30.*MeV) & (BPVDLS>5) & (PT > 500*MeV) & (MAXTREE('pi-'==ABSID, PROBNNpi) > 0.45) & (MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.4) & (MAXTREE('pi-'==ABSID, TRCHI2DOF) < 5)", 
        "LinePostscale": 1.0, 
        "LinePrescale": 1.0, 
        "MuonCuts": "(PROBNNmu > 0.1) & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)", 
        "Pion4TauCuts": "(PROBNNpi > 0.65) & (PT > 250*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 16)",
        "PionCuts": "(PROBNNpi > 0.65) & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 4)", 
        "ProtonCuts": "(PROBNNp > 0.2)   & (PT > 600*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)", 
        "SpdMult": 450.0, 
        'TauCombCuts': "in_range(500.*MeV, AM, 2100.*MeV) & (APT > 800*MeV) & ((AM12<1670.*MeV) or (AM23<1670.*MeV)) & (ACUTDOCA(0.08*mm,''))",
        'TauMomCuts': "in_range(600.*MeV, M, 2000.*MeV)  & (PT > 1000*MeV) & (VFASPF(VCHI2) < 9) & (BPVVDCHI2>16)"
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "BandQ" ]
}

#################################################################################
#                                                                               #
#                                                                               #
# Module for selecting                                                          #
# Ds+-> pi+etap(mumupipi), and Bu->eta_prime(mumupipi)K+, and norm channels     #
# Ds+-> phi(mumu)pipipi and Bu->J/PsiK+                                         #
#                                                                               #
# Author: Xabier Cid Vidal                                                      #
#                                                                               #
#                                                                               #
#################################################################################


Etap2pipimumu = {
    "BUILDERTYPE": "StrippingEtap2pipimumuConf", 
    "CONFIG": {
        "Bu2Etap2PiPiMuMuLinePostscale": 0, 
        "Bu2Etap2PiPiMuMuLinePrescale": 0, 
        "Bu2JPsiKForEtapLinePostscale": 0, 
        "Bu2JPsiKForEtapLinePrescale": 0, 
        "BuKaonMINIPCHI2": 5, 
        "BuKaonP": 2000.0, 
        "BuKaonPIDk": -5, 
        "BuKaonPT": 300.0, 
        "Bu_FDCHI2": 36, 
        "Bu_IPCHI2": 20, 
        "Bu_MASSWIN": 100, 
        "Bu_PT": 300, 
        "Bu_VCHI2DOF": 8, 
        "Ds2Eta2PiPiMuMuLinePostscale": 1, 
        "Ds2Eta2PiPiMuMuLinePrescale": 1, 
        "Ds2Etap2PiPiMuMuLinePostscale": 1, 
        "Ds2Etap2PiPiMuMuLinePrescale": 1, 
        "Ds2Phi3PiLineForEtapPostscale": 0, 
        "Ds2Phi3PiLineForEtapPrescale": 0, 
        "DsPionMINIPCHI2": 5, 
        "DsPionP": 200.0, 
        "DsPionPT": 25.0, 
        "Ds_FDCHI2": 10, 
        "Ds_IPCHI2": 10, 
        "Ds_MAXMASS": 2070, 
        "Ds_MINMASS": 1770, 
        "Ds_PT": 300, 
        "Ds_VCHI2DOF": 15, 
        "Eta_FDCHI2": 2.5, 
        "Eta_IPCHI2": 2.5, 
        "Eta_MASSWIN": 100, 
        "Eta_MAXDOCA": 2, 
        "Eta_PT": 50, 
        "Eta_VCHI2DOF": 20, 
        "EtapMuonMINIPCHI2": 5, 
        "EtapMuonP": 0.0, 
        "EtapMuonPIDmu": -5, 
        "EtapMuonPT": 0.0, 
        "EtapPionMINIPCHI2": 5, 
        "EtapPionP": 0.0, 
        "EtapPionPT": 0.0, 
        "Etap_FDCHI2": 5, 
        "Etap_IPCHI2": 5, 
        "Etap_MASSWIN": 50, 
        "Etap_MAXDOCA": 1, 
        "Etap_PT": 100, 
        "Etap_VCHI2DOF": 10, 
        "JPsi_FDCHI2": 5, 
        "JPsi_IPCHI2": 5, 
        "JPsi_MASSWIN": 75, 
        "JPsi_MAXDOCA": 1, 
        "JPsi_PT": 100, 
        "JPsi_VCHI2DOF": 10, 
        "Phi_FDCHI2": 5, 
        "Phi_IPCHI2": 5, 
        "Phi_MASSWIN": 75, 
        "Phi_MAXDOCA": 1, 
        "Phi_PT": 100, 
        "Phi_VCHI2DOF": 10, 
        "RelatedInfoTools": [
            {
                "Location": "RelInfoVertexIsolation", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[D_s+ -> ( X0 -> ^mu+ mu- pi+ pi- ) pi+ ]CC": "Muon1BDT", 
                    "[D_s+ -> ( X0 -> mu+ ^mu- pi+ pi- ) pi+ ]CC": "Muon2BDT", 
                    "[D_s+ -> ( X0 -> mu+ mu- ^pi+ pi- ) pi+ ]CC": "Pi1BDT", 
                    "[D_s+ -> ( X0 -> mu+ mu- pi+ ^pi- ) pi+ ]CC": "Pi2BDT", 
                    "[D_s+ -> ( X0 -> mu+ mu- pi+ pi- ) ^pi+ ]CC": "Pi3BDT"
                }, 
                "Type": "RelInfoMuonIDPlus", 
                "Variables": [ "MU_BDT" ]
            }, 
            {
                "Location": "RelInfoVertexIsolationBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "RelInfoConeVariables_1.0", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 1.5, 
                "Location": "RelInfoConeVariables_1.5", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 2.0, 
                "Location": "RelInfoConeVariables_2.0", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "RelInfoTrackIsolationBDT", 
                "Type": "RelInfoTrackIsolationBDT"
            }, 
            {
                "Location": "RelInfoTrackIsolationBDT2", 
                "Particles": [
                    1, 
                    2
                ], 
                "Type": "RelInfoTrackIsolationBDT2"
            }, 
            {
                "Location": "RelInfoBstautauCDFIso", 
                "Type": "RelInfoBstautauCDFIso"
            }
        ]
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "BandQ" ]
}


###############################################################################
#                                                                             #
#                                                                             #
# Module for selecting Heavy Baryon lines:                                    #
# Xi_b -> J/psi Xi                                                            #
# Xi_b0 -> J/psi Xi*                                                          #
# Omega_b -> J/psi Omega                                                      #
#                                                                             #
# Author: Yasmine Amhis, updated by Vitalii Lisovskyi                         #
#                                                                             #
#                                                                             #
###############################################################################


HeavyBaryon = {
    "BUILDERTYPE": "HeavyBaryonsConf", 
    "CONFIG": {
        "DLSForLongLived": 5.0, 
        "JpsiMassWindow": 100.0, 
        "KaonPIDK": -5.0, 
        "OmegaMassWindow": 30.0, 
        "OmegabminusMassWindow": 500.0, 
        "PionPIDK": 5.0, 
        "TRCHI2DOF": 4.0, 
        "XiMassWindow": 30.0, 
        "XibminusMassWindow": 300.0, 
        "XibzeroMassWindow": 500.0
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "BandQ" ]
}


###############################################################################
#                                                                             #
#                                                                             #
# Module for selecting Lambda_b0 -> Lambda0 X(3872)/psi(2S) line,             #
#                                                                             #
# Author: Xijun Wang                                                          #
#                                                                             #
#                                                                             #
###############################################################################


Lb2L0X3872 = {
    "BUILDERTYPE": "Lb2L0X3872Conf", 
    "CONFIG": {
        "JpsiComCuts": "(in_range(2.5*GeV, AM, 4.0*GeV))", 
        "JpsiMomCuts": "(VFASPF(VCHI2) < 25.)", 
        "L0Cuts": "(MAXTREE('p+'==ABSID, PT) > 10.*MeV) & (MAXTREE('pi-'==ABSID, PT) > 10.*MeV) & (ADMASS('Lambda0') < 50.*MeV) & (VFASPF(VCHI2) < 25 )", 
        "LbComCuts": "((AM > 5000.*MeV) & (AM < 6200.*MeV))", 
        "LbMomCuts": "(VFASPF(VCHI2) < 25.)", 
        "LooseJpsiCuts": "(MINTREE('mu+'==ABSID,PT) > 500.0 *MeV) & (MM > 2996.916) & (MM < 3196.916) & ((BPVDLS>2.5) | (BPVDLS<-2.5)) & (MINTREE('mu+'==ABSID,PIDmu) > 0.0)  & (VFASPF(VCHI2) < 25.)", 
        "PionCuts_X": "(TRCHI2DOF < 4.0) & (PT > 10.*MeV) & (PROBNNpi > 0.2) & ( MIPCHI2DV(PRIMARY) >4)", 
        "Prescale": 1.0, 
        "XComCuts": "((AM > 3200*MeV) & (AM < 5000.*MeV))", 
        "XMomCuts": "(VFASPF(VCHI2) < 100.)"
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "BandQ" ]
}


###############################################################################
#                                                                             #
#                                                                             #
# Module for selecting                                                        #
# Lambda_b -> Lambda K- D_s+                                                  #
# Xi_b- -> Lambda K- D0                                                       #
# Xi_b0 -> Lambda K- D+                                                       #
#                                                                             #
# Author: Marian Stahl                                                        #
#                                                                             #
#                                                                             #
###############################################################################


XbToLambdaKmX = {
    "BUILDERTYPE": "XbToLambdaKmXConf", 
    "CONFIG": {
        "b_decays": {
            "Lb2LKDs": {
                "DD": {
                    "comb12_cut": "(ADOCA(1,2)<0.3*mm) & (AMASS(1,2)<5*GeV)", 
                    "comb_cut": "(ADOCA(1,3)<2*mm) & (ADOCA(2,3)<2*mm) & (ASUM(PT)>4.8*GeV) & (AMASS()>5*GeV) & (AMASS()<6.1*GeV)", 
                    "daughters": [
                        "ds", 
                        "bach_kaon", 
                        "lambda_dd"
                    ], 
                    "descriptors": [
                        "[Lambda_b0 -> D_s+ K- Lambda0]cc"
                    ], 
                    "mother_cut": "(in_range(5.1*GeV,M,6*GeV)) & (P>36*GeV) & (PT>4.5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0.1*mm) & (BPVIPCHI2()<16) &\n                             (CHILDIP(1)<0.3*mm) & (CHILDIP(2)<0.2*mm) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>0.0*mm)"
                }, 
                "LL": {
                    "comb12_cut": "(ADOCA(1,2)<0.3*mm) & (AMASS(1,2)<5*GeV)", 
                    "comb_cut": "(ADOCA(1,3)<0.6*mm) & (ADOCA(2,3)<0.3*mm) & (ASUM(PT)>4.8*GeV) & (AMASS()>5*GeV) & (AMASS()<6.1*GeV)", 
                    "daughters": [
                        "ds", 
                        "bach_kaon", 
                        "lambda_ll"
                    ], 
                    "descriptors": [
                        "[Lambda_b0 -> D_s+ K- Lambda0]cc"
                    ], 
                    "mother_cut": "(in_range(5.1*GeV,M,6*GeV)) & (P>36*GeV) & (PT>4.5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0.2*mm) & (BPVIPCHI2()<16) & (CHILDIP(1)<0.3*mm) &\n                             (CHILDIP(2)<0.2*mm) & (CHILDIP(3)<0.4*mm) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>0.0*mm) & ((CHILD(VFASPF(VZ),3) - VFASPF(VZ))>10*mm)"
                }
            }, 
            "Xib02LKD": {
                "DD": {
                    "comb12_cut": "(ADOCA(1,2)<0.3*mm) & (AMASS(1,2)<5*GeV)", 
                    "comb_cut": "(ADOCA(1,3)<2*mm) & (ADOCA(2,3)<2*mm) & (ASUM(PT)>5.2*GeV) & (AMASS()>5.3*GeV) & (AMASS()<6.2*GeV)", 
                    "daughters": [
                        "dp", 
                        "bach_kaon", 
                        "lambda_dd"
                    ], 
                    "descriptors": [
                        "[Xi_b0 -> D+ K- Lambda0]cc"
                    ], 
                    "mother_cut": "(in_range(5.4*GeV,M,6.1*GeV)) & (P>36*GeV) & (PT>5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0.1*mm) & (BPVIPCHI2()<16) &\n                             (CHILDIP(1)<0.3*mm) & (CHILDIP(2)<0.2*mm) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>0.0*mm)"
                }, 
                "LL": {
                    "comb12_cut": "(ADOCA(1,2)<0.3*mm) & (AMASS(1,2)<5*GeV)", 
                    "comb_cut": "(ADOCA(1,3)<0.6*mm) & (ADOCA(2,3)<0.3*mm) & (ASUM(PT)>5.2*GeV) & (AMASS()>5.3*GeV) & (AMASS()<6.2*GeV)", 
                    "daughters": [
                        "dp", 
                        "bach_kaon", 
                        "lambda_ll"
                    ], 
                    "descriptors": [
                        "[Xi_b0 -> D+ K- Lambda0]cc"
                    ], 
                    "mother_cut": "(in_range(5.4*GeV,M,6.1*GeV)) & (P>36*GeV) & (PT>5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0.2*mm) & (BPVIPCHI2()<16) & (CHILDIP(1)<0.3*mm) &\n                             (CHILDIP(2)<0.2*mm) & (CHILDIP(3)<0.4*mm) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>0.0*mm) & ((CHILD(VFASPF(VZ),3) - VFASPF(VZ))>10*mm)"
                }
            }, 
            "Xib2LKD0": {
                "DD": {
                    "comb12_cut": "(ADOCA(1,2)<0.3*mm) & (AMASS(1,2)<5*GeV)", 
                    "comb_cut": "(ADOCA(1,3)<2*mm) & (ADOCA(2,3)<2*mm) & (ASUM(PT)>5.2*GeV) & (AMASS()>5.3*GeV) & (AMASS()<6.4*GeV)", 
                    "daughters": [
                        "dz", 
                        "bach_kaon", 
                        "lambda_dd"
                    ], 
                    "descriptors": [
                        "[Xi_b- -> D0 K- Lambda0]cc"
                    ], 
                    "mother_cut": "(in_range(5.4*GeV,M,6.3*GeV)) & (P>36*GeV) & (PT>5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0.1*mm) & (BPVIPCHI2()<16) &\n                             (CHILDIP(1)<0.3*mm) & (CHILDIP(2)<0.2*mm) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>0.0*mm)"
                }, 
                "LL": {
                    "comb12_cut": "(ADOCA(1,2)<0.3*mm) & (AMASS(1,2)<5*GeV)", 
                    "comb_cut": "(ADOCA(1,3)<0.6*mm) & (ADOCA(2,3)<0.3*mm) & (ASUM(PT)>5.2*GeV) & (AMASS()>5.3*GeV) & (AMASS()<6.4*GeV)", 
                    "daughters": [
                        "dz", 
                        "bach_kaon", 
                        "lambda_ll"
                    ], 
                    "descriptors": [
                        "[Xi_b- -> D0 K- Lambda0]cc"
                    ], 
                    "mother_cut": "(in_range(5.4*GeV,M,6.3*GeV)) & (P>36*GeV) & (PT>5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0.2*mm) & (BPVIPCHI2()<16) & (CHILDIP(1)<0.3*mm) &\n                             (CHILDIP(2)<0.2*mm) & (CHILDIP(3)<0.4*mm) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>0.0*mm) & ((CHILD(VFASPF(VZ),3) - VFASPF(VZ))>10*mm)"
                }
            }
        }, 
        "bach_kaon": {
            "filter": "(P>4*GeV) & (PT>250*MeV) & (MIPCHI2DV(PRIMARY)>4) & (PROBNNk>0.03)", 
            "tes": "Phys/StdAllNoPIDsKaons/Particles"
        }, 
        "bach_pion": {
            "filter": "(P>2*GeV) & (PT>150*MeV) & (MIPCHI2DV(PRIMARY)>4) & (PROBNNpi>0.03)", 
            "tes": "Phys/StdAllNoPIDsPions/Particles"
        }, 
        "dfb_kids": {
            "D": {
                "K": 1, 
                "pi1": 2, 
                "pi2": 3
            }, 
            "D0": {
                "K": 1, 
                "pi": 2
            }, 
            "Ds": {
                "Km": 2, 
                "Kp": 1, 
                "pi": 3
            }
        }, 
        "dfb_weights_path": "$TMVAWEIGHTSROOT/data/DfromB/{}Pi_2018_GBDT.weights.xml",
        "dp": {
            "comb12_cut": "(ADOCA(1,2)<0.2*mm) & (AMASS(1,2)<1840*MeV)", 
            "comb_cut": "(ASUM(PT)>1.4*GeV) & (ADAMASS('D+')<120*MeV) & (ADOCA(1,3)<0.3*mm) & (ADOCA(2,3)<0.3*mm)", 
            "descriptors": [
                "[D+ -> K- pi+ pi+]cc"
            ], 
            "mother_cut": "(ADMASS('D+')<80*MeV) & (P>12*GeV) & (PT>1.2*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0.4*mm) & (CHILDIP(1)<0.2*mm) &\n                         (CHILDIP(2)<0.2*mm) & (CHILDIP(3)<0.3*mm) & (VALUE('LoKi::Hybrid::DictValue/D_BDT')>-0.9)"
        }, 
        "ds": {
            "comb12_cut": "(ADOCA(1,2)<0.2*mm) & (AMASS(1,2)<1940*MeV)", 
            "comb_cut": "(ASUM(PT)>1.4*GeV) & (ADAMASS('D_s+')<120*MeV) & (ADOCA(1,3)<0.3*mm) & (ADOCA(2,3)<0.3*mm)", 
            "descriptors": [
                "[D_s+ -> K+ K- pi+]cc"
            ], 
            "mother_cut": "(ADMASS('D_s+')<80*MeV) & (P>12*GeV) & (PT>1.3*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0.4*mm) &\n                         (CHILDIP(1)<0.2*mm) & (CHILDIP(2)<0.2*mm) & (CHILDIP(3)<0.3*mm) & (VALUE('LoKi::Hybrid::DictValue/Ds_BDT')>-0.9)"
        }, 
        "dz": {
            "comb_cut": "(ASUM(PT)>1.4*GeV) & (ADAMASS('D0')<120*MeV) & (ADOCA(1,2)<0.3*mm)", 
            "descriptors": [
                "[D0 -> K- pi+]cc"
            ], 
            "mother_cut": "(ADMASS('D0')<80*MeV) & (P>12*GeV) & (PT>1.2*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0.4*mm) &\n                         (CHILDIP(1)<0.3*mm) & (CHILDIP(2)<0.3*mm) & (VALUE('LoKi::Hybrid::DictValue/D0_BDT')>-0.8)"
        }, 
        "lambda_dd": {
            "filter": "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>1*GeV) & (CHILDIP(1)<2*mm) & (MAXTREE('p+'==ABSID,P)>9*GeV)", 
            "tes": "Phys/StdLooseLambdaDD/Particles"
        }, 
        "lambda_ll": {
            "filter": "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>0.8*GeV) & (BPVVDZ>10*mm) & (BPVVDCHI2>32) &\n                    (DOCA(1,2)<0.5*mm) & (DOCACHI2(1,2)<16) & (MAXTREE('p+'==ABSID,P)>7.5*GeV) &\n                    (MAXTREE('p+'==ABSID,MIPCHI2DV(PRIMARY))>9) & (MAXTREE('pi+'==ABSID,MIPCHI2DV(PRIMARY))>9)", 
            "tes": "Phys/StdVeryLooseLambdaLL/Particles"
        }
    }, 
    "STREAMS": {
        "Bhadron": [
            "StrippingXbToLambdaKmX_Xib2LKD0Line", 
            "StrippingXbToLambdaKmX_Xib02LKDLine", 
            "StrippingXbToLambdaKmX_Lb2LKDsLine"
        ]
    }, 
    "WGs": [ "BandQ" ]
}


###############################################################################
#                                                                             #
#                                                                             #
# Module for selecting Ccbar->LambdaLambda detached line,                     #
# with loose PT, PID cuts.                                                    #
# Updated for run2 re-stripping                                               #
# A new line named 'Jpsi2LooseLambdaLambdaLine'                               #
# which is made up by Lambda0 anti-Lambda0                                    #
# from basic StdAllLoose and StdNoPIDsDown pion/proton combination            #
# Apply very loose P/PT cuts on proton and pions                              #
#                                                                             #
# Authos: Andrii Usachov, Jinlin Fu, Ziyi Wang                                #
#                                                                             #
#                                                                             #
###############################################################################

Ccbar2LambdaLambda = {
    'BUILDERTYPE' : 'Ccbar2LambdaLambdaConf',
    'CONFIG' : {
          'TRCHI2DOF'               :     5.
        , 'TRIPCHI2'                :     4.
        , 'ProtonProbNNp'           :     0.3
        , 'ProtonPTSec'             :   250.  # MeV
        , 'PionProbNNpi'            :     0.1 
        , 'PionPTSec'               :   250.  # MeV
        , 'LambdaMassW'             :    30   
        , 'LambdaVCHI2DOF'          :     9   
        , 'CombMaxMass'             : 15100.  # MeV, before Vtx fit
        , 'CombMinMass'             :  2700.  # MeV, before Vtx fit
        , 'MaxMass'                 : 15000.  # MeV, after Vtx fit
        , 'MinMass'                 :  2750.  # MeV, after Vtx fit
        , 'Lambda0_MassWindowLarge' :   180   #MeV
        , 'Lambda0_APT'             :   700   #MeV
        , 'Lambda0_ENDVERTEXCHI2'   :    10
        , 'LambdaBPVDLSMAX'         :    20
        , 'TRCHI2DOFMax'            :     3.0
        , 'TrGhostProbMax'          :     0.5
        , 'MINIPCHI2'               :     4.0
        , 'PionP'                   :     2.0*GeV
        , 'PionPT'                  :   100.0*MeV
        , 'PionPIDK'                :    10.0
        , 'ProtonP'                 :     2.0*GeV
        , 'ProtonPT'                :   100.0*MeV
        , 'Proton_PIDpPIDpi_Min'    :     5.0
        , 'Proton_PIDpPIDK_Min'     :     0.0
        , 'Jpsi_ENDVERTEX_Z'        :   200*mm
        , 'Jpsi_BPVIPCHI2'          :    25.0
        },
    'STREAMS' : [ 'Charm'],
    'WGs'     : [ 'BandQ']
    }
