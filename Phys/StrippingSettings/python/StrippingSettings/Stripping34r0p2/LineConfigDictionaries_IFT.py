###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

HeL2ppppi = {
    'WGs'         : ['IFT'],
    'BUILDERTYPE' : 'HeL2ppppiConf',
    'CONFIG'      : {'Trk_MaxChi2Ndof'    : 3.0,
                     'Trk_MaxGhostProb'   : 0.4,
                     'Trk_MinIPChi2'      : 9.0,

                     'Proton_MinP'        : 3000.0,
                     'Proton_MinProbNNp'  : 0.10,
                     'Pion_MinP'          : 1500.0,
                     'Pion_MinProbNNpi'   : 0.05,

                     'HeL_MaxM_4body'     : 5200.0,
                     'HeL_MaxM'           : 5000.0,
                     'HeL_MaxDOCAChi2'    : 20.0,
                     'HeL_MaxVtxChi2Ndof' : 10.0,
                     'HeL_MinFDChi2'      : 100.0,
                     'HeL_MaxIPChi2'      : 25.0,
                     'HeL_MinDira'        : 0.9995,
                     'Prescale'          : 1.0,
                     'Postscale'         : 1.0
                     },
    'STREAMS'     : ['BhadronCompleteEvent'],
}

HeMedian = {
    'BUILDERTYPE': 'HeMedianConf',
    'WGs': ['IFT'],
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
         'Prescale': 1.0,
         'HeliumMedian': 115.0,
         'HeliumP': 2500,
         'HeliumPT': 300,
         'TrackGhostProb': 0.15,
         'eta': 5.0,
         'NumVeloClusters' : 12,
         'TrackProbNNpi'    : 0.5,
         'RequiredRawEvents' : ["Calo", "Rich", "Velo", "Tracker"],
    },
}

HeNOverflows = {
    'BUILDERTYPE': 'HeNOverflowsConf',
    'WGs': ['IFT'],
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'Prescale': 1.0,
         'Trk_nOverflows': 3,
         'RequiredRawEvents' : ["Calo", "Rich", "Velo", "Tracker"],
    },
}

Lb2dpX = {
    'BUILDERTYPE' : 'Lb2dpXConf',
    'WGs'         : ['IFT'],
    'STREAMS'     : ['BhadronCompleteEvent'],
    'CONFIG'      : {
        'Prescale_pi'          : 0.5 ,
        'Prescale_pipi'        : 1.0 ,

        'TrackChi2Ndof'        : 3.0,
        'TrackGhostProb'       : 0.4,
        'TrackIPChi2'          : 16.,

        'PionPT'               : 500,
        'PionP'                : 1500,
        'PionPIDKpi'           : 0,

        'ProtonPT'             : 500,
        'ProtonP'              : 15000,
        'ProtonPIDppi'         : 15,
        'ProtonPIDpK'          : 10,

        'DeuteronPT'           : 500,
        'DeuteronP'            : 35000,

        'LbMassMax'            : 7000. ,
        'LbVtxChi2'            : 5. ,
        'LbDIRA_pipi'          : 0.999,
        'LbDIRA_pi'            : 0.9999,
        'LbFDChi2'             : 150,
        'LbPT'                 : 1500,
        'LbIPChi2_pi'          : 25,
        'LbIPChi2_pipi'        : 50,
    },
}

Lb2V0pphh = {
    'WGs': ['IFT'],
    'BUILDERTYPE': 'Lb2V0pphhConf',
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'Trk_Chi2'               : 3.0,
        'Trk_GhostProb'          : 0.4,
        'Trk_IPChi2'             :   9,

        'ProtonP'                : 15000,
        'ProtonPT'               : 500,
        'ProtonProbNNp'          : 0.05,
        'ProtonPIDppi'           : 5,
        'ProtonPIDpK'            : 0,
        'PionP'                  : 1500,
        'PionPT'                 :  500,

        'Lambda_LL_MassWindow'   : 20.0,
        'Lambda_LL_VtxChi2'      : 15.0,
        'Lambda_LL_FDChi2'       : 80.0,

        'Lb_Mlow'                : 1319.0,
        'Lb_Mhigh'               : 600.0,
        'Lb_APTmin'              : 250.0,
        'Lb_PTmin'               : 250.0,

        'LbDaug_MedPT_PT'        : 800.0,
        'LbDaug_MaxPT_IP'        : 0.05,
        'LbDaug_LL_maxDocaChi2'  : 10.0,
        'LbDaug_LL_PTsum'        : 1500.0,

        'Lbh_LL_PTMin'           : 500.0,
        'Lb_VtxChi2Ndof'         : 12.0,
        'Lb_LL_Dira'             : 0.995,
        'Lb_LL_IPCHI2wrtPV'      : 20.0,
        'Lb_FDwrtPV'             : 1.0,
        'Lb_LL_FDChi2'           : 40.0,

        'GEC_MaxTracks'          : 250,
        # 2015 Triggers
        'HLT1Dec'                : 'Hlt1(Two)?TrackMVADecision',
        'HLT2Dec'                : 'Hlt2Topo[234]BodyDecision',
        'Prescale'               : 1.0,
        'Postscale'              : 1.0,
    },
}
