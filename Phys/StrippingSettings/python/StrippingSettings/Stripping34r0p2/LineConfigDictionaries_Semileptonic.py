###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
 
################################################################################
##                          S T R I P P I N G  34r0p2                         ##
##                                                                            ##
##  Configuration for SL WG                                                   ##
##  Contact person: Lupato Anna (anna.lupato@cern.ch)                         ##
################################################################################

B23MuNu = {
    "BUILDERTYPE": "B23MuNuConf", 
    "CONFIG": {
        "BPT": 2000.0, 
        "CORRM_MAX": 10000.0, 
        "CORRM_MIN": 2500.0, 
        "DIRA": 0.99, 
        "Electron_MinIPCHI2": 25.0, 
        "Electron_PIDe": 2.0, 
        "Electron_PIDeK": 0.0, 
        "Electron_PT": 200.0, 
        "FlightChi2": 30.0, 
        "LOWERMASS": 0.0, 
        "MisIDPrescale": 0.01, 
        "Muon_MinIPCHI2": 9.0, 
        "Muon_PIDmu": 0.0, 
        "Muon_PIDmuK": 0.0, 
        "Muon_PT": 0.0, 
        "SpdMult": 900, 
        "Track_CHI2nDOF": 3.0, 
        "Track_GhostProb": 0.35, 
        "UPPERMASS": 7500.0, 
        "VertexCHI2": 4.0
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "Semileptonic" ]
}

B2Invis = {
    "BUILDERTYPE": "B2InvisAllLinesConf", 
    "CONFIG": {
        "BDIRA": 0.9995, 
        "BFDCHI2HIGH": 150.0, 
        "BVCHI2DOF": 4.0, 
        "GEC_nSPDHits": 600.0, 
        "PionMINIPCHI2": 16.0, 
        "PionP": 3000.0, 
        "PionPIDK": 0.0, 
        "PionPT": 500.0, 
        "PionTRCHI2": 3.0, 
        "ProtonMINIPCHI2": 16.0, 
        "ProtonP": 15000.0, 
        "ProtonPIDK": 4.0, 
        "ProtonPIDp": 4.0, 
        "ProtonPT": 800.0, 
        "ProtonTRCHI2": 3.0, 
        "TRGHOSTPROB": 0.3, 
        "XuDIRA": 0.9995, 
        "XuFDCHI2HIGH": 150.0, 
        "XuMassLower": 1230.0, 
        "XuMassUpper": 2800, 
        "XuPT": 1800.0, 
        "XuPiConstrainMLower": 2300, 
        "XuPiConstrainMUpper": 2700, 
        "XuPiMassLower": 1800.0, 
        "XuPiMassUpper": 2900, 
        "XuPiP": 40000.0, 
        "XuPiPT": 2500.0, 
        "XuVCHI2DOF": 3.0, 
        "massDiff": 400
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "Semileptonic" ]
}

B2XTauNuAllLines = {
    "BUILDERTYPE": "B2XTauNuAllLinesConf", 
    "CONFIG": {
        "B_BPVDIRA": 0.995, 
        "B_DOCAMAX": 0.15, 
        "B_DeltaM_high": 300.0, 
        "B_DeltaM_low": -2979.0, 
        "B_upperDeltaM_high": 1721.0, 
        "B_upperDeltaM_low": 720.0, 
        "D02K3pi_AMassW": 60.0, 
        "D02K3pi_BPVVDCHI2": 36.0, 
        "D02K3pi_DIRA": 0.997, 
        "D02K3pi_DOCAMAX": 0.2, 
        "D02K3pi_K_IPCHI2": 15.0, 
        "D02K3pi_K_P": 4.0, 
        "D02K3pi_K_PIDK": 1, 
        "D02K3pi_K_PT": 800.0, 
        "D02K3pi_K_TRCHI2DOF": 3.0, 
        "D02K3pi_MIPCHI2": 10.0, 
        "D02K3pi_MassW": 40.0, 
        "D02K3pi_P": 20.0, 
        "D02K3pi_PT": 2000.0, 
        "D02K3pi_Pis_IPCHI2": 25.0, 
        "D02K3pi_Pis_P": 2.0, 
        "D02K3pi_Pis_PIDK": 8.0, 
        "D02K3pi_Pis_PT": 400.0, 
        "D02K3pi_Pis_TRCHI2": 3.0, 
        "D02K3pi_VCHI2": 6.0, 
        "D02K3pi_pi_MAXPT": 800.0, 
        "D02K3pi_pi_SUMPT": 2000.0, 
        "D02K3pi_slowPi_PT": 50.0, 
        "D02K3pi_slowPi_TRCHI2DOF": 30.0, 
        "D02K3pi_tauR_high": 6.0, 
        "D0_BPVVDCHI2": 36.0, 
        "D0_DIRA": 0.995, 
        "D0_DOCAMAX": 0.5, 
        "D0_MassW": 40.0, 
        "D0_PT": 1200.0, 
        "D0_VCHI2": 10.0, 
        "DInvVertD": 1.0, 
        "D_BPVVDCHI2": 50.0, 
        "D_DIRA": 0.995, 
        "D_K_IPCHI2": 10.0, 
        "D_K_PIDK": -3, 
        "D_K_PT": 150.0, 
        "D_K_TRCHI2DOF": 30.0, 
        "D_MIPCHI2": 10.0, 
        "D_MassW": 40.0, 
        "D_PT": 1600.0, 
        "D_Pi_IPCHI2": 10.0, 
        "D_Pi_PIDK": 50.0, 
        "D_Pi_PT": 150.0, 
        "D_Pi_TRCHI2": 3.0, 
        "D_VCHI2": 10.0, 
        "DdoubleStar0_DeltaM_high": 450.0, 
        "DdoubleStar0_DeltaM_low": 350.0, 
        "DdoubleStar0_MassW": 300.0, 
        "DdoubleStar0_PT": 500.0, 
        "DdoubleStar0_Pi_IPCHI2": 4.0, 
        "DdoubleStar0_Pi_PIDK": 8.0, 
        "DdoubleStar0_Pi_TRCHI2": 3.0, 
        "DdoubleStar0_VCHI2": 25.0, 
        "DdoubleStar2PiDstar02GammaD0_DeltaM_high": 450.0, 
        "DdoubleStar2PiDstar02GammaD0_DeltaM_low": 350.0, 
        "DdoubleStar2PiDstar02GammaD0_MassW": 300.0, 
        "DdoubleStar2PiDstar02GammaD0_PT": 500.0, 
        "DdoubleStar2PiDstar02GammaD0_Pi_IPCHI2": 4.0, 
        "DdoubleStar2PiDstar02GammaD0_Pi_PIDK": 8.0, 
        "DdoubleStar2PiDstar02GammaD0_Pi_TRCHI2": 3.0, 
        "DdoubleStar2PiDstar02GammaD0_VCHI2": 25.0, 
        "DdoubleStar2PiDstar02Pi0D0_DeltaM_high": 450.0, 
        "DdoubleStar2PiDstar02Pi0D0_DeltaM_low": 350.0, 
        "DdoubleStar2PiDstar02Pi0D0_MassW": 300.0, 
        "DdoubleStar2PiDstar02Pi0D0_PT": 500.0, 
        "DdoubleStar2PiDstar02Pi0D0_Pi_IPCHI2": 4.0, 
        "DdoubleStar2PiDstar02Pi0D0_Pi_PIDK": 8.0, 
        "DdoubleStar2PiDstar02Pi0D0_Pi_TRCHI2": 3.0, 
        "DdoubleStar2PiDstar02Pi0D0_VCHI2": 25.0, 
        "Dplus_K_PIDK": 3, 
        "Dplus_K_PT": 1500.0, 
        "Dplus_K_TRPCHI2": 1e-08, 
        "Dplus_Pi_TRPCHI2": 1e-08, 
        "Ds_BPVVDCHI2": 36.0, 
        "Ds_K_PT": 1500.0, 
        "DstD02K3pi_D0_K_PIDK": -3, 
        "DstD02K3pi_D0_K_PT": 400.0, 
        "DstD02K3pi_D0_MIPCHI2": 0, 
        "DstD02K3pi_D0_Pis_IPCHI2": 15, 
        "DstD02K3pi_D0_Pis_PT": 250.0, 
        "DstD02K3pi_D0_VCHI2": 10.0, 
        "DstD02K3pi_D0_pi_MAXPT": 250, 
        "DstD02K3pi_D0_pi_SUMPT": 250, 
        "DstD02K3pi_DeltaM": 160.0, 
        "DstD02K3pi_MassW": 50.0, 
        "DstD02K3pi_PT": 1250.0, 
        "DstD02K3pi_VCHI2": 25.0, 
        "Dstar02GammaD0_DeltaM_high": 250.0, 
        "Dstar02GammaD0_DeltaM_low": 0.0, 
        "Dstar02GammaD0_MassW": 50.0, 
        "Dstar02GammaD0_PT": 1250.0, 
        "Dstar02GammaD0_Pi_PT": 400.0, 
        "Dstar02GammaD0_VCHI2": 25.0, 
        "Dstar02Pi0D0_DeltaM_high": 200.0, 
        "Dstar02Pi0D0_DeltaM_low": 120.0, 
        "Dstar02Pi0D0_MassW": 50.0, 
        "Dstar02Pi0D0_PT": 1250.0, 
        "Dstar02Pi0D0_Pi_PT": 200.0, 
        "Dstar02Pi0D0_VCHI2": 25.0, 
        "Dstar_DeltaM_high": 160.0, 
        "Dstar_DeltaM_low": 135.0, 
        "Dstar_MassW": 50.0, 
        "Dstar_PT": 1250.0, 
        "Dstar_VCHI2": 25.0, 
        "Jpsi_MassW": 80.0, 
        "Jpsi_PT": 2000.0, 
        "Jpsi_VCHI2": 9.0, 
        "LbInvVertD": 1.0, 
        "Lc_BPVVDCHI2": 50.0, 
        "Lc_DIRA": 0.995, 
        "Lc_K_IPCHI2": 10.0, 
        "Lc_K_PIDK": 3.0, 
        "Lc_K_PT": 150.0, 
        "Lc_K_TRCHI2DOF": 3.0, 
        "Lc_K_TRPCHI2": 1e-08, 
        "Lc_MIPCHI2": 10.0, 
        "Lc_MassW": 30.0, 
        "Lc_PT": 1200.0, 
        "Lc_Pi_IPCHI2": 10.0, 
        "Lc_Pi_PIDK": 50.0, 
        "Lc_Pi_PT": 150.0, 
        "Lc_Pi_TRCHI2": 3.0, 
        "Lc_Pi_TRPCHI2": 1e-08, 
        "Lc_VCHI2": 10.0, 
        "Lc_p_IPCHI2": 10.0, 
        "Lc_p_PIDp": 5.0, 
        "Lc_p_PT": 150.0, 
        "Lc_p_TRCHI2DOF": 3.0, 
        "Lc_p_TRPCHI2": 1e-08, 
        "Muon_PT": 1000.0, 
        "Muon_TRCHI2DOF": 3.0, 
        "Postscale": 1.0, 
        "Prescale_B0d2DTauNu": 1.0, 
        "Prescale_B0d2DdoubleStar2PiDstar02GammaD0TauNu": 1.0, 
        "Prescale_B0d2DdoubleStar2PiDstar02Pi0D0TauNu": 1.0, 
        "Prescale_B0d2DstarD02K3piSlowWSTauNu": 1.0, 
        "Prescale_B0d2DstarD02K3piTauNu": 1.0, 
        "Prescale_B0d2DstarD02K3piTauNuWS": 1.0, 
        "Prescale_B0d2DstarD02K3piWSSlowTauNu": 1.0, 
        "Prescale_B0d2DstarSlowWSTauNu": 1.0, 
        "Prescale_B0d2DstarTauNu": 1.0, 
        "Prescale_B0d2DstarWSSlowTauNu": 1.0, 
        "Prescale_B0s2DsTauNu": 1.0, 
        "Prescale_Bc2JpsiTauNu": 1.0, 
        "Prescale_Bu2D02K3piTauNu": 1.0, 
        "Prescale_Bu2D02K3piTauNuWS": 1.0, 
        "Prescale_Bu2D0TauNu": 1.0, 
        "Prescale_Bu2DdoubleStar0TauNu": 1.0, 
        "Prescale_Bu2Dstar02GammaD0TauNu": 1.0, 
        "Prescale_Bu2Dstar02Pi0D0TauNu": 1.0, 
        "Prescale_Lb2LcTauNu": 1.0, 
        "Prescale_Lb2pTauNu": 1.0, 
        "Prescale_NonPhys": 1.0, 
        "TRGHP": 0.4, 
        "TRGHP_slowPi": 0.6, 
        "TisTosSpecs": {
            "Hlt1.*Decision%TOS": 0
        }, 
        "p_IPCHI2": 35.0, 
        "p_PE": 15000, 
        "p_PIDp": 0.6, 
        "p_PT": 1750.0, 
        "p_TRCHI2DOF": 3.0, 
        "slowPi_PT": 50.0, 
        "slowPi_TRCHI2DOF": 30.0, 
        "tau_BPVVDR": 0.2, 
        "tau_pions_IPCHI2PV": 15.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "Semileptonic" ]
}

BXX = {
    "BUILDERTYPE": "BXXLinesBuilderConf", 
    "CONFIG": {
        "DstarBPVIPCHI2Max": 25.0, 
        "DstarBPVVDCHI2Max": 25.0, 
        "DstarHLT2TOSSelection": "Hlt2Global", 
        "DstarMassDiffDzeroPlusPionMax": 40.0, 
        "DstarPTMin": 3000.0, 
        "DstarPionMIPCHI2DVMax": 4.0, 
        "DstarPionMIPDVMax": 0.3, 
        "DstarPionPIDKMax": 10.0, 
        "DstarPionPTMin": 250.0, 
        "DstarPionTRCHI2DOFMax": 3.0, 
        "DstarVCHI2DOFMax": 5.0, 
        "DzeroADOCACHI2CUT": 20.0, 
        "DzeroBPVDIRAMin": 0.9997, 
        "DzeroBPVIPCHI2Max": 0.9997, 
        "DzeroBPVVDCHI2Min": 120.0, 
        "DzeroBPVVDMin": 4.0, 
        "DzeroDaughterKaonPIDKMin": 4.0, 
        "DzeroDaughterMIPCHI2DVMin": 4.0, 
        "DzeroDaughterPMin": 2000.0, 
        "DzeroDaughterPTMin": 400.0, 
        "DzeroDaughterPionPIDKMax": 10.0, 
        "DzeroDaughterPionPIDmuMax": 10.0, 
        "DzeroDaughterTRCHI2DOFMax": 3.0, 
        "DzeroMassMax": 1700.0, 
        "DzeroMassMin": 1400.0, 
        "DzeroPTMin": 3000.0, 
        "DzeroPionPairMassMax": 975.5, 
        "DzeroPionPairMassMin": 575.5, 
        "DzeroVCHI2DOFMax": 6.0, 
        "InputKaonsForDzeroLocation": "Phys/StdLooseKaons/Particles", 
        "InputPionsForDstarLocation": "Phys/StdAllLoosePions/Particles", 
        "InputPionsForDzeroLocation": "Phys/StdLoosePions/Particles", 
        "prescaleRS": 1.0, 
        "prescaleWS": 0.2
    }, 
    "STREAMS": {
        "Semileptonic": [
            "StrippingDstRSwD02K2PiD0forBXXLine", 
            "StrippingDstWSwD02K2PiD0forBXXLine"
        ]
    }, 
    "WGs": [
        "Charm", 
        "Semileptonic"
    ]
}

Bc2Bs = {
    "BUILDERTYPE": "Bc2BsBuilder", 
    "CONFIG": {
        "BachMuonMINIP": 0.02, 
        "BachMuonP": 1000.0, 
        "BachMuonPIDmu": 0, 
        "BachMuonPT": 150.0, 
        "BachMuonTRCHI2": 4.0, 
        "BachPionMINIP": 0.02, 
        "BachPionP": 1000.0, 
        "BachPionPIDK": 2, 
        "BachPionPT": 350.0, 
        "BachPionTRCHI2": 4.0, 
        "BcDIRA_Loose": 0.9, 
        "BcDIRA_Tight": 0.99, 
        "BcP": 30000.0, 
        "BcPT": 3000.0, 
        "BcPVVDZcut": 0.08, 
        "Bc_CHI2DOF": 20.0, 
        "BsDIRA": 0.9, 
        "BsFDCHI2HIGH": 35.0, 
        "BsMassWindowHigh": 6000.0, 
        "BsMassWindowLow": 5000.0, 
        "BsPVVDZcut": 2.0, 
        "Bs_DsPi_CHI2DOF": 25.0, 
        "Bs_JpsiPhi_CHI2DOF": 35.0, 
        "DsMassWindow": 400.0, 
        "Ds_BPVVDZcut": 0.0, 
        "Ds_CHI2DOF": 10.0, 
        "Ds_DIRA": 0.99, 
        "Ds_FDCHI2HIGH": 100.0, 
        "ElectronGHOSTPROB": 0.6, 
        "ElectronMINIP": 0.05, 
        "ElectronP": 3000.0, 
        "ElectronPIDe": -0.2, 
        "ElectronPT": 1000.0, 
        "ElectronTRCHI2": 10, 
        "GEC_nLongTrk": 250.0, 
        "JpsiMassWindow": 200.0, 
        "JpsiPT": 1000.0, 
        "Jpsi_BPVVDZcut": 1.5, 
        "Jpsi_CHI2DOF": 15.0, 
        "Jpsi_DIRA": 0.9, 
        "K_from_bc_MINIP": 0.05, 
        "K_from_bc_P": 1000.0, 
        "K_from_bc_PIDK": 0.0, 
        "K_from_bc_PT": 150.0, 
        "K_from_bc_TRCHI2": 4.0, 
        "K_from_bs_MINIP": 0.05, 
        "K_from_bs_P": 1000.0, 
        "K_from_bs_PIDK": 0.0, 
        "K_from_bs_PT": 150.0, 
        "K_from_bs_TRCHI2": 4.0, 
        "KaonMINIP": 0.05, 
        "KaonMINIP_phi": 0.025, 
        "KaonP": 3000.0, 
        "KaonPIDK": 5.0, 
        "KaonPIDK_phi": 0.0, 
        "KaonPIDmu": 5.0, 
        "KaonPIDmu_phi": -2.0, 
        "KaonPIDp": 5.0, 
        "KaonPIDp_phi": -2.0, 
        "KaonPT": 400.0, 
        "KaonPT_phi": 200.0, 
        "KaonP_phi": 3000.0, 
        "KaonTRCHI2": 4.0, 
        "MuonGHOSTPROB": 0.35, 
        "MuonMINIP": 0.025, 
        "MuonP": 3000.0, 
        "MuonPIDK": -2.0, 
        "MuonPIDmu": 0.0, 
        "MuonPT": 750.0, 
        "MuonTRCHI2": 4.0, 
        "PhiDIRA": 0.9, 
        "PhiMINIP": 0.05, 
        "PhiMassWindow": 200.0, 
        "PhiPT": 600.0, 
        "PhiVCHI2DOF": 6, 
        "Phi_CHI2DOF": 35.0, 
        "PionFromBsMINIP": 0.05, 
        "PionFromBsP": 1000.0, 
        "PionFromBsPIDK": 2, 
        "PionFromBsPT": 500.0, 
        "PionFromBsTRCHI2": 4.0, 
        "PionMINIP": 0.05, 
        "PionP": 3000.0, 
        "PionPIDK": -2.0, 
        "PionPT": 400.0, 
        "PionTRCHI2": 4.0, 
        "TRGHOSTPROB": 0.5
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "Semileptonic" ]
}


SemilepISR = {
    "BUILDERTYPE": "SemilepISRAllLinesConf", 
    "CONFIG": {
        "B_DIRA": 0.99, 
        "B_D_DZ": -2.0, 
        "B_DocaChi2Max": 10, 
        "B_MassMax": 8000.0, 
        "B_MassMin": 2200.0, 
        "B_VCHI2DOF": 9.0, 
        "D_AMassWin": 90.0, 
        "D_BPVDIRA": 0.99, 
        "D_DocaChi2Max": 20, 
        "D_FDCHI2": 25.0, 
        "D_MassMax": 1700.0, 
        "D_MassMin": 700.0, 
        "D_MassWin": {
            "Omegac": 60.0, 
            "Xic0": 60.0, 
            "default": 80.0
        }, 
        "D_VCHI2DOF": 6.0, 
        "ElectronPIDe": 3.0, 
        "ElectronPT": 300.0, 
        "GEC_nLongTrk": 250, 
        "HLT1": "HLT_PASS_RE('Hlt1.*Decision')", 
        "HLT2": "HLT_PASS_RE('Hlt2.*Decision')", 
        "HadronIPCHI2": 4.0, 
        "HadronP": 2000.0, 
        "HadronPT": 250.0, 
        "KaonP": 2000.0, 
        "KaonPIDK": -2.0, 
        "Monitor": False, 
        "MuonIPCHI2": 9.0, 
        "MuonP": 6000.0, 
        "MuonPIDmu": 0.0, 
        "MuonPT": 1000.0, 
        "PionPIDK": 10.0, 
        "ProtonP": 8000.0, 
        "ProtonPIDp": 0.0, 
        "ProtonPIDpK": 0.0, 
        "TRCHI2": 3.0, 
        "TRGHOSTPROB": 0.35, 
        "TTSpecs": {}, 
        "UseNoPIDsInputs": False, 
        "prescaleFakes": 0.05, 
        "prescales": 1
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "Semileptonic" ]
}

