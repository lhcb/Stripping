###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                          S T R I P P I N G  3 4 r 0 p 2                    ##
##                                                                            ##
##  Configuration for B2CC WG                                                 ##
##  Contact person: Jie Wu               (j.wu@cern.ch)                       ##
################################################################################

from GaudiKernel.SystemOfUnits import *

######################################################################
## StrippingB2JpsiHHBs2Jpsif0PrescaledLine (MicroDST)
## StrippingB2JpsiHHBs2JpsiKstarLine(MicroDST)
## StrippingB2JpsiHHLb2JpsipHLine(MicroDST)
## StrippingB2JpsiHHBs2Jpsif0Line (FullDST)
## StrippingB2JpsiHHBs2Jpsif0KaonLine(FullDST)
## StrippingB2JpsiHHBs2Jpsif0wsLine(FullDST)
## -------------------------------------------------------------------
## Lines defined in StrippingB2JpsiHH.py
## Authors: Liming Zhang, Xuesong Liu
## Last changes made by Piera Muzzetto
######################################################################

B2JpsiHH = {
    "BUILDERTYPE": "B2JpsiHHConf",
    "CONFIG": {
        "Bs2Jpsif0Prescale": 0.3,
        "DTF_CTAU": 0.0598,
        "HLTCuts": "HLT_PASS_RE('Hlt2DiMuonJPsiDecision')",
        "JpsiMassWindow": 80,
        "MVACut": "-0.1",
        "PIDKforKaon": 0.0,
        "PIDKforPion": 10.0,
        "PIDKpforKaon": -10.0,
        "PIDpKforProton": -10.0,
        "PIDpforProton": 0.0,
        "PROBNNk": 0.05,
        "PROBNNp": 0.05,
        "PROBNNpi": 0.05,
        "TRCHI2DOF": 5,
        "VCHI2PDOF": 10,
        "XmlFile": "$TMVAWEIGHTSROOT/data/Bs2Jpsif0_BDT_v1r1.xml"
    },
    "STREAMS": {
        "Dimuon": [
            "StrippingB2JpsiHHBs2Jpsif0Line",
            "StrippingB2JpsiHHBs2Jpsif0KaonLine",
            "StrippingB2JpsiHHBs2Jpsif0wsLine"
        ],
        "Leptonic": [
            "StrippingB2JpsiHHBs2Jpsif0PrescaledLine",
            "StrippingB2JpsiHHBs2JpsiKstarLine",
            "StrippingB2JpsiHHLb2JpsipHLine"
        ]
    },
    "WGs": [ "B2CC" ]
}

#############################################################################################
## BhadronCompleteEvent (FullDST) :
##    StrippingBetaSBs2DsPiDetachedLine
## Dimuon (FullDST) :
##    StrippingBetaSBu2JpsiKDetachedLine
##    StrippingBetaSBd2JpsiKstarDetachedLine
##    StrippingBetaSBs2JpsiPhiDetachedLine
##    StrippingBetaSJpsi2MuMuLine
##    StrippingBetaSBd2JpsiKsDetachedLine
## Leptonic (MicroDST) :
##    StrippingBetaSBu2JpsiKPrescaledLine
##    StrippingBetaSBs2JpsiPhiPrescaledLine
##    StrippingBetaSBd2JpsiKstarPrescaledLine
##    StrippingBetaSBd2JpsiKsPrescaledLine
##    StrippingBetaSBd2JpsiKsLDDetachedLine
##    StrippingBetaSBs2JpsiKstarWideLine
##    StrippingBetaSLambdab2JpsiLambdaUnbiasedLine
## ----------------------------------------------------------------------------
## Lines defined in StrippingB2JpsiXforBeta_s.py
## Authors: Greig Cowan, Juan Palacios, Francesca Dordei, Carlos Vazquez Sierra, Xuesong Liu
## Last changes made by Piera Muzzetto
#############################################################################################

BetaS = {
    "BUILDERTYPE": "B2JpsiXforBeta_sConf",
    "CONFIG": {
        "Bd2JpsiKsPrescale": 1.0,
        "Bd2JpsiKstarPrescale": 1.0,
        "Bs2JpsiPhiPrescale": 1.0,
        "Bu2JpsiKPrescale": 1.0,
        "DTF_CTAU": 0.0598,
        "DaughterPT": 1000,
        "HLTCuts": "HLT_PASS_RE('Hlt2DiMuonJPsiDecision')",
        "Jpsi2MuMuPrescale": 0.04,
        "JpsiMassWindow": 80,
        "PIDKCuts": 0.0,
        "PIDpiCuts": 0.0,
        "TRCHI2DOF": 5,
        "VCHI2PDOF": 10
    },
    "STREAMS": {
        "BhadronCompleteEvent": [ "StrippingBetaSBs2DsPiDetachedLine" ],
        "Dimuon": [
            "StrippingBetaSBu2JpsiKDetachedLine",
            "StrippingBetaSBd2JpsiKstarDetachedLine",
            "StrippingBetaSBs2JpsiPhiDetachedLine",
            "StrippingBetaSJpsi2MuMuLine",
            "StrippingBetaSBd2JpsiKsDetachedLine"
        ],
        "Leptonic": [
            "StrippingBetaSBu2JpsiKPrescaledLine",
            "StrippingBetaSBs2JpsiPhiPrescaledLine",
            "StrippingBetaSBd2JpsiKstarPrescaledLine",
            "StrippingBetaSBd2JpsiKsPrescaledLine",
            "StrippingBetaSBd2JpsiKsLDDetachedLine",
            "StrippingBetaSBs2JpsiKstarWideLine",
            "StrippingBetaSLambdab2JpsiLambdaUnbiasedLine"
        ]
    },
    "WGs": [ "B2CC" ]
}

#############################################################################################
## Dimuon (FullDST) :
##    StrippingBd2JpsieeKSFullDSTBd2JpsieeKSFullDSTDetachedLine
## ----------------------------------------------------------------------------
## Lines defined in StrippingBd2JpsieeKSFullDST.py
## Authors: Gerwin Meier
## Last changes made by Gerwin Meier
#############################################################################################

Bd2JpsieeKSFullDST = {
    "BUILDERTYPE": "Bd2JpsieeKSFullDSTConf",
    "CONFIG": {
        "B0_BPV_DIRA": 0.995,
        "B0_BPV_IP": 1.0,
        "B0_DOCA": 3.0,
        "B0_DOCA_chi2": 13.0,
        "BM_DTF_Jpsi_Const_max": 5900.0,
        "BM_DTF_Jpsi_Const_min": 4600.0,
        "BPVLTIME": 0.2,
        "BdMassMax": 6000.0,
        "BdMassMin": 4400.0,
        "BdVertexCHI2pDOF": 6.0,
        "EProbNNe": 0.01,
        "ElectronPID": 0.0,
        "ElectronPT": 500.0,
        "ElectronTrackCHI2pDOF": 5.0,
        "JpsiMassMax": 3300.0,
        "JpsiMassMin": 2300.0,
        "JpsiVertexCHI2pDOF": 13.0,
        "KSBPVDLS": 5.0,
        "KSMassMax": 535.0,
        "KSMassMin": 460.0,
        "KSTau": 1.0,
        "KSVCHI2": 20.0,
        "TRCHI2DOF": 3.0
    },
    "STREAMS": {
        "Dimuon": [ "StrippingBd2JpsieeKSFullDSTBd2JpsieeKSFullDSTDetachedLine" ]
    },
    "WGs": [ "B2CC" ]
}

#############################################################################################
## Dimuon (FullDST) :
##    StrippingBd2JpsieeKSBu2JpsieeKFromTracksDetachedLine
## Leptonic (MicroDST) :
##    StrippingBd2JpsieeKSBd2JpsieeKSFromTracksPrescaledLine
##    StrippingBd2JpsieeKSBd2JpsieeKSFromTracksDetachedLine
##    StrippingBd2JpsieeKSBd2JpsieeKSDetachedLine
## ----------------------------------------------------------------------------
## Lines defined in StrippingBd2JpsieeKS.py
## Authors: Ramon Niet
## Last changes made by Gerwin Meier
#############################################################################################
Bd2JpsieeKS = {
    "BUILDERTYPE": "Bd2JpsieeKSConf",
    "CONFIG": {
        "BPVLTIME": 0.2,
        "BdMassMax": 6000.0,
        "BdMassMin": 4400.0,
        "BdVertexCHI2pDOF": 7.0,
        "ElectronPID": 0.0,
        "ElectronPT": 500.0,
        "ElectronTrackCHI2pDOF": 5.0,
        "JpsiMassMax": 3300.0,
        "JpsiMassMin": 2300.0,
        "JpsiVertexCHI2pDOF": 15.0,
        "KSBPVDLS": 5.0,
        "KSVCHI2": 20.0,
        "KaonPID": 0.0,
        "KplusIP": 9.0,
        "KplusPT": 800.0,
        "Prescale": 0.1,
        "TRCHI2DOF": 3.0
    },
    "STREAMS": {
        "Dimuon": [ "StrippingBd2JpsieeKSBu2JpsieeKFromTracksDetachedLine" ],
        "Leptonic": [
            "StrippingBd2JpsieeKSBd2JpsieeKSFromTracksPrescaledLine",
            "StrippingBd2JpsieeKSBd2JpsieeKSFromTracksDetachedLine",
            "StrippingBd2JpsieeKSBd2JpsieeKSDetachedLine"
        ]
    },
    "WGs": [ "B2CC" ]
}





