###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

################################################################################
##                          S T R I P P I N G  34r0p3                         ##
##                                                                            ##
##  Configuration for IFT WG                                                  ##
##  Contact person: Hendrik Jage     (hendrik.jage@cern.ch)                   ##
################################################################################

He_VLTT_Long = {
    "BUILDERTYPE": "He_VLTT_LongConf", 
    "CONFIG": {
        "NumVeloPhiClusters": 3, 
        "NumVeloRClusters": 3, 
        "NumTTClusters": 2, 
        "VeloPhiDedxMedian": 100.0, 
        "VeloRDedxMedian": 100.0,
        "TTDedxMedian": 80.0, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ] 
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_VLTT_Up = {
    "BUILDERTYPE": "He_VLTT_UpConf", 
    "CONFIG": {
        "NumVeloPhiClusters": 3, 
        "NumVeloRClusters": 3, 
        "NumTTClusters": 2, 
        "VeloPhiDedxMedian": 90.0, 
        "VeloRDedxMedian": 90.0,
        "TTDedxMedian": 80.0, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ] 
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_IT_Long = {
    "BUILDERTYPE": "He_IT_LongConf", 
    "CONFIG": {
        "NumITClusters": 3, 
        "ITDedxMedian": 118.0, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ]
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_IT_Down = {
    "BUILDERTYPE": "He_IT_DownConf", 
    "CONFIG": {
        "NumITClusters": 2, 
        "ITDedxMedian": 90.0, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ]
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_TTOT_Long = {
    "BUILDERTYPE": "He_TTOT_LongConf", 
    "CONFIG": {
        "minP": 0,
        "NumTTClusters": 2, 
        "NumOTTimes": 7, 
        "TTLLDCut": 1.3, 
        "TTLLDVersionHe": "v2", 
        "TTLLDVersionZ1": "v2", 
        "OT_p0": -1.7189, 
        "OT_p1": 0.486, 
        "OT_p2": 13200, 
        "OT_dt": 1, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ] 
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_TTOT_Long2 = {
    "BUILDERTYPE": "He_TTOT_Long2Conf", 
    "CONFIG": {
        "minP": 0,
        "NumTTClusters": 2, 
        "NumOTTimes": 7, 
        "TTLLDVersionHe": "v3", 
        "TTLLDVersionZ1": "v2", 
        "TTLLDCut": 2.7, 
        "OT_p0": -1.7189, 
        "OT_p1": 0.486, 
        "OT_p2": 13200, 
        "OT_dt": 1, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ] 
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_TTOT_Down = {
    "BUILDERTYPE": "He_TTOT_DownConf", 
    "CONFIG": {
        "minP": 0,
        "NumTTClusters": 2, 
        "NumOTTimes": 7, 
        "TTLLDVersionHe": "v2", 
        "TTLLDVersionZ1": "v2", 
        "TTLLDCut": 1.5, 
        "OT_p0": -1.7189, 
        "OT_p1": 0.486, 
        "OT_p2": 13200, 
        "OT_dt": 1, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ] 
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_TTOT_Down2 = {
    "BUILDERTYPE": "He_TTOT_Down2Conf", 
    "CONFIG": {
        "minP": 0,
        "NumTTClusters": 2, 
        "NumOTTimes": 7, 
        "TTLLDVersionHe": "v3", 
        "TTLLDVersionZ1": "v2", 
        "TTLLDCut": 2.9, 
        "OT_p0": -1.7189, 
        "OT_p1": 0.486, 
        "OT_p2": 13200, 
        "OT_dt": 1, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ] 
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_TT_Up = {
    "BUILDERTYPE": "He_TT_UpConf", 
    "CONFIG": {
        "NumTTClusters": 3, 
        "TTLLDVersionHe": "v2", 
        "TTLLDVersionZ1": "v2",
        "TTLLDCut": 0.8, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ] 
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_TT_Up2 = {
    "BUILDERTYPE": "He_TT_Up2Conf", 
    "CONFIG": {
        "NumTTClusters": 2, 
        "TTLLDVersionHe": "v3", 
        "TTLLDVersionZ1": "v2",
        "TTLLDCut": 3.7, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ] 
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_VL_Long = {
    "BUILDERTYPE": "He_VL_LongConf", 
    "CONFIG": {
        "NumVeloPhiClusters": 3, 
        "NumVeloRClusters": 3, 
        "NumOTTimes": -1000, 
        "VeloPhiDedxMedian": 100.0, 
        "VeloRDedxMedian": 100.0, 
        "VeloPhiOverflows": 0, 
        "VeloROverflows": 0,
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ]
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_VL_Up = {
    "BUILDERTYPE": "He_VL_UpConf", 
    "CONFIG": {
        "NumVeloPhiClusters": 3, 
        "NumVeloRClusters": 3, 
        "VeloPhiDedxMedian": 100.0, 
        "VeloRDedxMedian": 100.0, 
        "VeloPhiOverflows": 0, 
        "VeloROverflows": 0,
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ] 
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_VL_Velo = {
    "BUILDERTYPE": "He_VL_VeloConf", 
    "CONFIG": {
        "NumVeloPhiClusters": 5, 
        "NumVeloRClusters": 5, 
        "VeloPhiDedxMedian": 120.0, 
        "VeloRDedxMedian": 120.0, 
        "VeloPhiOverflows": -1, 
        "VeloROverflows": -1,
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ] 
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_SV_DD = {
    "BUILDERTYPE": "He_SV_DDConf", 
    "CONFIG": {
        "Pi_MinIPChi2": -1000, 
        "He_minP": 0, 
        "He_NumTTClusters": 2, 
        "He_NumOTTimes": 7, 
        "He_TTLLDVersionHe": "v2", 
        "He_TTLLDVersionZ1": "v2", 
        "He_TTLLDCut": 1, 
        "OT_p0": -1.7189, 
        "OT_p1": 0.486, 
        "OT_p2": 13200, 
        "He_OT_dt": 1000, 
        "HePi_MaxDocaChi2": 30, 
        "HePi_MaxM_TL": 3100, 
        "HePi_MaxVtxChi2": 25, 
        "HePi_MinFDChi2": 50, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ]
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

He_SV_DD2 = {
    "BUILDERTYPE": "He_SV_DD2Conf", 
    "CONFIG": {
        "Pi_MinIPChi2": -1000, 
        "He_minP": 0, 
        "He_NumTTClusters": 2, 
        "He_NumOTTimes": 7, 
        "He_TTLLDVersionHe": "v3", 
        "He_TTLLDVersionZ1": "v2", 
        "He_TTLLDCut": 2.3, 
        "OT_p0": -1.7189, 
        "OT_p1": 0.486, 
        "OT_p2": 13200, 
        "He_OT_dt": 1000, 
        "HePi_MaxDocaChi2": 30, 
        "HePi_MaxM_TL": 3100, 
        "HePi_MaxVtxChi2": 25, 
        "HePi_MinFDChi2": 50, 
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ]
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}

HeNOverflows = {
    "BUILDERTYPE": "HeNOverflowsConf", 
    "CONFIG": {
        "NumVeloPhiClusters": 3, 
        "NumVeloRClusters": 3, 
        "Trk_nOverflows": 3,
        "Prescale": 1.0, 
        "RequiredRawEvents": [
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "Muon", 
            "HC"
        ]
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "IFT" ]
}
