###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

################################################################################
##                          S T R I P P I N G  34r0p3                         ##
##                                                                            ##
##  Configuration for Charmless B decays (BnoC)                               ##
##  Contact person: Ganrong Wang   (ganrong.wang@cern.ch)                    ##
################################################################################

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

B2Etaph = {
    "BUILDERTYPE": "B2EtaphConf",
    "CONFIG": {
        "BDaug_maxDocaChi2": 15.0,
        "B_Dira": 0.9995,
        "B_IPCHI2": 20.0,
        "B_MassWindow": 750.0,
        "B_PTmin": 1500.0,
        "B_VtxChi2": 15.0,
        "B_eta_IPCHI2": 6.0,
        "Eta2g_B_MassWindow": 250.0,
        "GEC_MaxTracks": 250,
        "Postscale": 1.0,
        "Prescale": 1.0,
        "ProbNNCut": 0.1,
        "TightProbNNCut": 0.5,
        "Tight_Trk_GP": 0.3,
        "Tight_pK_IPCHI2": 30.0,
        "Tight_pK_PT": 1000.0,
        "Trk_Chi2": 4.0,
        "Trk_GP": 0.5,
        "Trk_PT": 300.0,
        "ba_ProbNNCut": 0.0,
        "etaGG_Prescale": 1.0,
        "eta_DOCA": 10.0,
        "eta_MassWindow": 200.0,
        "eta_PT": 2000,
        "eta_prime_DOCA": 10.0,
        "eta_prime_MassWindow": 150.0,
        "eta_prime_PT": 2000.0,
        "eta_prime_vtxChi2": 10.0,
        "eta_vtxChi2": 10.0,
        "etaforetap_MassWindow": 75.0,
        "gamma_PT": 200,
        "pK_IPCHI2": 20.0,
        "pK_PT": 500.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

B2Ksthh = {
    "BUILDERTYPE": "B2KsthhConf",
    "CONFIG": {
        "BDaug_MaxPT_IP": 0.05,
        "BDaug_MedPT_PT": 800.0,
        "BDaug_PTsum": 3000.0,
        "B_APTmin": 1000.0,
        "B_Dira": 0.999,
        "B_FDChi2": 100.0,
        "B_FDwrtPV": 1.0,
        "B_IPCHI2sum": 50.0,
        "B_IPCHI2wrtPV": 8.0,
        "B_Mhigh": 921.0,
        "B_Mlow": 1279.0,
        "B_PTmin": 2000.0,
        "B_VtxChi2ndof": 4.0,
        "ConeAngle10": 1.0,
        "ConeAngle15": 1.5,
        "ConeAngle17": 1.7,
        "GEC_MaxTracks": 250,
        "HLT1Dec": "Hlt1(Two)?TrackMVADecision",
        "HLT2Dec": "Hlt2Topo[234]BodyDecision",
        "Kstar_MassHi": 5000.0,
        "Kstar_MassLo": 0.0,
        "Postscale": 1.0,
        "Prescale": 1.0,
        "Trk_Chi2": 4.0,
        "Trk_GhostProb": 0.4
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

B2LambdaLambdabarK = {
    "BUILDERTYPE": "B2LambdaLambdabarKLinesConf",
    "CONFIG": {
        "B0Daug_DD_PTsum":
        2000.0,
        "B0Daug_DD_maxDocaChi2":
        16.0,
        "B0Daug_LD_PTsum":
        2000.0,
        "B0Daug_LD_maxDocaChi2":
        16.0,
        "B0Daug_LL_PTsum":
        2000.0,
        "B0Daug_LL_maxDocaChi2":
        16.0,
        "B0_APTmin":
        900.0,
        "B0_DD_FDChi2":
        25,
        "B0_DD_IPCHI2wrtPV":
        25.0,
        "B0_Dira":
        0.999,
        "B0_FDwrtPV":
        0.8,
        "B0_LD_FDChi2":
        25,
        "B0_LD_IPCHI2wrtPV":
        25.0,
        "B0_LL_FDChi2":
        25,
        "B0_LL_IPCHI2wrtPV":
        25.0,
        "B0_Mhigh":
        1921.0,
        "B0_Mlow":
        779.0,
        "B0_PTmin":
        1000.0,
        "B0_VtxChi2":
        36.0,
        "GEC_MaxTracks":
        250,
        "Lambda_DD_FDChi2":
        50.0,
        "Lambda_DD_MassWindow":
        20.0,
        "Lambda_DD_Pmin":
        5000.0,
        "Lambda_DD_VtxChi2":
        12.0,
        "Lambda_LL_MassWindow":
        20.0,
        "Lambda_LL_VtxChi2":
        12.0,
        "Postscale":
        1.0,
        "Prescale":
        1.0,
        "RelatedInfoTools":
        [{
            "ConeAngle": 1.7,
            "Location": "ConeVar17",
            "Type": "RelInfoConeVariables",
            "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
        },
         {
             "ConeAngle": 1.5,
             "Location": "ConeVar15",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         },
         {
             "ConeAngle": 1.0,
             "Location": "ConeVar10",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         },
         {
             "ConeAngle": 0.8,
             "Location": "ConeVar08",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         }, {
             "Location": "VtxIsolationVar",
             "Type": "RelInfoVertexIsolation"
         }],
        "Trk_Chi2":
        4.0,
        "Trk_GhostProb":
        0.4
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

B2l0phh = {
    "BUILDERTYPE": "B2L0phhConf",
    "CONFIG": {
        "MaxBMass":
        7000.0,
        "MaxBPVIPChi2":
        12.0,
        "MaxBVertChi2DOF":
        15.0,
        "MaxKChi2":
        3.0,
        "MaxKGHP":
        0.3,
        "MaxLmDeltaM":
        18.0,
        "MaxLmPiTrkChi2":
        4.0,
        "MaxLmPrtTrkChi2":
        4.0,
        "MaxLmVertChi2DOF":
        15.0,
        "MaxMass":
        6000.0,
        "MaxPiChi2":
        3.0,
        "MaxPiGHP":
        0.25,
        "MaxTrLong":
        250,
        "MaxpChi2":
        3.0,
        "MaxpGHP":
        0.3,
        "MinBMass":
        4500.0,
        "MinBPVDIRA":
        0.999,
        "MinBPVVDChi2":
        20.0,
        "MinBPt":
        1200.0,
        "MinKIPChi2DV":
        5.0,
        "MinKPIDPi":
        -2.0,
        "MinKPIDp":
        -2.0,
        "MinKPt":
        200.0,
        "MinLmIPChi2":
        0.0,
        "MinLmPVVDChi2":
        12.0,
        "MinLmPiIPChi2":
        4.0,
        "MinLmPiPt":
        100.0,
        "MinLmPrtIPChi2":
        4.0,
        "MinLmPrtPIDPi":
        -3.0,
        "MinLmPrtPt":
        300.0,
        "MinLmPt":
        400.0,
        "MinPiIPChi2DV":
        5.0,
        "MinPiPIDK":
        -2.0,
        "MinPiPIDp":
        -2.0,
        "MinPiPt":
        300.0,
        "MinpIPChi2DV":
        5.0,
        "MinpPIDK":
        -2.0,
        "MinpPIDPi":
        -2.0,
        "MinpPt":
        250.0,
        "RelatedInfoTools":
        [{
            "ConeAngle": 1.7,
            "Location": "ConeVar17",
            "Type": "RelInfoConeVariables",
            "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
        },
         {
             "ConeAngle": 1.5,
             "Location": "ConeVar15",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         },
         {
             "ConeAngle": 1.0,
             "Location": "ConeVar10",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         },
         {
             "ConeAngle": 0.8,
             "Location": "ConeVar08",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         }, {
             "Location": "VtxIsolationVar",
             "Type": "RelInfoVertexIsolation"
         }],
        "doK":
        True,
        "doPi":
        True,
        "nbody":
        4,
        "prescale":
        1.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Bs2PhiKst = {
    "BUILDERTYPE": "StrippingBs2PhiKstConf",
    "CONFIG": {
        "BDIRA": 0.99,
        "BDOCA": 0.3,
        "BMassWin": 500.0,
        "BVCHI2": 15.0,
        "KaonIPCHI2": 9.0,
        "KaonLoosePIDK": -50.0,
        "KaonPIDK": 0.0,
        "KaonPT": 500.0,
        "KstarMassWin": 150.0,
        "KstarPT": 900.0,
        "KstarVCHI2": 9.0,
        "PhiMassWin": 25.0,
        "PhiPT": 900.0,
        "PhiVCHI2": 9.0,
        "PionIPCHI2": 9.0,
        "PionLoosePIDK": 50.0,
        "PionPIDK": 10.0,
        "PionPT": 500.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Hb2ppX = {
    "BUILDERTYPE": "Hb2ppXLines",
    "CONFIG": {
        "PPbarDIRA": 0.994,
        "PPbarFDCHI2HIGH": 150.0,
        "PPbarPT": 1500.0,
        "PPbarVCHI2DOF": 4.0,
        "PrescaleLb2Charged2Body": 1,
        "ProtonMINIPCHI2": 16.0,
        "ProtonP": 15000.0,
        "ProtonPIDK": 12.0,
        "ProtonPIDp": 12.0,
        "ProtonPT": 1000.0,
        "ProtonTRCHI2": 4.0,
        "TRGHOSTPROB": 0.015
    },
    "STREAMS": ["BhadronCompleteEvent"],
    "WGs": ["BnoC"]
}

Xibm2V03h = {
    "BUILDERTYPE": "StrippingXibm2V03h",
    'CONFIG': {
        'TRCHI2DOFMax':
        4.0,
        'TrGhostProbMax':
        0.3,  # same for all particles
        'MINIPCHI2':
        8.0,  # adimensiional, orig. 9 
        'pion_p_min':
        2.0 * GeV,
        'pion_pt_min':
        200 * MeV,
        'pion_pidk_max':
        10.0,
        'pion_pidpi_pidk_max':
        0.0,
        'kaon_p_min':
        2.0 * GeV,
        'kaon_pt_min':
        200 * MeV,
        'kaon_pidk_min':
        10.0,
        'kaon_pidk_pidpi_max':
        5.0,
        'proton_p_min':
        2.0 * GeV,
        'proton_pt_min':
        200 * MeV,
        'proton_pidp_pidpi_max':
        5.0,
        'proton_pidp_pidk_max':
        0.0,
        'ProbNNp':
        0.2,
        'ProbNNk':
        0.2,
        'ProbNNpi':
        0.2,
        'ProbNNpiMax':
        0.9,
        'ProbNNkMax':
        0.9,
        'lambda_ll_p_min':
        2500.0 * MeV,
        'lambda_ll_pt_min':
        100.0 * MeV,
        'lambda_ll_mass_window':
        25.0 * MeV,
        'lambda_ll_fdchi2_min':
        30.0,
        'lambda_dd_p_min':
        2000.0 * MeV,
        'lambda_dd_pt_min':
        100.0 * MeV,
        'lambda_dd_mass_window':
        20.0 * MeV,
        'lambda_dd_fdchi2_min':
        50.0,  ## unitless
        'LambdaLDPMin':
        3000.0 * MeV,
        'LambdaLDPTMin':
        200.0 * MeV,
        'LambdaLDCutMass':
        20.0 * MeV,
        'lambda_ld_fdchi2_min':
        100.0,  ## unitless
        'lambda_trackchi2_min':
        4.0,  ## unitless
        'lambda_vertexchi2_min':
        5.0,  ## max chi2/ndf for Lambda0 vertex
        'lambda_ll_dira_min':
        0.99,  ## unitless
        'lambda_dd_dira_min':
        0.9,  ## unitless
        'lambda_ll_vz_min':
        -100.0 * mm,
        'lambda_ll_vz_max':
        400.0 * mm,
        'lambda_dd_vz_min':
        400.0 * mm,
        'lambda_dd_vz_max':
        2275.0 * mm,
        'lambda_pp_p_min':
        0.0 * MeV,
        'lambda_pim_p_min':
        0.0 * MeV,
        'Xibm_vertexchi2_max':
        25.0,
        'Xibm_BPVVDCHI2_Min':
        25.0,
        'Xibm_BPVDIRA_Min':
        0.999,
        'Xibm_AM_Min':
        5300.0 * MeV,
        'Xibm_AM_Max':
        6350.0 * MeV,
        'Xibm_ADOCAMAXCHI2_max':
        25,
        'Xibm_APT_min':
        800 * MeV,
        'RelatedInfoTools':
        [{
            "Type": "RelInfoConeVariables",
            "ConeAngle": 1.7,
            "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
            "Location": 'ConeVar17'
        },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 1.5,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar15'
         },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 1.0,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar10'
         },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 0.8,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar08'
         }, {
             "Type": "RelInfoVertexIsolation",
             "Location": "VtxIsolationVar"
         }]
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Xibm2hyperon_hh_4h = {
    "BUILDERTYPE": "StrippingXibm2hyperon_hh_4h",
    'CONFIG': {
        "RequiredRawEvents": ["Velo"],
        'TRCHI2DOFMax':
        4.0,
        'TrGhostProbMax':
        0.3,  # same for all particles
        'MINIPCHI2':
        8.0,  # adimensiional, orig. 9 
        'MINIPCHI2_from_hyperon':
        4.0,
        'pion_p_min':
        2.0 * GeV,
        'pion_pt_min':
        200 * MeV,
        'pion_pidk_max':
        10.0,
        'pion_pidpi_pidk_max':
        0.0,
        'kaon_p_min':
        2.0 * GeV,
        'kaon_pt_min':
        200 * MeV,
        'kaon_down_pt_min':
        0 * MeV,
        'kaon_pidk_min':
        10.0,
        'kaon_pidk_pidpi_max':
        5.0,
        'proton_p_min':
        2.0 * GeV,
        'proton_pt_min':
        200 * MeV,
        'proton_pidp_pidpi_max':
        5.0,
        'proton_pidp_pidk_max':
        0.0,
        'ProbNNp':
        0.2,
        'ProbNNk':
        0.2,
        'ProbNNpi':
        0.2,
        'ProbNNpiMax':
        0.9,
        'ProbNNkMax':
        0.9,
        'lambda_ll_p_min':
        2000.0 * MeV,
        'lambda_ll_pt_min':
        100 * MeV,
        'lambda_ll_mass_window':
        35.0 * MeV,
        'lambda_ll_fdchi2_min':
        30.0,
        'lambda_dd_p_min':
        2000.0 * MeV,
        'lambda_dd_pt_min':
        100.0 * MeV,
        'lambda_dd_mass_window':
        40.0 * MeV,
        'lambda_dd_fdchi2_min':
        50.0,  ## unitless
        'LambdaLDPMin':
        3000.0 * MeV,
        'LambdaLDPTMin':
        200.0 * MeV,
        'LambdaLDCutMass':
        20.0 * MeV,
        'lambda_ld_fdchi2_min':
        100.0,  ## unitless
        'lambda_trackchi2_min':
        4.0,  ## unitless
        'lambda_vertexchi2_min':
        5.0,  ## max chi2/ndf for Lambda0 vertex
        'lambda_ll_dira_min':
        0.99,  ## unitless
        'lambda_dd_dira_min':
        0.9,  ## unitless
        'lambda_ll_vz_min':
        -100.0 * mm,
        'lambda_ll_vz_max':
        400.0 * mm,
        'lambda_dd_vz_min':
        400.0 * mm,
        'lambda_dd_vz_max':
        2275.0 * mm,
        'lambda_pp_pt_min':
        0.0 * MeV,
        'lambda_pim_pt_min':
        0.0 * MeV,
        'hyperon_mass_window_comb':
        80 * MeV,
        'hyperon_lll_mass_window':
        60 * MeV,
        'hyperon_ddl_mass_window':
        65 * MeV,
        'hyperon_ddd_mass_window':
        65 * MeV,
        'hyperon_ADOCACHI2CUT_max':
        25,
        'hyperon_VFASPF_max':
        25,
        'hyperon_p_min':
        1000 * MeV,
        'hyperon_pt_min':
        0 * MeV,
        'hyperon_VFASPFchi2ndof_max':
        12.0,
        'hyperon_bpvltime_min':
        '0.0005 * ps',
        'hyperon_long_vdchi2':
        10,
        'hyperon_down_vdchi2':
        20,
        'Xibm_vertexchi2_max':
        25.0,
        'Xibm_BPVVDCHI2_Min':
        25.0,
        'Xibm_BPVDIRA_Min':
        0.995,
        'Xibm_AM_Min':
        5300.0 * MeV,
        'Xibm_AM_Max':
        6250.0 * MeV,
        'Xibm_ADOCAMAXCHI2_max':
        25,
        'Xibm_APT_2b_min':
        500 * MeV,
        'Xibm_APT_3b_min':
        700 * MeV,
        'Xibm_APT_5b_min':
        1000 * MeV,
        'RelatedInfoTools':
        [{
            "Type": "RelInfoConeVariables",
            "ConeAngle": 1.7,
            "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
            "Location": 'ConeVar17'
        },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 1.5,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar15'
         },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 1.0,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar10'
         },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 0.8,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar08'
         }, {
             "Type": "RelInfoVertexIsolation",
             "Location": "VtxIsolationVar"
         }]
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}
