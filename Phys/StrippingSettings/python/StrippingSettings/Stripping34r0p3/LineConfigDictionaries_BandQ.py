###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

################################################################################
##                          S T R I P P I N G  3 4 r 0 p 3                    ##
##                                                                            ##
##  Configuration for B&Q WG                                                  ##
##  Contact person: Shuqi Sheng (shuqi.sheng@cern.ch)                         ##
################################################################################


#################################################################################
#                                                                               #
#                                                                               #
# Module for selecting                                                          #
# B0 -> Lc Lcbar Ks channel                                                     #
# 2 lines are added                                                             #
#                                                                               #
# Author: Shuqi Sheng                                                           #
#                                                                               #
#                                                                               #
#################################################################################

B02LcLcKs = {
    "BUILDERTYPE": "B02LcLcKsConf", 
    "CONFIG": {
        "B0ComCuts": "AALL", 
        "B0ComN3Cuts": "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)", 
        "B0MomCuts": "(VFASPF(VCHI2/VDOF)<10.) & (BPVLTIME()>0.2*ps) & (BPVIPCHI2()<25) & (BPVDIRA>0.)", 
        "KaonCuts": "(PT>100*MeV) & (PROBNNk>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0) & (TRCHI2DOF<3)", 
        "KsCuts": "(BPVDLS>5.)", 
        "LcComCuts": "(ADOCA(1,2)<0.5*mm)", 
        "LcComN3Cuts": "(ASUM(PT)>1000*MeV) & (ADAMASS('Lambda_c+')<110*MeV) & (AHASCHILD((ISBASIC & HASTRACK & (PT > 500*MeV) & (P > 5000*MeV)))) & (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)", 
        "LcMomCuts": "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)", 
        "PionCuts": "(PT>100*MeV) & (PROBNNpi>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0)& (TRCHI2DOF<3)", 
        "Prescale": 1.0, 
        "ProtonCuts": "(PT>100*MeV) & (PROBNNp>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0) & (TRCHI2DOF<3)"
    },   
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "BandQ" ]
}

#################################################################################
#                                                                               #
#                                                                               #
# Module for selecting                                                          #
# Tcc->D0 D0 pi+ channel where adding D0->K3pi mode and norm channels           #
# 21 lines are added                                                            #
#                                                                               #
# Author: Vanya BELYAEV, Ivan Polyakov                                          #
#                                                                               #
#                                                                               #
#################################################################################

PromptCharm = {
    "BUILDERTYPE": "StrippingPromptCharmConf", 
    "CONFIG": {
        "Charm&WPrescale": 1.0, 
        "CheckPV": True, 
        "Chi&CharmPrescale": 1.0, 
        "Chi&WPrescale": 1.0, 
        "D*CPPrescale": 1.0, 
        "D*Prescale": 1.0, 
        "D+Prescale": 0.4, 
        "D02KKPrescale": 1.0, 
        "D02pipiPrescale": 1.0, 
        "D0Prescale": 0.3, 
        "DiCharmPrescale": 1.0, 
        "DiMu&CharmPrescale": 1.0, 
        "DiMuon&WPrescale": 1.0, 
        "DiMuonAndDiCharmPrescale": 1.0, 
        "DoubleDiMuonAndCharmPrescale": 1.0, 
        "DoubleDiMuonPrescale": 1.0, 
        "DsPrescale": 0.7, 
        "GammaChi": " ( PT > 400 * MeV ) & ( CL > 0.05 ) ", 
        "KaonCut": """
                   ( PT          > 250 * MeV ) &
                   ( CLONEDIST   > 5000      ) &
                   ( TRGHOSTPROB < 0.5       ) &
                   in_range ( 2          , ETA , 4.9       ) &
                   in_range ( 3.2 * GeV  , P   , 150 * GeV ) &
                   HASRICH                     &
                   ( MIPCHI2DV()  > 9        )
                   """, 
        "KaonPIDCut": " PROBNNk  > 0.1 ", 
        "LambdaC*Prescale": 1.0, 
        "LambdaCLooseChi2IPPrescale": 1.0, 
        "LambdaCPrescale": 1.0, 
        "LambdaCpKKPrescale": 1.0, 
        "Monitor": False, 
        "MuonCut": """
                   ISMUON &
                   in_range ( 2 , ETA , 4.9     ) &
                   ( PT            >  550 * MeV ) &
                   ( PIDmu - PIDpi >    0       ) &
                   ( CLONEDIST     > 5000       )     
                   """, 
        "NOPIDHADRONS": False, 
        "OmegaC*2XiCPiKPrescale": 1.0, 
        "OmegaC*2XiCprimeKPrescale": 1.0, 
        "OmegaC*Prescale": 1.0,
        "OmegaC0Prescale": 1.0,
        "OmegaCC2XiCKKPrescale": 1.0,
        "OmegaCC2XiCKpiPrescale": 1.0,
        "OmegaCCPrescale": 1.0,
        "PhotonCLCut": 0.05,
        "PionCut": """
                   ( PT          > 250 * MeV ) &
                   ( CLONEDIST   > 5000      ) &
                   ( TRGHOSTPROB < 0.5       ) &
                   in_range ( 2          , ETA , 4.9       ) &
                   in_range ( 3.2 * GeV  , P   , 150 * GeV ) &
                   HASRICH                     &
                   ( MIPCHI2DV()  > 9        )
                   """,
        "PionPIDCut": " PROBNNpi > 0.1 ",
        "Preambulo": [
            "pipi   = DECTREE ('[D0]cc -> pi- pi+   ') ",
            "kk     = DECTREE ('[D0]cc -> K-  K+    ') ",
            "kpi    = DECTREE ('[D0    -> K-  pi+]CC') ",
            "ak2    = 2 == ANUM( 'K+' == ABSID ) ",
            "chi2vx = VFASPF(VCHI2) ",
            "from GaudiKernel.PhysicalConstants import c_light",
            "ctau     = BPVLTIME (   9 ) * c_light ",
            "ctau_9   = BPVLTIME (   9 ) * c_light ",
            "ctau_16  = BPVLTIME (  16 ) * c_light ",
            "ctau_25  = BPVLTIME (  25 ) * c_light ",
            "ctau_100 = BPVLTIME ( 100 ) * c_light ",
            "ctau_400 = BPVLTIME ( 400 ) * c_light ",
            "ctau_no  = BPVLTIME (     ) * c_light ",
            "psi           =   ADAMASS ('J/psi(1S)') < 150 * MeV",
            "psi_prime     =   ADAMASS (  'psi(2S)') < 150 * MeV"
        ],
        "PromptKaonCut": """
                         ( CLONEDIST   > 5000         ) &
                         ( TRGHOSTPROB < 0.5          ) &
                         in_range ( 2          , ETA , 4.9       ) &
                         in_range ( 3.2 * GeV  , P   , 150 * GeV ) &
                         HASRICH                     
                         """,
        "ProtonCut": """
                     ( PT           > 250 * MeV ) &
                     ( CLONEDIST    > 5000      ) &
                     ( TRGHOSTPROB  < 0.5       ) &
                     in_range ( 2         , ETA , 4.9       ) &
                     in_range ( 10 * GeV  , P   , 150 * GeV ) &
                     HASRICH                      &
                     ( MIPCHI2DV()  > 9         )
                     """,
        "ProtonPIDCut": " PROBNNp  > 0.1 ",
        "QvalueLcPiK": 700.0,
        "QvalueOmegaCC": 4500.0,
        "QvalueXiCK": 600.0,
        "QvalueXiCPiK": 600.0,
        "QvalueXiCprime": 250.0,
        "QvalueXiCprimeK": 600.0,
        "QvalueXiCstar": 150.0,
        "SigmaCPrescale": 1.0,
        "TrackCut": """
                    ( CLONEDIST   > 5000      ) &
                    ( TRGHOSTPROB < 0.5       ) &
                    in_range ( 2  , ETA , 4.9 ) &
                    HASRICH
                    """,
        "TriCharmPrescale": 1.0,
        "TripleDiMuPrescale": 1.0,
        "WCuts": " ( 'mu+'== ABSID ) & ( PT > 15 * GeV )",
        "XiC**2LcPiKPrescale": 1.0,
        "XiC*Prescale": 1.0,
        "XiC0Prescale": 1.0,
        "XiCprimePrescale": 1.0,
        "Xic02LcPiPrescale": 1.0,
        "pT(D+)": 1000.0,
        "pT(D0)": 1000.0,
        "pT(D0->HH)": 1000.0,
        "pT(Ds+)": 1000.0,
        "pT(Lc+)": 1000.0,
        "pT(Omgcc)": 1000.0,
        "pT(Xic0)": 1000.0
    },
    "STREAMS": {
        "Charm": [
            "StrippingD02KpiForPromptCharm",
            "StrippingD02K3piForPromptCharm",
            "StrippingDstarForPromptCharm",
            "StrippingDForPromptCharm",
            "StrippingDsForPromptCharm",
            "StrippingLambdaCForPromptCharm",
            "StrippingLambdaCLooseChi2IPForPromptCharm",
            #"StrippingXiC0ForPromptCharm",
            "StrippingLambdaC2pKKForPromptCharm",
            "StrippingSigmaCForPromptCharm",
            "StrippingLambdaCstarForPromptCharm",
            "StrippingOmegaCstarForPromptCharm",
            "StrippingXiCprimeForPromptCharm",
            #"StrippingXiCstarForPromptCharm",
            #"StrippingOmegaCstar2XiCPiKForPromptCharm",
            "StrippingOmegaCstar2XiCprimeKForPromptCharm",
            "StrippingXiCstarstar2LambdaCPiKForPromptCharm",
            "StrippingOmegaCC2XiCKpiForPromptCharm",
            "StrippingOmegaCC2XiCKKForPromptCharm",
            "StrippingXic02LcPiForPromptCharm",
            #"StrippingChiAndCharmForPromptCharm",
            #"StrippingCharmAndWForPromptCharm",
            #"StrippingDiMuonAndCharmForPromptCharm",
            "StrippingDiCharmForPromptCharm",
            #"StrippingTriCharmForPromptCharm",
            #"StrippingDiMuonAndDiCharmForPromptCharm",
            #"StrippingDoubleDiMuonAndCharmForPromptCharm",
            "StrippingD02KKForPromptCharm",
            "StrippingD02pipiForPromptCharm",
            "StrippingDstarCPForPromptCharm"
        ],
        #"CharmCompleteEvent": [ "StrippingOmegaC0ForPromptCharm" ],
        #"Leptonic": [
        #    "StrippingDiMuonAndWForPromptCharm",
        #    "StrippingChiAndWForPromptCharm",
        #    "StrippingDoubleDiMuonForPromptCharm",
        #    "StrippingTripleDiMuonForPromptCharm"
        #]
    },
    "WGs": [ "BandQ" ]
}


#################################################################################
#                                                                               #
#                                                                               #
# Module for selecting                                                          #
# b-> p Ds+ [Xib0**,Lambdab*]                                                   #
# b-> p D0b [Xib-***]                                                           #
# b-> p D0 [To pick up * control mode]                                          #
# b-> p J/psi [Xib-***]                                                         #
# b-> p K+ [Xib0**,lambdab*]                                                    #
# b-> lambdac(2595) [Xib-***], this is available in StrippingLambda2DM, from QEE#
# * lambdab-> p D0 pi 10%                                                       #
# ** Xib0 -> pD0K 30%, times hadr fraction                                      #
# *** Xib- -> pKK, 20%, times hadr fraction                                     #
# 7 lines are added                                                             #
#                                                                               #
# Author: Xabier Cid Vidal                                                      #
#                                                                               #
#                                                                               #
#################################################################################

Sexaquark = {
    "BUILDERTYPE": "SexaquarkConf",
    "CONFIG": {
        "Common": {
            "D_DOCA": 0.3,
            "D_FDCHI2": 50,
            "D_M_Max": 2000.0,
            "D_M_Min": 1830.0,
            "D_PT": 2500.0,
            "D_VCHI2": 6,
            "Jpsi_IPchi2": 5,
            "Jpsi_PT": 100.0,
            "KfromD_PT": 350.0,
            "KfromD_ProbNNghost": 0.1,
            "KfromD_ProbNNk": 0.8,
            "bBaryonToDandp_DOCA": 0.2,
            "bBaryonToDandp_DOCACHI2": 7,
            "bBaryonToDandp_FDCHI2": 30,
            "bBaryonToDandp_IPCHI2": 0,
            "bBaryonToDandp_ISO": 0.0,
            "bBaryonToDandp_M_Max": 5700.0,
            "bBaryonToDandp_M_Min": 0.0,
            "bBaryonToDandp_PT": 1000.0,
            "bBaryonToDandp_VCHI2": 8,
            "bBaryonToJpsiandp_DOCA": 1.0,
            "bBaryonToJpsiandp_DOCACHI2": 10,
            "bBaryonToJpsiandp_FDCHI2": 5,
            "bBaryonToJpsiandp_IPCHI2": 0,
            "bBaryonToJpsiandp_ISO": 0.0,
            "bBaryonToJpsiandp_M_Max": 5700.0,
            "bBaryonToJpsiandp_M_Min": 0.0,
            "bBaryonToJpsiandp_PT": 600.0,
            "bBaryonToJpsiandp_VCHI2": 10,
            "checkPV": False,
            "p_IPchi2": 20,
            "p_PT": 600.0,
            "p_ProbNNghost": 0.2,
            "p_ProbNNp": 0.8,
            "pifromDPT": 350.0,
            "pifromDProbNNghost": 0.1,
            "pifromDProbNNpi": 0.8
        },
        "StrippingSQbBaryonToDpAndpLine": {
            "Postscale": 1.0,
            "Prescale": 1.0
        },
        "StrippingSQbBaryonToDsAndpLine": {
            "Postscale": 1.0,
            "Prescale": 1.0
        },
        "StrippingSQbBaryonToDzAndpLine": {
            "Postscale": 1.0,
            "Prescale": 1.0
        },
        "StrippingSQbBaryonToDzbAndpLine": {
            "Postscale": 1.0,
            "Prescale": 1.0
        },
        "StrippingSQbBaryonToJpsiAndpLine": {
            "Postscale": 1.0,
            "Prescale": 1.0
        },
        "StrippingSQbBaryonToKAndpLine": {
            "K_IPchi2": 30,
            "K_PT": 1500.0,
            "K_ProbNNghost": 0.2,
            "K_ProbNNk": 0.8,
            "Postscale": 1.0,
            "Postscale_control": 1.0,
            "Prescale": 1.0,
            "Prescale_control": 0.15,
            "bBaryonToKandp_CORRM": 2800.0,
            "bBaryonToKandp_DOCA": 0.2,
            "bBaryonToKandp_DOCACHI2": 3,
            "bBaryonToKandp_FDCHI2": 50,
            "bBaryonToKandp_IPCHI2": 5,
            "bBaryonToKandp_ISO": 0.7,
            "bBaryonToKandp_M_Max": 5700.0,
            "bBaryonToKandp_M_Min": 1000.0,
            "bBaryonToKandp_PT": 4000.0,
            "bBaryonToKandp_VCHI2": 5,
            "p_IPchi2": 30,
            "p_PT": 1500.0,
            "p_ProbNNghost": 0.2,
            "p_ProbNNp": 0.8
        }
    },
    "STREAMS": [ "BhadronCompleteEvent" ],
    "WGs": [ "BandQ" ]
}

#################################################################################
#                                                                               #
#                                                                               #
# Module for selecting Hc->eta_c mu mu                                          #
# 4 lines are added                                                             #
#                                                                               #
# Author: Roberta Cardinale, Youhua Yang                                        #
#                                                                               #
#                                                                               #
#################################################################################

Hc2EtacMuMu = { 
    "BUILDERTYPE": "Hc2EtacMuMuConf", 
    "CONFIG": {
        "EtacComCuts": "(in_range(2.85*GeV, AM, 3.25*GeV))", 
        "EtacComN4Cuts": "(in_range(2.7*GeV, AM, 3.4*GeV))", 
        "EtacMomN4Cuts": "(VFASPF(VCHI2/VDOF) < 9.) & (in_range(2.8*GeV, MM, 3.3*GeV))", 
        "HcComCuts": "(ADAMASS('h_c(1P)') < 500 *MeV)", 
        "HcMomCuts": """
                     (VFASPF(VCHI2/VDOF) < 10.)
                     """, 
        "KKComAMCuts": "(AM>987.354*MeV) ", 
        "KaonCuts": "(PROBNNk > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)", 
        "KsCuts": "(ADMASS('KS0') < 30.*MeV) & (BPVDLS>5) & (PT > 300*MeV) & (MAXTREE('pi-'==ABSID, PROBNNpi) > 0.1) & (MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.4) & (MAXTREE('pi-'==ABSID, TRCHI2DOF) < 5) & (MAXTREE('pi-'==ABSID, MIPCHI2DV(PRIMARY)) > 4)", 
        "MuonCuts": "(PROBNNmu > 0.1) & (PT < 2*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)", 
        "PhiComN4Cuts": "", 
        "PhiCuts": "(VFASPF(VCHI2/VDOF) < 9.) & (PT > 800 * MeV) & (ADMASS('phi(1020)') < 30 * MeV)", 
        "PionCuts": "(PROBNNk > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)", 
        "Prescale": 1.0, 
        "ProtonCuts": "(PROBNNp > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)", 
        "RelatedInfoTools": [
            {
                "Location": "RelInfoVertexIsolation", 
                "Type": "RelInfoVertexIsolation"
            },
            {
                "ConeAngle": 1.0, 
                "Location": "RelInfoConeVariables_1.0", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            },
            {
                "ConeAngle": 1.5, 
                "Location": "RelInfoConeVariables_1.5", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            },
            {
                "ConeAngle": 2.0,
                "Location": "RelInfoConeVariables_2.0",
                "Type": "RelInfoConeVariables",
                "Variables": [
                    "CONEANGLE",
                    "CONEMULT",
                    "CONEPTASYM"
                ]
            }
        ],
        "pipiComAMCuts": "(AM>279.14*MeV) ",
        "ppbarComAMCuts": "(AM>1876.544*MeV) "
    },
    "STREAMS": [ "Bhadron" ],
    "WGs": [ "BandQ" ]
}

#################################################################################
#                                                                               #
#                                                                               #
# Module for weak decays of low-lying excited states                            #
# 14 lines are added                                                            #
#                                                                               #
# Author: Vitalii Lisovskyi                                                     #
#                                                                               #
#                                                                               #
#################################################################################

WeakExcitedDecays = {
    "BUILDERTYPE": "WeakExcitedDecaysConf",
    "CONFIG": {
        "BHadronMassWindow": 500.0,
        "BHadronMassWindow_Loose": 900.0,
        "CORRM_MIN_LOOSE": 3200.0,
        "DLSForLongLived": 5.0,
        "DiHadronADOCA": 0.7,
        "DiHadronMass": 3000.0,
        "FromB_BPVVDCHI2": 60,
        "FromB_BPVVDCHI2_Loose": 25,
        "FromB_BPVVDCHI2_Tight": 100,
        "FromB_DIRA": 0.9,
        "FromB_DIRA_Tight": 0.999,
        "FromB_PT": 1600,
        "FromB_PT_Loose": 1200,
        "FromB_PT_Tight": 2000,
        "HyperonPT": 500.0,
        "JpsiMassWindow": 100.0,
        "KaonPIDK": -5.0,
        "KaonProbNN": 0.15,
        "KaonProbNN_Loose": 0.1,
        "MaxFromB_VertChi2DOF": 10,
        "MaxFromB_VertChi2DOF_Loose": 25,
        "MaxFromB_VertChi2DOF_Tight": 6,
        "OmegaMassWindow": 30.0,
        "OmegacMassMax": 2900,
        "OmegacMassMaxComb": 2940,
        "OmegacMassMin": 2560,
        "OmegacMassMinComb": 2520,
        "PionProbNN": 0.1,
        "ProtProbNN": 0.2,
        "ProtProbNNK": 0.85,
        "ProtProbNN_Loose": 0.1,
        "ProtonPT": 750.0,
        "TRCHI2DOF": 4.0,
        "TrackMinIPCHI2": 4,
        "TrackMinIPCHI2_Tight": 6,
        "XiMassWindow": 30.0,
        "XicMassMax": 2820,
        "XicMassMaxComb": 2870,
        "XicMassMax_Tight": 2700,
        "XicMassMin": 2350,
        "XicMassMinComb": 2320
    },
    "STREAMS": {
        "Bhadron": [
            "StrippingWeakExcitedDecaysOmegacstar2pKKpi_inclfromBLine",
            "StrippingWeakExcitedDecaysOmegacstar2Omegapi_inclfromBLine",
            "StrippingWeakExcitedDecaysXicstarz2pKKpi_inclfromBLine",
            "StrippingWeakExcitedDecaysXicstarp2pKpi_inclfromBLine",
            "StrippingWeakExcitedDecaysXicstarz2XiPi_inclfromBLine",
            "StrippingWeakExcitedDecaysOmegab2OmegacstarPi_pKKpiLine",
            "StrippingWeakExcitedDecaysOmegab2OmegacstarPi_OmpiLine",
            "StrippingWeakExcitedDecaysXib2XicstarPi_pKpiLine",
            "StrippingWeakExcitedDecaysOmegab2XicstarKPi_pKpiLine",
            "StrippingWeakExcitedDecaysXibm2XicstarzPi_pKKpiLine",
            "StrippingWeakExcitedDecaysOmegab2OmegacstarMu_pKKpiLine",
            "StrippingWeakExcitedDecaysOmegab2OmegacstarE_pKKpiLine"
        ],
        "Dimuon": [
            "StrippingWeakExcitedDecaysOmegabstar2JpsiOmegaLine",
            "StrippingWeakExcitedDecaysBcstar2JpsiDsLine"
        ]
    },
    "WGs": [ "BandQ" ]
}


#################################################################################
#                                                                               #
#                                                                               #
# Module for selecting                                                          #
# Lb->Lc(PiPi)MuNu, Lc->Lambda(3)Pi                                             #
# with Lambda reconstructed as LL, DD and TT tracks.                            #
# 10 lines are added                                                            #
#                                                                               #
# Author: Mengzhen Wang, Nicola Neri, Tianze Rong                               #
#                                                                               #
#                                                                               #
#################################################################################

Lb2LcSLForEDM = {
    "BUILDERTYPE": "Lb2LcSLForEDMConf",
    "CONFIG": {
        "IsoMax": "4.0 * GeV",
        "LambdaDDCuts": "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>1*GeV) & (MAXTREE('p+'==ABSID,P)>9*GeV) & (MAXTREE('p+'==ABSID,PROBNNp)>0.15) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.1)",
        "LambdaLLCuts": "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>0.8*GeV) & (BPVVDZ>10*mm) & (BPVVDCHI2>32) & (DOCA(1,2)<0.5*mm) & (DOCACHI2(1,2)<16) & (MAXTREE('p+'==ABSID,P)>7.5*GeV) & (MAXTREE('p+'==ABSID,PROBNNp)>0.15) & (MAXTREE('p+'==ABSID,MIPCHI2DV(PRIMARY))>9) & (MAXTREE('pi+'==ABSID,MIPCHI2DV(PRIMARY))>9) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.1)",
        "LbCombCuts_DD": "( AM < 6.0*GeV )",
        "LbCombCuts_LL": "( AM < 6.0*GeV )",
        "LbMotherCuts_DD": "( M < 5.9*GeV ) & (VFASPF(VCHI2/VDOF)<10) ",
        "LbMotherCuts_LL": "( M < 5.9*GeV ) & (VFASPF(VCHI2/VDOF)<10)",
        "LbMuonCuts": "(PROBNNmu > 0.3) & (PT > 500*MeV) & (TRGHOSTPROB<0.3) & (TRCHI2DOF < 5) & (MIPCHI2DV() > 16)",
        "LbMuonCuts_Tight": "(PROBNNmu > 0.5) & (PT > 1.8*GeV) & (TRGHOSTPROB<0.3) & (TRCHI2DOF < 5) & (MIPCHI2DV() > 45)",
        "LbPionCuts": "(PROBNNpi > 0.4) & (PT > 250*MeV) & (TRGHOSTPROB<0.3) & (TRCHI2DOF < 5) & (MIPCHI2DV() > 16) & (P > 2.6*GeV)",
        "LcCombCuts_DD": "(ADAMASS('Lambda_c+') < 110.0)",
        "LcCombCuts_LL": "(ADAMASS('Lambda_c+') < 70.0)",
        "LcMotherCuts_DD": "(ADMASS('Lambda_c+') < 100.0) & (VFASPF(VCHI2/VDOF)<10)",
        "LcMotherCuts_LL": "(ADMASS('Lambda_c+') < 60.0) & (VFASPF(VCHI2/VDOF)<10)",
        "LcPionCuts": "(PROBNNpi > 0.4) & (PT > 150*MeV) & (TRGHOSTPROB<0.3) & (TRCHI2DOF < 5) & (MIPCHI2DV() > 16) & (P > 2.6*GeV)",
        "LcPionCuts_Tight": "(PROBNNpi > 0.5) & (PT > 800*MeV) & (TRGHOSTPROB<0.3) & (TRCHI2DOF < 5) & (MIPCHI2DV() > 30) & (P > 2.6*GeV)",
        "Mu3PiMomCuts": "( M < 4.7*GeV ) & (VFASPF(VCHI2/VDOF) > 16.)",
        "PiPiMu3PiMomCuts": "( M < 5.0*GeV ) & (VFASPF(VCHI2/VDOF) > 16.)",
        "PiPiMuCom12AMCuts": "(AM<3.45*GeV)",
        "PiPiMuCom12AMCuts_AboveRho": "(AM>1.0*GeV) & (AM<3.45*GeV)",
        "PiPiMuCom12AMCuts_RhoRegion": "(AM<1.0*GeV)",
        "PiPiMuComAMCuts": "(AM<3.45*GeV)",
        "PiPiMuComN3Cuts": """
                          (in_range(0.3*GeV, AM, 3.45*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3) ) > 2.0 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3))>60)
                          """,
        "PiPiMuComN3Cuts_Tight": """
                          (in_range(0.3*GeV, AM, 3.45*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3) ) > 4. *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3))>80)
                          """,
        "PiPiMuMomN3Cuts": """
                           (VFASPF(VCHI2/VDOF) < 9.) 
                           & (in_range(0.35*GeV, MM, 3.4*GeV)) 
                           & (MIPCHI2DV(PRIMARY) > 9.) 
                           & (BPVVDCHI2>10) 
                           & (BPVDIRA>0.)
                           """,
        "PiPiMuMomN3Cuts_Tight": """
                           (VFASPF(VCHI2/VDOF) < 4.) 
                           & (in_range(0.35*GeV, MM, 3.4*GeV)) 
                           & (MIPCHI2DV(PRIMARY) > 36.) 
                           & (BPVVDCHI2>160) 
                           & (BPVDIRA>0.)
                           """,
        "PiPiMuPiMomCuts": "( M < 4.7*GeV ) & (VFASPF(VCHI2/VDOF) > 16.)",
        "PiPiPiComAMCuts": "(AM<1.2*GeV)",
        "PiPiPiComN3Cuts": """
                          (in_range(0.3*GeV, AM, 1.2*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3) ) > 2.0 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3))>60)
                          """,
        "PiPiPiComN3Cuts_Tight": """
                          (in_range(0.3*GeV, AM, 1.2*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3) ) > 4.5 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3))>100)
                          """,
        "PiPiPiMomN3Cuts": """
                           (VFASPF(VCHI2/VDOF) < 9.) 
                           & (in_range(0.35*GeV, MM, 1.19*GeV)) 
                           & (MIPCHI2DV(PRIMARY) > 9.) 
                           & (BPVVDCHI2>10) 
                           & (BPVDIRA>0.)
                           """,
        "PiPiPiMomN3Cuts_Tight": """
                           (VFASPF(VCHI2/VDOF) < 4.) 
                           & (in_range(0.35*GeV, MM, 1.19*GeV)) 
                           & (MIPCHI2DV(PRIMARY) > 49.) 
                           & (BPVVDCHI2>180) 
                           & (BPVDIRA>0.)
                           """
    },
    "STREAMS": [ "BhadronCompleteEvent" ],
    "WGs": [ "BandQ" ]
}


#################################################################################
#                                                                               #
#                                                                               #
# Module for selecting                                                          #
# B hadrons to charm baryons                                                    #
# 9 lines are added                                                             #
#                                                                               #
# Author: Shuqi Sheng                                                           #
#                                                                               #
#                                                                               #
#################################################################################

B2CCH = {
    "BUILDERTYPE": "B2CCHConf",
    "CONFIG": {
        "B0ComCuts": "AALL",
        "B0ComN3Cuts": "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)",
        "B0ComN4Cuts": "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)",
        "B0MomCuts": "(VFASPF(VCHI2/VDOF)<10.) & (BPVLTIME()>0.2*ps) & (BPVIPCHI2()<25) & (BPVDIRA>0.)",
        "Bs0ComCuts": "AALL",
        "Bs0ComN3Cuts": "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)",
        "Bs0MomCuts": "(VFASPF(VCHI2/VDOF)<10.) & (BPVLTIME()>0.2*ps) & (BPVIPCHI2()<25) & (BPVDIRA>0.)",
        "BuComCuts": "AALL",
        "BuComN3Cuts": "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)",
        "BuMomCuts": "(VFASPF(VCHI2/VDOF)<10.) & (BPVLTIME()>0.2*ps) & (BPVIPCHI2()<25) & (BPVDIRA>0.)",
        "D0ComCuts": "AALL",
        "D0ComN4Cuts": "(ADAMASS('D0')<110*MeV) & (ASUM(PT)>500*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 300*MeV) & (P > 3000*MeV) & (TRCHI2DOF<4.))) & (ADOCA(1,2)<0.5*mm) &(ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm) & (ADOCA(1,4)<0.5*mm) & (ADOCA(2,4)<0.5*mm) & (ADOCA(3,4)<0.5*mm)",
        "D0MomCuts": "(VFASPF(VCHI2/VDOF)<10) & (ADMASS('D0')<100*MeV) & (BPVDIRA>0.)",
        "DmComCuts": "(ADOCA(1,2)<0.5*mm)",
        "DmComN3Cuts": "(ASUM(PT)>500*MeV) & (ADAMASS('D-')<110*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 300*MeV) & (P > 3000*MeV))) & (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        "DmMomCuts": "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)",
        "DspComCuts": "(ADOCA(1,2)<0.5*mm)",
        "DspComN3Cuts": "(ASUM(PT)>500*MeV) & (ADAMASS('D_s+')<110*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 300*MeV) & (P > 3000*MeV))) & (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        "DspMomCuts": "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)",
        "KaonCuts": "(PT>100*MeV) & (PROBNNk>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0) & (TRCHI2DOF<4)",
        "Lambdab0ComCuts": "AALL",
        "Lambdab0ComN3Cuts": "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)",
        "Lambdab0MomCuts": "(VFASPF(VCHI2/VDOF)<10.) & (BPVLTIME()>0.2*ps) & (BPVIPCHI2()<25) & (BPVDIRA>0.)",
        "LcComCuts": "(ADOCA(1,2)<0.5*mm)",
        "LcComN3Cuts": "(ASUM(PT)>1000*MeV) & (ADAMASS('Lambda_c+')<110*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 500*MeV) & (P > 5000*MeV))) & (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        "LcMomCuts": "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)",
        "PionCuts": "(PT>100*MeV) & (PROBNNpi>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0)& (TRCHI2DOF<4)",
        "Prescale": 1.0,
        "ProtonCuts": "(PT>100*MeV) & (PROBNNp>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0) & (TRCHI2DOF<4)",
        "Xic0barComCuts": "AALL",
        "Xic0barComN4Cuts": "(ASUM(PT)>1000*MeV) & (ADAMASS('Xi_c~0')<110*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 300*MeV) & (P > 3000*MeV) & (TRCHI2DOF<4.)))",
        "Xic0barMomCuts": "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)",
        "XicpComCuts": "(ADOCA(1,2)<0.5*mm)",
        "XicpComN3Cuts": "(ASUM(PT)>1000*MeV) & (ADAMASS('Xi_c+')<110*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 500*MeV) & (P > 5000*MeV))) & (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        "XicpMomCuts": "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)"
    },
    "STREAMS": [ "Bhadron" ],
    "WGs": [ "BandQ" ]
}
