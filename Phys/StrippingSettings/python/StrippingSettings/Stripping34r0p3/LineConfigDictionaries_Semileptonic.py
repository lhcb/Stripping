###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
 
################################################################################
##                          S T R I P P I N G  34r0p3                         ##
##                                                                            ##
##  Configuration for Semileptonic WG                                         ##
##  Contact person: Fabian Christoph Glaser (fabian.christoph.glaser@cern.ch) ##
################################################################################


################################################################################
#                                                                              #
#                                                                              #
# B23MuNu module for selecting                                                 #
# B+ -> l nu l' l'                                                             #
# 16 lines in total                                                            #
#                                                                              #
# Authors: Vitalii Lisovskyi, Fabian Christoph Glaser                          #
#                                                                              #
#                                                                              #
################################################################################

B23MuNu = {
    "BUILDERTYPE": "B23MuNuConf", 
    "CONFIG": {
        "BPT": 2000.0, 
        "BPT_LOOSE": 1500.0, 
        "CORRM_MAX": 10000.0, 
        "CORRM_MIN": 2500.0, 
        "CORRM_MIN_LOOSE": 2000.0, 
        "DIRA": 0.99, 
        "DIRA_LOOSE": 0.95, 
        "DiElectron_MASS_MAX": 5500.0, 
        "DiElectron_MASS_MAX_TIGHT": 1100.0, 
        "DiElectron_MASS_MIN": 0.0, 
        "DiElectron_PT": 0.0, 
        "DiElectron_PT_TIGHT": 800.0, 
        "DiElectron_Vertex_TIGHT": 4.0, 
        "Electron_MinIPCHI2": 25.0, 
        "Electron_PIDe": 2.0, 
        "Electron_PIDeK": 0.0, 
        "Electron_PT": 200.0, 
        "Electron_PT_TIGHT": 300.0, 
        "Electron_ProbNNe_Tight": 0.2, 
        "FakeTauPrescale": 1, 
        "FlightChi2": 30.0, 
        "FlightChi2_TIGHT": 64.0, 
        "LOWERMASS": 0.0, 
        "LOWERMASS_TIGHT": 400.0, 
        "Lepton_PT_VERYTIGHT": 500.0, 
        "MisIDPrescale": 0.01, 
        "MisIDPrescaleLoose": 0.025, 
        "MisIDPrescaleVeryLoose": 0.05, 
        "Muon_MinIPCHI2": 9.0, 
        "Muon_MinIPCHI2_Tight": 18.0, 
        "Muon_PIDmu": 0.0, 
        "Muon_PIDmuK": 0.0, 
        "Muon_PIDmu_Tight": 3.0, 
        "Muon_PT": 0.0, 
        "Muon_ProbNNmu_Tight": 0.2, 
        "SpdMult": 900, 
        "Tau21Pion_MinIPCHI2": 36.0, 
        "Tau21Pion_PIDK": 2, 
        "Tau21Pion_PT": 1000, 
        "Tau21Pion_ProbNNpi": 0.2, 
        "Tau_VertexChi2": 4.0, 
        "Track_CHI2nDOF": 3.0, 
        "Track_GhostProb": 0.35, 
        "UPPERMASS": 7500.0, 
        "UPPERMASS_TIGHT": 6800.0, 
        "VertexCHI2": 4.0, 
        "VertexCHI2_1PI": 64.0
    }, 
    "STREAMS": { 
        "Semileptonic": [
            "StrippingB23MuNu_TrieLine",
            "StrippingB23MuNu_MueeLine",
            "StrippingB23MuNu_MuMueLine",
            "StrippingB23MuNu_TrieFakeLine",
            "StrippingB23MuNu_MuMuEFakeLine",
            "StrippingB23MuNu_MuMuFakeELine",
            "StrippingB23MuNu_MuFakeEELine",
            "StrippingB23MuNu_MuEEFakeLine",
            "StrippingB23MuNu_MuMuTauLine",
            "StrippingB23MuNu_MuMuTauFakeLine",
            "StrippingB23MuNu_MuMuFakeTauLine",
            "StrippingB23MuNu_EETauLine",
            "StrippingB23MuNu_EETauFakeLine",
            "StrippingB23MuNu_EEFakeTauLine",
            "StrippingB23MuNu_MuMuTau1PiLine",
            "StrippingB23MuNu_MuMuFakeTau1PiLine"
        ]
    }, 
    "WGs": [ "Semileptonic" ]
}

################################################################################
#                                                                              #
#                                                                              #
# B2XuMuNuMVA module for selecting                                             #
# B -> pi mu nu                                                                #
# 4 lines in total                                                             #
#                                                                              #
# Authors: Mark Smith                                                          #
#                                                                              #
#                                                                              #
################################################################################

B2XuMuNuMVA = {
    "BUILDERTYPE": "B2XuMuNuMVABuilder", 
    "CONFIG": {
        "B2PiMuNuMVACut": "0.66", 
        "B2PiMuNuMVAFile": "$TMVAWEIGHTSROOT/data/B2PiMuNu_2023_stripping_BDT.xml", 
        "BCorrMHigh": 7000.0, 
        "BCorrMLow": 2500.0, 
        "BDIRA": 0.999, 
        "BDOCA": 0.5, 
        "BVCHI2DOF": 4.0, 
        "GEC_nLongTrk": 250.0, 
        "MuonMINIPCHI2": 9, 
        "MuonP": 3000.0, 
        "MuonProbNNmu": 0.6, 
        "MuonTRCHI2": 4.0, 
        "PiMuMassLow": 2000.0, 
        "PiMuMassUpper": 6000.0, 
        "PiMuNu_NoPIDhad_prescale": 1., 
        "PiMuNu_NoPIDmu_prescale": 0.04, 
        "PiMuNu_SameSign_prescale": 1.0, 
        "PionMINIPCHI2": 9, 
        "PionP": 3000.0, 
        "PionPT": 200.0, 
        "PionProbNNpi": 0.2, 
        "PionTRCHI2": 4.0, 
        "TRGHOSTPROB": 0.35
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "Semileptonic" ]
}

################################################################################
#                                                                              #
#                                                                              #
# BXX module for selecting                                                     #
# D*(2010) -> D0 (-> pi+ pi- K-) pi                                            #
# 2 lines in total                                                             #
#                                                                              #
# Authors: Michael Kent Wilkinson                                              #
#                                                                              #
#                                                                              #
################################################################################

BXX = {
    "BUILDERTYPE": "BXXLinesBuilderConf", 
    "CONFIG": {
        "DstarBPVIPCHI2Max": 25.0, 
        "DstarBPVVDCHI2Max": 25.0, 
        "DstarHLT2TOSSelection": "Hlt2CharmHadInclDst.*", 
        "DstarL0TOSSelection": "L0Hadron.*", 
        "DstarMassDiffDzeroPlusPionMax": 40.0, 
        "DstarPTMin": 3000.0, 
        "DstarPionMIPCHI2DVMax": 4.0, 
        "DstarPionMIPDVMax": 0.3, 
        "DstarPionPIDKMax": 10.0, 
        "DstarPionPTMin": 250.0, 
        "DstarPionTRCHI2DOFMax": 3.0, 
        "DstarVCHI2DOFMax": 5.0, 
        "DzeroADOCACHI2CUT": 20.0, 
        "DzeroBPVDIRAMin": 0.9997, 
        "DzeroBPVIPCHI2Max": 25, 
        "DzeroBPVVDCHI2Min": 120.0, 
        "DzeroBPVVDMin": 4.0, 
        "DzeroDaughterKaonPIDKMin": 4.0, 
        "DzeroDaughterMIPCHI2DVMin": 4.0, 
        "DzeroDaughterPMin": 2000.0, 
        "DzeroDaughterPTMin": 400.0, 
        "DzeroDaughterPionPIDKMax": 10.0, 
        "DzeroDaughterPionPIDmuMax": 10.0, 
        "DzeroDaughterTRCHI2DOFMax": 3.0, 
        "DzeroMassMax": 1700.0, 
        "DzeroMassMin": 1400.0, 
        "DzeroPTMin": 3000.0, 
        "DzeroPionPairMassMax": 975.5, 
        "DzeroPionPairMassMin": 575.5, 
        "DzeroVCHI2DOFMax": 6.0, 
        "InputKaonsForDzeroLocation": "Phys/StdLooseKaons/Particles", 
        "InputPionsForDstarLocation": "Phys/StdAllLoosePions/Particles", 
        "InputPionsForDzeroLocation": "Phys/StdLoosePions/Particles", 
        "prescaleRS": 1.0, 
        "prescaleWS": 1.0
    }, 
    "STREAMS": {
        "Semileptonic": [
            "StrippingDstRSwD02K2PiD0forBXXLine", 
            "StrippingDstWSwD02K2PiD0forBXXLine"
        ]
    }, 
    "WGs": [
        "Charm", 
        "Semileptonic"
    ]
}

