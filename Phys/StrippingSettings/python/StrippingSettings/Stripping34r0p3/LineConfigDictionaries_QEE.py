###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                          S T R I P P I N G  3 4 r 0 p 3                    ##
##                                                                            ##
##  Configuration for QEE WG                                                  ##
##  Contact person: Xiaolin Wang (xiaolin.wang@cern.ch)                       ##
################################################################################

Exotica = {
    "BUILDERTYPE": "ExoticaConf", 
    "CONFIG": {
        "Common": {
            "GhostProb": 0.3
        }, 
        "DiRHNu": {
            "PT": 0, 
            "VChi2": 10
        }, 
        "DisplDiE": {
            "EIPChi2": 4, 
            "EProbNNe": 0.1, 
            "FDChi2": 0, 
            "IPChi2": 32, 
            "MaxMM": 500.0, 
            "PT": 500.0, 
            "TisTosSpec": "Hlt1TrackMVA.*Decision"
        }, 
        "DisplDiEHighMass": {
            "EIPChi2": 4, 
            "EProbNNe": 0.1, 
            "FDChi2": 0, 
            "IPChi2": 32, 
            "MinMM": 500.0, 
            "PT": 500.0, 
            "TAU": 0.001, 
            "TisTosSpec": "Hlt1TrackMVA.*Decision"
        }, 
        "DisplDiELowMassMicro": {
            "EIPChi2": 4, 
            "EProbNNe": 0.1, 
            "HLT1FILTER": "HLT_PASS_RE('Hlt1DiElectronLowMassDecision')", 
            "HLT2FILTER": None, 
            "L0FILTER": None, 
            "MMAX": 300.0, 
            "MMIN": 5.0, 
            "PT": 1000.0, 
            "TisTosSpec": None
        }, 
        "DisplDiMuon": {
            "FDChi2": 4, 
            "IPChi2": 16, 
            "MaxMM": 500.0, 
            "MuIPChi2": 4, 
            "MuProbNNmu": 0.5, 
            "PT": 1000.0
        }, 
        "DisplDiMuonHighMass": {
            "FDChi2": 4, 
            "IPChi2": 16, 
            "MinMM": 500.0, 
            "MuIPChi2": 9, 
            "MuProbNNmu": 0.8, 
            "PT": 1000.0, 
            "TAU": 0.001
        }, 
        "DisplDiMuonNoPoint": {
            "FDChi2": 16, 
            "MSwitch": 500.0, 
            "MuIPChi2_highmass": 25, 
            "MuIPChi2_lowmass": 4, 
            "MuProbNNmu_highmass": 0.8, 
            "MuProbNNmu_lowmass": 0.5, 
            "PT": 1000.0, 
            "R": 0.0
        }, 
        "DisplDiMuonNoPointR": {
            "FDChi2": 16, 
            "MSwitch": 500.0, 
            "MuIPChi2_highmass": 25, 
            "MuIPChi2_lowmass": 4, 
            "MuProbNNmu_highmass": 0.8, 
            "MuProbNNmu_lowmass": 0.5, 
            "PT": 1000.0, 
            "R": 2.75
        }, 
        "DisplPhiPhi": {
            "FDChi2": 45, 
            "KIPChi2": 16, 
            "KPT": 500.0, 
            "KProbNNk": 0.1, 
            "PhiMassWindow": 20.0, 
            "PhiPT": 1000.0, 
            "TisTosSpec": "Hlt1IncPhi.*Decision", 
            "VChi2": 10, 
            "input": "Phys/StdLoosePhi2KK/Particles"
        }, 
        "EtaToDiEGammaMicro": {
            "EEPT": 1000.0, 
            "EIPChi2": -1, 
            "EProbNNe": 0.1, 
            "GCL": 0.2, 
            "GPT": 500.0, 
            "HLT1FILTER": "HLT_PASS_RE('Hlt1DiElectronLowMass.*Decision')", 
            "HLT2FILTER": None, 
            "L0FILTER": None, 
            "MMAX": 700.0, 
            "MMIN": 400.0, 
            "PT": 1000.0, 
            "TisTosSpec": None, 
            "input": "Phys/StdLooseAllPhotons/Particles"
        }, 
        "Pi0ToDiEGammaMicro": {
            "EEPT": 1000.0, 
            "EIPChi2": -1, 
            "EProbNNe": 0.1, 
            "GCL": 0.2, 
            "GPT": 500.0, 
            "HLT1FILTER": "HLT_PASS_RE('Hlt1DiElectronLowMass.*Decision')", 
            "HLT2FILTER": None, 
            "L0FILTER": None, 
            "MMAX": 300.0, 
            "MMIN": 0.0, 
            "PT": 1000.0, 
            "TisTosSpec": None, 
            "input": "Phys/StdLooseAllPhotons/Particles"
        }, 
        "Prescales": {
            "DiRHNu": 0, 
            "DisplDiE": 0, 
            "DisplDiEHighMass": 0, 
            "DisplDiELowMassMicro": 1.0, 
            "DisplDiMuon": 0, 
            "DisplDiMuonHighMass": 0, 
            "DisplDiMuonNoPoint": 0, 
            "DisplDiMuonNoPointR": 0, 
            "DisplPhiPhi": 0, 
            "EtaToDiEGammaMicro": 1.0, 
            "Pi0ToDiEGammaMicro": 1.0, 
            "PrmptDiMuonHighMass": 0, 
            "QuadMuonNoIP": 0, 
            "RHNu": 0, 
            "RHNuHighMass": 0
        }, 
        "PrmptDiMuonHighMass": {
            "FDChi2": 45, 
            "M": 3200.0, 
            "M_switch_ab": 740.0, 
            "M_switch_bc": 1100.0, 
            "M_switch_cd": 3000.0, 
            "M_switch_de": 3200.0, 
            "M_switch_ef": 9000.0, 
            "MuIPChi2": 6, 
            "MuP": 10000.0, 
            "MuPT": 500.0, 
            "MuPTPROD": 1000000.0, 
            "MuProbNNmu_a": 0.8, 
            "MuProbNNmu_b": 0.8, 
            "MuProbNNmu_c": 0.95, 
            "MuProbNNmu_d": 2.0, 
            "MuProbNNmu_e": 0.95, 
            "MuProbNNmu_f": 0.9, 
            "PT": 1000.0
        }, 
        "QuadMuonNoIP": {
            "PT": 0, 
            "VChi2": 10
        }, 
        "RHNu": {
            "M": 0, 
            "TAU": 0.001
        }, 
        "RHNuHighMass": {
            "M": 5000.0, 
            "ProbNNmu": 0.5, 
            "TAU": 0.001
        }, 
        "SharedDiENoIP": {
            "DOCA": 0.5, 
            "EP": 5000.0, 
            "EPT": 500.0, 
            "EProbNNe": 0.1, 
            "MM": 0.0, 
            "VChi2": 25, 
            "input": "Phys/StdAllLooseElectrons/Particles"
        }, 
        "SharedDiMuonNoIP": {
            "DOCA": 0.5, 
            "MuP": 10000.0, 
            "MuPT": 500.0, 
            "MuProbNNmu": 0.5, 
            "VChi2": 10, 
            "input": "Phys/StdAllLooseMuons/Particles"
        }, 
        "SharedRHNu": {
            "FDChi2": 45, 
            "IPChi2": 16, 
            "M": 0, 
            "P": 10000.0, 
            "PT": 500.0, 
            "ProbNNmu": 0.5, 
            "TAU": 0.001, 
            "VChi2": 10, 
            "XIPChi2": 16, 
            "input": [
                "Phys/StdAllLooseMuons/Particles", 
                "Phys/StdNoPIDsPions/Particles"
            ]
        }
    }, 
    "STREAMS": {
        #"Dimuon": [
        #    "StrippingExoticaDisplDiMuonLine", 
        #    "StrippingExoticaDisplDiMuonHighMassLine", 
        #    "StrippingExoticaDisplDiMuonNoPointLine", 
        #    "StrippingExoticaDisplDiMuonNoPointRLine"
        #], 
        #"EW": [
        #    "StrippingExoticaDisplPhiPhiLine", 
        #    "StrippingExoticaRHNuLine", 
        #    "StrippingExoticaRHNuHighMassLine", 
        #    "StrippingExoticaDiRHNuLine", 
        #    "StrippingExoticaPrmptDiMuonHighMassLine", 
        #    "StrippingExoticaQuadMuonNoIPLine", 
        #    "StrippingExoticaDisplDiELine", 
        #    "StrippingExoticaDisplDiEHighMassLine"
        #], 
        "Leptonic": [
            "StrippingExoticaDisplDiELowMassMicroLine", 
            "StrippingExoticaPi0ToDiEGammaMicroLine", 
            "StrippingExoticaEtaToDiEGammaMicroLine"
        ]
    }, 
    "WGs": [ "QEE" ]
}

LLP2Hadrons = {
    "BUILDERTYPE": "LLP2HadronsConf", 
    "CONFIG": {
        "Common": {
            "CommonTrackCuts": "(TRGHOSTPROB<0.3) & (P>3.*GeV) & (BPVIPCHI2() > 9.)"
        }, 
        "DD": {
            "Down": {
                "CombCuts": "(AM>3.0*GeV)", 
                "DCombCuts": "in_range(1.0*GeV, AM, 3.*GeV)", 
                "DMomCuts": "(ADMASS('D+') < 150.*MeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 9.)", 
                "MomCuts": "(M>3.0*GeV) & (PT>1.*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>3) & (VFASPF(VZ)>200*mm)", 
                "TrackCuts": "(PT>500*MeV)"
            }, 
            "Long": {
                "CombCuts": "(AM>3.0*GeV)", 
                "DCombCuts": "in_range(1.0*GeV, AM, 3.*GeV)", 
                "DMomCuts": "(ADMASS('D+') < 80.*MeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 4.)", 
                "K_id": "(PROBNNk  > 0.2)", 
                "MomCuts": "(M>3.0*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (PT>1.*GeV) & (BPVDLS>5)", 
                "Pi_id": "(PROBNNpi > 0.2)", 
                "TrackCuts": "(PT > 500*MeV)"
            }
        }, 
        "EtaPiPi": {
            "Down": {
                "CombCuts": "AALL", 
                "EtaCombCuts": "AALL", 
                "EtaMomCuts": "(ADMASS('eta') < 100.*MeV) & (VFASPF(VCHI2PDOF) < 9.)", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5) & (VFASPF(VZ)>200*mm) & (M<2.5*GeV)", 
                "Pi0_cuts": "(PT > 600*MeV)", 
                "TrackCuts": "(PT>500*MeV) & (PROBNNpi > 0.2)"
            }, 
            "Long": {
                "CombCuts": "AALL", 
                "EtaCombCuts": "(AM<1*GeV)", 
                "EtaMomCuts": "(ADMASS('eta') < 100.*MeV) & (VFASPF(VCHI2PDOF) < 9.)", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (PT>2.*GeV) & (BPVDLS>5) & (BPVIPCHI2() < 9.) & (M<2.5*GeV) & (BPVLTIME()>1.5*picosecond)", 
                "Pi0_cuts": "(PT>600*MeV)", 
                "Pi_id": "(PROBNNpi > 0.4)", 
                "TrackCuts": "(PT>500*MeV)"
            }
        }, 
        "EtaPiPi_gg": {
            "Down": {
                "CombCuts": "AALL", 
                "EtaCuts": "(CHILD(CL,1) > 0.6) & (CHILD(CL,2) > 0.6) & (CHILD(PT,1) > 0.8*GeV) & (CHILD(PT,2) > 0.8*GeV) & (PT>1.*GeV) & in_range(400,M,700)", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5) & (VFASPF(VZ)>200*mm) & (M<2.5*GeV) & (BPVIPCHI2() < 9.)", 
                "TrackCuts": "(PT>500*MeV)"
            }, 
            "Long": {
                "CombCuts": "AALL", 
                "EtaCuts": "(CHILD(CL,1) > 0.6) & (CHILD(CL,2) > 0.6) & (CHILD(PT,1) > 0.8*GeV) & (CHILD(PT,2) > 0.8*GeV) & (PT>1.5*GeV) & in_range(400,M,700)", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (PT>2.*GeV) & (BPVDLS>5) & (BPVIPCHI2() < 9.) & (M<2.5*GeV) & (BPVLTIME()>1.5*picosecond)", 
                "Pi_id": "(PROBNNpi > 0.4)", 
                "TrackCuts": "(PT>500*MeV)"
            }
        }, 
        "KsKpi_DD": {
            "Down": {
                "CombCuts": "AM>1.*GeV", 
                "Ks_MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (ADMASS('KS0') < 50.*MeV) & (BPVIPCHI2() > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)", 
                "Ks_PiCuts": "(MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5)  & (VFASPF(VZ)>200*mm) & (BPVIPCHI2() < 9.)", 
                "TrackCuts": "(PT>700*MeV)"
            }, 
            "Long": {
                "CombCuts": "in_range(1.0*GeV, AM, 12.2*GeV)", 
                "K_id": "(PROBNNk  > 0.4)", 
                "Ks_MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (ADMASS('KS0') < 30.*MeV) & (BPVIPCHI2() > 9.)", 
                "Ks_PiCuts": "(MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, MIPCHI2DV(PRIMARY)) > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)", 
                "MomCuts": "in_range(1.0*GeV, M, 12.0*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5) & (PT>2.*GeV) & (BPVIPCHI2() < 9.) & (BPVLTIME()>1.*picosecond)", 
                "Pi_id": "(PROBNNpi > 0.4)", 
                "TrackCuts": "(PT > 700*MeV)"
            }
        }, 
        "KsKpi_LL": {
            "Long": {
                "CombCuts": "in_range(1.0*GeV, AM, 12.2*GeV)", 
                "K_id": "(PROBNNk  > 0.4)", 
                "Ks_MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 9.) & (BPVDLS>5) & (ADMASS('KS0') < 30.*MeV)", 
                "Ks_PiCuts": "(MINTREE('pi-'==ABSID, PROBNNpi) > 0.4) & (MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, MIPCHI2DV(PRIMARY)) > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)", 
                "MomCuts": "in_range(1.0*GeV, M, 12.0*GeV) & (VFASPF(VCHI2PDOF)<9.) & (BPVDLS>10) & (PT>2.*GeV) & (BPVIPCHI2()<9.) & (BPVLTIME()>1.5*picosecond)", 
                "Pi_id": "(PROBNNpi > 0.4)", 
                "TrackCuts": "(PT > 700*MeV)"
            }
        }, 
        "KstKst": {
            "Down": {
                "CombCuts": "AALL", 
                "K_id": "(PROBNNk  > 0.1)", 
                "KstCombCuts": "(ADAMASS('K*(892)0')<300*MeV)", 
                "KstMomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (in_range( PDGM('K*(892)0') - 120*MeV , M , PDGM('K*(892)0') + 120*MeV ))", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS > 5)  & (VFASPF(VZ)>200*mm) & (PT>2.*GeV)", 
                "Pi_id": "(PROBNNpi > 0.1)", 
                "TrackCuts": "(PT>500*MeV)"
            }, 
            "Long": {
                "CombCuts": "AALL", 
                "K_id": "(PROBNNk  > 0.4)", 
                "KstCombCuts": "(ADAMASS('K*(892)0')<300*MeV)", 
                "KstMomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (in_range( PDGM('K*(892)0') - 120*MeV , M , PDGM('K*(892)0') + 120*MeV ))", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS > 10) & (PT>2.*GeV) & (BPVIPCHI2() < 9.)", 
                "Pi_id": "(PROBNNpi > 0.4)", 
                "TrackCuts": "(PT>500*MeV)"
            }
        }, 
        "Prescales": {
            "DD": 0, 
            "EtaPiPi": 0, 
            "EtaPiPi_gg": 1.0, 
            "KsKpi": 0, 
            "KstKst": 0
        }
    }, 
    "STREAMS": {
        "BhadronCompleteEvent": [
            #"StrippingLLP2Hadrons_KstKst_LongLine", 
            #"StrippingLLP2Hadrons_KsKpi_LL_LongLine", 
            #"StrippingLLP2Hadrons_KsKpi_DD_LongLine", 
            #"StrippingLLP2Hadrons_DpDm_LongLine", 
            #"StrippingLLP2Hadrons_DpDz_LongLine", 
            #"StrippingLLP2Hadrons_DpDp_LongLine", 
            #"StrippingLLP2Hadrons_DzDz_LongLine", 
            #"StrippingLLP2Hadrons_DzDzbar_LongLine", 
            #"StrippingLLP2Hadrons_EtaPiPi_LongLine", 
            "StrippingLLP2Hadrons_EtaPiPi_gg_LongLine", 
            #"StrippingLLP2Hadrons_KstKst_DownLine", 
            #"StrippingLLP2Hadrons_KsKpi_DD_DownLine", 
            #"StrippingLLP2Hadrons_DpDm_DownLine", 
            #"StrippingLLP2Hadrons_DpDz_DownLine", 
            #"StrippingLLP2Hadrons_DpDp_DownLine", 
            #"StrippingLLP2Hadrons_DzDz_DownLine", 
            #"StrippingLLP2Hadrons_DzDzbar_DownLine", 
            #"StrippingLLP2Hadrons_EtaPiPi_DownLine", 
            "StrippingLLP2Hadrons_EtaPiPi_gg_DownLine"
        ]
    }, 
    "WGs": [ "QEE" ]
}

LambdaDecaysDM = {
    "BUILDERTYPE": "LambdaDecaysDMConf", 
    "CONFIG": {
        "Common": {
            "checkPV": False
        }, 
        "StrippingDMLambda1520Line": {
            "Lambda1520_Daug_PT": 500.0, 
            "Lambda1520_FDChi2": 45, 
            "Lambda1520_GhostProb": 0.3, 
            "Lambda1520_IPChi2": 25, 
            "Lambda1520_M_Max": 1580, 
            "Lambda1520_M_Min": 1460, 
            "Lambda1520_MaxDoca": 0.1, 
            "Lambda1520_PT": 1000.0, 
            "Lambda1520_ProbNNK": 0.5, 
            "Lambda1520_ProbNNp": 0.8, 
            "Lambda1520_VChi2": 2, 
            "Lambda1520_iso": 0.85, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.1
        }, 
        "StrippingDMLambda2595Line": {
            "K_PT": 250.0, 
            "K_ProbNNghost": 0.1, 
            "K_ProbNNk": 0.5, 
            "Lambda2595_DOCA": 1.0, 
            "Lambda2595_FDCHI2": 5, 
            "Lambda2595_ISO": 0.6, 
            "Lambda2595_M_Max": 2645.0, 
            "Lambda2595_M_Min": 2545.0, 
            "Lambda2595_PT": 1500.0, 
            "Lambda2595_VCHI2": 4, 
            "Lambda_DOCA": 1.0, 
            "Lambda_FDCHI2": 50, 
            "Lambda_M_Max": 2336.0, 
            "Lambda_M_Min": 2236.0, 
            "Lambda_PT": 1500.0, 
            "Lambda_VCHI2": 4, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.6, 
            "p_PT": 250.0, 
            "p_ProbNNghost": 0.1, 
            "p_ProbNNp": 0.5, 
            "pi_lambda2595_IPchi2": 0.5, 
            "pi_lambda2595_PT": 100.0, 
            "pi_lambda2595_ProbNNghost": 0.1, 
            "pi_lambdac_PT": 250.0, 
            "pi_lambdac_ProbNNghost": 0.1
        }, 
        "StrippingDMLambdaToDKLine": {
            "D_DOCA": 0.4, 
            "D_FDCHI2": 50, 
            "D_M_Max": 1900.0, 
            "D_M_Min": 1840.0, 
            "D_PT": 2000.0, 
            "D_VCHI2": 6, 
            "KD_IPchi2": 20, 
            "KD_PT": 250.0, 
            "KD_ProbNNK": 0.7, 
            "KD_ProbNNghost": 0.2, 
            "K_PT": 250.0, 
            "K_ProbNNghost": 0.1, 
            "K_ProbNNk": 0.8, 
            "LambdaToDK_DOCA": 0.5, 
            "LambdaToDK_DOCACHI2": 8, 
            "LambdaToDK_FDCHI2": 30, 
            "LambdaToDK_IPCHI2": 25, 
            "LambdaToDK_ISO": 0.6, 
            "LambdaToDK_M_Max": 5700.0, 
            "LambdaToDK_M_Min": 1100.0, 
            "LambdaToDK_PT": 2000.0, 
            "LambdaToDK_VCHI2": 8, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.5, 
            "pi_PT": 250.0, 
            "pi_ProbNNghost": 0.1, 
            "pi_ProbNNpi": 0.8
        }, 
        "StrippingDMLambdaToDPiLine": {
            "D_DOCA": 0.4, 
            "D_FDCHI2": 50, 
            "D_M_Max": 1900.0, 
            "D_M_Min": 1840.0, 
            "D_PT": 2000.0, 
            "D_VCHI2": 6, 
            "K_PT": 800.0, 
            "K_ProbNNghost": 0.1, 
            "K_ProbNNk": 0.9, 
            "LambdaToDPi_DOCA": 0.3, 
            "LambdaToDPi_DOCACHI2": 7, 
            "LambdaToDPi_FDCHI2": 30, 
            "LambdaToDPi_IPCHI2": 20, 
            "LambdaToDPi_ISO": 0.6, 
            "LambdaToDPi_M_Max": 5700.0, 
            "LambdaToDPi_M_Min": 1100.0, 
            "LambdaToDPi_PT": 3000.0, 
            "LambdaToDPi_VCHI2": 8, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.3, 
            "piD_IPchi2": 20, 
            "piD_PT": 400.0, 
            "piD_ProbNNghost": 0.2, 
            "piD_ProbNNpi": 0.8, 
            "pi_PT": 800.0, 
            "pi_ProbNNghost": 0.1, 
            "pi_ProbNNpi": 0.8
        }, 
        "StrippingDMLambdaToKPiLine": {
            "LambdaToKPi_DOCAChi2": 4, 
            "LambdaToKPi_Daug_IPChi2": 20, 
            "LambdaToKPi_Daug_IP_max": 3.0, 
            "LambdaToKPi_Daug_IP_min": 0.1, 
            "LambdaToKPi_Daug_PT": 800.0, 
            "LambdaToKPi_FD": 5, 
            "LambdaToKPi_FDChi2": 40, 
            "LambdaToKPi_GhostProb": 0.1, 
            "LambdaToKPi_IPChi2": 15, 
            "LambdaToKPi_IPdivFD": 0.1, 
            "LambdaToKPi_M_Max": 5700, 
            "LambdaToKPi_M_Min": 1100, 
            "LambdaToKPi_MaxDoca": 0.1, 
            "LambdaToKPi_PT": 1000.0, 
            "LambdaToKPi_ProbNNK": 0.9, 
            "LambdaToKPi_ProbNNpi": 0.9, 
            "LambdaToKPi_VChi2": 4, 
            "LambdaToKPi_iso": 0.9, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.1
        }, 
        "StrippingDMLambdaToPiPiLine": {
            "LambdaToPiPi_DOCAChi2": 4, 
            "LambdaToPiPi_Daug_IPChi2": 12, 
            "LambdaToPiPi_Daug_IP_max": 3.0, 
            "LambdaToPiPi_Daug_IP_min": 0.1, 
            "LambdaToPiPi_Daug_PT": 1000.0, 
            "LambdaToPiPi_FD": 5.5, 
            "LambdaToPiPi_FDChi2": 10, 
            "LambdaToPiPi_GhostProb": 0.1, 
            "LambdaToPiPi_IPChi2": 10, 
            "LambdaToPiPi_IPdivFD": 0.1, 
            "LambdaToPiPi_M_Max": 5500, 
            "LambdaToPiPi_M_Min": 1100, 
            "LambdaToPiPi_MaxDoca": 0.05, 
            "LambdaToPiPi_PT": 1300.0, 
            "LambdaToPiPi_ProbNNpi": 0.9, 
            "LambdaToPiPi_VChi2": 4, 
            "LambdaToPiPi_iso": 0.95, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.1
        }
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

