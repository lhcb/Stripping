###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

################################################################################
##                          S T R I P P I N G  29r2p3                         ##
##                                                                            ##
##  Configuration for Charm WG                                                ##
##  Contact person: Sergio Jaimes     (sergio.jaimes@cern.ch)                 ##
################################################################################

Charm2PPX = {
    "BUILDERTYPE": "StrippingCharm2PPXConf", 
    "CONFIG": {
        "Comb_ADOCACHI2_MAX": 16, 
        "Daug_1ofAll_BPVIPCHI2_MIN": 9.0, 
        "Daug_1ofAll_PT_MIN": 700.0, 
        "Daug_All_BPVIPCHI2_MIN": 4.0, 
        "Daug_All_CloneDist_MIN": 5000, 
        "Daug_All_MIPCHI2_MIN": 4.0, 
        "Daug_All_PT_MIN": 200.0, 
        "Daug_All_TrChostProb_MAX": 0.5, 
        "Daug_ETA_MAX": 4.9, 
        "Daug_ETA_MIN": 2.0, 
        "Daug_P_MAX": 100000.0, 
        "Daug_P_MIN": 3200.0, 
        "Hc_BPVCTAU_MIN": 0.05, 
        "Hc_BPVVDCHI2_MIN": 9.0, 
        "Hc_MASS_MAX": 3500.0, 
        "Hc_PT_MIN": 1000.0, 
        "Hc_VCHI2VDOF_MAX": 25.0, 
        "Hcs_MASS_MAX": 3650.0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "HsComb_ADOCACHI2_MAX": 16, 
        "HsDaug_1ofAll_BPVIPCHI2_MIN": 9.0, 
        "HsDaug_1ofAll_PT_MIN": -500.0, 
        "HsDownDaug_1ofAll_BPVIPCHI2_MIN": -9.0, 
        "HsDown_BPVVDCHI2_MIN": 4.0, 
        "HsProton_1ofAll_ProbNN_MIN": 0.5, 
        "Hs_BPVCTAU_MIN": 0.1, 
        "Hs_BPVVDCHI2_MIN": 9.0, 
        "Hs_MASS_MAX": 2125.0, 
        "Hs_PT_MIN": -1000.0, 
        "Hs_VCHI2VDOF_MAX": 25.0, 
        "Kaon_ProbNN_MIN": 0.1, 
        "Lam_MASS_MAX": 1170.0, 
        "Lc_MASS_MAX": 2350.0, 
        "Pc_MASS_MAX": 3100.0, 
        "Pion_ProbNN_MIN": 0.1, 
        "PostscaleHc2LamPPi": 1.0, 
        "PostscaleHc2LamPPiPi": 1.0, 
        "PostscaleHc2PP": 1.0, 
        "PostscaleHc2PPK": 1.0, 
        "PostscaleHc2PPKPi": 1.0, 
        "PostscaleHc2PPKPiPi": 1.0, 
        "PostscaleHc2PPKs": 1.0, 
        "PostscaleHc2PPKsPi": 1.0, 
        "PostscaleHcs2LamLamPi": 1.0, 
        "PostscaleHcs2LamPKPi": 1.0, 
        "PostscaleHcs2PPKKPi": 1.0, 
        "PostscaleHcs2PPKsK": 1.0, 
        "PostscaleHs2PPPi": 1.0, 
        "PostscaleL2PPi": 1.0, 
        "PostscaleLc2PKPi": 1.0, 
        "PostscalePc2PKPiPi": 1.0, 
        "PostscalePc2PKsPi": 1.0, 
        "PostscalePs2PPiPi": 1.0, 
        "PrescaleHc2LamPPi": 1.0, 
        "PrescaleHc2LamPPiPi": 1.0, 
        "PrescaleHc2PP": 1.0, 
        "PrescaleHc2PPK": 1.0, 
        "PrescaleHc2PPKPi": 1.0, 
        "PrescaleHc2PPKPiPi": 1.0, 
        "PrescaleHc2PPKs": 1.0, 
        "PrescaleHc2PPKsPi": 1.0, 
        "PrescaleHcs2LamLamPi": 1.0, 
        "PrescaleHcs2LamPKPi": 1.0, 
        "PrescaleHcs2PPKKPi": 1.0, 
        "PrescaleHcs2PPKsK": 1.0, 
        "PrescaleHs2PPPi": 1.0, 
        "PrescaleL2PPi": 0.01, 
        "PrescaleLc2PKPi": 0.01, 
        "PrescalePc2PKPiPi": 0.1, 
        "PrescalePc2PKsPi": 0.1, 
        "PrescalePs2PPiPi": 0.1, 
        "Proton_1ofAll_ProbNN_MIN": 0.5, 
        "Proton_PT_MIN": 300.0, 
        "Proton_P_MIN": 10000.0, 
        "Proton_ProbNN_MIN": 0.1, 
        "Proton_ProbNNk_MAX": 0.8, 
        "Proton_ProbNNpi_MAX": 0.55, 
        "Ps_MASS_MAX": 1650.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

CharmedBaryonSL = {
    "BUILDERTYPE": "StrippingCharmedBaryonSL", 
    "CONFIG": {
        "GEC_nLongTrk": 250, 
        "GhostProb": 0.3, 
        "KaonP": 3000.0, 
        "KaonPIDK": 10.0, 
        "KaonPT": 500.0, 
        "KaonPT_ForOm": 100.0, 
        "KaonP_ForOm": 2000.0, 
        "Kaon_PIDKPIDpi_Min": 5.0, 
        "L0Mu_AM_Min": 1250.0, 
        "LambdaCutDIRA": 0.99, 
        "LambdaDDCutFDChi2": 100.0, 
        "LambdaDDCutMass": 20.0, 
        "LambdaDDMaxVZ": 2275.0, 
        "LambdaDDMinVZ": 400.0, 
        "LambdaDDPMin": 5000.0, 
        "LambdaDDPTMin": 800.0, 
        "LambdaDaugTrackChi2": 4.0, 
        "LambdaLDCutFDChi2": 100.0, 
        "LambdaLDCutMass": 20.0, 
        "LambdaLDPMin": 3000.0, 
        "LambdaLDPTMin": 800.0, 
        "LambdaLLCutFDChi2": 100.0, 
        "LambdaLLCutMass": 20.0, 
        "LambdaLLMaxVZ": 400.0, 
        "LambdaLLMinVZ": -100.0, 
        "LambdaLLPMin": 5000.0, 
        "LambdaLLPTMin": 800.0, 
        "LambdaPi_PT_MIN": 100.0, 
        "LambdaPr_PT_MIN": 500.0, 
        "LambdaVertexChi2": 5.0, 
        "Lc_ADMASS_HalfWin": 80.0, 
        "Lc_ADMASS_HalfWin_Tight": 55.0, 
        "Lc_ADOCAMAX_Max": 0.5, 
        "Lc_ADOCAMAX_Max_Tight": 0.15, 
        "Lc_AM_Max": 2370.0, 
        "Lc_APT_Min": 1000.0, 
        "Lc_APT_Min_Tight_PKMu": 1600.0, 
        "Lc_APT_Min_Tight_PPiMu": 1600.0, 
        "Lc_AP_Min_Tight": 25000.0, 
        "Lc_BPVDIRA_Min": 0.9, 
        "Lc_BPVDIRA_Min_Tight": 0.99, 
        "Lc_BPVVDCHI2_Min": 16.0, 
        "Lc_BPVVDCHI2_Min_Tight": 25.0, 
        "Lc_Daug_1of3_MIPCHI2DV_Min": 4.0, 
        "Lc_Daug_1of3_MIPCHI2DV_Min_Tight": 9.0, 
        "Lc_VCHI2_Max": 30.0, 
        "Lc_VCHI2_Max_Tight": 20.0, 
        "MINIPCHI2": 4.0, 
        "MisID_PROB_Max": 0.7, 
        "MuonIPCHI2": 5.0, 
        "MuonP": 3000.0, 
        "MuonPIDmu": 0.0, 
        "MuonPT": 500.0, 
        "Oc_AM_Max": 2780.0, 
        "OmMu_AM_Min": 1780.0, 
        "Omegac0_4Dau_VCHI2_Max": 60.0, 
        "Omegac0_ADAMASS_HalfWin": 170.0, 
        "Omegac0_ADMASS_HalfWin": 120.0, 
        "Omegac0_BPVDIRA_Min": 0.9, 
        "Omegac0_BPVVDCHI2_Min": 25.0, 
        "P_Min_Tight": 3500.0, 
        "PionP": 3000.0, 
        "PionPIDK": 10.0, 
        "PionPT": 500.0, 
        "PionPT_ForXi": 100.0, 
        "PionP_ForXi": 2000.0, 
        "Pion_PIDpiPIDK_Min": 0.0, 
        "ProbNNk": 0.4, 
        "ProbNNp": 0.4, 
        "ProbNNpMin": 0.2, 
        "ProbNNpi": 0.5, 
        "ProbNNpiMax": 0.95, 
        "ProtonP": 3000.0, 
        "ProtonPT": 500.0, 
        "Proton_PIDpPIDK_Min": 0.0, 
        "Proton_PIDpPIDpi_Min": 5.0, 
        "TRCHI2DOFMax": 3.0, 
        "TrGhostProbMax": 0.25, 
        "XiMu_AM_Min": 1430.0, 
        "Xic_ADAMASS_HalfWin": 170.0, 
        "Xic_ADMASS_HalfWin": 120.0, 
        "Xic_AM_Max": 2550.0, 
        "Xic_BPVDIRA_Min": 0.9, 
        "Xic_BPVVDCHI2_Min": 25.0, 
        "controlPrescaleViaLcL0DDPiNC": 0.8, 
        "controlPrescaleViaLcL0LLPiNC": 0.4, 
        "controlPrescaleViaLcPKPiNC": 0.09, 
        "controlPrescaleViaLcPKPiNC_Tight": 0.5, 
        "controlPrescaleViaLcPPiPiNC": 0.06, 
        "controlPrescaleViaLcPPiPiNC_Tight": 0.5, 
        "controlPrescaleViaOmegac0NC": 1.0, 
        "controlPrescaleViaXicL0DDPiNC": 0.8, 
        "controlPrescaleViaXicL0LLPiNC": 0.4, 
        "controlPrescaleViaXicXiPiNC": 1.0, 
        "pKMu_AM_Min": 1550.0, 
        "ppiMu_AM_Min": 1200.0, 
        "signalPrescaleViaLc": 1.0, 
        "signalPrescaleViaLcL0DDMuNu": 0.8, 
        "signalPrescaleViaLcPKMuNu_Tight": 1.0, 
        "signalPrescaleViaLcPPiMuNu": 0.25, 
        "signalPrescaleViaLcPPiMuNu_Tight": 1.0, 
        "signalPrescaleViaOmegac0": 1.0, 
        "signalPrescaleViaXic": 1.0, 
        "signalPrescaleViaXicL0DDMuNu": 0.8, 
        "signalPrescaleViaXicL0LLMuNu": 0.8
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}

CharmedMesonSL = {
    "BUILDERTYPE": "StrippingCharmedMesonSL", 
    "CONFIG": {
        "B0SL_M_Max": 5600.0, 
        "B0SL_M_Min": 2800.0, 
        "B0_M_Max": 5600.0, 
        "B0_M_Min": 5100.0, 
        "B_APT_Min": 1500.0, 
        "B_AP_Min": 20000.0, 
        "B_BPVDIRA_Min": 0.95, 
        "B_BPVVDCHI2_Min": 16, 
        "B_VCHI2_Max": 30, 
        "BpSL_M_Max": 5700.0, 
        "BpSL_M_Min": 3000.0, 
        "Bp_M_Max": 5500.0, 
        "Bp_M_Min": 5100.0, 
        "D0SL_AM_Max": 1960.0, 
        "D0_ADMASS_Max": 1900.0, 
        "D0_ADMASS_Min": 1820.0, 
        "DpSL_AM_Max": 2080.0, 
        "Dp_ADMASS_Max": 2030.0, 
        "Dp_ADMASS_Min": 1800.0, 
        "Dp_ADOCAMAX_Max": 0.25, 
        "Dp_APT_Min": 1200.0, 
        "Dp_AP_Min": 20000.0, 
        "Dp_BPVDIRA_Min": 0.95, 
        "Dp_BPVVDCHI2_Min": 16.0, 
        "Dp_Daug_1of3_MIPCHI2DV_Min": 4.0, 
        "Dp_VCHI2_Max": 30.0, 
        "EtaPiMu_AM_Min": 800.0, 
        "GEC_nLongTrk": 160, 
        "GhostProb": 0.3, 
        "KKMu_AM_Min": 1100.0, 
        "KaonP": 3000.0, 
        "KaonPIDK": 10.0, 
        "KaonPT": 250.0, 
        "Kaon_PIDKPIDpi_Min": 5.0, 
        "MINIPCHI2": 4.0, 
        "MuonIPCHI2": 4.0, 
        "MuonP": 3000.0, 
        "MuonPIDmu": 0.0, 
        "MuonPT": 250.0, 
        "PiKKMu_AM_Min": 1240.0, 
        "PiPiMu_AM_Min": 400.0, 
        "PiPiPiMu_AM_Min": 530.0, 
        "PionP": 3000.0, 
        "PionPIDK": 5.0, 
        "PionPT": 250.0, 
        "Pion_PIDpiPIDK_Min": 0.0, 
        "ProbNNk": 0.4, 
        "ProbNNpi": 0.5, 
        "ProbNNpiMax": 0.9, 
        "TRCHI2DOFMax": 3.0, 
        "TrGhostProbMax": 0.25, 
        "controlPrescaleViaD02KKpipi": 1.0, 
        "controlPrescaleViaD02etapipi": 1.0, 
        "controlPrescaleViaD02pipipipi": 1.0, 
        "controlPrescaleViaD2KKpi": 1.0, 
        "controlPrescaleViaD2pipipi": 1.0, 
        "signalPrescaleViaD02KKpimunu": 1.0, 
        "signalPrescaleViaD02etapimunu": 1.0, 
        "signalPrescaleViaD02pipipimunu": 1.0, 
        "signalPrescaleViaD2KKmunu": 1.0, 
        "signalPrescaleViaD2pipimunu": 1.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

DstarD02ETau = {
    "BUILDERTYPE": "StrippingDstarD02ETauConf",
    'CONFIG': {'PrescaleETauBox': 1.,
            'PrescaleETauSSBox': 1.,
            'PrescaleK3piBox': 0.03,  # needed to keep the rates compatible with DST
            'PrescaleK3piSSBox': 0.03,  # needed to keep the rates compatible with DST
            'prefix': '',
            #
        #                                                                      
            'P_THREE_PI': "1000",  # MeV                                          
            'PT_THREE_PI': "200",  # MeV                                          
            'TRACKCHI2_THREE_PI': "2",  # dimensionless                           
            'TRGHOPROB_THREE_PI': "0.1",  # dimensionless                         
            'MINIPCHI2BPV_THREE_PI': "0.45",  # dimensionless                      
            'CUTBASED_PROBNNPI_THREE_PI': '0.8', #to reduce rate                  
            'PROBNNGHOST_THREE_PI': '0.05',                                        
            # tau                                                                 
            'TAU_M_LOW_COMB': "200",  # MeV                                       
            'TAU_M_HIGH_COMB': "2300",  # MeV                                     
            'TAU_AMAXDOCA_PIS': "0.15",  # mm                                      
            'TAU_APT_COMB': "500.",  # MeV                                        
            'TAU_PT': "1500.",  # MeV                                              
            'TAU_P': "15000.",  # MeV                                             
            'TAU_M_LOW': "300",  # MeV                                            
            'TAU_M_HIGH': "2200",  # MeV                                          
            'TAU_BPVDIRA': "0.9995", #dimensionless                                
            'CUTBASED_TAU_BPVVDRHO_LOW': "0.1",  # mm                            
            'CUTBASED_TAU_BPVVDRHO_HIGH': "7",  # mm                              
            'CUTBASED_TAU_BPVVDZ': "0.0015",  # mm                                
            'CUTBASED_TAU_VCHI2': "10", #dimensionless                            
            'CUTBASED_TAU_BPVVDCHI2': "19",  # dimensionless                     
            # Electron                                                            
            'P_E': "1000",  # MeV                                                 
            'PT_E': "200",  # MeV                                                 
            'TRACKCHI2_E': "2",  # dimensionless                                  
            'TRGHOPROB_E': "0.1",  # dimensionless                                
            'MINIPCHI2BPV_E': "0.2",  # dimensionless                            
            'CUTBASED_PROBNNE_E': '0.8', #dimensionless                           
            'PROBNNGHOST_E': '0.3', #dimensionless                                
            #Kaon for CM                                                          
            'CUTBASED_PROBNNK_K': '0.5', #dimensionless                           
            # D0                                                                  
            'P_D0': "20000",  # MeV                                               
            'PT_D0': "2000",  # MeV                                               
            'CUTBASED_M_D0_LOW': '300',  # MeV                                    
            'CUTBASED_M_D0_HIGH': '2200',  # MeV                                  
            'CUTBASED_AMAXDOCA_D0_COMB': "0.35",  # mm                             
            'CUTBASED_MCORR_D0_LOW': '1000',  # MeV                               
            'CUTBASED_MCORR_D0_HIGH': '8000',  # MeV                              
            'CUTBASED_MISS_MASS_LOW': '0',  # MeV                                 
            'CUTBASED_MISS_MASS_HIGH': '250',  # MeV                              
            'CUTBASED_MISS_MASS_HIGH_KTHREEPI': '1500',  # MeV                    
            'CUTBASED_TAU_TAU_LOW': '-2.5',  # ps                                 
            'CUTBASED_TAU_TAU_HIGH': '3',  # ps                                   
            'CUTBASED_BPVVDRHO_D0_LOW': '0.1',  # mm                             
            'CUTBASED_BPVVDRHO_D0_HIGH': '7',  # mm                               
            'CUTBASED_VCHI2_D0':   '5',  # dimensionless      
            'CUTBASED_VCHI2_D0_KTHREEPI':   '25',  # dimensionless                                                                         
            'CUTBASED_D0_ABS_BPVDIRA': '0.999',  # dimensionless                   
            'CUTBASED_D0_BPVVDCHI2': "2",  # dimensionless                        
                                                                                
            # pi from Dst                                                         
            'PT_PI_FROM_DTSM': "200",  # MeV                                      
            'P_PI_FROM_DTSM': "1000",  # MeV                                      
            'TRACKCHI2_PI_FROM_DTSM': "2",  # dimensionless                       
            'TRGHOPROB_PI_FROM_DTSM': "0.1",  # dimensionless                     
            'PROBNNGHOST_PI_FROM_DTSM': '0.05', #dimensionless                     
            'CUTBASED_PROBNNPI_PI_FROM_DTSM': '0.8', #dimensionless               
            # Dstm                                                                
            'Dst_AMAXDOCA_COMB': "0.2",  # mm                                     
            'DstMassWin_LOW': "-1450.",  # MeV                                    
            'DstMassWin_HIGH': "300.",   # MeV                                    
            'DstD0PIDMWin_LOW': "-120.",    # MeV                                  
            'DstD0PIDMWin_HIGH': "120.",    # MeV                                 
            'DstmVtxChi2': "8.",  # dimensionless                                

            # Isolations
            'ConeAngles': {"08": 0.8, "10": 1.0, "12": 1.2, "14": 1.4},
            'ConeVariables': ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
            'RelInfoTools_D02ETau': [
                {"Type": "RelInfoVertexIsolation",
                "Location": "DstVars_VertexIsoInfo",
                "DaughterLocations": {"[D*(2010)+ -> ^(Charm -> l  l) pi+]CC": "D0_VertexIsoInfo",
                                    "[D*(2010)+ ->  (Charm -> ^l l) pi+]CC": "Tau_VertexIsoInfo",
                                    "[D*(2010)+ ->  (Charm -> l ^l) pi+]CC": "E_VertexIsoInfo",
                                    "[D*(2010)+ -> ^(Charm -> l  l) ^pi+]CC": "pi_VertexIsoInfo",
                                    }},
                {"Type": "RelInfoConeIsolation",
                "ConeSize": 0.5,
                "Variables": [],
                "Location": "DstVars_ConeIsoInfo",
                "DaughterLocations": {"[D*(2010)+ -> ^(Charm -> l  l) pi+]CC": "D0_ConeIsoInfo",
                                    "[D*(2010)+ ->  (Charm -> ^l l) pi+]CC": "Tau_ConeIsoInfo",
                                    "[D*(2010)+ ->  (Charm -> l ^l) pi+]CC": "E_ConeIsoInfo",
                                    "[D*(2010)+ -> ^(Charm -> l  l) ^pi+]CC": "pi_ConeIsoInfo",
                                    }},
            ],
            'RelInfoTools_D02K3pi': [
                {"Type": "RelInfoVertexIsolation",
                "Location": "DstVars_VertexIsoInfo",
                "DaughterLocations": {"[D*(2010)+ -> ^(Charm -> X X X X) pi+]CC": "D0_VertexIsoInfo",
                                    "[D*(2010)+ -> ^(Charm -> X X X X) ^pi+]CC": "pi_VertexIsoInfo",
                                    }},
                {"Type": "RelInfoConeIsolation",
                "ConeSize": 0.5,
                "Variables": [],
                "Location": "DstVars_ConeIsoInfo",
                "DaughterLocations": {"[D*(2010)+ -> ^(Charm -> X X X X) pi+]CC": "D0_ConeIsoInfo",
                                    "[D*(2010)+ -> ^(Charm -> X X X X) ^pi+]CC": "pi_ConeIsoInfo",
                                    }},
            ]
            },
    "STREAMS": [ "CharmCompleteEvent" ],
    "WGs": [ "Charm" ],
    
}


#Hc2V02H = {
#    "BUILDERTYPE": "StrippingHc2V02HConf", 
#    "CONFIG": {
#        "Bach_PT_MIN": 250.0, 
#        "Bach_P_MIN": 2000.0, 
#        "Comb_ADAMASS_WIN": 120.0, 
#        "Comb_ADOCAMAX_MAX": 0.4, 
#        "Hlt1Filter": None, 
#        "Hlt2Filter": None, 
#        "Lambda0_FDCHI2_MIN_DD": 4, 
#        "Lambda0_FDCHI2_MIN_LL": 4, 
#        "Lambda0_PT_MIN": 250.0, 
#        "Lambda0_P_MIN": 6000.0, 
#        "Lambda0_VCHI2VDOF_MAX_DD": 25.0, 
#        "Lambda0_VCHI2VDOF_MAX_LL": 12.0, 
#        "LambdaMinFD_DD": 500.0, 
#        "LambdaMinFD_LL": 5.0, 
#        "MIPCHI2DV_PRIMARY_Min": 2, 
#        "PostscaleXic2LambdaKKDD": 1.0, 
#        "PostscaleXic2LambdaKKLL": 1.0, 
#        "PostscaleXic2LambdaKPiDD": 1.0, 
#        "PostscaleXic2LambdaKPiLL": 1.0, 
#        "PostscaleXic2LambdaPiKDD": 1.0, 
#        "PostscaleXic2LambdaPiKLL": 1.0, 
#        "PostscaleXic2LambdaPiPiDD": 1.0, 
#        "PostscaleXic2LambdaPiPiLL": 1.0, 
#        "PrescaleXic2LambdaKKDD": 1.0, 
#        "PrescaleXic2LambdaKKLL": 1.0, 
#        "PrescaleXic2LambdaKPiDD": 1.0, 
#        "PrescaleXic2LambdaKPiLL": 1.0, 
#        "PrescaleXic2LambdaPiKDD": 1.0, 
#        "PrescaleXic2LambdaPiKLL": 1.0, 
#        "PrescaleXic2LambdaPiPiDD": 1.0, 
#        "PrescaleXic2LambdaPiPiLL": 1.0, 
#        "ProbNNkMin": 0.1, 
#        "ProbNNpMin_L0_DD": 0.1, 
#        "ProbNNpMin_L0_LL": 0.1, 
#        "ProbNNpiMin": 0.1, 
#        "ProbNNpiMin_L0_DD": 0.1, 
#        "ProbNNpiMin_L0_LL": 0.1, 
#        "TrGhostProbMax": 0.3, 
#        "Xic_ADMASS_WIN": 90.0, 
#        "Xic_PT_min": 250.0, 
#        "Xic_P_min": 10000.0, 
#        "Xic_VCHI2VDOF_MAX_DD": 3.0, 
#        "Xic_VCHI2VDOF_MAX_LL": 3.0, 
#        "Xic_acosBPVDIRA_MAX_DD": 0.0316, 
#        "Xic_acosBPVDIRA_MAX_LL": 0.02, 
#        "p_L0_PT_LL_min": 100.0, 
#        "p_L0_P_LL_min": 2000.0, 
#        "pi_L0_PT_LL_min": 100.0, 
#        "pi_L0_P_LL_min": 2000.0
#    }, 
#    "STREAMS": [ "Charm" ], 
#    "WGs": [ "Charm" ]
#}

Hc2V03H = {
    "BUILDERTYPE": "StrippingHc2V03HConf", 
    "CONFIG": {
        "Bach_IPCHI2_MAX": 9, 
        "Bach_IPCHI2_MIN": 1, 
        "Bach_PT_MIN": 150.0, 
        "Bach_P_MIN": 2000.0, 
        "Comb_ADAMASS_WIN": 90.0, 
        "Comb_ADOCAMAX_MAX": 0.5, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "Lambda0_FDCHI2_MIN": 49, 
        "Lambda0_PT_MIN": 500.0, 
        "Lambda0_P_MIN": 10000.0, 
        "Lambda0_VCHI2VDOF_MAX": 12.0, 
        "LambdaMinFD_DD": 200.0, 
        "LambdaMinFD_LL": 25.0, 
        "Lambdac_ADMASS_WIN": 75.0, 
        "Lambdac_PVDispCut_DD": "(BPVVDCHI2 > 9.0)", 
        "Lambdac_PVDispCut_LL": "(BPVVDCHI2 > 16.0)", 
        "Lambdac_VCHI2VDOF_MAX_DD": 3.0, 
        "Lambdac_VCHI2VDOF_MAX_LL": 3.0, 
        "Lambdac_acosBPVDIRA_MAX_DD": 0.045, 
        "Lambdac_acosBPVDIRA_MAX_LL": 0.14, 
        "PostscaleLambdac2Lambda3PiDD": 1.0, 
        "PostscaleLambdac2Lambda3PiLL": 1.0, 
        "PostscaleLambdac2LambdaK2PiDD": 1.0, 
        "PostscaleLambdac2LambdaK2PiLL": 1.0, 
        "PostscaleXic2LambdaK2PiDD": 1.0, 
        "PostscaleXic2LambdaK2PiLL": 1.0, 
        "PrescaleLambdac2Lambda3PiDD": 1.0, 
        "PrescaleLambdac2Lambda3PiLL": 1.0, 
        "PrescaleLambdac2LambdaK2PiDD": 1.0, 
        "PrescaleLambdac2LambdaK2PiLL": 1.0, 
        "PrescaleXic2LambdaK2PiDD": 1.0, 
        "PrescaleXic2LambdaK2PiLL": 1.0, 
        "ProbNNpMin_DD": 0.0, 
        "ProbNNpMin_LL": 0.1, 
        "Xic_ADMASS_WIN": 75.0, 
        "Xic_PVDispCut_DD": "(BPVVDCHI2 > 9.0)", 
        "Xic_PVDispCut_LL": "(BPVVDCHI2 > 16.0)", 
        "Xic_VCHI2VDOF_MAX_DD": 3.0, 
        "Xic_VCHI2VDOF_MAX_LL": 3.0, 
        "Xic_acosBPVDIRA_MAX_DD": 0.045, 
        "Xic_acosBPVDIRA_MAX_LL": 0.14
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

Hc2V2H = {
    "BUILDERTYPE": "StrippingHc2V2HConf", 
    "CONFIG": {
        "Bach_PT_MIN": 150.0, 
        "Bach_P_MIN": 2000.0, 
        "Comb_ADAMASS_WIN": 120.0, 
        "Comb_ADOCAMAX_MAX": 0.5, 
        "Hc_ADMASS_WIN": 90.0, 
        "Hc_PVDispCut_DDD": "(BPVVDCHI2 >  9.0)", 
        "Hc_PVDispCut_DDL": "(BPVVDCHI2 > 16.0)", 
        "Hc_PVDispCut_LLL": "(BPVVDCHI2 > 16.0)", 
        "Hc_VCHI2VDOF_MAX_DDD": 12.0, 
        "Hc_VCHI2VDOF_MAX_DDL": 12.0, 
        "Hc_VCHI2VDOF_MAX_LLL": 12.0, 
        "Hc_acosBPVDIRA_MAX_DDD": 0.14, 
        "Hc_acosBPVDIRA_MAX_DDL": 0.045, 
        "Hc_acosBPVDIRA_MAX_LLL": 0.14, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "Lambda0_FDCHI2_MIN": 49, 
        "Lambda0_PT_MIN": 250.0, 
        "Lambda0_P_MIN": 2000.0, 
        "Lambda0_VCHI2VDOF_MAX": 12.0, 
        "LambdaMinFD_DD": 0.0, 
        "LambdaMinFD_LL": 25.0, 
        "PostscaleLambdac2XiKPiDDD": 1.0, 
        "PostscaleLambdac2XiKPiDDL": 1.0, 
        "PostscaleLambdac2XiKPiLLL": 1.0, 
        "PostscaleXic2XiPiPiDDD": 1.0, 
        "PostscaleXic2XiPiPiDDL": 1.0, 
        "PostscaleXic2XiPiPiLLL": 1.0, 
        "PrescaleLambdac2XiKPiDDD": 1.0, 
        "PrescaleLambdac2XiKPiDDL": 1.0, 
        "PrescaleLambdac2XiKPiLLL": 1.0, 
        "PrescaleXic2XiPiPiDDD": 1.0, 
        "PrescaleXic2XiPiPiDDL": 1.0, 
        "PrescaleXic2XiPiPiLLL": 1.0, 
        "ProbNNpMin_DD": 0.0, 
        "ProbNNpMin_LL": 0.1, 
        "Xim_FDCHI2_MIN": 16, 
        "Xim_PT_MIN": 250.0, 
        "Xim_P_MIN": 2000.0, 
        "Xim_VCHI2VDOF_MAX": 12.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

Hc2V3H = {
    "BUILDERTYPE": "StrippingHc2V3HConf", 
    "CONFIG": {
        "Bach_IPCHI2_MAX": 9, 
        "Bach_IPCHI2_MIN": 1, 
        "Bach_PT_MIN": 200.0, 
        "Bach_P_MIN": 2000.0, 
        "Comb_ADAMASS_WIN": 120.0, 
        "Comb_ADOCAMAX_MAX": 0.5, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "Lambda0_FDCHI2_MIN": 25, 
        "Lambda0_PT_MIN": 250.0, 
        "Lambda0_P_MIN": 2000.0, 
        "Lambda0_VCHI2VDOF_MAX": 12.0, 
        "PostscaleXic2XimPiPiPiDDD": 1.0, 
        "PostscaleXic2XimPiPiPiDDL": 1.0, 
        "PostscaleXic2XimPiPiPiLLL": 1.0, 
        "PrescaleXic2XimPiPiPiDDD": 1.0, 
        "PrescaleXic2XimPiPiPiDDL": 1.0, 
        "PrescaleXic2XimPiPiPiLLL": 1.0, 
        "ProbNNpMin_DD": 0.0, 
        "ProbNNpMin_LL": 0.1, 
        "Xic_ADMASS_WIN": 90.0, 
        "Xic_PVDispCut_DDD": "(BPVVDCHI2 > 9.0)", 
        "Xic_PVDispCut_DDL": "(BPVVDCHI2 > 9.0)", 
        "Xic_PVDispCut_LLL": "(BPVVDCHI2 > 16.0)", 
        "Xic_VCHI2VDOF_MAX_DDD": 5.0, 
        "Xic_VCHI2VDOF_MAX_DDL": 5.0, 
        "Xic_VCHI2VDOF_MAX_LLL": 5.0, 
        "Xic_acosBPVDIRA_MAX_DDD": 0.14, 
        "Xic_acosBPVDIRA_MAX_DDL": 0.14, 
        "Xic_acosBPVDIRA_MAX_LLL": 0.14, 
        "Xim_FDCHI2_MIN": 9, 
        "Xim_PT_MIN": 250.0, 
        "Xim_P_MIN": 2000.0, 
        "Xim_VCHI2VDOF_MAX": 12.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

Lambdac2LambdaEtaPi = {
    "BUILDERTYPE": "StrippingLambdac2LambdaEtaPi", 
    "CONFIG": {
        "GEC_nLongTrk": 160, 
        "GhostProb": 0.3, 
        "KaonP": 3000.0, 
        "KaonPIDK": 10.0, 
        "KaonPT": 250.0, 
        "Kaon_PIDKPIDpi_Min": 5.0, 
        "LambdaDDCutFDChi2": 100.0, 
        "LambdaDDCutMass": 50.0, 
        "LambdaDDPMin": 2000.0, 
        "LambdaDDPTMin": 500.0, 
        "LambdaLLCutFDChi2": 100.0, 
        "LambdaLLCutMass": 50.0, 
        "LambdaLLPMin": 2000.0, 
        "LambdaLLPTMin": 500.0, 
        "LambdaVertexChi2": 10.0, 
        "Lb_APT_Min": 1500.0, 
        "Lb_AP_Min": 20000.0, 
        "Lb_BPVDIRA_Min": 0.95, 
        "Lb_BPVVDCHI2_Min": 16, 
        "Lb_M_HalfWin": 200.0, 
        "Lb_VCHI2_Max": 30, 
        "Lc_ADOCAMAX_Max": 0.15, 
        "Lc_APT_Min": 1200.0, 
        "Lc_AP_Min": 15000.0, 
        "Lc_BPVDIRA_Min": 0.95, 
        "Lc_BPVVDCHI2_Min": 16.0, 
        "Lc_BPVVDZ_Min": 0.5, 
        "Lc_Daug_1of3_MIPCHI2DV_Min": 3.0, 
        "Lc_M_HalfWin": 200.0, 
        "Lc_VCHI2_Max": 30.0, 
        "MINIPCHI2": 3.0, 
        "MuonIPCHI2": 4.0, 
        "MuonP": 3000.0, 
        "MuonPIDmu": 0.0, 
        "MuonPT": 250.0, 
        "PionP": 3000.0, 
        "PionPIDK": 5.0, 
        "PionPT": 250.0, 
        "Pion_PIDpiPIDK_Min": 0.0, 
        "ProbNNk": 0.4, 
        "ProbNNp": 0.4, 
        "ProbNNpMin": 0.2, 
        "ProbNNpi": 0.4, 
        "ProbNNpiMax": 0.9, 
        "TRCHI2DOFMax": 3.0, 
        "TrGhostProbMax": 0.25, 
        "signalPrescale": 1.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

Lambdac2PEta = {
    "BUILDERTYPE": "StrippingLambdac2PEta", 
    "CONFIG": {
        "GEC_nLongTrk": 160, 
        "Lb_APT_Min": 1500.0, 
        "Lb_AP_Min": 20000.0, 
        "Lb_BPVDIRA_Min": 0.95, 
        "Lb_BPVVDCHI2_Min": 16, 
        "Lb_BPVVDZ_Min": 0.7, 
        "Lb_M_HalfWin": 200.0, 
        "Lb_VCHI2_Max": 30, 
        "Lc_ADOCAMAX_Max": 0.15, 
        "Lc_APT_Min": 1200.0, 
        "Lc_AP_Min": 15000.0, 
        "Lc_BPVDIRA_Min": 0.95, 
        "Lc_BPVVDCHI2_Min": 16.0, 
        "Lc_BPVVDZ_Min": 0.5, 
        "Lc_Daug_1of3_MIPCHI2DV_Min": 3.0, 
        "Lc_M_HalfWin": 200.0, 
        "Lc_VCHI2_Max": 30.0, 
        "MINIPCHI2": 3.0, 
        "PionP": 3000.0, 
        "PionPIDK": 5.0, 
        "PionPT": 250.0, 
        "ProbNNp": 0.4, 
        "ProbNNpi": 0.4, 
        "ProbNNpiMax": 0.9, 
        "ProtonP": 3000.0, 
        "ProtonPT": 500.0, 
        "Proton_PIDpPIDK_Min": 0.0, 
        "Proton_PIDpPIDpi_Min": 5.0, 
        "TRCHI2DOFMax": 3.0, 
        "TrGhostProbMax": 0.25, 
        "signalPrescale": 1.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

NeutralCBaryons = {
    "BUILDERTYPE": "StrippingNeutralCBaryonsConf", 
    "CONFIG": {
        "Bachelor_PT_MIN": 50.0, 
        "KaonPIDK": -5.0, 
        "LambdaDDMassWin": 5.7, 
        "LambdaDDMaxVZ": 2275.0, 
        "LambdaDDMinVZ": 400.0, 
        "LambdaDDVtxChi2Max": 5.0, 
        "LambdaDeltaZ_MIN": 5.0, 
        "LambdaLLMassWin": 5.7, 
        "LambdaLLMaxVZ": 400.0, 
        "LambdaLLMinDecayTime": 0.005, 
        "LambdaLLMinVZ": -100.0, 
        "LambdaLLVtxChi2Max": 5.0, 
        "LambdaPi_PT_MIN": 50.0, 
        "LambdaPr_PT_MIN": 50.0, 
        "PionPIDK": 10.0, 
        "ProbNNkMin": 0.1, 
        "ProbNNpMinDD": 0.05, 
        "ProbNNpMinLL": 0.1, 
        "ProtonPIDp": 5, 
        "ProtonPIDpK": -3, 
        "TRCHI2DOFMax": 3.0, 
        "TrGhostProbMax": 0.25, 
        "tight_KaonPIDK": 0.0, 
        "tight_KaonPIDpi": 5.0, 
        "tight_ProtonPIDp": 7, 
        "tight_ProtonPIDpK": 0.0, 
        "tight_ProtonPIDppi": 5.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

Xib2Xic0PiXic02pK = {
    "BUILDERTYPE": "StrippingXib2Xic0PiXic02pKConf", 
    "CONFIG": {
        "PionPIDK": 10.0, 
        "ProbNNkMin": 0.1, 
        "TRCHI2DOFMax": 3.0, 
        "TrGhostProbMax": 0.25, 
        "tight_KaonPIDK": 0.0, 
        "tight_KaonPIDpi": 5.0, 
        "tight_ProtonPIDp": 7, 
        "tight_ProtonPIDpK": 0.0, 
        "tight_ProtonPIDppi": 5.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

Xic0ToXiMuNu = {
    "BUILDERTYPE": "Xic0ToXiMuNuConf", 
    "CONFIG": {
        "RequiredRawEvents": [ "Velo" ], 
        "amass_ximu": "(in_range(1400*MeV,AMASS(),4500*MeV)) & ", 
        "bach_muon": {
            "filter": "(PT>200*MeV) & (P>3000*MeV) & (PROBNNmu>0.3) & (BPVIPCHI2()>4)", 
            "tes": "Phys/StdAllNoPIDsMuons/Particles"
        }, 
        "bach_pion": {
            "filter": "(PT > 120*MeV) & (P > 1000*MeV) & (MIPDV(PRIMARY)>0.01*mm) & (MIPCHI2DV(PRIMARY) > 9.0) & (PROBNNpi>0.03)", 
            "tes": "Phys/StdAllNoPIDsPions/Particles"
        }, 
        "descriptor_xi": [
            "[Xi- -> Lambda0 pi-]cc"
        ], 
        "descriptor_ximu": [
            "[Xi_c0 -> Xi- mu+]cc"
        ], 
        "down_pion": {
            "filter": "(P>1000*MeV) & (PT>150*MeV) & (PROBNNpi>0.03)", 
            "tes": "Phys/StdNoPIDsDownPions/Particles"
        }, 
        "lambda_dd": {
            "filter": "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>400*MeV) & (CHILDIP(1)<2*mm) & (MAXTREE('p+'==ABSID,P)>9*GeV) & (CHI2VXNDF<30) &\n                    (MAXTREE('p+'==ABSID,PT)>200*MeV) & (MAXTREE('pi+'==ABSID,PT)>150*MeV) & (VFASPF(VZ) >  300.*mm) & (VFASPF(VZ) < 2485.*mm)", 
            "tes": "Phys/StdLooseLambdaDD/Particles"
        }, 
        "lambda_ll": {
            "filter": "(ADMASS('Lambda0')<20*MeV) & (P>6*GeV) & (PT>200*MeV) & (BPVVDZ>8*mm) & (BPVLTIME() > 2.0*ps) & (CHI2VXNDF<20) &\n                    (MAXTREE('p+'==ABSID,MIPCHI2DV(PRIMARY))>9) & (MAXTREE('pi-'==ABSID,MIPCHI2DV(PRIMARY))>24) & \n                    (MAXTREE('p+'==ABSID,PT)>100*MeV) & (MAXTREE('p+'==ABSID,P)>5.0*GeV) & (VFASPF(VZ) > -100.*mm) & (VFASPF(VZ) <  640.*mm) & (BPVVDCHI2 > 100)", 
            "tes": "Phys/StdVeryLooseLambdaLL/Particles"
        }, 
        "xc0_ddd": {
            "comb_cut": "AHASCHILD( (ABSID == 'mu+') ) & (ADOCA(1,2)<5.5)", 
            "mother_cut": "(VFASPF(VCHI2/VDOF)<25) & (BPVVDCHI2>4) & (P>20*GeV) & (PT>200*MeV)"
        }, 
        "xc0_ddl": {
            "comb_cut": "AHASCHILD( (ABSID == 'mu+') ) & (ADOCA(1,2)<3.5)", 
            "mother_cut": "(VFASPF(VCHI2/VDOF)<25) & (BPVVDCHI2>4) & (P>20*GeV) & (PT>200*MeV) "
        }, 
        "xc0_lll": {
            "comb_cut": "AHASCHILD( (ABSID == 'mu+') ) & (ADOCA(1,2)<1.5)", 
            "mother_cut": "(VFASPF(VCHI2/VDOF)<25) & (BPVVDCHI2>4) & (P>12*GeV) & (PT>200*MeV)"
        }, 
        "xi_ddd": {
            "comb_cut": "(ASUM(PT)>0.5*GeV) & (ADAMASS('Xi-')<30*MeV) & (ACHILD(2,MIPCHI2DV(PRIMARY))>20) & (ADOCA(1,2)<5*mm) & (ACHI2DOCA(1,2)<12)", 
            "mother_cut": "(ADMASS('Xi-')<25*MeV) & (VFASPF(VCHI2PDOF) < 10.0) &  (BPVVDCHI2>10) & (BPVLTIME() > 2.0*picosecond) &\n            (P>15*GeV) & (PT>600*MeV)"
        }, 
        "xi_ddl": {
            "comb_cut": "(ASUM(PT)>0.5*GeV) & (ADAMASS('Xi-')<30*MeV) & (ADOCA(1,2)<2.5*mm) & (ACHI2DOCA(1,2)<12) ", 
            "mother_cut": "(ADMASS('Xi-')<25*MeV) & (VFASPF(VCHI2PDOF) < 10.0) & (BPVVDCHI2>20) & (BPVLTIME() > 2.0*picosecond) &\n            (P>15*GeV) & (PT>600*MeV)"
        }, 
        "xi_lll": {
            "comb_cut": "(ASUM(PT)>0.5*GeV) & (ADAMASS('Xi-')<30*MeV) & (ACHILD(2,MIPCHI2DV(PRIMARY))>9) & (ADOCA(1,2)<0.5*mm) & (ACHI2DOCA(1,2)<20)", 
            "mother_cut": "(ADMASS('Xi-')<25*MeV) & (VFASPF(VCHI2PDOF) < 10.0) & (BPVVDCHI2>15) & (BPVLTIME() > 2.0*picosecond) & \n            (P>8*GeV) & (PT>400*MeV) "
        }
    }, 
    "STREAMS": {
        "CharmCompleteEvent": [
            "StrippingXic0ToXiMuNu_LLLLine", 
            "StrippingXic0ToXiMuNu_DDLLine", 
            "StrippingXic0ToXiMuNu_DDDLine"
        ]
    }, 
    "WGs": [ "Charm" ]
}

Xic0ToXiMuNu_WS = {
    "BUILDERTYPE": "Xic0ToXiMuNu_WSConf", 
    "CONFIG": {
        "RequiredRawEvents": [ "Velo" ], 
        "amass_ximu": "(in_range(1400*MeV,AMASS(),4500*MeV)) & ", 
        "bach_muon": {
            "filter": "(PT>200*MeV) & (P>3000*MeV) & (PROBNNmu>0.3) & (BPVIPCHI2()>4)", 
            "tes": "Phys/StdAllNoPIDsMuons/Particles"
        }, 
        "bach_pion": {
            "filter": "(PT > 120*MeV) & (P > 1000*MeV) & (MIPDV(PRIMARY)>0.01*mm) & (MIPCHI2DV(PRIMARY) > 9.0) & (PROBNNpi>0.03)", 
            "tes": "Phys/StdAllNoPIDsPions/Particles"
        }, 
        "descriptor_xi": [
            "[Xi- -> Lambda0 pi-]cc"
        ], 
        "descriptor_ximu": [
            "[Xi_c0 -> Xi- mu-]cc"
        ], 
        "down_pion": {
            "filter": "(P>1000*MeV) & (PT>150*MeV) & (PROBNNpi>0.03)", 
            "tes": "Phys/StdNoPIDsDownPions/Particles"
        }, 
        "lambda_dd": {
            "filter": "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>400*MeV) & (CHILDIP(1)<2*mm) & (MAXTREE('p+'==ABSID,P)>9*GeV) & (CHI2VXNDF<30) &\n                    (MAXTREE('p+'==ABSID,PT)>200*MeV) & (MAXTREE('pi+'==ABSID,PT)>150*MeV) & (VFASPF(VZ) >  300.*mm) & (VFASPF(VZ) < 2485.*mm)", 
            "tes": "Phys/StdLooseLambdaDD/Particles"
        }, 
        "lambda_ll": {
            "filter": "(ADMASS('Lambda0')<20*MeV) & (P>6*GeV) & (PT>200*MeV) & (BPVVDZ>8*mm) & (BPVLTIME() > 2.0*ps) & (CHI2VXNDF<20) &\n                    (MAXTREE('p+'==ABSID,MIPCHI2DV(PRIMARY))>9) & (MAXTREE('pi-'==ABSID,MIPCHI2DV(PRIMARY))>24) & \n                    (MAXTREE('p+'==ABSID,PT)>100*MeV) & (MAXTREE('p+'==ABSID,P)>5.0*GeV) & (VFASPF(VZ) > -100.*mm) & (VFASPF(VZ) <  640.*mm) & (BPVVDCHI2 > 100)", 
            "tes": "Phys/StdVeryLooseLambdaLL/Particles"
        }, 
        "xc0_ddd": {
            "comb_cut": "AHASCHILD( (ABSID == 'mu+') ) & (ADOCA(1,2)<5.5)", 
            "mother_cut": "(VFASPF(VCHI2/VDOF)<25) & (BPVVDCHI2>4) & (P>20*GeV) & (PT>200*MeV)"
        }, 
        "xc0_ddl": {
            "comb_cut": "AHASCHILD( (ABSID == 'mu+') ) & (ADOCA(1,2)<3.5)", 
            "mother_cut": "(VFASPF(VCHI2/VDOF)<25) & (BPVVDCHI2>4) & (P>20*GeV) & (PT>200*MeV) "
        }, 
        "xc0_lll": {
            "comb_cut": "AHASCHILD( (ABSID == 'mu+') ) & (ADOCA(1,2)<1.5)", 
            "mother_cut": "(VFASPF(VCHI2/VDOF)<25) & (BPVVDCHI2>4) & (P>12*GeV) & (PT>200*MeV)"
        }, 
        "xi_ddd": {
            "comb_cut": "(ASUM(PT)>0.5*GeV) & (ADAMASS('Xi-')<30*MeV) & (ACHILD(2,MIPCHI2DV(PRIMARY))>20) & (ADOCA(1,2)<5*mm) & (ACHI2DOCA(1,2)<12)", 
            "mother_cut": "(ADMASS('Xi-')<25*MeV) & (VFASPF(VCHI2PDOF) < 10.0) &  (BPVVDCHI2>10) & (BPVLTIME() > 2.0*picosecond) &\n            (P>15*GeV) & (PT>600*MeV)"
        }, 
        "xi_ddl": {
            "comb_cut": "(ASUM(PT)>0.5*GeV) & (ADAMASS('Xi-')<30*MeV) & (ADOCA(1,2)<2.5*mm) & (ACHI2DOCA(1,2)<12) ", 
            "mother_cut": "(ADMASS('Xi-')<25*MeV) & (VFASPF(VCHI2PDOF) < 10.0) & (BPVVDCHI2>20) & (BPVLTIME() > 2.0*picosecond) &\n            (P>15*GeV) & (PT>600*MeV)"
        }, 
        "xi_lll": {
            "comb_cut": "(ASUM(PT)>0.5*GeV) & (ADAMASS('Xi-')<30*MeV) & (ACHILD(2,MIPCHI2DV(PRIMARY))>9) & (ADOCA(1,2)<0.5*mm) & (ACHI2DOCA(1,2)<20)", 
            "mother_cut": "(ADMASS('Xi-')<25*MeV) & (VFASPF(VCHI2PDOF) < 10.0) & (BPVVDCHI2>15) & (BPVLTIME() > 2.0*picosecond) & \n            (P>8*GeV) & (PT>400*MeV) "
        }
    }, 
    "STREAMS": {
        "CharmCompleteEvent": [
            "StrippingXic0ToXiMuNu_WS_LLLLine", 
            "StrippingXic0ToXiMuNu_WS_DDLLine", 
            "StrippingXic0ToXiMuNu_WS_DDDLine"
        ]
    }, 
    "WGs": [ "Charm" ]
}

Xic2PHH = {
    "BUILDERTYPE": "StrippingXic2PHH", 
    "CONFIG": {
        "BPVIPCHI2_Phi": 4, 
        "GEC_nLongTrk": 250, 
        "GhostProb": 0.3, 
        "KSCutDIRA_DD": 0.999, 
        "KSCutDIRA_LL": 0.999, 
        "KSCutFDChi2_DD": 5, 
        "KSCutFDChi2_LL": 5, 
        "KSCutMass_DD": 50.0, 
        "KSCutMass_LL": 35.0, 
        "KaonP": 2000.0, 
        "KaonPIDK": 10.0, 
        "KaonPT": 200.0, 
        "Kaon_PIDKPIDpi_Min": 5.0, 
        "MINIPCHI2": 4.0, 
        "MaxDz_DD": 9999.0, 
        "MaxDz_LL": 9999.0, 
        "MaxKsVCHI2NDOF_DD": 20.0, 
        "MaxKsVCHI2NDOF_LL": 20.0, 
        "MaxPhiEta": 5.0, 
        "MinDz_DD": 250.0, 
        "MinDz_LL": 0.0, 
        "MinKsIpChi2_DD": 3, 
        "MinKsIpChi2_LL": 3, 
        "MinKsPT_DD": 200.0, 
        "MinKsPT_LL": 200.0, 
        "MinPhiEta": 2.0, 
        "MinPhiP": 3000.0, 
        "MinPhiPT": 200.0, 
        "PhiCutMass": 20.0, 
        "PionP": 3000.0, 
        "PionPIDK": 10.0, 
        "PionPT": 500.0, 
        "Pion_PIDpiPIDK_Min": 0.0, 
        "ProbNNk": 0.4, 
        "ProbNNp": 0.4, 
        "ProbNNpMin": 0.2, 
        "ProbNNpi": 0.5, 
        "ProbNNpiMax": 0.95, 
        "ProtonP": 3000.0, 
        "ProtonPT": 500.0, 
        "Proton_PIDpPIDK_Min": 0.0, 
        "Proton_PIDpPIDpi_Min": 5.0, 
        "TRCHI2DOFMax": 3.0, 
        "TrGhostProbMax": 0.25, 
        "Xic_ADAMASS_HalfWin": 170.0, 
        "Xic_ADMASS_HalfWin": 120.0, 
        "Xic_AM_Max": 2550.0, 
        "Xic_BPVDIRA_Min": 0.9, 
        "Xic_BPVVDCHI2_Min": 25.0, 
        "pKK_AM_Min": 2380.0, 
        "ppipi_AM_Min": 2380.0, 
        "signalPrescaleViaXicPKK": 1.0, 
        "signalPrescaleViaXicPKS0DD": 1.0, 
        "signalPrescaleViaXicPKS0LL": 1.0, 
        "signalPrescaleViaXicPPhi2KK": 1.0, 
        "signalPrescaleViaXicPPiPi": 1.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

