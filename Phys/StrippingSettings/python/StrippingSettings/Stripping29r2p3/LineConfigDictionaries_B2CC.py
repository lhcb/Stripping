###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

################################################################################
##                          S T R I P P I N G  29r2p3                         ##
##                                                                            ##
##  Configuration for B2CC WG                                                 ##
##  Contact person: Jie Wu               (j.wu@cern.ch)                       ##
################################################################################

######################################################################
## Dimuon (FullDST) :
##    StrippingB2JpsiX_eta2pipipi0Line 
##    StrippingB2JpsiX_omega2pipipi0Line
##    StrippingB2JpsiX_etaprime2pipipi0Line
##    StrippingB2JpsiX_phi2pipipi0Line  
## -------------------------------------------------------------------
## Lines defined in StrippingB2JpsiX.py
## Authors: Ganrong Wang
## Last changes made by Ganrong Wang
######################################################################
B2JpsiX = {
    "BUILDERTYPE": "B2JpsiXConf", 
    "CONFIG": {
        "BMassWindow": 1500, 
        "B_bpvdira": 0.9995, 
        "B_bpvipchi2": 25, 
        "B_bpvvdchi2": 64, 
        "B_mass_max": 6500, 
        "B_mass_min": 4500, 
        "B_mipchi2": 9, 
        "B_vchi2vdof": 10, 
        "Pi0_mer_pt": 300.0, 
        "Pi0_res_cl": -1000, 
        "Pi0_res_pt": 300.0, 
        "Pion_Trghp": 0.5, 
        "Pion_minipchi2": 6.0, 
        "Pion_pidk": 5, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "X_adamass": 200, 
        "X_apt": 1000, 
        "X_bpvdira": 0.95, 
        "X_bpvvdchi2": 25, 
        "X_bpvvdz": 0, 
        "X_vfaspf": 9, 
        "jpsi_bpvdls_max": 3, 
        "jpsi_bpvdls_min": -3, 
        "jpsi_mintree_pidmu": 0.0, 
        "jpsi_mintree_pt": 500.0, 
        "jpsi_mm_max": 3196.916, 
        "jpsi_mm_min": 2996.916
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "B2CC" ]
}

#############################################################################################
## Leptonic (MicroDST) :
##    StrippingBetaS_NoPIDBs2JpsiKstarWideLine
## ----------------------------------------------------------------------------
## Lines defined in StrippingBetaS_NoPIDBs2JpsiKstarWide.py
## Authors: Carlos Vazquez Sierra, Jie Wu
## Last changes made by Jie Wu
#############################################################################################
BetaS_NoPID = {
    "BUILDERTYPE": "B2JpsiXforBeta_s_NoPID_Conf", 
    "CONFIG": {
        "JpsiMassWindow": 80, 
        "VCHI2PDOF": 10
    }, 
    "STREAMS": {
        "Leptonic": [ "StrippingBetaS_NoPIDBs2JpsiKstarWideLine" ]
    }, 
    "WGs": [ "B2CC" ]
}
