###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

################################################################################
##                          S T R I P P I N G  29r2p3                         ##
##                                                                            ##
##  Configuration for QEE WG                                                  ##
##  Contact person: Xiaolin Wang (xiaolin.wang@cern.ch)                       ##
################################################################################

ALP2TauTau = {
    "BUILDERTYPE": "ALP2TauTauConf", 
    "CONFIG": {
        "HTAU_DOCA": 0.4, 
        "HTAU_DOCA_FH": 0.1, 
        "HTAU_ETA_DAUG_MAX": 5, 
        "HTAU_ETA_DAUG_MIN": 1.5, 
        "HTAU_ETA_MAX": 4.5, 
        "HTAU_ETA_MIN": 2, 
        "HTAU_FDMAX": 1.0, 
        "HTAU_FDMAX_FH": 0.25, 
        "HTAU_IPMAX": 0.2, 
        "HTAU_IPMAX_FH": 0.1, 
        "HTAU_MASS_HIGH": 1000000.0, 
        "HTAU_MASS_LOW": 0.0, 
        "HTAU_PT_DAUG_MAX": 7500.0, 
        "HTAU_PT_DAUG_MIN": 5000.0, 
        "HTAU_PT_MAX": 150000.0, 
        "HTAU_PT_MIN": 15000.0, 
        "HTau3HTau3H_LinePostscale": 1, 
        "HTau3HTau3H_LinePrescale": 1, 
        "HTau3HTauE_LinePostscale": 1, 
        "HTau3HTauE_LinePrescale": 1, 
        "HTau3HTauMu_LinePostscale": 1, 
        "HTau3HTauMu_LinePrescale": 1, 
        "HTauMuTauE_LinePostscale": 1, 
        "HTauMuTauE_LinePrescale": 1, 
        "SpdMult": "6000", 
        "TAUCOMB_DOCA": 0.05, 
        "TAUE_IP": 0.03, 
        "TAUE_ISO": 0.99, 
        "TAUE_P": 10000.0, 
        "TAUE_PROBNN": 0.25, 
        "TAUE_PROBNN_EMU": 0.5, 
        "TAUE_PT": 3500.0, 
        "TAUMU_IP": 0.01, 
        "TAUMU_ISO": 0.99, 
        "TAUMU_P": 10000.0, 
        "TAUMU_PROBNN": 0.8, 
        "TAUMU_PT": 3500.0, 
        "TAUPI_MINIP": 0.01, 
        "TAUPI_MINP": 2000.0, 
        "TAUPI_MINPID": 0.0, 
        "TAUPI_MINPT": 1000.0, 
        "TAUPI_MINPT_FH": 500.0, 
        "TAU_IPMAX": 0.2, 
        "TAU_IPMAX_FH": 0.1, 
        "TAU_ISO": 0.15, 
        "TAU_MASS_HIGH": 1700.0, 
        "TAU_MASS_LOW": 0.0, 
        "TAU_MCORR_MAX": 2500.0, 
        "TAU_MCORR_MIN": 1200.0, 
        "TAU_PT": 10000.0, 
        "TAU_PT_FH": 2500.0, 
        "TAU_RHO_MAX_FH": 5.0, 
        "TAU_RHO_MIN_FH": 0.1
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}



LLP2Hadrons = {
    "BUILDERTYPE": "LLP2HadronsConf", 
    "CONFIG": {
        "Common": {
            "CommonTrackCuts": "(TRGHOSTPROB<0.3) & (P>3.*GeV) & (BPVIPCHI2() > 9.)"
        }, 
        "DD": {
            "Down": {
                "CombCuts": "(AM>3.0*GeV)", 
                "DCombCuts": "in_range(1.0*GeV, AM, 3.*GeV)", 
                "DMomCuts": "(ADMASS('D+') < 150.*MeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 9.)", 
                "MomCuts": "(M>3.0*GeV) & (PT>1.*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>3) & (VFASPF(VZ)>200*mm)", 
                "TrackCuts": "(PT>500*MeV)"
            }, 
            "Long": {
                "CombCuts": "(AM>3.0*GeV)", 
                "DCombCuts": "in_range(1.0*GeV, AM, 3.*GeV)", 
                "DMomCuts": "(ADMASS('D+') < 80.*MeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 4.)", 
                "K_id": "(PROBNNk  > 0.2)", 
                "MomCuts": "(M>3.0*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (PT>1.*GeV) & (BPVDLS>5)", 
                "Pi_id": "(PROBNNpi > 0.2)", 
                "TrackCuts": "(PT > 500*MeV)"
            }
        }, 
        "EtaPiPi": {
            "Down": {
                "CombCuts": "AALL", 
                "EtaCombCuts": "AALL", 
                "EtaMomCuts": "(ADMASS('eta') < 100.*MeV) & (VFASPF(VCHI2PDOF) < 9.)", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5) & (VFASPF(VZ)>200*mm) & (M<2.5*GeV)", 
                "Pi0_cuts": "(PT > 600*MeV)", 
                "TrackCuts": "(PT>500*MeV) & (PROBNNpi > 0.2)"
            }, 
            "Long": {
                "CombCuts": "AALL", 
                "EtaCombCuts": "(AM<1*GeV)", 
                "EtaMomCuts": "(ADMASS('eta') < 100.*MeV) & (VFASPF(VCHI2PDOF) < 9.)", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (PT>2.*GeV) & (BPVDLS>5) & (BPVIPCHI2() < 9.) & (M<2.5*GeV) & (BPVLTIME()>1.5*picosecond)", 
                "Pi0_cuts": "(PT>600*MeV)", 
                "Pi_id": "(PROBNNpi > 0.4)", 
                "TrackCuts": "(PT>500*MeV)"
            }
        }, 
        "EtaPiPi_gg": {
            "Down": {
                "CombCuts": "AALL", 
                "EtaCuts": "(CHILD(CL,1) > 0.6) & (CHILD(CL,2) > 0.6) & (CHILD(PT,1) > 0.8*GeV) & (CHILD(PT,2) > 0.8*GeV) & (PT>1.*GeV) & in_range(400,M,700)", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5) & (VFASPF(VZ)>200*mm) & (M<2.5*GeV) & (BPVIPCHI2() < 9.)", 
                "TrackCuts": "(PT>500*MeV)"
            }, 
            "Long": {
                "CombCuts": "AALL", 
                "EtaCuts": "(CHILD(CL,1) > 0.6) & (CHILD(CL,2) > 0.6) & (CHILD(PT,1) > 0.8*GeV) & (CHILD(PT,2) > 0.8*GeV) & (PT>1.5*GeV) & in_range(400,M,700)", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (PT>2.*GeV) & (BPVDLS>5) & (BPVIPCHI2() < 9.) & (M<2.5*GeV) & (BPVLTIME()>1.5*picosecond)", 
                "Pi_id": "(PROBNNpi > 0.4)", 
                "TrackCuts": "(PT>500*MeV)"
            }
        }, 
        "KsKpi_DD": {
            "Down": {
                "CombCuts": "AM>1.*GeV", 
                "Ks_MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (ADMASS('KS0') < 50.*MeV) & (BPVIPCHI2() > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)", 
                "Ks_PiCuts": "(MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5)  & (VFASPF(VZ)>200*mm) & (BPVIPCHI2() < 9.)", 
                "TrackCuts": "(PT>700*MeV)"
            }, 
            "Long": {
                "CombCuts": "in_range(1.0*GeV, AM, 12.2*GeV)", 
                "K_id": "(PROBNNk  > 0.4)", 
                "Ks_MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (ADMASS('KS0') < 30.*MeV) & (BPVIPCHI2() > 9.)", 
                "Ks_PiCuts": "(MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, MIPCHI2DV(PRIMARY)) > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)", 
                "MomCuts": "in_range(1.0*GeV, M, 12.0*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5) & (PT>2.*GeV) & (BPVIPCHI2() < 9.) & (BPVLTIME()>1.*picosecond)", 
                "Pi_id": "(PROBNNpi > 0.4)", 
                "TrackCuts": "(PT > 700*MeV)"
            }
        }, 
        "KsKpi_LL": {
            "Long": {
                "CombCuts": "in_range(1.0*GeV, AM, 12.2*GeV)", 
                "K_id": "(PROBNNk  > 0.4)", 
                "Ks_MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 9.) & (BPVDLS>5) & (ADMASS('KS0') < 30.*MeV)", 
                "Ks_PiCuts": "(MINTREE('pi-'==ABSID, PROBNNpi) > 0.4) & (MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, MIPCHI2DV(PRIMARY)) > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)", 
                "MomCuts": "in_range(1.0*GeV, M, 12.0*GeV) & (VFASPF(VCHI2PDOF)<9.) & (BPVDLS>10) & (PT>2.*GeV) & (BPVIPCHI2()<9.) & (BPVLTIME()>1.5*picosecond)", 
                "Pi_id": "(PROBNNpi > 0.4)", 
                "TrackCuts": "(PT > 700*MeV)"
            }
        }, 
        "KstKst": {
            "Down": {
                "CombCuts": "AALL", 
                "K_id": "(PROBNNk  > 0.1)", 
                "KstCombCuts": "(ADAMASS('K*(892)0')<300*MeV)", 
                "KstMomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (in_range( PDGM('K*(892)0') - 120*MeV , M , PDGM('K*(892)0') + 120*MeV ))", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS > 5)  & (VFASPF(VZ)>200*mm) & (PT>2.*GeV)", 
                "Pi_id": "(PROBNNpi > 0.1)", 
                "TrackCuts": "(PT>500*MeV)"
            }, 
            "Long": {
                "CombCuts": "AALL", 
                "K_id": "(PROBNNk  > 0.4)", 
                "KstCombCuts": "(ADAMASS('K*(892)0')<300*MeV)", 
                "KstMomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (in_range( PDGM('K*(892)0') - 120*MeV , M , PDGM('K*(892)0') + 120*MeV ))", 
                "MomCuts": "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS > 10) & (PT>2.*GeV) & (BPVIPCHI2() < 9.)", 
                "Pi_id": "(PROBNNpi > 0.4)", 
                "TrackCuts": "(PT>500*MeV)"
            }
        }, 
        "Prescales": {
            "DD": 1.0, 
            "EtaPiPi": 1.0, 
            "EtaPiPi_gg": 1.0, 
            "KsKpi": 1.0, 
            "KstKst": 1.0
        }
    }, 
    "STREAMS": {
        "BhadronCompleteEvent": [
            "StrippingLLP2Hadrons_KstKst_LongLine", 
            "StrippingLLP2Hadrons_KsKpi_LL_LongLine", 
            "StrippingLLP2Hadrons_KsKpi_DD_LongLine", 
            "StrippingLLP2Hadrons_DpDm_LongLine", 
            "StrippingLLP2Hadrons_DpDz_LongLine", 
            "StrippingLLP2Hadrons_DpDp_LongLine", 
            "StrippingLLP2Hadrons_DzDz_LongLine", 
            "StrippingLLP2Hadrons_DzDzbar_LongLine", 
            "StrippingLLP2Hadrons_EtaPiPi_LongLine", 
            "StrippingLLP2Hadrons_EtaPiPi_gg_LongLine", 
            "StrippingLLP2Hadrons_KstKst_DownLine", 
            "StrippingLLP2Hadrons_KsKpi_DD_DownLine", 
            "StrippingLLP2Hadrons_DpDm_DownLine", 
            "StrippingLLP2Hadrons_DpDz_DownLine", 
            "StrippingLLP2Hadrons_DpDp_DownLine", 
            "StrippingLLP2Hadrons_DzDz_DownLine", 
            "StrippingLLP2Hadrons_DzDzbar_DownLine", 
            "StrippingLLP2Hadrons_EtaPiPi_DownLine", 
            "StrippingLLP2Hadrons_EtaPiPi_gg_DownLine"
        ]
    }, 
    "WGs": [ "QEE" ]
}

LambdaDecaysDM = {
    "BUILDERTYPE": "LambdaDecaysDMConf", 
    "CONFIG": {
        "Common": {
            "checkPV": False
        }, 
        "StrippingDMLambda1520Line": {
            "Lambda1520_Daug_PT": 500.0, 
            "Lambda1520_FDChi2": 45, 
            "Lambda1520_GhostProb": 0.3, 
            "Lambda1520_IPChi2": 25, 
            "Lambda1520_M_Max": 1580, 
            "Lambda1520_M_Min": 1460, 
            "Lambda1520_MaxDoca": 0.1, 
            "Lambda1520_PT": 1000.0, 
            "Lambda1520_ProbNNK": 0.5, 
            "Lambda1520_ProbNNp": 0.8, 
            "Lambda1520_VChi2": 2, 
            "Lambda1520_iso": 0.85, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.1
        }, 
        "StrippingDMLambda2595Line": {
            "K_PT": 250.0, 
            "K_ProbNNghost": 0.1, 
            "K_ProbNNk": 0.5, 
            "Lambda2595_DOCA": 1.0, 
            "Lambda2595_FDCHI2": 5, 
            "Lambda2595_ISO": 0.6, 
            "Lambda2595_M_Max": 2645.0, 
            "Lambda2595_M_Min": 2545.0, 
            "Lambda2595_PT": 1500.0, 
            "Lambda2595_VCHI2": 4, 
            "Lambda_DOCA": 1.0, 
            "Lambda_FDCHI2": 50, 
            "Lambda_M_Max": 2336.0, 
            "Lambda_M_Min": 2236.0, 
            "Lambda_PT": 1500.0, 
            "Lambda_VCHI2": 4, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.6, 
            "p_PT": 250.0, 
            "p_ProbNNghost": 0.1, 
            "p_ProbNNp": 0.5, 
            "pi_lambda2595_IPchi2": 0.5, 
            "pi_lambda2595_PT": 100.0, 
            "pi_lambda2595_ProbNNghost": 0.1, 
            "pi_lambdac_PT": 250.0, 
            "pi_lambdac_ProbNNghost": 0.1
        }, 
        "StrippingDMLambdaToDKLine": {
            "D_DOCA": 0.4, 
            "D_FDCHI2": 50, 
            "D_M_Max": 1900.0, 
            "D_M_Min": 1840.0, 
            "D_PT": 2000.0, 
            "D_VCHI2": 6, 
            "KD_IPchi2": 20, 
            "KD_PT": 250.0, 
            "KD_ProbNNK": 0.7, 
            "KD_ProbNNghost": 0.2, 
            "K_PT": 250.0, 
            "K_ProbNNghost": 0.1, 
            "K_ProbNNk": 0.8, 
            "LambdaToDK_DOCA": 0.5, 
            "LambdaToDK_DOCACHI2": 8, 
            "LambdaToDK_FDCHI2": 30, 
            "LambdaToDK_IPCHI2": 25, 
            "LambdaToDK_ISO": 0.6, 
            "LambdaToDK_M_Max": 5700.0, 
            "LambdaToDK_M_Min": 1100.0, 
            "LambdaToDK_PT": 2000.0, 
            "LambdaToDK_VCHI2": 8, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.5, 
            "pi_PT": 250.0, 
            "pi_ProbNNghost": 0.1, 
            "pi_ProbNNpi": 0.8
        }, 
        "StrippingDMLambdaToDPiLine": {
            "D_DOCA": 0.4, 
            "D_FDCHI2": 50, 
            "D_M_Max": 1900.0, 
            "D_M_Min": 1840.0, 
            "D_PT": 2000.0, 
            "D_VCHI2": 6, 
            "K_PT": 800.0, 
            "K_ProbNNghost": 0.1, 
            "K_ProbNNk": 0.9, 
            "LambdaToDPi_DOCA": 0.3, 
            "LambdaToDPi_DOCACHI2": 7, 
            "LambdaToDPi_FDCHI2": 30, 
            "LambdaToDPi_IPCHI2": 20, 
            "LambdaToDPi_ISO": 0.6, 
            "LambdaToDPi_M_Max": 5700.0, 
            "LambdaToDPi_M_Min": 1100.0, 
            "LambdaToDPi_PT": 3000.0, 
            "LambdaToDPi_VCHI2": 8, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.3, 
            "piD_IPchi2": 20, 
            "piD_PT": 400.0, 
            "piD_ProbNNghost": 0.2, 
            "piD_ProbNNpi": 0.8, 
            "pi_PT": 800.0, 
            "pi_ProbNNghost": 0.1, 
            "pi_ProbNNpi": 0.8
        }, 
        "StrippingDMLambdaToKPiLine": {
            "LambdaToKPi_DOCAChi2": 4, 
            "LambdaToKPi_Daug_IPChi2": 20, 
            "LambdaToKPi_Daug_IP_max": 3.0, 
            "LambdaToKPi_Daug_IP_min": 0.1, 
            "LambdaToKPi_Daug_PT": 800.0, 
            "LambdaToKPi_FD": 5, 
            "LambdaToKPi_FDChi2": 40, 
            "LambdaToKPi_GhostProb": 0.1, 
            "LambdaToKPi_IPChi2": 15, 
            "LambdaToKPi_IPdivFD": 0.1, 
            "LambdaToKPi_M_Max": 5700, 
            "LambdaToKPi_M_Min": 1100, 
            "LambdaToKPi_MaxDoca": 0.1, 
            "LambdaToKPi_PT": 1000.0, 
            "LambdaToKPi_ProbNNK": 0.9, 
            "LambdaToKPi_ProbNNpi": 0.9, 
            "LambdaToKPi_VChi2": 4, 
            "LambdaToKPi_iso": 0.9, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.1
        }, 
        "StrippingDMLambdaToPiPiLine": {
            "LambdaToPiPi_DOCAChi2": 4, 
            "LambdaToPiPi_Daug_IPChi2": 12, 
            "LambdaToPiPi_Daug_IP_max": 3.0, 
            "LambdaToPiPi_Daug_IP_min": 0.1, 
            "LambdaToPiPi_Daug_PT": 1000.0, 
            "LambdaToPiPi_FD": 5.5, 
            "LambdaToPiPi_FDChi2": 10, 
            "LambdaToPiPi_GhostProb": 0.1, 
            "LambdaToPiPi_IPChi2": 10, 
            "LambdaToPiPi_IPdivFD": 0.1, 
            "LambdaToPiPi_M_Max": 5500, 
            "LambdaToPiPi_M_Min": 1100, 
            "LambdaToPiPi_MaxDoca": 0.05, 
            "LambdaToPiPi_PT": 1300.0, 
            "LambdaToPiPi_ProbNNpi": 0.9, 
            "LambdaToPiPi_VChi2": 4, 
            "LambdaToPiPi_iso": 0.95, 
            "Postscale": 1.0, 
            "Postscale_control": 1.0, 
            "Prescale": 1.0, 
            "Prescale_control": 0.1
        }
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

