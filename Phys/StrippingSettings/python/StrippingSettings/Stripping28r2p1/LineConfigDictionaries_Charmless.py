###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


Bu2KSh = {
    'WGs'  : ['BnoC'],
    'BUILDERTYPE' : 'Bu2KShConf',
    'CONFIG' : {'Trk_Chi2'                : 3.0,
                'Trk_GhostProb'           : 0.5,
                'KS_DD_MassWindow'        : 30.0,
                'KS_DD_VtxChi2'           : 10.0,
                'KS_DD_FDChi2'            : 50.0,
                'KS_DD_Pmin'              : 8000.0,
                'KS_DD_Ptmin'             : 1000.0,
                'KS_LL_MassWindow'        : 15.0,
                'KS_LL_VtxChi2'           : 10.0,
                'KS_LL_FDChi2'            : 100.0,
                'Bach_Ptmin'              : 1000.0,
                'B_Mlow'                  : 500.0,
                'B_Mhigh'                 : 1500.0,
                'B_Pmin'                  : 25000.0,
                'BDaug_MaxPT_IP'          : 0.05,
                'BDaug_DD_PTsum'          : 4000.0,
                'BDaug_LL_PTsum'          : 4000.0,
                'B_VtxChi2'               : 6.0,
                'B_Dira'                  : 0.9995,
                'B_DD_IPCHI2wrtPV'        : 10.0,
                'B_LL_IPCHI2wrtPV'        : 10.0,
                'B_FDwrtPV'               : 1.0,
                'B_DD_FDChi2'             : 50.0,
                'B_LL_FDChi2'             : 50.0,
                'GEC_MaxTracks'           : 250,
                # 2012 Trigger                                                                                                                  
                #'HLT1Dec'                  : 'Hlt1TrackAllL0Decision',                                                                          
                #'HLT2Dec'                  : 'Hlt2Topo[234]Body.*Decision',                                                                     
                # 2015 Triggers                                                                                                                 
                'HLT1Dec'                  : 'Hlt1(Two)?TrackMVADecision',
                'HLT2Dec'                  : 'Hlt2Topo[234]BodyDecision',
                'Prescale'                 : 1.0,
                'Postscale'                : 1.0,
                'RelatedInfoTools' : [    { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 1.7,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar17'
                                            },
                                          { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 1.5,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar15'
                                            },
                                          { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 1.0,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar10'
                                            },
                                          { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 0.8,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar08'
                                            },
                                          { "Type" : "RelInfoVertexIsolation",
                                            "Location" : "VtxIsolationVar"
                                            }
                                          ]
                },
    'STREAMS' : ['Bhadron']
    }








B2Lambdappp = {
    'WGs'  : ['BnoC'],
    'BUILDERTYPE' : 'B2LambdapppLinesConf',
    'CONFIG' : {'Trk_Chi2'                 : 3.0,
                'Trk_GhostProb'            : 0.5,
                'Lambda_DD_MassWindow'     : 20.0,
                'Lambda_DD_VtxChi2'        : 9.0,
                'Lambda_DD_FDChi2'         : 50.0,
                'Lambda_DD_FD'             : 300.0,
                'Lambda_DD_Pmin'           : 5000.0,
                'Lambda_LL_MassWindow'     : 20.0,
                'Lambda_LL_VtxChi2'        : 9.0,
                'Lambda_LL_FDChi2'         : 0.0,
                'Lambda_LD_MassWindow'     : 25.0,
                'Lambda_LD_VtxChi2'        : 16.0,
                'Lambda_LD_FDChi2'         : 50.0,
                'Lambda_LD_FD'             : 300.0,
                'Lambda_LD_Pmin'           : 5000.0,
                'B0_Mlow'                  : 779.0,
                'B0_Mhigh'                 : 1921.0,
                'B0_APTmin'                : 1000.0,
                'B0_PTmin'                 : 1050,
                'B0Daug_MedPT_PT'          : 450.0,
                'B0Daug_MaxPT_IP'          : 0.05,
                'B0Daug_DD_maxDocaChi2'    : 16.0,
                'B0Daug_LL_maxDocaChi2'    : 5.0,
                'B0Daug_LD_maxDocaChi2'    : 5.0,
                'B0Daug_DD_PTsum'          : 2000.0,
                'B0Daug_LL_PTsum'          : 3000.0,
                'B0Daug_LD_PTsum'          : 4200.0,
                'B0_VtxChi2'               : 16.0,
                'B0_Dira'               : 0.9990,
                'B0_DD_IPCHI2wrtPV'        : 25.0,
                'B0_LL_IPCHI2wrtPV'        : 25.0,
                'B0_LD_IPCHI2wrtPV'        : 15.0,
                'B0_FDwrtPV'               : 0.8,
                'B0_DD_FDChi2'             : 0.5,
                'B0_LL_FDChi2'             : 0.5,
                'B0_LD_FDChi2'             : 30.0,
                'GEC_MaxTracks'            : 250,
                # 2012 Triggers                                                                                                                  
                #'HLT1Dec'                  : 'Hlt1TrackAllL0Decision',                                                                          
                #'HLT2Dec'                  : 'Hlt2Topo[234]Body.*Decision',                                                                     
                # 2015 Triggers                                                                                                                  
                #'HLT1Dec'                  : 'Hlt1(Two)?TrackMVADecision',                                                                      
                #'HLT2Dec'                  : 'Hlt2Topo[234]BodyDecision',                                                                       
                'Prescale'                 : 1.0,
                'Postscale'                : 1.0,
                'RelatedInfoTools' : [    { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 1.7,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar17'
                                            },
                                          { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 1.5,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar15'
                                            },
                                          { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 1.0,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar10'
                                            },
                                          { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 0.8,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar08'
                                            },
                                          { "Type" : "RelInfoVertexIsolation",
                                            "Location" : "VtxIsolationVar"
                                            }
                                          ]
                },
    'STREAMS' : ['Bhadron']
    }






Xb2phhh_raw = {
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'Xb2phhhConf_raw',
    'CONFIG'      : {'Trk_MaxChi2Ndof'   : 4.0,
                     'Trk_MaxGhostProb'  : 0.4,
                     'Trk_MinIPChi2'     : 16.0,
                     'Trk_MinP'          : 1500.0,
                     'Trk_MinProbNNp'    : 0.05,
                     'Xb_MinSumPTppi'    : 1500.0,
                     'Xb_MinSumPTppipi'  : 2500.0,
                     'Xb_MinM_4body'     : 5195.0,
                     'Xb_MaxM_4body'     : 6405.0,
                     'Xb_MinSumPT_4body' : 3500.0,
                     'Xb_MinPT_4body'    : 1500.0,
                     'Xb_MaxDOCAChi2'    : 20.0,
                     'Xb_MaxVtxChi2'     : 20.0,
                     'Xb_MinFDChi2'      : 50.0,
                     'Xb_MaxIPChi2'      : 16.0,
                     'Xb_MinDira'        : 0.9999,
                     'ConeAngles'        : [0.8,1.0,1.3,1.7],
                     'ConeInputs'        : {'Displaced' : ['/Event/Phys/StdNoPIDsPions'], 'Long': ['/Event/Phys/StdAllNoPIDsPions'] },
                     'ConeVariables'     : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                     'Prescale'          : 1.0,
                     'Postscale'         : 1.0,
                     'RequiredRawEvents' : ["Calo", "Rich", "Velo", "Tracker"]
                     },
    'STREAMS'     : ['Bhadron']
    }

Xib2LbXLb2ph = {
    "WGs"         : ["BnoC"],
    "BUILDERTYPE" : "Xib2LbXLb2phLines",
    "CONFIG"      : { "PrescaleXib2LbX"  : 1,
                      "MinPTLb2ph_h"     : 1000,
                      "MinPTLb2ph_p"     : 1000,
                      "MinPTXib2LbX_h"   : 100,
                      "MinIPChi2Lb2ph"   : 8,
                      "MinIPChi2Xib2LbX" : 4,
                      "TrChi2"           : 4,
                      "TrGhostProb"      : 0.5,
                      "MaxPTLb2ph"       : 1500,
                      "MaxIPChi2Lb2ph"   : 12,
                      "DOCA"             : 0.08,
                      "VCHI2"            : 10,
                      "XibVCHI2"         : 25,
                      "BVDCHI2"          : 100,
                      "BPT"              : 1500,
                      "XibPT"            : 1500,
                      "BTAU"             : 0.0006,
                      "LbCombMassLow"    : 4900,
                      "LbCombMassHigh"   : 6200,
                      "LbMassLow"        : 5000,
                      "LbMassHigh"       : 6100,
                      "XibCombMassLow"   : 5100,
                      "XibCombMassHigh"  : 6300,
                      "XibMassLow"       : 5200,
                      "XibMassHigh"      : 6200,
                      "ConeAngles"       : [ 1.0, 1.5, 1.7, 2.0 ] },
    "STREAMS"     : [ "Bhadron" ]
}


