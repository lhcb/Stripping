###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                          S T R I P P I N G  2 9 r 2 p 2                    ##
##                                                                            ##
##  Configuration for QEE WG                                                  ##
##  Contact person: igor.kostiuk@cern.ch                                      ##
################################################################################

from GaudiKernel.SystemOfUnits import *

B2EHNL = {
    "BUILDERTYPE": "B2EHNLLines", 
    "CONFIG"     : { "GEC_nLongTrk"          : 300.  ,#adimensional
                      #Muons
                      "MuonGHOSTPROB"         : 0.5   ,#adimensional
                      "MuonTRCHI2"            : 4.    ,#adimensional
                      "MuonP"                 : 3000. ,#MeV
                      "MuonPT"                : 250.  ,#MeV
                      "MuonPIDmu"             : 0.    ,#adimensional
                      "MuonMINIPCHI2"         : 12    ,#adminensional

                      "ElectronGHOSTPROB"         : 0.5   ,#adimensional
                      "ElectronTRCHI2"            : 4.    ,#adimensional
                      "ElectronP"                 : 3000. ,#MeV
                      "ElectronPT"                : 250.  ,#MeV
                      "ElectronPIDK"              : 3.0    ,#adimensional
                      "ElectronPIDpi"             : 3.0    ,#adimensional
                      "ElectronPIDp"              : 3.0    ,#adimensional
                      "ElectronMINIPCHI2"         : 12    ,#adminensional

                      #Lambda Daughter Cuts
                      "ElectronPIDe"          : 3.    ,#adimensional
                      "Lambda0DaugP"          : 2000. ,#MeV
                      "Lambda0DaugPT"         : 250.  ,#MeV
                      "Lambda0DaugTrackChi2"  : 4.    ,#adimensional
                      "Lambda0DaugMIPChi2"    : 10.   ,#adimensional
                      #Lambda cuts
                      "MajoranaCutFDChi2"     : 100.  ,#adimensional
                      "MajoranaCutBPVCORRMin"   : 1000. ,#MeV
                      "MajoranaCutBPVCORRMax"   : 6500. ,#MeV

                      "Lambda0VertexChi2"     : 16.   ,#adimensional
                      "Lambda0PT"             : 500.  ,#adimensional
                      #B Mother Cuts
                      "LambdaMuMassLowTight"  : 1500. ,#MeV
                      "BDIRA"                 : 0.99  ,#adminensional
                      "BVCHI2DOF"             : 16.    ,#adminensional
                      "XMuMassUpperHigh"      : 6500. ,#MeV
                      'LambdaZ_prompt'        : -1.,     #mm
                      'LambdaEPiOSZ_displ'   : 20.,     #mm
                      'LambdaEPiSSZ_displ'   : 20.,     #mm
                      'LambdaEEZ_displ'      : -1.,     #mm

                      "HLT1"      : "HLT_PASS_RE('Hlt1.*SingleElectronNoIPDecision')", #Hlt1SingleElectronNoIPDecision
                      "HLT2"      : "HLT_PASS_RE('Hlt2TopoE2BodyDecision')",
                      } ,
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "QEE" ]
}

B2MuHNL = {
    "BUILDERTYPE": "B2MuHNLLines", 
    "CONFIG"     : { "GEC_nLongTrk"          : 300.  ,#adimensional
                     #Muons
                     "MuonGHOSTPROB"         : 0.5   ,#adimensional
                     "MuonTRCHI2"            : 4.    ,#adimensional
                     "MuonP"                 : 3000. ,#MeV
                     "MuonPT"                : 250.  ,#MeV
                     "MuonPIDmu"             : 0.    ,#adimensional
                     "MuonMINIPCHI2"         : 12    ,#adminensional
                     #Lambda Daughter Cuts
                     "ElectronPIDe"          : 3.    ,#adimensional
                     "Lambda0DaugP"          : 2000. ,#MeV
                     "Lambda0DaugPT"         : 250.  ,#MeV
                     "Lambda0DaugTrackChi2"  : 4.    ,#adimensional
                     "Lambda0DaugMIPChi2"    : 10.   ,#adimensional
                     #Lambda cuts
                     "MajoranaCutFDChi2"     : 100.  ,#adimensional
                     "MajoranaCutBPVCORRMin"   : 1000. ,#MeV
                     "MajoranaCutBPVCORRMax"   : 6500. ,#MeV

                     "Lambda0VertexChi2"     : 16.   ,#adimensional
                     "Lambda0PT"             : 500.  ,#adimensional
                     #B Mother Cuts
                     "LambdaMuMassLowTight"  : 1500. ,#MeV
                     "XMuMassUpperHigh"      : 6500. ,#MeV
                     'LambdaZ_prompt'        : -1.,     #mm
                     'LambdaMuPiOSZ_displ'   : 15.,     #mm
                     'LambdaMuPiSSZ_displ'   : 15.,     #mm
                     'LambdaMuEZ_displ'      : -1.,     #mm

                      "HLT1"      : "HLT_PASS_RE('Hlt1.*TrackMuonDecision')",
                      "HLT2"      : "HLT_PASS_RE('Hlt2TopoMu2BodyDecision')"\
                                  "| HLT_PASS_RE('Hlt2Topo2BodyDecision')"\
                                  "| HLT_PASS_RE('Hlt2MajoranaBLambdaMuDDDecision')"\
                                  "| HLT_PASS_RE('Hlt2ExoticaRHNuDecision')", 
                      } ,
    
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "QEE" ]
}

LambdaDecaysDM = {
    "BUILDERTYPE": "LambdaDecaysDMConf", 
    'CONFIG'      : {
    'Common': {
              'checkPV'        : False
              },
    'StrippingDMLambda1520Line': {
              'Prescale'            : 1.0,
              'Postscale'           : 1.0,
              'Prescale_control'    : 0.1,
              'Postscale_control'   : 1.0,
              'Lambda1520_PT'       : 1000*MeV,
              'Lambda1520_iso'      : 0.85,
              'Lambda1520_ProbNNK'  : 0.5,
              'Lambda1520_ProbNNp'  : 0.8,
              'Lambda1520_Daug_PT'  : 500*MeV,
              'Lambda1520_GhostProb': 0.3,
              'Lambda1520_IPChi2'   : 25,
              'Lambda1520_VChi2'    :  2,
              'Lambda1520_M_Min'    : 1460,
              'Lambda1520_M_Max'    : 1580,
              'Lambda1520_FDChi2'   : 45,
              'Lambda1520_MaxDoca'  : 0.1*millimeter,
              },
    'StrippingDMLambdaToPiPiLine': {
              'Prescale'            : 1.0,
              'Postscale'           : 1.0,
              'Prescale_control'    : 0.1,
              'Postscale_control'   : 1.0,
              'LambdaToPiPi_PT'          : 1000*MeV,
              'LambdaToPiPi_iso'         : 0.95,
              'LambdaToPiPi_ProbNNpi'    : 0.9,
              'LambdaToPiPi_Daug_PT'     : 1000*MeV,
              'LambdaToPiPi_Daug_IP_min' : 0.1*millimeter,
              'LambdaToPiPi_Daug_IP_max' : 3*millimeter,
              'LambdaToPiPi_Daug_IPChi2' : 12,
              'LambdaToPiPi_GhostProb'   : 0.1,
              'LambdaToPiPi_IPChi2'      : 10,
              'LambdaToPiPi_VChi2'       : 4,
              'LambdaToPiPi_M_Min'       : 1100,
              'LambdaToPiPi_M_Max'       : 5700,
              'LambdaToPiPi_FDChi2'      : 10,
              'LambdaToPiPi_MaxDoca'     : 0.05*millimeter,
              'LambdaToPiPi_DOCAChi2'    : 4,
              'LambdaToPiPi_FD'          : 5.5,
              'LambdaToPiPi_IPdivFD'     : 0.1,
              },
    'StrippingDMLambdaToKPiLine': {
              'Prescale'            : 1.0,
              'Postscale'           : 1.0,
              'Prescale_control'    : 0.1,
              'Postscale_control'   : 1.0,
              'LambdaToKPi_PT'          : 1000*MeV,
              'LambdaToKPi_iso'         : 0.9,
              'LambdaToKPi_ProbNNpi'    : 0.9,
              'LambdaToKPi_Daug_PT'     : 800*MeV,
              'LambdaToKPi_Daug_IP_min' : 0.1*millimeter,
              'LambdaToKPi_Daug_IP_max' : 3*millimeter,
              'LambdaToKPi_Daug_IPChi2' : 20,
              'LambdaToKPi_GhostProb'   : 0.1,
              'LambdaToKPi_ProbNNK'     : 0.9,
              'LambdaToKPi_IPChi2'      : 15,
              'LambdaToKPi_VChi2'       : 4,
              'LambdaToKPi_M_Min'       : 1100,
              'LambdaToKPi_M_Max'       : 5700,
              'LambdaToKPi_FDChi2'      : 40,
              'LambdaToKPi_MaxDoca'     : 0.1*millimeter,
              'LambdaToKPi_DOCAChi2'    : 4,
              'LambdaToKPi_FD'          : 5,
              'LambdaToKPi_IPdivFD'     : 0.1,
              },
    'StrippingDMLambda2595Line': {
              'Prescale'                  : 1.0,
              'Postscale'                 : 1.0,
              'Prescale_control'          : 0.6,
              'Postscale_control'         : 1.0,
              'K_ProbNNghost'             : 0.1,
              'K_ProbNNk'                 : 0.5,
              'K_PT'                      : 250*MeV,
              'p_ProbNNghost'             : 0.1,
              'p_ProbNNp'                 : 0.5,
              'p_PT'                      : 250*MeV,
              'pi_lambda2595_IPchi2'      : 0.5,
              'pi_lambda2595_ProbNNghost' : 0.1,
              'pi_lambda2595_PT'          : 100*MeV,
              'pi_lambdac_ProbNNghost'    : 0.1,
              'pi_lambdac_PT'             : 250*MeV,
              'Lambda_DOCA'               : 1.0*millimeter,
              'Lambda_FDCHI2'             : 50,
              'Lambda_M_Min'              : 2236*MeV,
              'Lambda_M_Max'              : 2336*MeV,
              'Lambda_PT'                 : 1500*MeV,
              'Lambda_VCHI2'              : 4,
              'Lambda2595_DOCA'           : 1.0*millimeter,
              'Lambda2595_FDCHI2'         : 5,
              'Lambda2595_ISO'            : 0.6,
              'Lambda2595_M_Min'          : 2545*MeV,
              'Lambda2595_M_Max'          : 2645*MeV,
              'Lambda2595_PT'             : 1500*MeV,
              'Lambda2595_VCHI2'          : 4
    },
    'StrippingDMLambdaToDPiLine': {
              'Prescale'                  : 1.0,
              'Postscale'                 : 1.0,
              'Prescale_control'          : 0.6,
              'Postscale_control'         : 1.0,
              'K_ProbNNghost'             : 0.1,
              'K_ProbNNk'                 : 0.9,
              'K_PT'                      : 600*MeV,
              'piD_IPchi2'                : 20,
              'piD_ProbNNghost'           : 0.2,
              'piD_ProbNNpi'              : 0.8,
              'piD_PT'                    : 600*MeV,
              'pi_ProbNNghost'            : 0.1,
              'pi_PT'                     : 600*MeV,
              'pi_ProbNNpi'               : 0.8,
              'D_DOCA'                    : 0.3*millimeter,
              'D_FDCHI2'                  : 50,
              'D_M_Min'                   : 1840*MeV,
              'D_M_Max'                   : 1900*MeV,
              'D_PT'                      : 2000*MeV,
              'D_VCHI2'                   : 6,
              'LambdaToDPi_DOCA'          : 0.2*millimeter,
              'LambdaToDPi_DOCACHI2'      : 7,
              'LambdaToDPi_FDCHI2'        : 30,
              'LambdaToDPi_ISO'           : 0.6,
              'LambdaToDPi_M_Min'         : 1100*MeV,
              'LambdaToDPi_M_Max'         : 5700*MeV,
              'LambdaToDPi_PT'            : 3000*MeV,
              'LambdaToDPi_VCHI2'         : 8,
              'LambdaToDPi_IPCHI2'        : 20
    },
    'StrippingDMLambdaToDKLine': {
              'Prescale'                  : 1.0,
              'Postscale'                 : 1.0,
              'Prescale_control'          : 0.6,
              'Postscale_control'         : 1.0,
              'K_ProbNNghost'             : 0.1,
              'K_ProbNNk'                 : 0.8,
              'K_PT'                      : 250*MeV,
              'KD_IPchi2'                 : 20,
              'KD_ProbNNghost'            : 0.2,
              'KD_ProbNNK'                : 0.7,
              'KD_PT'                     : 250*MeV,
              'pi_ProbNNghost'            : 0.1,
              'pi_PT'                     : 250*MeV,
              'pi_ProbNNpi'               : 0.8,
              'D_DOCA'                    : 0.4*millimeter,
              'D_FDCHI2'                  : 50,
              'D_M_Min'                   : 1840*MeV,
              'D_M_Max'                   : 1900*MeV,
              'D_PT'                      : 2000*MeV,
              'D_VCHI2'                   : 6,
              'LambdaToDK_DOCA'           : 0.5*millimeter,
              'LambdaToDK_DOCACHI2'       : 8,
              'LambdaToDK_FDCHI2'         : 30,
              'LambdaToDK_ISO'            : 0.6,
              'LambdaToDK_M_Min'          : 1100*MeV,
              'LambdaToDK_M_Max'          : 5700*MeV,
              'LambdaToDK_PT'             : 2000*MeV,
              'LambdaToDK_VCHI2'          : 8,
              'LambdaToDK_IPCHI2'         : 25
    },
  },
    
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

ALP2TauTau = {
    "BUILDERTYPE": "ALP2TauTauConf", 
    'CONFIG'               : {
                           'SpdMult'                   : '6000',
                           # Leptons (e, mu) [tighter cuts with respect to arXiv:xxxx.xxxx - no max(e_ISO, mu_ISO) for emu mode]:
                           'TAUE_PROBNN_EMU'           : 0.5,
                           'TAUE_PT'                   : 3500 * MeV,
                           'TAUE_P'                    : 10000 * MeV,
                           'TAUE_IP'                   : 0.03 * mm,
                           'TAUE_PROBNN'               : 0.25,
                           'TAUE_ISO'                  : .99,
                           'TAUMU_PT'                  : 3500 * MeV,
                           'TAUMU_P'                   : 10000 * MeV,
                           'TAUMU_IP'                  : 0.01 * mm,
                           'TAUMU_PROBNN'              : 0.8,
                           'TAUMU_ISO'                 : .99,
                           # Tau leptons and pion daughters:
                           'TAUCOMB_DOCA'              : 0.05*mm,
                           'TAU_MASS_HIGH'             : 1700.*MeV,
                           'TAU_MASS_LOW'              : 0.*MeV,
                           'TAUPI_MINPT'               : 1000 * MeV,
                           'TAUPI_MINP'                : 2000 * MeV,
                           'TAUPI_MINIP'               : 0.01 * mm,
                           'TAUPI_MINPID'              : 0.,
                           'TAU_IPMAX'                 : 0.2* mm,
                           'TAU_MCORR_MAX'             : 2500.*MeV,
                           'TAU_MCORR_MIN'             : 1200.*MeV,
                           'TAU_PT'                    : 10000.*MeV,
                           'TAU_ISO'                   : .15, # this cut kills signal but also helps to reduce background.
                           ## Specific cuts for full hadronic modes:
                           'TAUPI_MINPT_FH'            : 500 * MeV,
                           'TAU_IPMAX_FH'              : 0.1* mm,
                           'TAU_RHO_MIN_FH'            : 0.1* mm,
                           'TAU_RHO_MAX_FH'            : 5.* mm,
                           'TAU_PT_FH'                 : 2500.*MeV,
                           ## ALP combination:
                           'HTAU_IPMAX'                : 0.2*mm,
                           'HTAU_DOCA'                 : 0.4*mm,
                           'HTAU_FDMAX'                : 1*mm,
                           'HTAU_DOCA_FH'              : 0.1*mm,
                           'HTAU_FDMAX_FH'             : 0.25*mm,
                           'HTAU_IPMAX_FH'             : 0.1*mm,
                           'HTAU_MASS_LOW'             : 0.*MeV,
                           'HTAU_MASS_HIGH'            : 1e6*MeV,
                           'HTAU_ETA_MIN'              : 2,
                           'HTAU_ETA_MAX'              : 4.5,
                           'HTAU_PT_MIN'               : 15*GeV,
                           'HTAU_PT_MAX'               : 150*GeV,
                           'HTAU_PT_DAUG_MIN'          : 5.*GeV,
                           'HTAU_PT_DAUG_MAX'          : 7.5*GeV,
                           'HTAU_ETA_DAUG_MIN'         : 1.5,
                           'HTAU_ETA_DAUG_MAX'         : 5,
                           ## Prescales and postscales:
                           'HTau3HTau3H_LinePrescale'  : 1,
                           'HTau3HTau3H_LinePostscale' : 1,
                           'HTau3HTauMu_LinePrescale'  : 1,
                           'HTau3HTauMu_LinePostscale' : 1,
                           'HTau3HTauE_LinePrescale'   : 1,
                           'HTau3HTauE_LinePostscale'  : 1,
                           'HTauMuTauE_LinePrescale'   : 1,
                           'HTauMuTauE_LinePostscale'  : 1
                           },
                           'STREAMS'     : ['BhadronCompleteEvent'],
                           "WGs": [ "QEE" ]
}

LLP2Hadrons = {
    "BUILDERTYPE": "LLP2HadronsConf", 
  'WGs'         : [ 'QEE' ],
  'STREAMS'     : {
    'BhadronCompleteEvent': [
      'StrippingLLP2Hadrons_KstKst_LongLine',
      'StrippingLLP2Hadrons_KsKpi_LL_LongLine',
      'StrippingLLP2Hadrons_KsKpi_DD_LongLine',
      'StrippingLLP2Hadrons_DpDm_LongLine',
      'StrippingLLP2Hadrons_DpDz_LongLine',
      'StrippingLLP2Hadrons_DpDp_LongLine',
      'StrippingLLP2Hadrons_DzDz_LongLine',
      'StrippingLLP2Hadrons_DzDzbar_LongLine',
      'StrippingLLP2Hadrons_EtaPiPi_LongLine',
      'StrippingLLP2Hadrons_KstKst_DownLine',
      'StrippingLLP2Hadrons_KsKpi_DD_DownLine',
      'StrippingLLP2Hadrons_DpDm_DownLine',
      'StrippingLLP2Hadrons_DpDz_DownLine',
      'StrippingLLP2Hadrons_DpDp_DownLine',
      'StrippingLLP2Hadrons_DzDz_DownLine',
      'StrippingLLP2Hadrons_DzDzbar_DownLine',
      'StrippingLLP2Hadrons_EtaPiPi_DownLine',
    ],
  },
  'CONFIG'      : {
    'Common': {
      'CommonTrackCuts': "(TRGHOSTPROB<0.3) & (P>3.*GeV) & (BPVIPCHI2() > 9.)",
    },
    'Prescales': {
      'KstKst'        : 1.0,
      'KsKpi'         : 1.0,
      'DD'            : 1.0,
      'EtaPiPi'       : 1.0,
    },
    'KstKst': {
      'Long':{
          'TrackCuts'     : "(PT>500*MeV)",
          'K_id'          : "(PROBNNk  > 0.4)",
          'Pi_id'         : "(PROBNNpi > 0.4)",
          'KstCombCuts'   : "(ADAMASS('K*(892)0')<300*MeV)",
          'KstMomCuts'    : "(VFASPF(VCHI2PDOF) < 9.) & (in_range( PDGM('K*(892)0') - 120*MeV , M , PDGM('K*(892)0') + 120*MeV ))",
          'CombCuts'      : "AALL",
          'MomCuts'       : "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS > 10) & (PT>2.*GeV) & (BPVIPCHI2() < 9.)"
      },
     'Down':{
          'TrackCuts'     : "(PT>500*MeV)",
          'KstCombCuts'   : "(ADAMASS('K*(892)0')<300*MeV)",
          'KstMomCuts'    : "(VFASPF(VCHI2PDOF) < 9.) & (in_range( PDGM('K*(892)0') - 120*MeV , M , PDGM('K*(892)0') + 120*MeV ))",
          'CombCuts'      : "AALL",
          'MomCuts'       : "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS > 5)  & (VFASPF(VZ)>200*mm) & (PT>2.*GeV)"
      },
    },
    'KsKpi_LL': {
      'Long':{
          'TrackCuts'    : "(PT > 700*MeV)", 
          'K_id'         : "(PROBNNk  > 0.4)",
          'Pi_id'        : "(PROBNNpi > 0.4)",
          'Ks_PiCuts'    : "(MINTREE('pi-'==ABSID, PROBNNpi) > 0.4) & (MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, MIPCHI2DV(PRIMARY)) > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)",
          'Ks_MomCuts'   : "(VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 9.) & (BPVDLS>5) & (ADMASS('KS0') < 30.*MeV)",
          'CombCuts'     : "in_range(1.0*GeV, AM, 12.2*GeV)",
          'MomCuts'      : "in_range(1.0*GeV, M, 12.0*GeV) & (VFASPF(VCHI2PDOF)<9.) & (BPVDLS>10) & (PT>2.*GeV) & (BPVIPCHI2()<9.) & (BPVLTIME()>1.5*picosecond)"
      }
    },
    'KsKpi_DD': {
        'Long':{
          'TrackCuts'  : "(PT > 700*MeV)", 
          'K_id'       : "(PROBNNk  > 0.4)",
          'Pi_id'      : "(PROBNNpi > 0.4)",
          'Ks_PiCuts'  : "(MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, MIPCHI2DV(PRIMARY)) > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)",
          'Ks_MomCuts' : "(VFASPF(VCHI2PDOF) < 9.) & (ADMASS('KS0') < 30.*MeV) & (BPVIPCHI2() > 9.)",
          'CombCuts'   : "in_range(1.0*GeV, AM, 12.2*GeV)",
          'MomCuts'    : "in_range(1.0*GeV, M, 12.0*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5) & (PT>2.*GeV) & (BPVIPCHI2() < 9.) & (BPVLTIME()>1.*picosecond)"
        },
        'Down':{
          'TrackCuts'  : "(PT>700*MeV)", 
          'Ks_PiCuts'  : "(MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)",
          'Ks_MomCuts' : "(VFASPF(VCHI2PDOF) < 9.) & (ADMASS('KS0') < 50.*MeV) & (BPVIPCHI2() > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)",
          'CombCuts'   : "AM>1.*GeV",
          'MomCuts'    : "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5)  & (VFASPF(VZ)>200*mm) & (BPVIPCHI2() < 9.)"
        },      
    },
    'DD': {
        'Long':{
          'TrackCuts'  : "(PT > 500*MeV)", 
          'K_id'       : "(PROBNNk  > 0.2)",
          'Pi_id'      : "(PROBNNpi > 0.2)",
          'DCombCuts'  : "in_range(1.0*GeV, AM, 3.*GeV)",
          'DMomCuts'   : "(ADMASS('D+') < 80.*MeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 4.)",
          'CombCuts'   : "(AM>3.0*GeV)",
          'MomCuts'    : "(M>3.0*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (PT>1.*GeV) & (BPVDLS>5)"
        },
        'Down':{
          'TrackCuts'  : "(PT>500*MeV)", 
          'DCombCuts'  : "in_range(1.0*GeV, AM, 3.*GeV)",
          'DMomCuts'   : "(ADMASS('D+') < 150.*MeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 9.)",
          'CombCuts'   : "(AM>3.0*GeV)",
          'MomCuts'    : "(M>3.0*GeV) & (PT>1.*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>3) & (VFASPF(VZ)>200*mm)"        
        },
    },
    'EtaPiPi': {
        'Long':{
          'TrackCuts'  : "(PT>500*MeV)",
          'Pi_id'      : "(PROBNNpi > 0.4)",
          'Pi0_cuts'   : "(PT>600*MeV)",
          'EtaCombCuts': "(AM<1*GeV)",
          'EtaMomCuts' : "(ADMASS('eta') < 100.*MeV) & (VFASPF(VCHI2PDOF) < 9.)",
          'CombCuts'   : "AALL",
          'MomCuts'    : "(VFASPF(VCHI2PDOF) < 9.) & (PT>2.*GeV) & (BPVDLS>5) & (BPVIPCHI2() < 9.) & (M<2.5*GeV) & (BPVLTIME()>1.5*picosecond)"
        },
        'Down':{
          'TrackCuts'  : "(PT>500*MeV)",
          'Pi0_cuts'   : "(PT > 600*MeV)",
          'EtaCombCuts': "AALL",
          'EtaMomCuts' : "(ADMASS('eta') < 100.*MeV) & (VFASPF(VCHI2PDOF) < 9.)",
          'CombCuts'   : "AALL",
          'MomCuts'    : "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5) & (VFASPF(VZ)>200*mm) & (M<2.5*GeV)"
        },
    },
  },
}

LLPV0 = {
    "BUILDERTYPE": "LLPV0_sConf", 
    'CONFIG'    : {
                         '2HHSinglePrescale'     :  0.0135 
                 ,       '2HHMultiPrescale'      :  1.0
                 ,       '2HHSingleKaonPrescale' :  0.045
                 ,       '2HHDoubleKaonPrescale' :  1.0
                 ,       '2mumuSinglePrescale'   :  0. # line removed
                 ,       '2MixedPrescale'        :  1.0
                 ,       'dimuProbNNmu'          :  0.
                 ,       'dihProbNNmu'           :  0.
                 ,       'dihProbNNk'            :  0.01
                 ,       'dikProbNNk'            :  0.55
                 ,       'dimuProbNNk'           :  0.5
                 ,       'mixedProbNNk'          :  0.01
                 ,       'dihhalt'               :  100
                 ,       'dihHHSingle'           :  1
                 ,       'dihHHMulti'            :  6
                 ,       'dikHHSingleKaon'       :  1
                 ,       'dikHHDoubleKaon'       :  2
                 ,       'dimuMuonsSingle'       :  1
                 ,       'dihMuons'              :  0
                 ,       'dimuMixed'             :  1
                 ,       'dihMixed'              :  2
                 ,       'daugsP'                :  5.*GeV
                 ,       'daugsPT'               :  1.*GeV
                 ,       'daugsChi2dv'           :  25.
                 ,       'combChi2'              :  25.
                 ,       'motherChi2vx'          :  16.
                 ,       'motherLifetime'        :  1.5*picosecond
                  },
    'STREAMS' : ['BhadronCompleteEvent'], 
    "WGs": [ "QEE" ]
}
