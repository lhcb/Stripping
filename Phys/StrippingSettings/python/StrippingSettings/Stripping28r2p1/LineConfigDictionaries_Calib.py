###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #                                                                                                                           
###############################################################################
Dst2D0Pi = {
    "BUILDERTYPE": "StrippingDst2D0PiConf", 
    "CONFIG": {
        "D0MaxIPChi2": 9, 
        "D0MaxM": 2100, 
        "D0MinDIRA": 0.9999, 
        "D0MinM": 1600, 
        "D0MinVVDChi2": 64, 
        "D0MinVtxProb": 0.001, 
        "Dst2D0PiLinePostscale": 1, 
        "Dst2D0PiLinePrescale": 1, 
        "Dst2D0etaPiLinePostscale": 1, 
        "Dst2D0etaPiLinePrescale": 1, 
        "Dst2D0pi0PiLinePostscale": 1, 
        "Dst2D0pi0PiLinePrescale": 1, 
        "Dst_PT_MIN": 4000, 
        "Dst_VCHI2PDOF_MAX": 9, 
        "Dst_VIPCHI2_MAX": 9, 
        "Dst_VVDCHI2_MAX": 16, 
        "Dst_dAM_MAX": 195.421, 
        "Dst_dAM_MIN": 95.421, 
        "Dst_dM_MAX": 155.421, 
        "Dst_dM_MIN": 135.421, 
        "EtaMassMax": 650, 
        "EtaMassMin": 450, 
        "EtaMinPT": 1500, 
        "GammaMinPT": 2000, 
        "Pi0MassMax": 200, 
        "Pi0MassMin": 70, 
        "Pi0MinPT": 1000, 
        "SoftPionMaxIPChi2": 16, 
        "TrackMaxGhostProb": 0.3, 
        "TrackMinIPChi2": 16, 
        "TrackMinPT": 300, 
        "TrackMinTrackProb": 1e-06
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Calib" ]
}

ElectronIDCalib = {
    "BUILDERTYPE": "ElectronIDConf", 
    "CONFIG": {
        "Both_MIPCHI2": 9.0, 
        "Both_P": 3000.0, 
        "Both_PT": 500.0, 
        "Both_TRCHI2DOF": 5.0, 
        "Bu2JpsieeKLine_BuComCut": "in_range(4.1*GeV,AM,6.1*GeV)", 
        "Bu2JpsieeKLine_BuMomCut": "in_range(4.2*GeV,M, 6.0*GeV) & (VFASPF(VCHI2PDOF)<9)", 
        "Bu2JpsieeKLine_HltFilter": None, 
        "Bu2JpsieeKLine_JpsiCut": "(BPVDLS>4)", 
        "Bu2JpsieeKLine_KaonCut": "(TRCHI2DOF<4) & (PT>1.0*GeV) & (P>3.0*GeV) & (PIDK>5) & (BPVIPCHI2()>9)", 
        "Bu2JpsieeKLine_Prescale": 1, 
        "Bu2JpsieeKLine_notagID_BuComCut": "in_range(3.6*GeV,AM,8.*GeV)", 
        "Bu2JpsieeKLine_notagID_BuMomCut": "(PT>3200) & (VFASPF(VCHI2PDOF)<9) & in_range(4.5*GeV,DTF_FUN(M,True,'J/psi(1S)'),6.0*GeV)", 
        "Bu2JpsieeKLine_notagID_HltFilter": None, 
        "Bu2JpsieeKLine_notagID_JpsiCut": "(BPVDLS>4)", 
        "Bu2JpsieeKLine_notagID_KaonCut": "(TRCHI2DOF<4) & (PT>1.0*GeV) & (P>3.0*GeV) & (PIDK>5) & (BPVIPCHI2()>9)", 
        "Bu2JpsieeKLine_notagID_Prescale": 1, 
        "JpsiLineCut": "(PT>2.*GeV) & (BPVDLS>50) ", 
        "JpsiLineHltFilter": None, 
        "JpsiLinePrescale": 0, 
        "Probe_MIPCHI2": 9.0, 
        "Probe_P": 3000.0, 
        "Probe_PT": 500.0, 
        "Tag_MIPCHI2": 9.0, 
        "Tag_P": 6000.0, 
        "Tag_PIDe": 5.0, 
        "Tag_PT": 1500.0, 
        "eeCombMaxMass": 4300.0, 
        "eeCombMinMass": 2100.0, 
        "eeMaxMass": 4200.0, 
        "eeMinMass": 2200.0, 
        "eeVCHI2PDOF": 9.0
    }, 
    "STREAMS": {
        "BhadronCompleteEvent": [ "StrippingElectronIDCalibBu2JpsiKLine" ], 
        "Leptonic": [ "StrippingElectronIDCalibBu2JpsiK_notagIDLine" ]
    }, 
    "WGs": [ "Calib" ]
}

Eta2MuMuGamma = {
    "BUILDERTYPE": "StrippingEta2MuMuGammaConf", 
    "CONFIG": {
        "EtaMassMax": 1000, 
        "EtaMassMin": 0, 
        "FDChi2": 45, 
        "GPT": 500, 
        "K0MassMax": 1000, 
        "K0MassMin": 0, 
        "K0_PT": 1000, 
        "MMG_LinePostscale": 1, 
        "MMG_LinePrescale": 1, 
        "MuIPChi2": 6, 
        "MuPT": 500, 
        "MuProbNNmu": 0.2
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Calib" ]
}

noPIDDstar = {
    "BUILDERTYPE": "NoPIDDstarWithD02RSKPiConf", 
    "CONFIG": {
        "D0BPVDira": 0.9999, 
        "D0FDChi2": 49, 
        "D0IPChi2": 30, 
        "D0MassWin": 100.0, 
        "D0Pt": 1600.0, 
        "D0VtxChi2Ndof": 10, 
        "DCS_WrongMass": 25.0, 
        "DaugIPChi2": 16, 
        "DaugP": 2000.0, 
        "DaugPt": 250.0, 
        "DaugTrkChi2": 4, 
        "DeltaM_Max": 155.0, 
        "DeltaM_Min": 130.0, 
        "DstarPt": 2200.0, 
        "DstarVtxChi2Ndof": 13, 
        "KK_WrongMass": 25.0, 
        "Monitor": False, 
        "PiPi_WrongMass": 25.0, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "SlowPiPt": 150.0, 
        "SlowPiTrkChi2": 4
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Calib" ]
}

noPIDDstar_brem = {
    "BUILDERTYPE": "NoPIDDstarWithD02RSKPi_bremConf", 
    "CONFIG": {
        "D0BPVDira": 0.9999, 
        "D0FDChi2": 49, 
        "D0IPChi2": 30, 
        "D0MassWin": 500.0, 
        "D0Pt": 1600.0, 
        "D0VtxChi2Ndof": 10, 
        "DCS_WrongMass": 25.0, 
        "DaugIPChi2": 16, 
        "DaugP": 2000.0, 
        "DaugPt": 250.0, 
        "DaugTrkChi2": 4, 
        "DeltaM_Max": 155.0, 
        "DeltaM_Min": 130.0, 
        "DstarPt": 2200.0, 
        "DstarVtxChi2Ndof": 13, 
        "KK_WrongMass": 25.0, 
        "Monitor": False, 
        "PiPi_WrongMass": 25.0, 
        "Postscale": 1.0, 
        "Prescale": 1, 
        "SlowPiPt": 150.0, 
        "SlowPiTrkChi2": 4
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Calib" ]
}

