/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PROCSTATUSCHECK_H
#define PROCSTATUSCHECK_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class ProcStatusCheck ProcStatusCheck.h
 *
 * A simple algorithm that checks for the events aborted by reconstruction
 * algorithms.
 *
 * Returns True if at least one record is present in ProcStatus structure.
 *
 * If OutputLevel == DEBUG, prints out the names and status of the failed
 * algorithms
 *
 *  @author Anton Poluektov
 *  @date   2010-06-24
 */
class ProcStatusCheck : public GaudiAlgorithm
{

public:

  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

};
#endif // PROCSTATUSCHECK_H
