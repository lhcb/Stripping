/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: StrippingCheck.h,v 1.1 2009/10/13 13:12:35 poluekt Exp $
#ifndef STRIPPINGCHECK_H
#define STRIPPINGCHECK_H 1

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class StrippingCheck StrippingCheck.h
 *
 *
 *  @author Anton Poluektov
 *  @date   2009-10-12
 */
class StrippingCheck : public GaudiAlgorithm
{

public:

  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute   () override;    ///< Algorithm execution

private:

  unsigned int numberOfCandidates(const std::string& selalgo) const;

  Gaudi::Property<std::string> m_inputLocation {this, "InputLocation"};

};

#endif // STRIPPINGCHECK_H
