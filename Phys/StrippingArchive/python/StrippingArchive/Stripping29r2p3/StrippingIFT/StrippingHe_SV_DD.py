from __future__ import print_function
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selection of He candidates via Median DEDX
'''

__author__ = ['Hendrik Jage', 'Dan Moise']
__date__ = '14.06.2023'
__version__ = 'v0r1'

__all__ = ('He_SV_DDConf', 'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

from StandardParticles import StdNoPIDsDownPions as DownPions
from StandardParticles import StdNoPIDsDownKaons as DownKaons

from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from Configurables import LoKi__VoidFilter as VoidFilter

default_config = {
    'NAME': 'He_SV_DD',
    'BUILDERTYPE': 'He_SV_DDConf',
    'WGs': ['IFT'],
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'Prescale': 1.0,
        'OT_p0': -1.7189,
        'OT_p1':   0.486,
        'OT_p2':   13200,
        'He_minP':             0,
        'He_NumTTClusters':    2,
        'He_NumOTTimes':       7,
        'He_TTLLDCut':         1,
        'He_TTLLDVersionHe': 'v2',
        'He_TTLLDVersionZ1': 'v2',
        'He_OT_dt':         1000,
        'Pi_MinIPChi2':    -1000,
        'HePi_MaxM_TL':     3100,
        'HePi_MaxDocaChi2':   30,
        'HePi_MaxVtxChi2':    25,
        'HePi_MinFDChi2':     50,
        'RequiredRawEvents': ["Calo", "Rich", "Velo", "Tracker", "Muon", "HC"],
    },
}


class He_SV_DDConf(LineBuilder):
    __configuration_keys__ = list(default_config['CONFIG'].keys())

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        he_presel_cut = "    (TRTTCLUSTERS  > %(He_NumTTClusters)s)" % self.config
        he_presel_cut += " & (TROTTIMES     > %(He_NumOTTimes)s)" % self.config
        he_presel_cut += " & (P             > %(He_minP)s)" % self.config

        he_presel_filter = FilterDesktop(Code=he_presel_cut)

        he_presel = Selection('DownHelium_TLDD_PreSel',
                              Algorithm=he_presel_filter,
                              RequiredSelections=[DownKaons])

        ot_preambulo = ["P_He     = 2*P*MeV",
                        "OT_t_eff = %(OT_p1)s/0.299792458*(sqrt((%(OT_p2)s/P_He)**2 + 1) - 1) + %(OT_p0)s" % self.config]

        he_pid_cut = "(   DEDXPID('He', 'TT', '%(He_TTLLDVersionHe)s')" % self.config
        he_pid_cut += " - DEDXPID('Z1', 'TT', '%(He_TTLLDVersionZ1)s') > %(He_TTLLDCut)s)" % self.config
        he_pid_cut += " & (abs(TRTIME() - OT_t_eff)                    < %(He_OT_dt)s)" % self.config

        he_pid_filter = FilterDesktop(Code=he_pid_cut,
                                      Preambulo=ot_preambulo)

        helium_sel = Selection('DownHelium_TLDD_ForSel',
                               Algorithm=he_pid_filter,
                               RequiredSelections=[he_presel])

        pion_cuts = "(MIPCHI2DV(PRIMARY) > %(Pi_MinIPChi2)s)" % self.config

        pion_filter = FilterDesktop(Code=pion_cuts)

        pion_sel = Selection('DownPion_TLDD_ForSel',
                             Algorithm=pion_filter,
                             RequiredSelections=[DownPions])

        # Mother
        _decayDescriptors = ["triton -> K+ pi-", "triton~ -> K- pi+",  # OS
                             "triton -> K+ pi+", "triton~ -> K- pi-"]  # SS

        preambulo = ["from LoKiPhys.decorators import *",
                     "m_He   = 2808.39161",
                     "P_He   = 2*ACHILD(P,1)",
                     "E_He   = sqrt(m_He*m_He + P_He*P_He)",
                     "Px_TL  = (2*ACHILD(PX,1)+ACHILD(PX,2))",
                     "Py_TL  = (2*ACHILD(PY,1)+ACHILD(PY,2))",
                     "Pz_TL  = (2*ACHILD(PZ,1)+ACHILD(PZ,2))",
                     "P2_TL  = Px_TL*Px_TL + Py_TL*Py_TL + Pz_TL*Pz_TL",
                     "E_TL   = E_He + ACHILD(2,E)",
                     "M_TL   = sqrt(E_TL*E_TL - P2_TL)"]

        comb_cut = '(M_TL                < %(HePi_MaxM_TL)s )' % self.config
        comb_cut += " & (ACUTDOCACHI2(%(HePi_MaxDocaChi2)s,''))" % self.config

        mother_cut = '    (VFASPF(VCHI2/VDOF) < %(HePi_MaxVtxChi2)s )' % self.config
        mother_cut += ' & (BPVVDCHI2          > %(HePi_MinFDChi2)s )' % self.config

        _HePiComb = CombineParticles(DecayDescriptors=_decayDescriptors,
                                     CombinationCut=comb_cut,
                                     MotherCut=mother_cut,
                                     Preambulo=preambulo)

        HePi_sel = Selection('HePi_TLDD_ForSel',
                             Algorithm=_HePiComb,
                             RequiredSelections=[helium_sel, pion_sel])

        He_SV_DD_Line = StrippingLine(
            self.name + '_Line',
            prescale=self.config['Prescale'],
            RequiredRawEvents=self.config['RequiredRawEvents'],
            selection=HePi_sel)

        self.registerLine(He_SV_DD_Line)
