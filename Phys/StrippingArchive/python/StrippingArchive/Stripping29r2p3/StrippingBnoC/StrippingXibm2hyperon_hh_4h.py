###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###
### Selection code for Xibm charmless decays to Xim/Omm together with KS0, Phi(1020) or 4h, where h = K/pi
###

__author__ = ['Miroslav Saur']
__date__ = '2023/07/07'
__version__ = '$Revision: 0.1 $'
__all__ = ('StrippingXibm2hyperon_hh_4h', 'default_config')

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays, DaVinci__N5BodyDecays
from StandardParticles import StdAllNoPIDsPions, StdNoPIDsDownPions, StdAllNoPIDsKaons, StdLooseDownKaons, StdAllLoosePions, StdAllLooseKaons, StdAllLooseProtons
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond as ps
from Configurables import CombineParticles

#### This is the dictionary of all tunable cuts ########
default_config = {
    'NAME': 'Xibm2hyperon_hh_4h',
    'WGs': ['BnoC'],
    'BUILDERTYPE': 'StrippingXibm2hyperon_hh_4h',
    'STREAMS': ['Bhadron'],
    'CONFIG': {
        "RequiredRawEvents": ["Velo"],
        'TRCHI2DOFMax':
        4.0,
        'TrGhostProbMax':
        0.3,  # same for all particles
        'MINIPCHI2':
        8.0,  # adimensiional, orig. 9 
        'MINIPCHI2_from_hyperon':
        4.0,
        'pion_p_min':
        2.0 * GeV,
        'pion_pt_min':
        200 * MeV,
        'pion_pidk_max':
        10.0,
        'pion_pidpi_pidk_max':
        0.0,
        'kaon_p_min':
        2.0 * GeV,
        'kaon_pt_min':
        200 * MeV,
        'kaon_down_pt_min':
        0 * MeV,
        'kaon_pidk_min':
        10.0,
        'kaon_pidk_pidpi_max':
        5.0,
        'proton_p_min':
        2.0 * GeV,
        'proton_pt_min':
        200 * MeV,
        'proton_pidp_pidpi_max':
        5.0,
        'proton_pidp_pidk_max':
        0.0,
        'ProbNNp':
        0.2,
        'ProbNNk':
        0.2,
        'ProbNNpi':
        0.2,
        'ProbNNpiMax':
        0.9,
        'ProbNNkMax':
        0.9,
        'lambda_ll_p_min':
        2000.0 * MeV,
        'lambda_ll_pt_min':
        100 * MeV,
        'lambda_ll_mass_window':
        35.0 * MeV,
        'lambda_ll_fdchi2_min':
        30.0,
        'lambda_dd_p_min':
        2000.0 * MeV,
        'lambda_dd_pt_min':
        100.0 * MeV,
        'lambda_dd_mass_window':
        40.0 * MeV,
        'lambda_dd_fdchi2_min':
        50.0,  ## unitless
        'LambdaLDPMin':
        3000.0 * MeV,
        'LambdaLDPTMin':
        200.0 * MeV,
        'LambdaLDCutMass':
        20.0 * MeV,
        'lambda_ld_fdchi2_min':
        100.0,  ## unitless
        'lambda_trackchi2_min':
        4.0,  ## unitless
        'lambda_vertexchi2_min':
        5.0,  ## max chi2/ndf for Lambda0 vertex
        'lambda_ll_dira_min':
        0.99,  ## unitless
        'lambda_dd_dira_min':
        0.9,  ## unitless
        'lambda_ll_vz_min':
        -100.0 * mm,
        'lambda_ll_vz_max':
        400.0 * mm,
        'lambda_dd_vz_min':
        400.0 * mm,
        'lambda_dd_vz_max':
        2275.0 * mm,
        'lambda_pp_pt_min':
        0.0 * MeV,
        'lambda_pim_pt_min':
        0.0 * MeV,
        'hyperon_mass_window_comb':
        80 * MeV,
        'hyperon_lll_mass_window':
        60 * MeV,
        'hyperon_ddl_mass_window':
        65 * MeV,
        'hyperon_ddd_mass_window':
        65 * MeV,
        'hyperon_ADOCACHI2CUT_max':
        25,
        'hyperon_VFASPF_max':
        25,
        'hyperon_p_min':
        1000 * MeV,
        'hyperon_pt_min':
        0 * MeV,
        'hyperon_VFASPFchi2ndof_max':
        12.0,
        'hyperon_bpvltime_min':
        0.0005 * ps,
        'hyperon_long_vdchi2':
        10,
        'hyperon_down_vdchi2':
        20,
        'Xibm_vertexchi2_max':
        25.0,
        'Xibm_BPVVDCHI2_Min':
        25.0,
        'Xibm_BPVDIRA_Min':
        0.995,
        'Xibm_AM_Min':
        5300.0 * MeV,
        'Xibm_AM_Max':
        6250.0 * MeV,
        'Xibm_ADOCAMAXCHI2_max':
        25,
        'Xibm_APT_2b_min':
        800 * MeV,  # 500
        'Xibm_APT_3b_min':
        1000 * MeV,  # 700
        'Xibm_APT_5b_min':
        1500 * MeV,  # 1000
        'RelatedInfoTools':
        [{
            "Type": "RelInfoConeVariables",
            "ConeAngle": 1.7,
            "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
            "Location": 'ConeVar17'
        },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 1.5,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar15'
         },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 1.0,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar10'
         },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 0.8,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar08'
         }, {
             "Type": "RelInfoVertexIsolation",
             "Location": "VtxIsolationVar"
         }]
    }  ## end of 'CONFIG' 
}  ## end of default_config


#-------------------------------------------------------------------------------------------------------------
class StrippingXibm2hyperon_hh_4h(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        ##########################################################################
        ## Basic particles: pi, K, p
        ##########################################################################
        self.selPion = Selection(
            "sel_pi_for" + name,
            Algorithm=self._pionFilter("pi_for_" + name),
            RequiredSelections=[StdAllLoosePions])

        self.selPion_long_for_hyperon = Selection(
            "sel_pi_long_for_hyperon_" + name,
            Algorithm=self._pionFilterForHyperon("pi_for_hyperon_" + name),
            RequiredSelections=[StdAllNoPIDsPions])

        self.selPion_down = Selection(
            "sel_pi_down_for" + name,
            Algorithm=self._pionFilter_down("pi_down_for_" + name),
            RequiredSelections=[StdNoPIDsDownPions])

        self.selKaon = Selection(
            "sel_k_for" + name,
            Algorithm=self._kaonFilter("kaon_for_" + name),
            RequiredSelections=[StdAllLooseKaons])

        self.selKaon_long_for_hyperon = Selection(
            "sel_k_long_for_hyperon_" + name,
            Algorithm=self._kaonFilterForHyperon("kaon_for_hyperon_" + name),
            RequiredSelections=[StdAllNoPIDsKaons])

        self.selKaon_down = Selection(
            "sel_k_down_for" + name,
            Algorithm=self._kaonFilter_down("kaon_down_for" + name),
            RequiredSelections=[StdLooseDownKaons])

        self.selProton = Selection(
            "sel_p_for" + name,
            Algorithm=self._protonFilter("proton_for_" + name),
            RequiredSelections=[StdAllLooseProtons])

        ##########################################################################
        ## KS0 -> pi+ pi-
        ##########################################################################
        _stdLooseKS0LL = DataOnDemand(
            Location="Phys/StdVeryLooseKsLL/Particles")
        _stdLooseKS0DD = DataOnDemand(Location="Phys/StdLooseKsDD/Particles")
        _stdLooseKS0LD = DataOnDemand(Location="Phys/StdLooseKsLD/Particles")

        self.selKS0LL = Selection(
            "SelKS0LLfor" + name,
            Algorithm=self._KS0_LL_Filter("KS0LLfor" + name),
            RequiredSelections=[_stdLooseKS0LL])

        self.selKS0DD = Selection(
            "SelKS0DDfor" + name,
            Algorithm=self._KS0_DD_Filter("KS0DDfor" + name),
            RequiredSelections=[_stdLooseKS0DD])

        self.selKS0LD = Selection(
            "SelKS0LDfor" + name,
            Algorithm=self._KS0_LD_Filter("KS0LDfor" + name),
            RequiredSelections=[_stdLooseKS0LD])

        ##########################################################################
        ## Phi(1020) -> K+ K-
        ##########################################################################
        _stdLoosePhi2KK = DataOnDemand(
            Location="Phys/StdLoosePhi2KK/Particles")

        self.selPhi = Selection(
            "SelPhifor" + name,
            Algorithm=self._Phi_Filter("Phi_for_" + name),
            RequiredSelections=[_stdLoosePhi2KK])

        ##########################################################################
        ## Lambda0 -> p+ pi-
        ##########################################################################
        _stdLooseLambdaLL = DataOnDemand(
            Location="Phys/StdVeryLooseLambdaLL/Particles")
        _stdLooseLambdaDD = DataOnDemand(
            Location="Phys/StdLooseLambdaDD/Particles")
        _stdLooseLambdaLD = DataOnDemand(
            Location="Phys/StdLooseLambdaLD/Particles")

        self.selLambdaLL = Selection(
            "SelLambdaLLfor" + name,
            Algorithm=self._LambdaLLFilter("LambdaLLfor" + name),
            RequiredSelections=[_stdLooseLambdaLL])

        self.selLambdaDD = Selection(
            "SelLambdaDDfor" + name,
            Algorithm=self._LambdaDDFilter("LambdaDDfor" + name),
            RequiredSelections=[_stdLooseLambdaDD])

        self.selLambdaLD = Selection(
            "SelLambdaLDfor" + name,
            Algorithm=self._LambdaLDFilter("LambdaLDfor" + name),
            RequiredSelections=[_stdLooseLambdaLD])

        ##########################################################################
        ## Xi- -> Lambda0 pi-
        ##########################################################################

        #hyperon_postvertex_cut =

        self.Ximinus2LambdaPi_LLL = self.createCombination2body_xim(OutputList = "Ximinus2LambdaPi_LLL_for"+ self.name,
                #DecayDescriptor = "[Xi- -> Lambda0 pi-]cc",
                DaughterLists   = [self.selLambdaLL, self.selPion_long_for_hyperon],
                DaughterCuts    = {},
                PreVertexCuts   = "(ADAMASS('Xi-') < %(hyperon_mass_window_comb)s) & (ADOCACHI2CUT(%(hyperon_ADOCACHI2CUT_max)s, ''))" % self.config,
                PostVertexCuts  = "(ADMASS('Xi-') < %(hyperon_lll_mass_window)s)"\
                                "& (VFASPF(VCHI2) < %(hyperon_VFASPF_max)s)"\
                                "& (P> %(hyperon_p_min)s) & (PT > %(hyperon_pt_min)s)"\
                                "& (BPVVDCHI2 > %(hyperon_long_vdchi2)s )"\
                                "& (VFASPF(VCHI2/VDOF) < %(hyperon_VFASPFchi2ndof_max)s)" \
                                "& (BPVLTIME() > %(hyperon_bpvltime_min)s)" % self.config )

        self.Ximinus2LambdaPi_DDL = self.createCombination2body_xim(OutputList = "Ximinus2LambdaPi_DDL_for"+ self.name,
                #DecayDescriptor = "[Xi- -> Lambda0 pi-]cc",
                DaughterLists   = [self.selLambdaDD, self.selPion_long_for_hyperon],
                DaughterCuts    = {},
                PreVertexCuts   = "(ADAMASS('Xi-') < %(hyperon_mass_window_comb)s) & (ADOCACHI2CUT(%(hyperon_ADOCACHI2CUT_max)s, ''))" % self.config,
                PostVertexCuts  = "(ADMASS('Xi-') < %(hyperon_lll_mass_window)s)"\
                                "& (VFASPF(VCHI2) < %(hyperon_VFASPF_max)s)"\
                                "& (P> %(hyperon_p_min)s) & (PT > %(hyperon_pt_min)s)"\
                                "& (BPVVDCHI2 > %(hyperon_down_vdchi2)s )"\
                                "& (VFASPF(VCHI2/VDOF) < %(hyperon_VFASPFchi2ndof_max)s)" \
                                "& (BPVLTIME() > %(hyperon_bpvltime_min)s)" % self.config )

        self.Ximinus2LambdaPi_DDD = self.createCombination2body_xim(OutputList = "Ximinus2LambdaPi_DDD_for"+ self.name,
                #DecayDescriptor = "[Xi- -> Lambda0 pi-]cc",
                DaughterLists   = [self.selLambdaLD, self.selPion_down],
                DaughterCuts    = {},
                PreVertexCuts   = "(ADAMASS('Xi-') < %(hyperon_mass_window_comb)s) & (ADOCACHI2CUT(%(hyperon_ADOCACHI2CUT_max)s, ''))" % self.config,
                PostVertexCuts  = "(ADMASS('Xi-') < %(hyperon_lll_mass_window)s)"\
                                "& (VFASPF(VCHI2) < %(hyperon_VFASPF_max)s)"\
                                "& (P> %(hyperon_p_min)s) & (PT > %(hyperon_pt_min)s)"\
                                "& (BPVVDCHI2 > %(hyperon_down_vdchi2)s )"\
                                "& (VFASPF(VCHI2/VDOF) < %(hyperon_VFASPFchi2ndof_max)s)" \
                                "& (BPVLTIME() > %(hyperon_bpvltime_min)s)" % self.config )

        self.XibmToXim = self.make_XibmToXim()
        self.OmbmList = self.make_XibmToOmm()

    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    # Sub Function
    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    def _pionFilter(self, _name):
        _code = "(TRCHI2DOF < %(TRCHI2DOFMax)s) & (TRGHOSTPROB < %(TrGhostProbMax)s) & (P>%(pion_p_min)s) & (PT > %(pion_pt_min)s)"\
                "& (~ISMUON) & (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PROBNNpi > %(ProbNNpi)s)" % self.config
        _pion = FilterDesktop(Code=_code)
        return _pion

    def _pionFilterForHyperon(self, name):
        _code = "(TRGHOSTPROB < %(TrGhostProbMax)s) & (MIPCHI2DV(PRIMARY)> %(MINIPCHI2_from_hyperon)s) & (~ISMUON)" % self.config
        _pion = FilterDesktop(Code=_code)
        return _pion

    def _pionFilter_down(self, _name):
        _code = "(MIPCHI2DV(PRIMARY)> %(MINIPCHI2_from_hyperon)s) & (~ISMUON)" % self.config
        _pion = FilterDesktop(Code=_code)
        return _pion

    def _kaonFilter(self, _name):
        _code = "(TRCHI2DOF < %(TRCHI2DOFMax)s) & (TRGHOSTPROB < %(TrGhostProbMax)s) & (P>%(kaon_p_min)s) & (PT > %(kaon_pt_min)s)"\
                "& (~ISMUON) & (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PROBNNk > %(ProbNNk)s)" % self.config
        _kaon = FilterDesktop(Code=_code)
        return _kaon

    def _kaonFilterForHyperon(self, name):
        _code = "(TRGHOSTPROB < %(TrGhostProbMax)s) & (MIPCHI2DV(PRIMARY)> %(MINIPCHI2_from_hyperon)s) & (~ISMUON)" % self.config
        _kaon = FilterDesktop(Code=_code)
        return _kaon

    def _kaonFilter_down(self, _name):
        _code = "(MIPCHI2DV(PRIMARY)> %(MINIPCHI2_from_hyperon)s) & (~ISMUON)" % self.config
        _kaon = FilterDesktop(Code=_code)
        return _kaon

    def _protonFilter(self, _name):
        _code = "(TRCHI2DOF < %(TRCHI2DOFMax)s) & (TRGHOSTPROB < %(TrGhostProbMax)s) & (PT > %(proton_pt_min)s) & (P>%(proton_p_min)s)"\
                "& (PROBNNp > %(ProbNNp)s) "\
                "& (~ISMUON) & (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)" % self.config
        _proton = FilterDesktop(Code=_code)
        return _proton

    def _LambdaLLFilter(self, _name):
        _code = " (P > %(lambda_ll_p_min)s) & (PT > %(lambda_ll_pt_min)s)" \
                " & (ADMASS('Lambda0')<%(lambda_ll_mass_window)s)" \
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 1) )"\
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 2) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 1) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 2) )" \
                " & (VFASPF(VCHI2/VDOF) < %(lambda_vertexchi2_min)s)" \
                " & (BPVDIRA > %(lambda_ll_dira_min)s )" % self.config
        _l0LL = FilterDesktop(Code=_code)
        return _l0LL

    def _LambdaDDFilter(self, _name):
        _code = " (P> %(lambda_dd_p_min)s) & (PT> %(lambda_dd_pt_min)s)" \
                " & (ADMASS('Lambda0') < %(lambda_dd_mass_window)s)" \
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 1) )"\
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 2) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 1) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 2) )" \
                " & (VFASPF(VCHI2/VDOF) < %(lambda_vertexchi2_min)s)" \
                " & (BPVDIRA > %(lambda_ll_dira_min)s )" % self.config
        _l0DD = FilterDesktop(Code=_code)
        return _l0DD

    def _LambdaLDFilter(self, _name):
        _code = " (P> %(LambdaLDPMin)s) & (PT> %(LambdaLDPTMin)s)" \
                " & (ADMASS('Lambda0') < %(LambdaLDCutMass)s)" \
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 1) )"\
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 2) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 1) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 2) )" \
                " & (VFASPF(VCHI2/VDOF) < %(lambda_vertexchi2_min)s)" \
                " & (BPVDIRA > %(lambda_dd_dira_min)s )" % self.config
        _l0LD = FilterDesktop(Code=_code)
        return _l0LD

    def _KS0_LL_Filter(self, name):
        _code = "(ADMASS('KS0') < 40*MeV ) & (BPVDIRA > %(lambda_ll_dira_min)s )" % self.config
        _ksLL = FilterDesktop(Code=_code)
        return _ksLL

    def _KS0_LD_Filter(self, name):
        _code = "(ADMASS('KS0') < 45*MeV) & (BPVDIRA > %(lambda_ll_dira_min)s )" % self.config
        _ksLD = FilterDesktop(Code=_code)
        return _ksLD

    def _KS0_DD_Filter(self, name):
        _code = "(ADMASS('KS0') < 60*MeV) & (BPVDIRA > %(lambda_dd_dira_min)s )" % self.config
        _ksDD = FilterDesktop(Code=_code)
        return _ksDD

    def _Phi_Filter(self, name):
        _code = "(ADMASS('phi(1020)') < 50*MeV)"
        _phi = FilterDesktop(Code=_code)
        return _phi

    ##------------------------------------------------------------------------------------------
    ## --------------------  Begin to make_Xibm  ------------
    def make_XibmToXim(self):
        #Cut for Basic
        combination12_cut = "( ACHI2DOCA(1,2) <%(Xibm_ADOCAMAXCHI2_max)s)"\
        "& ((APT1 + APT2)>%(Xibm_APT_2b_min)s)" % self.config
        combination123_cut = "( ACHI2DOCA(1,3) <%(Xibm_ADOCAMAXCHI2_max)s)" \
                             "& ( ACHI2DOCA(2,3) <%(Xibm_ADOCAMAXCHI2_max)s)"\
        "& ((APT1 + APT2 + APT3)> %(Xibm_APT_3b_min)s)" % self.config

        combination1234_cut = "( ACHI2DOCA(1,2) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(1,3) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(1,4) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(2,3) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(2,4) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(3,4) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ((APT1 + APT2 + APT3 + APT4)>%(Xibm_APT_5b_min)s)" % self.config

        basic_xibm_prevertex_cut = "(AM>%(Xibm_AM_Min)s) & (AM<%(Xibm_AM_Max)s)" % self.config

        basic_postvertex_cut = "( (VFASPF(VCHI2/VDOF)<%(Xibm_vertexchi2_max)s)" \
                         "& (BPVVDCHI2>%(Xibm_BPVVDCHI2_Min)s)" \
                         "& (BPVDIRA>%(Xibm_BPVDIRA_Min)s) )" % self.config

        xibm_prevertex_cut = basic_xibm_prevertex_cut
        xibm_postvertex_cut = basic_postvertex_cut

        ### Stripping Xi_b- -> Xi- pi+ pi+ pi- pi- ###
        Xibm2XimPipPipPimPim_LLL = self.createCombination5body(
            OutputList="Xibm2XimPipPipPimPim_LLL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- pi+ pi+ pi- pi-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_LLL, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPipPipPimPimLLL_Line = StrippingLine(
            "Xibm2XimPipPipPimPimLLL_Line",
            algos=[Xibm2XimPipPipPimPim_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPipPipPimPimLLL_Line)

        Xibm2XimPipPipPimPim_DDL = self.createCombination5body(
            OutputList="Xibm2XimPipPipPimPim_DDL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- pi+ pi+ pi- pi-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDL, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPipPipPimPimDDL_Line = StrippingLine(
            "Xibm2XimPipPipPimPimDDL_Line",
            algos=[Xibm2XimPipPipPimPim_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPipPipPimPimDDL_Line)

        Xibm2XimPipPipPimPim_DDD = self.createCombination5body(
            OutputList="Xibm2XimPipPipPimPim_DDD" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- pi+ pi+ pi- pi-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDD, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPipPipPimPimDDD_Line = StrippingLine(
            "Xibm2XimPipPipPimPimDDD_Line",
            algos=[Xibm2XimPipPipPimPim_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPipPipPimPimDDD_Line)

        ### Stripping Xi_b- -> Xi- K+ K- pi+ pi- ###
        Xibm2XimKpKmPipPim_LLL = self.createCombination5body(
            OutputList="Xibm2XimKpKmPipPim_LLL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- K+ K- pi+ pi-]cc",
            DaughterLists=[
                self.Ximinus2LambdaPi_LLL, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKpKmPipPimLLL_Line = StrippingLine(
            "Xibm2XimKpKmPipPimLLL_Line",
            algos=[Xibm2XimKpKmPipPim_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKpKmPipPimLLL_Line)

        Xibm2XimKpKmPipPim_DDL = self.createCombination5body(
            OutputList="Xibm2XimKpKmPipPim_DDL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- K+ K- pi+ pi-]cc",
            DaughterLists=[
                self.Ximinus2LambdaPi_DDL, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKpKmPipPimDDL_Line = StrippingLine(
            "Xibm2XimKpKmPipPimDDL_Line",
            algos=[Xibm2XimKpKmPipPim_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKpKmPipPimDDL_Line)

        Xibm2XimKpKmPipPim_DDD = self.createCombination5body(
            OutputList="Xibm2XimKpKmPipPim_DDD" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- K+ K- pi+ pi-]cc",
            DaughterLists=[
                self.Ximinus2LambdaPi_DDD, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKpKmPipPimDDD_Line = StrippingLine(
            "Xibm2XimKpKmPipPimDDD_Line",
            algos=[Xibm2XimKpKmPipPim_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKpKmPipPimDDD_Line)

        ##########################################################################
        ## Pseudo three-body decays
        ##########################################################################

        ### Stripping Xi_b- -> Xi- pi+ pi- ###
        Xibm2XimPipPim_LLL = self.createCombination3body(
            OutputList="Xibm2XimPipPim_LLL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- pi+ pi-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_LLL, self.selPion],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPipPimLLL_Line = StrippingLine(
            "Xibm2XimPipPimLLL_Line",
            algos=[Xibm2XimPipPim_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPipPimLLL_Line)

        Xibm2XimPipPim_DDL = self.createCombination3body(
            OutputList="Xibm2XimPipPim_DDL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- pi+ pi-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDL, self.selPion],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPipPimDDL_Line = StrippingLine(
            "Xibm2XimPipPimDDL_Line",
            algos=[Xibm2XimPipPim_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPipPimDDL_Line)

        Xibm2XimPipPim_DDD = self.createCombination3body(
            OutputList="Xibm2XimPipPim_DDD" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- pi+ pi-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDD, self.selPion],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPipPimDDD_Line = StrippingLine(
            "Xibm2XimPipPimDDD_Line",
            algos=[Xibm2XimPipPim_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPipPimDDD_Line)

        ### Stripping Xi_b- -> Xi- K+ pi- ###
        Xibm2XimKpPim_LLL = self.createCombination3body(
            OutputList="Xibm2XimKpPim_LLL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- K+ pi-]cc",
            DaughterLists=[
                self.Ximinus2LambdaPi_LLL, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKpPimLLL_Line = StrippingLine(
            "Xibm2XimKpPimLLL_Line",
            algos=[Xibm2XimKpPim_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKpPimLLL_Line)

        Xibm2XimKpPim_DDL = self.createCombination3body(
            OutputList="Xibm2XimKpPim_DDL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- K+ pi-]cc",
            DaughterLists=[
                self.Ximinus2LambdaPi_DDL, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKpPimDDL_Line = StrippingLine(
            "Xibm2XimKpPimDDL_Line",
            algos=[Xibm2XimKpPim_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKpPimDDL_Line)

        Xibm2XimKpPim_DDD = self.createCombination3body(
            OutputList="Xibm2XimKpPim_DDD" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- K+ pi-]cc",
            DaughterLists=[
                self.Ximinus2LambdaPi_DDD, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKpPimDDD_Line = StrippingLine(
            "Xibm2XimKpPimDDD_Line",
            algos=[Xibm2XimKpPim_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKpPimDDD_Line)

        ### Stripping Xi_b- -> Xi- K+ K- ###
        Xibm2XimKpKm_LLL = self.createCombination3body(
            OutputList="Xibm2XimKpKm_LLL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- K+ K-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_LLL, self.selKaon],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKpKmLLL_Line = StrippingLine(
            "Xibm2XimKpKmLLL_Line",
            algos=[Xibm2XimKpKm_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKpKmLLL_Line)

        Xibm2XimKpKm_DDL = self.createCombination3body(
            OutputList="Xibm2XimKpKm_DDL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- K+ K-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDL, self.selKaon],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKpKmDDL_Line = StrippingLine(
            "Xibm2XimKpKmDDL_Line",
            algos=[Xibm2XimKpKm_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKpKmDDL_Line)

        Xibm2XimKpKm_DDD = self.createCombination3body(
            OutputList="Xibm2XimKpKm_DDD" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- K+ K-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDD, self.selKaon],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKpKmDDD_Line = StrippingLine(
            "Xibm2XimKpKmDDD_Line",
            algos=[Xibm2XimKpKm_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKpKmDDD_Line)

        ### Stripping Xi_b- -> Xi- p+ p~- ###
        Xibm2XimPpPm_LLL = self.createCombination3body(
            OutputList="Xibm2XimPpPm_LLL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- p+ p~-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_LLL, self.selProton],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPpPmLLL_Line = StrippingLine(
            "Xibm2XimPpPmLLL_Line",
            algos=[Xibm2XimPpPm_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPpPmLLL_Line)

        Xibm2XimPpPm_DDL = self.createCombination3body(
            OutputList="Xibm2XimPpPm_DDL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- p+ p~-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDL, self.selProton],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPpPmDDL_Line = StrippingLine(
            "Xibm2XimPpPmDDL_Line",
            algos=[Xibm2XimPpPm_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPpPmDDL_Line)

        Xibm2XimPpPm_DDD = self.createCombination3body(
            OutputList="Xibm2XimPpPm_DDD" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- p+ p~-]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDD, self.selProton],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPpPmDDD_Line = StrippingLine(
            "Xibm2XimPpPmDDD_Line",
            algos=[Xibm2XimPpPm_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPpPmDDD_Line)

        ##########################################################################
        ## Pseudo two-body decays
        ##########################################################################

        ### Stripping Xi_b- / Omega_b- -> Xi- KS0 ###
        Xibm2XimKS0_LLLLL = self.createCombination2body(
            OutputList="Xibm2XimKS0_LLLLL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- KS0]cc",
            DaughterLists=[self.Ximinus2LambdaPi_LLL, self.selKS0LL],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKS0LLLLL_Line = StrippingLine(
            "Xibm2XimKS0LLLLL_Line",
            algos=[Xibm2XimKS0_LLLLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKS0LLLLL_Line)

        Xibm2XimKS0_LLLDD = self.createCombination2body(
            OutputList="Xibm2XimKS0_LLLDD" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- KS0]cc",
            DaughterLists=[self.Ximinus2LambdaPi_LLL, self.selKS0DD],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKS0LLLDDLine = StrippingLine(
            "Xibm2XimKS0LLLDD_Line",
            algos=[Xibm2XimKS0_LLLDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKS0LLLDDLine)

        Xibm2XimKS0_DDLLL = self.createCombination2body(
            OutputList="Xibm2XimKS0_DDLLL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- KS0]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDL, self.selKS0LL],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKS0DDLLL_Line = StrippingLine(
            "Xibm2XimKS0DDLLL_Line",
            algos=[Xibm2XimKS0_DDLLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKS0DDLLL_Line)

        Xibm2XimKS0_DDLDD = self.createCombination2body(
            OutputList="Xibm2XimKS0_DDLDD" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- KS0]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDL, self.selKS0DD],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKS0DDLDDLine = StrippingLine(
            "Xibm2XimKS0DDLDD_Line",
            algos=[Xibm2XimKS0_DDLDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKS0DDLDDLine)

        Xibm2XimKS0_DDDLL = self.createCombination2body(
            OutputList="Xibm2XimKS0_DDDLL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- KS0]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDD, self.selKS0LL],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKS0DDDLLLine = StrippingLine(
            "Xibm2XimKS0DDDLL_Line",
            algos=[Xibm2XimKS0_DDDLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKS0DDDLLLine)

        Xibm2XimKS0_DDDDD = self.createCombination2body(
            OutputList="Xibm2XimKS0_DDDDD" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- KS0]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDD, self.selKS0DD],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKS0DDDDD_Line = StrippingLine(
            "Xibm2XimKS0DDDDD_Line",
            algos=[Xibm2XimKS0_DDDDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKS0DDDDD_Line)

        ### Stripping Xi_b- / Omega_b- -> Xi- Phi ###
        Xibm2XimPhi_LLL = self.createCombination2body(
            OutputList="Xibm2XimPhi_LLL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- phi(1020)]cc",
            DaughterLists=[self.Ximinus2LambdaPi_LLL, self.selPhi],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPhiLLL_Line = StrippingLine(
            "Xibm2XimPhiLLL_Line",
            algos=[Xibm2XimPhi_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPhiLLL_Line)

        Xibm2XimPhi_DDL = self.createCombination2body(
            OutputList="Xibm2XimPhi_DDL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- phi(1020)]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDL, self.selPhi],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPhiDDL_Line = StrippingLine(
            "Xibm2XimPhiDDL_Line",
            algos=[Xibm2XimPhi_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPhiDDL_Line)

        Xibm2XimPhi_DDD = self.createCombination2body(
            OutputList="Xibm2XimPhi_DDD" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- phi(1020)]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDD, self.selPhi],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimPhiDDD_Line = StrippingLine(
            "Xibm2XimPhiDDD_Line",
            algos=[Xibm2XimPhi_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimPhiDDD_Line)

        ### Stripping Xi_b- / Omega_b- -> Xi- K+ ###
        Xibm2XimKp_LLL = self.createCombination2body(
            OutputList="Xibm2XimKp_LLL" + self.name,
            DecayDescriptor="[Xi_b- -> Xi- K+]cc",
            DaughterLists=[self.Ximinus2LambdaPi_LLL, self.selKaon],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2XimKpLLL_Line = StrippingLine(
            "Xibm2XimKpLLL_Line",
            algos=[Xibm2XimKp_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2XimKpLLL_Line)

        Xib02XimKp_DDL = self.createCombination2body(
            OutputList="Xib02XimKp_DDL" + self.name,
            DecayDescriptor="[Xi_b0 -> Xi- K+]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDL, self.selKaon],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xib02XimKpDDL_Line = StrippingLine(
            "Xib02XimKpDDL_Line",
            algos=[Xib02XimKp_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xib02XimKpDDL_Line)

        Xib02XimKp_DDD = self.createCombination2body(
            OutputList="Xib02XimKp_DDD" + self.name,
            DecayDescriptor="[Xi_b0 -> Xi- K+]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDD, self.selKaon],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xib02XimKpDDD_Line = StrippingLine(
            "Xib02XimKpDDD_Line",
            algos=[Xib02XimKp_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xib02XimKpDDD_Line)

        ### Stripping Xi_b- / Omega_b- -> Xi- pi+ ###
        Xib02XimPip_LLL = self.createCombination2body(
            OutputList="Xib02XimPip_LLL" + self.name,
            DecayDescriptor="[Xi_b0 -> Xi- pi+]cc",
            DaughterLists=[self.Ximinus2LambdaPi_LLL, self.selPion],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xib02XimPipLLL_Line = StrippingLine(
            "Xib02XimPipLLL_Line",
            algos=[Xib02XimPip_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xib02XimPipLLL_Line)

        Xib02XimPip_DDL = self.createCombination2body(
            OutputList="Xib02XimPip_DDL" + self.name,
            DecayDescriptor="[Xi_b0 -> Xi- pi+]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDL, self.selPion],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xib02XimPipDDL_Line = StrippingLine(
            "Xib02XimPipDDL_Line",
            algos=[Xib02XimPip_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xib02XimPipDDL_Line)

        Xib02XimPip_DDD = self.createCombination2body(
            OutputList="Xib02XimPip_DDD" + self.name,
            DecayDescriptor="[Xi_b0 -> Xi- pi+]cc",
            DaughterLists=[self.Ximinus2LambdaPi_DDD, self.selPion],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xib02XimPipDDD_Line = StrippingLine(
            "Xib02XimPipDDD_Line",
            algos=[Xib02XimPip_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xib02XimPipDDD_Line)

    ##  --------------------  end of make_Xibm  ------------
    ##------------------------------------------------------------------------------------------

    def make_XibmToOmm(self):

        ##########################################################################
        ## Omega- -> Lambda0 K-
        ##########################################################################
        self.Omegaminus2LambdaK_LLL = self.createCombination2body_omm(OutputList = "Omegaminus2LambdaK_LLL_for_"+ self.name,
                #DecayDescriptor = "[Omega- -> Lambda0 K-]cc",
                DaughterLists   = [self.selLambdaLL, self.selKaon_long_for_hyperon],
                DaughterCuts    = {},
                PreVertexCuts   = "(ADAMASS('Omega-') < %(hyperon_mass_window_comb)s) & (ADOCACHI2CUT(%(hyperon_ADOCACHI2CUT_max)s, ''))" % self.config,
                PostVertexCuts  = "(ADMASS('Omega-') < %(hyperon_lll_mass_window)s)"\
                                "& (VFASPF(VCHI2) < %(hyperon_VFASPF_max)s)"\
                                "& (P> %(hyperon_p_min)s) & (PT > %(hyperon_pt_min)s)"\
                                "& (BPVVDCHI2 > %(hyperon_long_vdchi2)s )"\
                                "& (VFASPF(VCHI2/VDOF) < %(hyperon_VFASPFchi2ndof_max)s)" \
                                "& (BPVLTIME() > %(hyperon_bpvltime_min)s)" % self.config )

        self.Omegaminus2LambdaK_DDL = self.createCombination2body_omm(OutputList = "Omegaminus2LambdaK_DDL_for_"+ self.name,
                #DecayDescriptor = "[Omega- -> Lambda0 K-]cc",
                DaughterLists   = [self.selLambdaDD, self.selKaon_long_for_hyperon],
                DaughterCuts    = {},
                PreVertexCuts   = "(ADAMASS('Omega-') < %(hyperon_mass_window_comb)s) & (ADOCACHI2CUT(%(hyperon_ADOCACHI2CUT_max)s, ''))" % self.config ,
                PostVertexCuts  = "(ADMASS('Omega-') < %(hyperon_lll_mass_window)s)"\
                                "& (VFASPF(VCHI2) < %(hyperon_VFASPF_max)s)"\
                                "& (P> %(hyperon_p_min)s) & (PT > %(hyperon_pt_min)s)"\
                                "& (BPVVDCHI2 > %(hyperon_down_vdchi2)s )"\
                                "& (VFASPF(VCHI2/VDOF) < %(hyperon_VFASPFchi2ndof_max)s)" \
                                "& (BPVLTIME() > %(hyperon_bpvltime_min)s)" % self.config )

        self.Omegaminus2LambdaK_DDD = self.createCombination2body_omm(OutputList = "Omegaminus2LambdaK_DDD_for_"+ self.name,
                #DecayDescriptor = "[Omega- -> Lambda0 K-]cc",
                DaughterLists   = [self.selLambdaDD, self.selKaon_down],
                DaughterCuts    = {},
                PreVertexCuts   = "(ADAMASS('Omega-') < %(hyperon_mass_window_comb)s) & (ADOCACHI2CUT(%(hyperon_ADOCACHI2CUT_max)s, ''))" % self.config,
                PostVertexCuts  = "(ADMASS('Omega-') < %(hyperon_lll_mass_window)s)"\
                                "& (VFASPF(VCHI2) < %(hyperon_VFASPF_max)s)"\
                                "& (P> %(hyperon_p_min)s) & (PT > %(hyperon_pt_min)s)"\
                                "& (BPVVDCHI2 > %(hyperon_down_vdchi2)s )"\
                                "& (VFASPF(VCHI2/VDOF) < %(hyperon_VFASPFchi2ndof_max)s)" \
                                "& (BPVLTIME() > %(hyperon_bpvltime_min)s)" % self.config )

        #Cut for Basic
        combination12_cut = "( ACHI2DOCA(1,2) <%(Xibm_ADOCAMAXCHI2_max)s)"\
        "& ((APT1 + APT2)>%(Xibm_APT_2b_min)s)" % self.config
        combination123_cut = "( ACHI2DOCA(1,3) <%(Xibm_ADOCAMAXCHI2_max)s)" \
                             "& ( ACHI2DOCA(2,3) <%(Xibm_ADOCAMAXCHI2_max)s)"\
        "& ((APT1 + APT2 + APT3)> %(Xibm_APT_3b_min)s)" % self.config

        combination1234_cut = "( ACHI2DOCA(1,2) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(1,3) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(1,4) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(2,3) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(2,4) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(3,4) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ((APT1 + APT2 + APT3 + APT4)>%(Xibm_APT_5b_min)s)" % self.config

        basic_xibm_prevertex_cut = "(AM>%(Xibm_AM_Min)s) & (AM<%(Xibm_AM_Max)s)" % self.config

        basic_postvertex_cut = "( (VFASPF(VCHI2/VDOF)<%(Xibm_vertexchi2_max)s)" \
                         "& (BPVVDCHI2>%(Xibm_BPVVDCHI2_Min)s)" \
                         "& (BPVDIRA>%(Xibm_BPVDIRA_Min)s) )" % self.config

        xibm_prevertex_cut = basic_xibm_prevertex_cut
        xibm_postvertex_cut = basic_postvertex_cut

        ### Stripping Omega_b- -> Omega- pi+ pi+ pi- pi- ###
        Ombm2OmmPipPipPimPim_LLL = self.createCombination5body(
            OutputList="Ombm2OmmPipPipPimPim_LLL" + self.name,
            DecayDescriptor="[Omega_b- -> Omega- pi+ pi+ pi- pi-]cc",
            DaughterLists=[self.Omegaminus2LambdaK_LLL, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Ombm2OmmPipPipPimPimLLL_Line = StrippingLine(
            "Ombm2OmmPipPipPimPimLLL_Line",
            algos=[Ombm2OmmPipPipPimPim_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Ombm2OmmPipPipPimPimLLL_Line)

        Ombm2OmmPipPipPimPim_DDL = self.createCombination5body(
            OutputList="Ombm2OmmPipPipPimPim_DDL" + self.name,
            DecayDescriptor="[Omega_b- -> Omega- pi+ pi+ pi- pi-]cc",
            DaughterLists=[self.Omegaminus2LambdaK_DDL, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Ombm2OmmPipPipPimPimDDL_Line = StrippingLine(
            "Ombm2OmmPipPipPimPimDDL_Line",
            algos=[Ombm2OmmPipPipPimPim_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Ombm2OmmPipPipPimPimDDL_Line)

        Ombm2OmmPipPipPimPim_DDD = self.createCombination5body(
            OutputList="Ombm2OmmPipPipPimPim_DDD" + self.name,
            DecayDescriptor="[Omega_b- -> Omega- pi+ pi+ pi- pi-]cc",
            DaughterLists=[self.Omegaminus2LambdaK_DDD, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Ombm2OmmPipPipPimPimDDD_Line = StrippingLine(
            "Ombm2OmmPipPipPimPimDDD_Line",
            algos=[Ombm2OmmPipPipPimPim_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Ombm2OmmPipPipPimPimDDD_Line)

        ### Stripping Omega_b- -> Omega- K+ K- pi+ pi- ###
        Ombm2OmmKpKmPipPim_LLL = self.createCombination5body(
            OutputList="Ombm2OmmKpKmPipPim_LLL" + self.name,
            DecayDescriptor="[Omega_b- -> Omega- K+ K- pi+ pi-]cc",
            DaughterLists=[
                self.Omegaminus2LambdaK_LLL, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Ombm2OmmKpKmPipPimLLL_Line = StrippingLine(
            "Ombm2OmmKpKmPipPimLLL_Line",
            algos=[Ombm2OmmKpKmPipPim_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Ombm2OmmKpKmPipPimLLL_Line)

        Ombm2OmmKpKmPipPim_DDL = self.createCombination5body(
            OutputList="Ombm2OmmKpKmPipPim_DDL" + self.name,
            DecayDescriptor="[Omega_b- -> Omega- K+ K- pi+ pi-]cc",
            DaughterLists=[
                self.Omegaminus2LambdaK_DDL, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Ombm2OmmKpKmPipPimDDL_Line = StrippingLine(
            "Ombm2OmmKpKmPipPimDDL_Line",
            algos=[Ombm2OmmKpKmPipPim_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Ombm2OmmKpKmPipPimDDL_Line)

        Ombm2OmmKpKmPipPim_DDD = self.createCombination5body(
            OutputList="Ombm2OmmKpKmPipPim_DDD" + self.name,
            DecayDescriptor="[Omega_b- -> Omega- K+ K- pi+ pi-]cc",
            DaughterLists=[
                self.Omegaminus2LambdaK_DDD, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Ombm2OmmKpKmPipPimDDD_Line = StrippingLine(
            "Ombm2OmmKpKmPipPimDDD_Line",
            algos=[Ombm2OmmKpKmPipPim_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Ombm2OmmKpKmPipPimDDD_Line)

        ### Stripping Xi_b- -> Omega- K+ pi- ###
        Xibm2OmmKpPim_LLL = self.createCombination3body(
            OutputList="Xibm2OmmKpPim_LLL" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- K+ pi-]cc",
            DaughterLists=[
                self.Omegaminus2LambdaK_LLL, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmKpPimLLL_Line = StrippingLine(
            "Xibm2OmmKpPimLLL_Line",
            algos=[Xibm2OmmKpPim_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmKpPimLLL_Line)

        Xibm2OmmKpPim_DDL = self.createCombination3body(
            OutputList="Xibm2OmmKpPim_DDL" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- K+ pi-]cc",
            DaughterLists=[
                self.Omegaminus2LambdaK_DDL, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmKpPimDDL_Line = StrippingLine(
            "Xibm2OmmKpPimDDL_Line",
            algos=[Xibm2OmmKpPim_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmKpPimDDL_Line)

        Xibm2OmmKpPim_DDD = self.createCombination3body(
            OutputList="Xibm2OmmKpPim_DDD" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- K+ pi-]cc",
            DaughterLists=[
                self.Omegaminus2LambdaK_DDD, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmKpPimDDD_Line = StrippingLine(
            "Xibm2OmmKpPimDDD_Line",
            algos=[Xibm2OmmKpPim_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmKpPimDDD_Line)

        ### Stripping Xi_b- -> Omega- p+ p~- ###
        Xibm2OmmPpPm_LLL = self.createCombination3body(
            OutputList="Xibm2OmmPpPm_LLL" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- p+ p~-]cc",
            DaughterLists=[self.Omegaminus2LambdaK_LLL, self.selProton],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmPpPmLLL_Line = StrippingLine(
            "Xibm2OmmPpPmLLL_Line",
            algos=[Xibm2OmmPpPm_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmPpPmLLL_Line)

        Xibm2OmmPpPm_DDL = self.createCombination3body(
            OutputList="Xibm2OmmPpPm_DDL" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- p+ p~-]cc",
            DaughterLists=[self.Omegaminus2LambdaK_LLL, self.selProton],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmPpPmDDL_Line = StrippingLine(
            "Xibm2OmmPpPmDDL_Line",
            algos=[Xibm2OmmPpPm_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmPpPmDDL_Line)

        Xibm2OmmPpPm_DDD = self.createCombination3body(
            OutputList="Xibm2OmmPpPm_DDD" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- p+ p~-]cc",
            DaughterLists=[self.Omegaminus2LambdaK_LLL, self.selProton],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmPpPmDDD_Line = StrippingLine(
            "Xibm2OmmPpPmDDD_Line",
            algos=[Xibm2OmmPpPm_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmPpPmDDD_Line)

        ### Stripping Omega_b- -> Omega- pi+ pi- ###
        Ombm2OmmPipPim_LLL = self.createCombination3body(
            OutputList="Ombm2OmmPipPim_LLL" + self.name,
            DecayDescriptor="[Omega_b- -> Omega- pi+ pi-]cc",
            DaughterLists=[self.Omegaminus2LambdaK_LLL, self.selPion],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Ombm2OmmPipPimLLL_Line = StrippingLine(
            "Ombm2OmmPipPimLLL_Line",
            algos=[Ombm2OmmPipPim_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Ombm2OmmPipPimLLL_Line)

        Ombm2OmmPipPim_DDL = self.createCombination3body(
            OutputList="Ombm2OmmPipPim_DDL" + self.name,
            DecayDescriptor="[Omega_b- -> Omega- pi+ pi-]cc",
            DaughterLists=[self.Omegaminus2LambdaK_LLL, self.selPion],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Ombm2OmmPipPimDDL_Line = StrippingLine(
            "Ombm2OmmPipPimDDL_Line",
            algos=[Ombm2OmmPipPim_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Ombm2OmmPipPimDDL_Line)

        Ombm2OmmPipPim_DDD = self.createCombination3body(
            OutputList="Ombm2OmmPipPim_DDD" + self.name,
            DecayDescriptor="[Omega_b- -> Omega- pi+ pi-]cc",
            DaughterLists=[self.Omegaminus2LambdaK_LLL, self.selPion],
            Combination12Cut=combination12_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Ombm2OmmPipPimDDD_Line = StrippingLine(
            "Ombm2OmmPipPimDDD_Line",
            algos=[Ombm2OmmPipPim_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Ombm2OmmPipPimDDD_Line)

        ### Stripping Xi_b- / Omega_b- -> Omega- KS0 ###
        Xibm2OmmKS0_LLLLL = self.createCombination2body(
            OutputList="Xibm2OmmKS0_LLLLL" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- KS0]cc",
            DaughterLists=[self.Omegaminus2LambdaK_LLL, self.selKS0LL],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmKS0LLLLL_Line = StrippingLine(
            "Xibm2OmmKS0LLLLL_Line",
            algos=[Xibm2OmmKS0_LLLLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmKS0LLLLL_Line)

        Xibm2OmmKS0_LLLDD = self.createCombination2body(
            OutputList="Xibm2OmmKS0_LLLDD" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- KS0]cc",
            DaughterLists=[self.Omegaminus2LambdaK_LLL, self.selKS0DD],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmKS0LLLDDLine = StrippingLine(
            "Xibm2OmmKS0LLLDD_Line",
            algos=[Xibm2OmmKS0_LLLDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmKS0LLLDDLine)

        Xibm2OmmKS0_DDLLL = self.createCombination2body(
            OutputList="Xibm2OmmKS0_DDLLL" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- KS0]cc",
            DaughterLists=[self.Omegaminus2LambdaK_DDL, self.selKS0LL],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmKS0DDLLL_Line = StrippingLine(
            "Xibm2OmmKS0DDLLL_Line",
            algos=[Xibm2OmmKS0_DDLLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmKS0DDLLL_Line)

        Xibm2OmmKS0_DDLDD = self.createCombination2body(
            OutputList="Xibm2OmmKS0_DDLDD" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- KS0]cc",
            DaughterLists=[self.Omegaminus2LambdaK_DDL, self.selKS0DD],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmKS0DDLDDLine = StrippingLine(
            "Xibm2OmmKS0DDLDD_Line",
            algos=[Xibm2OmmKS0_DDLDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmKS0DDLDDLine)

        Xibm2OmmKS0_DDDLL = self.createCombination2body(
            OutputList="Xibm2OmmKS0_DDDLL" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- KS0]cc",
            DaughterLists=[self.Omegaminus2LambdaK_DDD, self.selKS0LL],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmKS0DDDLLLine = StrippingLine(
            "Xibm2OmmKS0DDDLL_Line",
            algos=[Xibm2OmmKS0_DDDLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmKS0DDDLLLine)

        Xibm2OmmKS0_DDDDD = self.createCombination2body(
            OutputList="Xibm2OmmKS0_DDDDD" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- KS0]cc",
            DaughterLists=[self.Omegaminus2LambdaK_DDD, self.selKS0DD],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmKS0DDDDD_Line = StrippingLine(
            "Xibm2OmmKS0DDDDD_Line",
            algos=[Xibm2OmmKS0_DDDDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmKS0DDDDD_Line)

        ### Stripping Xi_b- / Omega_b- -> Omega- Phi ###
        Xibm2OmmPhi_LLL = self.createCombination2body(
            OutputList="Xibm2OmmPhi_LLL" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- phi(1020)]cc",
            DaughterLists=[self.Omegaminus2LambdaK_LLL, self.selPhi],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmPhiLLL_Line = StrippingLine(
            "Xibm2OmmPhiLLL_Line",
            algos=[Xibm2OmmPhi_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmPhiLLL_Line)

        Xibm2OmmPhi_DDL = self.createCombination2body(
            OutputList="Xibm2OmmPhi_DDL" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- phi(1020)]cc",
            DaughterLists=[self.Omegaminus2LambdaK_DDL, self.selPhi],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmPhiDDL_Line = StrippingLine(
            "Xibm2OmmPhiDDL_Line",
            algos=[Xibm2OmmPhi_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmPhiDDL_Line)

        Xibm2OmmPhi_DDD = self.createCombination2body(
            OutputList="Xibm2OmmPhi_DDD" + self.name,
            DecayDescriptor="[Xi_b- -> Omega- phi(1020)]cc",
            DaughterLists=[self.Omegaminus2LambdaK_DDD, self.selPhi],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2OmmPhiDDD_Line = StrippingLine(
            "Xibm2OmmPhiDDD_Line",
            algos=[Xibm2OmmPhi_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xibm2OmmPhiDDD_Line)

        ### Stripping Xi_b0 / Omega_b- -> Omega- K+ ###
        Xib02OmmKp_LLL = self.createCombination2body(
            OutputList="Xib02OmmKp_LLL" + self.name,
            DecayDescriptor="[Xi_b0 -> Omega- K+]cc",
            DaughterLists=[self.Omegaminus2LambdaK_LLL, self.selKaon],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xib02OmmKpLLL_Line = StrippingLine(
            "Xib02OmmKpLLL_Line",
            algos=[Xib02OmmKp_LLL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xib02OmmKpLLL_Line)

        Xib02OmmKp_DDL = self.createCombination2body(
            OutputList="Xib02OmmKp_DDL" + self.name,
            DecayDescriptor="[Xi_b0 -> Omega- K+]cc",
            DaughterLists=[self.Omegaminus2LambdaK_DDL, self.selKaon],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xib02OmmKpDDL_Line = StrippingLine(
            "Xib02OmmKpDDL_Line",
            algos=[Xib02OmmKp_DDL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xib02OmmKpDDL_Line)

        Xib02OmmKp_DDD = self.createCombination2body(
            OutputList="Xib02OmmKp_DDD" + self.name,
            DecayDescriptor="[Xi_b0 -> Omega- K+]cc",
            DaughterLists=[self.Omegaminus2LambdaK_DDD, self.selKaon],
            DaughterCuts={},
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xib02OmmKpDDD_Line = StrippingLine(
            "Xib02OmmKpDDD_Line",
            algos=[Xib02OmmKp_DDD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'],
            RequiredRawEvents=self.config["RequiredRawEvents"])
        self.registerLine(Xib02OmmKpDDD_Line)

    ##########################################################################
    ## Combiners
    ##########################################################################

    def createCombination2body(self, OutputList, DecayDescriptor,
                               DaughterLists, DaughterCuts, PreVertexCuts,
                               PostVertexCuts):
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles(
            name='Combiner_for_' + self.name,
            DecayDescriptor=DecayDescriptor,
            DaughtersCuts=DaughterCuts,
            MotherCut=PostVertexCuts,
            CombinationCut=PreVertexCuts,
            ReFitPVs=True)
        return Selection(
            OutputList, Algorithm=combiner, RequiredSelections=DaughterLists)

    def createCombination2body_xim(self, OutputList, DaughterLists,
                                   DaughterCuts, PreVertexCuts,
                                   PostVertexCuts):
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles(
            name='Combiner_for_' + self.name,
            DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
            DaughtersCuts=DaughterCuts,
            MotherCut=PostVertexCuts,
            CombinationCut=PreVertexCuts,
            ReFitPVs=True)
        return Selection(
            OutputList, Algorithm=combiner, RequiredSelections=DaughterLists)

    def createCombination2body_omm(self, OutputList, DaughterLists,
                                   DaughterCuts, PreVertexCuts,
                                   PostVertexCuts):
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles(
            name='Combiner_for_' + self.name,
            DecayDescriptor="[Omega- -> Lambda0 K-]cc",
            DaughtersCuts=DaughterCuts,
            MotherCut=PostVertexCuts,
            CombinationCut=PreVertexCuts,
            ReFitPVs=True)
        return Selection(
            OutputList, Algorithm=combiner, RequiredSelections=DaughterLists)

    def createCombination3body(self, OutputList, DecayDescriptor,
                               DaughterLists, Combination12Cut, PreVertexCuts,
                               PostVertexCuts):
        ###create a selection using a DaVinci__N3BodyDecays with a single decay descriptor###
        combiner = DaVinci__N3BodyDecays(
            DecayDescriptor=DecayDescriptor,
            Combination12Cut=Combination12Cut,
            CombinationCut=PreVertexCuts,
            MotherCut=PostVertexCuts,
            ReFitPVs=True)
        return Selection(
            OutputList, Algorithm=combiner, RequiredSelections=DaughterLists)

    def createCombination5body(self, OutputList, DecayDescriptor,
                               DaughterLists, Combination12Cut,
                               Combination123Cut, Combination1234Cut,
                               PreVertexCuts, PostVertexCuts):
        ###create a selection using a DaVinci__N5BodyDecays with a single decay descriptor###
        combiner = DaVinci__N5BodyDecays(
            DecayDescriptor=DecayDescriptor,
            Combination12Cut=Combination12Cut,
            Combination123Cut=Combination123Cut,
            Combination1234Cut=Combination1234Cut,
            CombinationCut=PreVertexCuts,
            MotherCut=PostVertexCuts,
            ReFitPVs=True)
        return Selection(
            OutputList, Algorithm=combiner, RequiredSelections=DaughterLists)
