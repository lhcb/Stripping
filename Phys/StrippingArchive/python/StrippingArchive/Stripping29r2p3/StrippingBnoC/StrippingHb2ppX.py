###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping options for (pre-)selecting Lb -> p pbar n'

Authors: Ned Howarth, Eduardo Rodrigues
"""

########################################################################
__author__ = ['Ned Howarth', 'Eduardo Rodrigues']
__date__ = '22/06/2023'
__version__ = '$Revision: 1.1 $'

__all__ = ('Hb2ppXLines',
           'makeLb2Charged2Body',
           'makeLb2Charged2BodySS',
           'makeB2ppK',
           'default_config')

from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from StandardParticles                     import StdLooseProtons, StdNoPIDsKaons 

from PhysSelPython.Wrappers      import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils        import LineBuilder, checkConfig


default_config = {
    'NAME'        : 'Hb2ppX',
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'Hb2ppXLines',
    'CONFIG'      : {'PrescaleLb2Charged2Body'  : 1,
                     #Proton Cuts
                     "ProtonTRCHI2"        : 4.     ,#adimensional
                     "ProtonP"             : 15000. ,#MeV
                     "ProtonPT"            : 1000.  ,#MeV
                     "ProtonPIDK"          : 12.     ,#adimensional
                     "ProtonPIDp"          : 12.     ,#adimensional
                     "ProtonMINIPCHI2"     : 16.    ,#adminensional
                     "TRGHOSTPROB"         : 0.015   ,#adimensional
                     
                     #B Mother Cuts
                     "PPbarVCHI2DOF"           : 4.     ,#adminensional
                     "PPbarDIRA"               : 0.994  ,#adminensional
                     "PPbarFDCHI2HIGH"         : 150.   ,#adimensional
                     "PPbarPT"               : 1500.  #MeV
                    },
    'STREAMS'     : ['BhadronCompleteEvent']
}

class Hb2ppXLines( LineBuilder ) :
    """Class defining Hb -> p pbar X stripping lines"""
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__( self,name,config ) :        
        
        LineBuilder.__init__(self, name, config)
        
        Lb2Charged2BodyName     = name + "Lb2ppn"
        Lb2Charged2BodyNameSS   = name + "Lb2ppnSS"
        B2ppKName               = name + "B2ppK"
        
        # make the various stripping selections
        self.Lb2Charged2Body = makeLb2Charged2Body( Lb2Charged2BodyName,
                                                    config['PPbarVCHI2DOF'],
                                                    config['PPbarDIRA'],
                                                    config['PPbarFDCHI2HIGH'],
                                                    config['PPbarPT'],

                                                    config['ProtonTRCHI2'],
                                                    config['ProtonP'],
                                                    config['ProtonPT'],
                                                    config['TRGHOSTPROB'],
                                                    config['ProtonPIDp'],
                                                    config['ProtonPIDK'],
                                                    config['ProtonMINIPCHI2'])
        
        self.Lb2Charged2BodySS = makeLb2Charged2BodySS( Lb2Charged2BodyNameSS,
                                                    config['PPbarVCHI2DOF'],
                                                    config['PPbarDIRA'],
                                                    config['PPbarFDCHI2HIGH'],
                                                    config['PPbarPT'],

                                                    config['ProtonTRCHI2'],
                                                    config['ProtonP'],
                                                    config['ProtonPT'],
                                                    config['TRGHOSTPROB'],
                                                    config['ProtonPIDp'],
                                                    config['ProtonPIDK'],
                                                    config['ProtonMINIPCHI2'])
        self.B2ppK = makeB2ppK( B2ppKName,
                                                    config['PPbarVCHI2DOF'],
                                                    config['PPbarDIRA'],
                                                    config['PPbarFDCHI2HIGH'],
                                                    config['PPbarPT'],

                                                    config['ProtonTRCHI2'],
                                                    config['ProtonP'],
                                                    config['ProtonPT'],
                                                    config['TRGHOSTPROB'],
                                                    config['ProtonPIDp'],
                                                    config['ProtonPIDK'],
                                                    config['ProtonMINIPCHI2'])


        self.lineLb2Charged2Body = StrippingLine( Lb2Charged2BodyName+"Line",
                                                  prescale  = config['PrescaleLb2Charged2Body'],
                                                  selection = self.Lb2Charged2Body,
                                                  EnableFlavourTagging = False,
                                                  RequiredRawEvents = ["Velo"])

        self.lineLb2Charged2BodySS = StrippingLine( Lb2Charged2BodyNameSS+"Line",
                                                  prescale  = config['PrescaleLb2Charged2Body'],
                                                  selection = self.Lb2Charged2BodySS,
                                                  EnableFlavourTagging = False,
                                                  RequiredRawEvents = ["Velo"])
        
        self.lineB2ppK = StrippingLine( B2ppKName +"Line",
                                                  prescale  = config['PrescaleLb2Charged2Body'],
                                                  selection = self.B2ppK,
                                                  EnableFlavourTagging = False,
                                                  RequiredRawEvents = ["Velo"])

        self.registerLine(self.lineLb2Charged2Body)
        self.registerLine(self.lineLb2Charged2BodySS)
        self.registerLine(self.lineB2ppK)

def makeLb2Charged2Body( name,
                         PPbarVCHI2DOF,PPbarDIRA,PPbarFDCHI2HIGH,PPbarPT,
                         ProtonTRCHI2,ProtonP,ProtonPT,TRGHOSTPROB,ProtonPIDp,ProtonPIDK,ProtonMINIPCHI2 ) :
    
    _daughters_cuts = " (TRCHI2DOF < %(ProtonTRCHI2)s ) \
                      & (P> %(ProtonP)s *MeV) \
                      & (PT> %(ProtonPT)s *MeV) \
                      & (TRGHOSTPROB < %(TRGHOSTPROB)s) \
                      & (PIDp-PIDpi> %(ProtonPIDp)s ) \
                      & (PIDp-PIDK> %(ProtonPIDK)s ) \
                      & (MIPCHI2DV(PRIMARY)> %(ProtonMINIPCHI2)s )"%locals()

    _mother_cuts =    " (VFASPF(VCHI2/VDOF)< %(PPbarVCHI2DOF)s) \
                      & (BPVDIRA> %(PPbarDIRA)s) \
                      & (PT > %(PPbarPT)s) \
                      & (BPVVDCHI2 >%(PPbarFDCHI2HIGH)s)"%locals()

    CombineLb2Charged2Body = CombineParticles( DecayDescriptor = 'Lambda_b0 -> p+ p~-',
                                               DaughtersCuts = { "p+" : _daughters_cuts },
                                               MotherCut = _mother_cuts )
    CombineLb2Charged2Body.ReFitPVs = True

    return Selection( name,
                      Algorithm = CombineLb2Charged2Body,
                      RequiredSelections = [ StdLooseProtons ] )

def makeLb2Charged2BodySS( name,
                           PPbarVCHI2DOF,PPbarDIRA,PPbarFDCHI2HIGH,PPbarPT,
                           ProtonTRCHI2,ProtonP,ProtonPT,TRGHOSTPROB,ProtonPIDp,ProtonPIDK,ProtonMINIPCHI2 ) :

    _daughters_cuts = " (TRCHI2DOF < %(ProtonTRCHI2)s ) \
                      & (P> %(ProtonP)s *MeV) \
                      & (PT> %(ProtonPT)s *MeV) \
                      & (TRGHOSTPROB < %(TRGHOSTPROB)s) \
                      & (PIDp-PIDpi> %(ProtonPIDp)s ) \
                      & (PIDp-PIDK> %(ProtonPIDK)s ) \
                      & (MIPCHI2DV(PRIMARY)> %(ProtonMINIPCHI2)s )"%locals()

    _mother_cuts =    " (VFASPF(VCHI2/VDOF)< %(PPbarVCHI2DOF)s) \
                      & (BPVDIRA> %(PPbarDIRA)s) \
                      & (PT > %(PPbarPT)s) \
                      & (BPVVDCHI2 >%(PPbarFDCHI2HIGH)s)"%locals()

    CombineLb2Charged2Body = CombineParticles( DecayDescriptor = '[Lambda_b0 -> p+ p+]cc',
                                               DaughtersCuts = { "p+" : _daughters_cuts },
                                               MotherCut = _mother_cuts )
    CombineLb2Charged2Body.ReFitPVs = True
    return Selection( name,
                      Algorithm = CombineLb2Charged2Body,
                      RequiredSelections = [ StdLooseProtons ] )

def makeB2ppK( name,
               PPbarVCHI2DOF,PPbarDIRA,PPbarFDCHI2HIGH,PPbarPT,
               ProtonTRCHI2,ProtonP,ProtonPT,TRGHOSTPROB,ProtonPIDp,ProtonPIDK,ProtonMINIPCHI2) :

    _daughters_cuts = " (TRCHI2DOF < %(ProtonTRCHI2)s ) \
                      & (P> %(ProtonP)s *MeV) \
                      & (PT> %(ProtonPT)s *MeV) \
                      & (TRGHOSTPROB < %(TRGHOSTPROB)s) \
                      & (PIDp-PIDpi> %(ProtonPIDp)s ) \
                      & (PIDp-PIDK> %(ProtonPIDK)s ) \
                      & (MIPCHI2DV(PRIMARY)> %(ProtonMINIPCHI2)s )"%locals()

    _mother_cuts =    " (VFASPF(VCHI2/VDOF)< %(PPbarVCHI2DOF)s) \
                      & (BPVDIRA> %(PPbarDIRA)s) \
                      & (PT > %(PPbarPT)s) \
                      & (BPVVDCHI2 >%(PPbarFDCHI2HIGH)s)"%locals()

    CombineB2ppK = CombineParticles( DecayDescriptor = '[B+ -> p+ p~- K+]cc',
                                               DaughtersCuts = { "p+" : _daughters_cuts},
                                               MotherCut = _mother_cuts )
    CombineB2ppK.ReFitPVs = True
    return Selection( name,
                      Algorithm = CombineB2ppK,
                      RequiredSelections = [ StdNoPIDsKaons, StdLooseProtons ])
    
########################################################################

