###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# $Id: StrippingB2L0phh.py,v 1.0 2011-02-8 12:53:17 roldeman Exp $
__author__ = ['Liang Sun']
__date__ = '08/06/2023'
__version__ = '$Revision: 1.0 $'

__all__ = ('B2L0phhConf',
           'makeB2L0phh',
           'makePi',
           'makeK',
           'makep',
           'default_config')


default_config ={
  'B2l0phh': {
    'BUILDERTYPE' : 'B2L0phhConf',
    'CONFIG' : { 'nbody':                 4,
                  'MinBMass':        4500.0,
                  'MaxBMass':        7000.0,
                  'MinBPt':          1200.0,
                  'MaxBVertChi2DOF':   15.0,
                  'MinBPVVDChi2':     20.0, # Flight CHI2
                  'MaxBPVIPChi2':      12.0,
                  'MinBPVDIRA':         0.999,
                  'MaxMass':         6000.0,
                  'doPi':              True,
                  'doK':               True,
                  'prescale':           1.0,
                  'MaxTrLong':           250,
                  'MinPiPt':        300.0,
                  'MinPiIPChi2DV':     5.0,
                  'MaxPiChi2':          3.0,
                  'MinPiPIDK':          -2.0,
                  'MinPiPIDp':          -2.0,
                  'MaxPiGHP':           0.25,
                  'MinKPt':          200.0,
                  'MinKIPChi2DV':      5.0,
                  'MaxKChi2':           3.0,
                  'MinKPIDPi':          -2.0,
                  'MinKPIDp':           -2.0,
                  'MaxKGHP':            0.3,
                  'MinpPt':          250.0,
                  'MinpIPChi2DV':      5.0,
                  'MaxpChi2':           3.0,
                  'MinpPIDPi':          -2.0,
                  'MinpPIDK':           -2.0,
                  'MaxpGHP':            0.3,
                  'MaxLmDeltaM':       18.0,
                  'MinLmPt':          400.0,
                  'MaxLmVertChi2DOF':  15.0,
                  'MinLmPVVDChi2':    12.0,
                  'MinLmIPChi2':        0.0,
                  'MinLmPrtPt':       300.0,
                  'MinLmPiPt':        100.0,
                  'MinLmPrtPIDPi':     -3.0,
                  'MinLmPrtIPChi2':     4.0,
                  'MinLmPiIPChi2':     4.0,
                  'MaxLmPrtTrkChi2':    4.0,
                  'MaxLmPiTrkChi2':     4.0,
                  'RelatedInfoTools' : [    { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 1.7,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar17'
                                            },
                                          { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 1.5,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar15'
                                            },
                                          { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 1.0,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar10'
                                            },
                                          { "Type" : "RelInfoConeVariables",
                                            "ConeAngle" : 0.8,
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                            "Location"  : 'ConeVar08'
                                            },
                                          { "Type" : "RelInfoVertexIsolation",
                                            "Location" : "VtxIsolationVar"
                                            }
                                          ]
                  },
    'STREAMS' : [ 'Bhadron' ],
    'WGs' : [ 'BnoC' ]
    }
}

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, MergedSelection, DataOnDemand, VoidEventSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


class B2L0phhConf(LineBuilder) :

    __configuration_keys__ = default_config['B2l0phh']['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        self.selPi = makePi( 'PiFor'+name,
                             MinPiPt      =config['MinPiPt'],
                             MinPiIPChi2DV=config['MinPiIPChi2DV'],
                             MaxPiChi2    =config['MaxPiChi2'],
                             MinPiPIDK    =config['MinPiPIDK'],
                             MinPiPIDp    =config['MinPiPIDp'],
                             MaxPiGHP     =config['MaxPiGHP'])
        self.selK = makeK( 'KFor'+name,
                             MinKPt      =config['MinKPt'],
                             MinKIPChi2DV=config['MinKIPChi2DV'],
                             MaxKChi2    =config['MaxKChi2'],
                             MinKPIDPi   =config['MinKPIDPi'],
                             MinKPIDp    =config['MinKPIDp'],
                             MaxKGHP     =config['MaxKGHP'])
        self.selp = makep( 'pFor'+name,
                             MinpPt      =config['MinpPt'],
                             MinpIPChi2DV=config['MinpIPChi2DV'],
                             MaxpChi2    =config['MaxpChi2'],
                             MinpPIDPi   =config['MinpPIDPi'],
                             MinpPIDK    =config['MinpPIDK'],
                             MaxpGHP     =config['MaxpGHP'])
        self.selLm = makeLm('LmFor'+name,
                            MaxLmDeltaM     =config['MaxLmDeltaM'],
                            MinLmPt         =config['MinLmPt'],
                            MaxLmVertChi2DOF=config['MaxLmVertChi2DOF'],
                            MinLmPVVDChi2   =config['MinLmPVVDChi2'],
                            MinLmIPChi2     =config['MinLmIPChi2'],
                            MinLmPrtPt      =config['MinLmPrtPt'],
                            MinLmPiPt       =config['MinLmPiPt'],
                            MinLmPrtPIDPi       =config['MinLmPrtPIDPi'],
                            MinLmPrtIPChi2  =config['MinLmPrtIPChi2'],
                            MinLmPiIPChi2   =config['MinLmPiIPChi2'],
                            MaxLmPrtTrkChi2 =config['MaxLmPrtTrkChi2'],
                            MaxLmPiTrkChi2  =config['MaxLmPiTrkChi2'])

        self.selLm_LL = makeLm('LmLLFor'+name,
                            MaxLmDeltaM     =config['MaxLmDeltaM'],
                            MinLmPt         =config['MinLmPt'],
                            MaxLmVertChi2DOF=config['MaxLmVertChi2DOF'],
                            MinLmPVVDChi2   =config['MinLmPVVDChi2'],
                            MinLmIPChi2     =config['MinLmIPChi2'],
                            MinLmPrtPt      =config['MinLmPrtPt'],
                            MinLmPiPt       =config['MinLmPiPt'],
                            MinLmPrtPIDPi       =config['MinLmPrtPIDPi'],
                            MinLmPrtIPChi2  =config['MinLmPrtIPChi2'],
                            MinLmPiIPChi2   =config['MinLmPiIPChi2'],
                            MaxLmPrtTrkChi2 =config['MaxLmPrtTrkChi2'],
                            MaxLmPiTrkChi2  =config['MaxLmPiTrkChi2'],
                            doLL = True, doDD = False)

        self.selLm_DD = makeLm('LmDDFor'+name,
                            MaxLmDeltaM     =config['MaxLmDeltaM'],
                            MinLmPt         =config['MinLmPt'],
                            MaxLmVertChi2DOF=config['MaxLmVertChi2DOF'],
                            MinLmPVVDChi2   =config['MinLmPVVDChi2'],
                            MinLmIPChi2     =config['MinLmIPChi2'],
                            MinLmPrtPt      =config['MinLmPrtPt'],
                            MinLmPiPt       =config['MinLmPiPt'],
                            MinLmPrtPIDPi       =config['MinLmPrtPIDPi'],
                            MinLmPrtIPChi2  =config['MinLmPrtIPChi2'],
                            MinLmPiIPChi2   =config['MinLmPiIPChi2'],
                            MaxLmPrtTrkChi2 =config['MaxLmPrtTrkChi2'],
                            MaxLmPiTrkChi2  =config['MaxLmPiTrkChi2'],
                            doLL = False, doDD = True)


        self.selB2L0L0hh = makeB2L0L0hh(name,
                                     PiSel=self.selPi,
                                     KSel =self.selK,
                                     pSel =self.selp,
                                     LmSel=self.selLm,
                                     nbody          =config['nbody'],
                                     MinBMass       =config['MinBMass'],
                                     MaxBMass       =config['MaxBMass'],
                                     MinBPt         =config['MinBPt'],
                                     MaxBVertChi2DOF=config['MaxBVertChi2DOF'],
                                     MinBPVVDChi2   =config['MinBPVVDChi2'],
                                     MaxBPVIPChi2   =config['MaxBPVIPChi2'],
                                     MinBPVDIRA     =config['MinBPVDIRA'],
                                     MaxMass        =config['MaxMass'],
                                     doPi           =config['doPi'],
                                     doK            =config['doK']
                                     )
        self.selB2L0phh_LL = makeB2L0phh(name+'_LL',
                                     PiSel=self.selPi,
                                     KSel =self.selK,
                                     pSel =self.selp,
                                     LmSel=self.selLm_LL,
                                     nbody          =config['nbody'],
                                     MinBMass       =config['MinBMass'],
                                     MaxBMass       =config['MaxBMass'],
                                     MinBPt         =config['MinBPt'],
                                     MaxBVertChi2DOF=config['MaxBVertChi2DOF'],
                                     MinBPVVDChi2   =config['MinBPVVDChi2'],
                                     MaxBPVIPChi2   =config['MaxBPVIPChi2'],
                                     MinBPVDIRA     =config['MinBPVDIRA'],
                                     MaxMass        =config['MaxMass'],
                                     doPi           =config['doPi'],
                                     doK            =config['doK']
                                     )
        self.selB2L0phh_DD = makeB2L0phh(name+'_DD',
                                     PiSel=self.selPi,
                                     KSel =self.selK,
                                     pSel =self.selp,
                                     LmSel=self.selLm_DD,
                                     nbody          =config['nbody'],
                                     MinBMass       =config['MinBMass'],
                                     MaxBMass       =config['MaxBMass'],
                                     MinBPt         =config['MinBPt'],
                                     MaxBVertChi2DOF=config['MaxBVertChi2DOF'],
                                     MinBPVVDChi2   =config['MinBPVVDChi2'],
                                     MaxBPVIPChi2   =config['MaxBPVIPChi2'],
                                     MinBPVDIRA     =config['MinBPVDIRA'],
                                     MaxMass        =config['MaxMass'],
                                     doPi           =config['doPi'],
                                     doK            =config['doK']
                                     )
        TrLongFilter={'Code' : "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %s )"% config['MaxTrLong'],
                      'Preambulo' : [ "from LoKiTracks.decorators import *",'from LoKiCore.functions import *' ]}

        for name1,sel1 in self.selB2L0phh_LL.items():
            line = StrippingLine(name1+"Line",
                                  selection = sel1, #self.selB2L0phh[name1],
                                  prescale = config['prescale'],
                                  RelatedInfoTools = config['RelatedInfoTools'],
                                  FILTER = TrLongFilter )
            self.registerLine(line)
        for name1,sel1 in self.selB2L0phh_DD.items():
            line = StrippingLine(name1+"Line",
                                  selection = sel1, #self.selB2L0phh[name1],
                                  prescale = config['prescale'],
                                  RelatedInfoTools = config['RelatedInfoTools'],
                                  FILTER = TrLongFilter )
            self.registerLine(line)
        for name1,sel1 in self.selB2L0L0hh.items():
            line = StrippingLine(name1+"Line",
                                  selection = sel1, #self.selB2L0phh[name1],
                                  prescale = config['prescale'],
                                  RelatedInfoTools = config['RelatedInfoTools'],
                                  FILTER = TrLongFilter )
            self.registerLine(line)



def makeB2L0phh(name,PiSel,KSel,pSel,LmSel,
                nbody,MinBMass,MaxBMass,MinBPt,MaxBVertChi2DOF,MinBPVVDChi2,MaxBPVIPChi2,MinBPVDIRA,MaxMass,doPi,doK):

    descriptors = ['[B+ -> Lambda~0 p+ p+ p~-]cc']
    if doPi:
        descriptors.append('[B+ -> Lambda~0 p+ pi+ pi-]cc')
        descriptors.append('[B+ -> Lambda0 p~- pi+ pi+]cc')
    if doK:
        descriptors.append('[B+ -> Lambda~0 p+ K+ K-]cc')
        descriptors.append('[B+ -> Lambda0 p~- K+ K+]cc')
    if doPi and doK:
        descriptors.append('[B- -> Lambda0 p~- K- pi+]cc')
        descriptors.append('[B- -> Lambda0 p~- K+ pi-]cc')
        descriptors.append('[B+ -> Lambda0 p~- K+ pi+]cc')

    #for descriptor in descriptors: print "DESCRIPTOR:",descriptor

    #make a merge of the input selections
    AllSel=[LmSel, pSel]
    if doPi:AllSel.append(PiSel)
    if doK: AllSel.append(KSel)
    InputSel= MergedSelection("InputFor"+name, RequiredSelections = AllSel )

    _combinationCuts =    "(in_range(%(MinBMass)s*MeV, AM, %(MaxBMass)s*MeV))" % locals()
    _combinationCuts += "& (APT>%(MinBPt)s*MeV)"%locals()
    _motherCuts = "(VFASPF(VCHI2/VDOF)<%(MaxBVertChi2DOF)s)"%locals()
    _motherCuts += "& (BPVVDCHI2 > %(MinBPVVDChi2)s)"% locals()
    _motherCuts += "& (BPVIPCHI2() < %(MaxBPVIPChi2)s)"%locals()
    _motherCuts += "& (BPVDIRA > %(MinBPVDIRA)s)"% locals()

    #make a selection
    selections = {}
    part_dicts = {
            'p+' : 'p',
            'p~-' : 'pbar',
            'K+':'Kp',
            'K-':'Km',
            'pi+':'pip',
            'pi-':'pim'}
    for desc in descriptors:
        name1 = name + '_'
        name1 += part_dicts[desc.split(']')[0].split()[-2]]
        name1 += part_dicts[desc.split(']')[0].split()[-1]]
        _B=CombineParticles(DecayDescriptors = [desc], CombinationCut = _combinationCuts, MotherCut = _motherCuts)
        #make a preselection
        _presel=VoidEventSelection("preselFor"+name1,
                               Code="(CONTAINS('%s')> %3.1f)"%(InputSel.outputLocation(),nbody-0.5 ),
                               RequiredSelection=InputSel
                               )
        selections[name1] = Selection(name1, Algorithm = _B, RequiredSelections = [_presel] )

    return selections

def makeB2L0L0hh(name,PiSel,KSel,pSel,LmSel,
                nbody,MinBMass,MaxBMass,MinBPt,MaxBVertChi2DOF,MinBPVVDChi2,MaxBPVIPChi2,MinBPVDIRA,MaxMass,doPi,doK):

    descriptors = ['B0 -> Lambda0 Lambda~0 p+ p~-']
    if doPi:
        descriptors.append('B0 -> Lambda0 Lambda~0 pi+ pi-')
    if doK:
        descriptors.append('B0 -> Lambda0 Lambda~0 K+ K-')
    if doPi and doK:
        descriptors.append('[B0 -> Lambda0 Lambda~0 K+ pi-]cc')

    #for descriptor in descriptors: print "DESCRIPTOR:",descriptor

    #make a merge of the input selections
    AllSel=[LmSel, pSel]
    if doPi:AllSel.append(PiSel)
    if doK: AllSel.append(KSel)
    InputSel= MergedSelection("InputFor"+name, RequiredSelections = AllSel )

    _combinationCuts =    "(in_range(%(MinBMass)s*MeV, AM, %(MaxBMass)s*MeV))" % locals()
    _combinationCuts += "& (APT>%(MinBPt)s*MeV)"%locals()
    _motherCuts = "(VFASPF(VCHI2/VDOF)<%(MaxBVertChi2DOF)s)"%locals()
    _motherCuts += "& (BPVVDCHI2 > %(MinBPVVDChi2)s)"% locals()
    _motherCuts += "& (BPVIPCHI2() < %(MaxBPVIPChi2)s)"%locals()
    _motherCuts += "& (BPVDIRA > %(MinBPVDIRA)s)"% locals()

    #make a selection
    selections = {}
    part_dicts = {
            'p+' : 'p',
            'p~-' : 'pbar',
            'K+':'Kp',
            'K-':'Km',
            'pi+':'pip',
            'pi-':'pim'}
    name = name.replace('l0p','l0l0')
    for desc in descriptors:
        name1 = name + '_'
        name1 += part_dicts[desc.split(']')[0].split()[-2]]
        name1 += part_dicts[desc.split(']')[0].split()[-1]]
        _B=CombineParticles(DecayDescriptors = [desc], CombinationCut = _combinationCuts, MotherCut = _motherCuts)
        #make a preselection
        _presel=VoidEventSelection("preselFor"+name1,
                               Code="(CONTAINS('%s')> %3.1f)"%(InputSel.outputLocation(),nbody-0.5 ),
                               RequiredSelection=InputSel
                               )
        selections[name1] = Selection(name1, Algorithm = _B, RequiredSelections = [_presel] )

    return selections

from StandardParticles import StdLoosePions
def makePi(name, MinPiPt, MinPiIPChi2DV, MaxPiChi2, MinPiPIDK, MinPiPIDp, MaxPiGHP) :
    _code =  "(PT > %(MinPiPt)s*MeV)"%locals()
    _code+="& (MIPCHI2DV(PRIMARY) > %(MinPiIPChi2DV)s)"%locals()
    _code+="& (TRCHI2DOF<%(MaxPiChi2)s)"%locals()
    _code+="& (PIDpi-PIDK > %(MinPiPIDK)s)"%locals()
    #make a preselection
    _code+="& (PIDpi-PIDp > %(MinPiPIDp)s)"%locals()
    _code+="& (TRGHP < %(MaxPiGHP)s)"%locals()
    _PiFilter=FilterDesktop(Code=_code)
    return Selection (name,  Algorithm = _PiFilter, RequiredSelections = [StdLoosePions])

from StandardParticles import StdLooseKaons
def makeK(name, MinKPt, MinKIPChi2DV, MaxKChi2, MinKPIDPi, MinKPIDp, MaxKGHP) :
    _code =  "(PT > %(MinKPt)s*MeV)"%locals()
    _code+="& (MIPCHI2DV(PRIMARY) > %(MinKIPChi2DV)s)"%locals()
    _code+="& (TRCHI2DOF<%(MaxKChi2)s)"%locals()
    _code+="& (PIDK-PIDpi > %(MinKPIDPi)s)"%locals()
    _code+="& (PIDK-PIDp > %(MinKPIDp)s)"%locals()
    _code+="& (TRGHP < %(MaxKGHP)s)"%locals()
    _KFilter=FilterDesktop(Code=_code)
    return Selection (name,  Algorithm = _KFilter, RequiredSelections = [StdLooseKaons])

from StandardParticles import StdLooseProtons
def makep(name, MinpPt, MinpIPChi2DV, MaxpChi2, MinpPIDPi, MinpPIDK, MaxpGHP) :
    _code =  "(PT > %(MinpPt)s*MeV)"%locals()
    _code+="& (MIPCHI2DV(PRIMARY) > %(MinpIPChi2DV)s)"%locals()
    _code+="& (TRCHI2DOF<%(MaxpChi2)s)"%locals()
    _code+="& (PIDp-PIDpi > %(MinpPIDPi)s)"%locals()
    _code+="& (PIDp-PIDK > %(MinpPIDK)s)"%locals()
    _code+="& (TRGHP < %(MaxpGHP)s)"%locals()
    _pFilter=FilterDesktop(Code=_code)
    return Selection (name,  Algorithm = _pFilter, RequiredSelections = [StdLooseProtons])

from StandardParticles import StdVeryLooseLambdaLL, StdLooseLambdaDD
def makeLm(name, MaxLmDeltaM, MinLmPt, MaxLmVertChi2DOF, MinLmPVVDChi2, MinLmIPChi2, MinLmPrtPt, MinLmPiPt, MinLmPrtPIDPi, MinLmPrtIPChi2, MinLmPiIPChi2, MaxLmPrtTrkChi2, MaxLmPiTrkChi2, doLL = True, doDD = True) :
    _code =  "(ADMASS('Lambda0') < %(MaxLmDeltaM)s*MeV)"%locals()
    _code+="& (PT>%(MinLmPt)s*MeV)"%locals()
    _code+="& (VFASPF(VCHI2/VDOF)<%(MaxLmVertChi2DOF)s)"%locals()
    _code+="& (BPVVDCHI2>%(MinLmPVVDChi2)s)"%locals()
    _code+="& (MIPCHI2DV(PRIMARY)>%(MinLmIPChi2)s)"%locals()
    _code+="& CHILDCUT ( PT > %(MinLmPrtPt)s , 1 )"%locals()
    _code+="& CHILDCUT ( PT > %(MinLmPiPt)s , 2 )"%locals()
    _code+="& CHILDCUT ( PIDp-PIDpi > %(MinLmPrtPIDPi)s , 1 )"%locals()
    _code+="& CHILDCUT ( MIPCHI2DV ( PRIMARY ) > %(MinLmPrtIPChi2)s , 1 )"%locals()
    _code+="& CHILDCUT ( MIPCHI2DV ( PRIMARY ) > %(MinLmPiIPChi2)s , 2 )"%locals()
    _code+="& CHILDCUT ( TRCHI2DOF < %(MaxLmPrtTrkChi2)s , 1 )"%locals()
    _code+="& CHILDCUT ( TRCHI2DOF < %(MaxLmPiTrkChi2)s , 2 )"%locals()
    _LmFilter = FilterDesktop(Code = _code)
    _stdLmLL    = DataOnDemand(Location = "Phys/StdVeryLooseLambdaLL/Particles")
    _stdLmDD    = DataOnDemand(Location = "Phys/StdLooseLambdaDD/Particles")
    _mergeLm = MergedSelection( "MergedLmFor" + name, RequiredSelections = [_stdLmLL, _stdLmDD] )



    if doLL and not doDD: return Selection(name, Algorithm = _LmFilter, RequiredSelections = [_stdLmLL])
    elif doDD and not doLL: return Selection(name, Algorithm = _LmFilter, RequiredSelections = [_stdLmDD])
    else: return Selection(name, Algorithm = _LmFilter, RequiredSelections = [_mergeLm])
