###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
1)Xi_b- -> Xi_c0 pi-, Xi_c0 -> p+ K-

'''

__author__ = ['Xiao-Rui Lyu', 'Miroslav Saur', 'Ziyi Wang', 'Pei-Rong Li', 'Ying-Hao Wang']
__date__ = '2023/07/06'
__version__ = '$Revision: 0.1 $'
__all__ = ('StrippingXib2Xic0PiXic02pKConf'
           ,'default_config')


from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import  StdAllLoosePions, StdAllLooseKaons, StdAllLooseProtons

from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from PhysSelPython.Wrappers import MultiSelectionSequence
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, mm, picosecond


default_name='Xib2Xic0PiXic02pK'
    #### This is the dictionary of all tunable cuts ########
default_config={
      'NAME'        :   'Xib2Xic0PiXic02pK',
      'WGs'         :   ['Charm'],
      'BUILDERTYPE' : 'StrippingXib2Xic0PiXic02pKConf',
      'STREAMS'     : ['Charm'],
      'CONFIG'      : {
                     'TRCHI2DOFMax'           : 3.0
                   , 'PionPIDK'               :  10.0
                   , 'tight_ProtonPIDp'       : 7 #to be studied
                   , 'tight_ProtonPIDppi'     :  5.0
                   , 'tight_ProtonPIDpK'      :  0.0
                   , 'tight_KaonPIDpi'        :  5.0
                   , 'tight_KaonPIDK'         :  0.0
                   , 'TrGhostProbMax'         :  0.25
                   , 'ProbNNkMin'             :  0.10
      }## end of 'CONFIG'
}## end of default_config

#-------------------------------------------------------------------------------------------------------------
class StrippingXib2Xic0PiXic02pKConf(LineBuilder) :
        __configuration_keys__ = default_config['CONFIG'].keys()

        def __init__(self, name, config) :
            LineBuilder.__init__(self, name, config)
            self.name = name
            self.config = config

            # take all long tracks - Long Protons
            self.LongProtonsList = MergedSelection("LongProtonsFor" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdAllLooseProtons/Particles")])

            # take all long tracks - Long Pions
            self.LongPionsList = MergedSelection("LongPionsFor" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdAllLoosePions/Particles")])

            # take all long tracks - Long Kaons
            self.LongKaonsList = MergedSelection("LongKaonsFor" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdAllLooseKaons/Particles")])

            # Good tracks - Tight Protons
            self.GoodTightProtonsList = self.createSubSel( OutputList = "GoodTightProtonsFor" + self.name,
                                                InputList = self.LongProtonsList,
                                                Cuts = "(TRCHI2DOF < %(TRCHI2DOFMax)s )"\
                                                " & (TRGHOSTPROB <%(TrGhostProbMax)s )" \
                                                " & (PIDp > %(tight_ProtonPIDp)s) "\
                                                " & ((PIDp-PIDpi) > %(tight_ProtonPIDppi)s) "\
                                                " & ((PIDp-PIDK)  > %(tight_ProtonPIDpK)s) " % self.config )
            # Good tracks - Long Pions
            self.GoodLongPionsList = self.createSubSel( OutputList = "GoodLongPionsFor" + self.name,
                                                InputList = self.LongPionsList,
                                                Cuts = "(TRCHI2DOF < %(TRCHI2DOFMax)s )"\
                                                " & (TRGHOSTPROB <%(TrGhostProbMax)s )" \
                                                " & (PIDK < %(PionPIDK)s )" % self.config )

            # Good tracks - Tight Kaons
            self.GoodTightKaonsList = self.createSubSel( OutputList = "GoodTightKaonsFor" + self.name,
                                                InputList = self.LongKaonsList,
                                                Cuts = "(TRCHI2DOF < %(TRCHI2DOFMax)s )"\
                                                " & (TRGHOSTPROB < %(TrGhostProbMax)s )" \
                                                " & (PROBNNk > %(ProbNNkMin)s )" \
                                                " & ((PIDK-PIDpi)>%(tight_KaonPIDpi)s)"\
                                                " & (PIDK > %(tight_KaonPIDK)s )" % self.config )

            # Xi_c0 -> p+ K- list
            self.Xic0List = self.createCombinationSel(OutputList = "Xic0For"+ self.name,
                                                         DecayDescriptor = "[Xi_c0 -> p+ K-]cc",
                                                         DaughterLists   = [self.GoodTightProtonsList, self.GoodTightKaonsList],
                                                         DaughterCuts    = {"K-"      : " (MIPCHI2DV(PRIMARY)>4)",
                                                                            "p+"     : " (MIPCHI2DV(PRIMARY)>4)"},
                                                         PreVertexCuts   = "(ADAMASS('Xi_c0') < 120 *MeV) & (ADOCACHI2CUT(30, '')) & (ADOCAMAX('')<2.5*mm)",
                                                         PostVertexCuts  = "(ADMASS('Xi_c0') < 90 *MeV) &(VFASPF(VCHI2/VDOF)< 12.) & (BPVVDCHI2 > 16.0) & (BPVDIRA >0.999) "
                                                         )

            self.XibList = self.makeXib()

        #------------------------------------------------------------------------------------------
        #------------------------------------------------------------------------------------------

        def createSubSel( self, OutputList, InputList, Cuts ) :
            '''create a selection using a FilterDesktop'''
            filter = FilterDesktop(Code = Cuts)
            return Selection( OutputList,
                             Algorithm = filter,
                             RequiredSelections = [ InputList ] )

        def createCombinationSel( self, OutputList,
                                 DecayDescriptor,
                                 DaughterLists,
                                 DaughterCuts = {} ,
                                 PreVertexCuts = "ALL",
                                 PostVertexCuts = "ALL" ) :
            '''create a selection using a ParticleCombiner with a single decay descriptor'''
            combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                        DaughtersCuts = DaughterCuts,
                                        MotherCut = PostVertexCuts,
                                        CombinationCut = PreVertexCuts,
                                        ReFitPVs = True)
            return Selection ( OutputList,
                              Algorithm = combiner,
                              RequiredSelections = DaughterLists)

        def makeXib( self ):

            ''' Stripping Xi_b- -> Xi_c0 pi-'''
            Xib2Xic0piXic02pK = self.createCombinationSel(OutputList = "Xib2Xic0piXic02pK"+ self.name,
                                                         DecayDescriptor = "[Xi_b- -> Xi_c0 pi-]cc",
                                                         DaughterLists   = [self.Xic0List, self.GoodLongPionsList],
                                                         DaughterCuts    = {"pi-"      : " (MIPCHI2DV(PRIMARY)>4)"
                                                         },
                                                         PreVertexCuts   = "(ADAMASS('Xi_b-') < 800 *MeV)",
                                                         PostVertexCuts  = "(ADMASS('Xi_b-') < 400 *MeV)"
                                                         )
            Xib2Xic0piXic02pKLine = StrippingLine( self.name + "Xib2Xic0piXic02pKLine", algos = [ Xib2Xic0piXic02pK ], EnableFlavourTagging = True )
            self.registerLine (Xib2Xic0piXic02pKLine)

        #------------------------------------------------------------------------------------------
