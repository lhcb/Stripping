###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = 'Max Chefdeville'
__date__ = '03/09/2021'
__version__ = '$Revision: 1.0 $'

'''
Stripping selection for D0 -> K pi pi0
'''
####################################################################
# Stripping selection for D0 -> K pi pi0
# copy of the module StrippingD02KPiPi0.py from Regis Lefevre
# modif: remove photon PID cut, enlarge resolved pi0 sidebands,
#        remove line for merged pi0s and prescale (0.5) on resolved pi0 line
####################################################################

__all__ = ('StrippingD02KPiPi0ForPidConf',
           'makeD02KPiPi0R',
           'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdTightPions,StdTightKaons,StdLoosePi02gg

default_config = {
    'NAME'        : 'D02KPiPi0ForPid',
    'WGs'         : ['Calib'],
    'BUILDERTYPE' : 'StrippingD02KPiPi0ForPidConf',
    'CONFIG'      : {   'TrackMinPT_R'         : 600       # MeV
                       ,'TrackMinTrackProb'    : 0.000001  # unitless
                       ,'TrackMaxGhostProb'    : 0.3       # unitless
                       ,'TrackMinIPChi2'       : 16        # unitless
                       ,'Pi0MinPT_R'           : 1000      # MeV
                       ,'ResPi0MinGamCL'       : 0.0       # unitless
                       ,'D0MinM'               : 1600      # MeV
                       ,'D0MaxM'               : 2100      # MeV
                       ,'D0MinVtxProb'         : 0.001     # unitless
                       ,'D0MaxIPChi2'          : 9         # unitless
                       ,'D0MinDIRA'            : 0.9999    # unitless
                       ,'D0MinVVDChi2'         : 64        # unitless
                       ,'ResolvedLinePrescale' : 1.        # unitless
                       ,'ResolvedLinePostscale': 1.        # unitless
                      },
    'STREAMS'     : ['CharmCompleteEvent']
    }

class StrippingD02KPiPi0ForPidConf(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        myPions       = StdTightPions
        myKaons       = StdTightKaons
        myResolvedPi0 = StdLoosePi02gg

        #---------------------------------------
        # B -> HHPi0 selections
        self.selresolved = makeD02KPiPi0R( name + 'R',
                                           config,
                                           DecayDescriptor = '[D0 -> K- pi+ pi0]cc',
                                           inputSel = [myKaons, myPions, myResolvedPi0]
                                           )
        #---------------------------------------
        # Stripping lines
        self.D02KPiPi0R_line = StrippingLine(name + "_R" %locals()['config'],
                                             prescale = config['ResolvedLinePrescale'],
                                             postscale = config['ResolvedLinePostscale'],
                                             RequiredRawEvents = ["Calo"],
                                             selection = self.selresolved
                                             )
        # register lines
        self.registerLine(self.D02KPiPi0R_line)

##############################################################
def makeD02KPiPi0R( name,
                  config,
                  DecayDescriptor,
                  inputSel
                  ) :

    _TrackCuts    = "(PT>%(TrackMinPT_R)s *MeV) & (TRPCHI2>%(TrackMinTrackProb)s) & (TRGHOSTPROB<%(TrackMaxGhostProb)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPChi2)s)" %locals()['config']    
    _pi0Cuts      = "(PT>%(Pi0MinPT_R)s *MeV) & (CHILD(CL,1)>%(ResPi0MinGamCL)s) & (CHILD(CL,2)>%(ResPi0MinGamCL)s)" %locals()['config']
    _daughterCuts = { 'K-' : _TrackCuts, 'pi+' : _TrackCuts, 'pi0' : _pi0Cuts }
    _combCuts     = "(AM>%(D0MinM)s *MeV) & (AM<%(D0MaxM)s *MeV)" % locals()['config']
    _motherCuts   = "(VFASPF(VPCHI2)>%(D0MinVtxProb)s) & (BPVVDCHI2>%(D0MinVVDChi2)s) & (BPVIPCHI2()<%(D0MaxIPChi2)s) & (BPVDIRA>%(D0MinDIRA)s)" % locals()['config']

    _D = CombineParticles( DecayDescriptor = DecayDescriptor,
                           MotherCut = _motherCuts,
                           CombinationCut = _combCuts,
                           DaughtersCuts = _daughterCuts
                           )

    return Selection( name+'Sel',
                      Algorithm = _D,
                      RequiredSelections = inputSel
                      )
##############################################################
