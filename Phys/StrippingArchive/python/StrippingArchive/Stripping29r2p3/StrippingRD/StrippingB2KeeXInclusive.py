###############################################################################
#
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration
#
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization
# or submit itself to any jurisdiction.
###############################################################################
__author__ = ['R. Coutinho', 'F. Almeida']
__date__ = '13/07/2023'
__version__ = '$Revision: 0 $'

__all__ = ('B2KeeXInclusiveConf', 'default_config')

"""
Stripping selection for inclusively selected B->KeeX decays.
"""

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from LHCbKernel.Configuration import *

#################
#
# Define Cuts here
#
#################

default_config = {
    'NAME': 'B2KeeXInclusive',
    'BUILDERTYPE': 'B2KeeXInclusiveConf',
    'CONFIG': {
        # Incl (dimu) cuts
        'IPCHI2': 9.0,
        'FlightChi2': 100.0,
        'DIRA': 0.995,
        'VertexCHI2': 6.0,
        'CORRM_MIN': 3000.0,  # MeV
        'CORRM_MAX': 15000.0,  # MeV

        # Track cuts
        'Track_CHI2nDOF': 3.0,
        'Track_GhostProb': 0.5,

        # Kaon cuts
        'Kaon_MinIPCHI2': 9.0,
        'Kaon_ProbNNK': 0.2,
        'Kaon_ProbNNKpi': 0.1,
        'Kaon_PT': 500,

        # Electron cuts
        'Electron_MinIPCHI2': 16.0,
        'Electron_PIDe': 0,
        'Electron_PT': 500,

        # GEC
        'SpdMult': 450,
    },
    'WGs': ['RD'],
    'STREAMS': ['Leptonic']
}

#################
#
#  Make line here
#
#################

defaultName = 'B2KeeXInclusive'


class B2KeeXInclusiveConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.TrackCuts = "(TRCHI2DOF < %(Track_CHI2nDOF)s) & (TRGHP < %(Track_GhostProb)s)" % config
        self.DiElectronCut = "(ID=='J/psi(1S)') & "\
                             "(MINTREE(ABSID<14,PT) > %(Electron_PT)s *MeV) & "\
                             "(2 == NINTREE((ABSID==11)&(PIDe > %(Electron_PIDe)s))) & "\
                             "(MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s)" % config
        self.KaonCut = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(Kaon_MinIPCHI2)s) & " \
            " (PPINFO(PROBNNK) > %(Kaon_ProbNNK)s) & " \
            " (PPINFO(PROBNNK)-PPINFO(PROBNNpi) > %(Kaon_ProbNNKpi)s) & "\
            " (PT > %(Kaon_PT)s)" % config
        self.InclKLLCut = "(BPVCORRM > %(CORRM_MIN)s *MeV) & " \
            "(BPVCORRM < %(CORRM_MAX)s *MeV) &" \
            "(BPVDIRA > %(DIRA)s) & " \
            "(BPVVDCHI2 > %(FlightChi2)s) & " \
            "(VFASPF(VCHI2/VDOF) < %(VertexCHI2)s)" % config
        self.Kaon = self.__Kaons__(config)
        self.InclDielectron = self.__InclDielectron__(config)
        self.InclDielectronSS = self.__makeEESS__(config)
        self.InclKee = self.__InclKee__(config)
        self.InclKeeSS = self.__InclKeeSS__(config)

        # inclusive dielectron line
        self.inclusive_Kee_line = StrippingLine(
            self.name+"_InclKeeLine",
            prescale=1,
            FILTER={
                'Code': "( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.InclKee]
        )

        # inclusive dielectron SS line
        self.inclusive_KeeSS_line = StrippingLine(
            self.name+"_InclKeeSSLine",
            prescale=0.06,
            FILTER={
                'Code': "( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.InclKeeSS]
        )

        self.registerLine(self.inclusive_Kee_line)
        self.registerLine(self.inclusive_KeeSS_line)

    def __Kaons__(self, conf):
        """
        Filter kaons from StdLooseKaons
        """
        _code = self.KaonCut
        _name = "Selection_"+self.name+"_Kaons"

        _kaons = DataOnDemand(Location='Phys/StdNoPIDsKaons/Particles')
        _filter = FilterDesktop(Code=_code)
        _sel = Selection(_name, RequiredSelections=[_kaons], Algorithm=_filter)
        return _sel

    def __InclDielectron__(self, conf):
        '''
        Create a new dielectron for high q2 inclusive B->eeX
        '''

        from StandardParticles import StdDiElectronFromTracks as DiElectrons

        _name = "Sel_"+self.name+"_DiElectronFilter"
        _Code = self.DiElectronCut

        _Filter = FilterDesktop(Code=_Code)
        from PhysSelPython.Wrappers import Selection
        SelDiElectron = Selection(
            _name, Algorithm=_Filter, RequiredSelections=[DiElectrons])
        return SelDiElectron

    def __makeEESS__(self, conf):
        """
        Makes EE same-sign combinations
        """
        _name = "Sel_"+self.name+"_DiElectronSSFilter"
        from Configurables import DiElectronMaker, ProtoParticleCALOFilter
        from CommonParticles.Utils import trackSelector
        from GaudiKernel.SystemOfUnits import MeV
        ee = DiElectronMaker('DiElectronsSS' + _name)
        ee.Particle = "J/psi(1S)"
        # ee.DecayDescriptor = "[J/psi(1S) -> e+ e+]cc"
        selector = trackSelector(ee, trackTypes=["Long"])

        ee.addTool(ProtoParticleCALOFilter('Electron'))
        ee.Electron.Selection = ["RequiresDet='CALO'"]
        ee.DiElectronMassMin = 0.*MeV
        ee.DiElectronMassMax = 5000.*MeV
        ee.DiElectronPtMin = 500.*MeV
        ee.OppositeSign = 0

        ee.Electron.Selection = [
            "RequiresDet='CALO' CombDLL(e-pi)>'%(Electron_PIDe)s'" % conf]

        return Selection(_name+'eeSS_Selection', Algorithm=ee)

    def __InclKee__(self, conf):
        '''
        Create a new Kee for high q2 inclusive B->KeeX
        '''
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineDiElectron = CombineParticles()
        CombineDiElectron.DecayDescriptors = ["[B+ -> J/psi(1S) K+]cc"]
        sel_name = "InclKee"
        CombineDiElectron.MotherCut = self.InclKLLCut
        # choose
        dielectron = self.InclDielectron
        kaon = self.Kaon
        _name = "Sel_"+self.name+"Kee"

        from PhysSelPython.Wrappers import Selection
        SelDiElectron = Selection(_name, Algorithm=CombineDiElectron,
                                  RequiredSelections=[dielectron, kaon])
        return SelDiElectron

    def __InclKeeSS__(self, conf):
        '''
        Create a new Kee for high q2 inclusive B->KeeX
        '''
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineDiElectron = CombineParticles()
        CombineDiElectron.DecayDescriptors = ["[B+ -> J/psi(1S) K+]cc"]
        sel_name = "InclKeeSS"
        CombineDiElectron.MotherCut = self.InclKLLCut
        # choose
        dielectronSS = self.InclDielectronSS
        kaon = self.Kaon
        _name = "Sel_"+self.name+"KeeSS"

        from PhysSelPython.Wrappers import Selection
        SelDiElectron = Selection(_name, Algorithm=CombineDiElectron,
                                  RequiredSelections=[dielectronSS, kaon])
        return SelDiElectron
