###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__  = "Niladri Sahoo"
__date__    = "09/06/2023"
__version__ = "$Revision: 0$"

__all__ = ( 'Bc2Ds2MuMuConf', 'default_config' )

"""
Selections for Bc+ -> Ds2*(2573)+ mu mu where Ds2*(2573) -> D0 (-> K- pi+) K+
"""


default_config = {
    'NAME'                       : 'Bc2Ds2MuMu',
    'BUILDERTYPE'                : 'Bc2Ds2MuMuConf',
    'CONFIG'                     :
    {
        'BFlightCHI2'            : 36 #100
        , 'BDIRA'                : 0.999
        , 'BIPCHI2'              : 25
        , 'BVertexCHI2'          : 16
        , 'DiLeptonPT'           : 0
        , 'DiLeptonFDCHI2'       : 16
        , 'DiLeptonIPCHI2'       : 0
        , 'MaxVCHI2DOF'          : 10
        , 'LeptonIPCHI2'         : 9
        , 'LeptonPT'             : 300
        , 'LeptonPTTight'        : 500
        , 'KaonIPCHI2'           : 9
        , 'KaonPT'               : 250
        , 'Trk_Chi2'             : 3
        , 'Trk_GhostProb'        : 0.3
        , 'DiHadronMass'         : 6000 #2600
        , 'D0MassWindow'         : 100 #80, not used
        , 'D0DIRA'               : 0.9 # not used
        , 'Ds2st_VtxChi2'        : 20 #25, 36
        , 'Ds2st_SumPTHad'       : 800 # not used
        , 'Ds2st_SumIPChi2Had'   : 48.0 # not used
        , 'Ds2stVertexCHI2'      : 25
        , 'Ds2stMassWindow'      : 300
        , 'Ds2stADOCACHI2'       : 30
        , 'UpperMass'            : 6500
        , 'BMassWindow'          : 1500
        , 'CTAU_Bc'              : 75   # mm
        , 'Bc2Ds2MuMuPrescale'   : 1
        , 'Bc2Ds2MuMuSSPrescale' : 1

        },
    'WGs'     : [ 'RD' ],
    'STREAMS' : [ 'Dimuon' ]     #Leptonic
    }



from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays, DaVinci__N5BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder



class Bc2Ds2MuMuConf(LineBuilder) :
    """
    Builder for Bc -> Ds2*(2573) mu mu measurements
    """

    # now just define keys. Default values are fixed later
    __configuration_keys__ = (
        'BFlightCHI2'
        , 'BDIRA'
        , 'BIPCHI2'
        , 'BVertexCHI2'
        , 'DiLeptonPT'
        , 'DiLeptonFDCHI2'
        , 'DiLeptonIPCHI2'
        , 'MaxVCHI2DOF'
        , 'LeptonIPCHI2'
        , 'LeptonPT'
        , 'LeptonPTTight'
        , 'KaonIPCHI2'
        , 'KaonPT'
        , 'Trk_Chi2'
        , 'Trk_GhostProb'
        , 'DiHadronMass'       
        , 'D0MassWindow'
        , 'D0DIRA'      
        , 'Ds2st_VtxChi2'
        , 'Ds2st_SumPTHad'
        , 'Ds2st_SumIPChi2Had'
        , 'Ds2stVertexCHI2'    
        , 'Ds2stMassWindow'    
        , 'Ds2stADOCACHI2'     
        , 'UpperMass'
        , 'BMassWindow'
        , 'CTAU_Bc'
        , 'Bc2Ds2MuMuPrescale'
        , 'Bc2Ds2MuMuSSPrescale'

      )

    #__configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self._name = name 

        mmXLine_name   = name 
        mmXSSLine_name = name + "_SS"



        from StandardParticles import StdLoosePions as Pions
        from StandardParticles import StdLooseKaons as Kaons
        from StandardParticles import StdAllLoosePions as AllPions
        from StandardParticles import StdAllLooseKaons as AllKaons
        from StandardParticles import StdLooseD02KPi as DZeros


        #++++++++++++++++++++++
        # 1 : Make K, pi, D0, Ds2* 
        #++++++++++++++++++++++
        SelKaons  = self._filterHadron( name   = "KaonsFor" + self._name,
                                        sel    = Kaons,
                                        params = config )

        SelPions  = self._filterHadron( name   = "PionsFor" + self._name,
                                        sel    = Pions,
                                        params = config )

        SelDZeros = self._filterHadron( name   = "DZerosFor" + self._name, 
                                        sel    = DZeros, 
                                        params = config )


        SelDs2stPlus  = self._makeDs2stPlus( name    = "Ds2stPlusFor" + self._name,
                                             dzeros  = DZeros,
                                             kaons   = Kaons,
                                             params  = config )



        #++++++++++++++++++++++
        # 2 : Make Dileptons
        #++++++++++++++++++++++
        from StandardParticles import StdLooseDiMuon as DiMuons

        MuonID     = "(HASMUON)&(ISMUON)"
        DiMuonID   = "(2 == NINTREE((ABSID==13)&(HASMUON)&(ISMUON)))"


        MuMu_SS = self._makeMuMuSS( "MuMuSSFor" + self._name, 
                                    params = config, 
                                    muonid = MuonID )


        SelDiMuon = self._filterDiLepton( "SelDiMuonsFor" + self._name,
                                          dilepton = DiMuons,
                                          params   = config,
                                          idcut    = DiMuonID )


        SelDiMuon_SS = self._filterDiLepton( "SelMuMuSSFor" + self._name,
                                             dilepton = MuMu_SS,
                                             params   = config,
                                             idcut    = DiMuonID )


        
        #++++++++++++++++++++++
        # 4 : Combine Particles
        #++++++++++++++++++++++
        SelB2mmX = self._makeB2LLX(mmXLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelPions, SelKaons, SelDZeros, SelDs2stPlus ],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)



        SelB2mmX_SS = self._makeB2LLX(mmXSSLine_name,
                                      dilepton = SelDiMuon_SS,
                                      hadrons  = [ SelPions, SelKaons, SelDZeros, SelDs2stPlus ],
                                      params   = config,
                                      masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)




        #++++++++++++++++++++++
        # 5 : Declare Lines
        #++++++++++++++++++++++
        SPDFilter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 600 )" ,
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb" ]
            }



        self.B2mmXLine = StrippingLine(mmXLine_name + "Line",
                                       prescale          = config['Bc2Ds2MuMuPrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False )

        
        
        self.B2mmX_SSLine = StrippingLine(mmXSSLine_name + "Line",
                                          prescale          = config['Bc2Ds2MuMuSSPrescale'],
                                          postscale         = 1,
                                          selection         = SelB2mmX_SS,
                                          FILTER            = SPDFilter,
                                          RequiredRawEvents = [],
                                          MDSTFlag          = False,
                                          MaxCandidates     = 300)

        
        
        #++++++++++++++++++++++
        # 6 : Register Lines
        #++++++++++++++++++++++
        self.registerLine( self.B2mmXLine )
        self.registerLine( self.B2mmX_SSLine )



    #####################################################
    def _filterHadron( self, name, sel, params ):
        """
        Filter for all hadronic final states
        """

        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        # need to add the ID here
        _Code = "(PT > %(KaonPT)s *MeV) & " \
                "((ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) | " \
                "(NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))))" % params
            
        _Filter = FilterDesktop( Code = _Code )
            
        return Selection( name, Algorithm = _Filter, RequiredSelections = [ sel ] )


    #####################################################
    def _filterDiLepton( self, name, dilepton, params, idcut = None ) :
        """
        Handy interface for dilepton filter
        """

        _Code = "(ID=='J/psi(1S)') & "\
                "(PT > %(DiLeptonPT)s *MeV) & "\
                "(MM < %(UpperMass)s *MeV) & "\
                "(MINTREE(ABSID<14,PT) > %(LeptonPT)s *MeV) & "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "\
                "(VFASPF(VCHI2/VDOF) < %(MaxVCHI2DOF)s) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "\
                "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params


        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )

        _Filter = FilterDesktop( Code = _Code )

        return Selection(name, Algorithm = _Filter, RequiredSelections = [ dilepton ] )


    #####################################################
    # not used anymore for SSLines, rather use _filterDiLepton
    def _filterDiLeptonTight( self, name, dilepton, params, idcut = None ) :
        """
        Handy interface for dilepton filter
        """

        _Code = "(ID=='J/psi(1S)') & "\
            "(PT > %(DiLeptonPT)s *MeV) & "\
            "(MM < %(UpperMass)s *MeV) & "\
            "(MINTREE(ABSID<14,PT) > %(LeptonPTTight)s *MeV) & "\
            "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "\
            "(VFASPF(VCHI2/VDOF) < %(MaxVCHI2DOF)s) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "\
            "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params

        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )

        _Filter = FilterDesktop( Code = _Code )

        return Selection(name, Algorithm = _Filter, RequiredSelections = [ dilepton ] )


    #####################################################
    def _makeDs2stPlus( self, name, dzeros, kaons, params):
        """
        Make a Ds2*(2573)+ -> D0 K+
        Check for list as input for D0s which use (Very)Loose and Brunel candidates
        """
        if isinstance(dzeros,list):
            sel_list = [MergedSelection("Merged"+name,RequiredSelections= dzeros,Unique=True),kaons]
        else:
            sel_list = [dzeros,kaons]

        _Decays = "[D*_s2+ -> D0 K+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(ADAMASS('D*_s2+') < %(Ds2stMassWindow)s *MeV) & " \
                          "(ADOCACHI2CUT( %(Ds2stADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(Ds2stVertexCHI2)s)" % params


        _DzeroCut = "(PT > %(KaonPT)s *MeV) & " \
                    "(M < %(DiHadronMass)s*MeV) & " \
                    "(NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)))" % params

        _KaonCut = "(PT > %(KaonPT)s *MeV) & " \
                   "(M < %(DiHadronMass)s*MeV) & " \
                   "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (HASRICH) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params



        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D0"  : _DzeroCut,
            "K+"  : _KaonCut
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = sel_list )


    ####################################################
    def _makeMuMuSS( self, name, params, muonid = None):
        """
        Makes MuMu same sign combinations
        """
        from StandardParticles import StdLooseMuons as Muons

        _DecayDescriptor = "[J/psi(1S) -> mu+ mu+]cc"
        _MassCut = "(AM > 100*MeV)"
        _MotherCut = "(VFASPF(VCHI2/VDOF) < 9)"
        _DaughtersCut = "(PT > %(LeptonPT)s) & " \
                        "(MIPCHI2DV(PRIMARY) > %(LeptonIPCHI2)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _DecayDescriptor,
                                     CombinationCut  = _MassCut,
                                     MotherCut       = _MotherCut )

        _MuonCut     = _DaughtersCut

        if muonid     : _MuonCut     += ( "&" + muonid )

        _Combine.DaughtersCuts = {
            "mu+" : _MuonCut,
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ Muons ] )


    #####################################################
    def _makeB2LLX( self, name, dilepton, hadrons, params, masscut = "(ADAMASS('B+')< 1500 *MeV" ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [
                    "[ B_c+ -> J/psi(1S)  D*_s2+ ]cc",

                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s) "\
               "& (BPVLTIME()>0.05*ps))" % params

        
        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )


    #####################################################
