###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for
    Bs to K K (e/mu) (tau ->e/mu)
    Bd to K pi (e/mu) (tau ->e/mu)
    Lb to p K (e, mu) (tau ->e/mu)
Same-sign combinations are included.
"""

__author__ = 'Hanae Tilquin, Lakshan Madhan, Federico Betti'
__date__ = '13/06/2023'
__version__ = '$Revision: 1.0 $'

__all__ = ('B2XLTauLeptonicConf', 'default_config')

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop

from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME': 'B2XLTauLeptonic',
    'BUILDERTYPE': 'B2XLTauLeptonicConf',
    'CONFIG':
        {
            "Bs_Comb_MassHigh": 7250.0,
            "Bs_FlightChi2": 16,
            "Bd_Comb_MassHigh": 6750.0,
            "Bd_FlightChi2": 30.0,
            "Lb_Comb_MassHigh": 7750.0,
            "Lb_FlightChi2": 16,
            "B_DIRA": 0.999,
            "Lb_DIRA": 0.999,
            "Bs_VertexCHI2": 100.0,
            "B0_VertexCHI2": 50.0,
            "Lb_VertexCHI2": 100.0,
            "Hadron_MinIPCHI2": 25.0,
            "Muon_MinIPCHI2": 9.0,
            "MuonFromB_MinIPCHI2": 25.0,
            "Electron_MinIPCHI2": 9.0,
            "ElectronFromB_MinIPCHI2": 25.0,
            "Phi_FlightChi2": 25.0,
            "Phi_Comb_MassHigh": 3400.0,
            "Phi_PT": 800,
            "Phi_VertexCHI2": 4,
            "Phi_DOCA": 0.15,
            "Phi_DOCACHI2": 4,
            "PhiMu_VertexCHI2": 9,
            "PhiMu_Comb_MassHigh": 3400.0,
            "PhiMu_DOCA": 0.8,
            "PhiMu_DOCACHI2": 9,
            "PhiE_VertexCHI2": 9,
            "PhiE_Comb_MassHigh": 3400.0,
            "PhiE_DOCA": 0.8,
            "PhiE_DOCACHI2": 9,
            "Kstar_FlightChi2": 150.0,
            "Kstar_Comb_MassHigh": 3550.0,
            "Kstar_Comb_MassLow": 795.0,
            "Kstar_PT": 1100,
            "Kstar_DOCA": 0.12,
            "Kstar_DOCACHI2": 3,
            "Kstar_VertexCHI2": 3,
            "KstarMu_VertexCHI2": 6,
            "KstarMu_Comb_MassHigh": 3550.0,
            "KstarMu_DOCA": 0.6,
            "KstarMu_DOCACHI2": 6,
            "KstarE_VertexCHI2": 6,
            "KstarE_Comb_MassHigh": 3550.0,
            "KstarE_DOCA": 0.6,
            "KstarE_DOCACHI2": 6,
            "Lambdastar_FlightChi2": 25,
            "Lambdastar_Comb_MassHigh": 5000.0,
            "Lambdastar_Comb_MassLow": 1600.0,
            "Lambdastar_PT": 1250,
            "Lambdastar_DOCA": 0.15,
            "Lambdastar_DOCACHI2": 4,
            "Lambdastar_VertexCHI2": 4,
            "LambdastarMu_Comb_MassHigh": 5000.0,
            "LambdastarMu_DOCA": 0.8,
            "LambdastarMu_DOCACHI2": 8,
            "LambdastarMu_VertexCHI2": 8,
            "LambdastarE_Comb_MassHigh": 5000.0,
            "LambdastarE_DOCA": 0.8,
            "LambdastarE_DOCACHI2": 8,
            "LambdastarE_VertexCHI2": 8,
            "MuonPT": 500,
            "ElectronPT": 500,
            "MuonPID": 0.0,
            "ElectronPID": 2.0,
            "ElectronProbNN": 0.2,
            "ElectronProbNN_diE": 0.42,
            "KaonPID": 5.0,
            "ProtonPID": 5.0,
            "Pion_ProbNN": 0.3,
            "Pion_ProbNN_B2Kstar": 0.8,
            "Kaon_Pion_ProbNN_B2Kstar": 0.8,
            "Hadron_P": 5000,
            "SpdMult": 600,
            "Track_GhostProb": 0.3,
            "Track_TRCHI2": 3,
            "UseNoPIDsHadrons": False,
            "UseNoPIDsMuons": False,
            'UseNoPIDsElectrons': False,
            "HLT1_FILTER": None,
            "HLT2_FILTER": None,
            "L0DU_FILTER": None,
        },

    'WGs': ['RD'],
    'STREAMS': ['Semileptonic']
}


class B2XLTauLeptonicConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name

        self.BsCombCut = "(AM < %(Bs_Comb_MassHigh)s * MeV)" % config
        self.BsCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Bs_VertexCHI2)s) & (BPVVDCHI2 > %(Bs_FlightChi2)s)" % config

        self.BdCombCut = "(AM < %(Bd_Comb_MassHigh)s * MeV)" % config
        self.BdCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(B0_VertexCHI2)s) & (BPVVDCHI2 > %(Bd_FlightChi2)s)" % config

        self.LambdaBCombCut = "(AM < %(Lb_Comb_MassHigh)s * MeV)" % config
        self.LambdaBCut = "(BPVDIRA > %(Lb_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Lb_VertexCHI2)s) & (BPVVDCHI2 > %(Lb_FlightChi2)s)" % config

        self.PhiCombCut = "(AM < %(Phi_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(Phi_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(Phi_DOCACHI2)s, ''))" % config
        self.PhiCut = "(VFASPF(VCHI2/VDOF) < %(Phi_VertexCHI2)s) & (PT > %(Phi_PT)s * MeV) & (BPVVDCHI2 > %(Phi_FlightChi2)s)" % config

        self.KstarCombCut = "(AM < %(Kstar_Comb_MassHigh)s*MeV) & (AM > %(Kstar_Comb_MassLow)s * MeV) & " \
            "(ACUTDOCA(%(Kstar_DOCA)s * mm,'')) & (ACUTDOCACHI2(%(Kstar_DOCACHI2)s, ''))" % config
        self.KstarCut = "(VFASPF(VCHI2/VDOF) < %(Kstar_VertexCHI2)s) & " \
            "(PT > %(Kstar_PT)s * MeV) & (BPVVDCHI2 > %(Kstar_FlightChi2)s)" % config

        self.LambdastarCombCut = "(AM < %(Lambdastar_Comb_MassHigh)s*MeV) & (AM > %(Lambdastar_Comb_MassLow)s*MeV) & " \
            "(ACUTDOCA(%(Lambdastar_DOCA)s * mm,'')) & (ACUTDOCACHI2(%(Lambdastar_DOCACHI2)s, ''))" % config
        self.LambdastarCut = "(PT > %(Lambdastar_PT)s*MeV) & " \
            "(VFASPF(VCHI2/VDOF) < %(Lambdastar_VertexCHI2)s) & (BPVVDCHI2 > %(Lambdastar_FlightChi2)s)" % config

        self.PhiMuCut = "(VFASPF(VCHI2/VDOF) < %(PhiMu_VertexCHI2)s)" % config
        self.PhiMuCombCut = "(AM < %(PhiMu_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(PhiMu_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(PhiMu_DOCACHI2)s, ''))" % config

        self.KstarMuCut = "(VFASPF(VCHI2/VDOF) < %(KstarMu_VertexCHI2)s)" % config
        self.KstarMuCombCut = "(AM < %(KstarMu_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(KstarMu_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(KstarMu_DOCACHI2)s, ''))" % config

        self.LambdastarMuCut = "(VFASPF(VCHI2/VDOF) < %(LambdastarMu_VertexCHI2)s)" % config
        self.LambdastarMuCombCut = "(AM < %(LambdastarMu_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(LambdastarMu_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(LambdastarMu_DOCACHI2)s, ''))" % config


        self.PhiECut = "(VFASPF(VCHI2/VDOF) < %(PhiE_VertexCHI2)s)" % config
        self.PhiECombCut = "(AM < %(PhiE_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(PhiE_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(PhiE_DOCACHI2)s, ''))" % config

        self.KstarECut = "(VFASPF(VCHI2/VDOF) < %(KstarE_VertexCHI2)s)" % config
        self.KstarECombCut = "(AM < %(KstarE_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(KstarE_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(KstarE_DOCACHI2)s, ''))" % config

        self.LambdastarECut = "(VFASPF(VCHI2/VDOF) < %(LambdastarE_VertexCHI2)s)" % config
        self.LambdastarECombCut = "(AM < %(LambdastarE_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(LambdastarE_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(LambdastarE_DOCACHI2)s, ''))" % config


        self.TrackCuts = "(TRGHP < %(Track_GhostProb)s) & (TRCHI2DOF < %(Track_TRCHI2)s)" % config

        self.HadronCuts = "(MIPCHI2DV(PRIMARY) > %(Hadron_MinIPCHI2)s)" % config

        self.KaonCutBase = self.TrackCuts + " & " + \
            self.HadronCuts + " & (P > %(Hadron_P)s*MeV)" % config
        self.KaonCut = self.KaonCutBase + \
            " & (PIDK > %(KaonPID)s) & (~ISMUON)" % config
        self.KaonCutReversePID = self.KaonCutBase + \
            " & (PIDK < %(KaonPID)s)" % config
        self.KaonCut_B2Kstar = self.KaonCutBase + \
            " & (PROBNNK * (1-PROBNNpi) > %(Kaon_Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.KaonCutReversePID_B2Kstar = self.KaonCutBase + \
            " & (PROBNNK * (1-PROBNNpi) < %(Kaon_Pion_ProbNN_B2Kstar)s)" % config

        self.PionCutBase = self.TrackCuts + " & " + self.HadronCuts
        self.PionCut_B2Kstar = self.PionCutBase + \
            " & (PROBNNpi > %(Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.PionCutReversePID_B2Kstar = self.PionCutBase + \
            " & (PROBNNpi < %(Pion_ProbNN_B2Kstar)s)" % config

        self.ProtonCutBase = self.TrackCuts + " & " + \
            self.HadronCuts + " & (P > %(Hadron_P)s * MeV)" % config
        self.ProtonCut = self.ProtonCutBase + \
            " & (PIDp > %(ProtonPID)s)  & (~ISMUON)" % config
        self.ProtonCutReversePID = self.ProtonCutBase + \
            " & (PIDp < %(ProtonPID)s)" % config

        self.MuonCutBase = self.TrackCuts + \
            " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s)" % config
        self.MuonCut = self.MuonCutBase + \
            " & (PIDmu> %(MuonPID)s) & (ISMUON)" % config
        self.MuonCutReversePID = self.MuonCutBase + \
            " & (PIDmu < %(MuonPID)s) " % config

        self.MuonFromBCutBase = self.MuonCutBase + \
            " & (PT > %(MuonPT)s * MeV) & (MIPCHI2DV(PRIMARY) > %(MuonFromB_MinIPCHI2)s)" % config
        self.MuonFromBCut = self.MuonCut + \
            " & (PT > %(MuonPT)s * MeV) & (MIPCHI2DV(PRIMARY) > %(MuonFromB_MinIPCHI2)s)" % config
        self.MuonFromBCutReversePID = self.MuonCutReversePID + \
            " & (PT > %(MuonPT)s * MeV) & (MIPCHI2DV(PRIMARY) > %(MuonFromB_MinIPCHI2)s)" % config

        self.ElectronCutBase = self.TrackCuts + \
            " & (MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s)" % config
        self.ElectronCut = self.ElectronCutBase + \
            " & (PIDe > %(ElectronPID)s) & (PROBNNe > %(ElectronProbNN)s) & (~ISMUON) & (HASCALOS) & (HASRICH)" % config
        self.ElectronCutReversePID = self.ElectronCutBase + \
            " & (PIDe < %(ElectronPID)s) " % config

        self.ElectronFromBCutBase = self.ElectronCutBase + \
            " & (PT > %(ElectronPT)s * MeV) & (MIPCHI2DV(PRIMARY) > %(ElectronFromB_MinIPCHI2)s)" % config

        self.ElectronFromBCut = self.ElectronCut + \
            " & (PT > %(ElectronPT)s * MeV) & (MIPCHI2DV(PRIMARY) > %(ElectronFromB_MinIPCHI2)s)" % config
        self.ElectronFromBCutReversePID = self.ElectronCutReversePID + \
            " & (PT > %(ElectronPT)s * MeV) & (MIPCHI2DV(PRIMARY) > %(ElectronFromB_MinIPCHI2)s)" % config

        self.ElectronCut_diE = self.ElectronCutBase + \
            " & (PIDe > %(ElectronPID)s) & (PROBNNe > %(ElectronProbNN_diE)s) & (~ISMUON) & (HASCALOS) & (HASRICH)" % config
        self.ElectronCutReversePID_diE = self.ElectronCutBase + \
            " & (PIDe < %(ElectronPID)s)  & (PROBNNe < %(ElectronProbNN_diE)s)" % config

        self.ElectronFromBCut_diE = self.ElectronCut_diE + \
            " & (PT > %(ElectronPT)s * MeV) & (MIPCHI2DV(PRIMARY) > %(ElectronFromB_MinIPCHI2)s)" % config
        self.ElectronFromBCutReversePID_diE = self.ElectronCutReversePID_diE + \
            " & (PT > %(ElectronPT)s * MeV) & (MIPCHI2DV(PRIMARY) > %(ElectronFromB_MinIPCHI2)s)" % config


        self.Kaons = self.__Kaons__(config)
        self.FakeKaons = self.__FakeKaons__()

        self.Kaons_Kstar = self.__Kaons__(config, sel_name="_B2Kstar")
        self.FakeKaons_Kstar = self.__FakeKaons__(sel_name="_B2Kstar")

        self.Pions_Kstar = self.__Pions__(config, sel_name="_B2Kstar")
        self.FakePions_Kstar = self.__FakePions__(sel_name="_B2Kstar")

        self.Protons = self.__Protons__(config)
        self.FakeProtons = self.__FakeProtons__()

        self.MuonsFromTau = self.__Muons__(config, mother='tau')
        self.FakeMuonsFromTau = self.__FakeMuons__(mother='tau')

        self.MuonsFromB = self.__Muons__(config, mother='b_hadron')
        self.FakeMuonsFromB = self.__FakeMuons__(mother='b_hadron')

        self.ElectronsFromTau = self.__Electrons__(config, mother='tau')
        self.FakeElectronsFromTau = self.__FakeElectrons__(mother='tau')

        self.ElectronsFromTau_diE = self.__Electrons__(config, mother='tau', diE=True)
        self.FakeElectronsFromTau_diE = self.__FakeElectrons__(mother='tau', diE=True)


        self.ElectronsFromB = self.__Electrons__(config, mother='b_hadron')
        self.FakeElectronsFromB = self.__FakeElectrons__(mother='b_hadron')

        self.ElectronsFromB_diE = self.__Electrons__(config, mother='b_hadron', diE=True)
        self.FakeElectronsFromB_diE = self.__FakeElectrons__(mother='b_hadron', diE=True)

        self.Phi = self.__Phi__(self.Kaons, conf=config)
        self.FakePhi = self.__Phi__(self.Kaons, self.FakeKaons, conf=config)

        self.PhiMu = self.__PhiMu__(self.Phi, self.MuonsFromB)
        self.PhiMu_FakeKaon = self.__PhiMu__(
            self.FakePhi, self.MuonsFromB, pid_selection='ReversePIDK_')
        self.PhiMu_FakeMuon = self.__PhiMu__(
            self.Phi,  self.FakeMuonsFromB, pid_selection='ReversePIDMu_')

        self.PhiE = self.__PhiE__(self.Phi, self.ElectronsFromB)
        self.PhiE_FakeKaon = self.__PhiE__(
            self.FakePhi, self.ElectronsFromB, pid_selection='ReversePIDK_')
        self.PhiE_FakeElectron = self.__PhiE__(
            self.Phi,  self.FakeElectronsFromB, pid_selection='ReversePIDe_')

        
        self.PhiE_diE = self.__PhiE__(self.Phi, self.ElectronsFromB_diE, diE=True)
        self.PhiE_FakeKaon_diE = self.__PhiE__(
            self.FakePhi, self.ElectronsFromB_diE, pid_selection='ReversePIDK_', diE=True)
        self.PhiE_FakeElectron_diE = self.__PhiE__(
            self.Phi,  self.FakeElectronsFromB_diE, pid_selection='ReversePIDe_', diE=True)

        self.Kstar = self.__Kstar__(self.Kaons_Kstar, self.Pions_Kstar)
        self.FakePionKstar = self.__Kstar__(
            self.Kaons_Kstar, self.FakePions_Kstar, pid_selection="ReversePIDPi_")
        self.FakeKaonKstar = self.__Kstar__(
            self.FakeKaons_Kstar, self.Pions_Kstar, pid_selection="ReversePIDK_")

        self.KstarMu = self.__KstarMu__(self.Kstar, self.MuonsFromB)
        self.KstarMu_FakeKaon = self.__KstarMu__(
            self.FakeKaonKstar, self.MuonsFromB, pid_selection='ReversePIDK_')
        self.KstarMu_FakePion = self.__KstarMu__(
            self.FakePionKstar, self.MuonsFromB, pid_selection='ReversePIDpi_')
        self.KstarMu_FakeMuon = self.__KstarMu__(
            self.Kstar, self.FakeMuonsFromB, pid_selection='ReversePIDMu_')

        self.KstarE = self.__KstarE__(self.Kstar, self.ElectronsFromB)
        self.KstarE_FakeKaon = self.__KstarE__(
            self.FakeKaonKstar, self.ElectronsFromB, pid_selection='ReversePIDK_')
        self.KstarE_FakePion = self.__KstarE__(
            self.FakePionKstar, self.ElectronsFromB, pid_selection='ReversePIDpi_')
        self.KstarE_FakeElectron = self.__KstarE__(
            self.Kstar, self.FakeElectronsFromB, pid_selection='ReversePIDe_')

        self.KstarE_diE = self.__KstarE__(self.Kstar, self.ElectronsFromB_diE, diE=True)
        self.KstarE_FakeKaon_diE = self.__KstarE__(
            self.FakeKaonKstar, self.ElectronsFromB_diE, pid_selection='ReversePIDK_', diE=True)
        self.KstarE_FakePion_diE = self.__KstarE__(
            self.FakePionKstar, self.ElectronsFromB_diE, pid_selection='ReversePIDpi_', diE=True)
        self.KstarE_FakeElectron_diE = self.__KstarE__(
            self.Kstar, self.FakeElectronsFromB_diE, pid_selection='ReversePIDe_', diE=True)

        self.LambdaStar = self.__Lambdastar__(self.Protons, self.Kaons)
        self.FakeProtonLambdaStar = self.__Lambdastar__(
            self.FakeProtons, self.Kaons, pid_selection="ReversePIDp_")
        self.FakeKaonLambdaStar = self.__Lambdastar__(
            self.Protons, self.FakeKaons, pid_selection="ReversePIDK_")

        self.LambdastarMu = self.__LambdastarMu__(
            self.LambdaStar, self.MuonsFromB)
        self.LambdastarMu_FakeKaon = self.__LambdastarMu__(
            self.FakeKaonLambdaStar, self.MuonsFromB, pid_selection='ReversePIDK_')
        self.LambdastarMu_FakeProton = self.__LambdastarMu__(
            self.FakeProtonLambdaStar, self.MuonsFromB, pid_selection='ReversePIDp_')
        self.LambdastarMu_FakeMuon = self.__LambdastarMu__(
            self.LambdaStar, self.FakeMuonsFromB, pid_selection='ReversePIDMu_')

        self.LambdastarE = self.__LambdastarE__(
            self.LambdaStar, self.ElectronsFromB)
        self.LambdastarE_FakeKaon = self.__LambdastarE__(
            self.FakeKaonLambdaStar, self.ElectronsFromB, pid_selection='ReversePIDK_')
        self.LambdastarE_FakeProton = self.__LambdastarE__(
            self.FakeProtonLambdaStar, self.ElectronsFromB, pid_selection='ReversePIDp_')
        self.LambdastarE_FakeElectron = self.__LambdastarE__(
            self.LambdaStar, self.FakeElectronsFromB, pid_selection='ReversePIDe_')

        
        self.LambdastarE_diE = self.__LambdastarE__(
            self.LambdaStar, self.ElectronsFromB_diE, diE=True)
        self.LambdastarE_FakeKaon_diE = self.__LambdastarE__(
            self.FakeKaonLambdaStar, self.ElectronsFromB_diE, pid_selection='ReversePIDK_', diE=True)
        self.LambdastarE_FakeProton_diE = self.__LambdastarE__(
            self.FakeProtonLambdaStar, self.ElectronsFromB_diE, pid_selection='ReversePIDp_', diE=True)
        self.LambdastarE_FakeElectron_diE = self.__LambdastarE__(
            self.LambdaStar, self.FakeElectronsFromB_diE, pid_selection='ReversePIDe_', diE=True)

        self.FilterSPD = {'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                          'Preambulo': ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]}

        # ======================================================
        # mu(tau->mu) final state
        # ======================================================

        # -----------------------------------------------------
        DeclaredDaughters = [self.PhiMu, self.MuonsFromTau]
        self.Bs = self.__Bs_Phi__(
            daughters=DeclaredDaughters, tau_daughter='mu', b_daughter='mu')

        self.Bs2phimumu_line = StrippingLine(  # Bs->phimu(tau->mu)
            self.name + "_Bs2Phimumu_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phimumu_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.PhiMu, self.FakeMuonsFromTau]
        self.Bs_ReversePIDmuFromTau = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromTau_", tau_daughter='mu', b_daughter='mu')

        DeclaredDaughters = [self.PhiMu_FakeMuon, self.MuonsFromTau]
        self.Bs_ReversePIDmuFromB = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromb_", tau_daughter='mu', b_daughter='mu')

        self.Bs_ReversePIDmu = Selection("Bs_ReversePIDmu_sel_for" + self.name, Algorithm=FilterDesktop(Code='ALL'),
                                    RequiredSelections=[self.Bs_ReversePIDmuFromTau, self.Bs_ReversePIDmuFromB])

        self.Bs2phimumu_ReversePIDmu_line = StrippingLine(  # Bs->phimu(tau->mu)
            self.name + "_Bs2Phimumu_ReversePIDMuLine", prescale=0.3,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phimumu_ReversePIDmu_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.PhiMu_FakeKaon, self.MuonsFromTau]
        self.Bs_ReversePIDK = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='mu', b_daughter='mu')

        self.Bs2phimumu_ReversePIDK_line = StrippingLine(  # Bs->phimu(tau->mu)
            self.name + "_Bs2Phimumu_ReversePIDKLine", prescale=0.15,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phimumu_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarMu, self.MuonsFromTau]
        self.Bd = self.__B_Kstar__(
            daughters=DeclaredDaughters, tau_daughter='mu', b_daughter='mu')

        self.Bd2Kstmumu_line = StrippingLine(  # B0->Kstmu(tau->mu)
            self.name + "_B2Kstarmumu_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bd2Kstmumu_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarMu, self.FakeMuonsFromTau]
        self.Bd_ReversePIDmuFromTau = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromTau_", tau_daughter='mu', b_daughter='mu')

        DeclaredDaughters = [self.KstarMu_FakeMuon, self.MuonsFromTau]
        self.Bd_ReversePIDmuFromB = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromb_", tau_daughter='mu', b_daughter='mu')

        self.Bd_ReversePIDmu = Selection("Bd_ReversePIDmu_sel_for" + self.name, Algorithm=FilterDesktop(Code='ALL'),
                                    RequiredSelections=[self.Bd_ReversePIDmuFromTau, self.Bd_ReversePIDmuFromB])

        self.Bd2Kstmumu_ReversePIDmu_line = StrippingLine(  # B0->Kstmu(tau->mu)
            self.name + "_B2Kstarmumu_ReversePIDMuLine", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstmumu_ReversePIDmu_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarMu_FakeKaon, self.MuonsFromTau]
        self.Bd_ReversePIDK = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='mu', b_daughter='mu')

        self.Bd2Kstmumu_ReversePIDK_line = StrippingLine(  # B0->Kstmu(tau->mu)
            self.name + "_B2Kstarmumu_ReversePIDKLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstmumu_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarMu_FakePion, self.MuonsFromTau]

        self.Bd_ReversePIDpi = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpi_", tau_daughter='mu', b_daughter='mu')
        self.Bd2Kstmumu_ReversePIDpi_line = StrippingLine(  # B0->Kstmu(tau->mu)
            self.name + "_B2Kstarmumu_ReversePIDpiLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstmumu_ReversePIDpi_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.LambdastarMu, self.MuonsFromTau]
        self.Lb = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, tau_daughter='mu', b_daughter='mu')
        self.Lb2Lstmumu_line = StrippingLine(  # Lb-> pKmu(tau->mu)
            self.name + "_Lb2Lstarmumu_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Lb2Lstmumu_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.LambdastarMu, self.FakeMuonsFromTau]
        self.Lb_ReversePIDmuFromTau = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromTau_", tau_daughter='mu', b_daughter='mu')

        DeclaredDaughters = [self.LambdastarMu_FakeMuon, self.MuonsFromTau]
        self.Lb_ReversePIDmuFromB = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromb_", tau_daughter='mu', b_daughter='mu')

        self.Lb_ReversePIDmu = Selection("Lb_ReversePIDmu_sel_for" + self.name, Algorithm=FilterDesktop(Code='ALL'),
                                    RequiredSelections=[self.Lb_ReversePIDmuFromTau, self.Lb_ReversePIDmuFromB])

        self.Lb2Lstmumu_ReversePIDmu_line = StrippingLine(  # Lb-> pKmu(tau->mu)
            self.name + "_Lb2Lstarmumu_ReversePIDMuLine", prescale=0.3,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstmumu_ReversePIDmu_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.LambdastarMu_FakeKaon, self.MuonsFromTau]
        self.Lb_ReversePIDK = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='mu', b_daughter='mu')

        self.Lb2Lstmumu_ReversePIDK_line = StrippingLine(  # Lb-> pKmu(tau->mu)
            self.name + "_Lb2Lstarmumu_ReversePIDKLine", prescale=0.7,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstmumu_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.LambdastarMu_FakeProton, self.MuonsFromTau]
        self.Lb_ReversePIDp = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDp_", tau_daughter='mu', b_daughter='mu')
        self.Lb2Lstmumu_ReversePIDp_line = StrippingLine(  # Lb-> pKmu(tau->mu)
            self.name + "_Lb2Lstarmumu_ReversePIDpLine", prescale=0.5,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstmumu_ReversePIDp_line)

        # -----------------------------------------------------

        # #======================================================
        # # mu(tau->e) final state
        # #======================================================

        # -----------------------------------------------------
        DeclaredDaughters = [self.PhiMu, self.ElectronsFromTau]
        self.Bs = self.__Bs_Phi__(
            daughters=DeclaredDaughters, tau_daughter='e', b_daughter='mu')

        self.Bs2phimue_line = StrippingLine(  # Bs->phimu(tau->e)
            self.name + "_Bs2Phimue_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phimue_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.PhiMu, self.FakeElectronsFromTau]
        self.Bs_ReversePIDeFromTau = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromTau_", tau_daughter='e', b_daughter='mu')

        self.Bs2phimue_ReversePIDe_line = StrippingLine(  # Bs->phimu(tau->e)
            self.name + "_Bs2Phimue_ReversePIDeLine", prescale=0.11,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDeFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phimue_ReversePIDe_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.PhiMu_FakeMuon, self.ElectronsFromTau]
        self.Bs_ReversePIDmuFromB = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromb_", tau_daughter='e', b_daughter='mu')

        self.Bs2phimue_ReversePIDmu_line = StrippingLine(  # Bs->phimu(tau->e)
            self.name + "_Bs2Phimue_ReversePIDMuLine", prescale=0.07,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDmuFromB], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phimue_ReversePIDmu_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.PhiMu_FakeKaon, self.ElectronsFromTau]
        self.Bs_ReversePIDK = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='e', b_daughter='mu')

        self.Bs2phimue_ReversePIDK_line = StrippingLine(  # Bs->phimu(tau->e)
            self.name + "_Bs2Phimue_ReversePIDKLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phimue_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarMu, self.ElectronsFromTau]
        self.Bd = self.__B_Kstar__(
            daughters=DeclaredDaughters, tau_daughter='e', b_daughter='mu')

        self.Bd2Kstmue_line = StrippingLine(  # B0->Kstmu(tau->e)
            self.name + "_B2Kstarmue_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bd2Kstmue_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.KstarMu, self.FakeElectronsFromTau]
        self.Bd_ReversePIDeFromTau = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromTau_", tau_daughter='e', b_daughter='mu')

        self.Bd2Kstmue_ReversePIDe_line = StrippingLine(  # B0->Kstmu(tau->e)
            self.name + "_B2Kstarmue_ReversePIDeLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDeFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstmue_ReversePIDe_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.KstarMu_FakeMuon, self.ElectronsFromTau]
        self.Bd_ReversePIDmuFromB = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromb_", tau_daughter='e', b_daughter='mu')

        self.Bd2Kstmue_ReversePIDmu_line = StrippingLine(  # B0->Kstmu(tau->e)
            self.name + "_B2Kstarmue_ReversePIDMuLine", prescale=0.05,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDmuFromB], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstmue_ReversePIDmu_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarMu_FakeKaon, self.ElectronsFromTau]
        self.Bd_ReversePIDK = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='e', b_daughter='mu')

        self.Bd2Kstmue_ReversePIDK_line = StrippingLine(  # B0->Kstmu(tau->e)
            self.name + "_B2Kstarmue_ReversePIDKLine", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstmue_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarMu_FakePion, self.ElectronsFromTau]

        self.Bd_ReversePIDpi = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpi_", tau_daughter='e', b_daughter='mu')
        self.Bd2Kstmue_ReversePIDpi_line = StrippingLine(  # B0->Kstmu(tau->e)
            self.name + "_B2Kstarmue_ReversePIDpiLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstmue_ReversePIDpi_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.LambdastarMu, self.ElectronsFromTau]
        self.Lb = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, tau_daughter='e', b_daughter='mu')
        self.Lb2Lstmue_line = StrippingLine(  # Lb-> pKmu(tau->e)
            self.name + "_Lb2Lstarmue_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Lb2Lstmue_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.LambdastarMu, self.FakeElectronsFromTau]
        self.Lb_ReversePIDeFromTau = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromTau_", tau_daughter='e', b_daughter='mu')

        self.Lb2Lstmue_ReversePIDe_line = StrippingLine(  # Lb-> pKmu(tau->e)
            self.name + "_Lb2Lstarmue_ReversePIDeLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDeFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstmue_ReversePIDe_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.LambdastarMu_FakeMuon, self.ElectronsFromTau]
        self.Lb_ReversePIDmuFromB = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromb_", tau_daughter='e', b_daughter='mu')
        self.Lb2Lstmue_ReversePIDmu_line = StrippingLine(  # Lb-> pKmu(tau->e)
            self.name + "_Lb2Lstarmue_ReversePIDMuLine", prescale=0.07,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDmuFromB], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstmue_ReversePIDmu_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.LambdastarMu_FakeKaon, self.ElectronsFromTau]
        self.Lb_ReversePIDK = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='e', b_daughter='mu')

        self.Lb2Lstmue_ReversePIDK_line = StrippingLine(  # Lb-> pKmu(tau->e)
            self.name + "_Lb2Lstarmue_ReversePIDKLine", prescale=0.35,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstmue_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [
            self.LambdastarMu_FakeProton, self.ElectronsFromTau]
        self.Lb_ReversePIDp = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDp_", tau_daughter='e', b_daughter='mu')
        self.Lb2Lstmue_ReversePIDp_line = StrippingLine(  # Lb-> pKmu(tau->e)
            self.name + "_Lb2Lstarmue_ReversePIDpLine", prescale=0.3,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstmue_ReversePIDp_line)

        # -----------------------------------------------------

        # ======================================================
        # e(tau->mu) final state
        # ======================================================

        # -----------------------------------------------------
        DeclaredDaughters = [self.PhiE, self.MuonsFromTau]
        self.Bs = self.__Bs_Phi__(
            daughters=DeclaredDaughters, tau_daughter='mu', b_daughter='e')

        self.Bs2phiemu_line = StrippingLine(  # Bs->phie(tau->mu)
            self.name + "_Bs2Phiemu_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phiemu_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.PhiE, self.FakeMuonsFromTau]
        self.Bs_ReversePIDmuFromTau = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromTau_", tau_daughter='mu', b_daughter='e')

        self.Bs2phiemu_ReversePIDmu_line = StrippingLine(  # Bs->phie(tau->mu)
            self.name + "_Bs2Phiemu_ReversePIDMuLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDmuFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phiemu_ReversePIDmu_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.PhiE_FakeElectron, self.MuonsFromTau]
        self.Bs_ReversePIDeFromB = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromb_", tau_daughter='mu', b_daughter='e')

        self.Bs2phiemu_ReversePIDe_line = StrippingLine(  # Bs->phie(tau->mu)
            self.name + "_Bs2Phiemu_ReversePIDeLine", prescale=0.08,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDeFromB], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phiemu_ReversePIDe_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.PhiE_FakeKaon, self.MuonsFromTau]
        self.Bs_ReversePIDK = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='mu', b_daughter='e')

        self.Bs2phiemu_ReversePIDK_line = StrippingLine(  # Bs->phie(tau->mu)
            self.name + "_Bs2Phiemu_ReversePIDKLine", prescale=0.15,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phiemu_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarE, self.MuonsFromTau]
        self.Bd = self.__B_Kstar__(
            daughters=DeclaredDaughters, tau_daughter='mu', b_daughter='e')

        self.Bd2Kstemu_line = StrippingLine(  # B0->Kste(tau->mu)
            self.name + "_B2Kstaremu_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bd2Kstemu_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarE, self.FakeMuonsFromTau]
        self.Bd_ReversePIDmuFromTau = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromTau_", tau_daughter='mu', b_daughter='e')

        self.Bd2Kstemu_ReversePIDmu_line = StrippingLine(  # B0->Kste(tau->mu)
            self.name + "_B2Kstaremu_ReversePIDMuLine", prescale=0.15,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDmuFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstemu_ReversePIDmu_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.KstarE_FakeElectron, self.MuonsFromTau]
        self.Bd_ReversePIDeFromB = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromb_", tau_daughter='mu', b_daughter='e')

        self.Bd2Kstemu_ReversePIDe_line = StrippingLine(  # B0->Kste(tau->mu)
            self.name + "_B2Kstaremu_ReversePIDeLine", prescale=0.05,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDeFromB], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstemu_ReversePIDe_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarE_FakeKaon, self.MuonsFromTau]
        self.Bd_ReversePIDK = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='mu', b_daughter='e')

        self.Bd2Kstemu_ReversePIDK_line = StrippingLine(  # B0->Kste(tau->mu)
            self.name + "_B2Kstaremu_ReversePIDKLine", prescale=0.25,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstemu_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarE_FakePion, self.MuonsFromTau]

        self.Bd_ReversePIDpi = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpi_", tau_daughter='mu', b_daughter='e')
        self.Bd2Kstemu_ReversePIDpi_line = StrippingLine(  # B0->Kste(tau->mu)
            self.name + "_B2Kstaremu_ReversePIDpiLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstemu_ReversePIDpi_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.LambdastarE, self.MuonsFromTau]
        self.Lb = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, tau_daughter='mu', b_daughter='e')
        self.Lb2Lstemu_line = StrippingLine(  # Lb-> pKe(tau->mu)
            self.name + "_Lb2Lstaremu_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Lb2Lstemu_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.LambdastarE, self.FakeMuonsFromTau]
        self.Lb_ReversePIDmuFromTau = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromTau_", tau_daughter='mu', b_daughter='e')

        self.Lb2Lstemu_ReversePIDmu_line = StrippingLine(  # Lb-> pKe(tau->mu)
            self.name + "_Lb2Lstaremu_ReversePIDMuLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDmuFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstemu_ReversePIDmu_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.LambdastarE_FakeElectron, self.MuonsFromTau]
        self.Lb_ReversePIDeFromB = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromb_", tau_daughter='mu', b_daughter='e')

        self.Lb2Lstemu_ReversePIDmu_line = StrippingLine(  # Lb-> pKe(tau->mu)
            self.name + "_Lb2Lstaremu_ReversePIDeLine", prescale=0.08,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDeFromB], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstemu_ReversePIDmu_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.LambdastarE_FakeKaon, self.MuonsFromTau]
        self.Lb_ReversePIDK = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='mu', b_daughter='e')

        self.Lb2Lstemu_ReversePIDK_line = StrippingLine(  # Lb-> pKe(tau->mu)
            self.name + "_Lb2Lstaremu_ReversePIDKLine", prescale=0.3,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstemu_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.LambdastarE_FakeProton, self.MuonsFromTau]
        self.Lb_ReversePIDp = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDp_", tau_daughter='mu', b_daughter='e')
        self.Lb2Lstemu_ReversePIDp_line = StrippingLine(  # Lb-> pKe(tau->mu)
            self.name + "_Lb2Lstaremu_ReversePIDpLine", prescale=0.3,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstemu_ReversePIDp_line)

        # # -----------------------------------------------------

        # ======================================================
        # e(tau->e) final state
        # ======================================================

        # -----------------------------------------------------
        DeclaredDaughters = [self.PhiE_diE, self.ElectronsFromTau_diE]
        self.Bs = self.__Bs_Phi__(
            daughters=DeclaredDaughters, tau_daughter='e', b_daughter='e')

        self.Bs2phiee_line = StrippingLine(  # Bs->phie(tau->e)
            self.name + "_Bs2Phiee_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phiee_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.PhiE_diE, self.FakeElectronsFromTau_diE]
        self.Bs_ReversePIDeFromTau = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromTau_", tau_daughter='e', b_daughter='e')

        DeclaredDaughters = [self.PhiE_FakeElectron, self.ElectronsFromTau]
        self.Bs_ReversePIDeFromB = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromb_", tau_daughter='e', b_daughter='e')

        self.Bs_ReversePIDe = Selection("Bs_ReversePIDe_sel_for" + self.name, Algorithm=FilterDesktop(Code='ALL'),
                                   RequiredSelections=[self.Bs_ReversePIDeFromTau, self.Bs_ReversePIDeFromB])

        self.Bs2phiee_ReversePIDe_line = StrippingLine(  # Bs->phie(tau->e)
            self.name + "_Bs2Phiee_ReversePIDeLine", prescale=0.35,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phiee_ReversePIDe_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.PhiE_FakeKaon_diE, self.ElectronsFromTau_diE]
        self.Bs_ReversePIDK = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='e', b_daughter='e')

        self.Bs2phiee_ReversePIDK_line = StrippingLine(  # Bs->phie(tau->e)
            self.name + "_Bs2Phiee_ReversePIDKLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phiee_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarE_diE, self.ElectronsFromTau_diE]
        self.Bd = self.__B_Kstar__(
            daughters=DeclaredDaughters, tau_daughter='e', b_daughter='e')

        self.Bd2Kstee_line = StrippingLine(  # B0->Kste(tau->e)
            self.name + "_B2Kstaree_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bd2Kstee_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarE_diE, self.FakeElectronsFromTau_diE]
        self.Bd_ReversePIDeFromTau = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromTau_", tau_daughter='e', b_daughter='e')

        DeclaredDaughters = [self.KstarE_FakeElectron_diE, self.ElectronsFromTau_diE]
        self.Bd_ReversePIDeFromB = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromb_", tau_daughter='e', b_daughter='e')

        self.Bd_ReversePIDe = Selection("Bd_ReversePIDe_sel_for" + self.name, Algorithm=FilterDesktop(Code='ALL'),
                                   RequiredSelections=[self.Bd_ReversePIDeFromTau, self.Bd_ReversePIDeFromB])

        self.Bd2Kstee_ReversePIDe_line = StrippingLine(  # B0->Kste(tau->e)
            self.name + "_B2Kstaree_ReversePIDeLine", prescale=0.25,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstee_ReversePIDe_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarE_FakeKaon_diE, self.ElectronsFromTau_diE]
        self.Bd_ReversePIDK = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='e', b_daughter='e')

        self.Bd2Kstee_ReversePIDK_line = StrippingLine(  # B0->Kste(tau->e)
            self.name + "_B2Kstaree_ReversePIDKLine", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstee_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.KstarE_FakePion_diE, self.ElectronsFromTau_diE]

        self.Bd_ReversePIDpi = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpi_", tau_daughter='e', b_daughter='e')
        self.Bd2Kstee_ReversePIDpi_line = StrippingLine(  # B0->Kste(tau->e)
            self.name + "_B2Kstaree_ReversePIDpiLine", prescale=0.8,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_ReversePIDpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstee_ReversePIDpi_line)

        # -----------------------------------------------------
        DeclaredDaughters = [self.LambdastarE_diE, self.ElectronsFromTau_diE]
        self.Lb = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, tau_daughter='e', b_daughter='e')
        self.Lb2Lstee_line = StrippingLine(  # Lb-> pKe(tau->e)
            self.name + "_Lb2Lstaree_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Lb2Lstee_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.LambdastarE_diE, self.FakeElectronsFromTau_diE]
        self.Lb_ReversePIDeFromTau = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromTau_", tau_daughter='e', b_daughter='e')

        DeclaredDaughters = [
            self.LambdastarE_FakeElectron_diE, self.ElectronsFromTau_diE]
        self.Lb_ReversePIDeFromB = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromb_", tau_daughter='e', b_daughter='e')

        self.Lb_ReversePIDe = Selection("Lb_ReversePIDe_sel_for" + self.name, Algorithm=FilterDesktop(Code='ALL'),
                                   RequiredSelections=[self.Lb_ReversePIDeFromTau, self.Lb_ReversePIDeFromB])

        self.Lb2Lstee_ReversePIDe_line = StrippingLine(  # Lb-> pKe(tau->e)
            self.name + "_Lb2Lstaree_ReversePIDeLine", prescale=0.35,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstee_ReversePIDe_line)

        # -----------------------------------------------------

        DeclaredDaughters = [self.LambdastarE_FakeKaon_diE, self.ElectronsFromTau_diE]
        self.Lb_ReversePIDK = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", tau_daughter='e', b_daughter='e')

        self.Lb2Lstee_ReversePIDK_line = StrippingLine(  # Lb-> pKe(tau->e)
            self.name + "_Lb2Lstaree_ReversePIDKLine", prescale=0.35,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstee_ReversePIDK_line)

        # -----------------------------------------------------
        DeclaredDaughters = [
            self.LambdastarE_FakeProton_diE, self.ElectronsFromTau_diE]
        self.Lb_ReversePIDp = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDp_", tau_daughter='e', b_daughter='e')
        self.Lb2Lstee_ReversePIDp_line = StrippingLine(  # Lb-> pKe(tau->e)
            self.name + "_Lb2Lstaree_ReversePIDpLine", prescale=0.25,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_ReversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstee_ReversePIDp_line)

        # -----------------------------------------------------

    def __Muons__(self, conf, mother):
        from StandardParticles import StdAllLooseMuons, StdAllNoPIDsMuons
        muons = StdAllNoPIDsMuons if conf['UseNoPIDsMuons'] else StdAllLooseMuons
        if mother == 'tau':
            muon_cut = self.MuonCutBase if conf['UseNoPIDsMuons'] else self.MuonCut
        else:
            muon_cut = self.MuonFromBCutBase if conf['UseNoPIDsMuons'] else self.MuonFromBCut
        _filter = FilterDesktop(Code=muon_cut)
        _sel = Selection("Selection_" + self.name + "_StdLooseMuons" + "from" + mother, RequiredSelections=[muons],
                         Algorithm=_filter)
        return _sel

    def __FakeMuons__(self, mother):
        from StandardParticles import StdAllNoPIDsMuons
        if mother == 'tau':
            _filter = FilterDesktop(Code=self.MuonCutReversePID)
        else:
            _filter = FilterDesktop(Code=self.MuonFromBCutReversePID)
        _sel = Selection("Selection_" + self._name + "StdAllReversePIDsMuons" + "from" + mother, Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsMuons])
        return _sel

    def __Electrons__(self, conf, mother, diE=False):
        from StandardParticles import StdAllLooseElectrons, StdAllNoPIDsElectrons
        electrons = StdAllNoPIDsElectrons if conf['UseNoPIDsElectrons'] else StdAllLooseElectrons
        
        if diE:
            electron_cut = self.ElectronCut_diE
            electron_cut_fromB = self.ElectronFromBCut_diE
            diE_name = '_diE'
        else:
            electron_cut = self.ElectronCut
            electron_cut_fromB = self.ElectronFromBCut
            diE_name = ''

        if mother == 'tau':
            electron_cut = self.ElectronCutBase if conf['UseNoPIDsElectrons'] else electron_cut
        else:
            electron_cut = self.ElectronFromBCutBase if conf[
                'UseNoPIDsElectrons'] else electron_cut_fromB
            
        
        _filter = FilterDesktop(Code=electron_cut)
        _sel = Selection("Selection_" + self.name + "_StdLooseElectrons" + "from" + mother+diE_name, RequiredSelections=[electrons],
                         Algorithm=_filter)
        return _sel

    def __FakeElectrons__(self, mother, diE=False):
        from StandardParticles import StdAllNoPIDsElectrons
        diE_name = '' if not diE else '_diE'
        if mother == 'tau':
            code = self.ElectronCutReversePID if  not diE else self.ElectronCutReversePID_diE
            _filter = FilterDesktop(Code=code)
        else:
            code = self.ElectronFromBCutReversePID if not diE else self.ElectronFromBCutReversePID_diE
            _filter = FilterDesktop(Code=code)
        _sel = Selection("Selection_" + self._name + "StdAllReversePIDsElectrons" + "from" + mother+diE_name, Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsElectrons])
        return _sel

    def __Protons__(self, conf):
        from StandardParticles import StdLooseProtons, StdNoPIDsProtons
        protons = StdNoPIDsProtons if conf['UseNoPIDsHadrons'] else StdLooseProtons
        proton_cuts = self.ProtonCutBase if conf['UseNoPIDsHadrons'] else self.ProtonCut
        _filter = FilterDesktop(Code=proton_cuts)
        _sel = Selection("Selection_" + self.name + "_StdLooseProtons",
                         RequiredSelections=[protons], Algorithm=_filter)
        return _sel

    def __FakeProtons__(self):
        from StandardParticles import StdNoPIDsProtons
        _filter = FilterDesktop(Code=self.ProtonCutReversePID)
        _sel = Selection("Selection_" + self._name + "_StdAllReversePIDsProtons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsProtons])
        return _sel

    def __Kaons__(self, conf, sel_name="_"):
        from StandardParticles import StdLooseKaons, StdNoPIDsKaons
        kaons = StdNoPIDsKaons if conf['UseNoPIDsHadrons'] else StdLooseKaons
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.KaonCutBase)
        elif "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCut_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCut)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLooseKaons", RequiredSelections=[kaons],
                         Algorithm=_filter)
        return _sel

    def __FakeKaons__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsKaons
        if "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCutReversePID_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCutReversePID)
        _sel = Selection("Selection_" + self._name + sel_name + "StdAllReversePIDsKaons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsKaons])
        return _sel

    def __Pions__(self, conf, sel_name="_"):
        from StandardParticles import StdLoosePions, StdNoPIDsPions
        pions = StdNoPIDsPions if conf['UseNoPIDsHadrons'] else StdLoosePions
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.PionCutBase)
        else:
            _filter = FilterDesktop(Code=self.PionCut_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLoosePions", RequiredSelections=[pions],
                         Algorithm=_filter)
        return _sel

    def __FakePions__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsPions
        _filter = FilterDesktop(Code=self.PionCutReversePID_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdAllReversePIDsPions", RequiredSelections=[StdNoPIDsPions],
                         Algorithm=_filter)
        return _sel

    def __Phi__(self, Kaons, fakekaon=None, conf=None):
        _phi2kk = CombineParticles()
        _phi2kk.DecayDescriptors = [
            "phi(1020) -> K+ K-", "phi(1020) -> K+ K+", "phi(1020) -> K- K-"]
        _phi2kk.MotherCut = self.PhiCut
        if fakekaon is None:
            _phi2kk.CombinationCut = self.PhiCombCut
            _sel = Selection("Phi_selection_for" + self.name,
                             Algorithm=_phi2kk, RequiredSelections=[Kaons])
        else:
            _phi2kk.CombinationCut = self.PhiCombCut + \
                " & (AHASCHILD((PIDK < %(KaonPID)s)))" % conf
            _sel = Selection("Phi_ReversePIDK_selection_for" + self.name, Algorithm=_phi2kk,
                             RequiredSelections=[Kaons, fakekaon])
        return _sel

    def __Kstar__(self, Kaons, Pions, pid_selection="_"):
        _kstar2kpi = CombineParticles()
        _kstar2kpi.DecayDescriptors = [
            "[K*(892)0 -> K+ pi-]cc", "K*(892)0 -> K+ pi+", "K*(892)0 -> K- pi-"]
        _kstar2kpi.CombinationCut = self.KstarCombCut
        _kstar2kpi.MotherCut = self.KstarCut
        _sel = Selection("Kstar" + pid_selection + "selection_for" + self.name, Algorithm=_kstar2kpi,
                         RequiredSelections=[Kaons, Pions])
        return _sel

    def __Lambdastar__(self, Protons, Kaons, pid_selection="_"):
        _lstar2pk = CombineParticles()
        _lstar2pk.DecayDescriptors = [
            "[Lambda(1520)0 -> p+ K-]cc", "Lambda(1520)0 -> p+ K+", "Lambda(1520)0 -> p~- K-"]
        _lstar2pk.CombinationCut = self.LambdastarCombCut
        _lstar2pk.MotherCut = self.LambdastarCut
        _sel = Selection("Lambdastar" + pid_selection + "selection_for" + self.name, Algorithm=_lstar2pk,
                         RequiredSelections=[Protons, Kaons])
        return _sel

    def __PhiMu__(self, Phi, Muon, conf=None, pid_selection='_'):
        _b2phimu = CombineParticles()
        _b2phimu.DecayDescriptors = ["[B0 -> phi(1020) mu+]cc"]
        _b2phimu.MotherCut = self.PhiMuCut
        _b2phimu.CombinationCut = self.PhiMuCombCut
        _sel = Selection("B2PhiMu" + pid_selection + self.name,
                         Algorithm=_b2phimu, RequiredSelections=[Phi, Muon])
        return _sel

    def __KstarMu__(self, Kstar, Muon, conf=None, pid_selection='_'):
        _b2kstmu = CombineParticles()
        _b2kstmu.DecayDescriptors = [
            "[B0 -> K*(892)0 mu+]cc", "[B0 -> K*(892)0 mu-]cc"]
        _b2kstmu.MotherCut = self.KstarMuCut
        _b2kstmu.CombinationCut = self.KstarMuCombCut
        _sel = Selection("B2KstarMu" + pid_selection + self.name,
                         Algorithm=_b2kstmu, RequiredSelections=[Kstar, Muon])
        return _sel

    def __LambdastarMu__(self, Lambdastar, Muon, conf=None, pid_selection='_'):
        _b2lstmu = CombineParticles()
        _b2lstmu.DecayDescriptors = [
            "[B0 -> Lambda(1520)0 mu+]cc", "[B0 -> Lambda(1520)0 mu-]cc"]
        _b2lstmu.MotherCut = self.LambdastarMuCut
        _b2lstmu.CombinationCut = self.LambdastarMuCombCut
        _sel = Selection("B2LambdastarMu" + pid_selection + self.name,
                         Algorithm=_b2lstmu, RequiredSelections=[Lambdastar, Muon])
        return _sel

    def __PhiE__(self, Phi, Electron, conf=None, pid_selection='_', diE=False):
        _b2phie = CombineParticles()
        _b2phie.DecayDescriptors = ["[B0 -> phi(1020) e+]cc"]
        _b2phie.MotherCut = self.PhiECut
        _b2phie.CombinationCut = self.PhiECombCut
        diE_name = '' if not diE else '_diE'
        _sel = Selection("B2PhiE" + pid_selection + self.name+diE_name,
                         Algorithm=_b2phie, RequiredSelections=[Phi, Electron])
        return _sel

    def __KstarE__(self, Kstar, Electron, conf=None, pid_selection='_', diE=False):
        _b2kste = CombineParticles()
        _b2kste.DecayDescriptors = [
            "[B0 -> K*(892)0 e+]cc", "[B0 -> K*(892)0 e-]cc"]
        _b2kste.MotherCut = self.KstarECut
        _b2kste.CombinationCut = self.KstarECombCut
        diE_name = '' if not diE else '_diE'
        _sel = Selection("B2KstarE" + pid_selection + self.name+diE_name,
                         Algorithm=_b2kste, RequiredSelections=[Kstar, Electron])
        return _sel

    def __LambdastarE__(self, Lambdastar, Electron, conf=None, pid_selection='_', diE=False):
        _b2lste = CombineParticles()
        _b2lste.DecayDescriptors = [
            "[B0 -> Lambda(1520)0 e+]cc", "[B0 -> Lambda(1520)0 e-]cc"]
        _b2lste.MotherCut = self.LambdastarECut
        _b2lste.CombinationCut = self.LambdastarECombCut
        diE_name = '' if not diE else '_diE'
        _sel = Selection("B2LambdastarE" + pid_selection + self.name+diE_name,
                         Algorithm=_b2lste, RequiredSelections=[Lambdastar, Electron])
        return _sel

    def __Bs_Phi__(self, daughters, pid_selection="_", tau_daughter='', b_daughter=''):

        _b2philtau = CombineParticles(DecayDescriptors=["[B_s0 -> B0 {0}+]cc".format(tau_daughter),
                                                        "[B_s0 -> B0 {0}-]cc".format(tau_daughter)],
                                      MotherCut=self.BsCut, CombinationCut=self.BsCombCut)
        if tau_daughter == 'mu':
            tau_daughter = 'Muonic'
        elif tau_daughter == 'e':
            tau_daughter = 'Electronic'
        else:
            raise Exception('tau_daughter must be mu or e')

        if b_daughter not in ['mu', 'e']:
            raise Exception('b_daughter must be mu or e')

        sel = Selection("Phi" + pid_selection + "for" + self.name.replace('Leptonic',
                        tau_daughter).replace('L', b_daughter), Algorithm=_b2philtau, RequiredSelections=daughters)
        return sel

    def __B_Kstar__(self, daughters, pid_selection="_", tau_daughter='', b_daughter=''):

        _b2kstltau = CombineParticles(DecayDescriptors=["[B_s0 -> B0 {0}+]cc".format(tau_daughter),
                                                        "[B_s0 -> B0 {0}-]cc".format(tau_daughter)],
                                      MotherCut=self.BdCut, CombinationCut=self.BdCombCut)

        if tau_daughter == 'mu':
            tau_daughter = 'Muonic'
        elif tau_daughter == 'e':
            tau_daughter = 'Electronic'
        else:
            raise Exception('tau_daughter must be mu or e')

        if b_daughter not in ['mu', 'e']:
            raise Exception('b_daughter must be mu or e')

        sel = Selection("Kstar" + pid_selection + "for" + self.name.replace('Leptonic',
                        tau_daughter).replace('L', b_daughter), Algorithm=_b2kstltau, RequiredSelections=daughters)
        return sel

    def __Lambdab_Lambdastar__(self, daughters, pid_selection="_", tau_daughter='', b_daughter=''):

        _b2lstltau = CombineParticles(DecayDescriptors=["[Lambda_b0 -> B0 {0}+]cc".format(tau_daughter),
                                                        "[Lambda_b0 -> B0 {0}-]cc".format(tau_daughter)],
                                      MotherCut=self.LambdaBCut, CombinationCut=self.LambdaBCombCut)

        if tau_daughter == 'mu':
            tau_daughter = 'Muonic'
        elif tau_daughter == 'e':
            tau_daughter = 'Electronic'
        else:
            raise Exception('tau_daughter must be mu or e')

        if b_daughter not in ['mu', 'e']:
            raise Exception('b_daughter must be mu or e')

        sel = Selection("Lambdastar" + pid_selection + "for" + self.name.replace('Leptonic',
                        tau_daughter).replace('L', b_daughter), Algorithm=_b2lstltau, RequiredSelections=daughters)
        return sel
