###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from StandardParticles import StdAllLooseKaons
from StandardParticles import StdNoPIDsPions, StdLoosePions
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine
from PhysSelPython.Wrappers import Selection, SimpleSelection, PassThroughSelection, CombineSelection
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, DaVinci__N3BodyDecays
from GaudiConfUtils.ConfigurableGenerators import AddRelatedInfo
from Configurables import RelInfoVertexIsolation

__author__ = ['F. Blanc', "A. Venkateswaran"]
__date__ = '17/07/2023'
__version__ = '$Revision: 0.2$'

# Stripping line for B->tau nu

from GaudiKernel.SystemOfUnits import MeV
from GaudiKernel.SystemOfUnits import mm
"""
  B+ -> (tau+ -> pi+ pi- pi+ nu) nu, Bs2* -> (B+ -> (tau+ -> pi+ pi- pi+ nu) nu) K-
"""
__all__ = ('B2TauNuConf', 'default_config')

default_config = {
    'NAME': 'B2TauNu',
    'BUILDERTYPE': 'B2TauNuConf',
    'WGs': ['RD'],
    'CONFIG': {
        'SpdMult': '600',
        #
        'FDZ_MIN_B': 10,
        'FDZ_MAX_B': 100,
        'PT_MIN_B': 2500 * MeV,
        'P_MIN_B': 10000 * MeV,
        'FDCHI2_MIN_B': 16,
        'VTXCHI2_MAX_B': 6,
        'BPVDIRA_MIN_B': 0.9,
        'MASS_MIN_pipipi': 400 * MeV,
        'MASS_MAX_pipipi': 2100 * MeV,
        'MVIS_MIN_B': 500 * MeV,
        'MVIS_MAX_B': 1900 * MeV,
        'CORRM_MIN_B': 1900 * MeV,
        'CORRM_MAX_B': 10000 * MeV,
        'PT_BS2ST_MIN': 50 * MeV,
        'CORRM_MIN_B_BS2STAR': 1000 * MeV,
        'VCHI2_BK_MAX': 60,
        'VTXISODCHI2ONETRACK_MIN': 25,
        #
        'COMB_PT': 800 * MeV,
        'COMB_NUMPT': 1000 * MeV,
        'COMB_MAXDOCA': 0.2 * mm,
        'COMB_AM_M3PI_MAX': 1800 * MeV,
        'COMB_AM_BK_MAX': 4000 * MeV,
        #
        'PT_Pion': 250 * MeV,
        'P_Pion': 1000 * MeV,
        'IPCHI2_Tr': 16,
        'TRACKCHI2_Tr': 4,
        'TRGHOPROB_Tr': 0.4,
        'PROBNNPI_Pion': 0.55,
        'IPCHI2_PROMPTK_MAX': 9,
        'PIDK_MU_PROMPTK_MIN': 0,
        'PIDK_P_PROMPTK_MIN': 0,
        'PIDK_PI_PROMPTK_MIN': 16,
        'PT_PROMPTK_MIN': 500 * MeV,
        'PROBNNK_PROMPTK_MIN': 0.8,
        'TRACKCHI2_PROMPTK_MAX': 3,
        #
        'B2TauNu_LineNoPrescale': 1,
        'B2TauNu_LinePrescale': 0.1,
        'B2TauNu_LinePostscale': 1,
        #
        'B2TauNu_HLTSelection': "HLT_PASS_RE('Hlt1(Two)?Track.*Decision')",
        #
        'RelInfoTools': [
            {"Type": "RelInfoVertexIsolation",
             "Location": "BVars_VertexIsoInfo",
             "IgnoreUnmatchedDescriptors": True,
             "DaughterLocations": {"[Beauty -> ^Beauty Meson]CC": "B_VertexIsoInfo"}
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 0.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone05",
             "IgnoreUnmatchedDescriptors": True,
             "DaughterLocations":{"[Beauty -> ^Beauty Meson]CC": "B_ConeIsoInfo_Cone05"}
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 0.8,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone08",
             "IgnoreUnmatchedDescriptors": True,
             "DaughterLocations":{"[Beauty -> ^Beauty Meson]CC": "B_ConeIsoInfo_Cone08"}
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone10",
             "IgnoreUnmatchedDescriptors": True,
             "DaughterLocations":{"[Beauty -> ^Beauty Meson]CC": "B_ConeIsoInfo_Cone10"}
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.3,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone13",
             "IgnoreUnmatchedDescriptors": True,
             "DaughterLocations":{"[Beauty -> ^Beauty Meson]CC": "B_ConeIsoInfo_Cone13"}
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone15",
             "IgnoreUnmatchedDescriptors": True,
             "DaughterLocations":{"[Beauty -> ^Beauty Meson]CC": "B_ConeIsoInfo_Cone15"}
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 2.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone20",
             "IgnoreUnmatchedDescriptors": True,
             "DaughterLocations":{"[Beauty -> ^Beauty Meson]CC": "B_ConeIsoInfo_Cone20"}
             },
            {"Type": 'RelInfoVertexIsolationBDT',
             "Location": 'BVars_VertexIsoBDTInfo',
             "IgnoreUnmatchedDescriptors": True,
             "DaughterLocations": {"[Beauty -> ^Beauty Meson]CC": "B_VertexIsoBDTInfo"}
             }
        ],
        'RelInfoTools_TrackIso': [
            {'Type': 'RelInfoTrackIsolationBDT',
             'Variables': 2,
             'WeightsFile':  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             'DaughterLocations': {"[Beauty -> ^X+ X- X+]CC": "H1_TrackIsoBDTInfo",
                                   "[Beauty -> X+ ^X- X+]CC": "H2_TrackIsoBDTInfo",
                                   "[Beauty -> X+ X- ^X+]CC": "H3_TrackIsoBDTInfo",
                                   }
             }
        ],
        'RelInfoTools_TrackIso_SS': [
            {'Type': 'RelInfoTrackIsolationBDT',
             'Variables': 2,
             'WeightsFile':  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             'DaughterLocations': {"[Beauty -> ^X+ X+ X+]CC": "H1_TrackIsoBDTInfo",
                                   "[Beauty -> X+ ^X+ X+]CC": "H2_TrackIsoBDTInfo",
                                   "[Beauty -> X+ X+ ^X+]CC": "H3_TrackIsoBDTInfo",
                                   }
             }
        ],
        # RelInfo B2star->BK 
        # 'RelInfoTools_Bs2star2BK': [
        #     {"Type": "RelInfoVertexIsolation",
        #      "Location": "Bs2stVars_VertexIsoInfo",
        #      "DaughterLocations": {"[Beauty -> ^Beauty X-]CC": "B_VertexIsoInfo"}
        #      },
        #     {"Type": "RelInfoConeIsolation",
        #      "ConeSize": 0.5,
        #      "Variables": [],
        #      "Location": "Bs2stVars_ConeIsoInfo_Cone05",
        #      "DaughterLocations":{"[Beauty -> ^Beauty X-]CC": "B_ConeIsoInfo_Cone05"}
        #      },
        #     {"Type": "RelInfoConeIsolation",
        #      "ConeSize": 0.8,
        #      "Variables": [],
        #      "Location": "Bs2stVars_ConeIsoInfo_Cone08",
        #      "DaughterLocations":{"[Beauty -> ^Beauty X-]CC": "B_ConeIsoInfo_Cone08"}
        #      },
        #     {"Type": "RelInfoConeIsolation",
        #      "ConeSize": 1.0,
        #      "Variables": [],
        #      "Location": "Bs2stVars_ConeIsoInfo_Cone10",
        #      "DaughterLocations":{"[Beauty -> ^Beauty X-]CC": "B_ConeIsoInfo_Cone10"}
        #      },
        #     {"Type": "RelInfoConeIsolation",
        #      "ConeSize": 1.3,
        #      "Variables": [],
        #      "Location": "Bs2stVars_ConeIsoInfo_Cone13",
        #      "DaughterLocations":{"[Beauty -> ^Beauty X-]CC": "B_ConeIsoInfo_Cone13"}
        #      },
        #     {"Type": "RelInfoConeIsolation",
        #      "ConeSize": 1.5,
        #      "Variables": [],
        #      "Location": "Bs2stVars_ConeIsoInfo_Cone15",
        #      "DaughterLocations":{"[Beauty -> ^Beauty X-]CC": "B_ConeIsoInfo_Cone15"}
        #      },
        #     {"Type": "RelInfoConeIsolation",
        #      "ConeSize": 2.0,
        #      "Variables": [],
        #      "Location": "Bs2stVars_ConeIsoInfo_Cone20",
        #      "DaughterLocations":{"[Beauty -> ^Beauty X-]CC": "B_ConeIsoInfo_Cone20"}
        #      },
        #     {"Type": 'RelInfoVertexIsolationBDT',
        #         "Location": 'Bs2stVars_VertexIsoBDTInfo',
        #      "DaughterLocations": {"[Beauty -> ^Beauty X-]CC": "B_VertexIsoBDTInfo"}
        #      }
        # ],
        'RelInfoTools_TrackIso_Bs2star': [
            {'Type': 'RelInfoTrackIsolationBDT',
             'Variables': 2,
             'WeightsFile':  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             'DaughterLocations': {"[Beauty -> (Beauty -> ^X+ X- X+) X-]CC": "H1_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X+ ^X- X+) X-]CC": "H2_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X+ X- ^X+) X-]CC": "H3_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X+ X- X+) ^X-]CC": "BachH_TrackIsoBDTInfo"
                                   }
             }
        ],
        'RelInfoTools_TrackIso_Bs2starSS': [
            {'Type': 'RelInfoTrackIsolationBDT',
             'Variables': 2,
             'WeightsFile':  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             'DaughterLocations': {"[Beauty -> (Beauty -> ^X+ X- X+) X+]CC": "H1_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X+ ^X- X+) X+]CC": "H2_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X+ X- ^X+) X+]CC": "H3_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X+ X- X+) ^X+]CC": "BachH_TrackIsoBDTInfo"
                                   }
             }
        ],
        'RelInfoTools_TrackIso_Bs2starSS3pi': [
            {'Type': 'RelInfoTrackIsolationBDT',
             'Variables': 2,
             'WeightsFile':  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             "IgnoreUnmatchedDescriptors": True,
             'DaughterLocations': {"[Beauty -> (Beauty -> ^X+ X+ X+) X-]CC": "H1_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X+ ^X+ X+) X-]CC": "H2_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X+ X+ ^X+) X-]CC": "H3_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X+ X+ X+) ^X-]CC": "BachH_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> ^X- X- X-) X-]CC": "H1_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X- ^X- X-) X-]CC": "H2_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X- X- ^X-) X-]CC": "H3_TrackIsoBDTInfo",
                                   "[Beauty -> (Beauty -> X- X- X-) ^X-]CC": "BachH_TrackIsoBDTInfo"
                                   }
             }
        ],
    },
    'STREAMS': ['Bhadron']
}


class B2TauNuConf(LineBuilder):
    """
      Builder for B->TauNu
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        # removing TRCHI2DOF < 4 cut because already applied in reco
        trackCuts = "(MIPCHI2DV(PRIMARY) > %(IPCHI2_Tr)s) & (TRGHOSTPROB < %(TRGHOPROB_Tr)s)" % config
        trackCuts += " & (PT > %(PT_Pion)s) & (P > %(P_Pion)s)" % config
        PionCuts = trackCuts + " & (PROBNNpi > %(PROBNNPI_Pion)s)" % config

        self.FilterSPD = {
            'Code':
            " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )"
            % config,
            'Preambulo': [
                "from LoKiNumbers.decorators import *",
                "from LoKiCore.basic import LHCb"
            ]
        }

        self.selPions = SimpleSelection(
            "Pions" + name, FilterDesktop, [StdLoosePions], Code=PionCuts)
        self.selTracks = SimpleSelection(
            "Tracks" + name, FilterDesktop, [StdNoPIDsPions], Code=trackCuts)

        self.selB2TauNu = self._makeB2TauNu(name, self.selPions, config)

        self.selB2TauNu_Bs2star2BK = self._makeB2TauNu_Bs2star(
            name+"_Bs2star2BK", self.selPions, config)
        self.selB2TauNu_Bs2star2BK_SS3pi = self._makeB2TauNu_Bs2star(
            name+"_Bs2star2BK_SS3pi", self.selPions, config, SS=False, SS3pi=True)
        self.selB2TauNu_Bs2star2BKSS = self._makeB2TauNu_Bs2star(
            name+"_Bs2star2BKSS", self.selPions, config, SS=True)

        self.selB2TauNuNoPID = self._makeB2TauNu(
            name + "NoPID", self.selTracks, config)
        self.selB2TauNuSS = self._makeB2TauNu(
            name + "SS", self.selPions, config, SS=True)

        # Finished making selections build and register lines
        self.B2TauNu_Line = self._makeLine("B2TauNuLine",
                                           self.selB2TauNu, config)
        self.B2TauNuSS_Line = self._makeLine("B2TauNuSSLine",
                                             self.selB2TauNuSS, config)

        self.B2TauNu_Bs2star2BK_Line = self._makeLine("B2TauNu_Bs2star2BKLine",
                                                      self.selB2TauNu_Bs2star2BK, config)

        self.B2TauNu_Bs2star2BK_SS3pi_Line = self._makeLine("B2TauNu_Bs2star2BK_SS3pi_Line",
                                                            self.selB2TauNu_Bs2star2BK_SS3pi, config)

        self.B2TauNu_Bs2star2BKSS_Line = self._makeLine("B2TauNu_Bs2star2BKSSLine",
                                                        self.selB2TauNu_Bs2star2BKSS, config)

        self.B2TauNuNoPID_Line = self._makeLine("B2TauNuNoPIDLine",
                                                self.selB2TauNuNoPID, config, PreScale=True)

# Make B candidate selection
    def _makeB2TauNu_Bs2star(self, name, tracks, config, SS=False, SS3pi=False):
        combcut = "(APT>%(COMB_PT)s) & (AMAXDOCA('')<%(COMB_MAXDOCA)s) & (ANUM(PT > %(COMB_NUMPT)s) >= 1)" % config
        combcut += " & (in_range ( %(MASS_MIN_pipipi)s, AM, %(MASS_MAX_pipipi)s ) )" % config
        # combcut += "& (AM < %(COMB_AM_M3PI_MAX)s)" % config

        mothercut = "( BPVVDCHI2 > %(FDCHI2_MIN_B)s ) & ( BPVVDZ > %(FDZ_MIN_B)s*mm ) & ( BPVVDZ < %(FDZ_MAX_B)s*mm )" % config
        mothercut += " & (PT > %(PT_MIN_B)s) & (P > %(P_MIN_B)s) & (M > %(MVIS_MIN_B)s) & (M < %(MVIS_MAX_B)s) " % config
        mothercut += " & (VFASPF(VCHI2) < %(VTXCHI2_MAX_B)s) & (BPVDIRA > %(BPVDIRA_MIN_B)s )" % config
        mothercut += " & (BPVCORRM > %(CORRM_MIN_B)s ) & (BPVCORRM < %(CORRM_MAX_B)s ) " % config

        descriptors = ["[B+ -> pi+ pi- pi+]cc"]
        if SS3pi:
            descriptors = ["[B+ -> pi+ pi+ pi+]cc"]
        
        Combine_B23pi = CombineSelection(
            name+'_B23pi',
            [tracks],
            DecayDescriptors=descriptors,
            CombinationCut=combcut,
            MotherCut=mothercut)

        descriptors_Bs2star2BK = "[B*_s20 -> B+ K-]cc"
        if SS:
            descriptors_Bs2star2BK = "[B*_s20 -> B+ K+]cc"

        Combine_Bs2star2BK = CombineSelection(
            name,
            [Combine_B23pi, StdAllLooseKaons],
            DecayDescriptor=descriptors_Bs2star2BK,
            DaughtersCuts={
                'K+': "(PT > %(PT_PROMPTK_MIN)s) & (MIPCHI2DV(PRIMARY) < %(IPCHI2_PROMPTK_MAX)s) & (TRGHOSTPROB < %(TRGHOPROB_Tr)s) & (PIDK-PIDpi > %(PIDK_PI_PROMPTK_MIN)s) & (PIDK-PIDp > %(PIDK_P_PROMPTK_MIN)s) & (PIDK-PIDmu > %(PIDK_MU_PROMPTK_MIN)s) & (PROBNNk > %(PROBNNK_PROMPTK_MIN)s) & (TRCHI2DOF < %(TRACKCHI2_PROMPTK_MAX)s)" % config,
                'K-': "(PT > %(PT_PROMPTK_MIN)s) & (MIPCHI2DV(PRIMARY) < %(IPCHI2_PROMPTK_MAX)s) & (TRGHOSTPROB < %(TRGHOPROB_Tr)s) & (PIDK-PIDpi > %(PIDK_PI_PROMPTK_MIN)s) & (PIDK-PIDp > %(PIDK_P_PROMPTK_MIN)s) & (PIDK-PIDmu > %(PIDK_MU_PROMPTK_MIN)s) & (PROBNNk > %(PROBNNK_PROMPTK_MIN)s) & (TRCHI2DOF < %(TRACKCHI2_PROMPTK_MAX)s)" % config,
            },
            CombinationCut="(AM < %(COMB_AM_BK_MAX)s) & (abs(ACHILD(BPV(VZ),1)-ACHILD(BPV(VZ),2))<1.0*mm)" % config,
            MotherCut="(PT > %(PT_BS2ST_MIN)s) & (VFASPF(VCHI2) < %(VCHI2_BK_MAX)s)" % config
        )

        return Combine_Bs2star2BK

    def _makeB2TauNu(self, name, tracks, config, SS=False):

        combcut = "(APT>%(COMB_PT)s) & (AMAXDOCA('')<%(COMB_MAXDOCA)s) & (ANUM(PT > %(COMB_NUMPT)s) >= 1)" % config
        combcut += " & (in_range ( %(MASS_MIN_pipipi)s, AM, %(MASS_MAX_pipipi)s ) )" % config

        mothercut = "( BPVVDCHI2 > %(FDCHI2_MIN_B)s ) & ( BPVVDZ > %(FDZ_MIN_B)s*mm ) & ( BPVVDZ < %(FDZ_MAX_B)s*mm )" % config
        mothercut += " & (PT > %(PT_MIN_B)s) & (P > %(P_MIN_B)s) & (M > %(MVIS_MIN_B)s) & (M < %(MVIS_MAX_B)s) " % config
        mothercut += " & (VFASPF(VCHI2) < %(VTXCHI2_MAX_B)s) & (BPVDIRA > %(BPVDIRA_MIN_B)s )" % config
        mothercut += " & (BPVCORRM > %(CORRM_MIN_B)s ) & (BPVCORRM < %(CORRM_MAX_B)s ) " % config

        descriptors = ["[B+ -> pi+ pi- pi+]cc"]
        if SS:
            descriptors = ["[B+ -> pi+ pi+ pi+]cc"]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM < %(COMB_AM_M3PI_MAX)s" % config,
            CombinationCut=combcut,
            MotherCut=mothercut)

        presel = Selection(name, Algorithm=Combine,
                           RequiredSelections=[tracks])

        # determine vertex isolation variables, and add them to the candidate for further selection
        tool = RelInfoVertexIsolation('TauIsoInfo')
        algo = AddRelatedInfo()
        algo.Inputs = [presel.outputLocation()]
        algo.Location = 'Iso'
        algo.Tool = tool.getFullName()
        tau_withinfo = PassThroughSelection(
            name+'TauInfo', Algorithm=algo, RequiredSelection=presel)
        loc = presel.outputLocation().replace('/Particles', '/Iso')
        # apply selection based on isolation variables
        sel = SimpleSelection(name+"IsoFilter",
                              FilterDesktop,
                              [tau_withinfo],
                              Code="RELINFO('"+loc+"', 'VTXISODCHI2ONETRACK', 100000.) > %(VTXISODCHI2ONETRACK_MIN)s" % config)

        return sel

# Helpers to make lines

    def _makeLine(self, name, sel, config, PreScale=False):

        pscale = config['B2TauNu_LineNoPrescale']
        if PreScale:
            pscale = config['B2TauNu_LinePrescale']

        line = StrippingLine(
            name,
            prescale=pscale,
            postscale=config['B2TauNu_LinePostscale'],
            # MDSTFlag = False,
            FILTER=self.FilterSPD,
            HLT1=config['B2TauNu_HLTSelection'],
            # RelatedInfoTools=getRelInfoB2TauNu(),
            selection=sel,
            MaxCandidates=50)

        if 'SS' in name and 'Bs2star' not in name:
            line.RelatedInfoTools = config['RelInfoTools'] + \
                config['RelInfoTools_TrackIso_SS']
        elif 'Bs2star' in name and 'SS' not in name:
            line.RelatedInfoTools = config['RelInfoTools'] + \
                config['RelInfoTools_TrackIso_Bs2star']
        elif 'Bs2star' in name and 'SS3pi' in name:
            line.RelatedInfoTools = config['RelInfoTools'] + \
                config['RelInfoTools_TrackIso_Bs2starSS3pi']
        elif 'Bs2star' in name and 'SS' in name:
            line.RelatedInfoTools = config['RelInfoTools'] + \
                config['RelInfoTools_TrackIso_Bs2starSS']
        else:
            line.RelatedInfoTools = config['RelInfoTools'] + \
                config['RelInfoTools_TrackIso']

        self.registerLine(line)
        return line

# # Related Info


# def getRelInfoB2TauNu():

#     relInfo = []
#     for coneAngle in [0.5, 0.8, 1.0, 1.3, 1.5]:
#         conestr = str(coneAngle).replace('.', '')
#         relInfo += [{
#             "Type":
#             "RelInfoConeVariables",
#             "IgnoreUnmatchedDescriptors":
#             True,
#             "ConeAngle":
#             coneAngle,
#             "Location":
#             "VertexConeInfo",
#             "Variables": [
#                 'CONEANGLE', 'CONEMULT', 'CONEPASYM', 'CONEPTASYM',
#                 'CONEDELTAETA'
#             ],
#             "DaughterLocations": {
#                 "^[Beauty -> X+ X- X+]CC": 'P2ConeVar%s_B' % conestr
#             }
#         }]
#     relInfo += [{
#         "Type": "RelInfoVertexIsolation",
#         "Location": "VertexIsoInfo"
#     }]
#     return relInfo
