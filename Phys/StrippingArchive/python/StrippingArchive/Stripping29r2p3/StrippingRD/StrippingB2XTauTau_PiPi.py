###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for
    Bs to K K tau tau
    Bs to K* K* tau tau
    B0 to K* tau tau
    Lb to p K tau tau
Same-sign combinations are included.
"""

__author__ = 'H. Tilquin'
__date__ = '03/06/2021'
__version__ = '$Revision: 0.0 $'

__all__ = ('B2XTauTau_PiPiConf', 'default_config')

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop, DaVinci__N3BodyDecays

from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME': 'B2XTauTau_PiPi',
    'BUILDERTYPE': 'B2XTauTau_PiPiConf',
    'CONFIG':
        {
            "Bs_Comb_MassHigh": 7000.0,
            "Bd_Comb_MassHigh": 6300.0,
            "Lb_Comb_MassHigh": 7500.0,
            "Bs_VertDist": -1, 
            "Bd_VertDist": 0.5,
            "Lb_VertDist": -1,
            "B_DIRA": 0.9995,
            "Lb_DIRA": 0.9995,
            "Bs_VertexCHI2": 50.0,
            "B0_VertexCHI2": 25.0,
            "Lb_VertexCHI2": 50.0,
            "Dau_DIRA": 0.995,  
            "Dipion_DIRA": 0.975,  
            "Hadron_MinIPCHI2": 36,
            "DiHadron_MinIPCHI2": 36,
            "PionFromTau_MinIPCHI2": 16.0,
            "DipionUPPERMASS": 4350.0,
            "DipionLOWERMASS": 300,
            "Phi_FlightChi2": 50.0,
            "Phi_Comb_MassHigh": 1850.0,
            "Phi_PT": 800,
            "Phi_VertexCHI2": 3,
            "Kstar_for_B2Kstar_FlightChi2": 300.0,
            "Kstar_for_B2Kstar_Comb_MassHigh": 1750.0,
            "Kstar_for_B2Kstar_Comb_MassLow": 795.0,
            "Kstar_for_B2Kstar_PT": 2000,
            "Kstar_for_B2Kstar_VertexCHI2": 2.5,
            "Kstar_for_B2KstarKstar_MassWindow": 100,
            "Kstar_for_B2KstarKstar_PT": 400,
            "Kstar_for_B2KstarKstar_VertexCHI2": 6,
            "KstarKstar_DOCACHI2": 25,
            "KstarKstar_Comb_MassHigh": 2400,
            "Lambdastar_FlightChi2": 50,
            "Lambdastar_Comb_MassHigh": 2200.0,
            "Lambdastar_Comb_MassLow": 1400.0,
            "Lambdastar_PT": 1500,
            "Lambdastar_VertexCHI2": 3,
            "Dipion_VertexCHI2": 10,
            "KaonPID": 4.0,
            "Kaon_Proton_PID": -1,
            "Proton_Kaon_PID": -1,
            "ProtonPID": 5.0,
            "Pion_ProbNN": 0.97,
            "Pion_Kaon_ProbNN": 0.97,
            "Pion_ProbNN_B2Kstar": 0.96,
            "Kaon_Pion_ProbNN_B2Kstar": 0.96,
            "Hadron_P": 3000,
            "Pion_PT": 500,
            "Hadron_PT": 1000,
            "PionFromDipion_ANUM_PT": 1000,
            "SpdMult": 600,
            "Track_GhostProb": 0.3,
            "Track_TRCHI2": 3,
            "UseNoPIDsHadrons": False,
            "UseNoPIDsPions": False,
            "HLT1_FILTER": None,
            "HLT2_FILTER": None,
            "L0DU_FILTER": None,
        },

    'WGs': ['RD'],
    'STREAMS': ['EW']
}


class B2XTauTau_PiPiConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name

        self.BsCombCut = "(AM < %(Bs_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bs_VertDist)s * mm)" % config
        self.Bs_for_B2KstarKstarCombCut = "(AM < %(Bs_Comb_MassHigh)s * MeV)" % config
        self.BsCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Bs_VertexCHI2)s)" % config

        self.BdCombCut = "(AM < %(Bd_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bd_VertDist)s * mm)" % config
        self.BdCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(B0_VertexCHI2)s)" % config

        self.LambdaBCombCut = "(AM < %(Lb_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Lb_VertDist)s * mm)" % config
        self.LambdaBCut = "(BPVDIRA> %(Lb_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Lb_VertexCHI2)s)" % config

        DaughterCuts = "(BPVDIRA> %(Dau_DIRA)s)" % config

        self.PhiCombCut = "(AM < %(Phi_Comb_MassHigh)s * MeV)" % config
        self.PhiCut = DaughterCuts + " & (VFASPF(VCHI2/VDOF) < %(Phi_VertexCHI2)s) & (PT > %(Phi_PT)s * MeV) & " \
                                     "(BPVVDCHI2 > %(Phi_FlightChi2)s) & (MIPCHI2DV(PRIMARY) > %(DiHadron_MinIPCHI2)s)" % config

        self.Kstar_for_B2KstarCombCut = "(AM < %(Kstar_for_B2Kstar_Comb_MassHigh)s*MeV) & (AM > %(Kstar_for_B2Kstar_Comb_MassLow)s*MeV)" % config
        self.Kstar_for_B2KstarCut = DaughterCuts + " & (VFASPF(VCHI2/VDOF) < %(Kstar_for_B2Kstar_VertexCHI2)s) & " \
                                                   "(PT > %(Kstar_for_B2Kstar_PT)s * MeV) & (BPVVDCHI2 > %(Kstar_for_B2Kstar_FlightChi2)s) & (MIPCHI2DV(PRIMARY) > %(DiHadron_MinIPCHI2)s)" % config

        self.Kstar_for_B2KstarKstarCombCut = "(ADAMASS('K*(892)0')< %(Kstar_for_B2KstarKstar_MassWindow)s * MeV )" % config
        self.Kstar_for_B2KstarKstarCut = DaughterCuts + " &  (VFASPF(VCHI2/VDOF) < %(Kstar_for_B2KstarKstar_VertexCHI2)s) & " \
                                                        "(PT > %(Kstar_for_B2KstarKstar_PT)s * MeV)" % config

        self.LambdaStarCombCut = "(AM < %(Lambdastar_Comb_MassHigh)s * MeV) & (AM > %(Lambdastar_Comb_MassLow)s * MeV)" % config
        self.LambdaStarCut = DaughterCuts + " & (PT > %(Lambdastar_PT)s * MeV) & (VFASPF(VCHI2/VDOF) < %(Lambdastar_VertexCHI2)s) & (BPVVDCHI2 > %(Lambdastar_FlightChi2)s) & (MIPCHI2DV(PRIMARY) > %(DiHadron_MinIPCHI2)s)" % config

        self.DiPionCombCut = "(AM > %(DipionLOWERMASS)s * MeV) & (AM < %(DipionUPPERMASS)s * MeV) & (ANUM(PT < %(PionFromDipion_ANUM_PT)s * MeV) <= 1) & (ANUM(MIPCHI2DV(PRIMARY) < %(Hadron_MinIPCHI2)s) <= 1)" % config
        self.DiPionCut = "(BPVDIRA> %(Dipion_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Dipion_VertexCHI2)s)" % config

        self.TrackCuts = "(TRGHP < %(Track_GhostProb)s) & (TRCHI2DOF < %(Track_TRCHI2)s)" % config

        self.HadronCuts = "(MIPCHI2DV(PRIMARY) > %(Hadron_MinIPCHI2)s) & (PT > %(Hadron_PT)s * MeV) & (P > %(Hadron_P)s * MeV)" % config

        self.KaonCutBase = self.TrackCuts + " & " + self.HadronCuts
        self.KaonCut = self.KaonCutBase + " & (PIDK > %(KaonPID)s) & (~ISMUON) & (PIDK - PIDp > %(Kaon_Proton_PID)s)" % config
        self.KaonCut_B2Kstar = self.KaonCutBase + " & (PROBNNK * (1-PROBNNpi) > %(Kaon_Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.KaonCutReversePID_B2Kstar = self.KaonCutBase + " & (PROBNNK * (1-PROBNNpi) < %(Kaon_Pion_ProbNN_B2Kstar)s)" % config
        self.KaonCutReversePID = self.KaonCutBase + " & (PIDK < %(KaonPID)s)" % config

        self.PionCutBase = self.TrackCuts + " & " + self.HadronCuts
        self.PionCut_B2Kstar = self.PionCutBase + " & (PROBNNpi > %(Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.PionCutReversePID_B2Kstar = self.PionCutBase + " & (PROBNNpi < %(Pion_ProbNN_B2Kstar)s)" % config

        self.PionFromTauCutBase = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(PionFromTau_MinIPCHI2)s) & (PT > %(Pion_PT)s) & (P > %(Hadron_P)s * MeV)" % config
        self.PionFromTauCut = self.PionFromTauCutBase + " & (PROBNNpi > %(Pion_ProbNN)s) & (~ISMUON) & (PROBNNpi * (1 - PROBNNK) > %(Pion_Kaon_ProbNN)s) & (PIDp < 0) & (PIDK < 0)" % config
        self.PionFromTauCutReversePID = self.PionFromTauCutBase + " & (PROBNNpi < %(Pion_ProbNN)s)" % config

        self.ProtonCutBase = self.TrackCuts + " & " + self.HadronCuts
        self.ProtonCut = self.ProtonCutBase + " & (PIDp > %(ProtonPID)s)  & (~ISMUON) & (PIDp - PIDK > %(Proton_Kaon_PID)s)" % config
        self.ProtonCutReversePID = self.ProtonCutBase + " & (PIDp < %(ProtonPID)s)" % config

        self.Kaons = self.__Kaons__(config)
        self.FakeKaons = self.__FakeKaons__()

        self.Kaons_Kstar = self.__Kaons__(config, sel_name="_B2Kstar")
        self.FakeKaons_Kstar = self.__FakeKaons__(sel_name="_B2Kstar")

        self.Pions_Kstar = self.__Pions__(config, sel_name="_B2Kstar")
        self.FakePions_Kstar = self.__FakePions__(sel_name="_B2Kstar")

        self.Pions = self.__Pions__(config)
        self.FakePions = self.__FakePions__()

        self.PionsFromTau = self.__PionsFromTau__(config)
        self.FakePionsFromTau = self.__FakePionsFromTau__()

        self.Protons = self.__Protons__(config)
        self.FakeProtons = self.__FakeProtons__()

        self.Dipion = self.__Dipion__(self.PionsFromTau)
        self.FakeDipion = self.__FakeDipion__(self.PionsFromTau, self.FakePionsFromTau, conf=config)

        self.Phi = self.__Phi__(self.Kaons, conf=config)
        self.FakePhi = self.__Phi__(self.Kaons, self.FakeKaons, conf=config)

        self.Kstar_for_B2KstarKstar = self.__Kstar__(self.Kaons_Kstar, self.Pions_Kstar, sel_name="_B2KstarKstar")
        self.FakePionKstar_for_B2KstarKstar = self.__Kstar__(self.Kaons_Kstar, self.FakePions_Kstar, pid_selection="ReversePIDPi_",
                                                             sel_name="_B2KstarKstar")
        self.FakeKaonKstar_for_B2KstarKstar = self.__Kstar__(self.FakeKaons_Kstar, self.Pions_Kstar, pid_selection="ReversePIDK_",
                                                             sel_name="_B2KstarKstar")

        self.Kstar_for_B2Kstar = self.__Kstar__(self.Kaons_Kstar, self.Pions_Kstar, sel_name="_B2Kstar")
        self.FakePionKstar_for_B2Kstar = self.__Kstar__(self.Kaons_Kstar, self.FakePions_Kstar,
                                                        pid_selection="ReversePIDPi_", sel_name="_B2Kstar")
        self.FakeKaonKstar_for_B2Kstar = self.__Kstar__(self.FakeKaons_Kstar, self.Pions_Kstar,
                                                        pid_selection="ReversePIDK_", sel_name="_B2Kstar")

        self.LambdaStar = self.__Lambdastar__(self.Protons, self.Kaons)
        self.FakeProtonLambdaStar = self.__Lambdastar__(self.FakeProtons, self.Kaons, pid_selection="ReversePIDp_")
        self.FakeKaonLambdaStar = self.__Lambdastar__(self.Protons, self.FakeKaons, pid_selection="ReversePIDK_")

        self.DeclaredDaughters = [self.Phi, self.Dipion]
        self.Bs = self.__Bs_Phi__(daughters=self.DeclaredDaughters)

        self.DeclaredDaughters = [self.Phi, self.FakeDipion]
        self.Bs_ReversePIDPiFromTau = self.__Bs_Phi__(daughters=self.DeclaredDaughters, pid_selection="ReversePIDPiFromTau_")

        self.DeclaredDaughters = [self.FakePhi, self.Dipion]
        self.Bs_ReversePIDK = self.__Bs_Phi__(daughters=self.DeclaredDaughters, pid_selection="ReversePIDK_")

        self.DeclaredDaughters = [self.Kstar_for_B2KstarKstar, self.Dipion]
        self.Bs_kstarkstar = self.__Bs_KstarKstar__(daughters=self.DeclaredDaughters, conf=config)

        self.DeclaredDaughters = [self.FakeKaonKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.Dipion]
        self.Bs_kstarkstar_reversepidK = self.__Bs_KstarKstar__(daughters=self.DeclaredDaughters, conf=config, pid_selection="ReversePIDK_")

        self.DeclaredDaughters = [self.FakePionKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.Dipion]
        self.Bs_kstarkstar_reversepidpi = self.__Bs_KstarKstar__(daughters=self.DeclaredDaughters, conf=config, pid_selection="ReversePIDpi_")

        self.DeclaredDaughters = [self.Kstar_for_B2KstarKstar, self.FakeDipion]
        self.Bs_kstarkstar_reversepidpifromtau = self.__Bs_KstarKstar__(daughters=self.DeclaredDaughters, conf=config, pid_selection="ReversePIDPiFromTau_")

        self.DeclaredDaughters = [self.Kstar_for_B2Kstar, self.Dipion]
        self.Bs_kstar = self.__B0_Kstar__(daughters=self.DeclaredDaughters)

        self.DeclaredDaughters = [self.FakeKaonKstar_for_B2Kstar, self.Dipion]
        self.Bs_kstar_reversepidK = self.__B0_Kstar__(daughters=self.DeclaredDaughters, pid_selection="ReversePIDK_")

        self.DeclaredDaughters = [self.FakePionKstar_for_B2Kstar, self.Dipion]
        self.Bs_kstar_reversepidpi = self.__B0_Kstar__(daughters=self.DeclaredDaughters, pid_selection="ReversePIDpi_")

        self.DeclaredDaughters = [self.Kstar_for_B2Kstar, self.FakeDipion]
        self.Bs_kstar_reversepidpifromtau = self.__B0_Kstar__(daughters=self.DeclaredDaughters, pid_selection="ReversePIDPiFromTau_")

        self.LambdaB_pk = self.__LambdaB_pK__(daughters=[self.Dipion, self.LambdaStar])
        self.LambdaB_pk_ReversePIDK = self.__LambdaB_pK__(daughters=[self.Dipion, self.FakeKaonLambdaStar],
                                                     pid_selection="ReversePIDK_")
        self.LambdaB_pk_ReversePIDp = self.__LambdaB_pK__(daughters=[self.Dipion, self.FakeProtonLambdaStar],
                                                     pid_selection="ReversePIDp_")
        self.LambdaB_pk_ReversePIDPiFromTau = self.__LambdaB_pK__(daughters=[self.FakeDipion, self.LambdaStar],
                                                      pid_selection="ReversePIDPiFromTau_")

        self.FilterSPD = {'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                          'Preambulo': ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]}

        self.Bs2phi_line = StrippingLine(
            self.name + "_Bs2Phi_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phi_line)

        self.Bs2Phi_ReversePIDPiFromTau_line = StrippingLine(
            self.name + "_Bs2Phi_ReversePIDPiFromTauLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDPiFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_ReversePIDPiFromTau_line)

        self.Bs2Phi_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2Phi_ReversePIDKLine", prescale=0.07,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_ReversePIDK_line)

        self.Bs2KstarKstar_line = StrippingLine(
            self.name + "_Bs2KstarKstar_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_line)

        self.Bs2KstarKstar_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2KstarKstar_ReversePIDKLine", prescale=0.6,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_reversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_ReversePIDK_line)

        self.Bs2KstarKstar_ReversePIDpi_line = StrippingLine(
            self.name + "_Bs2KstarKstar_ReversePIDPiLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_ReversePIDpi_line)

        self.Bs2KstarKstar_ReversePIDPiFromTau_line = StrippingLine(
            self.name + "_Bs2KstarKstar_ReversePIDPiFromTauLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_reversepidpifromtau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_ReversePIDPiFromTau_line)

        self.Bd2Kstar_line = StrippingLine(
            self.name + "_B02KstarLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstar], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_line)

        self.Bd2Kstar_ReversePIDK_line = StrippingLine(
            self.name + "_B02Kstar_ReversePIDKLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstar_reversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_ReversePIDK_line)

        self.Bd2Kstar_ReversePIDpi_line = StrippingLine(
            self.name + "_B02Kstar_ReversePIDPiLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstar_reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_ReversePIDpi_line)

        self.Bd2Kstar_ReversePIDPiFromTau_line = StrippingLine(
            self.name + "_B02Kstar_ReversePIDPiFromTauLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstar_reversepidpifromtau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_ReversePIDPiFromTau_line)

        self.Lb2pK_line = StrippingLine(
            self.name + "_Lb2pK_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_line)

        self.Lb2pK_ReversePIDK_line = StrippingLine(
            self.name + "_Lb2pK_ReversePIDKLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_ReversePIDK_line)

        self.Lb2pK_ReversePIDp_line = StrippingLine(
            self.name + "_Lb2pK_ReversePIDpLine", prescale=0.15,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_ReversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_ReversePIDp_line)

        self.Lb2pK_ReversePIDPiFromTau_line = StrippingLine(
            self.name + "_Lb2pK_ReversePIDPiFromTauLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_ReversePIDPiFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_ReversePIDPiFromTau_line)

    def __Pions__(self, conf):
        from StandardParticles import StdAllLoosePions, StdAllNoPIDsPions
        pions = StdAllNoPIDsPions if conf['UseNoPIDsPions'] else StdAllLoosePions
        pion_cut = self.PionCutBase if conf['UseNoPIDsPions'] else self.PionCut
        _filter = FilterDesktop(Code=pion_cut)
        _sel = Selection("Selection_" + self.name + "_StdLoosePions", RequiredSelections=[pions],
                         Algorithm=_filter)
        return _sel

    def __FakePions__(self):
        from StandardParticles import StdAllNoPIDsPions
        _filter = FilterDesktop(Code=self.PionCutReversePID)
        _sel = Selection("Selection_" + self._name + "StdAllNoPIDsPions", Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsPions])
        return _sel

    def __PionsFromTau__(self, conf):
        from StandardParticles import StdAllLoosePions, StdAllNoPIDsPions
        pions = StdAllNoPIDsPions if conf['UseNoPIDsPions'] else StdAllLoosePions
        pion_cut = self.PionFromTauCutBase if conf['UseNoPIDsPions'] else self.PionFromTauCut
        _filter = FilterDesktop(Code=pion_cut)
        _sel = Selection("Selection_" + self.name + "_StdLoosePionsFromTau", RequiredSelections=[pions],
                         Algorithm=_filter)
        return _sel

    def __FakePionsFromTau__(self):
        from StandardParticles import StdAllNoPIDsPions
        _filter = FilterDesktop(Code=self.PionFromTauCutReversePID)
        _sel = Selection("Selection_" + self._name + "StdAllNoPIDsPionsFromTau", Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsPions])
        return _sel


    def __Protons__(self, conf):
        from StandardParticles import StdLooseProtons, StdNoPIDsProtons
        protons = StdNoPIDsProtons if conf['UseNoPIDsHadrons'] else StdLooseProtons
        proton_cuts = self.ProtonCutBase if conf['UseNoPIDsHadrons'] else self.ProtonCut
        _filter = FilterDesktop(Code=proton_cuts)
        _sel = Selection("Selection_" + self.name + "_StdLooseProtons", RequiredSelections=[protons], Algorithm=_filter)
        return _sel

    def __FakeProtons__(self):
        from StandardParticles import StdNoPIDsProtons
        _filter = FilterDesktop(Code=self.ProtonCutReversePID)
        _sel = Selection("Selection_" + self._name + "_StdAllNoPIDsProtons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsProtons])
        return _sel

    def __Kaons__(self, conf, sel_name="_"):
        from StandardParticles import StdLooseKaons, StdNoPIDsKaons
        kaons = StdNoPIDsKaons if conf['UseNoPIDsHadrons'] else StdLooseKaons
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.KaonCutBase)
        elif "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCut_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCut)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLooseKaons", RequiredSelections=[kaons],
                         Algorithm=_filter)
        return _sel

    def __FakeKaons__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsKaons
        if "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCutReversePID_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCutReversePID)
        _sel = Selection("Selection_" + self._name + sel_name + "StdAllNoPIDsKaons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsKaons])
        return _sel

    def __Pions__(self, conf, sel_name="_"):
        from StandardParticles import StdLoosePions, StdNoPIDsPions
        pions = StdNoPIDsPions if conf['UseNoPIDsHadrons'] else StdLoosePions
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.PionCutBase)
        else:
            _filter = FilterDesktop(Code=self.PionCut_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLoosePions", RequiredSelections=[pions],
                         Algorithm=_filter)
        return _sel

    def __FakePions__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsPions
        _filter = FilterDesktop(Code=self.PionCutReversePID_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdAllNoPIDsPions", RequiredSelections=[StdNoPIDsPions],
                         Algorithm=_filter)
        return _sel

    def __Dipion__(self, Pions):
        CombineDipion = CombineParticles()
        CombineDipion.DecayDescriptors = ["D0 -> pi- pi+", "D0 -> pi+ pi+", " D0 -> pi- pi-"]
        CombineDipion.CombinationCut = self.DiPionCombCut
        CombineDipion.MotherCut = self.DiPionCut
        _sel = Selection("Selection_" + self.name + "_Dipion", Algorithm=CombineDipion, RequiredSelections=[Pions])
        return _sel

    def __FakeDipion__(self, Pions, fakePions, conf):
        CombineDipion = CombineParticles()
        CombineDipion.DecayDescriptors = ["D0 -> pi- pi+", "D0 -> pi+ pi+", " D0 -> pi- pi-"]
        CombineDipion.CombinationCut = self.DiPionCombCut + " & (AHASCHILD((PROBNNpi < %(Pion_ProbNN)s)))" % conf
        CombineDipion.MotherCut = self.DiPionCut
        _sel = Selection("Selection_" + self.name + "_DipionNoPID", Algorithm=CombineDipion,
                         RequiredSelections=[Pions, fakePions])
        return _sel

    def __Phi__(self, Kaons, fakekaon=None, conf=None):
        _phi2kk = CombineParticles()
        _phi2kk.DecayDescriptors = ["phi(1020) -> K+ K-", "phi(1020) -> K+ K+", "phi(1020) -> K- K-"]
        _phi2kk.MotherCut = self.PhiCut
        if fakekaon is None:
            _phi2kk.CombinationCut = self.PhiCombCut
            _sel = Selection("Phi_selection_for" + self.name, Algorithm=_phi2kk, RequiredSelections=[Kaons])
        else:
            _phi2kk.CombinationCut = self.PhiCombCut + " & (AHASCHILD((PIDK < %(KaonPID)s)))" % conf
            _sel = Selection("Phi_NoPIDK_selection_for" + self.name, Algorithm=_phi2kk,
                             RequiredSelections=[Kaons, fakekaon])
        return _sel

    def __Kstar__(self, Kaons, Pions, pid_selection="_", sel_name="_B2KstarKstar"):

        _kstar2kpi = CombineParticles()
        if sel_name == "_B2KstarKstar":
            _kstar2kpi.DecayDescriptors = ["[K*(892)0 -> K+ pi-]cc"]
            _kstar2kpi.CombinationCut = self.Kstar_for_B2KstarKstarCombCut
            _kstar2kpi.MotherCut = self.Kstar_for_B2KstarKstarCut
        else:
            _kstar2kpi.DecayDescriptors = ["[K*(892)0 -> K+ pi-]cc", "K*(892)0 -> K+ pi+", "K*(892)0 -> K- pi-"]
            _kstar2kpi.CombinationCut = self.Kstar_for_B2KstarCombCut
            _kstar2kpi.MotherCut = self.Kstar_for_B2KstarCut
        _sel = Selection("Kstar_for" + sel_name + pid_selection + "selection_for" + self.name, Algorithm=_kstar2kpi,
                         RequiredSelections=[Kaons, Pions])
        return _sel

    def __Lambdastar__(self, Protons, Kaons, pid_selection="_"):
        _lstar2pk = CombineParticles()
        _lstar2pk.DecayDescriptors = ["[Lambda(1520)0 -> p+ K-]cc", "Lambda(1520)0 -> p+ K+", "Lambda(1520)0 -> p~- K-"]
        _lstar2pk.CombinationCut = self.LambdaStarCombCut
        _lstar2pk.MotherCut = self.LambdaStarCut
        _sel = Selection("Lambdastar" + pid_selection + "selection_for" + self.name, Algorithm=_lstar2pk,
                         RequiredSelections=[Protons, Kaons])
        return _sel

    def __Bs_Phi__(self, daughters, pid_selection="_"):
        _b2phitautau = CombineParticles(DecayDescriptors=["B_s0 -> phi(1020) D0"],
                                        MotherCut=self.BsCut, CombinationCut=self.BsCombCut)
        sel = Selection("Phi" + pid_selection + "for" + self.name, Algorithm=_b2phitautau, RequiredSelections=daughters)
        return sel

    def __Bs_KstarKstar__(self, daughters, conf, pid_selection="_"):
        _b2kstarkstartautau = DaVinci__N3BodyDecays()
        _b2kstarkstartautau.DecayDescriptors = ["B_s0 -> K*(892)0 K*(892)~0 D0", "B_s0 -> K*(892)0 K*(892)0 D0", "B_s0 -> K*(892)~0 K*(892)~0 D0"]
        _b2kstarkstartautau.CombinationCut = self.Bs_for_B2KstarKstarCombCut
        _b2kstarkstartautau.MotherCut = self.BsCut
        _b2kstarkstartautau.Combination12Cut = "(ACHI2DOCA(1,2) < %(KstarKstar_DOCACHI2)s) & (AM < %(KstarKstar_Comb_MassHigh)s * MeV)" % conf
        sel = Selection("KstarKstar" + pid_selection + "for" + self.name + "_daughters", Algorithm=_b2kstarkstartautau,
                        RequiredSelections=daughters)
        return sel

    def __B0_Kstar__(self, daughters, pid_selection="_"):
        _b2kstartautau = CombineParticles(DecayDescriptors=["B0 -> K*(892)0 D0", "B~0 -> K*(892)~0 D0"],
                                          MotherCut=self.BdCut, CombinationCut=self.BdCombCut)
        sel = Selection("Kstar" + pid_selection + "for" + self.name, Algorithm=_b2kstartautau,
                        RequiredSelections=daughters)
        return sel

    def __LambdaB_pK__(self, daughters, pid_selection="_"):
        _b2lambdatautau = CombineParticles(DecayDescriptors=["Lambda_b0 -> Lambda(1520)0 D0", "Lambda_b~0 -> Lambda(1520)~0 D0"],
                                           MotherCut=self.LambdaBCut, CombinationCut=self.LambdaBCombCut)
        sel = Selection("Lambda" + pid_selection + "for" + self.name, Algorithm=_b2lambdatautau,
                        RequiredSelections=daughters)
        return sel
