###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of B0 ->p p~ gamma stripping Selections and StrippingLines.
Provides class B2ppbargammaConf, which constructs the Selections and StrippingLines
given a configuration dictionary.
Exported symbols (use python help!):
   - B2ppbargammaConf
"""

__author__ = ["Xingyu Tong"]
__date__ = '10/07/2023'
__version__ = '$Revision: 1.0 $'
__all__ = 'StrippingB2ppbargammaConf'

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

from StandardParticles import StdLooseProtons as Protons
from StandardParticles import StdLooseAllPhotons as Photons

'''
2016:
StrippingReport                                                INFO Event 20000, Good event 20000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |_StrippingGlobal_                                            |  0.1350|        27|       |   4.633|
 |_StrippingSequenceStreamBhadron_                             |  0.1350|        27|       |   4.623|
 |!StrippingB2ppbargammaLine                                   |  0.1350|        27|  3.259|   3.905|
 |!StrippingB2ppbargammaLine_TIMING                            |  0.1350|        27|  3.259|   0.077|
2017:
StrippingReport                                                INFO Event 20000, Good event 20000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |_StrippingGlobal_                                            |  0.1650|        33|       |   5.633|
 |_StrippingSequenceStreamBhadron_                             |  0.1650|        33|       |   5.619|
 |!StrippingB2ppbargammaLine                                   |  0.1650|        33|  2.970|   4.801|
 |!StrippingB2ppbargammaLine_TIMING                            |  0.1650|        33|  2.970|   0.086|
2018:
StrippingReport                                                INFO Event 20000, Good event 20000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |_StrippingGlobal_                                            |  0.1550|        31|       |   5.934|
 |_StrippingSequenceStreamBhadron_                             |  0.1550|        31|       |   5.922|
 |!StrippingB2ppbargammaLine                                   |  0.1550|        31|  3.226|   5.093|
 |!StrippingB2ppbargammaLine_TIMING                            |  0.1550|        31|  3.226|   0.110|
'''

default_config = {
    'NAME': 'B2ppbargamma',
    'WGs': ['RD'],
    'BUILDERTYPE': 'B2ppbargammaConf',
    'CONFIG': {'Trk_Chi2': 4.0,
               'Trk_GhostProb': 0.4,
               'gamma_PT_MIN': 1000.0,
               'gamma_CL_MIN': 0.25,
               'gamma_ISNOTE_MIN': -999.0,
               'ProtonP': 5000.,  # MeV
               'ProtonPT': 500.,  # MeV
               'ProtonPIDp': 5.,
               'ProtonDLLpK': 5.,
               'B0_Mlow': 779.0,
               'B0_Mhigh': 1921.0,
               'B0_APTmin': 0.0,
               'B0_PTmin': 0.0,
               'B0Daug_maxDocaChi2': 5.0,
               'B0Daug_PTsum': 0.0,
               'B0_VtxChi2': 16.0,
               'B0_Dira': 0.9990,
               'B0_IPCHI2wrtPV': 25.0,
               'B0_FDwrtPV': 0.8,
               'B0_FDChi2': 25,
               'GEC_MaxTracks': 250,
               'Prescale': 1.0,
               'Postscale': 1.0,
               'RelatedInfoTools': [{"Type": "RelInfoConeVariables",
                                     "ConeAngle": 1.7,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar17'
                                     },
                                    {"Type": "RelInfoConeVariables",
                                     "ConeAngle": 1.5,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar15'
                                     },
                                    {"Type": "RelInfoConeVariables",
                                     "ConeAngle": 1.0,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar10'
                                     },
                                    {"Type": "RelInfoConeVariables",
                                     "ConeAngle": 0.8,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar08'
                                     },
                                    {"Type": "RelInfoVertexIsolation",
                                     "Location": "VtxIsolationVar"
                                     }
                                    ]
               },
    'STREAMS': ['Bhadron']
}


class B2ppbargammaConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        GECCode = {'Code': "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %s)" % config['GEC_MaxTracks'],
                   'Preambulo': ["from LoKiTracks.decorators import *"]}

        self.protons = Protons
        self.photons = Photons

        namesSelections = [(name, self.makeB2ppbargamma(name, config)),
                           ]

        # make lines

        for selName, sel in namesSelections:

            extra = {}

            line = StrippingLine(selName + 'Line',
                                 selection=sel,
                                 prescale=config['Prescale'],
                                 postscale=config['Postscale'],
                                 RelatedInfoTools=config['RelatedInfoTools'],
                                 FILTER=GECCode,
                                 **extra)

            self.registerLine(line)

    def makeB2ppbargamma(self, name, config):
        """
        Create and store a B0 -> p+ p~- gamma Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _gammacut = "(PT > %s*MeV)" % config['gamma_PT_MIN']
        _gammacut += '&'+"(CL > %f)" % config['gamma_CL_MIN']
        _gammacut += '&' + \
            "(PPINFO(LHCb.ProtoParticle.IsNotE,-1) > %s)" % config['gamma_ISNOTE_MIN']

        _massCutLow = "(AM>(5279-%s)*MeV)" % config['B0_Mlow']
        _massCutHigh = "(AM<(5279+%s)*MeV)" % config['B0_Mhigh']
        _aptCut = "(APT>%s*MeV)" % config['B0_APTmin']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))" % config['B0Daug_maxDocaChi2']
        _daugPtSumCut = "((APT1+APT2+APT3)>%s*MeV)" % config['B0Daug_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut = "(PT>%s*MeV)" % config['B0_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)" % config['B0_VtxChi2']
        _diraCut = "(BPVDIRA>%s)" % config['B0_Dira']
        _ipChi2Cut = "(MIPCHI2DV(PRIMARY)<%s)" % config['B0_IPCHI2wrtPV']
        _fdCut = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B0_FDwrtPV']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['B0_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdChi2Cut
        _motherCuts += '&'+_fdCut

        _B0 = CombineParticles()

        _B0.DecayDescriptors = ["B0 -> p+ p~- gamma"]

        _trkGhostProbCut = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut = "(TRCHI2DOF<%s)" % config['Trk_Chi2']
        _protonPCut = "(P>%s*MeV)" % config["ProtonP"]
        _protonPTCut = "(PT>%s*MeV)" % config["ProtonPT"]
        _protonPIDpCut = "(PIDp-PIDpi> %s ) " % config["ProtonPIDp"]
        _protonDLLpKCut = "(PIDp-PIDK> %s ) " % config["ProtonDLLpK"]

        _daughtersCuts = _trkChi2Cut
        _daughtersCuts += '&'+_trkGhostProbCut
        _daughtersCuts += '&' + _protonPCut
        _daughtersCuts += '&' + _protonPTCut
        _daughtersCuts += '&' + _protonPIDpCut
        _daughtersCuts += '&' + _protonDLLpKCut

        _B0.DaughtersCuts = {"p+": _daughtersCuts,
                             "p~-": _daughtersCuts,
                             "gamma": _gammacut,
                             }
        _B0.CombinationCut = _combCuts
        _B0.MotherCut = _motherCuts
        _B0.ReFitPVs = True

        _B0Conf = _B0.configurable(name + '_combined')

        self.selB2ppbargamma = Selection(
            name, Algorithm=_B0Conf, RequiredSelections=[self.protons, self.photons])
        return self.selB2ppbargamma
