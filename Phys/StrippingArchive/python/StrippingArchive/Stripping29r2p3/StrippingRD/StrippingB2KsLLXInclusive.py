###############################################################################
#
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration
#
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization
# or submit itself to any jurisdiction.
###############################################################################
__author__ = ['R. Coutinho', 'F. Almeida']
__date__ = '13/07/2023'
__version__ = '$Revision: 0 $'

__all__ = ('B2KsLLXInclusiveConf', 'default_config')

"""
Stripping selection for inclusively selected B->KS0LLX decays.
"""

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from LHCbKernel.Configuration import *

#################
#
# Define Cuts here
#
#################

default_config = {
    'NAME': 'B2KsLLXInclusive',
    'BUILDERTYPE': 'B2KsLLXInclusiveConf',
    'CONFIG': {
        # Incl (dimu) cuts
        'IPCHI2': 9.0,
        'FlightChi2': 100.0,
        'DIRA': 0.995,
        'VertexCHI2': 6.0,
        'LOWERMASS': 0.040,  # MeV
        'UPPERMASS': 5000.0,  # MeV
        'CORRM_MIN': 3000.0,  # MeV
        'CORRM_MAX': 15000.0,  # MeV
        # Track cuts
        'Track_CHI2nDOF': 3.0,
        'Track_GhostProb': 0.5,

        # Muon cuts
        'Muon_MinIPCHI2': 16.0,
        'Muon_PIDmu': 0.0,
        'Muon_PIDmuK': 0.0,
        'Muon_PT': 500,

        # Electron cuts
        'Electron_MinIPCHI2': 16.0,
        'Electron_PIDe': 0,
        'Electron_PT': 500,

        # Kshort cuts
        'Kshort_MassWindow': 30.0,
        'Kshort_PT': 300.,
        'Kshort_Tau': 2,
        'Kshort_daughters_IPCHI2': 4,

        # Wrong sign combinations
        'WS': False,

        # GEC
        'SpdMult': 450,
        'HLT_FILTER': "HLT_PASS_RE('Hlt2DiMuonDetachedDecision')|HLT_PASS_RE('Hlt2DiMuonDetachedHeavyDecision')|HLT_PASS_RE('Hlt2SingleMuonDecision')",
    },
    'WGs': ['RD'],
    'STREAMS': ['Leptonic']
}

#################
#
#  Make line here
#
#################

defaultName = 'B2KsLLXInclusive'


class B2KsLLXInclusiveConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.InclDiMuCombCut = "(AM > %(LOWERMASS)s *MeV) & " \
                               "(AM < %(UPPERMASS)s *MeV)" % config
        self.InclDiMuCut = "(M > %(LOWERMASS)s) & " \
                           "(M < %(UPPERMASS)s)" % config
        self.InclKSLLCut = "(BPVCORRM > %(CORRM_MIN)s *MeV) & " \
            "(BPVCORRM < %(CORRM_MAX)s *MeV) &" \
            "(BPVDIRA > %(DIRA)s) & " \
            "(BPVVDCHI2 > %(FlightChi2)s) & " \
            "(VFASPF(VCHI2/VDOF) < %(VertexCHI2)s)" % config
        self.TrackCuts = "(TRCHI2DOF < %(Track_CHI2nDOF)s) & (TRGHP < %(Track_GhostProb)s)" % config
        self.MuonCut = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s) & " \
            " (PIDmu> %(Muon_PIDmu)s) & " \
            " (PIDmu-PIDK> %(Muon_PIDmuK)s) & "\
            " (PT > %(Muon_PT)s)" % config
        self.DiElectronCut = "(ID=='J/psi(1S)') & "\
                             "(MINTREE(ABSID<14,PT) > %(Electron_PT)s *MeV) & "\
                             "(2 == NINTREE((ABSID==11)&(PIDe > %(Electron_PIDe)s))) & "\
                             "(MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s)" % config
        self.KSCut = "(ADMASS('KS0') < %(Kshort_MassWindow)s * MeV) & " \
            " (PT > %(Kshort_PT)s * MeV) & " \
            " (NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(Kshort_daughters_IPCHI2)s))) & " \
            " (BPVLTIME() > %(Kshort_Tau)s * ps)" % config
        self.Muons = self.__Muons__(config)
        self.KS = self.__Kshort__(config)
        self.InclDimu = self.__InclDimu__(config)
        self.InclDielectron = self.__InclDielectron__(config)
        self.InclDielectronSS = self.__makeEESS__(config)
        self.InclKSMuMu = self.__InclKSMuMu__(config)
        self.InclKSee = self.__InclKSee__(config)
        self.InclKSeeSS = self.__InclKSeeSS__(config)

        # inclusive dimuon line
        self.inclusive_KSMuMu_line = StrippingLine(
            self.name+"_InclKSMuMuLine",
            prescale=1,
            FILTER={
                'Code': "( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.InclKSMuMu]
        )
        # inclusive dielectron line
        self.inclusive_KSee_line = StrippingLine(
            self.name+"_InclKSeeLine",
            prescale=1,
            FILTER={
                'Code': "( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.InclKSee]
        )
        # inclusive dielectron SS line
        self.inclusive_KSeeSS_line = StrippingLine(
            self.name+"_InclKSeeSSLine",
            prescale=0.15,
            FILTER={
                'Code': "( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.InclKSeeSS]
        )

        self.registerLine(self.inclusive_KSMuMu_line)
        self.registerLine(self.inclusive_KSee_line)
        self.registerLine(self.inclusive_KSeeSS_line)

    def __Muons__(self, conf):
        """
        Filter muons from StdLooseMuons
        """
        _code = self.MuonCut
        _muons = DataOnDemand(Location='Phys/StdLooseMuons/Particles')
        _name = "Selection_"+self.name+"_Muons"

        _filter = FilterDesktop(Code=_code)
        _sel = Selection(_name, RequiredSelections=[_muons], Algorithm=_filter)
        return _sel

    def __Kshort__(self, conf):
        """
        Filter kshort from StdLooseKshort
        """
        _ksdd = AutomaticData(Location='Phys/StdLooseKsDD/Particles')
        _ksll = AutomaticData(Location='Phys/StdVeryLooseKsLL/Particles')
        _filter_ksdd = FilterDesktop(Code=self.KSCut)
        _filter_ksll = FilterDesktop(Code=self.KSCut)
        _selksdd = Selection("Selection_"+self.name+"_Ksdd",
                             RequiredSelections=[_ksdd],
                             Algorithm=_filter_ksdd)
        _selksll = Selection("Selection_"+self.name+"_Ksll",
                             RequiredSelections=[_ksll],
                             Algorithm=_filter_ksll)

        _sel = MergedSelection("Selection_"+self.name+"_Kshort",
                               RequiredSelections=[_selksdd, _selksll])
        return _sel

    def __InclDielectron__(self, conf):
        '''
        Create a new dimuon for high q2 inclusive B->Xee
        '''
        from StandardParticles import StdDiElectronFromTracks as DiElectrons

        _name = "Sel_"+self.name+"_electronfilter"
        _Code = self.DiElectronCut

        _Filter = FilterDesktop(Code=_Code)
        from PhysSelPython.Wrappers import Selection
        SelDiElectron = Selection(
            _name, Algorithm=_Filter, RequiredSelections=[DiElectrons])
        return SelDiElectron

    def __makeEESS__(self, conf):
        """
        Makes EE same-sign combinations
        """
        _name = "Sel_"+self.name+"_DiElectronSSFilter"
        from Configurables import DiElectronMaker, ProtoParticleCALOFilter
        from CommonParticles.Utils import trackSelector
        from GaudiKernel.SystemOfUnits import MeV
        ee = DiElectronMaker('DiElectronsSS' + _name)
        ee.Particle = "J/psi(1S)"
        # ee.DecayDescriptor = "[J/psi(1S) -> e+ e+]cc"
        selector = trackSelector(ee, trackTypes=["Long"])

        ee.addTool(ProtoParticleCALOFilter('Electron'))
        ee.Electron.Selection = ["RequiresDet='CALO'"]
        ee.DiElectronMassMin = 0.*MeV
        ee.DiElectronMassMax = 5000.*MeV
        ee.DiElectronPtMin = 500.*MeV
        ee.OppositeSign = 0

        ee.Electron.Selection = [
            "RequiresDet='CALO' CombDLL(e-pi)>'%(Electron_PIDe)s'" % conf]

        return Selection(_name+'eeSS_Selection', Algorithm=ee)

    def __InclDimu__(self, conf):
        '''
        Create a new dimuon for high q2 inclusive B->Xmumu
        '''
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineDiMuon = CombineParticles()
        CombineDiMuon.DecayDescriptors = [
            "J/psi(1S) -> mu- mu+"]
        CombineDiMuon.MotherCut = self.InclDiMuCut

        muons = self.Muons
        _name = "Sel_"+self.name+"DiMu"

        _sel = MergedSelection(
            _name+"_Merged", RequiredSelections=[muons, muons])

        from PhysSelPython.Wrappers import Selection
        SelDiMuon = Selection(
            _name, Algorithm=CombineDiMuon, RequiredSelections=[_sel])
        return SelDiMuon

    def __InclKSMuMu__(self, conf):
        '''
        Create a new dimuon for high q2 inclusive B->KS0mumuX
        '''
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineKSMuMu = CombineParticles()
        CombineKSMuMu.DecayDescriptors = ["B0 -> J/psi(1S) KS0"]
        sel_name = "InclKSMuMu"
        CombineKSMuMu.MotherCut = self.InclKSLLCut
        # choose
        dimuon = self.InclDimu
        kshort = self.KS
        _name = "Sel_"+self.name+"KSMuMu"

        from PhysSelPython.Wrappers import Selection
        SelKSMuMu = Selection(_name, Algorithm=CombineKSMuMu,
                              RequiredSelections=[dimuon, kshort])
        return SelKSMuMu

    def __InclKSee__(self, conf):
        '''
        Create a new dimuon for high q2 inclusive B->Xmumu
        '''
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineKSee = CombineParticles()
        CombineKSee.DecayDescriptors = ["B0 -> J/psi(1S) KS0"]
        sel_name = "InclKSee"
        CombineKSee.MotherCut = self.InclKSLLCut
        # choose
        dielectron = self.InclDielectron
        kshort = self.KS
        _name = "Sel_"+self.name+"KSee"

        from PhysSelPython.Wrappers import Selection
        SelKSee = Selection(_name, Algorithm=CombineKSee,
                            RequiredSelections=[dielectron, kshort])
        return SelKSee

    def __InclKSeeSS__(self, conf):
        '''
        Create a new KSee for high q2 inclusive B->KSeeX
        '''
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineDiElectron = CombineParticles()
        CombineDiElectron.DecayDescriptors = ["B0 -> J/psi(1S) KS0"]
        sel_name = "InclKSee"
        CombineDiElectron.MotherCut = self.InclKSLLCut
        # choose
        dielectronSS = self.InclDielectronSS
        kshort = self.KS
        _name = "Sel_"+self.name+"KSeeSS"

        from PhysSelPython.Wrappers import Selection
        SelDiElectron = Selection(_name, Algorithm=CombineDiElectron,
                                  RequiredSelections=[dielectronSS, kshort])
        return SelDiElectron
