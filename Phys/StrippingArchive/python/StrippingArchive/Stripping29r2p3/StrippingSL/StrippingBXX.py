"""Define BXX stripping lines.

Revival of StrippingDstRSwD02K2PiD0forBXXLine and StrippingDstWSwD02K2PiD0forBXXLine,
previously used in Stripping21r1.
"""
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = ["Michael Wilkinson"]
__date__ = "13/07/2023"
__version__ = "$Revision: 2.0 $"

from string import Template

from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PhysConf.Selections import (
    Combine3BodySelection,
    CombineSelection,
    Hlt2TOSSelection,
    L0TOSSelection,
)
from PhysSelPython.Wrappers import AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

__all__ = ("BXXLinesBuilderConf", "default_config")

default_config = {
    "BXX": {
        "BUILDERTYPE": "BXXLinesBuilderConf",
        "CONFIG": {
            "InputPionsForDstarLocation": "Phys/StdAllLoosePions/Particles",
            "InputPionsForDzeroLocation": "Phys/StdLoosePions/Particles",
            "InputKaonsForDzeroLocation": "Phys/StdLooseKaons/Particles",
            "prescaleRS": 1.0,
            "prescaleWS": 1.0,
            "DzeroDaughterTRCHI2DOFMax": 3.0,
            "DzeroDaughterPTMin": 400 * MeV,
            "DzeroDaughterPMin": 2 * GeV,
            "DzeroDaughterMIPCHI2DVMin": 4.0,
            "DzeroDaughterKaonPIDKMin": 4.0,
            "DzeroDaughterPionPIDKMax": 10.0,
            "DzeroDaughterPionPIDmuMax": 10.0,
            "DzeroMassMin": 1.4 * GeV,
            "DzeroMassMax": 1.7 * GeV,
            "DzeroPTMin": 3 * GeV,
            "DzeroADOCACHI2CUT": 20.0,
            "DzeroPionPairMassMin": 575.5 * MeV,
            "DzeroPionPairMassMax": 975.5 * MeV,
            "DzeroVCHI2DOFMax": 6.0,
            "DzeroBPVVDMin": 4 * mm,
            "DzeroBPVVDCHI2Min": 120.0,
            "DzeroBPVDIRAMin": 0.9997,
            "DzeroBPVIPCHI2Max": 25,
            "DstarPionPTMin": 250 * MeV,
            "DstarPionTRCHI2DOFMax": 3.0,
            "DstarPionMIPDVMax": 0.3 * mm,
            "DstarPionMIPCHI2DVMax": 4.0,
            "DstarPionPIDKMax": 10.0,
            "DstarMassDiffDzeroPlusPionMax": 40 * MeV,
            "DstarPTMin": 3 * GeV,
            "DstarVCHI2DOFMax": 5.0,
            "DstarBPVVDCHI2Max": 25.0,
            "DstarBPVIPCHI2Max": 25.0,
            "DstarL0TOSSelection": "L0Hadron.*",
            "DstarHLT2TOSSelection": "Hlt2CharmHadInclDst.*",
        },
        "STREAMS": {
            "Semileptonic": [
                "StrippingDstRSwD02K2PiD0forBXXLine",
                "StrippingDstWSwD02K2PiD0forBXXLine",
            ],
        },
        "WGs": ["Charm", "Semileptonic"],
    }
}


class BXXLinesBuilderConf(LineBuilder):
    """BXX Stripping lines."""

    __configuration_keys__ = default_config["BXX"]["CONFIG"].keys()

    __confdict__ = {}

    def __init__(self, name, config):
        """Create Stripping lines Dst{RS,WS}wD02K2PiD0for{name}Line."""
        LineBuilder.__init__(self, name, config)
        self.__confdict__ = config

        # -- get input particles
        inputs = {
            "InputPionsForDstar": AutomaticData(
                Location=config["InputPionsForDstarLocation"]
            ),
            "InputPionsForDzero": AutomaticData(
                Location=config["InputPionsForDzeroLocation"]
            ),
            "InputKaonsForDzero": AutomaticData(
                Location=config["InputKaonsForDzeroLocation"]
            ),
        }

        # -- create D0
        DzeroDaughterCuts = Template(
            "(TRCHI2DOF < ${DzeroDaughterTRCHI2DOFMax})"
            " & (PT > ${DzeroDaughterPTMin}) & (P > ${DzeroDaughterPMin})"
            " & (MIPCHI2DV(PRIMARY) > ${DzeroDaughterMIPCHI2DVMin})"
        ).substitute(config)
        DzeroDaughterKaonCuts = Template(
            "(PIDK > ${DzeroDaughterKaonPIDKMin})"
        ).substitute(config)
        DzeroDaughterPionCuts = Template(
            "(PIDK < ${DzeroDaughterPionPIDKMax})"
            " & (PIDmu < ${DzeroDaughterPionPIDmuMax})"
        ).substitute(config)
        Dzero = Combine3BodySelection(
            "D02K2PiforK0for{}".format(name),
            [inputs["InputPionsForDzero"], inputs["InputKaonsForDzero"]],
            DecayDescriptor="[D0 -> pi+ pi- K-]cc",
            DaughtersCuts={
                "": "ALL",
                "K+": "{} & {}".format(DzeroDaughterCuts, DzeroDaughterKaonCuts),
                "K-": "{} & {}".format(DzeroDaughterCuts, DzeroDaughterKaonCuts),
                "pi+": "{} & {}".format(DzeroDaughterCuts, DzeroDaughterPionCuts),
                "pi-": "{} & {}".format(DzeroDaughterCuts, DzeroDaughterPionCuts),
            },
            Combination12Cut=Template(
                "(AM12 > ${DzeroPionPairMassMin}) & (AM12 < ${DzeroPionPairMassMax})"
            ).substitute(config),
            CombinationCut=Template(
                "(AM > ${DzeroMassMin}) & (AM < ${DzeroMassMax})"
                " & (APT > ${DzeroPTMin}) & (ADOCACHI2CUT(${DzeroADOCACHI2CUT}, ''))"
            ).substitute(config),
            MotherCut=Template(
                "(VFASPF(VCHI2/VDOF) < ${DzeroVCHI2DOFMax})"
                " & (BPVVD > ${DzeroBPVVDMin})"
                " & (BPVVDCHI2 > ${DzeroBPVVDCHI2Min})"
                " & (BPVDIRA > ${DzeroBPVDIRAMin})"
                " & (BPVIPCHI2() < ${DzeroBPVIPCHI2Max})"
            ).substitute(config),
        )

        # -- define how to create D*(2010)
        def makeDstar(_name, descriptor):
            # check descriptor
            allowedDescriptors = ["[D*(2010)+ -> D0 pi+]cc", "[D*(2010)- -> D0 pi-]cc"]
            if descriptor not in allowedDescriptors:
                raise ValueError(
                    "descriptor '{}' does not match any of {}".format(
                        descriptor, allowedDescriptors
                    )
                )
            # define added pion cuts
            DstarPionCuts = Template(
                "(PT > ${DstarPionPTMin})"
                " & (TRCHI2DOF < ${DstarPionTRCHI2DOFMax})"
                " & (MIPDV(PRIMARY) < ${DstarPionMIPDVMax})"
                " & (MIPCHI2DV(PRIMARY) < ${DstarPionMIPCHI2DVMax})"
                " & (PIDK < ${DstarPionPIDKMax})"
            ).substitute(config)
            # create D*(2010)
            return CombineSelection(
                "Dst{}wD0K2PiD0for{}".format(_name, name),
                [inputs["InputPionsForDstar"], Dzero],
                DecayDescriptor=descriptor,
                DaughtersCuts={
                    "": "ALL",
                    "D0": "ALL",
                    "D~0": "ALL",
                    "pi+": DstarPionCuts,
                    "pi-": DstarPionCuts,
                },
                CombinationCut=Template(
                    "(AM - AM1 - 139.57 * MeV < ${DstarMassDiffDzeroPlusPionMax})"
                    " & (APT > ${DstarPTMin})"
                ).substitute(config),
                MotherCut=Template(
                    "(VFASPF(VCHI2/VDOF) < ${DstarVCHI2DOFMax})"
                    " & (BPVVDCHI2 < ${DstarBPVVDCHI2Max})"
                    " & (BPVIPCHI2() < ${DstarBPVIPCHI2Max})"
                ).substitute(config),
            )

        # -- declare lines
        self.RSLine = StrippingLine(
            "DstRSwD02K2PiD0for{}Line".format(name),
            prescale=config["prescaleRS"],
            algos=[
                Hlt2TOSSelection(
                    L0TOSSelection(
                        makeDstar("RS", "[D*(2010)+ -> D0 pi+]cc"),
                        config["DstarL0TOSSelection"],
                    ),
                    config["DstarHLT2TOSSelection"],
                )
            ],
        )
        self.WSLine = StrippingLine(
            "DstWSwD02K2PiD0for{}Line".format(name),
            prescale=config["prescaleWS"],
            algos=[
                Hlt2TOSSelection(
                    L0TOSSelection(
                        makeDstar("WS", "[D*(2010)- -> D0 pi-]cc"),
                        config["DstarL0TOSSelection"],
                    ),
                    config["DstarHLT2TOSSelection"],
                )
            ],
        )

        # -- register lines
        self.registerLine(self.RSLine)
        self.registerLine(self.WSLine)
