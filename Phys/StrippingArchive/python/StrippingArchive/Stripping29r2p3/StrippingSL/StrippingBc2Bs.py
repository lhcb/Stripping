###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = ['Adam Davis']
__date__ = '12/03/2021'
__version__ = '$Revision: 1.0 $'
from Gaudi.Configuration import *
from StrippingUtils.Utils import LineBuilder
import logging

"""
Selections for Bc -> Bs X
"""


default_config = {
  'NAME'        : 'Bc2Bs',
  'WGs'         : ['Semileptonic'],
  'BUILDERTYPE' : 'Bc2BsBuilder',
  'CONFIG'      :  {
      "GEC_nLongTrk"        : 250.  , #adimensional
      "TRGHOSTPROB"         : 0.5    ,#adimensional
      #Kaons
      "KaonTRCHI2"          : 4.     ,#adimensional
      "KaonP"               : 3000.  ,#MeV
      "KaonPT"              : 400.   ,#MeV
      "KaonPIDK"            : 5.     ,#adimensional 
      "KaonPIDmu"           : 5.     ,#adimensional
      "KaonPIDp"            : 5.     ,#adimensional
      "KaonMINIP"           : 0.05   ,#mm

      "KaonP_phi"           : 3000.  ,#MeV
      "KaonPT_phi"          : 200.   ,#MeV
      "KaonPIDK_phi"        : 0.     ,#adimensional 
      "KaonPIDmu_phi"       : -2.    ,#adimensional
      "KaonPIDp_phi"        : -2.    ,#adimensional
      "KaonMINIP_phi"       : 0.025  ,#mm

      #k from bc

      "K_from_bc_TRCHI2"      : 4.     ,#adminensional
      "K_from_bc_P"           : 1000.  ,#MeV
      "K_from_bc_PT"          : 150.   ,#MeV
      "K_from_bc_PIDK"        : 0.     ,#adimensional
      "K_from_bc_MINIP"       : 0.05   ,#mm

      #k from bs
      "K_from_bs_TRCHI2"      : 4.     ,#adminensional
      "K_from_bs_P"           : 1000.  ,#MeV
      "K_from_bs_PT"          : 150.   ,#MeV
      "K_from_bs_PIDK"        : 0.     ,#adimensional
      "K_from_bs_MINIP"       : 0.05   ,#mm
      #Pions 
      "PionTRCHI2"          : 4.     ,#adimensional
      "PionP"               : 3000.  ,#MeV
      "PionPT"              : 400.   ,#MeV
      "PionPIDK"            : -2.    ,#adimensional 
      "PionMINIP"           : 0.05   ,#mm

      "BachPionTRCHI2"      : 4.     ,#adminensional
      "BachPionP"           : 1000.  ,#MeV
      "BachPionPT"          : 350.   ,#MeV
      "BachPionPIDK"        : 2      ,#adimensional
      "BachPionMINIP"       : 0.02   ,#mm

      "PionFromBsTRCHI2"    : 4.     ,#adminensional
      "PionFromBsP"         : 1000.  ,#MeV
      "PionFromBsPT"        : 500.   ,#MeV
      "PionFromBsPIDK"      : 2      ,#adimensional
      "PionFromBsMINIP"     : 0.05   ,#mm
      #muons
      "MuonGHOSTPROB"       : 0.35   ,#adimensional
      "MuonTRCHI2"          : 4.     ,#adimensional
      "MuonP"               : 3000.  ,#MeV
      "MuonPT"              : 750.  ,#MeV
      "MuonPIDK"            : -2.    ,#adimensional
      "MuonPIDmu"           : 0.     ,#adimensional      
      "MuonMINIP"           : 0.025  ,#mm
      "BachMuonTRCHI2"      : 4.     ,#adminensional
      "BachMuonP"           : 1000.  ,#MeV
      "BachMuonPT"          : 150.   ,#MeV
      "BachMuonPIDmu"       : 0      ,#adimensional
      "BachMuonMINIP"       : 0.02   ,#mm
      #electron
      "ElectronTRCHI2"      : 10     ,#adimensional
      "ElectronP"           : 3000.  ,#MeV
      "ElectronPT"          : 1000.  ,#MeV
      "ElectronGHOSTPROB"   : 0.6    ,#adimensional
      "ElectronPIDe"        : -0.2   ,#adimensional
      "ElectronMINIP"       : 0.05   ,#mm
      #phi 
      "PhiVCHI2DOF"         : 6     ,#adimensional
      "PhiPT"               : 600.  ,#MeV
      "PhiMINIP"            : 0.05  ,#adimensional
      "PhiDIRA"             : 0.9   ,#adimensional
      "PhiMassWindow"       : 200.  ,#MeV
      "Phi_CHI2DOF"         : 35.   ,#MeV

      #jpsi
      "JpsiMassWindow"      : 200.  ,#MeV
      "JpsiPT"              : 1000. ,#MeV
      "Jpsi_CHI2DOF"        : 15.   ,#adimensional
      "Jpsi_DIRA"           : 0.9   ,#adimensional  
      "Jpsi_BPVVDZcut"      : 1.5     ,#mm
      #ds
      "DsMassWindow"       : 400.  ,#MeV
      "Ds_CHI2DOF"         : 10.   ,#adimensional
      "Ds_FDCHI2HIGH"      : 100.  ,#adimensional
      "Ds_DIRA"            : 0.99 ,#adimensional
      "Ds_BPVVDZcut"       : 0.0   ,#mm
      #bs
      "BsMassWindowHigh"   : 6000. ,#MeV
      "BsMassWindowLow"    : 5000. ,#MeV,
      "BsFDCHI2HIGH"       : 35.   ,#adimensional
      "BsDIRA"             : 0.9   ,#adimensional
      "BsPVVDZcut"         : 2.    ,#mm
      "Bs_JpsiPhi_CHI2DOF" : 35.   ,#adimensional
      "Bs_DsPi_CHI2DOF"    : 25.   ,#adimensional
      #bc
      "BcPVVDZcut"         : 0.080  ,#mm
      "Bc_CHI2DOF"         : 20.   ,#adimensional
      "BcPT"               : 3000. ,#MeV
      "BcP"                : 30000.,#MeV
      "BcDIRA_Tight"       : 0.99  ,#adimensional
      "BcDIRA_Loose"       : 0.9   ,#adimensional
   
      "RelatedInfoTools": [
        {
        "Location": "ConeVarInfo", 
        "Type": "RelInfoConeVariables",
        },
        {
        "Location": "ConeIsoInfo0p4",
        "Type": "RelInfoConeIsolation",  
        "ConeSize": 0.4,
        "FillCharged": False,
        "FillNeutral": True,
        },
        {
        "Location": "ConeIsoInfo0p5",
        "Type": "RelInfoConeIsolation",     
        "ConeSize": 0.5,
        "FillCharged": False,
        "FillNeutral": True,
        },
        {
        "Location": "ConeIsoInfo0p6",
        "Type": "RelInfoConeIsolation",     
        "ConeSize": 0.6,
        "FillCharged": False,
        "FillNeutral": True,
        },
    
      ],
  },
  'STREAMS'     : ['Leptonic']    
}



class Bc2BsBuilder(LineBuilder):
    """
    definition of the Bc->BsX stripping module
    """
    __configuration_keys__ = default_config['CONFIG'].keys()
    def __init__(self,name,config):
        LineBuilder.__init__(self,name,config)
        self.config = config
        from PhysSelPython.Wrappers import Selection, DataOnDemand
        ## GECs for # long tracks
        self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}
        ## Pions
        self._pionSel=None
        self._pionFilter()
        self._pionFromBsSel = None
        self._pionFromBsFilter()
        self._bachPionSel = None
        self._bachPionFilter()
        ## Kaons
        self._kaonSel=None
        self._kaonFilter()
        self._kaonSelForPhi = None
        self._kaonFilter_forPhi()

        self._kaonFromBcSel = None
        self._kaonFromBcFilter()

        self._kaonFromBsSel = None
        self._kaonFromBsFilter()
        #muons
        self._muonSel=None
        self._muonFilter()
        self._bachMuSel = None
        self._bachMuFilter()

        #electrons
        self._electronSel = None
        self._electronFilter()
        #ds
        self._Ds2KKpiSel = None
        self._Ds2KKpiMaker()
        #jpsi
        self._Jpsi2mumuSel = None
        self._Jpsi2mumuMaker()
        #phi
        self._Phi2KKSel = None
        self._Phi2KKMaker()
        
        #jpsi/phi
        self._Bs2JpsiPhiSel = None
        self._Bs2JpsiPhiMaker()
        #dspi
        self._Bs2DsPiSel = None
        self._Bs2DsPiMaker()
        #dsk
        self._Bs2DsKSel = None
        self._Bs2DsKMaker()
        #register lines
        for line in self.Bc2BsMu():
            self.registerLine(line)
        for line in self.Bc2BsE():
            self.registerLine(line)
        for line in self.Bc2BsPi():
            self.registerLine(line)
        for line in self.Bc2BsK():
            self.registerLine(line)


    #selections for the muons
    def _NominalMuSelection( self ):
        return "(TRCHI2DOF < %(MuonTRCHI2)s )  &"\
               "(P> %(MuonP)s *MeV)  &"\
               "(PT> %(MuonPT)s* MeV)  &"\
               "(TRGHOSTPROB < %(MuonGHOSTPROB)s) &"\
               "(PIDmu > %(MuonPIDmu)s )  &"\
               "(MIPDV(PRIMARY)> %(MuonMINIP)s )"

    def _BachMuSelection( self ):
      return "(TRCHI2DOF < %(BachMuonTRCHI2)s )&"\
             "(P> %(BachMuonP)s *MeV)&"\
             "(PT> %(BachMuonPT)s* MeV)&"\
             "(TRGHOSTPROB < %(MuonGHOSTPROB)s)&"\
             "(PIDmu > %(BachMuonPIDmu)s )&"\
             "(MIPDV(PRIMARY)> %(BachMuonMINIP)s)"

    def _NominalPiSelection( self ):
        return "(TRCHI2DOF < %(PionTRCHI2)s )&"\
               "(P> %(PionP)s *MeV)&"\
               "(PT> %(PionPT)s *MeV)&"\
               "(TRGHOSTPROB < %(TRGHOSTPROB)s)&"\
               "(PIDK < %(PionPIDK)s )&"\
               "(MIPDV(PRIMARY)> %(PionMINIP)s ) "

    def _NominalPiFromBsSelection( self ):
        return "(TRCHI2DOF < %(PionFromBsTRCHI2)s )&"\
               "(P> %(PionFromBsP)s *MeV)& "\
               "(PT> %(PionFromBsPT)s *MeV)& "\
               "(TRGHOSTPROB < %(TRGHOSTPROB)s)& "\
               "(PIDK < %(PionFromBsPIDK)s )& "\
               "(MIPDV(PRIMARY)> %(PionFromBsMINIP)s )"

    def _NominalKSelection( self ):
        return " (TRCHI2DOF < %(KaonTRCHI2)s )& "\
               " (P> %(KaonP)s *MeV)& "\
               " (PT> %(KaonPT)s *MeV)&"\
               " (TRGHOSTPROB < %(TRGHOSTPROB)s)&"\
               " (PIDK > %(KaonPIDK)s )&"\
               " (MIPDV(PRIMARY)> %(KaonMINIP)s )"

    def _NominalKSelectionForPhi( self ):
        return "(TRCHI2DOF < %(KaonTRCHI2)s )& "\
               "(P> %(KaonP_phi)s *MeV)& "\
               "(PT> %(KaonPT_phi)s *MeV)&"\
               "(TRGHOSTPROB < %(TRGHOSTPROB)s)&"\
               "(PIDK > %(KaonPIDK_phi)s )&"\
               "(MIPDV(PRIMARY)> %(KaonMINIP_phi)s )"

    def _NominalKFromBsSelection( self ):
        return "(TRCHI2DOF < %(K_from_bs_TRCHI2)s )&"\
               "(P> %(K_from_bs_P)s *MeV)& "\
               "(PT> %(K_from_bs_PT)s *MeV)& "\
               "(TRGHOSTPROB < %(TRGHOSTPROB)s)& "\
               "(PIDK > %(K_from_bs_PIDK)s )& "\
               "(MIPDV(PRIMARY)> %(K_from_bs_MINIP)s )"

    def _NominalElectronSelection( self ):
        return "(TRCHI2DOF < %(ElectronTRCHI2)s ) &"\
               "( P> %(ElectronP)s * MeV) &"\
               "( PT > %(ElectronPT)s * MeV) &"\
               "( TRGHOSTPROB < %(ElectronGHOSTPROB)s ) &"\
               "( PIDe > %(ElectronPIDe)s ) &"\
               "(MIPDV(PRIMARY) > %(ElectronMINIP)s ) "
    
    def _NominalBachPiSelection( self ):
        return "(TRCHI2DOF < %(BachPionTRCHI2)s ) & "\
               "(P> %(BachPionP)s *MeV) & "\
               "(PT> %(BachPionPT)s *MeV) &"\
               "(TRGHOSTPROB < %(TRGHOSTPROB)s) &"\
               "(PIDK < %(BachPionPIDK)s ) &"\
               "(MIPDV(PRIMARY)> %(BachPionMINIP)s ) "
    
    def _NominalKfromBcSelection( self ):
      return "(TRCHI2DOF < %(K_from_bc_TRCHI2)s ) & "\
             "(P> %(K_from_bc_P)s *MeV) & "\
             "(PT> %(K_from_bc_PT)s *MeV) &"\
             "(TRGHOSTPROB < %(TRGHOSTPROB)s) &"\
             "(PIDK > %(K_from_bc_PIDK)s ) &"\
             "(MIPDV(PRIMARY)> %(K_from_bc_MINIP)s ) "
    
    def _kaonFromBcFilter( self ):
      if self._kaonFromBcSel  is not None:
        return self._kaonFromBcSel

      from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
      from PhysSelPython.Wrappers import Selection
      from StandardParticles import StdAllLooseKaons
      _k_from_bc =  FilterDesktop( Code = self._NominalKfromBcSelection() % self._config )
      _k_from_bc_sel = Selection("K_from_bc_for"+self._name,
                                Algorithm = _k_from_bc,
                                RequiredSelections = [StdAllLooseKaons]
                                )
      self._kaonFromBcSel = _k_from_bc_sel
      return _k_from_bc_sel

    def _electronFilter( self ):
        if self._electronSel is not None:
            return self._electronSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseElectrons

        _el = FilterDesktop( Code = self._NominalElectronSelection() % self._config )
        _electronSel=Selection("Electron_for"+self._name,
                               Algorithm=_el,
                               RequiredSelections = [StdAllLooseElectrons]
                               )
        
        self._electronSel=_electronSel
        
        return _electronSel
    
    def _muonFilter( self ):
        if self._muonSel is not None:
            return self._muonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseMuons
        
        _mu = FilterDesktop( Code = self._NominalMuSelection()%self._config )
        _muSel=Selection("Mu_for"+self._name,
                         Algorithm=_mu,
                         RequiredSelections = [StdAllLooseMuons]
                         )
        
        self._muonSel=_muSel

        return _muSel

    def _pionFilter( self ):
        if self._pionSel is not None:
            return self._pionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions
        _pia = FilterDesktop( Code = self._NominalPiSelection() % self._config )
        _piaSel=Selection("Pi_for"+self._name,
                          Algorithm=_pia,
                          RequiredSelections = [StdAllLoosePions]
                          )
        self._pionSel=_piaSel
        return _piaSel

    def _pionFromBsFilter( self ):
        if self._pionFromBsSel is not None:
            return self._pionFromBsSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions
        _pib = FilterDesktop( Code = self._NominalPiFromBsSelection() % self._config )
        _pibSel=Selection("PiFromBs_for"+self._name,
                          Algorithm=_pib,
                          RequiredSelections = [StdAllLoosePions]
                          )
        self._pionFromBsSel=_pibSel
        return _pibSel

    def _kaonFilter( self ):
        if self._kaonSel is not None:
            return self._kaonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllNoPIDsKaons
        _ka = FilterDesktop( Code = self._NominalKSelection() % self._config )
        _kaSel=Selection("K_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdAllNoPIDsKaons]
                         )
        self._kaonSel=_kaSel
        return _kaSel
    
    def _kaonFilter_forPhi( self ):
        if self._kaonSelForPhi is not None:
            return self._kaonSelForPhi
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseKaons

        _kap = FilterDesktop(Code = self._NominalKSelectionForPhi() % self._config )
        _kapSel=Selection("K_for_phi_for"+self._name,
                         Algorithm=_kap,
                         RequiredSelections = [StdAllLooseKaons]
                         )
        self._kaonSelForPhi=_kapSel
        return _kapSel
    def _kaonFromBsFilter( self ):
        if self._kaonFromBsSel is not None:
            return self._kaonFromBsSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseKaons
        _kib = FilterDesktop( Code = self._NominalKFromBsSelection() % self._config )
        _kibSel=Selection("KFromBs_for"+self._name,
                          Algorithm=_kib,
                          RequiredSelections = [StdAllLooseKaons]
                          )
        self._kaonFromBsSel=_kibSel
        return _kibSel

    def _bachPionFilter( self ):
        if self._bachPionSel is not None:
            return self._bachPionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions
        _bachpi = FilterDesktop( Code = self._NominalBachPiSelection() % self._config )
        _bachpiSel=Selection("pis_for"+self._name,
                         Algorithm=_bachpi,
                         RequiredSelections = [StdAllLoosePions]
                         )
        self._bachPionSel=_bachpiSel
        return _bachpiSel

    def _bachMuFilter( self ):
        if self._bachMuSel is not None:
            return self._bachMuSel
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseMuons
        _bachmu = FilterDesktop( Code = self._BachMuSelection() % self._config )
        _bachmusel = Selection("bach_mu_for"+self._name,
                               Algorithm = _bachmu,
                               RequiredSelections = [StdAllLooseMuons]
                               )
        self._bachMuSel = _bachmusel
        return _bachmusel
    
    def _Bc2BsXMothCuts( self, points_to_PV=False ):
      the_string = "( VFASPF(VCHI2/VDOF)< %(Bc_CHI2DOF)s )&"\
             "( PT > %(BcPT)s * MeV )&"\
             "( P  > %(BcP)s * MeV )&"\
             "(BPVVDZ > %(BcPVVDZcut)s)"
      if True==points_to_PV:
          the_string += " & ( BPVDIRA > %(BcDIRA_Tight)s )"
      else:
          the_string += " & ( BPVDIRA > %(BcDIRA_Loose)s )"
      return the_string

    #### composites
    def _Ds2KKpiMaker( self ):
        if self._Ds2KKpiSel is not None:
            return self._Ds2KKpiSel
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions, StdAllLooseKaons

        _Ds2KKpi = CombineParticles(
            DecayDescriptors = ["[D_s+ -> K+ K- pi+]cc"],
            DaughtersCuts = {"K+":self._NominalKSelection() % self._config,
                             "pi+":self._NominalPiSelection() % self._config },
            CombinationCut =  "( ADAMASS('D_s+') < %(DsMassWindow)s * MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(Ds_CHI2DOF)s )&"\
                        "(BPVVDCHI2 >%(Ds_FDCHI2HIGH)s)&"\
                        "(BPVDIRA > %(Ds_DIRA)s)&"\
                        "(BPVVDZ > %(Ds_BPVVDZcut)s)" % self._config)
        _Ds2KKpiSel = Selection("Ds2KKpi_for"+self._name,
                                Algorithm = _Ds2KKpi,
                                RequiredSelections = [self._pionFilter(), self._kaonFilter()])
        self._Ds2KKpiSel = _Ds2KKpiSel
        return _Ds2KKpiSel

    def _Phi2KKMaker( self ):
        if self._Phi2KKSel is not None:
            return self._Phi2KKSel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions, StdAllLooseKaons

        _Phi2KK = CombineParticles(
            DecayDescriptors = ["phi(1020) -> K+ K-"],
            DaughtersCuts = {"K+":self._NominalKSelection() % self._config,
                             "K-":self._NominalKSelection() % self._config},
            CombinationCut =  "( ADAMASS('phi(1020)') < %(PhiMassWindow)s * MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(Phi_CHI2DOF)s ) &"\
                        "(PT > %(PhiPT)s * MeV) &"\
                        "(BPVDIRA > %(PhiDIRA)s) "%self._config)
        _Phi2KKSel = Selection("Phi2KK_for"+self._name,
                                Algorithm = _Phi2KK,
                                RequiredSelections = [ self._kaonFilter_forPhi()])
        self._Phi2KKSel = _Phi2KKSel
        return _Phi2KKSel

    def _Jpsi2mumuMaker( self ):
        if self._Jpsi2mumuSel is not None:
            return self._Jpsi2mumuSel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseMuons

        _Jpsi2mumu = CombineParticles(
            DecayDescriptors = ["J/psi(1S) -> mu+ mu-"],
            DaughtersCuts = {"mu+":'ALL',#self._NominalMuSelection() % self._config,
                             "mu-":'ALL',#self._NominalMuSelection() % self._config
                             },
            CombinationCut =  "( ADAMASS('J/psi(1S)') < %(JpsiMassWindow)s * MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(Jpsi_CHI2DOF)s )&"\
                        "(PT > %(JpsiPT)s )&"\
                        "(BPVDIRA > %(Jpsi_DIRA)s)&"\
                        "(BPVVDZ > %(Jpsi_BPVVDZcut)s)" % self._config)
        _Jpsi2mumuSel = Selection("Jpsi2mumu_for"+self._name,
                                Algorithm = _Jpsi2mumu,
                                RequiredSelections = [self._muonFilter()])
        self._Jpsi2mumuSel = _Jpsi2mumuSel
        return _Jpsi2mumuSel

    def _Bs2JpsiPhiMaker( self ):
        if self._Bs2JpsiPhiSel is not None:
            return self._Bs2JpsiPhiSel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection


        _Bs2JpsiPhi = CombineParticles(
            DecayDescriptors = ["[ B_s0 -> J/psi(1S)  phi(1020) ]cc"],
            DaughtersCuts = {'J/psi(1S)':"ALL",'phi(1020)':"ALL"},
            CombinationCut =  "( AM < %(BsMassWindowHigh)s * MeV) & ( AM > %(BsMassWindowLow)s )" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(Bs_JpsiPhi_CHI2DOF)s ) &"\
                        "(BPVVDCHI2 >%(BsFDCHI2HIGH)s) &"\
                        "(BPVDIRA > %(BsDIRA)s) &"\
                        "(BPVVDZ > %(BsPVVDZcut)s) " % self._config)
        _Bs2JpsiPhiSel = Selection("Bs2JpsiPhi_for"+self._name,
                                Algorithm = _Bs2JpsiPhi,
                                RequiredSelections = [ self._Jpsi2mumuMaker(), self._Phi2KKMaker() ])
        self._Bs2JpsiPhiSel = _Bs2JpsiPhiSel
        return _Bs2JpsiPhiSel

    def _Bs2DsPiMaker( self ):
        if self._Bs2DsPiSel is not None:
            return self._Bs2DsPiSel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions

        _Bs2DsPi = CombineParticles(
            DecayDescriptors = ["[ B_s0 -> D_s- pi+ ]cc", "[B_s0 -> D_s+ pi+]cc"],
            DaughtersCuts = {'D_s-':"ALL",'pi+':"ALL"},
            CombinationCut =  "( AM < %(BsMassWindowHigh)s * MeV) & ( AM > %(BsMassWindowLow)s )" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(Bs_DsPi_CHI2DOF)s )  &"\
                        "(BPVVDCHI2 >%(BsFDCHI2HIGH)s)  &"\
                        "(BPVDIRA > %(BsDIRA)s)  &"\
                        "(BPVVDZ > %(BsPVVDZcut)s) " % self._config)
        _Bs2DsPiSel = Selection("Bs2DsPi_for"+self._name,
                                Algorithm = _Bs2DsPi,
                                RequiredSelections = [self._Ds2KKpiMaker(), self._pionFromBsFilter() ])
        self._Bs2DsPiSel = _Bs2DsPiSel
        return _Bs2DsPiSel


    def _Bs2DsKMaker( self ):
        if self._Bs2DsKSel is not None:
            return self._Bs2DsKSel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseKaons

        _Bs2DsK = CombineParticles(
            DecayDescriptors = ["[ B_s0 -> D_s- K+ ]cc", "[B_s0 -> D_s+ K+]cc"],
            DaughtersCuts = {'D_s-':"ALL",'pi+':"ALL"},
            CombinationCut =  "( AM < %(BsMassWindowHigh)s * MeV) & ( AM > %(BsMassWindowLow)s )" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(Bs_DsPi_CHI2DOF)s )  &"\
                        "(BPVVDCHI2 >%(BsFDCHI2HIGH)s)  &"\
                        "(BPVDIRA > %(BsDIRA)s)  &"\
                        "(BPVVDZ > %(BsPVVDZcut)s) " % self._config)
        _Bs2DsKSel = Selection("Bs2DsK_for"+self._name,
                                Algorithm = _Bs2DsK,
                                RequiredSelections = [self._Ds2KKpiMaker(), self._kaonFromBsFilter() ])
        self._Bs2DsKSel = _Bs2DsKSel
        return _Bs2DsKSel
    # finally Bc
    def Bc2BsXmaker( self, _name, _descriptors,_mothCuts, _bachPart, _BsSel ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _cparts = CombineParticles(
            DecayDescriptors = _descriptors,
            MotherCut = _mothCuts
            )
        _sel = Selection(_name,
                         Algorithm = _cparts,
                         RequiredSelections = [_bachPart,_BsSel])
        return _sel
        

    def Bc2BsMu(self):
        from StrippingConf.StrippingLine import StrippingLine
        return [ StrippingLine(self._name+"Bc2BsMu_Jpsiphi", prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsMuNuJpsiPhiSel",
                                                   ['[B_c+ -> B_s0 mu+]cc', '[B_c+ -> B_s~0 mu+]cc'],
                                                   self._Bc2BsXMothCuts(points_to_PV=False)%self._config,
                                                   self._bachMuFilter(),
                                                   self._Bs2JpsiPhiMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools']),
                 StrippingLine(self._name+"Bc2BsMu_Dspi",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsMuNuDsPiSel",
                                                        ['[B_c+ -> B_s0 mu+]cc','[B_c+ -> B_s~0 mu+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=False)%self._config,
                                                        self._bachMuFilter(),
                                                        self._Bs2DsPiMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools']),
                 StrippingLine(self._name+"Bc2BsMu_DsK",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsMuNuDsKSel",
                                                        ['[B_c+ -> B_s0 mu+]cc','[B_c+ -> B_s~0 mu+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=False)%self._config,
                                                        self._bachMuFilter(),
                                                        self._Bs2DsKMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools']),
                 ]
    
                                                        
                                                        
    def Bc2BsE(self):
        from StrippingConf.StrippingLine import StrippingLine
        return [ StrippingLine(self._name+"Bc2BsE_Jpsiphi", prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsENuJpsiPhiSel",
                                                   ['[B_c+ -> B_s0 e+]cc', '[B_c+ -> B_s~0 e+]cc'],
                                                   self._Bc2BsXMothCuts(points_to_PV=False)%self._config,
                                                   self._electronFilter(),
                                                   self._Bs2JpsiPhiMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools']),
                 StrippingLine(self._name+"Bc2BsE_Dspi",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsENuDsPiSel",
                                                        ['[B_c+ -> B_s0 e+]cc','[B_c+ -> B_s~0 e+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=False)%self._config,
                                                        self._electronFilter(),
                                                        self._Bs2DsPiMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools']),
                 StrippingLine(self._name+"Bc2BsE_DsK",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsENuDsKSel",
                                                        ['[B_c+ -> B_s0 e+]cc','[B_c+ -> B_s~0 e+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=False)%self._config,
                                                        self._electronFilter(),
                                                        self._Bs2DsKMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools'])
                 ]
    def Bc2BsPi(self):
        from StrippingConf.StrippingLine import StrippingLine
        return [ StrippingLine(self._name+"Bc2BsPi_Jpsiphi", prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsPiJpsiPhiSel",
                                                   ['[B_c+ -> B_s0 pi+]cc', '[B_c+ -> B_s~0 pi+]cc'],
                                                   self._Bc2BsXMothCuts(points_to_PV=True)%self._config,
                                                   self._bachPionFilter(),
                                                   self._Bs2JpsiPhiMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools']),
                 StrippingLine(self._name+"Bc2BsPi_Dspi",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsPiDsPiSel",
                                                        ['[B_c+ -> B_s0 pi+]cc','[B_c+ -> B_s~0 pi+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=True)%self._config,
                                                        self._bachPionFilter(),
                                                        self._Bs2DsPiMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools']),
                 StrippingLine(self._name+"Bc2BsPi_DsK",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsPiDsKSel",
                                                        ['[B_c+ -> B_s0 pi+]cc','[B_c+ -> B_s~0 pi+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=True)%self._config,
                                                        self._bachPionFilter(),
                                                        self._Bs2DsKMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools'])
                 ]
    def Bc2BsK(self):
        from StrippingConf.StrippingLine import StrippingLine
        return [ StrippingLine(self._name+"Bc2BsK_Jpsiphi", prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsKJpsiPhiSel",
                                                   ['[B_c+ -> B_s0 K+]cc', '[B_c+ -> B_s~0 K+]cc'],
                                                   self._Bc2BsXMothCuts(points_to_PV=True)%self._config,
                                                   self._kaonFromBcFilter(),
                                                   self._Bs2JpsiPhiMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools']),
                 StrippingLine(self._name+"Bc2BsK_Dspi",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsKDsPiSel",
                                                        ['[B_c+ -> B_s0 K+]cc','[B_c+ -> B_s~0 K+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=True)%self._config,
                                                        self._kaonFromBcFilter(),
                                                        self._Bs2DsPiMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools']),
                 StrippingLine(self._name+"Bc2BsK_DsK",prescale = 1.0,
                               FILTER = self.GECs,
                               algos = [self.Bc2BsXmaker("Bc2BsKDsKSel",
                                                        ['[B_c+ -> B_s0 K+]cc','[B_c+ -> B_s~0 K+]cc'],
                                                        self._Bc2BsXMothCuts(points_to_PV=True)%self._config,
                                                        self._kaonFromBcFilter(),
                                                        self._Bs2DsKMaker())],
                               RequiredRawEvents=[ "Calo"],
                               RelatedInfoTools = self._config['RelatedInfoTools'])
                 ]
