###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = 'P. Owen, T.Mombacher, V.Lisovskyi, F.Glaser'
__date__ = '12/07/2023'
__version__ = '$Revision: 3.0 $'

__all__ = ('B23MuNuConf', 'default_config')

"""
Stripping selection for B to three leptons and a neutrino.
"""


from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop

from PhysSelPython.Wrappers import Selection, AutomaticData, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


#################
#
#  Define Cuts here
#
#################

default_config = {
    'NAME': 'B23MuNu',
    'WGs': ['Semileptonic'],
    'BUILDERTYPE': 'B23MuNuConf',
    'CONFIG': {
        #  mother cuts
        'FlightChi2':           30.0,
        'FlightChi2_TIGHT':     64.0,
        'DIRA':                 0.99,
        'DIRA_LOOSE':           0.95,
        'BPT':                  2000.0,
        'BPT_LOOSE':            1500.0,
        'VertexCHI2':           4.0,
        # 'VertexCHI2_LOOSE':     6.0,
        'VertexCHI2_1PI':       64.0,
        'LOWERMASS':            0.0,  # MeV
        'LOWERMASS_TIGHT':      400.0,  # MeV
        'UPPERMASS':            7500.0,  # MeV
        'UPPERMASS_TIGHT':      6800.0,  # MeV
        'CORRM_MIN':            2500.0,  # MeV
        'CORRM_MIN_LOOSE':      2000.0,  # MeV
        'CORRM_MAX':            10000.0,  # MeV
        # Track cuts
        'Track_CHI2nDOF':       3.0,
        'Track_GhostProb':      0.35,

        # Muon cuts
        'Muon_MinIPCHI2':       9.0,
        'Muon_MinIPCHI2_Tight': 18.0,
        'Muon_PIDmu':           0.0,
        'Muon_PIDmu_Tight':     3.0,
        'Muon_PIDmuK':          0.0,
        'Muon_PT':              0.0,  # MeV
        'Muon_ProbNNmu_Tight':  0.2,        

        # Electron cuts
        'Electron_PIDe':        2.0,
        'Electron_ProbNNe_Tight': 0.2,
        'Electron_PIDeK':       0.0,
        'Electron_MinIPCHI2':   25.0,
        'Electron_PT':          200.0,  # MeV
        'Electron_PT_TIGHT':    300.0,  # MeV

        'Lepton_PT_VERYTIGHT':  500.0, # MeV

        # dilepton cuts
        'DiElectron_MASS_MIN':  0.0,  # MeV
        'DiElectron_MASS_MAX':  5500.0,  # MeV
        'DiElectron_MASS_MAX_TIGHT':  1100.0, # MeV
        'DiElectron_PT':        0.0,  # MeV
        'DiElectron_PT_TIGHT':  800.0, # MeV
        'DiElectron_Vertex_TIGHT':    4.0,

        # tau cuts
        'Tau21Pion_MinIPCHI2':  36.0,
        'Tau21Pion_PT':         1000,
        'Tau21Pion_PIDK':       2,
        'Tau21Pion_ProbNNpi':   0.2,    
        'Tau_VertexChi2':       4.0,

        # GEC
        'SpdMult':              900,

        'MisIDPrescale':        0.01, 
        'MisIDPrescaleLoose':   0.025, 
        'MisIDPrescaleVeryLoose':  0.05,
        'FakeTauPrescale':      1,
    },
    'STREAMS': ['Semileptonic']
}

defaultName = "B23MuNu"


class B23MuNuConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        self.name = name

        self.TriMuCut = "(BPVCORRM > %(CORRM_MIN)s *MeV) & " \
            "(BPVCORRM < %(CORRM_MAX)s *MeV) & " \
            "(BPVDIRA > %(DIRA)s) & " \
            "(PT > %(BPT)s) & " \
            "(BPVVDCHI2 > %(FlightChi2)s) & " \
            "(VFASPF(VCHI2/VDOF) < %(VertexCHI2)s) & " \
            "(M > %(LOWERMASS)s) & " \
            "(M < %(UPPERMASS)s) " % config

        self.MuMuTauCut = "(BPVCORRM > %(CORRM_MIN_LOOSE)s *MeV) & " \
            "(BPVCORRM < %(CORRM_MAX)s *MeV) & " \
            "(BPVDIRA > %(DIRA)s) & " \
            "(PT > %(BPT)s) & " \
            "(BPVVDCHI2 > %(FlightChi2)s) & " \
            "(VFASPF(VCHI2/VDOF) < %(VertexCHI2)s) & " \
            "(M > %(LOWERMASS)s) & " \
            "(M < %(UPPERMASS_TIGHT)s)" % config

        self.TightMuonIDCut = "& (2 == NINTREE((ABSID==13) & (PIDmu>%(Muon_PIDmu)s)))" % config
        self.TightElectronIDCut = "& (2 == NINTREE((ABSID==11) & (HASRICH) & (HASCALOS) & " \
            "(PROBNNe>%(Electron_ProbNNe_Tight)s) & (PT>%(Electron_PT_TIGHT)s))) & " \
            "(MAXTREE(ABSID==11,PT) > %(Lepton_PT_VERYTIGHT)s *MeV) & (PT> %(DiElectron_PT_TIGHT)s * MeV) & (VFASPF(VCHI2/VDOF) < %(VertexCHI2)s)" % config

        self.MuMuTau1PiCut = "(BPVCORRM > %(CORRM_MIN_LOOSE)s *MeV) & " \
            "(BPVCORRM < %(CORRM_MAX)s *MeV) & " \
            "(BPVDIRA > %(DIRA)s) & " \
            "(PT > %(BPT)s) & " \
            "(BPVVDCHI2 > %(FlightChi2_TIGHT)s) & " \
            "(VFASPF(VCHI2/VDOF) < %(VertexCHI2_1PI)s) & " \
            "(M > %(LOWERMASS_TIGHT)s) & " \
            "(M < %(UPPERMASS_TIGHT)s) " % config

        self.TrackCuts = "(HASTRACK) & (TRCHI2DOF < %(Track_CHI2nDOF)s) & (TRGHP < %(Track_GhostProb)s)" \
                         " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s) " \
                         " & (PT > %(Muon_PT)s)" % config

        self.TrackCutsElectron = "(HASTRACK) & (TRCHI2DOF < %(Track_CHI2nDOF)s) & (TRGHP < %(Track_GhostProb)s)" \
            " & (MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s) " \
            " & (PT > %(Electron_PT)s)" % config

        self.MuonCut = self.TrackCuts + "& (PIDmu> %(Muon_PIDmu)s) & " \
                                        " (PIDmu-PIDK> %(Muon_PIDmuK)s)" % config

        self.ElectronCut = self.TrackCutsElectron + "& (PIDe> %(Electron_PIDe)s) & " \
                                                    " (PIDe-PIDK> %(Electron_PIDeK)s)" % config

        self.Tau21PionCut = self.TrackCuts + "& (PT > %(Tau21Pion_PT)s) & (MIPCHI2DV(PRIMARY) > %(Tau21Pion_MinIPCHI2)s) & " \
            " (PIDK < %(Tau21Pion_PIDK)s) & (~ISMUON) & (PROBNNpi> %(Tau21Pion_ProbNNpi)s)" % config

        self.Tau23PionCut = "(VFASPF(VCHI2) < 6) & (BPVVDCHI2>12)" % config

        self.LooseJpsiCut = "(2 == NINTREE(" + self.TrackCuts + \
            ")) & (0 < NINTREE(" + self.MuonCut + "))"
        self.LooseJpsieeCut = "(2 == NINTREE(" + self.TrackCutsElectron + \
            ")) & (0 < NINTREE(" + self.ElectronCut + " & (ISLONG)))"
        self.JpsieeCut = "(2 == NINTREE(" + self.ElectronCut + \
            ")) & (0 < NINTREE(" + self.ElectronCut + " & (ISLONG)))"

        self.Muons = self.__Muons__(config)
        self.Electrons = self.__Electrons__(config)
        self.FakeMuons = self.__FakeMuons__(config)
        self.FakeElectrons = self.__FakeElectrons__(config)
        self.Tau23pi = self.__Tau23pi__(config)
        self.Tau23piFake = self.__Tau23piFake__(config)
        self.Jpsi = self.__Jpsi__(config)
        self.FakeJpsi = self.__FakeJpsi__(config)
        self.Jpsiee = self.__Jpsiee__(config)
        self.JpsieeOS = self.__JpsieeOS__(config)
        self.JpsieeSS = self.__JpsieeSS__(config)
        self.FakeJpsieeOS = self.__FakeJpsieeOS__(config)
        self.FakeJpsieeSS = self.__FakeJpsieeSS__(config)
        self.Trimu = self.__Trimu__(config)
        self.Trie = self.__Trie__(config)
        self.MuMue = self.__MuMue__(config)
        self.Muee = self.__Muee__(config)
        self.FakeTrimu = self.__FakeTrimu__(config)
        self.FakeTrie = self.__FakeTrie__(config)
        self.MuMuEFake = self.__MuMuEFake__(config)
        self.MuMuFakeE = self.__MuMuFakeE__(config)
        self.MuFakeEE = self.__MuFakeEE__(config)
        self.MuEEFake = self.__MuEEFake__(config)
        self.MuMuTau = self.__MuMuTau__(config)
        self.MuMuTauFake = self.__MuMuTauFake__(config)
        self.MuMuFakeTau = self.__MuMuFakeTau__(config)
        self.EETau = self.__EETau__(config)
        self.EETauFake = self.__EETauFake__(config)
        self.EEFakeTau = self.__EEFakeTau__(config)
        self.Tau21Pi = self.__Tau21Pi__(config)
        self.MuMuTau1Pi = self.__MuMuTau1Pi__(config)
        self.MuMuFakeTau1Pi = self.__MuMuFakeTau1Pi__(config)

        RelInfoTools = [
            {"Type": "RelInfoMuonIDPlus",
             "Variables": ["MU_BDT"],
             "DaughterLocations": {
                 "[B+ -> ^l+ l+ [l-]CC ]CC": "Muon1BDT",
                 "[B+ -> l+ ^l+ [l-]CC ]CC": "Muon2BDT",
                 "[B+ -> l+ l+ ^[l-]CC ]CC": "Muon3BDT",
             }
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_first',
             'Particles': [0, 1]
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_second',
             'Particles': [1, 2]
             }
        ]

        self.TriMu_line = StrippingLine(
            self.name+"_TriMuLine",
            prescale=1,
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.Trimu],
            RelatedInfoTools=RelInfoTools
        )

        self.Trie_line = StrippingLine(
            self.name+"_TrieLine",
            prescale=1,
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.Trie],
            RelatedInfoTools=RelInfoTools
        )

        # specify leptons to identify the muon candidate
        RelInfoTools = [
            {"Type": "RelInfoMuonIDPlus",
             "Variables": ["MU_BDT"],
             "DaughterLocations": {
                 "[B+ -> [J/psi(1S) -> ^mu+ [mu-]CC ]CC e+]CC": "Muon1BDT",
                 "[B+ -> [J/psi(1S) -> mu+ ^[mu-]CC ]CC e+]CC": "Muon2BDT",
                 "[B+ -> [J/psi(1S) -> mu+ [mu-]CC ]CC ^e+]CC": "Muon3BDT"
             }
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_first',
             'Particles': [1, 2]
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_second',
             'Particles': [2, 3]
             }
        ]

        self.MuMue_line = StrippingLine(
            self.name+"_MuMueLine",
            prescale=1,
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.MuMue],
            RelatedInfoTools=RelInfoTools
        )

        # specify leptons to identify the muon candidate
        RelInfoTools = [
            {"Type": "RelInfoMuonIDPlus",
             "Variables": ["MU_BDT"],
             "DaughterLocations": {
                 "[B+ -> [J/psi(1S) -> ^e+ [e-]CC ]CC mu+]CC": "Muon1BDT",
                 "[B+ -> [J/psi(1S) -> e+ ^[e-]CC ]CC mu+]CC": "Muon2BDT",
                 "[B+ -> [J/psi(1S) -> e+ [e-]CC ]CC ^mu+]CC": "Muon3BDT"
             }
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_first',
             'Particles': [1, 2]
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_second',
             'Particles': [2, 3]
             }
        ]

        self.Muee_line = StrippingLine(
            self.name+"_MueeLine",
            prescale=1,
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.Muee],
            RelatedInfoTools=RelInfoTools
        )

        RelInfoTools_fake = [
            {"Type": "RelInfoMuonIDPlus",
             "Variables": ["MU_BDT"],
             "DaughterLocations": {
                 "[B+ -> [J/psi(1S) -> ^l+ [l-]CC ]CC l+]CC": "Muon1BDT",
                 "[B+ -> [J/psi(1S) -> l+ ^[l-]CC ]CC l+]CC": "Muon2BDT",
                 "[B+ -> [J/psi(1S) -> l+ [l-]CC ]CC ^l+]CC": "Muon3BDT",
             }
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_first',
             'Particles': [1, 2]
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_second',
             'Particles': [2, 3]
             }
        ]

        self.FakeTriMu_line = StrippingLine(
            self.name+"_TriFakeMuLine",
            prescale=config['MisIDPrescale'],
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.FakeTrimu],
            RelatedInfoTools=RelInfoTools_fake
        )

        self.FakeTrie_line = StrippingLine(
            self.name+"_TrieFakeLine",
            prescale=config['MisIDPrescaleLoose'],
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.FakeTrie],
            RelatedInfoTools=RelInfoTools_fake
        )

        # specify leptons to identify the muon candidate
        RelInfoTools_fake = [
            {"Type": "RelInfoMuonIDPlus",
             "Variables": ["MU_BDT"],
             "DaughterLocations": {
                 "[B+ -> [J/psi(1S) -> ^mu+ [mu-]CC ]CC e+]CC": "Muon1BDT",
                 "[B+ -> [J/psi(1S) -> mu+ ^[mu-]CC ]CC e+]CC": "Muon2BDT",
                 "[B+ -> [J/psi(1S) -> mu+ [mu-]CC ]CC ^e+]CC": "Muon3BDT"
             }
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_first',
             'Particles': [1, 2]
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_second',
             'Particles': [2, 3]
             }
        ]

        self.MuMuEFake_line = StrippingLine(
            self.name+"_MuMuEFakeLine",
            prescale=config['MisIDPrescaleVeryLoose'],
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.MuMuEFake],
            RelatedInfoTools=RelInfoTools_fake
        )

        self.MuMuFakeE_line = StrippingLine(
            self.name+"_MuMuFakeELine",
            prescale=config['MisIDPrescale'],
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.MuMuFakeE],
            RelatedInfoTools=RelInfoTools_fake
        )

        # specify leptons to identify the muon candidate
        RelInfoTools_fake = [
            {"Type": "RelInfoMuonIDPlus",
             "Variables": ["MU_BDT"],
             "DaughterLocations": {
                 "[B+ -> [J/psi(1S) -> ^e+ [e-]CC ]CC mu+]CC": "Muon1BDT",
                 "[B+ -> [J/psi(1S) -> e+ ^[e-]CC ]CC mu+]CC": "Muon2BDT",
                 "[B+ -> [J/psi(1S) -> e+ [e-]CC ]CC ^mu+]CC": "Muon3BDT"
             }
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_first',
             'Particles': [1, 2]
             },
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_second',
             'Particles': [2, 3]
             }
        ]

        self.MuFakeEE_line = StrippingLine(
            self.name+"_MuFakeEELine",
            prescale=config['MisIDPrescaleVeryLoose'],
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.MuFakeEE],
            RelatedInfoTools=RelInfoTools_fake
        )

        self.MuEEFake_line = StrippingLine(
            self.name+"_MuEEFakeLine",
            prescale=config['MisIDPrescaleVeryLoose'],
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.MuEEFake],
            RelatedInfoTools=RelInfoTools_fake
        )

        # specify leptons to identify the muon candidate
        RelInfoTools_tau = [
            {'Type': 'RelInfoTrackIsolationBDT2',
             'Location': 'TrackIsolationBDT2_dimuon',
             'Particles': [1, 2]
             }
        ]

        self.MuMuTau_line = StrippingLine(
            self.name+"_MuMuTauLine",
            prescale=1,
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.MuMuTau],
            RelatedInfoTools=RelInfoTools_tau
        )

        self.MuMuTauFake_line = StrippingLine(
            self.name+"_MuMuTauFakeLine",
            prescale=config['FakeTauPrescale'],
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.MuMuTauFake],
            RelatedInfoTools=RelInfoTools_tau
        )

        self.MuMuFakeTau_line = StrippingLine(
            self.name+"_MuMuFakeTauLine",
            prescale=config['MisIDPrescale'],
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.MuMuFakeTau],
            RelatedInfoTools=RelInfoTools_tau
        )

        self.EETau_line = StrippingLine(
            self.name+"_EETauLine",
            prescale=1,
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.EETau],
            RelatedInfoTools=RelInfoTools_tau
        )

        self.EETauFake_line = StrippingLine(
            self.name+"_EETauFakeLine",
            prescale=config['FakeTauPrescale'],
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.EETauFake],
            RelatedInfoTools=RelInfoTools_tau
        )

        self.EEFakeTau_line = StrippingLine(
            self.name+"_EEFakeTauLine",
            prescale=config['MisIDPrescaleLoose'],
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.EEFakeTau],
            RelatedInfoTools=RelInfoTools_tau
        )

        self.MuMuTau1Pi_line = StrippingLine(
            self.name+"_MuMuTau1PiLine",
            prescale=1,
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.MuMuTau1Pi],
            RelatedInfoTools=RelInfoTools_tau
        )

        self.MuMuFakeTau1Pi_line = StrippingLine(
            self.name+"_MuMuFakeTau1PiLine",
            prescale=config['MisIDPrescale'],
            FILTER={
                'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                'Preambulo': [
                    "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
                ]
            },
            algos=[self.MuMuFakeTau1Pi],
            RelatedInfoTools=RelInfoTools_tau
        )

        self.registerLine(self.TriMu_line)
        self.registerLine(self.Trie_line)
        self.registerLine(self.Muee_line)
        self.registerLine(self.MuMue_line)
        self.registerLine(self.FakeTriMu_line)
        self.registerLine(self.FakeTrie_line)
        self.registerLine(self.MuMuEFake_line)
        self.registerLine(self.MuMuFakeE_line)
        self.registerLine(self.MuFakeEE_line)
        self.registerLine(self.MuEEFake_line)
        self.registerLine(self.MuMuTau_line)
        self.registerLine(self.MuMuTauFake_line)
        self.registerLine(self.MuMuFakeTau_line)
        self.registerLine(self.EETau_line)
        self.registerLine(self.EETauFake_line)
        self.registerLine(self.EEFakeTau_line)
        self.registerLine(self.MuMuTau1Pi_line)
        self.registerLine(self.MuMuFakeTau1Pi_line)

    def __Muons__(self, conf):
        """
        Filter muons from StdAllLooseMuons
        """
        from StandardParticles import StdAllLooseMuons
        _muons = StdAllLooseMuons
        _filter = FilterDesktop(Code=self.MuonCut)
        _sel = Selection("Selection_"+self.name+"_Muons",
                         RequiredSelections=[_muons],
                         Algorithm=_filter)
        return _sel

    def __Electrons__(self, conf):
        """
        Filter Electrons from StdAllLooseElectrons
        """
        from StandardParticles import StdAllLooseElectrons
        _electrons = StdAllLooseElectrons
        _filter = FilterDesktop(Code=self.ElectronCut)
        _sel = Selection("Selection_"+self.name+"_Electrons",
                         RequiredSelections=[_electrons],
                         Algorithm=_filter)
        return _sel

    def __FakeMuons__(self, conf):
        """
        Filter muons from StdAllNoPIDsMuons
        """
        from StandardParticles import StdAllNoPIDsMuons
        _fakemuons = StdAllNoPIDsMuons
        _filter = FilterDesktop(Code=self.TrackCuts)
        _sel = Selection("Selection_"+self.name+"_FakeMuons",
                         RequiredSelections=[_fakemuons],
                         Algorithm=_filter)
        return _sel

    def __FakeElectrons__(self, conf):
        """
        Filter electrons from StdAllNoPIDsElectrons
        """
        from StandardParticles import StdAllNoPIDsElectrons
        _fakeelectrons = StdAllNoPIDsElectrons
        _filter = FilterDesktop(Code=self.TrackCutsElectron)
        _sel = Selection("Selection_"+self.name+"_FakeElectrons",
                         RequiredSelections=[_fakeelectrons],
                         Algorithm=_filter)
        return _sel

    def __Tau23pi__(self, conf):
        """
        Retrieve the taus
        """
        from PhysSelPython.Wrappers import DataOnDemand
        from StandardParticles import StdLooseDetachedTau3piNoPID #StdLooseDetachedTau3pi
        _tau23pi = StdLooseDetachedTau3piNoPID #StdLooseDetachedTau3pi
        # _tau23pi = DataOnDemand(
        #     #Location="Phys/StdLooseDetachedTau3piNoPID/Particles")
        #     Location="Phys/StdLooseDetachedTau3pi/Particles")
        # return _tau23pi
        _filter = FilterDesktop(Code=self.Tau23PionCut)
        _sel = Selection("Selection_"+self.name+"_Tau23Pion",
                         RequiredSelections=[_tau23pi],
                         Algorithm=_filter)
        return _sel

    def __Tau23piFake__(self, conf):
        """
        Retrieve the taus
        """
        from StandardParticles import StdLooseDetachedTau3piNonPhys
        _tau23pi = StdLooseDetachedTau3piNonPhys
        # from PhysSelPython.Wrappers import DataOnDemand
        # _tau23pi = DataOnDemand(
        #     Location="Phys/StdLooseDetachedTau3piNonPhys/Particles")
        # return _tau23pi
        _filter = FilterDesktop(Code=self.Tau23PionCut)
        _sel = Selection("Selection_"+self.name+"_Tau23PionNonPhys",
                         RequiredSelections=[_tau23pi],
                         Algorithm=_filter)
        return _sel

    def __Tau21Pi__(self, conf):
        """
        Filter muons from StdAllLooseMuons
        """
        from StandardParticles import StdAllLoosePions
        _pions = StdAllLoosePions
        _filter = FilterDesktop(Code=self.Tau21PionCut)
        _sel = Selection("Selection_"+self.name+"_Tau21Pion",
                         RequiredSelections=[_pions],
                         Algorithm=_filter)
        return _sel

    def __Jpsi__(self, conf):
        """
        Creates Jpsi as proxy for dimuon
        """

        CombineJpsi = CombineParticles(DecayDescriptors=["J/psi(1S) -> mu+ mu-", "[J/psi(1S) -> mu+ mu+]cc"],
                                       MotherCut="ALL")
        sel_name = "Jpsi"
        SelJpsi = Selection("Sel_" + self.name + "_Jpsi", Algorithm=CombineJpsi,
                            RequiredSelections=[self.Muons])
        return SelJpsi

    def __FakeJpsi__(self, conf):
        """
        Creates Jpsi as proxy for dimuon with one fake muon
        """

        NoPIDsJpsi = CombineParticles(DecayDescriptors=["J/psi(1S) -> mu+ mu-", "[J/psi(1S) -> mu+ mu+]cc"],
                                      MotherCut=self.LooseJpsiCut)
        sel_name = "FakeJpsi"
        SelJpsi = Selection("Sel_" + self.name + "_FakeJpsi", Algorithm=NoPIDsJpsi,
                            RequiredSelections=[self.FakeMuons])

        return SelJpsi

    def __Jpsiee__(self, conf):
        """
        Creates Jpsi as proxy for dielectron
        """

        CombineJpsi = CombineParticles(DecayDescriptors=["J/psi(1S) -> e+ e-", "[J/psi(1S) -> e+ e+]cc"],
                                       MotherCut="ALL")
        sel_name = "Jpsiee"
        SelJpsi = Selection("Sel_" + self.name + "_Jpsiee", Algorithm=CombineJpsi,
                            RequiredSelections=[self.Electrons])
        return SelJpsi

    def __JpsieeOS__(self, conf):
        """
        Creates Jpsi as proxy for dielectron using DiElectronMaker
        """
        from Configurables import DiElectronMaker
        from CommonParticles.Utils import updateDoD
        from Configurables import FilterDesktop as FilterDesktopNamed

        LongElectronsOS = FilterDesktopNamed("LongElectronsOS", Inputs=['Phys/StdAllLooseElectrons/Particles'],
            Code=self.TrackCutsElectron + ' & (ISLONG)')
        DownElectronsOS = FilterDesktopNamed("DownElectronsOS", Inputs=['Phys/StdNoPIDsDownElectrons/Particles'],
            Code=self.TrackCutsElectron + ' & (ISDOWN)')
        updateDoD(LongElectronsOS)
        updateDoD(DownElectronsOS)

        dieLL = DiElectronMaker("DiElectronOS")
        dieLL.Particle = 'J/psi(1S)'
        dieLL.ElectronInputs = ['Phys/LongElectronsOS/Particles','Phys/DownElectronsOS/Particles']
        dieLL.ElectronPtMin = conf['Electron_PT']
        dieLL.DiElectronPtMin = conf['DiElectron_PT']
        dieLL.DiElectronMassMin = conf['DiElectron_MASS_MIN']
        dieLL.DiElectronMassMax = conf['DiElectron_MASS_MAX']
        dieLL.OppositeSign = True
        JpsiFromTracks = Selection(
            'Sel_'+self.name+'_JpsieeOS', Algorithm=dieLL)

        _filterJpsi = FilterDesktop(Code=self.JpsieeCut)

        SelJpsi = Selection("Sel_"+self.name+"JpsieeOS",
                            Algorithm=_filterJpsi, RequiredSelections=[JpsiFromTracks])
        return SelJpsi

    def __JpsieeSS__(self, conf):
        """
        Creates Jpsi as proxy for dielectron using DiElectronMaker
        """
        from Configurables import DiElectronMaker
        from CommonParticles.Utils import updateDoD
        from Configurables import FilterDesktop as FilterDesktopNamed

        LongElectronsSS = FilterDesktopNamed("LongElectronsSS", Inputs=['Phys/StdAllLooseElectrons/Particles'],
            Code=self.TrackCutsElectron + ' & (ISLONG)')
        DownElectronsSS = FilterDesktopNamed("DownElectronsSS", Inputs=['Phys/StdNoPIDsDownElectrons/Particles'],
            Code=self.TrackCutsElectron + ' & (ISDOWN)')
        updateDoD(LongElectronsSS)
        updateDoD(DownElectronsSS)

        dieLL = DiElectronMaker("DiElectronSS")
        dieLL.Particle = 'J/psi(1S)'
        dieLL.ElectronInputs = ['Phys/LongElectronsSS/Particles','Phys/DownElectronsSS/Particles']
        dieLL.ElectronPtMin = conf['Electron_PT']
        dieLL.DiElectronPtMin = conf['DiElectron_PT']
        dieLL.DiElectronMassMin = conf['DiElectron_MASS_MIN']
        dieLL.DiElectronMassMax = conf['DiElectron_MASS_MAX']
        dieLL.OppositeSign = False
        JpsiFromTracks = Selection(
            'Sel_'+self.name+'_JpsieeSS', Algorithm=dieLL)

        _filterJpsi = FilterDesktop(Code=self.JpsieeCut)

        SelJpsi = Selection("Sel_"+self.name+"JpsieeSS",
                            Algorithm=_filterJpsi, RequiredSelections=[JpsiFromTracks])
        return SelJpsi

    def __FakeJpsieeOS__(self, conf):
        """
        Creates Jpsi as proxy for dielectron using DiElectronMaker
        One electron without PID
        """
        from Configurables import DiElectronMaker
        from CommonParticles.Utils import updateDoD
        from Configurables import FilterDesktop as FilterDesktopNamed

        FakeLongElectronsOS = FilterDesktopNamed("FakeLongElectronsOS", Inputs=['Phys/StdAllNoPIDsElectrons/Particles'],
            Code=self.TrackCutsElectron + ' & (ISLONG)')
        FakeDownElectronsOS = FilterDesktopNamed("FakeDownElectronsOS", Inputs=['Phys/StdNoPIDsDownElectrons/Particles'],
            Code=self.TrackCutsElectron + ' & (ISDOWN)')
        updateDoD(FakeLongElectronsOS)
        updateDoD(FakeDownElectronsOS)

        dieLL = DiElectronMaker("FakeDiElectronOS")
        dieLL.Particle = 'J/psi(1S)'
        dieLL.ElectronInputs = ['Phys/FakeLongElectronsOS/Particles','Phys/FakeDownElectronsOS/Particles']
        dieLL.ElectronPtMin = conf['Electron_PT']
        dieLL.DiElectronPtMin = conf['DiElectron_PT']
        dieLL.DiElectronMassMin = conf['DiElectron_MASS_MIN']
        dieLL.DiElectronMassMax = conf['DiElectron_MASS_MAX']
        dieLL.OppositeSign = True
        NoPIDsJpsiFromTracks = Selection(
            'Sel_'+self.name+'_NoPIDsJpsieeOS', Algorithm=dieLL)

        _filterFakeJpsi = FilterDesktop(Code=self.LooseJpsieeCut)

        SelJpsi = Selection("Sel_"+self.name+"FakeJpsieeOS",
                            Algorithm=_filterFakeJpsi, RequiredSelections=[NoPIDsJpsiFromTracks])
        return SelJpsi

    def __FakeJpsieeSS__(self, conf):
        """
        Creates Jpsi as proxy for dielectron using DiElectronMaker
        One electron without PID
        """
        from Configurables import DiElectronMaker
        from CommonParticles.Utils import updateDoD
        from Configurables import FilterDesktop as FilterDesktopNamed

        FakeLongElectronsSS = FilterDesktopNamed("FakeLongElectronsSS", Inputs=['Phys/StdAllNoPIDsElectrons/Particles'],
            Code=self.TrackCutsElectron + ' & (ISLONG)')
        FakeDownElectronsSS = FilterDesktopNamed("FakeDownElectronsSS", Inputs=['Phys/StdNoPIDsDownElectrons/Particles'],
            Code=self.TrackCutsElectron + ' & (ISDOWN)')
        updateDoD(FakeLongElectronsSS)
        updateDoD(FakeDownElectronsSS)

        dieLL = DiElectronMaker("FakeDiElectronSS")
        dieLL.Particle = 'J/psi(1S)'
        dieLL.ElectronInputs = ['Phys/FakeLongElectronsSS/Particles','Phys/FakeDownElectronsSS/Particles']
        dieLL.ElectronPtMin = conf['Electron_PT']
        dieLL.DiElectronPtMin = conf['DiElectron_PT']
        dieLL.DiElectronMassMin = conf['DiElectron_MASS_MIN']
        dieLL.DiElectronMassMax = conf['DiElectron_MASS_MAX']
        dieLL.OppositeSign = False
        NoPIDsJpsiFromTracks = Selection(
            'Sel_'+self.name+'_NoPIDsJpsieeSS', Algorithm=dieLL)

        _filterFakeJpsi = FilterDesktop(Code=self.LooseJpsieeCut)

        SelJpsi = Selection("Sel_"+self.name+"FakeJpsieeSS",
                            Algorithm=_filterFakeJpsi, RequiredSelections=[NoPIDsJpsiFromTracks])
        return SelJpsi

    def __Trimu__(self, conf):
        '''
        Create trimuon
        '''
        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptors = [
            "[B+ -> mu+ mu+ mu-]cc", "[B+ -> mu+ mu+ mu+]cc"]
        sel_name = "TriMu"
        CombineTriMuon.MotherCut = self.TriMuCut

        SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name,
                               Algorithm=CombineTriMuon,
                               RequiredSelections=[self.Muons])
        return SelTriMuon

    def __Trie__(self, conf):
        '''
        Create trielectron
        '''
        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptors = [
            "[B+ -> e+ e+ e-]cc", "[B+ -> e+ e+ e+]cc"]
        sel_name = "Trie"
        CombineTriMuon.MotherCut = self.TriMuCut

        SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name,
                               Algorithm=CombineTriMuon,
                               RequiredSelections=[self.Electrons])
        return SelTriMuon

    def __MuMue__(self, conf):
        '''
        Create MuMue combination
        '''
        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) e+]cc"
        sel_name = "MuMue"
        CombineTriMuon.MotherCut = self.TriMuCut

        SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name,
                               Algorithm=CombineTriMuon,
                               RequiredSelections=[self.Jpsi, self.Electrons])
        return SelTriMuon

    def __Muee__(self, conf):
        '''
        Create Muee combination
        '''
        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) mu+]cc"
        sel_name = "Muee"
        CombineTriMuon.MotherCut = self.TriMuCut

        Jpsi_SSandOS = MergedSelection(
            "MergedJpsi_"+sel_name, RequiredSelections=[self.JpsieeSS, self.JpsieeOS])
        SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name,
                               Algorithm=CombineTriMuon,
                               RequiredSelections=[self.Muons, Jpsi_SSandOS])
        return SelTriMuon

    def __FakeTrimu__(self, conf):
        """
        Create fake trimuon
        """

        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) mu+]cc"
        sel_name = "FakeTriMu"
        CombineTriMuon.MotherCut = self.TriMuCut

        SelTriMuon = Selection(sel_name,
                               Algorithm=CombineTriMuon,
                               RequiredSelections=[self.FakeMuons, self.Jpsi])

        return SelTriMuon

    def __FakeTrie__(self, conf):
        """
        Create fake trielectron
        """

        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) e+]cc"
        sel_name = "FakeTrie"
        CombineTriMuon.MotherCut = self.TriMuCut

        SelTriMuon = Selection(sel_name,
                               Algorithm=CombineTriMuon,
                               RequiredSelections=[self.FakeElectrons, self.Jpsiee])

        return SelTriMuon

    def __MuMuEFake__(self, conf):
        """
        Create fake MuMue with fake electron
        """

        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) e+]cc"
        sel_name = "MuMuEFake"
        CombineTriMuon.MotherCut = self.TriMuCut

        SelTriMuon = Selection(sel_name,
                               Algorithm=CombineTriMuon,
                               RequiredSelections=[self.FakeElectrons, self.Jpsi])

        return SelTriMuon

    def __MuMuFakeE__(self, conf):
        """
        Create fake MuMue with fake muon
        """

        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) e+]cc"
        sel_name = "MuMuFakeE"
        CombineTriMuon.MotherCut = self.TriMuCut

        SelTriMuon = Selection(sel_name,
                               Algorithm=CombineTriMuon,
                               RequiredSelections=[self.Electrons, self.FakeJpsi])

        return SelTriMuon

    def __MuFakeEE__(self, conf):
        """
        Create Muee with fake muon
        """

        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) mu+]cc"
        sel_name = "MuFakeEE"
        CombineTriMuon.MotherCut = self.TriMuCut

        Jpsi_SSandOS = MergedSelection(
            "MergedJpsi_"+sel_name, RequiredSelections=[self.JpsieeSS, self.JpsieeOS])
        SelTriMuon = Selection(sel_name,
                               Algorithm=CombineTriMuon,
                               RequiredSelections=[self.FakeMuons, Jpsi_SSandOS])

        return SelTriMuon

    def __MuEEFake__(self, conf):
        """
        Create Muee with fake electron
        """

        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) mu+]cc"
        sel_name = "MuEEFake"
        CombineTriMuon.MotherCut = self.TriMuCut

        FakeJpsi_SSandOS = MergedSelection(
            "MergedFakeJpsi_"+sel_name, RequiredSelections=[self.FakeJpsieeSS, self.FakeJpsieeOS])
        SelTriMuon = Selection(sel_name,
                               Algorithm=CombineTriMuon,
                               RequiredSelections=[self.Muons, FakeJpsi_SSandOS])

        return SelTriMuon

    def __MuMuTau__(self, conf):
        '''
        Create MuMuTau combination
        '''

        CombineMuMuTau = CombineParticles()
        CombineMuMuTau.DecayDescriptors = ["[B+ -> J/psi(1S) tau+ ]cc"]
        sel_name = "MuMuTau"
        CombineMuMuTau.MotherCut = self.MuMuTauCut + self.TightMuonIDCut

        SelMuMuTau = Selection("Sel_" + self.name + "_"+sel_name,
                               Algorithm=CombineMuMuTau,
                               RequiredSelections=[self.Jpsi, self.Tau23pi])
        return SelMuMuTau

    def __MuMuTauFake__(self, conf):
        '''
        Create MuMuTau combination with the fake tau
        '''

        CombineMuMuTau = CombineParticles()
        CombineMuMuTau.DecayDescriptors = ["[B+ -> J/psi(1S) tau+ ]cc"]
        sel_name = "MuMuTauFake"
        CombineMuMuTau.MotherCut = self.MuMuTauCut + self.TightMuonIDCut

        SelMuMuTau = Selection("Sel_" + self.name + "_"+sel_name,
                               Algorithm=CombineMuMuTau,
                               RequiredSelections=[self.Jpsi, self.Tau23piFake])
        return SelMuMuTau

    def __MuMuFakeTau__(self, conf):
        '''
        Create MuMuTau combination with a fake muon
        '''

        CombineMuMuTau = CombineParticles()
        CombineMuMuTau.DecayDescriptors = ["[B+ -> J/psi(1S) tau+ ]cc"]
        sel_name = "MuMuFakeTau"
        CombineMuMuTau.MotherCut = self.MuMuTauCut

        SelMuMuTau = Selection("Sel_" + self.name + "_"+sel_name,
                               Algorithm=CombineMuMuTau,
                               RequiredSelections=[self.FakeJpsi, self.Tau23pi])
        return SelMuMuTau

    def __EETau__(self, conf):
        '''
        Create EETau combination
        '''

        CombineEETau = CombineParticles()
        CombineEETau.DecayDescriptors = ["[B+ -> J/psi(1S) tau+ ]cc"]
        sel_name = "EETau"
        CombineEETau.MotherCut = self.MuMuTauCut + self.TightElectronIDCut
        CombineEETau.DaughtersCuts = {'tau+' : '(VFASPF(VCHI2) < %(Tau_VertexChi2)s)' % conf}

        Jpsi_SSandOS = MergedSelection(
            "MergedJpsi_"+sel_name, RequiredSelections=[self.JpsieeSS, self.JpsieeOS])
        SelEETau = Selection("Sel_" + self.name + "_"+sel_name,
                             Algorithm=CombineEETau,
                             RequiredSelections=[Jpsi_SSandOS, self.Tau23pi])
        return SelEETau

    def __EETauFake__(self, conf):
        '''
        Create EETau combination with the fake tau
        '''

        CombineEETau = CombineParticles()
        CombineEETau.DecayDescriptors = ["[B+ -> J/psi(1S) tau+ ]cc"]
        sel_name = "EETauFake"
        CombineEETau.MotherCut = self.MuMuTauCut + self.TightElectronIDCut
        CombineEETau.DaughtersCuts = {'tau+' : '(VFASPF(VCHI2) < %(Tau_VertexChi2)s)' % conf}

        Jpsi_SSandOS = MergedSelection(
            "MergedJpsiForFakeTau_"+sel_name, RequiredSelections=[self.JpsieeSS, self.JpsieeOS])
        SelEETau = Selection("Sel_" + self.name + "_"+sel_name,
                             Algorithm=CombineEETau,
                             RequiredSelections=[Jpsi_SSandOS, self.Tau23piFake])
        return SelEETau

    def __EEFakeTau__(self, conf):
        '''
        Create EETau combination with fake electron
        '''

        CombineEETau = CombineParticles()
        CombineEETau.DecayDescriptors = ["[B+ -> J/psi(1S) tau+ ]cc"]
        sel_name = "EEFakeTau"
        CombineEETau.MotherCut = self.MuMuTauCut

        Jpsi_SSandOS = MergedSelection(
            "MergedFakeJpsi_"+sel_name, RequiredSelections=[self.FakeJpsieeSS, self.FakeJpsieeOS])
        SelEETau = Selection("Sel_" + self.name + "_"+sel_name,
                             Algorithm=CombineEETau,
                             RequiredSelections=[Jpsi_SSandOS, self.Tau23pi])
        return SelEETau

    def __MuMuTau1Pi__(self, conf):
        '''
        Create MuMuTau combination with tau decay to 1 pion + neutrals.
        Require dimuon to have low mass (i.e. come from gamma* or rho/omega) to control the rate and remove stuff with the Jpsi.
        '''
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineMuMuTau = CombineParticles()
        CombineMuMuTau.DaughtersCuts = {
            "J/psi(1S)": "(VFASPF(VCHI2/VDOF) < %(DiElectron_Vertex_TIGHT)s) & (M < %(DiElectron_MASS_MAX_TIGHT)s * MeV) & " \
            "(0.5 < NINTREE((ABSID==13) & (PIDmu> %(Muon_PIDmu_Tight)s) & (HASRICH))) & (MAXTREE(ABSID==13,PT) > %(Lepton_PT_VERYTIGHT)s * MeV) & " \
                "(MAXTREE(ABSID==13,PROBNNmu) > %(Muon_ProbNNmu_Tight)s) & (MINTREE(ABSID==13,MIPCHI2DV(PRIMARY)) > %(Muon_MinIPCHI2_Tight)s)" % conf}
        CombineMuMuTau.DecayDescriptors = ["[B+ -> J/psi(1S) pi+ ]cc"]
        sel_name = "MuMuTau1Pi"
        CombineMuMuTau.MotherCut = self.MuMuTau1PiCut

        from PhysSelPython.Wrappers import Selection
        SelMuMuTau = Selection("Sel_" + self.name + "_"+sel_name,
                               Algorithm=CombineMuMuTau,
                               RequiredSelections=[self.Jpsi, self.Tau21Pi])
        return SelMuMuTau

    def __MuMuFakeTau1Pi__(self, conf):
        '''
        Create MuMuTau combination with tau decay to 1 pion + neutrals.
        Require dimuon to have low mass (i.e. come from gamma* or rho/omega) to control the rate and remove stuff with the Jpsi.
        '''
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineMuMuTau = CombineParticles()
        CombineMuMuTau.DaughtersCuts = {
            "J/psi(1S)": "(VFASPF(VCHI2/VDOF) < %(DiElectron_Vertex_TIGHT)s) & (M < %(DiElectron_MASS_MAX_TIGHT)s * MeV) & " \
            "(0.5 < NINTREE((ABSID==13) & (PIDmu> %(Muon_PIDmu_Tight)s) & (HASRICH))) & (MAXTREE(ABSID==13,PT) > %(Lepton_PT_VERYTIGHT)s * MeV) & " \
                "(MAXTREE(ABSID==13,PROBNNmu) > %(Muon_ProbNNmu_Tight)s) & (MINTREE(ABSID==13,MIPCHI2DV(PRIMARY)) > %(Muon_MinIPCHI2_Tight)s)" % conf}
        CombineMuMuTau.DecayDescriptors = ["[B+ -> J/psi(1S) pi+ ]cc"]
        sel_name = "MuMuFakeTau1Pi"
        CombineMuMuTau.MotherCut = self.MuMuTau1PiCut

        from PhysSelPython.Wrappers import Selection
        SelMuMuTau = Selection("Sel_" + self.name + "_"+sel_name,
                               Algorithm=CombineMuMuTau,
                               RequiredSelections=[self.FakeJpsi, self.Tau21Pi])
        return SelMuMuTau
