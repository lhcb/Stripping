###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting B0->Lc Lcbar KS0
'''

__author__=['Shuqi Sheng']
__date__ = '15/06/2023'
__version__= '$Revision: 2.0$'

__all__ = ( 
    'B02LcLcKsConf',
    'default_config'
    )

default_config =  {
    'NAME'              :  'B02LcLcKs',
    'BUILDERTYPE'       :  'B02LcLcKsConf',
    'CONFIG'    : {
        'ProtonCuts'    : "(PT>100*MeV) & (PROBNNp>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0) & (TRCHI2DOF<3)",
        'KaonCuts'      : "(PT>100*MeV) & (PROBNNk>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0) & (TRCHI2DOF<3)",
        'PionCuts'      : "(PT>100*MeV) & (PROBNNpi>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0)& (TRCHI2DOF<3)",
        'LcComCuts'     : "(ADOCA(1,2)<0.5*mm)",
        'LcComN3Cuts'   : "(ASUM(PT)>1000*MeV) & (ADAMASS('Lambda_c+')<110*MeV) & (AHASCHILD((ISBASIC & HASTRACK & (PT > 500*MeV) & (P > 5000*MeV)))) & (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        'LcMomCuts'     : "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)",
        'KsCuts'        : "(BPVDLS>5.)",
        'B0ComCuts'     : "AALL",
        'B0ComN3Cuts'   : "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)",
        'B0MomCuts'     : "(VFASPF(VCHI2/VDOF)<10.) & (BPVLTIME()>0.2*ps) & (BPVIPCHI2()<25) & (BPVDIRA>0.)",
        'Prescale'      : 1.
    },  
    'STREAMS'           : ['BhadronCompleteEvent'],
    'WGs'               : ['BandQ'],
    }

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays
from PhysSelPython.Wrappers import MergedSelection

class B02LcLcKsConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config ):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        self.SelKaons = self.createSubSel( OutputList = self.name + "SelKaons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseKaons/Particles' ),
                                           Cuts = config['KaonCuts']
                                           )

        self.SelPions = self.createSubSel( OutputList = self.name + "SelPions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLoosePions/Particles' ),
                                           Cuts = config['PionCuts']
                                           )

        self.SelProtons = self.createSubSel( OutputList = self.name + "SelProtons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseProtons/Particles' ),
                                           Cuts = config['ProtonCuts']
                                           )
        """
        Kshort 
        """
        self.SelKsDDs = self.createSubSel( OutputList = self.name + "SelKsDDs",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseKsDD/Particles' ),
                                           Cuts = config['KsCuts']
                                           )

        self.SelKsLLs = self.createSubSel( OutputList = self.name + "SelKsLLs",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseKsLL/Particles' ),
                                           Cuts = config['KsCuts']
                                           )
        """
        Lc-> P K Pi
        """
        self.SelLc2PKPi = self.createN3BodySel( OutputList = self.name + "SelLc2PKPi",
                                                    DaughterLists = [ self.SelProtons,self.SelKaons, self.SelPions ],
                                                    DecayDescriptor = "[ Lambda_c+ -> p+ K- pi+ ]cc",
                                                    ComAMCuts      = config['LcComCuts'],
                                                    PreVertexCuts  = config['LcComN3Cuts'],
                                                    PostVertexCuts = config['LcMomCuts']
                                                    )
        """
        B0->Lc Lc KS0DD 
        """
        self.SelB2LcLcKsDD = self.createN3BodySel( OutputList = self.name + "SelB2LcLcKsDD",
                                                              DecayDescriptor = "B0 -> Lambda_c+ Lambda_c~- KS0",
                                                              DaughterLists = [ self.SelLc2PKPi, self.SelKsDDs ],
                                                              ComAMCuts      = config['B0ComCuts'],
                                                              PreVertexCuts  = config['B0ComN3Cuts'],
                                                              PostVertexCuts = config['B0MomCuts'] )

        self.B2LcLcKsDDLine = StrippingLine( self.name + 'DDLine',
                                                   prescale  = config['Prescale'],
                                                   algos     = [ self.SelB2LcLcKsDD ],
                                                   )

        self.registerLine( self.B2LcLcKsDDLine )
        """
        B0->Lc Lc KS0LL 
        """
        self.SelB2LcLcKsLL = self.createN3BodySel( OutputList = self.name + "SelB2LcLcKsLL",
                                                              DecayDescriptor = "B0 -> Lambda_c+ Lambda_c~- KS0",
                                                              DaughterLists = [ self.SelLc2PKPi, self.SelKsLLs ],
                                                              ComAMCuts      = config['B0ComCuts'],
                                                              PreVertexCuts  = config['B0ComN3Cuts'],
                                                              PostVertexCuts = config['B0MomCuts'] )

        self.B2LcLcKsLLLine = StrippingLine( self.name + 'LLLine',
                                                   prescale  = config['Prescale'],
                                                   algos     = [ self.SelB2LcLcKsLL ],
                                                   )

        self.registerLine( self.B2LcLcKsLLLine )


    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL") :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     CombinationCut = PreVertexCuts,
                                     MotherCut = PostVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
    def createN3BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N3BodyDecays ( DecayDescriptor = DecayDescriptor,
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts,
                                           CombinationCut = PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
