###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Stripping Line for Jpsi+Jet production studies 
#
# R. McNulty with lots of help from D. Johnson and V. Zhovkovska
#
# Jpsi production to be as unbiased as possible.  Require 1 PV and low Herschel activity in the backwards direction
#
# Require following in the DaVinci configuration:                             
# from Configurables import HCRawBankDecoderHlt                               
# Hlt1LowMult_HCRawBankDecoderHlt = HCRawBankDecoderHlt('hcrawbankdecoder')   
# DaVinci().appendToMainSequence([Hlt1LowMult_HCRawBankDecoderHlt])           

__author__ = ['R. McNulty']

__all__ = (
  'OniaPhotoProductionConf',
  'default_config',
)

from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllLooseMuons
from GaudiKernel.SystemOfUnits import GeV
from Configurables import HCRawBankDecoderHlt

default_config = {
  'JpsiPhotoProduction':
  {  'BUILDERTYPE' : 'OniaPhotoProductionConf',
      'WGs'         : [ 'BandQ'],
      'STREAMS'     : [ 'Dimuon' ],
      'CONFIG'      : { 
    
        'Prescale'  : 0.5,
        'Postscale' : 1.0,
        'pT'        : 0.  * GeV,
        'MMmin'     : 2.977 * GeV,
        'MMmax'     : 3.217 * GeV,
        'MaxHRCADC' : 350,
        'RawEvents' : ["Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
      }  
    },
  'Psi2SPhotoProduction':
  {  'BUILDERTYPE'  : 'OniaPhotoProductionConf',
      'WGs'         : [ 'BandQ'],
      'STREAMS'     : [ 'Dimuon' ],
      'CONFIG'      : { 
    
        'Prescale'  : 1.0,
        'Postscale' : 1.0,
        'pT'        : 0.  * GeV,
        'MMmin'     : 3.566 * GeV,
        'MMmax'     : 3.806 * GeV,
        'MaxHRCADC' : 350,
        'RawEvents' : ["Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
      }  
    },
  'UpsilonPhotoProduction':
  {  'BUILDERTYPE'  : 'OniaPhotoProductionConf',
      'WGs'         : [ 'BandQ'],
      'STREAMS'     : [ 'Dimuon' ],
      'CONFIG'      : { 
    
        'Prescale'  : 1.0,
        'Postscale' : 1.0,
        'pT'        : 0.  * GeV,
        'MMmin'     : 9.300 * GeV,
        'MMmax'     : 999.0 * GeV,
        'MaxHRCADC' : 500,
        'RawEvents' : ["Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
      }  
    },
}

class OniaPhotoProductionConf( LineBuilder ) :

  __configuration_keys__ = default_config['JpsiPhotoProduction']['CONFIG'].keys()

  def __init__( self, name, config ) :

    LineBuilder.__init__( self, name, config )

    self.registerLine(StrippingLine( name + 'Line',
      prescale          = config[ 'Prescale'  ],
      postscale         = config[ 'Postscale' ],
      RequiredRawEvents = config[ 'RawEvents' ],
      checkPV           = (0,1),
      FILTER ={
        'Code':
        "( ( HRCSUMADC('Raw/HC/Sums','B2') < %(MaxHRCADC)s ) & ( HRCSUMADC('Raw/HC/Sums','B1') < %(MaxHRCADC)s ) &    ( HRCSUMADC('Raw/HC/Sums','B0') < %(MaxHRCADC)s ) )"%config
         },
      selection         = makeCombination(name+'PhotoProduction', config),
    ))



def makeCombination( name, config):
  # Define the cuts
  dcut = '(PT>%(pT)s)'%config
  mcut = '(MM>%(MMmin)s) & (MM<%(MMmax)s)'%config


  return SimpleSelection(name, CombineParticles, [StdAllLooseMuons],
    DecayDescriptor    = 'J/psi(1S) -> mu+ mu-',
    DaughtersCuts      = { 'mu+' : dcut, 'mu-' : dcut },
    MotherCut          = mcut,
    WriteP2PVRelations = False
  )
