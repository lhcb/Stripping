###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for B_c->eta_c semileptonic

========== Description of lines ==========

B_c->eta_c mu, with eta_c->ppbar
B_c->eta_c mu, with eta_c->K_s K pi
B_c->eta_c tau, with eta_c->ppbar and tau->mu
B_c->eta_c tau, with eta_c->K_s K pi and tau->mu
B_c->eta_c tau, with eta_c->ppbar and tau->3pi
B_c->eta_c tau, with eta_c->K_s K pi and tau->3pi
'''

__author__ = ['Valeriia Zhovkovska']
__date__ = '24/06/2021'
__version__ = '$Revision: 1.1 $'

__all__ = ('Bc2EtacSLConf', 'default_config')

# If you have several configs in one module your default_config should
# be a dict of configs with the names of the configs as keys. The configs
# themselves then shouldn't have a 'NAME' element.
# M.A. 2017/05/02.
default_config = {
    'NAME':        'Bc2EtacSL',
    'BUILDERTYPE': 'Bc2EtacSLConf',
    'CONFIG': {
        'LinePrescale':
        1.,
        'LinePostscale':
        1.,
        'SpdMult':
        450.,  # dimensionless, Spd Multiplicy cut
        'MuonCuts':
        "(PROBNNmu > 0.1) & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)",
        'FakeMuonCuts':
        "(PROBNNmu > 0.1) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 16)",
        'TauCombCuts':
        "in_range(500.*MeV, AM, 2100.*MeV) & (APT > 800*MeV) & ((AM12<1670.*MeV) or (AM23<1670.*MeV)) & (ACUTDOCA(0.08*mm,''))",
        'TauMomCuts':
        "in_range(600.*MeV, M, 2000.*MeV)  & (PT > 1000*MeV) & (VFASPF(VCHI2) < 9) & (BPVVDCHI2>16)",
        'Pion4TauCuts':
        "(PROBNNpi > 0.65) & (PT > 250*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 16)",
        'ProtonCuts':
        "(PROBNNp > 0.2)   & (PT > 600*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)",
        'PionCuts':
        "(PROBNNpi > 0.65) & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 4)",
        'KaonCuts':
        "(PROBNNk > 0.65)  & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 4)",
        'KsCuts':
        "(ADMASS('KS0') < 30.*MeV) & (BPVDLS>5) & (PT > 500*MeV) & (MAXTREE('pi-'==ABSID, PROBNNpi) > 0.45) & (MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.4) & (MAXTREE('pi-'==ABSID, TRCHI2DOF) < 5)",
        'EtacCombCuts':
        "in_range(2.7*GeV, AM, 3.3*GeV)",
        'EtacMomCuts':
        "in_range(2.7*GeV, MM, 3.3*GeV) & (VFASPF(VCHI2/VDOF) < 9.)",
        'BcTightCombCuts':
        "in_range(3.2*GeV, AM, 7.5*GeV)",
        'BcLooseCombCuts':
        "in_range(0.75*GeV, AM, 7.5*GeV)",
        'Bc4PpbarMuMomCuts':
        "in_range(3.2*GeV, MM, 7.5*GeV)  & in_range(4.0*GeV, BPVCORRM, 12.0*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 3000)",
        'Bc4KsKpiMuMomCuts':
        "in_range(3.2*GeV, MM, 7.5*GeV)  & in_range(4.0*GeV, BPVCORRM, 12.0*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 5000)",
        'Bc4PpbarTauMomCuts':
        "in_range(0.75*GeV, MM, 7.5*GeV) & in_range(1.0*GeV, BPVCORRM, 12.0*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 3000)",
        'Bc4KsKpiTauMomCuts':
        "in_range(0.75*GeV, MM, 7.5*GeV) & in_range(1.0*GeV, BPVCORRM, 12.0*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 5000)",
        'CCCut':
        ""
    },
    'STREAMS': ['CharmCompleteEvent'],
    'WGs': ['BandQ']
}

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


class Bc2EtacSLConf(LineBuilder):

    # __configuration_keys__ = default_config['CONFIG'].keys()

    __configuration_keys__ = ('LinePrescale', 'LinePostscale', 'SpdMult',
                              'MuonCuts', 'FakeMuonCuts', 
                              'TauMomCuts', 'TauCombCuts','Pion4TauCuts',
                              'ProtonCuts', 'PionCuts', 'KaonCuts',
                              'KsCuts', 'EtacCombCuts', 'EtacMomCuts',
                              'BcTightCombCuts',    'BcLooseCombCuts', 
                              'Bc4PpbarMuMomCuts',  'Bc4KsKpiMuMomCuts',
                              'Bc4PpbarTauMomCuts', 'Bc4KsKpiTauMomCuts',
                              'CCCut')

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        self.name = name
        self.config = config

        from PhysSelPython.Wrappers import MergedSelection

        self.InputKs = MergedSelection(
            self.name + "InputKs",
            RequiredSelections=[
                DataOnDemand(Location="Phys/StdLooseKsDD/Particles"),
                DataOnDemand(Location="Phys/StdVeryLooseKsLL/Particles")
            ])

        self.SelKs = self.createSubSel(
            OutputList=self.name + "SelKs",
            InputList=self.InputKs,
            Cuts=config['KsCuts'])

        self.SelKaons = self.createSubSel(
            OutputList=self.name + "SelKaons",
            InputList=DataOnDemand(Location='Phys/StdLooseKaons/Particles'),
            Cuts=config['KaonCuts'])

        self.SelPions = self.createSubSel(
            OutputList=self.name + "SelPions",
            InputList=DataOnDemand(
                Location='Phys/StdAllNoPIDsPions/Particles'),
            Cuts=config['PionCuts'])

        self.SelProtons = self.createSubSel(
            OutputList=self.name + "SelProtons",
            InputList=DataOnDemand(Location='Phys/StdLooseProtons/Particles'),
            Cuts=config['ProtonCuts'])

        self.SelMuons = self.createSubSel(
            OutputList=self.name + "SelMuons",
            InputList=DataOnDemand(Location='Phys/StdLooseMuons/Particles'),
            Cuts=config['MuonCuts'])

        self.SelFakeMuons = self.createSubSel(
            OutputList=self.name + "SelFakeMuons",
            InputList=DataOnDemand(Location='Phys/StdAllNoPIDsMuons/Particles'),
            Cuts=config['FakeMuonCuts'])

        self.SelPions4Tau = self.createSubSel(
            OutputList=self.name + "SelPions4Tau",
            InputList=DataOnDemand(Location='Phys/StdLoosePions/Particles'),
            # InputList=DataOnDemand(Location='Phys/StdLooseANNPions/Particles'),
            Cuts=config['Pion4TauCuts'])

        # tau+ -> pi+ pi- pi+
        self.SelTau = self.createCombinationSel(
            OutputList=self.name + "SelTau",
            DecayDescriptor="[tau+ -> pi+ pi- pi+]cc",
            DaughterLists=[self.SelPions4Tau],
            PreVertexCuts=config['TauCombCuts'],
            PostVertexCuts=config['TauMomCuts'])

        # Eta_c -> KS0 K Pi
        self.SelEtac2KsKPi = self.createCombinationSel(
            OutputList=self.name + "SelEtac2KsKPi",
            DecayDescriptor="[eta_c(1S) -> KS0 K+ pi-]cc",
            DaughterLists=[self.SelKs, self.SelKaons, self.SelPions],
            PreVertexCuts=config['EtacCombCuts'],
            PostVertexCuts=config['EtacMomCuts'])

        # Eta_c -> p pbar
        self.SelEtac2Ppbar = self.createCombinationSel(
            OutputList=self.name + "SelEtac2Ppbar",
            DecayDescriptor="eta_c(1S) -> p+ p~-",
            DaughterLists=[self.SelProtons],
            PreVertexCuts=config['EtacCombCuts'],
            PostVertexCuts=config['EtacMomCuts'])

        SpdMultForBcCut = config['SpdMult']

        ################ Bc-> eta_c mu nu SELECTION ################

        self.SelBc2EtacMu_Ppbar = self.createCombinationSel(
            OutputList=self.name + "SelBc2EtacMu_Ppbar",
            DecayDescriptor="[B_c+ -> eta_c(1S) mu+]cc",
            DaughterLists=[self.SelEtac2Ppbar, self.SelMuons],
            PreVertexCuts=config['BcTightCombCuts'],
            PostVertexCuts=config['Bc4PpbarMuMomCuts'])

        self.Bc2EtacMu_PpbarLine = StrippingLine(
            self.name + "_Mu_PpbarLine",
            prescale=config['LinePrescale'],
            postscale=config['LinePostscale'],
            FILTER={
                'Code':
                " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMultForBcCut)s )"
                % locals(),
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            checkPV=True,
            algos=[self.SelBc2EtacMu_Ppbar])

        self.registerLine(self.Bc2EtacMu_PpbarLine)

        self.SelBc2EtacMu_KsKpi = self.createCombinationSel(
            OutputList=self.name + "SelBc2EtacMu_KsKpi",
            DecayDescriptor="[B_c+ -> eta_c(1S) mu+]cc",
            DaughterLists=[self.SelEtac2KsKPi, self.SelMuons],
            PreVertexCuts=config['BcTightCombCuts'],
            PostVertexCuts=config['Bc4KsKpiMuMomCuts'])

        self.Bc2EtacMu_KsKpiLine = StrippingLine(
            self.name + "_Mu_KsKpiLine",
            prescale=config['LinePrescale'],
            postscale=config['LinePostscale'],
            FILTER={
                'Code':
                " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMultForBcCut)s )"
                % locals(),
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            checkPV=True,
            algos=[self.SelBc2EtacMu_KsKpi])

        self.registerLine(self.Bc2EtacMu_KsKpiLine)

        ################ Bc-> eta_c (tau->mu nu nu) nu SELECTION ################

        self.SelBc2EtacTau2Mu_Ppbar = self.createCombinationSel(
            OutputList=self.name + "SelBc2EtacTau2Mu_Ppbar",
            DecayDescriptor="[B_c+ -> eta_c(1S) mu+]cc",
            DaughterLists=[self.SelEtac2Ppbar, self.SelFakeMuons],
            PreVertexCuts=config['BcLooseCombCuts'],
            PostVertexCuts=config['Bc4PpbarTauMomCuts'])

        self.Bc2EtacTau2Mu_PpbarLine = StrippingLine(
            self.name + "_Tau2Mu_PpbarLine",
            prescale=config['LinePrescale'],
            postscale=config['LinePostscale'],
            FILTER={
                'Code':
                " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMultForBcCut)s )"
                % locals(),
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            checkPV=True,
            algos=[self.SelBc2EtacTau2Mu_Ppbar])

        self.registerLine(self.Bc2EtacTau2Mu_PpbarLine)

        self.SelBc2EtacTau2Mu_KsKpi = self.createCombinationSel(
            OutputList=self.name + "SelBc2EtacTau2Mu_KsKpi",
            DecayDescriptor="[B_c+ -> eta_c(1S) mu+]cc",
            DaughterLists=[self.SelEtac2KsKPi, self.SelFakeMuons],
            PreVertexCuts=config['BcLooseCombCuts'],
            PostVertexCuts=config['Bc4KsKpiTauMomCuts'])

        self.Bc2EtacTau2Mu_KsKpiLine = StrippingLine(
            self.name + "_Tau2Mu_KsKpiLine",
            prescale=config['LinePrescale'],
            postscale=config['LinePostscale'],
            FILTER={
                'Code':
                " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMultForBcCut)s )"
                % locals(),
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            checkPV=True,
            algos=[self.SelBc2EtacTau2Mu_KsKpi])

        self.registerLine(self.Bc2EtacTau2Mu_KsKpiLine)

        ################ Bc-> eta_c (tau -> 3pi) nu SELECTION ################

        self.SelBc2EtacTau23pi_Ppbar = self.createCombinationSel(
            OutputList=self.name + "SelBc2EtacTau23pi_Ppbar",
            DecayDescriptor="[B_c+ -> eta_c(1S) tau+]cc",
            DaughterLists=[self.SelEtac2Ppbar, self.SelTau],
            PreVertexCuts=config['BcLooseCombCuts'],
            PostVertexCuts=config['Bc4PpbarTauMomCuts'])

        self.Bc2EtacTau23pi_PpbarLine = StrippingLine(
            self.name + "_Tau23pi_PpbarLine",
            prescale=config['LinePrescale'],
            postscale=config['LinePostscale'],
            FILTER={
                'Code':
                " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMultForBcCut)s )"
                % locals(),
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            checkPV=True,
            algos=[self.SelBc2EtacTau23pi_Ppbar])

        self.registerLine(self.Bc2EtacTau23pi_PpbarLine)

        self.SelBc2EtacTau23pi_KsKpi = self.createCombinationSel(
            OutputList=self.name + "SelBc2EtacTau23pi_KsKpi",
            DecayDescriptor="[B_c+ -> eta_c(1S) tau+]cc",
            DaughterLists=[self.SelEtac2KsKPi, self.SelTau],
            PreVertexCuts=config['BcLooseCombCuts'],
            PostVertexCuts=config['Bc4KsKpiTauMomCuts'])

        self.Bc2EtacTau23pi_KsKpiLine = StrippingLine(
            self.name + "_Tau23pi_KsKpiLine",
            prescale=config['LinePrescale'],
            postscale=config['LinePostscale'],
            FILTER={
                'Code':
                " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMultForBcCut)s )"
                % locals(),
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            checkPV=True,
            algos=[self.SelBc2EtacTau23pi_KsKpi])

        self.registerLine(self.Bc2EtacTau23pi_KsKpiLine)

    def createSubSel(self, OutputList, InputList, Cuts):
        '''create a selection using a FilterDesktop'''
        filt = FilterDesktop(Code=Cuts)
        return Selection(
            OutputList, Algorithm=filt, RequiredSelections=[InputList])

    def createCombinationSel(self,
                             OutputList,
                             DecayDescriptor,
                             DaughterLists,
                             DaughterCuts={},
                             PreVertexCuts="ALL",
                             PostVertexCuts="ALL",
                             Combination12Cut = "ALL",
                             ReFitPVs=True):
        '''create a selection using a ParticleCombiner with a single decay descriptor'''

        combiner = CombineParticles(
            DecayDescriptor=DecayDescriptor,
            DaughtersCuts=DaughterCuts,
            MotherCut=PostVertexCuts,
            CombinationCut=PreVertexCuts,
            ReFitPVs=True)
        return Selection(
            OutputList, Algorithm=combiner, RequiredSelections=DaughterLists)
