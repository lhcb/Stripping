###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = 'Max Chefdeville'
__date__ = '03/09/2019'
__version__ = '$Revision: 1.1 $'

'''
Stripping selection for D*+ -> [D0 -> K pi gamma] pi+
+ selection for D*+ -> [D0 -> K pi eta] pi+
'''
####################################################################
# Stripping selection for D*+ -> [D0 -> K pi gamma] pi+
#  line intended to measure PID performance of isPhoton
#  by measuring pi0's reco'ed as photons
# Selections taken from Regis Lefevre D02Kpipi0 lines for the D0
# and from TURCAL for the D*0
# +
# Stripping selection for D*+ -> [D0 -> K pi [eta -> gamma gamma]] pi+
# first PID sample of eta2gg in whole LHCb dataset
# selections aligned with D02Kpipi0 from Regis Lefevre
####################################################################

__all__ = ('StrippingDst2D0PiConf',
           'makeD02KPiGamma',
           'makeD02KPiEta',
           'makeDst2D0Pi',
           'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdTightPions,StdTightKaons,StdLooseAllPhotons,StdNoPIDsPions, StdLooseEta2gg

default_config = {
    'NAME'        : 'Dst2D0Pi',
    'WGs'         : ['Calib'],
    'BUILDERTYPE' : 'StrippingDst2D0PiConf',
    'CONFIG'      : {    'TrackMinPT'         : 300       # MeV
                        ,'TrackMinTrackProb'    : 0.000001  # unitless
                        ,'TrackMaxGhostProb'    : 0.3       # unitless
                        ,'TrackMinIPChi2'       : 16        # unitless
                        ,'GammaMinPT'           : 2000      # MeV
                        ,'D0MinM'               : 1600      # MeV
                        ,'D0MaxM'               : 2100      # MeV
                        ,'D0MinVtxProb'         : 0.001     # unitless
                        ,'D0MaxIPChi2'          : 9         # unitless
                        ,'D0MinDIRA'            : 0.9999    # unitless
                        ,'D0MinVVDChi2'         : 64        # unitless
                        ,'SoftPionMaxIPChi2'    : 16        # unitless
                        ,'Dst_PT_MIN'           : 4000      # MeV
                        ,'Dst_dAM_MIN'          : 95.421    # MeV
                        ,'Dst_dAM_MAX'          : 195.421   # MeV
                        ,'Dst_dM_MIN'           : 135.421   # MeV
                        ,'Dst_dM_MAX'           : 155.421   # MeV
                        ,'Dst_VCHI2PDOF_MAX'    : 9         # unitless
                        ,'Dst_VVDCHI2_MAX'      : 16        # unitless
                        ,'Dst_VIPCHI2_MAX'      : 9         # unitless
                        ,'Dst2D0PiLinePrescale'    : 1      # unitless
                        ,'Dst2D0PiLinePostscale'   : 1      # unitless

                        ,'EtaMinPT'                   : 1500   # MeV
                        ,'EtaMassMin'                 : 450    # MeV
                        ,'EtaMassMax'                 : 650    # MeV
                        ,'Dst2D0etaPiLinePrescale'    : 1      # unitless
                        ,'Dst2D0etaPiLinePostscale'   : 1      # unitless
                      },
    'STREAMS'     : ['CharmCompleteEvent']
    }

class StrippingDst2D0PiConf(LineBuilder) :

    __configuration_keys__ = (  'TrackMinPT'
                               ,'TrackMinTrackProb'
                               ,'TrackMaxGhostProb'
                               ,'TrackMinIPChi2'
                               ,'GammaMinPT'
                               ,'D0MinM'
                               ,'D0MaxM'
                               ,'D0MinVtxProb'
                               ,'D0MaxIPChi2'
                               ,'D0MinDIRA'
                               ,'D0MinVVDChi2'
                               ,'SoftPionMaxIPChi2'
                               ,'Dst_PT_MIN'
                               ,'Dst_dAM_MIN'
                               ,'Dst_dAM_MAX'
                               ,'Dst_dM_MIN'
                               ,'Dst_dM_MAX'
                               ,'Dst_VCHI2PDOF_MAX'
                               ,'Dst_VVDCHI2_MAX'
                               ,'Dst_VIPCHI2_MAX'
                               ,'Dst2D0PiLinePrescale'
                               ,'Dst2D0PiLinePostscale'
                               ,'EtaMinPT'
                               ,'EtaMassMin'
                               ,'EtaMassMax'
                               ,'Dst2D0etaPiLinePrescale'
                               ,'Dst2D0etaPiLinePostscale'
                               )

##############################################################
    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        myPions       = StdTightPions
        mySoftPions   = StdNoPIDsPions
        myKaons       = StdTightKaons
        myGammas      = StdLooseAllPhotons
        myEtas        = StdLooseEta2gg

        #---------------------------------------
        # D -> HHGamma selections
        self.seld0 = makeD02KPiGamma( 'D0_' + name ,
                                      config,
                                      DecayDescriptor = '[D0 -> K- pi+ gamma]cc',
                                      inputSel = [myKaons, myPions, myGammas]
                                      )

        #---------------------------------------
        # Dst -> DPi selections
        self.seldst = makeDst2D0Pi( 'Dst_' + name ,
                                    config,
                                    DecayDescriptor = '[D*(2010)+ -> D0 pi+]cc',
                                    inputSel = [self.seld0, mySoftPions]
                                    )

        #---------------------------------------
        # Stripping lines
        self.Dst2D0Pi_line = StrippingLine(name %locals()['config'],
                                           prescale = config['Dst2D0PiLinePrescale'],
                                           postscale = config['Dst2D0PiLinePostscale'],
                                           RequiredRawEvents = ["Calo"],
                                           selection = self.seldst
                                           )
        # register lines
        self.registerLine(self.Dst2D0Pi_line)

        #---------------------------------------
        # D -> HHEta selections
        self.seld0eta = makeD02KPiEta( 'D0eta_' + name ,
                                       config,
                                       DecayDescriptor = '[D0 -> K- pi+ eta]cc',
                                       inputSel = [myKaons, myPions, myEtas]
                                       )

        #---------------------------------------
        # Dst -> DPi selections
        self.seldsteta = makeDst2D0Pi( 'Dsteta_' + name ,
                                       config,
                                       DecayDescriptor = '[D*(2010)+ -> D0 pi+]cc',
                                       inputSel = [self.seld0eta, mySoftPions]
                                       )

        #---------------------------------------
        # Stripping lines
        self.Dst2D0etaPi_line = StrippingLine(name %locals()['config'] + "_D02KPiEta",
                                              prescale = config['Dst2D0etaPiLinePrescale'],
                                              postscale = config['Dst2D0etaPiLinePostscale'],
                                              RequiredRawEvents = ["Calo"],
                                              selection = self.seldsteta
                                              )
        # register lines
        self.registerLine(self.Dst2D0etaPi_line)

##############################################################
def makeD02KPiGamma( name,
                     config,
                     DecayDescriptor,
                     inputSel
                     ) :

    _TrackCuts    = "(PT>%(TrackMinPT)s *MeV) & (TRPCHI2>%(TrackMinTrackProb)s) & (TRGHOSTPROB<%(TrackMaxGhostProb)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPChi2)s)" %locals()['config']    
    _gammaCuts    = "(PT>%(GammaMinPT)s *MeV)" %locals()['config']
    _daughterCuts = { 'K-' : _TrackCuts, 'pi+' : _TrackCuts, 'gamma' : _gammaCuts }
    _combCuts     = "(AM>%(D0MinM)s *MeV) & (AM<%(D0MaxM)s *MeV)" % locals()['config']
    _motherCuts   = "(VFASPF(VPCHI2)>%(D0MinVtxProb)s) & (BPVVDCHI2>%(D0MinVVDChi2)s) & (BPVIPCHI2()<%(D0MaxIPChi2)s) & (BPVDIRA>%(D0MinDIRA)s)" % locals()['config']

    _D = CombineParticles( DecayDescriptor = DecayDescriptor,
                           MotherCut = _motherCuts,
                           CombinationCut = _combCuts,
                           DaughtersCuts = _daughterCuts
                           )

    return Selection( name+'Sel',
                      Algorithm = _D,
                      RequiredSelections = inputSel
                      )
##############################################################
def makeD02KPiEta( name,
                   config,
                   DecayDescriptor,
                   inputSel
                   ) :

    _TrackCuts    = "(PT>%(TrackMinPT)s *MeV) & (TRPCHI2>%(TrackMinTrackProb)s) & (TRGHOSTPROB<%(TrackMaxGhostProb)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPChi2)s)" %locals()['config']    
    _etaCuts    = "(PT>%(EtaMinPT)s *MeV) & (in_range(%(EtaMassMin)s,M,%(EtaMassMax)s))" %locals()['config']
    _daughterCuts = { 'K-' : _TrackCuts, 'pi+' : _TrackCuts, 'eta' : _etaCuts }
    _combCuts     = "(AM>%(D0MinM)s *MeV) & (AM<%(D0MaxM)s *MeV)" % locals()['config']
    _motherCuts   = "(VFASPF(VPCHI2)>%(D0MinVtxProb)s) & (BPVVDCHI2>%(D0MinVVDChi2)s) & (BPVIPCHI2()<%(D0MaxIPChi2)s) & (BPVDIRA>%(D0MinDIRA)s)" % locals()['config']

    _D = CombineParticles( DecayDescriptor = DecayDescriptor,
                           MotherCut = _motherCuts,
                           CombinationCut = _combCuts,
                           DaughtersCuts = _daughterCuts
                           )

    return Selection( name+'Sel',
                      Algorithm = _D,
                      RequiredSelections = inputSel
                      )
##############################################################
def makeDst2D0Pi( name,
                  config,
                  DecayDescriptor,
                  inputSel
                  ) :

    _SoftPionCuts = "(MIPCHI2DV(PRIMARY)<%(SoftPionMaxIPChi2)s)" %locals()['config']
    _daughterCuts = { 'pi+' : _SoftPionCuts }
    _combCuts     = "(in_range( %(Dst_dAM_MIN)s *MeV, (AM - AM1), %(Dst_dAM_MAX)s *MeV))" % locals()['config']
    _motherCuts   = "(PT > %(Dst_PT_MIN)s) & (VFASPF(VCHI2PDOF) < %(Dst_VCHI2PDOF_MAX)s) & (BPVVDCHI2 < %(Dst_VVDCHI2_MAX)s) & (BPVIPCHI2() < %(Dst_VIPCHI2_MAX)s) & (in_range( %(Dst_dM_MIN)s *MeV, (M - M1), %(Dst_dM_MAX)s *MeV))" % locals()['config']

    _Dst = CombineParticles( DecayDescriptor = DecayDescriptor,
                             MotherCut = _motherCuts,
                             CombinationCut = _combCuts,
                             DaughtersCuts = _daughterCuts
                             )

    return Selection( name+'Sel',
                      Algorithm = _Dst,
                      RequiredSelections = inputSel
                      )
##############################################################
