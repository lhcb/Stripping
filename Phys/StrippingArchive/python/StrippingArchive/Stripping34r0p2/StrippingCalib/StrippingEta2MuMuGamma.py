###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = 'Max Chefdeville'
__date__ = '19/03/2021'
__version__ = '$Revision: 1.1 $'

'''
Stripping selection for eta -> mu+ mu- gamma where the photon is reconstructed as an e+e- pair
'''
####################################################################
# Stripping selection for eta -> (KS0 -> mu+ mu-) gamma
#  line intended to control radiative decays with converted photons
####################################################################

__all__ = ('StrippingEta2MuMuGammaConf',
           'makeMuMu',
           'makeMuMuGamma',
           'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdTightPions,StdTightKaons,StdLooseAllPhotons,StdNoPIDsPions, StdLooseEta2gg
from StandardParticles import StdAllLooseGammaDD, StdAllLooseGammaLL, StdAllLooseMuons

default_config = {
    'NAME'        : 'Eta2MuMuGamma',
    'WGs'         : ['Calib'],
    'BUILDERTYPE' : 'StrippingEta2MuMuGammaConf',
    'CONFIG'      : { 'MuProbNNmu'         : 0.2    # MeV
                     ,'MuPT'               : 500    # MeV                     
                     ,'MuIPChi2'           : 6      # unitless
                     ,'FDChi2'             : 45     # unitless
                     ,'K0_PT'              : 1000   # unitless
                     ,'GPT'                : 500    # MeV
                     ,'K0MassMin'          : 0      # MeV
                     ,'K0MassMax'          : 1000   # MeV
                     ,'EtaMassMin'         : 0      # MeV
                     ,'EtaMassMax'         : 1000   # MeV
                     ,'MMG_LinePrescale'   : 1      # unitless
                     ,'MMG_LinePostscale'  : 1      # unitless                         
                     },
    'STREAMS'     : ['CharmCompleteEvent']
    }

class StrippingEta2MuMuGammaConf(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        myMuons       = StdAllLooseMuons
        myGammaDD     = StdAllLooseGammaDD
        myGammaLL     = StdAllLooseGammaLL        

        #---------------------------------------        
        # K0 -> MuMu selections
        self.selK0 = makeMuMu( 'K0_' + name ,
                               config,
                               DecayDescriptor = 'KS0 -> mu+ mu-',
                               inputSel = [myMuons]
                               )

        #---------------------------------------        
        # eta -> K0 GammaDD selections
        self.selEtaDD = makeMuMuGamma( 'EtaDD_' + name ,
                                       config,
                                       DecayDescriptor = 'eta -> KS0 gamma',
                                       inputSel = [self.selK0, myGammaDD]
                                       )

        # Stripping lines
        self.MMGDD_line = StrippingLine(name %locals()['config'] + "_DD",
                                        prescale = config['MMG_LinePrescale'],
                                        postscale = config['MMG_LinePostscale'],
                                        selection = self.selEtaDD
                                        )
        # register lines
        self.registerLine(self.MMGDD_line)

        #---------------------------------------
        # eta -> K0 GammaLL selections
        self.selEtaLL = makeMuMuGamma( 'EtaLL_' + name ,
                                       config,
                                       DecayDescriptor = 'eta -> KS0 gamma',
                                       inputSel = [self.selK0, myGammaLL]
                                       )

        self.MMGLL_line = StrippingLine(name %locals()['config'] + "_LL",
                                        prescale = config['MMG_LinePrescale'],
                                        postscale = config['MMG_LinePostscale'],
                                        selection = self.selEtaLL
                                        )
        # register lines
        self.registerLine(self.MMGLL_line)

def makeMuMu( name,
              config,
              DecayDescriptor,
              inputSel
              ) :

    _TrackCuts    = "(PT > %(MuPT)s *MeV) & (PROBNNmu > %(MuProbNNmu)s) & (MIPCHI2DV(PRIMARY) < %(MuIPChi2)s)" %locals()['config']
    _daughterCuts = { 'mu-' : _TrackCuts, 'mu+' : _TrackCuts}
    _combCuts     = "(AM > %(K0MassMin)s *MeV) & (AM<%(K0MassMax)s *MeV) & (APT>%(K0_PT)s *MeV)" % locals()['config']
    _motherCuts   = "(BPVVDCHI2<%(FDChi2)s)" % locals()['config']

    _K0 = CombineParticles( DecayDescriptor = DecayDescriptor,
                            MotherCut = _motherCuts,
                            CombinationCut = _combCuts,
                            DaughtersCuts = _daughterCuts
                            )

    return Selection( name+'Sel',
                      Algorithm = _K0,
                      RequiredSelections = inputSel
                      )

def makeMuMuGamma( name,
             config,
             DecayDescriptor,
             inputSel
             ) :

    _GammaCuts = "(PT > %(GPT)s)" %locals()['config']
    _daughterCuts = { 'gamma' : _GammaCuts }
    _combCuts     = "(AM > %(EtaMassMin)s *MeV) & (AM<%(EtaMassMax)s *MeV)" % locals()['config']
    _motherCuts   = "ALL" % locals()['config']

    _MMG = CombineParticles( DecayDescriptor = DecayDescriptor,
                             MotherCut = _motherCuts,
                             CombinationCut = _combCuts,
                             DaughtersCuts = _daughterCuts
                             )

    return Selection( name+'Sel',
                      Algorithm = _MMG,
                      RequiredSelections = inputSel
                      )
