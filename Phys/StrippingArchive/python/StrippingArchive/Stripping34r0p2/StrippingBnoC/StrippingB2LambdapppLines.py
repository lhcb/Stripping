###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of B+->Lambdappp stripping Selections and StrippingLines.
Provides functions to build Lambda0->DD, Lambda0->LL, and Lambda0->LD selections.
Stripping20 with an inclusive approach for B+->Lambdappp modes.
Provides class B2LambdapppLinesConf, which constructs the Selections and StrippingLines
given a configuration dictionary.
Exported symbols (use python help!):
   - B2LambdapppLinesConf
"""

__author__ = ["La Wang"]
__date__ = '05/02/2021'
__version__ = 'Stripping29r2p2'
__all__ = 'B2LambdapppLinesConf'

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

from StandardParticles import StdLooseProtons as Protons

'''
StrippingReport                                                INFO Event 160000, Good event 160000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |_StrippingGlobal_                                            |  0.3256|       521|       |   9.091|
 |_StrippingSequenceStreamBhadron_                             |  0.3256|       521|       |   9.074|
 |!StrippingB2LambdapppDDLine                                  |  0.1862|       298|  1.846|   5.646|
 |!StrippingB2LambdapppLLLine                                  |  0.0356|        57|  1.912|   1.082|
 |!StrippingB2LambdapppLDLine                                  |  0.0063|        10|  1.900|   0.575|
 |!StrippingB2LambdapppDDSSLine                                |  0.1212|       194|  1.804|   0.114|
 |!StrippingB2LambdapppLLSSLine                                |  0.0244|        39|  1.744|   0.074|
 |!StrippingB2LambdapppLDSSLine                                |  0.0044|         7|  1.286|   0.073|
 |!StrippingB2LambdapppDDLine_TIMING                           |  0.1862|       298|  1.846|   0.102|
 |!StrippingB2LambdapppLLLine_TIMING                           |  0.0356|        57|  1.912|   0.065|
 |!StrippingB2LambdapppLDLine_TIMING                           |  0.0063|        10|  1.900|   0.065|
 |!StrippingB2LambdapppDDSSLine_TIMING                         |  0.1212|       194|  1.804|   0.090|
 |!StrippingB2LambdapppLLSSLine_TIMING                         |  0.0244|        39|  1.744|   0.062|
 |!StrippingB2LambdapppLDSSLine_TIMING                         |  0.0044|         7|  1.286|   0.059|
'''

default_config = {
    'NAME' : 'B2Lambdappp',
    'WGs'  : ['BnoC'],
    'BUILDERTYPE' : 'B2LambdapppLinesConf',
    'CONFIG' : {'Trk_Chi2'                 : 3.0,    
                'Trk_GhostProb'            : 0.5,     
                'Lambda_DD_MassWindow'     : 20.0,    
                'Lambda_DD_VtxChi2'        : 9.0,     
                'Lambda_DD_FDChi2'         : 50.0,    
                'Lambda_DD_FD'             : 300.0,   
                'Lambda_DD_Pmin'           : 5000.0,  
                'Lambda_LL_MassWindow'     : 20.0,    
                'Lambda_LL_VtxChi2'        : 9.0,     
                'Lambda_LL_FDChi2'         : 0.0,     
                'Lambda_LD_MassWindow'     : 25.0,    
                'Lambda_LD_VtxChi2'        : 16.0,    
                'Lambda_LD_FDChi2'         : 50.0,    
                'Lambda_LD_FD'             : 300.0,   
                'Lambda_LD_Pmin'           : 5000.0,  
                'B0_Mlow'                  : 779.0,   
                'B0_Mhigh'                 : 1921.0,   
                'B0_APTmin'                : 1000.0,  
                'B0_PTmin'                 : 1050,    
                'B0Daug_MedPT_PT'          : 450.0,   
                'B0Daug_MaxPT_IP'          : 0.05,   
                'B0Daug_DD_maxDocaChi2'    : 16.0,
                'B0Daug_LL_maxDocaChi2'    : 5.0,
                'B0Daug_LD_maxDocaChi2'    : 5.0,
                'B0Daug_DD_PTsum'          : 2000.0,
                'B0Daug_LL_PTsum'          : 3000.0,
                'B0Daug_LD_PTsum'          : 4200.0,
                'B0_VtxChi2'               : 16.0,
                'B0_Dira'               : 0.9990,
                'B0_DD_IPCHI2wrtPV'        : 25.0,
                'B0_LL_IPCHI2wrtPV'        : 25.0,
                'B0_LD_IPCHI2wrtPV'        : 15.0,
                'B0_FDwrtPV'               : 0.8,
                'B0_DD_FDChi2'             : 0.5,
                'B0_LL_FDChi2'             : 0.5,
                'B0_LD_FDChi2'             : 30.0,
                'GEC_MaxTracks'            : 250,
                # 2012 Triggers
                #'HLT1Dec'                  : 'Hlt1TrackAllL0Decision',
                #'HLT2Dec'                  : 'Hlt2Topo[234]Body.*Decision',
                # 2015 Triggers
                #'HLT1Dec'                  : 'Hlt1(Two)?TrackMVADecision',
                #'HLT2Dec'                  : 'Hlt2Topo[234]BodyDecision',
                'Prescale'                 : 1.0,
                'Postscale'                : 1.0,
                'RelatedInfoTools' : [    { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 1.7, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar17'
                                            }, 
                                          { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 1.5, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar15'
                                            }, 
                                          { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 1.0, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar10'
                                            },
                                          { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 0.8, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar08'
                                            },
                                          { "Type" : "RelInfoVertexIsolation",
                                            "Location" : "VtxIsolationVar"
                                            }
                                          ]
                },
    'STREAMS' : ['Bhadron']
    }

class B2LambdapppLinesConf(LineBuilder) :
    """
    Builder of B+ ->Lambdappp stripping Selection and StrippingLine.
    Constructs B+ -> Lambda p+ p+ p~- Selections and StrippingLines from a configuration dictionary.
    Usage:
    >>> config = { .... }
    >>> B2LambdapppLinesConf = B2LambdapppLinesConf('B2LambdapppTest',config)
    >>> B0LambdapppLines = B2LambdapppLinesConf.lines
    >>> for line in line :
    >>>  print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selLambda2DD           : Lambda0 -> Down Down Selection object
    selLambda2LL           : Lambda0 -> Long Long Selection object
    selLambda2LD           : Lambda0 -> Long Down Selection object

    selB2LambdapppDD           : B+ -> Lambda0(DD) p+ p+ p~- Selection object
    selB2LambdapppLL           : B+ -> Lambda0(LL) p+ p+ p~- Selection object
    selB2LambdapppLD           : B+ -> Lambda0(LD) p+ p+ p~- Selection object

    B2LambdapppDDLine             : StrippingLine made out of selB2LambdapppDD
    B2LambdapppLLLine             : StrippingLine made out of selB2LambdapppLL
    B2LambdapppLDLine             : StrippingLine made out of selB2LambdapppLD

    lines   : List of lines, [B2LambdapppDDLine,B2LambdapppLLLine,B2LambdapppLDLine]

    Exports as class data member:
    B2LambdapppLinesConf.__configuration_keys__ : List of required configuration parameters.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        GECCode = {'Code' : "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %s)" % config['GEC_MaxTracks'],
                   'Preambulo' : ["from LoKiTracks.decorators import *"]}

        #self.hlt1Filter = {'Code' : "HLT_PASS_RE('%s')" % config['HLT1Dec'],
        #                   'Preambulo' : ["from LoKiCore.functions import *"]}
        #self.hlt2Filter = {'Code' : "HLT_PASS_RE('%s')" % config['HLT2Dec'],
        #                   'Preambulo' : ["from LoKiCore.functions import *"]}

        self.protons   = Protons
        
        self.makeLambda2DD( 'Lambda0DD', config )
        self.makeLambda2LL( 'Lambda0LL', config )
        self.makeLambda2LD( 'Lambda0LD', config )


        namesSelections = [ (name + 'DD', self.makeB2LambdapppDD(name + 'DD', config)),
                            (name + 'LL', self.makeB2LambdapppLL(name + 'LL', config)),
                            (name + 'LD', self.makeB2LambdapppLD(name + 'LD', config)),

                            (name + 'DDSS', self.makeB2LambdapppDD(name + 'DDSS', config)),
                            (name + 'LLSS', self.makeB2LambdapppLL(name + 'LLSS', config)),
                            (name + 'LDSS', self.makeB2LambdapppLD(name + 'LDSS', config)),
                          ]

        # make lines
        
        for selName, sel in namesSelections:

            extra = {}

            #if 'SS' in selName:
                #extra['HLT1'] = self.hlt1Filter
                #extra['HLT2'] = self.hlt2Filter

            line = StrippingLine(selName + 'Line',
                                 selection = sel,
                                 prescale = config['Prescale'],
                                 postscale = config['Postscale'],
                                 RelatedInfoTools = config['RelatedInfoTools'], 
                                 FILTER = GECCode,
                                 **extra) 

            self.registerLine(line)

    def makeLambda2DD( self, name, config ) :
        # define all the cuts
        _massCut          = "(ADMASS('Lambda0')<%s*MeV)"      % config['Lambda_DD_MassWindow']
        _vtxCut           = "(VFASPF(VCHI2)<%s)   "           % config['Lambda_DD_VtxChi2']
        _fdChi2Cut        = "(BPVVDCHI2>%s)"                  % config['Lambda_DD_FDChi2']
        _momCut           = "(P>%s*MeV)"                      % config['Lambda_DD_Pmin']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))"  % config['Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))"  % config['Trk_GhostProb']

        _allCuts = _momCut
        _allCuts += '&'+_massCut
        _allCuts += '&'+_vtxCut 
        _allCuts += '&'+_fdChi2Cut
        #_allCuts += '&'+_trkGhostProbCut1
        #_allCuts += '&'+_trkGhostProbCut2

        # get the Lambda0's to filter
        _stdLambdaDD = DataOnDemand(Location = "Phys/StdLooseLambdaDD/Particles")
        
        # make the filter
        _filterLambdaDD = FilterDesktop( Code = _allCuts )

        # make and store the Selection object
        self.selLambda2DD = Selection( name, Algorithm = _filterLambdaDD, RequiredSelections = [_stdLambdaDD] )

        return self.selLambda2DD

    def makeLambda2LL( self, name, config ) : 
        # define all the cuts
        _massCut    = "(ADMASS('Lambda0')<%s*MeV)"           % config['Lambda_LL_MassWindow']
        _vtxCut     = "(VFASPF(VCHI2)<%s)"                   % config['Lambda_LL_VtxChi2']
        _trkChi2Cut1 = "(CHILDCUT((TRCHI2DOF<%s),1))"        % config['Trk_Chi2']
        _trkChi2Cut2 = "(CHILDCUT((TRCHI2DOF<%s),2))"        % config['Trk_Chi2']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))" % config['Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))" % config['Trk_GhostProb']

        _allCuts = _massCut
        _allCuts += '&'+_trkChi2Cut1
        _allCuts += '&'+_trkChi2Cut2
        _allCuts += '&'+_vtxCut
        _allCuts += '&'+_trkGhostProbCut1
        _allCuts += '&'+_trkGhostProbCut2

        # get the Lambda's to filter
        _stdLambdaLL = DataOnDemand(Location = "Phys/StdLooseLambdaLL/Particles")

        # make the filter
        _filterLambdaLL = FilterDesktop( Code = _allCuts )
        
        # make and store the Selection object
        self.selLambda2LL = Selection( name, Algorithm = _filterLambdaLL, RequiredSelections = [_stdLambdaLL] )

        return self.selLambda2LL

    def makeLambda2LD( self, name, config ) :
        # define all the cuts
        _massCut          = "(ADMASS('Lambda0')<%s*MeV)"      % config['Lambda_DD_MassWindow']
        _vtxCut           = "(VFASPF(VCHI2)<%s)   "           % config['Lambda_DD_VtxChi2']
        _fdChi2Cut        = "(BPVVDCHI2>%s)"                  % config['Lambda_DD_FDChi2']
        _momCut           = "(P>%s*MeV)"                      % config['Lambda_DD_Pmin']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))"  % config['Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))"  % config['Trk_GhostProb']

        _allCuts = _momCut
        _allCuts += '&'+_massCut
        _allCuts += '&'+_vtxCut
        _allCuts += '&'+_fdChi2Cut
        #_allCuts += '&'+_trkGhostProbCut1
        #_allCuts += '&'+_trkGhostProbCut2

        # get the Lambda0's to filter
        _stdLambdaLD = DataOnDemand(Location = "Phys/StdLooseLambdaLD/Particles")
        
        # make the filter
        _filterLambdaLD = FilterDesktop( Code = _allCuts )

        # make and store the Selection object
        self.selLambda2LD = Selection( name, Algorithm = _filterLambdaLD, RequiredSelections = [_stdLambdaLD] )

        return self.selLambda2LD

    def makeB2LambdapppDD( self, name, config ) :
        """
        Create and store a B+ ->Lambda0(DD) p+ p+ p~- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5279-%s)*MeV)"               % config['B0_Mlow']
        _massCutHigh    = "(AM<(5279+%s)*MeV)"               % config['B0_Mhigh']
        _aptCut         = "(APT>%s*MeV)"                     % config['B0_APTmin']
        _daugMedPtCut   = "(ANUM(PT>%s*MeV)>=2)"             % config['B0Daug_MedPT_PT']
        _daugMaxPtIPCut = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)" % config['B0Daug_MaxPT_IP']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))"            % config['B0Daug_DD_maxDocaChi2']
        _daugPtSumCut   = "((APT1+APT2+APT3+APT4)>%s*MeV)"        % config['B0Daug_DD_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut    
        _combCuts += '&'+_daugMedPtCut    
        _combCuts += '&'+_massCutLow     
        _combCuts += '&'+_massCutHigh     
        #_combCuts += '&'+_daugMaxPtIPCut  # does not work properly 
        _combCuts += '&'+_maxDocaChi2Cut  

        _ptCut      = "(PT>%s*MeV)"                    % config['B0_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['B0_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['B0_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B0_DD_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B0_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['B0_DD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut 
        _motherCuts += '&'+_diraCut  
        _motherCuts += '&'+_ipChi2Cut 
        _motherCuts += '&'+_fdCut #lookhere
        _motherCuts += '&'+_fdChi2Cut 

        _B0 = CombineParticles()

        if 'SS' in name: # Same sign
            _B0.DecayDescriptors = [ "[B+ -> p+ p+ p+ Lambda0]cc", "[B+ -> p~- p~- p~- Lambda0]cc"]
        else:
            _B0.DecayDescriptors = [ "[B+ -> Lambda0 p+ p+ p~-]cc" ]

        _trkGhostProbCut  = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut       = "(TRCHI2DOF<%s)"   % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _B0.DaughtersCuts = { "p+" : _daughtersCuts }
        _B0.CombinationCut =  _combCuts
        _B0.MotherCut = _motherCuts
        _B0.ReFitPVs = True
        
        _B0Conf = _B0.configurable(name + '_combined')

        if 'SS' in name: # Same sign
            self.selB2LambdapppDDSS = Selection (name, Algorithm = _B0Conf, RequiredSelections = [self.selLambda2DD, self.protons ])
            return self.selB2LambdapppDDSS
        else:
            self.selB2LambdapppDD = Selection (name, Algorithm = _B0Conf, RequiredSelections = [self.selLambda2DD, self.protons ])
            return self.selB2LambdapppDD

    def makeB2LambdapppLL( self, name, config ) :
        """
        Create and store a B+ -> Lambda0(LL) p+ p+ p~- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5279-%s)*MeV)"               % config['B0_Mlow']
        _massCutHigh    = "(AM<(5279+%s)*MeV)"               % config['B0_Mhigh']
        _aptCut         = "(APT>%s*MeV)"                     % config['B0_APTmin']
        _daugMedPtCut   = "(ANUM(PT>%s*MeV)>=2)"             % config['B0Daug_MedPT_PT']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))"            % config['B0Daug_LL_maxDocaChi2']
        _daugPtSumCut   = "((APT1+APT2+APT3+APT4)>%s*MeV)"        % config['B0Daug_LL_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_daugMedPtCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        _combCuts += '&'+_maxDocaChi2Cut
        
        _ptCut      = "(PT>%s*MeV)"                    % config['B0_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['B0_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['B0_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B0_LL_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B0_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['B0_LL_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdChi2Cut
        _motherCuts += '&'+_fdCut

        _B0 = CombineParticles()

        if 'SS' in name: # Same sign
            _B0.DecayDescriptors = [ "[B+ -> p+ p+ p+ Lambda0]cc", "[B+ -> p~- p~- p~- Lambda0]cc"]
        else:
            _B0.DecayDescriptors = [ "[B+ -> Lambda0 p+ p+ p~-]cc" ]

        _trkGhostProbCut  = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut       = "(TRCHI2DOF<%s)"   % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _B0.DaughtersCuts = { "p+" : _daughtersCuts }
        _B0.CombinationCut = _combCuts
        _B0.MotherCut = _motherCuts
        _B0.ReFitPVs = True
        
        _B0Conf = _B0.configurable(name + '_combined')

        if 'SS' in name: # Same sign
            self.selB2LambdapppLLSS = Selection (name, Algorithm = _B0Conf, RequiredSelections = [self.selLambda2LL, self.protons  ])
            return self.selB2LambdapppLLSS
        else:
            self.selB2LambdapppLL = Selection (name, Algorithm = _B0Conf, RequiredSelections = [self.selLambda2LL, self.protons  ])
            return self.selB2LambdapppLL

    def makeB2LambdapppLD( self, name, config ) :
        """
        Create and store a B+ ->Lambda0(LD) p+ p+ p~- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5279-%s)*MeV)"               % config['B0_Mlow']
        _massCutHigh    = "(AM<(5279+%s)*MeV)"               % config['B0_Mhigh']
        _aptCut         = "(APT>%s*MeV)"                     % config['B0_APTmin']
        _daugMedPtCut   = "(ANUM(PT>%s*MeV)>=2)"             % config['B0Daug_MedPT_PT']
        _daugMaxPtIPCut = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)" % config['B0Daug_MaxPT_IP']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))"            % config['B0Daug_LD_maxDocaChi2']
        _daugPtSumCut   = "((APT1+APT2+APT3+APT4)>%s*MeV)"        % config['B0Daug_LD_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_daugMedPtCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        #_combCuts += '&'+_daugMaxPtIPCut
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut      = "(PT>%s*MeV)"                    % config['B0_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['B0_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['B0_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B0_LD_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B0_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['B0_LD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdChi2Cut

        _B0 = CombineParticles()

        if 'SS' in name: # Same sign
            _B0.DecayDescriptors = [ "[B+ -> p+ p+ p+ Lambda0]cc", "[B+ -> p~- p~- p~- Lambda0]cc"]
        else:
            _B0.DecayDescriptors = [ "[B+ -> Lambda0 p+ p+ p~-]cc" ]

        _trkGhostProbCut  = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut       = "(TRCHI2DOF<%s)"   % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _B0.DaughtersCuts = { "p+" : _daughtersCuts }
        _B0.CombinationCut = _combCuts
        _B0.MotherCut = _motherCuts
        _B0.ReFitPVs = True
        
        _B0Conf = _B0.configurable(name + '_combined')

        if 'SS' in name: # Same sign
            self.selB2LambdapppLDSS = Selection (name, Algorithm = _B0Conf, RequiredSelections = [self.selLambda2LD, self.protons ])
            return self.selB2LambdapppLDSS
        else:
            self.selB2LambdapppLD = Selection (name, Algorithm = _B0Conf, RequiredSelections = [self.selLambda2LD, self.protons ])
            return self.selB2LambdapppLD


