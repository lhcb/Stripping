###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = 'P. Owen, T.Mombacher'
__date__ = '11/03/2021'
__version__ = '$Revision: 2.0 $'

__all__ = ( 'B23MuNuConf', 'default_config' )

"""
Stripping selection for B to three muons and a neutrino.
"""

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import  CombineParticles, FilterDesktop

from PhysSelPython.Wrappers import Selection, AutomaticData, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from LHCbKernel.Configuration import *  #check if needed
from Configurables import SubstitutePID
from Configurables import SubPIDMMFilter


#################
#
#  Define Cuts here
#
#################

default_config = {
    'NAME'        : 'B23MuNu',
    'WGs'         : ['Semileptonic'],
    'BUILDERTYPE' : 'B23MuNuConf',
    'CONFIG'      : {
      #  (dimu) cuts
      'FlightChi2'      :    30.0,
      'DIRA'            :   0.99,
      'BPT'             :  2000.0,
      'VertexCHI2'      :     4.0,
      'LOWERMASS'       :     0.0, # MeV
      'UPPERMASS'       :  7500.0, # MeV
      'CORRM_MIN'       :  2500.0, # MeV
      'CORRM_MAX'       : 10000.0, # MeV
      # Track cuts
      'Track_CHI2nDOF'      :    3.0,
      'Track_GhostProb'     :    0.35,  

      # Muon cuts
      'Muon_MinIPCHI2'   :    9.0,
      'Muon_PIDmu'       :    0.0,
      'Muon_PIDmuK'      :    0.0,
      'Muon_PT'          :    0.0,

      # Electron cuts
      'Electron_PIDe'       :    2.0,
      'Electron_PIDeK'      :    0.0,
      'Electron_MinIPCHI2'  :    25.0,
      'Electron_PT'         :    200.0,

      # GEC
      'SpdMult'             :  900,

      'MisIDPrescale'       : 0.01,
    },
   'STREAMS'     : ['Semileptonic']   
}

defaultName = "B23MuNu"


class B23MuNuConf(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config) :


        LineBuilder.__init__(self, name, config)

        self.name = name
        

        self.TriMuCut = "(BPVCORRM > %(CORRM_MIN)s *MeV) & " \
                           "(BPVCORRM < %(CORRM_MAX)s *MeV) & " \
                                "(BPVDIRA > %(DIRA)s) & " \
                                "(PT > %(BPT)s) & " \
                                "(BPVVDCHI2 > %(FlightChi2)s) & " \
                                "(VFASPF(VCHI2/VDOF) < %(VertexCHI2)s) & " \
                                "(M > %(LOWERMASS)s) & " \
                                "(M < %(UPPERMASS)s) " %config

        self.TrackCuts = "(TRCHI2DOF < %(Track_CHI2nDOF)s) & (TRGHP < %(Track_GhostProb)s)" \
                         " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s) " \
                         " & (PT > %(Muon_PT)s)" %config

        self.TrackCutsElectron = "(TRCHI2DOF < %(Track_CHI2nDOF)s) & (TRGHP < %(Track_GhostProb)s)" \
                         " & (MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s) " \
                         " & (PT > %(Electron_PT)s)" %config

        
        self.MuonCut = self.TrackCuts + "& (PIDmu> %(Muon_PIDmu)s) & " \
                                        " (PIDmu-PIDK> %(Muon_PIDmuK)s)" %config


        self.ElectronCut = self.TrackCutsElectron + "& (PIDe> %(Electron_PIDe)s) & " \
                                        " (PIDe-PIDK> %(Electron_PIDeK)s)" %config


        self.Muons = self.__Muons__(config)
        self.Electrons = self.__Electrons__(config)
        self.FakeMuons = self.__FakeMuons__(config)
        self.FakeElectrons = self.__FakeElectrons__(config)
        self.Jpsi = self.__Jpsi__(config)
        self.Jpsiee = self.__Jpsiee__(config)
        self.Trimu = self.__Trimu__(config)
        self.Trie = self.__Trie__(config)
        self.MuMue = self.__MuMue__(config)
        self.Muee = self.__Muee__(config)
        self.FakeTrimu = self.__FakeTrimu__(config)
        self.FakeTrie = self.__FakeTrie__(config)
        self.FakeMuMue = self.__FakeMuMue__(config)
        self.FakeMuee = self.__FakeMuee__(config)

        RelInfoTools=[
                { "Type" : "RelInfoMuonIDPlus",
                    "Variables" : ["MU_BDT"],
                    "DaughterLocations"  : {
                    "[B+ -> ^l+ l+ [l-]CC ]CC" : "Muon1BDT",
                    "[B+ -> l+ ^l+ [l-]CC ]CC" : "Muon2BDT",
                    "[B+ -> l+ l+ ^[l-]CC ]CC" : "Muon3BDT",
                    }
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_first',
                    'Particles' : [0,1]
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_second',
                    'Particles' : [1,2]
                }
        ]        


        self.TriMu_line =  StrippingLine(
            self.name+"_TriMuLine",
            prescale = 1,
            FILTER = {
            'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" %config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
            ]
            },
            algos=[self.Trimu],
            RelatedInfoTools = RelInfoTools
            )

        self.Trie_line =  StrippingLine(
            self.name+"_TrieLine",
            prescale = 1,
            FILTER = {
            'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" %config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
            ]
            },
            algos=[self.Trie],
            RelatedInfoTools = RelInfoTools
            )

        # specify leptons to identify the muon candidate
        RelInfoTools=[
                { "Type" : "RelInfoMuonIDPlus",
                    "Variables" : ["MU_BDT"],
                    "DaughterLocations"  : {
                    "[B+ -> ^[e+]CC mu+ [mu-]CC ]CC" : "Muon1BDT",
                    "[B+ -> [e+]CC ^mu+ [mu-]CC ]CC" : "Muon2BDT",
                    "[B+ -> [e+]CC mu+ ^[mu-]CC ]CC" : "Muon3BDT"
                    }
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_first',
                    'Particles' : [0,1]
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_second',
                    'Particles' : [1,2]
                }
        ]        

        self.MuMue_line =  StrippingLine(
            self.name+"_MuMueLine",
            prescale = 1,
            FILTER = {
            'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" %config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
            ]
            },
            algos=[self.MuMue],
            RelatedInfoTools = RelInfoTools
            )

        # specify leptons to identify the muon candidate
        RelInfoTools=[
                { "Type" : "RelInfoMuonIDPlus",
                    "Variables" : ["MU_BDT"],
                    "DaughterLocations"  : {
                    "[B+ -> ^[mu+]CC e+ [e-]CC ]CC" : "Muon1BDT",
                    "[B+ -> [mu+]CC ^e+ [e-]CC ]CC" : "Muon2BDT",
                    "[B+ -> [mu+]CC e+ ^[e-]CC ]CC" : "Muon3BDT"
                    }
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_first',
                    'Particles' : [0,1]
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_second',
                    'Particles' : [1,2]
                }
        ]        


        self.Muee_line =  StrippingLine(
            self.name+"_MueeLine",
            prescale = 1,
            FILTER = {
            'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" %config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
            ]
            },
            algos=[self.Muee],
            RelatedInfoTools = RelInfoTools
            )


        RelInfoTools_fake=[
                { "Type" : "RelInfoMuonIDPlus",
                    "Variables" : ["MU_BDT"],
                    "DaughterLocations"  : {
                    "[B+ -> [J/psi(1S) -> ^l+ [l-]CC ]CC l+]CC" : "Muon1BDT",
                    "[B+ -> [J/psi(1S) -> l+ ^[l-]CC ]CC l+]CC" : "Muon2BDT",
                    "[B+ -> [J/psi(1S) -> l+ [l-]CC ]CC ^l+]CC" : "Muon3BDT",
                    }
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_first',
                    'Particles' : [1,2]
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_second',
                    'Particles' : [2,3]
                }
        ]        


        self.FakeTriMu_line =  StrippingLine(
            self.name+"_TriFakeMuLine",
            prescale = config['MisIDPrescale'],
            FILTER = {
            'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" %config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
            ]
            },
            algos=[self.FakeTrimu],
            RelatedInfoTools = RelInfoTools_fake
            )

        self.FakeTrie_line =  StrippingLine(
            self.name+"_TrieFakeLine",
            prescale = config['MisIDPrescale'],
            FILTER = {
            'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" %config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
            ]
            },
            algos=[self.FakeTrie],
            RelatedInfoTools = RelInfoTools_fake
            )

        # specify leptons to identify the muon candidate
        RelInfoTools_fake_mumue = RelInfoTools_fake
        # RelInfoTools_fake_mumue[0] = { "Type" : "RelInfoMuonIDPlus",
        #                     "Variables" : ["MU_BDT"],
        #                     "DaughterLocations"  : {
        #                     "[B+ -> [J/psi(1S) -> ^mu+ [mu-]CC ]CC e+]CC" : "Muon1BDT",
        #                     "[B+ -> [J/psi(1S) -> mu+ ^[mu-]CC ]CC e+]CC" : "Muon2BDT",
        #                     "[B+ -> [J/psi(1S) -> mu+ [mu-]CC ]CC ^e+]CC" : "Muon3BDT"}
        #                     }
        RelInfoTools_fake=[
                { "Type" : "RelInfoMuonIDPlus",
                    "Variables" : ["MU_BDT"],
                    "DaughterLocations"  : {
                    "[B+ -> [J/psi(1S) -> ^mu+ [mu-]CC ]CC e+]CC" : "Muon1BDT",
                    "[B+ -> [J/psi(1S) -> mu+ ^[mu-]CC ]CC e+]CC" : "Muon2BDT",
                    "[B+ -> [J/psi(1S) -> mu+ [mu-]CC ]CC ^e+]CC" : "Muon3BDT"
                    }
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_first',
                    'Particles' : [1,2]
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_second',
                    'Particles' : [2,3]
                }
        ]        


        self.FakeMuMue_line =  StrippingLine(
            self.name+"_MuMueFakeLine",
            prescale = config['MisIDPrescale'],
            FILTER = {
            'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" %config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
            ]
            },
            algos=[self.FakeMuMue],
            RelatedInfoTools = RelInfoTools_fake
            )

        # specify leptons to identify the muon candidate
        RelInfoTools_fake=[
                { "Type" : "RelInfoMuonIDPlus",
                    "Variables" : ["MU_BDT"],
                    "DaughterLocations"  : {
                    "[B+ -> [J/psi(1S) -> ^e+ [e-]CC ]CC mu+]CC" : "Muon1BDT",
                    "[B+ -> [J/psi(1S) -> e+ ^[e-]CC ]CC mu+]CC" : "Muon2BDT",
                    "[B+ -> [J/psi(1S) -> e+ [e-]CC ]CC ^mu+]CC" : "Muon3BDT"
                    }
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_first',
                    'Particles' : [1,2]
                },
                {'Type' : 'RelInfoTrackIsolationBDT2',
                    'Location' : 'TrackIsolationBDT2_second',
                    'Particles' : [2,3]
                }
        ]        


        self.FakeMuee_line =  StrippingLine(
            self.name+"_MueeFakeLine",
            prescale = config['MisIDPrescale'],
            FILTER = {
            'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" %config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
            ]
            },
            algos=[self.FakeMuee],
            RelatedInfoTools = RelInfoTools_fake
            )


        self.registerLine( self.TriMu_line )
        self.registerLine( self.Trie_line )
        self.registerLine( self.Muee_line )
        self.registerLine( self.MuMue_line )
        self.registerLine( self.FakeTriMu_line )
        self.registerLine( self.FakeTrie_line )
        self.registerLine( self.FakeMuMue_line )
        self.registerLine( self.FakeMuee_line )



    def __Muons__(self, conf):
        """
        Filter muons from StdAllLooseMuons
        """  
        from StandardParticles import StdAllLooseMuons
        _muons = StdAllLooseMuons
        _filter = FilterDesktop(Code = self.MuonCut)
        _sel = Selection("Selection_"+self.name+"_Muons",
                         RequiredSelections = [ _muons ] ,
                         Algorithm = _filter)
        return _sel

    def __Electrons__(self, conf):
        """
        Filter Electrons from StdLooseElectrons
        """  
        from StandardParticles import StdAllLooseElectrons
        _electrons = StdAllLooseElectrons
        _filter = FilterDesktop(Code = self.ElectronCut)
        _sel = Selection("Selection_"+self.name+"_Electrons",
                         RequiredSelections = [ _electrons ] ,
                         Algorithm = _filter)
        return _sel
 
        
    def __FakeMuons__(self, conf):
        """
        Filter muons from StdAllNoPIDsMuons
        """  
        from StandardParticles import StdAllNoPIDsMuons
        _fakemuons = StdAllNoPIDsMuons
        _filter = FilterDesktop(Code = self.TrackCuts)
        _sel = Selection("Selection_"+self.name+"_FakeMuons",
                         RequiredSelections = [ _fakemuons ] ,
                         Algorithm = _filter)
        return _sel

    def __FakeElectrons__(self, conf):
        """
        Filter electrons from StdAllNoPIDsElectrons
        """  
        from StandardParticles import StdAllNoPIDsElectrons
        _fakeelectrons = StdAllNoPIDsElectrons
        _filter = FilterDesktop(Code = self.TrackCutsElectron)
        _sel = Selection("Selection_"+self.name+"_FakeElectrons",
                         RequiredSelections = [ _fakeelectrons ] ,
                         Algorithm = _filter)
        return _sel


    def __Jpsi__(self, conf):
        """
        Creates Jpsi as proxy for dimuon
        """
        from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand, MergedSelection
        from  GaudiConfUtils.ConfigurableGenerators import CombineParticles
        #from Configurables import CombineParticles 

        CombineJpsi= CombineParticles(DecayDescriptors = ["J/psi(1S) -> mu+ mu-","[J/psi(1S) -> mu+ mu+]cc"],                       
                     MotherCut = "ALL")
        
        sel_name = "Jpsi"
        
        from PhysSelPython.Wrappers import Selection
        SelJpsi = Selection("Sel_" + self.name + "_Jpsi", Algorithm = CombineJpsi,
                              RequiredSelections = [ self.Muons ] )
        return SelJpsi

    def __Jpsiee__(self, conf):
        """
        Creates Jpsi as proxy for dielectron
        """
        from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand, MergedSelection
        from  GaudiConfUtils.ConfigurableGenerators import CombineParticles
        #from Configurables import CombineParticles 

        CombineJpsi= CombineParticles(DecayDescriptors = ["J/psi(1S) -> e+ e-","[J/psi(1S) -> e+ e+]cc"],                       
                     MotherCut = "ALL")
        
        sel_name = "Jpsi"
        
        from PhysSelPython.Wrappers import Selection
        SelJpsi = Selection("Sel_" + self.name + "_Jpsiee", Algorithm = CombineJpsi,
                              RequiredSelections = [ self.Electrons ] )
        return SelJpsi



    def __Trimu__(self, conf):
        '''
        Create trimuon
        '''
        from  GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptors = ["[B+ -> mu+ mu+ mu-]cc", "[B+ -> mu+ mu+ mu+]cc"]
        sel_name="TriMu"
        CombineTriMuon.MotherCut     = self.TriMuCut
        # choose

        from PhysSelPython.Wrappers import Selection
        SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name, 
                              Algorithm = CombineTriMuon,
                              RequiredSelections = [ self.Muons ] )
        return SelTriMuon
   
    def __Trie__(self, conf):
        '''
        Create trielectron
        '''
        from  GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptors = ["[B+ -> e+ e+ e-]cc", "[B+ -> e+ e+ e+]cc"]
        sel_name="Trie"
        CombineTriMuon.MotherCut     = self.TriMuCut
        # choose

        from PhysSelPython.Wrappers import Selection
        SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name, 
                              Algorithm = CombineTriMuon,
                              RequiredSelections = [ self.Electrons ] )
        return SelTriMuon


    def __MuMue__(self, conf):
        '''
        Create MuMue combination
        '''
        from  GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptors = ["[B+ -> e+ mu+ mu-]cc","[B+ -> mu+ mu+ e-]cc", "[B+ -> mu+ mu+ e+]cc"]
        sel_name="MuMue"
        CombineTriMuon.MotherCut     = self.TriMuCut
        # choose

        from PhysSelPython.Wrappers import Selection
        SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name, 
                              Algorithm = CombineTriMuon,
                              RequiredSelections = [ self.Muons, self.Electrons ] )
        return SelTriMuon


    def __Muee__(self, conf):
        '''
        Create Muee combination
        '''
        from  GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptors = ["[B+ -> mu+ e+ e-]cc", "[B+ -> e+ e+ mu-]cc", "[B+ -> mu+ e+ e+]cc"]
        sel_name="Muee"
        CombineTriMuon.MotherCut     = self.TriMuCut
        # choose

        from PhysSelPython.Wrappers import Selection
        SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name, 
                              Algorithm = CombineTriMuon,
                              RequiredSelections = [ self.Muons, self.Electrons ] )
        return SelTriMuon


    def __FakeTrimu__(self, conf):
        """
        Create fake trimuon
        """

        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) mu+]cc"
        sel_name="FakeTriMu"
        #CombineTriMuon.CombinationCut = self.TriMuLowQ2CombCut
        #["[B+ ->mu+ mu+ mu-]cc", "[B+ ->mu+ mu+ mu+]cc"]
        CombineTriMuon.MotherCut     = self.TriMuCut
        # choose

        from PhysSelPython.Wrappers import Selection
        #SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name, Algorithm = CombineTriMuon,
        #                      RequiredSelections = [self.Jpsi, self.FakeMuon ])
        SelTriMuon = Selection(sel_name,
                 Algorithm = CombineTriMuon,
                 RequiredSelections = [ self.FakeMuons, self.Jpsi ])


        return SelTriMuon

    def __FakeTrie__(self, conf):
        """
        Create fake trielectron
        """

        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) e+]cc"
        sel_name="FakeTrie"
        #CombineTriMuon.CombinationCut = self.TriMuLowQ2CombCut
        #["[B+ ->mu+ mu+ mu-]cc", "[B+ ->mu+ mu+ mu+]cc"]
        CombineTriMuon.MotherCut     = self.TriMuCut
        # choose

        from PhysSelPython.Wrappers import Selection
        #SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name, Algorithm = CombineTriMuon,
        #                      RequiredSelections = [self.Jpsi, self.FakeMuon ])
        SelTriMuon = Selection(sel_name,
                 Algorithm = CombineTriMuon,
                 RequiredSelections = [ self.FakeElectrons, self.Jpsiee ])


        return SelTriMuon

 
    def __FakeMuMue__(self, conf):
        """
        Create fake MuMue
        """

        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) e+]cc"
        sel_name="FakeMuMue"
        #CombineTriMuon.CombinationCut = self.TriMuLowQ2CombCut
        #["[B+ ->mu+ mu+ mu-]cc", "[B+ ->mu+ mu+ mu+]cc"]
        CombineTriMuon.MotherCut     = self.TriMuCut
        # choose

        from PhysSelPython.Wrappers import Selection
        #SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name, Algorithm = CombineTriMuon,
        #                      RequiredSelections = [self.Jpsi, self.FakeMuon ])
        SelTriMuon = Selection(sel_name,
                 Algorithm = CombineTriMuon,
                 RequiredSelections = [ self.FakeElectrons, self.Jpsi ])


        return SelTriMuon

    def __FakeMuee__(self, conf):
        """
        Create fake Muee
        """

        CombineTriMuon = CombineParticles()
        CombineTriMuon.DecayDescriptor = "[B+ -> J/psi(1S) mu+]cc"
        sel_name="FakeMuee"
        #CombineTriMuon.CombinationCut = self.TriMuLowQ2CombCut
        #["[B+ ->mu+ mu+ mu-]cc", "[B+ ->mu+ mu+ mu+]cc"]
        CombineTriMuon.MotherCut     = self.TriMuCut
        # choose

        from PhysSelPython.Wrappers import Selection
        #SelTriMuon = Selection("Sel_" + self.name + "_"+sel_name, Algorithm = CombineTriMuon,
        #                      RequiredSelections = [self.Jpsi, self.FakeMuon ])
        SelTriMuon = Selection(sel_name,
                 Algorithm = CombineTriMuon,
                 RequiredSelections = [ self.FakeMuons, self.Jpsiee ])


        return SelTriMuon
