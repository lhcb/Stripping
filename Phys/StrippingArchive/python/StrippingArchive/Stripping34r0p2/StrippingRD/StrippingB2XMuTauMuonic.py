###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for
    Bs to phi_3(1850)/phi(1020) mu tau
    Bd to Kstar mu tau
    Lb to p K mu tau
Same-sign combinations are included.
"""

__author__ = 'H. Tilquin'
__date__ = '03/06/2021'
__version__ = '$Revision: 0.0 $'

__all__ = ('B2XMuTauMuonicConf', 'default_config')

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop

from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME': 'B2XMuTauMuonic',
    'BUILDERTYPE': 'B2XMuTauMuonicConf',
    'CONFIG':
        {
            "Bs_Comb_MassHigh": 7250.0,
            "Bs_FlightChi2": 16,
            "Bd_Comb_MassHigh": 6750.0,
            "Bd_FlightChi2" : 25,
            "Lb_Comb_MassHigh": 7750.0,
            "Lb_FlightChi2": 16,
            "B_DIRA": 0.999,
            "Lb_DIRA": 0.999,
            "Bs_VertexCHI2": 100.0,
            "B0_VertexCHI2": 50.0,
            "Lb_VertexCHI2": 100.0,
            "Hadron_MinIPCHI2": 9.0,
            "Muon_MinIPCHI2": 9.0,
            "Phi_FlightChi2": 25.0,
            "Phi_Comb_MassHigh": 3650.0,
            "Phi_PT": 800,
            "Phi_VertexCHI2": 4,
            "Phi_DOCA": 0.15,
            "Phi_DOCACHI2": 4,
            "PhiMu_VertexCHI2": 9,
            "PhiMu_Comb_MassHigh": 3650.0,
            "PhiMu_DOCA": 0.8,
            "PhiMu_DOCACHI2": 9,
            "Kstar_FlightChi2": 150.0,
            "Kstar_Comb_MassHigh": 3550.0,
            "Kstar_Comb_MassLow": 795.0,
            "Kstar_PT": 1100,
            "Kstar_DOCA": 0.12,
            "Kstar_DOCACHI2": 3,
            "Kstar_VertexCHI2": 3,
            "KstarMu_VertexCHI2": 6,
            "KstarMu_Comb_MassHigh": 3550.0,
            "KstarMu_DOCA": 0.6,
            "KstarMu_DOCACHI2": 6,
            "Lambdastar_FlightChi2": 25,
            "Lambdastar_Comb_MassHigh": 5000.0,
            "Lambdastar_Comb_MassLow": 1400.0,
            "Lambdastar_PT": 1250,
            "Lambdastar_DOCA": 0.15,
            "Lambdastar_DOCACHI2": 4,
            "Lambdastar_VertexCHI2": 4,
            "LambdastarMu_Comb_MassHigh": 5000.0,
            "LambdastarMu_DOCA": 0.8,
            "LambdastarMu_DOCACHI2": 8,
            "LambdastarMu_VertexCHI2": 8,
            "MuonPT": 500,
            "MuonPID": 0.0,
            "KaonPID": 4.0,
            "ProtonPID": 5.0,
            "Pion_ProbNN": 0.3,
            "Pion_ProbNN_B2Kstar": 0.8,
            "Kaon_Pion_ProbNN_B2Kstar": 0.8,
            "Hadron_P": 3000,
            "SpdMult": 600,
            "Track_GhostProb": 0.3,
            "Track_TRCHI2": 3,
            "UseNoPIDsHadrons": False,
            "UseNoPIDsMuons": False,
            "HLT1_FILTER": None,
            "HLT2_FILTER": None,
            "L0DU_FILTER": None,
        },

    'WGs': ['RD'],
    'STREAMS': ['Dimuon']
}


class B2XMuTauMuonicConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name

        self.BsCombCut = "(AM < %(Bs_Comb_MassHigh)s * MeV)" % config
        self.BsCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Bs_VertexCHI2)s) & (BPVVDCHI2 > %(Bs_FlightChi2)s)" % config
 
        self.BdCombCut = "(AM < %(Bd_Comb_MassHigh)s * MeV)" % config
        self.BdCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(B0_VertexCHI2)s) & (BPVVDCHI2 > %(Bd_FlightChi2)s)" % config

        self.LambdaBCombCut = "(AM < %(Lb_Comb_MassHigh)s * MeV)" % config
        self.LambdaBCut = "(BPVDIRA > %(Lb_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Lb_VertexCHI2)s) & (BPVVDCHI2 > %(Lb_FlightChi2)s)" % config

        self.PhiCombCut = "(AM < %(Phi_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(Phi_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(Phi_DOCACHI2)s, ''))" % config
        self.PhiCut = "(VFASPF(VCHI2/VDOF) < %(Phi_VertexCHI2)s) & (PT > %(Phi_PT)s * MeV) & (BPVVDCHI2 > %(Phi_FlightChi2)s)" % config

        self.KstarCombCut = "(AM < %(Kstar_Comb_MassHigh)s*MeV) & (AM > %(Kstar_Comb_MassLow)s * MeV) & " \
                                                   "(ACUTDOCA(%(Kstar_DOCA)s * mm,'')) & (ACUTDOCACHI2(%(Kstar_DOCACHI2)s, ''))" % config
        self.KstarCut = "(VFASPF(VCHI2/VDOF) < %(Kstar_VertexCHI2)s) & " \
                                                   "(PT > %(Kstar_PT)s * MeV) & (BPVVDCHI2 > %(Kstar_FlightChi2)s)" % config

        self.LambdastarCombCut = "(AM < %(Lambdastar_Comb_MassHigh)s*MeV) & (AM > %(Lambdastar_Comb_MassLow)s*MeV) & " \
                                                   "(ACUTDOCA(%(Lambdastar_DOCA)s * mm,'')) & (ACUTDOCACHI2(%(Lambdastar_DOCACHI2)s, ''))" % config
        self.LambdastarCut = "(PT > %(Lambdastar_PT)s*MeV) & " \
                                                   "(VFASPF(VCHI2/VDOF) < %(Lambdastar_VertexCHI2)s) & (BPVVDCHI2 > %(Lambdastar_FlightChi2)s)" % config

        self.PhiMuCut = "(VFASPF(VCHI2/VDOF) < %(PhiMu_VertexCHI2)s)" % config
        self.PhiMuCombCut = "(AM < %(Phi_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(PhiMu_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(PhiMu_DOCACHI2)s, ''))" % config
        
        self.KstarMuCut = "(VFASPF(VCHI2/VDOF) < %(KstarMu_VertexCHI2)s)" % config
        self.KstarMuCombCut = "(AM < %(KstarMu_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(KstarMu_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(KstarMu_DOCACHI2)s, ''))" % config

        self.LambdastarMuCut = "(VFASPF(VCHI2/VDOF) < %(LambdastarMu_VertexCHI2)s)" % config
        self.LambdastarMuCombCut = "(AM < %(LambdastarMu_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(LambdastarMu_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(LambdastarMu_DOCACHI2)s, ''))" % config
        
        self.TrackCuts = "(TRGHP < %(Track_GhostProb)s) & (TRCHI2DOF < %(Track_TRCHI2)s)" % config

        self.HadronCuts = "(MIPCHI2DV(PRIMARY) > %(Hadron_MinIPCHI2)s)" % config

        self.KaonCutBase = self.TrackCuts + " & " + self.HadronCuts + " & (P > %(Hadron_P)s*MeV)" % config
        self.KaonCut = self.KaonCutBase + " & (PIDK > %(KaonPID)s) & (~ISMUON)" % config
        self.KaonCutNoPID = self.KaonCutBase + " & (PIDK < %(KaonPID)s)" % config
        self.KaonCut_B2Kstar = self.KaonCutBase + " & (PROBNNK * (1-PROBNNpi) > %(Kaon_Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.KaonCutNoPID_B2Kstar = self.KaonCutBase + " & (PROBNNK * (1-PROBNNpi) < %(Kaon_Pion_ProbNN_B2Kstar)s)" % config

        self.PionCutBase = self.TrackCuts + " & " + self.HadronCuts
        self.PionCut_B2Kstar = self.PionCutBase + " & (PROBNNpi > %(Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.PionCutNoPID_B2Kstar = self.PionCutBase + " & (PROBNNpi < %(Pion_ProbNN_B2Kstar)s)" % config

        self.ProtonCutBase = self.TrackCuts + " & " + self.HadronCuts + " & (P > %(Hadron_P)s * MeV)" % config
        self.ProtonCut = self.ProtonCutBase + " & (PIDp > %(ProtonPID)s)  & (~ISMUON)" % config
        self.ProtonCutNoPID = self.ProtonCutBase + " & (PIDp < %(ProtonPID)s)" % config

        self.MuonCutBase = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s)" % config
        self.MuonCut = self.MuonCutBase + " & (PIDmu> %(MuonPID)s) & (ISMUON)" % config
        self.MuonCutNoPID = self.MuonCutBase + " & (PIDmu < %(MuonPID)s) " % config

        self.MuonFromBCutBase = self.MuonCutBase + " & (PT > %(MuonPT)s * MeV)" % config
        self.MuonFromBCut = self.MuonCut + " & (PT > %(MuonPT)s * MeV)" % config
        self.MuonFromBCutNoPID = self.MuonCutNoPID + " & (PT > %(MuonPT)s * MeV)" % config

        self.Kaons = self.__Kaons__(config)
        self.FakeKaons = self.__FakeKaons__()
        
        self.Kaons_Kstar = self.__Kaons__(config, sel_name="_B2Kstar")
        self.FakeKaons_Kstar = self.__FakeKaons__(sel_name="_B2Kstar")

        self.Pions_Kstar = self.__Pions__(config, sel_name="_B2Kstar")
        self.FakePions_Kstar = self.__FakePions__(sel_name="_B2Kstar")

        self.Protons = self.__Protons__(config)
        self.FakeProtons = self.__FakeProtons__()
        
        self.MuonsFromTau = self.__Muons__(config, mother='tau')
        self.FakeMuonsFromTau = self.__FakeMuons__(mother='tau')

        self.MuonsFromB = self.__Muons__(config, mother='b_hadron')
        self.FakeMuonsFromB = self.__FakeMuons__(mother='b_hadron')

        self.Phi = self.__Phi__(self.Kaons, conf=config)
        self.FakePhi = self.__Phi__(self.Kaons, self.FakeKaons, conf=config)

        self.PhiMu = self.__PhiMu__(self.Phi, self.MuonsFromB)
        self.PhiMu_FakeKaon = self.__PhiMu__(self.FakePhi, self.MuonsFromB, pid_selection='NoPIDK_')
        self.PhiMu_FakeMuon = self.__PhiMu__(self.Phi,  self.FakeMuonsFromB, pid_selection='NoPIDMu_')

        self.Kstar = self.__Kstar__(self.Kaons_Kstar, self.Pions_Kstar)
        self.FakePionKstar = self.__Kstar__(self.Kaons_Kstar, self.FakePions_Kstar, pid_selection="NoPIDPi_")
        self.FakeKaonKstar = self.__Kstar__(self.FakeKaons_Kstar, self.Pions_Kstar, pid_selection="NoPIDK_")

        self.KstarMu = self.__KstarMu__(self.Kstar, self.MuonsFromB)
        self.KstarMu_FakeKaon = self.__KstarMu__(self.FakeKaonKstar, self.MuonsFromB, pid_selection='NoPIDK_')
        self.KstarMu_FakePion = self.__KstarMu__(self.FakePionKstar, self.MuonsFromB, pid_selection='NoPIDpi_')
        self.KstarMu_FakeMuon = self.__KstarMu__(self.Kstar, self.FakeMuonsFromB, pid_selection='NoPIDMu_')

        self.LambdaStar = self.__Lambdastar__(self.Protons, self.Kaons)
        self.FakeProtonLambdaStar = self.__Lambdastar__(self.FakeProtons, self.Kaons, pid_selection="NoPIDp_")
        self.FakeKaonLambdaStar = self.__Lambdastar__(self.Protons, self.FakeKaons, pid_selection="NoPIDK_")

        self.LambdastarMu = self.__LambdastarMu__(self.LambdaStar, self.MuonsFromB)
        self.LambdastarMu_FakeKaon = self.__LambdastarMu__(self.FakeKaonLambdaStar, self.MuonsFromB, pid_selection='NoPIDK_')
        self.LambdastarMu_FakeProton = self.__LambdastarMu__(self.FakeProtonLambdaStar, self.MuonsFromB, pid_selection='NoPIDp_')
        self.LambdastarMu_FakeMuon = self.__LambdastarMu__(self.LambdaStar, self.FakeMuonsFromB, pid_selection='NoPIDMu_')

        self.DeclaredDaughters = [self.PhiMu, self.MuonsFromTau]
        self.Bs = self.__Bs_Phi__(daughters=self.DeclaredDaughters)

        self.DeclaredDaughters = [self.PhiMu, self.FakeMuonsFromTau]
        self.Bs_NoPIDmuFromTau = self.__Bs_Phi__(daughters=self.DeclaredDaughters, pid_selection="NoPIDmuFromTau_")

        self.DeclaredDaughters = [self.PhiMu_FakeMuon, self.MuonsFromTau]
        self.Bs_NoPIDmuFromB = self.__Bs_Phi__(daughters=self.DeclaredDaughters, pid_selection="NoPIDmuFromb_")

        self.Bs_NoPIDmu = Selection("Bs_NoPIDmu_sel_for" + self.name, Algorithm=FilterDesktop(Code='ALL'), 
                                    RequiredSelections=[self.Bs_NoPIDmuFromTau, self.Bs_NoPIDmuFromB])

        self.DeclaredDaughters = [self.PhiMu_FakeKaon, self.MuonsFromTau]
        self.Bs_NoPIDK = self.__Bs_Phi__(daughters=self.DeclaredDaughters, pid_selection="NoPIDK_")
        
        self.DeclaredDaughters = [self.KstarMu, self.MuonsFromTau]
        self.Bd = self.__B_Kstar__(daughters=self.DeclaredDaughters)

        self.DeclaredDaughters = [self.KstarMu, self.FakeMuonsFromTau]
        self.Bd_NoPIDmuFromTau = self.__B_Kstar__(daughters=self.DeclaredDaughters, pid_selection="NoPIDmuFromTau_")
        
        self.DeclaredDaughters = [self.KstarMu_FakeMuon, self.MuonsFromTau]
        self.Bd_NoPIDmuFromB = self.__B_Kstar__(daughters=self.DeclaredDaughters, pid_selection="NoPIDmuFromb_")

        self.Bd_NoPIDmu = Selection("Bd_NoPIDmu_sel_for" + self.name, Algorithm=FilterDesktop(Code='ALL'), 
                          RequiredSelections=[self.Bd_NoPIDmuFromTau, self.Bd_NoPIDmuFromB])

        self.DeclaredDaughters = [self.KstarMu_FakeKaon, self.MuonsFromTau]
        self.Bd_NoPIDK = self.__B_Kstar__(daughters=self.DeclaredDaughters, pid_selection="NoPIDK_")

        self.DeclaredDaughters = [self.KstarMu_FakePion, self.MuonsFromTau]
        self.Bd_NoPIDpi = self.__B_Kstar__(daughters=self.DeclaredDaughters, pid_selection="NoPIDpi_")

        self.DeclaredDaughters = [self.LambdastarMu, self.MuonsFromTau]
        self.Lb = self.__Lambdab_Lambdastar__(daughters=self.DeclaredDaughters)

        self.DeclaredDaughters = [self.LambdastarMu, self.FakeMuonsFromTau]
        self.Lb_NoPIDmuFromTau = self.__Lambdab_Lambdastar__(daughters=self.DeclaredDaughters, pid_selection="NoPIDmuFromTau_")
        
        self.DeclaredDaughters = [self.LambdastarMu_FakeMuon, self.MuonsFromTau]
        self.Lb_NoPIDmuFromB = self.__Lambdab_Lambdastar__(daughters=self.DeclaredDaughters, pid_selection="NoPIDmuFromb_")

        self.Lb_NoPIDmu = Selection("Lb_NoPIDmu_sel_for" + self.name, Algorithm=FilterDesktop(Code='ALL'), 
                          RequiredSelections=[self.Lb_NoPIDmuFromTau, self.Lb_NoPIDmuFromB])

        self.DeclaredDaughters = [self.LambdastarMu_FakeKaon, self.MuonsFromTau]
        self.Lb_NoPIDK = self.__Lambdab_Lambdastar__(daughters=self.DeclaredDaughters, pid_selection="NoPIDK_")

        self.DeclaredDaughters = [self.LambdastarMu_FakeProton, self.MuonsFromTau]
        self.Lb_NoPIDp = self.__Lambdab_Lambdastar__(daughters=self.DeclaredDaughters, pid_selection="NoPIDp_")


        self.FilterSPD = {'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                          'Preambulo': ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]}

        self.Bs2phi_line = StrippingLine(
            self.name + "_Bs2Phi_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phi_line)

        self.Bs2phi_NoPIDmu_line = StrippingLine(
            self.name + "_Bs2Phi_NoPIDMuLine", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_NoPIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phi_NoPIDmu_line)

        self.Bs2phi_NoPIDK_line = StrippingLine(
            self.name + "_Bs2Phi_NoPIDKLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_NoPIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phi_NoPIDK_line)

        self.Bd2Kst_line = StrippingLine(
            self.name + "_B2Kstar_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bd2Kst_line)

        self.Bd2Kst_NoPIDmu_line = StrippingLine(
            self.name + "_B2Kstar_NoPIDMuLine", prescale=0.15,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_NoPIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kst_NoPIDmu_line)

        self.Bd2Kst_NoPIDK_line = StrippingLine(
            self.name + "_B2Kstar_NoPIDKLine", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_NoPIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kst_NoPIDK_line)

        self.Bd2Kst_NoPIDpi_line = StrippingLine(
            self.name + "_B2Kstar_NoPIDpiLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_NoPIDpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kst_NoPIDpi_line)

        self.Lb2Lst_line = StrippingLine(
            self.name + "_Lb2Lstar_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb], MDSTFlag=False, MaxCandidates=1000)

        self.registerLine(self.Lb2Lst_line)

        self.Lb2Lst_NoPIDmu_line = StrippingLine(
            self.name + "_Lb2Lstar_NoPIDMuLine", prescale=0.15,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_NoPIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lst_NoPIDmu_line)

        self.Lb2Lst_NoPIDK_line = StrippingLine(
            self.name + "_Lb2Lstar_NoPIDKLine", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_NoPIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lst_NoPIDK_line)

        self.Lb2Lst_NoPIDp_line = StrippingLine(
            self.name + "_Lb2Lstar_NoPIDpLine", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_NoPIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lst_NoPIDp_line)


    def __Muons__(self, conf, mother):
        from StandardParticles import StdAllLooseMuons, StdAllNoPIDsMuons
        muons = StdAllNoPIDsMuons if conf['UseNoPIDsMuons'] else StdAllLooseMuons
        if mother == 'tau':
            muon_cut = self.MuonCutBase if conf['UseNoPIDsMuons'] else self.MuonCut
        else:
            muon_cut = self.MuonFromBCutBase if conf['UseNoPIDsMuons'] else self.MuonFromBCut
        _filter = FilterDesktop(Code=muon_cut)
        _sel = Selection("Selection_" + self.name + "_StdLooseMuons" + "from" + mother, RequiredSelections=[muons],
                         Algorithm=_filter)
        return _sel

    def __FakeMuons__(self, mother):
        from StandardParticles import StdAllNoPIDsMuons
        if mother == 'tau':
            _filter = FilterDesktop(Code=self.MuonCutNoPID)
        else:
            _filter = FilterDesktop(Code=self.MuonFromBCutNoPID)
        _sel = Selection("Selection_" + self._name + "StdAllNoPIDsMuons" + "from" + mother, Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsMuons])
        return _sel

    def __Protons__(self, conf):
        from StandardParticles import StdLooseProtons, StdNoPIDsProtons
        protons = StdNoPIDsProtons if conf['UseNoPIDsHadrons'] else StdLooseProtons
        proton_cuts = self.ProtonCutBase if conf['UseNoPIDsHadrons'] else self.ProtonCut
        _filter = FilterDesktop(Code=proton_cuts)
        _sel = Selection("Selection_" + self.name + "_StdLooseProtons", RequiredSelections=[protons], Algorithm=_filter)
        return _sel

    def __FakeProtons__(self):
        from StandardParticles import StdNoPIDsProtons
        _filter = FilterDesktop(Code=self.ProtonCutNoPID)
        _sel = Selection("Selection_" + self._name + "_StdAllNoPIDsProtons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsProtons])
        return _sel

    def __Kaons__(self, conf, sel_name="_"):
        from StandardParticles import StdLooseKaons, StdNoPIDsKaons
        kaons = StdNoPIDsKaons if conf['UseNoPIDsHadrons'] else StdLooseKaons
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.KaonCutBase)
        elif "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCut_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCut)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLooseKaons", RequiredSelections=[kaons],
                         Algorithm=_filter)
        return _sel

    def __FakeKaons__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsKaons
        if "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCutNoPID_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCutNoPID)
        _sel = Selection("Selection_" + self._name + sel_name + "StdAllNoPIDsKaons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsKaons])
        return _sel

    def __Pions__(self, conf, sel_name="_"):
        from StandardParticles import StdLoosePions, StdNoPIDsPions
        pions = StdNoPIDsPions if conf['UseNoPIDsHadrons'] else StdLoosePions
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.PionCutBase)
        else:
            _filter = FilterDesktop(Code=self.PionCut_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLoosePions", RequiredSelections=[pions],
                         Algorithm=_filter)
        return _sel

    def __FakePions__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsPions
        _filter = FilterDesktop(Code=self.PionCutNoPID_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdAllNoPIDsPions", RequiredSelections=[StdNoPIDsPions],
                         Algorithm=_filter)
        return _sel

    def __Phi__(self, Kaons, fakekaon=None, conf=None):
        _phi2kk = CombineParticles()
        _phi2kk.DecayDescriptors = ["phi(1020) -> K+ K-", "phi(1020) -> K+ K+", "phi(1020) -> K- K-"]
        _phi2kk.MotherCut = self.PhiCut
        if fakekaon is None:
            _phi2kk.CombinationCut = self.PhiCombCut
            _sel = Selection("Phi_selection_for" + self.name, Algorithm=_phi2kk, RequiredSelections=[Kaons])
        else:
            _phi2kk.CombinationCut = self.PhiCombCut + " & (AHASCHILD((PIDK < %(KaonPID)s)))" % conf
            _sel = Selection("Phi_NoPIDK_selection_for" + self.name, Algorithm=_phi2kk,
                             RequiredSelections=[Kaons, fakekaon])
        return _sel

    def __Kstar__(self, Kaons, Pions, pid_selection="_"):
        _kstar2kpi = CombineParticles()
        _kstar2kpi.DecayDescriptors = ["[K*(892)0 -> K+ pi-]cc", "K*(892)0 -> K+ pi+", "K*(892)0 -> K- pi-"]
        _kstar2kpi.CombinationCut = self.KstarCombCut
        _kstar2kpi.MotherCut = self.KstarCut
        _sel = Selection("Kstar" + pid_selection + "selection_for" + self.name, Algorithm=_kstar2kpi,
                         RequiredSelections=[Kaons, Pions])
        return _sel

    def __Lambdastar__(self, Protons, Kaons, pid_selection="_"):
        _lstar2pk = CombineParticles()
        _lstar2pk.DecayDescriptors = ["[Lambda(1520)0 -> p+ K-]cc", "Lambda(1520)0 -> p+ K+", "Lambda(1520)0 -> p~- K-"]
        _lstar2pk.CombinationCut = self.LambdastarCombCut
        _lstar2pk.MotherCut = self.LambdastarCut
        _sel = Selection("Lambdastar" + pid_selection + "selection_for" + self.name, Algorithm=_lstar2pk,
                         RequiredSelections=[Protons, Kaons])
        return _sel

    
    def __PhiMu__(self, Phi, Muon, conf=None, pid_selection='_'):
        _b2phimu = CombineParticles()
        _b2phimu.DecayDescriptors = ["[B0 -> phi(1020) mu+]cc"]
        _b2phimu.MotherCut = self.PhiMuCut
        _b2phimu.CombinationCut = self.PhiMuCombCut
        _sel = Selection("B2PhiMu" + pid_selection + self.name, Algorithm=_b2phimu, RequiredSelections=[Phi,Muon])
        return _sel

    def __KstarMu__(self, Kstar, Muon, conf=None, pid_selection='_'):
        _b2kstmu = CombineParticles()
        _b2kstmu.DecayDescriptors = ["[B0 -> K*(892)0 mu+]cc", "[B0 -> K*(892)0 mu-]cc"]
        _b2kstmu.MotherCut = self.KstarMuCut
        _b2kstmu.CombinationCut = self.KstarMuCombCut
        _sel = Selection("B2KstarMu" + pid_selection + self.name, Algorithm=_b2kstmu, RequiredSelections=[Kstar,Muon])
        return _sel
     
    def __LambdastarMu__(self, Lambdastar, Muon, conf=None, pid_selection='_'):
        _b2lstmu = CombineParticles()
        _b2lstmu.DecayDescriptors = ["[B0 -> Lambda(1520)0 mu+]cc", "[B0 -> Lambda(1520)0 mu-]cc"]
        _b2lstmu.MotherCut = self.LambdastarMuCut
        _b2lstmu.CombinationCut = self.LambdastarMuCombCut
        _sel = Selection("B2LambdastarMu" + pid_selection + self.name, Algorithm=_b2lstmu, RequiredSelections=[Lambdastar,Muon])
        return _sel


    def __Bs_Phi__(self, daughters, pid_selection="_"):
        _b2phimutau = CombineParticles(DecayDescriptors=["[B_s0 -> B0 mu+]cc", "[B_s0 -> B0 mu-]cc"],
                                        MotherCut=self.BsCut, CombinationCut=self.BsCombCut)
        sel = Selection("Phi" + pid_selection + "for" + self.name, Algorithm=_b2phimutau, RequiredSelections=daughters)
        return sel

    def __B_Kstar__(self, daughters, pid_selection="_"):
        _b2kstmutau = CombineParticles(DecayDescriptors=["[B_s0 -> B0 mu+]cc", "[B_s0 -> B0 mu-]cc"],
                                        MotherCut=self.BdCut, CombinationCut=self.BdCombCut)
        sel = Selection("Kstar" + pid_selection + "for" + self.name, Algorithm=_b2kstmutau, RequiredSelections=daughters)
        return sel

    def __Lambdab_Lambdastar__(self, daughters, pid_selection="_"):
        _b2lstmutau = CombineParticles(DecayDescriptors=["[Lambda_b0 -> B0 mu+]cc", "[Lambda_b0 -> B0 mu-]cc"],
                                        MotherCut=self.BsCut, CombinationCut=self.BsCombCut)
        sel = Selection("Lambdastar" + pid_selection + "for" + self.name, Algorithm=_b2lstmutau, RequiredSelections=daughters)
        return sel
