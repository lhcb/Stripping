from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selection of He3 candidates via VELO strip overflows
'''

__author__ = ['Gediminas Sarpis, Hendrik Jage']
__date__ = '19.03.2021'
__version__ = 'v0r1'

__all__ = ('HeNOverflowsConf', 'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

from StandardParticles import StdAllNoPIDsProtons as Protons
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from Configurables import LoKi__VoidFilter as VoidFilter

default_config = {
    'NAME': 'HeNOverflows',
    'BUILDERTYPE': 'HeNOverflowsConf',
    'WGs': ['IFT'],
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'Prescale': 1.0,
         'Trk_nOverflows': 3,
         'RequiredRawEvents' : ["Calo", "Rich", "Velo", "Tracker"],

    },
}

class HeNOverflowsConf(LineBuilder):
    __configuration_keys__ = list(default_config['CONFIG'].keys())

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        _heNOverflowCuts = "(TRVELOCLUSTEROVERFLOWS() > %(Trk_nOverflows)s)" % self.config

        heNOverflowsFilter = FilterDesktop(Code = _heNOverflowCuts)

        myHeNOverflows = Selection('HeliumForOverflowsSel',
                                Algorithm = heNOverflowsFilter,
                                RequiredSelections = [Protons] )


        HeNOverflowsLine = StrippingLine(
            self.name + '_Line',
            prescale=self.config['Prescale'],
            RequiredRawEvents=self.config['RequiredRawEvents'],
            selection=myHeNOverflows)

        self.registerLine(HeNOverflowsLine)
