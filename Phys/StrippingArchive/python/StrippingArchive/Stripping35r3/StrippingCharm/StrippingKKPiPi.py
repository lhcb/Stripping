###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting [cs][ubar dbar] -> K+ K+ pi- pi-
'''

__author__=['Mengzhen Wang']
__date__ = '10/10/2017'
__version__= '$Revision: 1.0 $'


__all__ = (
    'KKPiPiConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'KKPiPi',
    'BUILDERTYPE'       :  'KKPiPiConf',
    'CONFIG'    : {

        'DetachedKaonCuts'    : "(PROBNNk  > 0.4) & (PT > 250*MeV) & (TRGHOSTPROB<0.2) & (MIPCHI2DV(PRIMARY) > 4.)",
        'DetachedPionCuts'      : "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.2) & (MIPCHI2DV(PRIMARY) > 4.)",
        'DetachedComAMCuts'     : "AALL",
        'DetachedComN4Cuts'     : "(AM > 1.0 *GeV) & (APT>3.0 *GeV)",
        'DetachedMomN4Cuts'     : "(VFASPF(VCHI2/VDOF) < 9.) & (MM>1.5 *GeV) & (MM<3.0 *GeV) & (BPVDIRA>0.9995)",

        'Prescale'      : 1.
        },
    'STREAMS'           : ['Charm' ],
    'WGs'               : ['Charm'],
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays

class KKPiPiConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()


    def __init__(self, name, config ):

        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        """
        Detached [cs][ubar dbar] -> K+ K+ pi- pi-
        """

        self.SelDetachedKaons = self.createSubSel( OutputList = self.name + "SelDetachedKaons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllNoPIDsKaons/Particles' ),
                                           Cuts = config['DetachedKaonCuts']
                                           )
        self.SelDetachedPions = self.createSubSel( OutputList = self.name + "DetachedSelPions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllNoPIDsPions/Particles' ),
                                           Cuts = config['DetachedPionCuts']
                                           )
        self.SelDetached2KKPiPi = self.createN4BodySel( OutputList = self.name + "SelDetached2KKPiPi",
                                                     DaughterLists = [ self.SelDetachedKaons, self.SelDetachedPions ],
                                                     DecayDescriptor = "h_c(1P) -> K+ K+ pi- pi-",
                                                     ComAMCuts      = config['DetachedComAMCuts'],
                                                     PreVertexCuts  = config['DetachedComN4Cuts'],
                                                     PostVertexCuts = config['DetachedMomN4Cuts']
                                                     )
        self.Detached2KKPiPiLine = StrippingLine( self.name + 'DetachedLine',
                                               prescale  = config['Prescale'],
                                               algos     = [ self.SelDetached2KKPiPi ],
                                               MDSTFlag  = False
                                               )
        self.registerLine( self.Detached2KKPiPiLine )



    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createN4BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N4BodyDecays ( DecayDescriptor = DecayDescriptor,
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts + "&" + "( ACHI2DOCA(1,2)<15 )",
                                           Combination123Cut = ComAMCuts + "&" + "( ACHI2DOCA(1,3)<15 ) & ( ACHI2DOCA(2,3)<15 )",
                                           CombinationCut = "( ACHI2DOCA(1,4)<15 ) & ( ACHI2DOCA(2,4)<15 ) & ( ACHI2DOCA(3,4)<15 )" + " & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
