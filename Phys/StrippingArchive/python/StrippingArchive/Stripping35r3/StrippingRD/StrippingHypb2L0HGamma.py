###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = 'Luis Miguel Garcia Martin, luis.miguel.garcia.martin@cern.ch'
__date__ = '08/04/2016'
__version__ = '1.0'
__description__ = '[Xi_b- -> (Xi- -> (Lambda0 -> p+ pi-) pi-) gamma], [Omega_b- -> (Omega- -> (Lambda0 -> p+ pi-) K-) gamma]'
__all__ = ('Hypb2L0HGammaConf',
     'default_config', )

from Gaudi.Configuration import *
from Configurables import FilterDesktop, CombineParticles, TisTosParticleTagger
from PhysSelPython.Wrappers      import Selection, MergedSelection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils        import LineBuilder
from StandardParticles import StdLooseAllPhotons

default_config = {
  'NAME'      : 'Hypb2L0HGamma',
  'WGs'     : ['RD'],
  'BUILDERTYPE' : 'Hypb2L0HGammaConf',
  'CONFIG'    : {'Prescale'       : 1,
           'Photon_PT_Min'        : 2500.0,
           'Photon_CL_Min'        : 0.15,
           #Tracks
           'TRCHI2DOF_MAX'        : 4.,
           'TRACK_IPCHI2_MIN'     : 16.,            #
           #                            'VCHI2PDOF_MAX'        :   16.0,
           'Ghost_Prob'           : 0.4,
           'p_PT_Min'             : 630,
           'pi_L0_PT_Min'         : 130,
           'pi_Xi_PT_Min'         : 120,
           #Lambda
           'L0_VCHI2'             : 9,
           'L0_PT_Min'            : 850,
           #Xi
           'CombMassWinXi'        : 60.,
           'MassWinXi'            : 30.,
           'MinPTXi'              : 1120.,
           'MinPXi'               : 10000.,
           'Xi_VCHI2'             : 8,
           #              'BPVLTIME_MIN'         :   2.0 ,        proper time cut
           #                           'BPVDIRA_MIN'           :   0.93,    proper time cut
           #                           'BPVVDCHI2_MIN'         :   0.0,   Proper time cut
           #Xib
           'CombMassWinXib'       : 800.,
           'MinPTXib'             : 500.,
           'MinPXib'              : 15000.,
           'MTDOCACHI2_MAX'       :   15.,     #
           #Omega
           'CombMassWinOmega'     : 120.,
           'MassWinOmega'         : 70.,
           'MinPTO'               : 1000.,
           'MinPO'                : 10000.,
           #Omegab
           'CombMassWinOmegab'    : 1000.,
           'MinPTOb'              : 1000.,
           'MinPOb'               : 15000.,

           'TISTOSLinesDict': {'L0Photon.*Decision%TOS':0,
                   'L0Electron.*Decision%TOS':0#,
                   #'L0Photon.*Decision%TIS':0,
                   #'L0Electron.*Decision%TIS':0,
                   }
           },
  'STREAMS'   : ['Leptonic'],
  }

#######################################################################

class Hypb2L0HGammaConf(LineBuilder):

	__configuration_keys__ = default_config['CONFIG'].keys()

	def __init__(self, name, config):

		LineBuilder.__init__(self, name, config)

		PionsL =DataOnDemand('Phys/StdLoosePions/Particles')
		PionsD =DataOnDemand('Phys/StdNoPIDsDownPions/Particles')

##############################################################
####################### Lambda Filter  #######################
##############################################################

		tracks_cuts = """(MAXTREE(TRCHI2DOF, HASTRACK) < %(TRCHI2DOF_MAX)s) &
				         (MAXTREE(TRGHOSTPROB, HASTRACK) < %(Ghost_Prob)s) &
				         (INTREE(('p+'==ABSID) & (PT > %(p_PT_Min)s))) &
				         (INTREE(('pi+'==ABSID) & (PT > %(pi_L0_PT_Min)s)))"""

		Lambda_cuts = """(PT>%(L0_PT_Min)s*MeV) &
				             (VFASPF(VCHI2/VDOF)<%(L0_VCHI2)s)"""
		Lambda_code = (Lambda_cuts + " & " + tracks_cuts) % config
		Lambda_filter = FilterDesktop(name = 'Lambda_filter',
		                              Code = Lambda_code)

#		Lambda_ll_dod = DataOnDemand('Phys/StdLooseLambdaLL/Particles')
		Lambda_ll_dod = DataOnDemand('Phys/StdVeryLooseLambdaLL/Particles')
		Lambda_ll = Selection("LooseLambda0LL_for_Hypb2L0HGamma",
				               Algorithm=Lambda_filter,
				               RequiredSelections=[Lambda_ll_dod])


		Lambda_dd_dod = DataOnDemand('Phys/StdLooseLambdaDD/Particles')
		Lambda_dd = Selection("LooseLambda0DD_for_Hypb2L0HGamma",
				               Algorithm=Lambda_filter,
				               RequiredSelections=[Lambda_dd_dod])



##############################################################
####################### Photon Filter  #######################
##############################################################
		photons_filter = FilterDesktop(name = "Photons_filter",
					       Code="(PT > %(Photon_PT_Min)s*MeV) & (CL > %(Photon_CL_Min)s)" % config)
		Photons = Selection("Photons"+name,
				    Algorithm=photons_filter,
				    RequiredSelections=[StdLooseAllPhotons])

#############		Making all Xi		#######################
		self.selXi_LLL = makeXi2L0pi(name 	= "Xi2L0pi_LLL" + name,
					     pions 	= PionsL,
					     lambdas = Lambda_ll,
					     params = config
					     )

		self.selXi_DDD = makeXi2L0pi(name 	= "Xi2L0pi_DDD" + name,
					     pions 	= PionsD,
					     lambdas = Lambda_dd,
					     params = config
					     )

		self.selXi_DDL = makeXi2L0pi(name 	= "Xi2L0pi_DDL" + name,
					     pions 	= PionsL,
					     lambdas = Lambda_dd,
					     params = config
					     )

		#		self.mergedXi = MergedSelection("Xi_minus",
#										RequiredSelections=[self.selXi_LLL, self.selXi_DDD, self.selXi_DDL])

#############		Making  Xi_b LLL	#######################
		children_Xib = {'Xi': '[Xi_b- -> ^Xi- gamma]CC',
                       'gamma': '[Xi_b- -> Xi- ^gamma]CC'}

		self.SelXib_LLL = makeXib2XiGamma(name	=	"Xib2XiGamma_LLL",
						  xiSel	=	self.selXi_LLL,
						  photons=	Photons,
						  params = config
						  )

		self.lineXib2XiGamma_LLL = StrippingLine("StripXib2XiGamma_LLL",
							 prescale = config['Prescale'],
							 algos = [tisTosSelection(self.SelXib_LLL, config['TISTOSLinesDict'])],
							 postselalg = makevelotracks("StripXib2XiGamma_LLL"),
							 MDSTFlag = False,
							 RelatedInfoTools = [
###		Track Match Xib
							 {'Type' : 'RelInfoVeloTrackMatch',
									      'InputTracks' : "Rec/Track/Velo"+"StripXib2XiGamma_LLL",
									      'Location' : 'P2VELOTRACKMATCH',
									      'ConeAroundMomentum' : 'True'
									      },
#
### Photon Isolation
							 {"Type": "RelInfoGammaIso",
			                 "DaughterLocations": {"[Xi_b- -> Xi- ^gamma]CC": "GammaIsolation"}},
#
#### Photon Cone Iso Variables:
                		get_cone_relinfo(1.7, children=children_Xib),
                		get_cone_relinfo(1.35, children=children_Xib),
						get_cone_relinfo(1.0, children=children_Xib),
#
#### Neutral Cone Iso Variables:
                		get_neutral_cone_relinfo(1.7, children=children_Xib),
                		get_neutral_cone_relinfo(1.35, children=children_Xib),
						get_neutral_cone_relinfo(1.0, children=children_Xib),
						get_neutral_cone_relinfo(0.4, children=children_Xib)
						]
							 )
#
		self.registerLine(self.lineXib2XiGamma_LLL)

#############		Making  Xi_b DDD   #######################
		self.SelXib_DDD = makeXib2XiGamma(name	=	"Xib2XiGamma_DDD",
						  xiSel	=	self.selXi_DDD,
						  photons=	Photons,
						  params = config
						  )

		self.lineXib2XiGamma_DDD = StrippingLine("StripXib2XiGamma_DDD",
							 prescale = config['Prescale'],
                             algos = [tisTosSelection(self.SelXib_DDD, config['TISTOSLinesDict'])],
							 postselalg = makevelotracks("StripXib2XiGamma_DDD"),
							 MDSTFlag = False,
							 RelatedInfoTools = [
###		Track Match Xib
							 {'Type' : 'RelInfoVeloTrackMatch',
									      'InputTracks' : "Rec/Track/Velo"+"StripXib2XiGamma_DDD",
									      'Location' : 'P2VELOTRACKMATCH',
									      'ConeAroundMomentum' : 'True'
									      },
#
### Photon Isolation
							 {"Type": "RelInfoGammaIso",
			                 "DaughterLocations": {"[Xi_b- -> Xi- ^gamma]CC": "GammaIsolation"}},
#
#### Photon Cone Iso Variables:
                		get_cone_relinfo(1.7, children=children_Xib),
                		get_cone_relinfo(1.35, children=children_Xib),
						get_cone_relinfo(1.0, children=children_Xib),
#
#### Neutral Cone Iso Variables:
                		get_neutral_cone_relinfo(1.7, children=children_Xib),
                		get_neutral_cone_relinfo(1.35, children=children_Xib),
						get_neutral_cone_relinfo(1.0, children=children_Xib),
						get_neutral_cone_relinfo(0.4, children=children_Xib)
						]
							 )
#

		self.registerLine(self.lineXib2XiGamma_DDD)

#############		Making  Xi_b DDL	#######################
		self.SelXib_DDL = makeXib2XiGamma(name	=	"Xib2XiGamma_DDL",
						  xiSel	=	self.selXi_DDL,
						  photons=	Photons,
						  params = config
						  )

		self.lineXib2XiGamma_DDL = StrippingLine("StripXib2XiGamma_DDL",
							 prescale = config['Prescale'],
                             algos = [tisTosSelection(self.SelXib_DDL, config['TISTOSLinesDict'])],
							 postselalg = makevelotracks("StripXib2XiGamma_DDL"),
							 MDSTFlag = False,
							 RelatedInfoTools = [
###		Track Match Xib
							 {'Type' : 'RelInfoVeloTrackMatch',
									      'InputTracks' : "Rec/Track/Velo"+"StripXib2XiGamma_DDL",
									      'Location' : 'P2VELOTRACKMATCH',
									      'ConeAroundMomentum' : 'True'
									      },
#
### Photon Isolation
							 {"Type": "RelInfoGammaIso",
			                 "DaughterLocations": {"[Xi_b- -> Xi- ^gamma]CC": "GammaIsolation"}},
#
#### Photon Cone Iso Variables:
                		get_cone_relinfo(1.7, children=children_Xib),
                		get_cone_relinfo(1.35, children=children_Xib),
						get_cone_relinfo(1.0, children=children_Xib),
#
#### Neutral Cone Iso Variables:
                		get_neutral_cone_relinfo(1.7, children=children_Xib),
                		get_neutral_cone_relinfo(1.35, children=children_Xib),
						get_neutral_cone_relinfo(1.0, children=children_Xib),
						get_neutral_cone_relinfo(0.4, children=children_Xib)
						]
							 )
#

		self.registerLine(self.lineXib2XiGamma_DDL)

	##################################################################################
	##############################	Omega	##########################################
	##################################################################################

		KaonsL =DataOnDemand('Phys/StdLooseKaons/Particles')
		KaonsD =DataOnDemand('Phys/StdLooseDownKaons/Particles')

	#############		Making all Omegas		#######################
		self.selO_LLL = makeO2L0K(name 	= "O2L0K_LLL" + name,
					  kaons 	= KaonsL,
					  lambdas = Lambda_ll,
					  params = config
					  )

		self.selO_DDD = makeO2L0K(name 	= "O2L0K_DDD" + name,
					  kaons 	= KaonsD,
					  lambdas = Lambda_dd,
					  params = config
					  )

		self.selO_DDL = makeO2L0K(name 	= "O2L0K_DDL" + name,
					  kaons 	= KaonsL,
					  lambdas = Lambda_dd,
					  params = config
					  )

		#		self.mergedO = MergedSelection("O_minus",
	#										RequiredSelections=[self.selO_LLL, self.selO_DDD, self.selO_DDL])

#############		Making  Omega_b		#######################
		self.SelOb_LLL = makeOb2OGamma(name	=	"Ob2OGamma_LLL",
					       oSel	=	self.selO_LLL,
					       photons=	Photons,
					       params = config
					       )


		self.lineOb2OGamma_LLL = StrippingLine("StripOb2OGamma_LLL",
						       prescale = config['Prescale'],
						       algos = [tisTosSelection(self.SelOb_LLL, config['TISTOSLinesDict'])],
						       postselalg = makevelotracks("StripOb2OGamma_LLL"),
						       MDSTFlag = False,
						       RelatedInfoTools = [{'Type' : 'RelInfoVeloTrackMatch',
									    'InputTracks' : "Rec/Track/Velo"+"StripOb2OGamma_LLL",
									    'Location' : 'P2VELOTRACKMATCH',
									     'ConeAroundMomentum' : 'True'
									    }]
						       )

		self.registerLine(self.lineOb2OGamma_LLL)

#############		Making  Omega_b		#######################
		self.SelOb_DDD = makeOb2OGamma(name	=	"Ob2OGamma_DDD",
					       oSel	=	self.selO_DDD,
					       photons=	Photons,
					       params = config
					       )


		self.lineOb2OGamma_DDD = StrippingLine("StripOb2OGamma_DDD",
						       prescale = config['Prescale'],
						       #selection = tisTosSelection(self.SelOb_DDD, config['TISTOSLinesDict']),
						       algos = [tisTosSelection(self.SelOb_DDD, config['TISTOSLinesDict'])],
						       postselalg = makevelotracks("StripOb2OGamma_DDD"),
						       MDSTFlag = False,
						       RelatedInfoTools = [{'Type' : 'RelInfoVeloTrackMatch',
									    'InputTracks' : "Rec/Track/Velo"+"StripOb2OGamma_DDD",
									    'Location' : 'P2VELOTRACKMATCH',
									     'ConeAroundMomentum' : 'True'
									    }]
						       )

		self.registerLine(self.lineOb2OGamma_DDD)


#############		Making  Omega_b		#######################
		self.SelOb_DDL = makeOb2OGamma(name	=	"Ob2OGamma_DDL",
					       oSel	=	self.selO_DDL,
					       photons=	Photons,
					       params = config
					       )


		self.lineOb2OGamma_DDL = StrippingLine("StripOb2OGamma_DDL",
						       prescale = config['Prescale'],
						       algos = [tisTosSelection(self.SelOb_DDL, config['TISTOSLinesDict'])],
						       postselalg = makevelotracks("StripOb2OGamma_DDL"),
						       #selection = tisTosSelection(self.SelOb_DDL, config['TISTOSLinesDict']),
						       MDSTFlag = False,
						       RelatedInfoTools = [{'Type' : 'RelInfoVeloTrackMatch',
									    'InputTracks' : "Rec/Track/Velo"+"StripOb2OGamma_DDL",
									    'Location' : 'P2VELOTRACKMATCH',
									     'ConeAroundMomentum' : 'True'
									    }]
						       )

		self.registerLine(self.lineOb2OGamma_DDL)







#################################################################################################
#################################################################################################
#################################################################################################


def makeXi2L0pi(name, pions, lambdas, params):
  _name = name
  _daughter_cut = {'Lambda0' : 'ALL' ,
       'pi-'     : '(MIPCHI2DV(PRIMARY) > %(TRACK_IPCHI2_MIN)s) & (TRCHI2DOF < %(TRCHI2DOF_MAX)s) & (PT > %(pi_Xi_PT_Min)s * MeV)' %params }
  _combcut = "(ADAMASS('Xi-') < %(CombMassWinXib)s * MeV)" %params

  _mothercut = "(ADMASS('Xi-') < %(MassWinXi)s * MeV) & (PT > %(MinPTXi)s * MeV) & (P > %(MinPXi)s*MeV) & (MAXTREE(TRGHOSTPROB, HASTRACK) < %(Ghost_Prob)s) & (VFASPF(VCHI2/VDOF)<%(Xi_VCHI2)s)" %params

  _Combine = CombineParticles(name = _name,
            DecayDescriptor = "[Xi- -> Lambda0 pi-]cc",
            CombinationCut = _combcut,
            MotherCut = _mothercut
            )

  return Selection(name + 'L0pi',
       Algorithm = _Combine,
       RequiredSelections = [pions, lambdas]
       )

#################################################################################################
def makeXib2XiGamma(name, xiSel, photons, params):
  _name = name

  _combcut = "(ADAMASS('Xi_b-') < %(CombMassWinXib)s * MeV)" %params

  _mothercut = "(PT > %(MinPTXib)s * MeV) & (P > %(MinPXib)s*MeV) & (MTDOCACHI2(1) < %(MTDOCACHI2_MAX)s)" %params

  _Combine = CombineParticles(name = _name,
                DecayDescriptor = "[Xi_b- -> Xi- gamma]cc",
                CombinationCut = _combcut,
                MotherCut = _mothercut
                )
  _Combine.ParticleCombiners = {'' : 'ParticleAdder'}
#  _Combine.ParticleCombiners = {'' : 'MomentumCombiner:PUBLIC'}

  return Selection(name + 'XiGamma',
           Algorithm = _Combine,
           RequiredSelections = [xiSel, photons]
           )



#################################################################################
#################################################################################
def makeO2L0K(name, kaons, lambdas, params):
  _name = name
  _daughter_cut = {'Lambda0' : 'ALL' ,
       'K-'     : '(MIPCHI2DV(PRIMARY) > %(TRACK_IPCHI2_MIN)s) & (TRCHI2DOF < %(TRCHI2DOF_MAX)s)' %params }
  _combcut = "(ADAMASS('Omega-') < %(CombMassWinOmega)s * MeV)" %params
  _mothercut = "(ADMASS('Omega-') < %(MassWinOmega)s * MeV) & (PT > %(MinPTO)s * MeV) & (P > %(MinPO)s*MeV) & (MAXTREE(TRGHOSTPROB, HASTRACK) < %(Ghost_Prob)s)" %params
  _Combine = CombineParticles(name = _name,
            DecayDescriptor = "[Omega- -> Lambda0 K-]cc",
            CombinationCut = _combcut,
            MotherCut = _mothercut
            )
  return Selection(name + 'L0K',
       Algorithm = _Combine,
       RequiredSelections = [kaons, lambdas]
       )

###################################################################################
def makeOb2OGamma(name, oSel, photons, params):
  _name = name
  _combcut = "(ADAMASS('Omega_b-') < %(CombMassWinOmegab)s * MeV)" %params
  _mothercut = "(PT > %(MinPTOb)s * MeV) & (P > %(MinPOb)s*MeV) & (MTDOCACHI2(1) < %(MTDOCACHI2_MAX)s)" %params
  _Combine = CombineParticles(name = _name,
            DecayDescriptor = "[Omega_b- -> Omega- gamma]cc",
            CombinationCut = _combcut,
            MotherCut = _mothercut
            )
  _Combine.ParticleCombiners = {'' : 'ParticleAdder'}
  return Selection(name + 'OGamma',
       Algorithm = _Combine,
       RequiredSelections = [oSel, photons]
       )

def makeTISTOSFilter(name,dict_TISTOS):
  specs = dict_TISTOS
  from Configurables import TisTosParticleTagger
  tisTosFilter = TisTosParticleTagger(name+'TISTOSFilter')
  tisTosFilter.TisTosSpecs = specs
  return tisTosFilter

def tisTosSelection(sel,TISTOSdict):
  tisTosFilter = makeTISTOSFilter(sel.name(),TISTOSdict)
  return Selection(sel.name()+'TISTOS', Algorithm=tisTosFilter, RequiredSelections=[sel])


def makevelotracks(name):
    from Configurables import  FastVeloTracking, TrackPrepareVelo, DecodeVeloRawBuffer, TrackEventFitter, TrackStateInitAlg,  TrackContainerCopy
    from TrackFitter.ConfiguredFitters import ConfiguredEventFitter

    MyFastVeloTracking = FastVeloTracking("For%sFastVelo"%name,OutputTracksName="Rec/Track/MyVeloFor%s"%name)
    MyFastVeloTracking.OnlyForward = True
    MyFastVeloTracking.ResetUsedFlags = True
    ### prepare for fitting
    preve = TrackStateInitAlg("For%sInitSeedFit"%name,TrackLocation = "Rec/Track/MyVeloFor%s"%name)
    preve.StateInitTool.VeloFitterName = "FastVeloFitLHCbIDs"
    copyVelo = TrackContainerCopy( "For%sCopyVelo"%name )
    copyVelo.inputLocations = ["Rec/Track/MyVeloFor%s"%name]
    copyVelo.outputLocation = "Rec/Track/Velo"+name
    MyVeloFit = ConfiguredEventFitter(Name="For%sVeloRefitterAlg"%name,
              TracksInContainer="Rec/Track/Velo"+name,
#              TracksOutContainer="Rec/Track/Velo"+name,
              SimplifiedGeometry = True)
    maketracks = GaudiSequencer('For%sMakeVeloTracksGS'%name)
    maketracks.Members += [ MyFastVeloTracking ]
    maketracks.Members += [ preve ]
    maketracks.Members += [ copyVelo ]
    maketracks.Members += [ MyVeloFit ]

    return maketracks

######################################################################################
def get_cone_relinfo(angle, head=None, children=None):
    tool = {'Type'     : 'RelInfoConeVariables',
            'ConeAngle': angle,
            'Variables': ['CONEANGLE', 'CONEMULT', 'CONEP', 'CONEPASYM', 'CONEPT', 'CONEPTASYM']}
    # Some shortcuts
    base_location = 'ConeVarsInfo/%%s/%s' % angle
    # Head
    if head:
        tool.update({'Location'    : base_location % 'XiB',
                     'TopSelection': head})
    if children:
        tool.update({'DaughterLocations': dict([(sel_string, base_location % name)
                                                for name, sel_string in children.items()])})
    return tool

#############################################################################################
def get_neutral_cone_relinfo(size, head=None, children=None):
    tool = {'Type'       : 'RelInfoConeIsolation',
            'FillCharged': False,
            'ConeSize'   : size}
# Some shortcuts
    base_location = 'NeutralConeVarsInfo/%%s/%s' % size
# Head
    if head:
        tool.update({'Location'    : base_location % 'XiB',
                     'TopSelection': head})
    if children:
        tool.update({'DaughterLocations': dict([(sel_string, base_location % name)
                                                for name, sel_string in children.items()])})
    return tool
