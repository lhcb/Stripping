###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of Lambda_c -> mu ll lines
Lc-> pemu and Lc->p pi0 ll lines added by Jolanta Brodzicka in Jan-2019
Sigma_c tagged lines added by Jolanta Brodzicka in Jan-2019

Performance (with prescaling);

Full.dst:
#########

StrippingReport                                                INFO Event 1000000, Good event 1000000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |_StrippingGlobal_                                            |  0.0242|       242|       |   9.439|
 |!StrippingLc23MuLc23muLine_TIMING                            |  0.0056|        56|  1.071|   0.056|
 |!StrippingLc23MuLc2mueeLine_TIMING                           |  0.0041|        41|  1.073|   0.067|
 |!StrippingLc23MuLc2pmumuLine_TIMING                          |  0.0036|        36|  1.028|   0.102|
 |!StrippingLc23MuLc2peeLine_TIMING                            |  0.0075|        75|  1.027|   0.109|
 |!StrippingLc23MuLc2pKpiLine_TIMING                           |  0.0019|        19|  1.158|   0.025|

MC: Lc -> 3mu (25113002)
########################
StrippingReport                                                INFO Event 100000, Good event 100000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |!StrippingLc23MuLc23muLine                                   |  2.0770|      2077|  1.025|   5.726|

MC: Lc -> p+ mu+ mu- (25113000)
###############################
StrippingReport                                                INFO Event 100000, Good event 100000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |!StrippingLc23MuLc2pmumuLine                                 |  1.0300|      1030|  1.042|   1.736|


MC: Lc -> p~- mu+ mu+ (25113001)
################################
StrippingReport                                                INFO Event 100000, Good event 100000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |!StrippingLc23MuLc2pmumuLine                                 |  0.9420|       942|  1.041|   1.720|

MC: Lc -> p+ K- pi+ (25103000)
##############################
StrippingReport                                                INFO Event 100000, Good event 100000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |!StrippingLc23MuLc2pKpiLine                                  |  0.0080|         8|  1.000|   0.061|

Exported symbols (use python help!):
   -
"""

__author__ = ["Oliver Gruenberg, Jolanta Brodzicka"]
__date__ = "15.01.2019"
__version__ = "$Revision: 2.0 $"

###################################################################################################

__all__ = ( "Lc23MuLinesConf",
            "config_default", )

###################################################################################################

from Gaudi.Configuration import *
from Configurables import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays, DaVinci__N4BodyDecays
#from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.PhysicalConstants import c_light

###################################################################################################

default_config = {
    "NAME"        : "Lc23Mu",
    "WGs"         : [ "RD" ],
    "STREAMS"     : [ "Leptonic" ],
    "BUILDERTYPE" : "Lc23MuLinesConf",
    "CONFIG"      : {
    "MDSTflag"            : False, # True or False
    # TrackCuts
    "MinTrPT"             : 300, # (MeV)
    "MinTrIPChi2"         : 9,
    "MaxTrChi2Dof"        : 4.0,
    "MaxTrGhp"            : 0.4,
    # Pi0Cuts
    "MinPi0R_PT"          : 300, #200, # (MeV) #StdLooseResolvedPi0 PT>200MeV
    "MinPi0M_PT"          : 500, # (MeV) #StdLooseMergedPi0 (start at PT>2GeV really)
    "Pi0R_DMASS"          : 25,  #30,  # (MeV) #StdLooseResolvedPi0 30MeV
    "Pi0M_DMASS"          : 60,  # (MeV) #StdLooseMergedPi0 60MeV
    "MinPhotonCL"         : 0.2, #0.1,
    # CombinationCuts
    "MaxDoca"             : 0.3, # (mm)
    "MaxDocaChi2"         : 10., # for Pi0-track distance
    "mDiffLcVeryLoose"    : 500, # (MeV) #for 4-body [expected 5(8)times worse resol wrt 3-body w/ added Pi0R(M)]
    "mDiffLcLoose"        : 350, # (MeV) #200->350 #min needed:(-100,+100) pmumu, (-300,+200) pemu, (-350,+250) pee
    "mDiffLcTight"        : 150, # (MeV) #for pKpi
    # MotherCuts
    "MaxVtxChi2"          : 15,
    "MinVD"               : 70, # (micrometer)
    "MaxIPChi2"           : 100,
    "MinBPVDira"          : 0.999, #0.9, #for 4-body
    #Sigmac MotherCuts
    "Sigmac_AMDiff_MAX"   : 400,#(MeV) #PDG 167 for Sc(2455); 232 for Sc(2520) full-width~15MeV min cut:232+5*8+50~330
    "Sigmac_VCHI2VDOF_MAX": 30.,
    # scalings
    "Postscale"           : 1,
    "Lc23muPrescale"      : 1,
    "Lc2mueePrescale"     : 1,
    "Lc2pmumuPrescale"    : 1,
    "Lc2peePrescale"      : 1,
    "Lc2pemuPrescale"     : 1,
    "Lc2pKpiPrescale"     : 0.01,
#
    "Lc2pmumupi0RPrescale": 1,
    "Lc2pmumupi0MPrescale": 1,
    "Lc2peepi0RPrescale"  : 1,
    "Lc2peepi0MPrescale"  : 1,
    "Lc2pemupi0RPrescale" : 1,
    "Lc2pemupi0MPrescale" : 1,
    "Lc2pKpipi0RPrescale" : 0.01,
    "Lc2pKpipi0MPrescale" : 0.1,
#
    "SigmaczSignalPrescale"  : 1,
    "SigmacppSignalPrescale" : 1,
    "SigmaczRefPrescale"     : 0.01,
    "SigmacppRefPrescale"    : 0.01,
    "SigmaczRefPi0MPrescale" : 0.1,
    "SigmacppRefPi0MPrescale": 0.1,
    "CommonRelInfoTools"  : [ { "Type": "RelInfoVertexIsolation", "Location":"VtxIsoInfo" },
                              { "Type": "RelInfoVertexIsolationBDT", "Location":"VtxIsoInfoBDT" },
                              { "Type" : "RelInfoBs2MuMuBIsolations",
                                "Variables" : [],
                                "Location"  : "BsMuMuBIsolation",
                                "tracktype" : 3,
                                "makeTrackCuts" : False, },
                              ] # closes CommonRelInfoTools
    } # closes CONFIG
    } # closes default_config

class Lc23MuLinesConf(LineBuilder) :
    """
    Builder
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name = "Lc23mu", config = default_config) :

        LineBuilder.__init__(self, name, config)


#######################################################################################################

        self.TrackCuts = """
                         (PT > %(MinTrPT)s*MeV)
                         & (BPVIPCHI2() > %(MinTrIPChi2)s)
                         & (TRCHI2DOF < %(MaxTrChi2Dof)s)
                         & (TRGHP < %(MaxTrGhp)s)
                         """ %config

        self.Pi0RCuts = """
                        (PT > %(MinPi0R_PT)s*MeV)
                        & (M > 135 - %(Pi0R_DMASS)s *MeV) & (M < 135 + %(Pi0R_DMASS)s *MeV)
                        & ( CHILDCUT( (CL > %(MinPhotonCL)s) ,1) )
                        & ( CHILDCUT( (CL > %(MinPhotonCL)s) ,2) )
                        """ %config

        self.Pi0MCuts = """
                        ( PT > %(MinPi0M_PT)s*MeV )
                        & (M > 135 - %(Pi0M_DMASS)s *MeV) & (M < 135 + %(Pi0M_DMASS)s *MeV)
                        """ %config

        self.Combination12Cuts = "(ADOCA(1,2) < %(MaxDoca)s*mm)" %config

        self.CombinationCutsLoose = """
                                 (ADAMASS('Lambda_c+') < %(mDiffLcLoose)s*MeV)
                                 & (ADOCA(1,3) < %(MaxDoca)s*mm)
                                 & (ADOCA(2,3) < %(MaxDoca)s*mm)
                                 """ %config


        self.CombinationCutsTight = """
                                 (ADAMASS('Lambda_c+') < %(mDiffLcTight)s*MeV)
                                 & (ADOCA(1,3) < %(MaxDoca)s*mm)
                                 & (ADOCA(2,3) < %(MaxDoca)s*mm)
                                 """ %config

        self.MotherCuts = """
                          ( VFASPF(VCHI2) < %(MaxVtxChi2)s )
                          & ( (BPVLTIME()*c_light) > %(MinVD)s*micrometer )
                          & ( BPVIPCHI2() < %(MaxIPChi2)s )
                          """ %config

#
        self.Combination123Cuts = """
                                 (ADOCA(1,3) < %(MaxDoca)s*mm)
                                 & (ADOCA(2,3) < %(MaxDoca)s*mm)
                                  """ %config

        self.Combination4BodyCutsLoose = """
                                 (ADAMASS('Lambda_c+') < %(mDiffLcVeryLoose)s*MeV)
                                 & (ACHI2DOCA(1,4) < %(MaxDocaChi2)s)
                                 & (ACHI2DOCA(2,4) < %(MaxDocaChi2)s)
                                 & (ACHI2DOCA(3,4) < %(MaxDocaChi2)s)
                                 """ %config

        self.Mother4BodyCuts = """
                          ( VFASPF(VCHI2) < %(MaxVtxChi2)s )
                          & ( (BPVLTIME()*c_light) > %(MinVD)s*micrometer )
                          & ( BPVIPCHI2() < %(MaxIPChi2)s )
                          & ( BPVDIRA > %(MinBPVDira)s )
                          """ %config

#tagged
        self.SigmacCombinationCuts = """
                          ( (AM - AM1) < %(Sigmac_AMDiff_MAX)s*MeV )
                          """ %config

        self.SigmacMotherCuts = """
                          ( VFASPF(VCHI2/VDOF) < %(Sigmac_VCHI2VDOF_MAX)s )
                          """ %config

#######################################################################################################
        # Decay descriptors for tagged lines
        Sigmacz2LambdacPi  = ['[Sigma_c0 -> Lambda_c+ pi-]cc']
        Sigmacpp2LambdacPi = ['[Sigma_c++ -> Lambda_c+ pi+]cc']

        # Names
        Lc23mu_name   = name+"Lc23mu"
        Lc2muee_name  = name+"Lc2muee"
        Lc2pmumu_name = name+"Lc2pmumu"
        Lc2pee_name   = name+"Lc2pee"
        Lc2pemu_name  = name+"Lc2pemu"
        Lc2pKpi_name  = name+"Lc2pKpi"
#
        Lc2pmumupi0R_name = name+"Lc2pmumupi0R"
        Lc2pmumupi0M_name = name+"Lc2pmumupi0M"
        Lc2peepi0R_name = name+"Lc2peepi0R"
        Lc2peepi0M_name = name+"Lc2peepi0M"
        Lc2pemupi0R_name = name+"Lc2pemupi0R"
        Lc2pemupi0M_name = name+"Lc2pemupi0M"
        Lc2pKpipi0R_name  = name+"Lc2pKpipi0R"
        Lc2pKpipi0M_name  = name+"Lc2pKpipi0M"
#
        SigmaczLc2pmumu_name = name+"SigmaczLc2pmumu"
        SigmacppLc2pmumu_name = name+"SigmacppLc2pmumu"
        SigmaczLc2pee_name = name+"SigmaczLc2pee"
        SigmacppLc2pee_name = name+"SigmacppLc2pee"
        SigmaczLc2pemu_name = name+"SigmaczLc2pemu"
        SigmacppLc2pemu_name = name+"SigmacppLc2pemu"
        SigmaczLc2pKpi_name = name+"SigmaczLc2pKpi"
        SigmacppLc2pKpi_name = name+"SigmacppLc2pKpi"
#
        SigmaczLc2pmumupi0R_name = name+"SigmaczLc2pmumupi0R"
        SigmacppLc2pmumupi0R_name = name+"SigmacppLc2pmumupi0R"
        SigmaczLc2pmumupi0M_name = name+"SigmaczLc2pmumupi0M"
        SigmacppLc2pmumupi0M_name = name+"SigmacppLc2pmumupi0M"

        SigmaczLc2peepi0R_name = name+"SigmaczLc2peepi0R"
        SigmacppLc2peepi0R_name = name+"SigmacppLc2peepi0R"
        SigmaczLc2peepi0M_name = name+"SigmaczLc2peepi0M"
        SigmacppLc2peepi0M_name = name+"SigmacppLc2peepi0M"

        SigmaczLc2pemupi0R_name = name+"SigmaczLc2pemupi0R"
        SigmacppLc2pemupi0R_name = name+"SigmacppLc2pemupi0R"
        SigmaczLc2pemupi0M_name = name+"SigmaczLc2pemupi0M"
        SigmacppLc2pemupi0M_name = name+"SigmacppLc2pemupi0M"

        SigmaczLc2pKpipi0R_name = name+"SigmaczLc2pKpipi0R"
        SigmacppLc2pKpipi0R_name = name+"SigmacppLc2pKpipi0R"
        SigmaczLc2pKpipi0M_name = name+"SigmaczLc2pKpipi0M"
        SigmacppLc2pKpipi0M_name = name+"SigmacppLc2pKpipi0M"



        self.selLc23mu    = self.makeLc23mu(Lc23mu_name)
        self.selLc2muee   = self.makeLc2muee(Lc2muee_name)
        self.selLc2pmumu  = self.makeLc2pmumu(Lc2pmumu_name)
        self.selLc2pee    = self.makeLc2pee(Lc2pee_name)
        self.selLc2pemu   = self.makeLc2pemu(Lc2pemu_name)
        self.selLc2pKpi   = self.makeLc2pKpi(Lc2pKpi_name)
#4-body
        self.selLc2pmumupi0R  = self.makeLc2pmumupi0R(Lc2pmumupi0R_name)
        self.selLc2pmumupi0M  = self.makeLc2pmumupi0M(Lc2pmumupi0M_name)
        self.selLc2peepi0R    = self.makeLc2peepi0R(Lc2peepi0R_name)
        self.selLc2peepi0M    = self.makeLc2peepi0M(Lc2peepi0M_name)
        self.selLc2pemupi0R   = self.makeLc2pemupi0R(Lc2pemupi0R_name)
        self.selLc2pemupi0M   = self.makeLc2pemupi0M(Lc2pemupi0M_name)
        self.selLc2pKpipi0R   = self.makeLc2pKpipi0R(Lc2pKpipi0R_name)
        self.selLc2pKpipi0M   = self.makeLc2pKpipi0M(Lc2pKpipi0M_name)

#tagged
        self.selSigmaczLc2pmumu = self.makeSigmac2Lcpi(SigmaczLc2pmumu_name,
             inputSel=self.selLc2pmumu,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2pmumu = self.makeSigmac2Lcpi(SigmacppLc2pmumu_name,
             inputSel=self.selLc2pmumu,
             decDescriptors=Sigmacpp2LambdacPi
        )
        self.selSigmaczLc2pee = self.makeSigmac2Lcpi(SigmaczLc2pee_name,
             inputSel=self.selLc2pee,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2pee = self.makeSigmac2Lcpi(SigmacppLc2pee_name,
             inputSel=self.selLc2pee,
             decDescriptors=Sigmacpp2LambdacPi
        )
        self.selSigmaczLc2pemu = self.makeSigmac2Lcpi(SigmaczLc2pemu_name,
             inputSel=self.selLc2pemu,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2pemu = self.makeSigmac2Lcpi(SigmacppLc2pemu_name,
             inputSel=self.selLc2pemu,
             decDescriptors=Sigmacpp2LambdacPi
        )
        self.selSigmaczLc2pKpi = self.makeSigmac2Lcpi(SigmaczLc2pKpi_name,
             inputSel=self.selLc2pKpi,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2pKpi = self.makeSigmac2Lcpi(SigmacppLc2pKpi_name,
             inputSel=self.selLc2pKpi,
             decDescriptors=Sigmacpp2LambdacPi
        )
#tagged 4-body
        self.selSigmaczLc2pmumupi0R = self.makeSigmac2Lcpi(SigmaczLc2pmumupi0R_name,
             inputSel=self.selLc2pmumupi0R,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2pmumupi0R = self.makeSigmac2Lcpi(SigmacppLc2pmumupi0R_name,
             inputSel=self.selLc2pmumupi0R,
             decDescriptors=Sigmacpp2LambdacPi
        )
        self.selSigmaczLc2pmumupi0M = self.makeSigmac2Lcpi(SigmaczLc2pmumupi0M_name,
             inputSel=self.selLc2pmumupi0M,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2pmumupi0M = self.makeSigmac2Lcpi(SigmacppLc2pmumupi0M_name,
             inputSel=self.selLc2pmumupi0M,
             decDescriptors=Sigmacpp2LambdacPi
        )
#
        self.selSigmaczLc2peepi0R = self.makeSigmac2Lcpi(SigmaczLc2peepi0R_name,
             inputSel=self.selLc2peepi0R,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2peepi0R = self.makeSigmac2Lcpi(SigmacppLc2peepi0R_name,
             inputSel=self.selLc2peepi0R,
             decDescriptors=Sigmacpp2LambdacPi
        )
        self.selSigmaczLc2peepi0M = self.makeSigmac2Lcpi(SigmaczLc2peepi0M_name,
             inputSel=self.selLc2peepi0M,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2peepi0M = self.makeSigmac2Lcpi(SigmacppLc2peepi0M_name,
             inputSel=self.selLc2peepi0M,
             decDescriptors=Sigmacpp2LambdacPi
        )
#
        self.selSigmaczLc2pemupi0R = self.makeSigmac2Lcpi(SigmaczLc2pemupi0R_name,
             inputSel=self.selLc2pemupi0R,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2pemupi0R = self.makeSigmac2Lcpi(SigmacppLc2pemupi0R_name,
             inputSel=self.selLc2pemupi0R,
             decDescriptors=Sigmacpp2LambdacPi
        )
        self.selSigmaczLc2pemupi0M = self.makeSigmac2Lcpi(SigmaczLc2pemupi0M_name,
             inputSel=self.selLc2pemupi0M,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2pemupi0M = self.makeSigmac2Lcpi(SigmacppLc2pemupi0M_name,
             inputSel=self.selLc2pemupi0M,
             decDescriptors=Sigmacpp2LambdacPi
        )
#
        self.selSigmaczLc2pKpipi0R = self.makeSigmac2Lcpi(SigmaczLc2pKpipi0R_name,
             inputSel=self.selLc2pKpipi0R,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2pKpipi0R = self.makeSigmac2Lcpi(SigmacppLc2pKpipi0R_name,
             inputSel=self.selLc2pKpipi0R,
             decDescriptors=Sigmacpp2LambdacPi
        )
        self.selSigmaczLc2pKpipi0M = self.makeSigmac2Lcpi(SigmaczLc2pKpipi0M_name,
             inputSel=self.selLc2pKpipi0M,
             decDescriptors=Sigmacz2LambdacPi
        )
        self.selSigmacppLc2pKpipi0M = self.makeSigmac2Lcpi(SigmacppLc2pKpipi0M_name,
             inputSel=self.selLc2pKpipi0M,
             decDescriptors=Sigmacpp2LambdacPi
        )


#######################################################################################################

        self.Lc23mu_Line = StrippingLine(Lc23mu_name+"Line",
                                         prescale = config["Lc23muPrescale"],
                                         postscale = config["Postscale"],
                                         MDSTFlag = config["MDSTflag"],
                                         selection = self.selLc23mu,
                                         RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^mu+  mu+  l]CC"  : "ConeIso05mu1",
                                             "[Lambda_c+ ->  mu+ ^mu+  l]CC"  : "ConeIso05mu2",
                                             "[Lambda_c+ ->  mu+  mu+ ^l]CC"  : "ConeIso05mu3",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^mu+  mu+  l]CC"  : "ConeIso10mu1",
                                             "[Lambda_c+ ->  mu+ ^mu+  l]CC"  : "ConeIso10mu2",
                                             "[Lambda_c+ ->  mu+  mu+ ^l]CC"  : "ConeIso10mu3",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^mu+  mu+  l]CC"  : "ConeIso15mu1",
                                             "[Lambda_c+ ->  mu+ ^mu+  l]CC"  : "ConeIso15mu2",
                                             "[Lambda_c+ ->  mu+  mu+ ^l]CC"  : "ConeIso15mu3",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^mu+  mu+  l]CC"  : "TrackIsoBDTmu1",
                                             "[Lambda_c+ ->  mu+ ^mu+  l]CC"  : "TrackIsoBDTmu2",
                                             "[Lambda_c+ ->  mu+  mu+ ^l]CC"  : "TrackIsoBDTmu3",
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^mu+  mu+  l]CC"  : "BsMuMuTrackIsomu1",
                                             "[Lambda_c+ ->  mu+ ^mu+  l]CC"  : "BsMuMuTrackIsomu2",
                                             "[Lambda_c+ ->  mu+  mu+ ^l]CC"  : "BsMuMuTrackIsomu3",
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline



        self.Lc2muee_Line = StrippingLine(Lc2muee_name+"Line",
                                          prescale = config["Lc2mueePrescale"],
                                          postscale = config["Postscale"],
                                          MDSTFlag = config["MDSTflag"],
                                          selection = self.selLc2muee,
                                          RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^l  l  l]CC"  : "ConeIso05mu",
                                             "[Lambda_c+ ->  l ^l  l]CC"  : "ConeIso05e1",
                                             "[Lambda_c+ ->  l  l ^l]CC"  : "ConeIso05e2",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^l  l  l]CC"  : "ConeIso10mu",
                                             "[Lambda_c+ ->  l ^l  l]CC"  : "ConeIso10e1",
                                             "[Lambda_c+ ->  l  l ^l]CC"  : "ConeIso10e2",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^l  l  l]CC"  : "ConeIso15mu",
                                             "[Lambda_c+ ->  l ^l  l]CC"  : "ConeIso15e1",
                                             "[Lambda_c+ ->  l  l ^l]CC"  : "ConeIso15e2",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^l  l  l]CC"  : "TrackIsoBDTmu",
                                             "[Lambda_c+ ->  l ^l  l]CC"  : "TrackIsoBDTe1",
                                             "[Lambda_c+ ->  l  l ^l]CC"  : "TrackIsoBDTe2",
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^l  l  l]CC"  : "BsMuMuTrackIsomu",
                                             "[Lambda_c+ ->  l ^l  l]CC"  : "BsMuMuTrackIsoe1",
                                             "[Lambda_c+ ->  l  l ^l]CC"  : "BsMuMuTrackIsoe2",
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                         ) # closes Strippingline


        self.Lc2pmumu_Line = StrippingLine(Lc2pmumu_name+"Line",
                                           prescale = config["Lc2pmumuPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selLc2pmumu,
                                           RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "ConeIso05mu1",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "ConeIso05mu2",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "ConeIso10mu1",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "ConeIso10mu2",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "ConeIso15mu1",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "ConeIso15mu2",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "TrackIsoBDTmu1",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "TrackIsoBDTmu2",
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "BsMuMuTrackIsomu1",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "BsMuMuTrackIsomu2",
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline

        self.Lc2pee_Line = StrippingLine(Lc2pee_name+"Line",
                                         prescale = config["Lc2peePrescale"],
                                         postscale = config["Postscale"],
                                         MDSTFlag = config["MDSTflag"],
                                         selection = self.selLc2pee,
                                         RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "ConeIso05e1",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "ConeIso05e2",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "ConeIso10e1",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "ConeIso10e2",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "ConeIso15e1",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "ConeIso15e2",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "TrackIsoBDTe1",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "TrackIsoBDTe2",
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "BsMuMuTrackIsoe1",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "BsMuMuTrackIsoe2",
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline

        self.Lc2pKpi_Line = StrippingLine(Lc2pKpi_name+"Line",
                                          prescale = config["Lc2pKpiPrescale"],
                                          postscale = config["Postscale"],
                                          MDSTFlag = config["MDSTflag"],
                                          selection = self.selLc2pKpi,
                                          RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  p+ ^K-  pi+]CC"  : "ConeIso05K",
                                             "[Lambda_c+ ->  p+  K- ^pi+]CC"  : "ConeIso05pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  p+ ^K-  pi+]CC"  : "ConeIso10K",
                                             "[Lambda_c+ ->  p+  K- ^pi+]CC"  : "ConeIso10pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  p+ ^K-  pi+]CC"  : "ConeIso15K",
                                             "[Lambda_c+ ->  p+  K- ^pi+]CC"  : "ConeIso15pi",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  p+ ^K-  pi+]CC"  : "TrackIsoBDTK",
                                             "[Lambda_c+ ->  p+  K- ^pi+]CC"  : "TrackIsoBDTpi",
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  p+ ^K-  pi+]CC"  : "BsMuMuTrackIsoK",
                                             "[Lambda_c+ ->  p+  K- ^pi+]CC"  : "BsMuMuTrackIsopi",
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline

        self.Lc2pemu_Line = StrippingLine(Lc2pemu_name+"Line",
                                         prescale = config["Lc2pemuPrescale"],
                                         postscale = config["Postscale"],
                                         MDSTFlag = config["MDSTflag"],
                                         selection = self.selLc2pemu,
                                         RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "ConeIso05e",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "ConeIso05mu",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "ConeIso10e",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "ConeIso10mu",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "ConeIso15e",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "ConeIso15mu",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "TrackIsoBDTe",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "TrackIsoBDTmu",
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  X ^l  l]CC"  : "BsMuMuTrackIsoe",
                                             "[Lambda_c+ ->  X  l ^l]CC"  : "BsMuMuTrackIsomu",
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline

##4-bodies##
        self.Lc2pmumupi0R_Line = StrippingLine(Lc2pmumupi0R_name+"Line",
                                           prescale = config["Lc2pmumupi0RPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selLc2pmumupi0R,
                                           RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  mu+  mu-  pi0]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  p+ ^mu+  mu-  pi0]CC"  : "ConeIso05mu1",
                                             "[Lambda_c+ ->  p+  mu+ ^mu-  pi0]CC"  : "ConeIso05mu2",
                                             "[Lambda_c+ ->  p+  mu+  mu- ^pi0]CC"  : "ConeIso05pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  mu+  mu-  pi0]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  p+ ^mu+  mu-  pi0]CC"  : "ConeIso10mu1",
                                             "[Lambda_c+ ->  p+  mu+ ^mu-  pi0]CC"  : "ConeIso10mu2",
                                             "[Lambda_c+ ->  p+  mu+  mu- ^pi0]CC"  : "ConeIso10pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  mu+  mu-  pi0]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  p+ ^mu+  mu-  pi0]CC"  : "ConeIso15mu1",
                                             "[Lambda_c+ ->  p+  mu+ ^mu-  pi0]CC"  : "ConeIso15mu2",
                                             "[Lambda_c+ ->  p+  mu+  mu- ^pi0]CC"  : "ConeIso15pi",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  mu+  mu-  pi0]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  p+ ^mu+  mu-  pi0]CC"  : "TrackIsoBDTmu1",
                                             "[Lambda_c+ ->  p+  mu+ ^mu-  pi0]CC"  : "TrackIsoBDTmu2",
                                             #"[Lambda_c+ ->  p+  mu+  mu- ^pi0]CC"  : "TrackIsoBDTpi", #?
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  mu+  mu-  pi0]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  p+ ^mu+  mu-  pi0]CC"  : "BsMuMuTrackIsomu1",
                                             "[Lambda_c+ ->  p+  mu+ ^mu-  pi0]CC"  : "BsMuMuTrackIsomu2",
                                             #"[Lambda_c+ ->  p+  mu+  mu- ^pi0]CC"  : "BsMuMuTrackIsopi", #?
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline


        self.Lc2pmumupi0M_Line = StrippingLine(Lc2pmumupi0M_name+"Line",
                                           prescale = config["Lc2pmumupi0MPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selLc2pmumupi0M,
                                           RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  mu+  mu-  pi0]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  p+ ^mu+  mu-  pi0]CC"  : "ConeIso05mu1",
                                             "[Lambda_c+ ->  p+  mu+ ^mu-  pi0]CC"  : "ConeIso05mu2",
                                             "[Lambda_c+ ->  p+  mu+  mu- ^pi0]CC"  : "ConeIso05pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  mu+  mu-  pi0]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  p+ ^mu+  mu-  pi0]CC"  : "ConeIso10mu1",
                                             "[Lambda_c+ ->  p+  mu+ ^mu-  pi0]CC"  : "ConeIso10mu2",
                                             "[Lambda_c+ ->  p+  mu+  mu- ^pi0]CC"  : "ConeIso10pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  mu+  mu-  pi0]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  p+ ^mu+  mu-  pi0]CC"  : "ConeIso15mu1",
                                             "[Lambda_c+ ->  p+  mu+ ^mu-  pi0]CC"  : "ConeIso15mu2",
                                             "[Lambda_c+ ->  p+  mu+  mu- ^pi0]CC"  : "ConeIso15pi",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  mu+  mu-  pi0]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  p+ ^mu+  mu-  pi0]CC"  : "TrackIsoBDTmu1",
                                             "[Lambda_c+ ->  p+  mu+ ^mu-  pi0]CC"  : "TrackIsoBDTmu2",
                                             #"[Lambda_c+ ->  p+  mu+  mu- ^pi0]CC"  : "TrackIsoBDTpi", #?
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  mu+  mu-  pi0]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  p+ ^mu+  mu-  pi0]CC"  : "BsMuMuTrackIsomu1",
                                             "[Lambda_c+ ->  p+  mu+ ^mu-  pi0]CC"  : "BsMuMuTrackIsomu2",
                                             #"[Lambda_c+ ->  p+  mu+  mu- ^pi0]CC"  : "BsMuMuTrackIsopi", #?
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline

        self.Lc2peepi0R_Line = StrippingLine(Lc2peepi0R_name+"Line",
                                           prescale = config["Lc2peepi0RPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selLc2peepi0R,
                                           RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  e+  e-  pi0]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  p+ ^e+  e-  pi0]CC"  : "ConeIso05e1",
                                             "[Lambda_c+ ->  p+  e+ ^e-  pi0]CC"  : "ConeIso05e2",
                                             "[Lambda_c+ ->  p+  e+  e- ^pi0]CC"  : "ConeIso05pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  e+  e-  pi0]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  p+ ^e+  e-  pi0]CC"  : "ConeIso10e1",
                                             "[Lambda_c+ ->  p+  e+ ^e-  pi0]CC"  : "ConeIso10e2",
                                             "[Lambda_c+ ->  p+  e+  e- ^pi0]CC"  : "ConeIso10pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  e+  e-  pi0]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  p+ ^e+  e-  pi0]CC"  : "ConeIso15e1",
                                             "[Lambda_c+ ->  p+  e+ ^e-  pi0]CC"  : "ConeIso15e2",
                                             "[Lambda_c+ ->  p+  e+  e- ^pi0]CC"  : "ConeIso15pi",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  e+  e-  pi0]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  p+ ^e+  e-  pi0]CC"  : "TrackIsoBDTe1",
                                             "[Lambda_c+ ->  p+  e+ ^e-  pi0]CC"  : "TrackIsoBDTe2",
                                             #"[Lambda_c+ ->  p+  e+  e- ^pi0]CC"  : "TrackIsoBDTpi", #?
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  e+  e-  pi0]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  p+ ^e+  e-  pi0]CC"  : "BsMuMuTrackIsoe1",
                                             "[Lambda_c+ ->  p+  e+ ^e-  pi0]CC"  : "BsMuMuTrackIsoe2",
                                             #"[Lambda_c+ ->  p+  e+  e- ^pi0]CC"  : "BsMuMuTrackIsopi", #?
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline


        self.Lc2peepi0M_Line = StrippingLine(Lc2peepi0M_name+"Line",
                                           prescale = config["Lc2peepi0MPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selLc2peepi0M,
                                           RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  e+  e-  pi0]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  p+ ^e+  e-  pi0]CC"  : "ConeIso05e1",
                                             "[Lambda_c+ ->  p+  e+ ^e-  pi0]CC"  : "ConeIso05e2",
                                             "[Lambda_c+ ->  p+  e+  e- ^pi0]CC"  : "ConeIso05pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  e+  e-  pi0]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  p+ ^e+  e-  pi0]CC"  : "ConeIso10e1",
                                             "[Lambda_c+ ->  p+  e+ ^e-  pi0]CC"  : "ConeIso10e2",
                                             "[Lambda_c+ ->  p+  e+  e- ^pi0]CC"  : "ConeIso10pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  e+  e-  pi0]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  p+ ^e+  e-  pi0]CC"  : "ConeIso15e1",
                                             "[Lambda_c+ ->  p+  e+ ^e-  pi0]CC"  : "ConeIso15e2",
                                             "[Lambda_c+ ->  p+  e+  e- ^pi0]CC"  : "ConeIso15pi",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  e+  e-  pi0]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  p+ ^e+  e-  pi0]CC"  : "TrackIsoBDTe1",
                                             "[Lambda_c+ ->  p+  e+ ^e-  pi0]CC"  : "TrackIsoBDTe2",
                                             #"[Lambda_c+ ->  p+  e+  e- ^pi0]CC"  : "TrackIsoBDTpi", #?
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  e+  e-  pi0]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  p+ ^e+  e-  pi0]CC"  : "BsMuMuTrackIsoe1",
                                             "[Lambda_c+ ->  p+  e+ ^e-  pi0]CC"  : "BsMuMuTrackIsoe2",
                                             #"[Lambda_c+ ->  p+  e+  e- ^pi0]CC"  : "BsMuMuTrackIsopi", #?
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline

        self.Lc2pemupi0R_Line = StrippingLine(Lc2pemupi0R_name+"Line",
                                           prescale = config["Lc2pemupi0RPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selLc2pemupi0R,
                                           RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l  pi0]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  X ^l  l  pi0]CC"  : "ConeIso05e",
                                             "[Lambda_c+ ->  X  l ^l  pi0]CC"  : "ConeIso05mu",
                                             "[Lambda_c+ ->  X  l  l ^pi0]CC"  : "ConeIso05pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l  pi0]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  X ^l  l  pi0]CC"  : "ConeIso10e",
                                             "[Lambda_c+ ->  X  l ^l  pi0]CC"  : "ConeIso10mu",
                                             "[Lambda_c+ ->  X  l  l ^pi0]CC"  : "ConeIso10pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l  pi0]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  X ^l  l  pi0]CC"  : "ConeIso15e",
                                             "[Lambda_c+ ->  X  l ^l  pi0]CC"  : "ConeIso15mu",
                                             "[Lambda_c+ ->  X  l  l ^pi0]CC"  : "ConeIso15pi",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l  pi0]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  X ^l  l  pi0]CC"  : "TrackIsoBDTe",
                                             "[Lambda_c+ ->  X  l ^l  pi0]CC"  : "TrackIsoBDTmu",
                                             #"[Lambda_c+ ->  X  l  l ^pi0]CC"  : "TrackIsoBDTpi", #?
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l  pi0]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  X ^l  l  pi0]CC"  : "BsMuMuTrackIsoe",
                                             "[Lambda_c+ ->  X  l ^l  pi0]CC"  : "BsMuMuTrackIsomu",
                                             #[Lambda_c+ ->  X  l  l ^pi0]CC"  : "BsMuMuTrackIsopi", #?
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline

        self.Lc2pemupi0M_Line = StrippingLine(Lc2pemupi0M_name+"Line",
                                           prescale = config["Lc2pemupi0MPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selLc2pemupi0M,
                                           RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l  pi0]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  X ^l  l  pi0]CC"  : "ConeIso05e",
                                             "[Lambda_c+ ->  X  l ^l  pi0]CC"  : "ConeIso05mu",
                                             "[Lambda_c+ ->  X  l  l ^pi0]CC"  : "ConeIso05pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l  pi0]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  X ^l  l  pi0]CC"  : "ConeIso10e",
                                             "[Lambda_c+ ->  X  l ^l  pi0]CC"  : "ConeIso10mu",
                                             "[Lambda_c+ ->  X  l  l ^pi0]CC"  : "ConeIso10pi",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l  pi0]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  X ^l  l  pi0]CC"  : "ConeIso15e",
                                             "[Lambda_c+ ->  X  l ^l  pi0]CC"  : "ConeIso15mu",
                                             "[Lambda_c+ ->  X  l  l ^pi0]CC"  : "ConeIso15pi",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l  pi0]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  X ^l  l  pi0]CC"  : "TrackIsoBDTe",
                                             "[Lambda_c+ ->  X  l ^l  pi0]CC"  : "TrackIsoBDTmu",
                                             #"[Lambda_c+ ->  X  l  l ^pi0]CC"  : "TrackIsoBDTpi", #?
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^X  l  l  pi0]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  X ^l  l  pi0]CC"  : "BsMuMuTrackIsoe",
                                             "[Lambda_c+ ->  X  l ^l  pi0]CC"  : "BsMuMuTrackIsomu",
                                             #"[Lambda_c+ ->  X  l  l ^pi0]CC"  : "BsMuMuTrackIsopi", #?
                                             }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline

        self.Lc2pKpipi0R_Line = StrippingLine(Lc2pKpipi0R_name+"Line",
                                          prescale = config["Lc2pKpipi0RPrescale"],
                                          postscale = config["Postscale"],
                                          MDSTFlag = config["MDSTflag"],
                                          selection = self.selLc2pKpipi0R,
                                          RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+  pi0]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  p+ ^K-  pi+  pi0]CC"  : "ConeIso05K",
                                             "[Lambda_c+ ->  p+  K- ^pi+  pi0]CC"  : "ConeIso05pi",
                                             "[Lambda_c+ ->  p+  K- ^pi+ ^pi0]CC"  : "ConeIso05pi0",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+  pi0]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  p+ ^K-  pi+  pi0]CC"  : "ConeIso10K",
                                             "[Lambda_c+ ->  p+  K- ^pi+  pi0]CC"  : "ConeIso10pi",
                                             "[Lambda_c+ ->  p+  K- ^pi+ ^pi0]CC"  : "ConeIso10pi0",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+  pi0]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  p+ ^K-  pi+  pi0]CC"  : "ConeIso15K",
                                             "[Lambda_c+ ->  p+  K- ^pi+  pi0]CC"  : "ConeIso15pi",
                                             "[Lambda_c+ ->  p+  K- ^pi+ ^pi0]CC"  : "ConeIso15pi0",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+  pi0]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  p+ ^K-  pi+  pi0]CC"  : "TrackIsoBDTK",
                                             "[Lambda_c+ ->  p+  K- ^pi+  pi0]CC"  : "TrackIsoBDTpi",
                                             #"[Lambda_c+ ->  p+  K- ^pi+ ^pi0]CC"  : "TrackIsoBDTpi0",
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+  pi0]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  p+ ^K-  pi+  pi0]CC"  : "BsMuMuTrackIsoK",
                                             "[Lambda_c+ ->  p+  K- ^pi+  pi0]CC"  : "BsMuMuTrackIsopi",
                                             #"[Lambda_c+ ->  p+  K- ^pi+ ^pi0]CC"  : "BsMuMuTrackIsopi0",
                                            }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline

        self.Lc2pKpipi0M_Line = StrippingLine(Lc2pKpipi0M_name+"Line",
                                          prescale = config["Lc2pKpipi0MPrescale"],
                                          postscale = config["Postscale"],
                                          MDSTFlag = config["MDSTflag"],
                                          selection = self.selLc2pKpipi0M,
                                          RelatedInfoTools = [
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 0.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso05Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+  pi0]CC"  : "ConeIso05p",
                                             "[Lambda_c+ ->  p+ ^K-  pi+  pi0]CC"  : "ConeIso05K",
                                             "[Lambda_c+ ->  p+  K- ^pi+  pi0]CC"  : "ConeIso05pi",
                                             "[Lambda_c+ ->  p+  K- ^pi+ ^pi0]CC"  : "ConeIso05pi0",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.0,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso10Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+  pi0]CC"  : "ConeIso10p",
                                             "[Lambda_c+ ->  p+ ^K-  pi+  pi0]CC"  : "ConeIso10K",
                                             "[Lambda_c+ ->  p+  K- ^pi+  pi0]CC"  : "ConeIso10pi",
                                             "[Lambda_c+ ->  p+  K- ^pi+ ^pi0]CC"  : "ConeIso10pi0",
                                             }, },
                                           { "Type" : "RelInfoConeVariables",
                                             "ConeAngle" : 1.5,
                                             "Variables" : [],
                                             "Location"  : 'ConeIso15Lc',
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+  pi0]CC"  : "ConeIso15p",
                                             "[Lambda_c+ ->  p+ ^K-  pi+  pi0]CC"  : "ConeIso15K",
                                             "[Lambda_c+ ->  p+  K- ^pi+  pi0]CC"  : "ConeIso15pi",
                                             "[Lambda_c+ ->  p+  K- ^pi+ ^pi0]CC"  : "ConeIso15pi0",
                                             }, },
                                           { "Type": "RelInfoTrackIsolationBDT",
                                             "Variables" : 0,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+  pi0]CC"  : "TrackIsoBDTp",
                                             "[Lambda_c+ ->  p+ ^K-  pi+  pi0]CC"  : "TrackIsoBDTK",
                                             "[Lambda_c+ ->  p+  K- ^pi+  pi0]CC"  : "TrackIsoBDTpi",
                                             #"[Lambda_c+ ->  p+  K- ^pi+ ^pi0]CC"  : "TrackIsoBDTpi0",
                                             }, },
                                          { "Type" : "RelInfoBs2MuMuTrackIsolations",
                                             "Variables" : [],
                                             "IsoTwoBody" : True,
                                             "DaughterLocations" : {
                                             "[Lambda_c+ -> ^p+  K-  pi+  pi0]CC"  : "BsMuMuTrackIsop",
                                             "[Lambda_c+ ->  p+ ^K-  pi+  pi0]CC"  : "BsMuMuTrackIsoK",
                                             "[Lambda_c+ ->  p+  K- ^pi+  pi0]CC"  : "BsMuMuTrackIsopi",
                                             #"[Lambda_c+ ->  p+  K- ^pi+ ^pi0]CC"  : "BsMuMuTrackIsopi0",
                                            }, },
                                           ] + config["CommonRelInfoTools"] # end of RelatedInfoTools
                                        )# closes Strippingline

#Tagged
        self.SigmaczLc2pmumu_Line = StrippingLine(SigmaczLc2pmumu_name+"Line",
                                           prescale = config["SigmaczSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2pmumu
                                        )# closes Strippingline

        self.SigmacppLc2pmumu_Line = StrippingLine(SigmacppLc2pmumu_name+"Line",
                                           prescale = config["SigmacppSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2pmumu
                                        )# closes Strippingline

        self.SigmaczLc2pee_Line = StrippingLine(SigmaczLc2pee_name+"Line",
                                           prescale = config["SigmaczSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2pee
                                        )# closes Strippingline

        self.SigmacppLc2pee_Line = StrippingLine(SigmacppLc2pee_name+"Line",
                                           prescale = config["SigmacppSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2pee
                                        )# closes Strippingline

        self.SigmaczLc2pemu_Line = StrippingLine(SigmaczLc2pemu_name+"Line",
                                           prescale = config["SigmaczSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2pemu
                                        )# closes Strippingline

        self.SigmacppLc2pemu_Line = StrippingLine(SigmacppLc2pemu_name+"Line",
                                           prescale = config["SigmacppSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2pemu
                                        )# closes Strippingline

        self.SigmaczLc2pKpi_Line = StrippingLine(SigmaczLc2pKpi_name+"Line",
                                           prescale = config["SigmaczRefPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2pKpi
                                        )# closes Strippingline

        self.SigmacppLc2pKpi_Line = StrippingLine(SigmacppLc2pKpi_name+"Line",
                                           prescale = config["SigmacppRefPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2pKpi
                                        )# closes Strippingline
#4-body tagged
        self.SigmaczLc2pmumupi0R_Line = StrippingLine(SigmaczLc2pmumupi0R_name+"Line",
                                           prescale = config["SigmaczSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2pmumupi0R
                                        )# closes Strippingline
        self.SigmacppLc2pmumupi0R_Line = StrippingLine(SigmacppLc2pmumupi0R_name+"Line",
                                           prescale = config["SigmacppSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2pmumupi0R
                                        )# closes Strippingline
        self.SigmaczLc2pmumupi0M_Line = StrippingLine(SigmaczLc2pmumupi0M_name+"Line",
                                           prescale = config["SigmaczSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2pmumupi0M
                                        )# closes Strippingline
        self.SigmacppLc2pmumupi0M_Line = StrippingLine(SigmacppLc2pmumupi0M_name+"Line",
                                           prescale = config["SigmacppSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2pmumupi0M
                                        )# closes Strippingline

        self.SigmaczLc2peepi0R_Line = StrippingLine(SigmaczLc2peepi0R_name+"Line",
                                           prescale = config["SigmaczSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2peepi0R
                                        )# closes Strippingline
        self.SigmacppLc2peepi0R_Line = StrippingLine(SigmacppLc2peepi0R_name+"Line",
                                           prescale = config["SigmacppSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2peepi0R
                                        )# closes Strippingline
        self.SigmaczLc2peepi0M_Line = StrippingLine(SigmaczLc2peepi0M_name+"Line",
                                           prescale = config["SigmaczSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2peepi0M
                                        )# closes Strippingline
        self.SigmacppLc2peepi0M_Line = StrippingLine(SigmacppLc2peepi0M_name+"Line",
                                           prescale = config["SigmacppSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2peepi0M
                                        )# closes Strippingline

        self.SigmaczLc2pemupi0R_Line = StrippingLine(SigmaczLc2pemupi0R_name+"Line",
                                           prescale = config["SigmaczSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2pemupi0R
                                        )# closes Strippingline
        self.SigmacppLc2pemupi0R_Line = StrippingLine(SigmacppLc2pemupi0R_name+"Line",
                                           prescale = config["SigmacppSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2pemupi0R
                                        )# closes Strippingline
        self.SigmaczLc2pemupi0M_Line = StrippingLine(SigmaczLc2pemupi0M_name+"Line",
                                           prescale = config["SigmaczSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2pemupi0M
                                        )# closes Strippingline
        self.SigmacppLc2pemupi0M_Line = StrippingLine(SigmacppLc2pemupi0M_name+"Line",
                                           prescale = config["SigmacppSignalPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2pemupi0M
                                        )# closes Strippingline

        self.SigmaczLc2pKpipi0R_Line = StrippingLine(SigmaczLc2pKpipi0R_name+"Line",
                                           prescale = config["SigmaczRefPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2pKpipi0R
                                        )# closes Strippingline
        self.SigmacppLc2pKpipi0R_Line = StrippingLine(SigmacppLc2pKpipi0R_name+"Line",
                                           prescale = config["SigmacppRefPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2pKpipi0R
                                        )# closes Strippingline
        self.SigmaczLc2pKpipi0M_Line = StrippingLine(SigmaczLc2pKpipi0M_name+"Line",
                                           prescale = config["SigmaczRefPi0MPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmaczLc2pKpipi0M
                                        )# closes Strippingline
        self.SigmacppLc2pKpipi0M_Line = StrippingLine(SigmacppLc2pKpipi0M_name+"Line",
                                           prescale = config["SigmacppRefPi0MPrescale"],
                                           postscale = config["Postscale"],
                                           MDSTFlag = config["MDSTflag"],
                                           selection = self.selSigmacppLc2pKpipi0M
                                        )# closes Strippingline

#######################################################################################################

        ### Signal ch.
        self.registerLine(self.Lc23mu_Line)
        self.registerLine(self.Lc2muee_Line)
        self.registerLine(self.Lc2pmumu_Line)
        self.registerLine(self.Lc2pee_Line)
        self.registerLine(self.Lc2pemu_Line)
#
        self.registerLine(self.Lc2pmumupi0R_Line)
        self.registerLine(self.Lc2pmumupi0M_Line)
        self.registerLine(self.Lc2peepi0R_Line)
        self.registerLine(self.Lc2peepi0M_Line)
        self.registerLine(self.Lc2pemupi0R_Line)
        self.registerLine(self.Lc2pemupi0M_Line)

        ### Control ch.
        self.registerLine(self.Lc2pKpi_Line)
        self.registerLine(self.Lc2pKpipi0R_Line)
        self.registerLine(self.Lc2pKpipi0M_Line)

        ### Tagged signal ch.
        self.registerLine(self.SigmaczLc2pmumu_Line)
        self.registerLine(self.SigmacppLc2pmumu_Line)
        self.registerLine(self.SigmaczLc2pee_Line)
        self.registerLine(self.SigmacppLc2pee_Line)
        self.registerLine(self.SigmaczLc2pemu_Line)
        self.registerLine(self.SigmacppLc2pemu_Line)
#
        self.registerLine(self.SigmaczLc2pmumupi0R_Line)
        self.registerLine(self.SigmacppLc2pmumupi0R_Line)
        self.registerLine(self.SigmaczLc2pmumupi0M_Line)
        self.registerLine(self.SigmacppLc2pmumupi0M_Line)

        self.registerLine(self.SigmaczLc2peepi0R_Line)
        self.registerLine(self.SigmacppLc2peepi0R_Line)
        self.registerLine(self.SigmaczLc2peepi0M_Line)
        self.registerLine(self.SigmacppLc2peepi0M_Line)

        self.registerLine(self.SigmaczLc2pemupi0R_Line)
        self.registerLine(self.SigmacppLc2pemupi0R_Line)
        self.registerLine(self.SigmaczLc2pemupi0M_Line)
        self.registerLine(self.SigmacppLc2pemupi0M_Line)


        ### Tagged control ch.
        self.registerLine(self.SigmaczLc2pKpi_Line)
        self.registerLine(self.SigmacppLc2pKpi_Line)
#
        self.registerLine(self.SigmaczLc2pKpipi0R_Line)
        self.registerLine(self.SigmacppLc2pKpipi0R_Line)

        self.registerLine(self.SigmaczLc2pKpipi0M_Line)
        self.registerLine(self.SigmacppLc2pKpipi0M_Line)



#######################################################################################################

    def makeLc23mu(self,name):

        Lc23mu = DaVinci__N3BodyDecays("Combine"+name)
        Lc23mu.DecayDescriptors = [ "[Lambda_c+ -> mu+ mu+ mu-]cc","[Lambda_c+ -> mu+ mu+ mu+]cc" ]

        Lc23mu.DaughtersCuts = { "mu+" : self.TrackCuts }

        Lc23mu.Combination12Cut = self.Combination12Cuts
        Lc23mu.CombinationCut   = self.CombinationCutsLoose

        Lc23mu.MotherCut = self.MotherCuts

        _myMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")

        return Selection (name, Algorithm = Lc23mu, RequiredSelections = [ _myMuons ])

#######################################################################################################

    def makeLc2muee(self,name):

        Lc2muee = DaVinci__N3BodyDecays("Combine"+name)
        Lc2muee.DecayDescriptors = [ "[Lambda_c+ -> mu+ e+ e-]cc",
                                     "[Lambda_c+ -> mu- e+ e+]cc",
                                     "[Lambda_c+ -> mu+ e+ e+]cc"]

        Lc2muee.DaughtersCuts = { "mu+" : self.TrackCuts,
                                  "e+"  : self.TrackCuts + " & ((PIDe-PIDpi)>2)" }

        Lc2muee.Combination12Cut = self.Combination12Cuts
        Lc2muee.CombinationCut   = self.CombinationCutsLoose
        Lc2muee.MotherCut        = self.MotherCuts

        _myMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
        _myElectrons = DataOnDemand(Location = "Phys/StdLooseElectrons/Particles")

        return Selection (name, Algorithm = Lc2muee, RequiredSelections = [ _myMuons, _myElectrons ])


#######################################################################################################

    def makeLc2pmumu(self,name):

        Lc2pmumu = DaVinci__N3BodyDecays("Combine"+name)
        Lc2pmumu.DecayDescriptors = [ "[Lambda_c+ -> p+ mu+ mu-]cc",
                                      "[Lambda_c+ -> p~- mu+ mu+]cc",
                                      "[Lambda_c+ -> p+ mu+ mu+]cc"]

        Lc2pmumu.DaughtersCuts = { "p+"  : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                   "mu+" : self.TrackCuts + " & ((PIDmu-PIDpi)>-5)" + " & ((PIDmu-PIDK)>-5)" }

        Lc2pmumu.Combination12Cut = self.Combination12Cuts
        Lc2pmumu.CombinationCut   = self.CombinationCutsLoose
        Lc2pmumu.MotherCut        = self.MotherCuts

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")

        return Selection (name, Algorithm = Lc2pmumu, RequiredSelections = [ _myProtons, _myMuons ])

#######################################################################################################

    def makeLc2pee(self,name):

        Lc2pee = DaVinci__N3BodyDecays("Combine"+name)
        Lc2pee.DecayDescriptors = [ "[Lambda_c+ -> p+ e+ e-]cc",
                                "[Lambda_c+ -> p~- e+ e+]cc",
                                "[Lambda_c+ -> p+ e+ e+]cc"]

        Lc2pee.DaughtersCuts = { "p+" : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                 "e+" : self.TrackCuts + " & ((PIDe-PIDpi)>2)" }

        Lc2pee.Combination12Cut = self.Combination12Cuts
        Lc2pee.CombinationCut   = self.CombinationCutsLoose
        Lc2pee.MotherCut        = self.MotherCuts

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myElectrons = DataOnDemand(Location = "Phys/StdLooseElectrons/Particles")

        return Selection (name, Algorithm = Lc2pee, RequiredSelections = [ _myProtons, _myElectrons ])

#######################################################################################################

    def makeLc2pKpi(self,name):

        Lc2pKpi = DaVinci__N3BodyDecays("Combine"+name)
        Lc2pKpi.DecayDescriptors = [ "[Lambda_c+ -> p+ K- pi+]cc" ]

        Lc2pKpi.DaughtersCuts = { "p+"  : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                  "K-"  : self.TrackCuts + " & ((PIDK-PIDpi)>5)" + " & ((PIDK-PIDp)>0)",
                                  "pi+" : self.TrackCuts }

        Lc2pKpi.Combination12Cut = self.Combination12Cuts
        Lc2pKpi.CombinationCut   = self.CombinationCutsTight
        Lc2pKpi.MotherCut        = self.MotherCuts

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myKaons = DataOnDemand(Location = "Phys/StdLooseKaons/Particles")
        _myPions = DataOnDemand(Location = "Phys/StdLoosePions/Particles")

        return Selection (name, Algorithm = Lc2pKpi, RequiredSelections = [ _myProtons, _myKaons, _myPions ])

#######################################################################################################

    def makeLc2pemu(self,name):

        Lc2pemu = DaVinci__N3BodyDecays("Combine"+name)
        Lc2pemu.DecayDescriptors = [ "[Lambda_c+ -> p+ e+ mu-]cc", "[Lambda_c+ -> p+ e- mu+]cc",
                                "[Lambda_c+ -> p~- e+ mu+]cc",
                                "[Lambda_c+ -> p+ e+ mu+]cc"]

        Lc2pemu.DaughtersCuts = { "p+" : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                  "e+" : self.TrackCuts + " & ((PIDe-PIDpi)>2)",
                                  "mu-" : self.TrackCuts + " & ((PIDmu-PIDpi)>-5)" + " & ((PIDmu-PIDK)>-5)" }

        Lc2pemu.Combination12Cut = self.Combination12Cuts
        Lc2pemu.CombinationCut   = self.CombinationCutsLoose
        Lc2pemu.MotherCut        = self.MotherCuts

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myElectrons = DataOnDemand(Location = "Phys/StdLooseElectrons/Particles")
        _myMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")

        return Selection (name, Algorithm = Lc2pemu, RequiredSelections = [ _myProtons, _myElectrons, _myMuons ])

###4-body###
#######################################################################################################

    def makeLc2pmumupi0R(self,name):

        Lc2pmumupi0R = DaVinci__N4BodyDecays("Combine"+name)
        Lc2pmumupi0R.DecayDescriptors = [ "[Lambda_c+ -> p+ mu+ mu- pi0]cc" ]

        Lc2pmumupi0R.DaughtersCuts = { "p+"  : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                    "mu+" : self.TrackCuts + " & ((PIDmu-PIDpi)>-5)" + " & ((PIDmu-PIDK)>-5)",
                                    "pi0" : self.Pi0RCuts }

        Lc2pmumupi0R.Combination12Cut  = self.Combination12Cuts
        Lc2pmumupi0R.Combination123Cut = self.Combination123Cuts
        Lc2pmumupi0R.CombinationCut    = self.Combination4BodyCutsLoose
        Lc2pmumupi0R.MotherCut         = self.Mother4BodyCuts #or MotherCuts enough?

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
        _myPi0R = DataOnDemand(Location = "Phys/StdLooseResolvedPi0/Particles")

        return Selection (name, Algorithm = Lc2pmumupi0R, RequiredSelections = [ _myProtons, _myMuons, _myPi0R ])

#######################################################################################################

    def makeLc2pmumupi0M(self,name):

        Lc2pmumupi0M = DaVinci__N4BodyDecays("Combine"+name)
        Lc2pmumupi0M.DecayDescriptors = [ "[Lambda_c+ -> p+ mu+ mu- pi0]cc"]

        Lc2pmumupi0M.DaughtersCuts = { "p+"  : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                    "mu+" : self.TrackCuts + " & ((PIDmu-PIDpi)>-5)" + " & ((PIDmu-PIDK)>-5)",
                                    "pi0" : self.Pi0MCuts }

        Lc2pmumupi0M.Combination12Cut = self.Combination12Cuts
        Lc2pmumupi0M.Combination123Cut = self.Combination123Cuts
        Lc2pmumupi0M.CombinationCut   = self.Combination4BodyCutsLoose
        Lc2pmumupi0M.MotherCut        = self.Mother4BodyCuts #or MotherCuts enough?

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
        _myPi0M = DataOnDemand(Location = "Phys/StdLooseMergedPi0/Particles")

        return Selection (name, Algorithm = Lc2pmumupi0M, RequiredSelections = [ _myProtons, _myMuons, _myPi0M ])

#######################################################################################################

    def makeLc2peepi0R(self,name):

        Lc2peepi0R = DaVinci__N4BodyDecays("Combine"+name)
        Lc2peepi0R.DecayDescriptors = [ "[Lambda_c+ -> p+ e+ e- pi0]cc" ]

        Lc2peepi0R.DaughtersCuts = { "p+"  : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                    "e+" : self.TrackCuts + " & ((PIDe-PIDpi)>2)",
                                    "pi0" : self.Pi0RCuts }

        Lc2peepi0R.Combination12Cut  = self.Combination12Cuts
        Lc2peepi0R.Combination123Cut = self.Combination123Cuts
        Lc2peepi0R.CombinationCut    = self.Combination4BodyCutsLoose
        Lc2peepi0R.MotherCut         = self.Mother4BodyCuts #or MotherCuts enough?

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myElectrons = DataOnDemand(Location = "Phys/StdLooseElectrons/Particles")
        _myPi0R = DataOnDemand(Location = "Phys/StdLooseResolvedPi0/Particles")

        return Selection (name, Algorithm = Lc2peepi0R, RequiredSelections = [ _myProtons, _myElectrons, _myPi0R ])

#######################################################################################################

    def makeLc2peepi0M(self,name):

        Lc2peepi0M = DaVinci__N4BodyDecays("Combine"+name)
        Lc2peepi0M.DecayDescriptors = [ "[Lambda_c+ -> p+ e+ e- pi0]cc" ]

        Lc2peepi0M.DaughtersCuts = { "p+"  : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                    "e+" : self.TrackCuts + " & ((PIDe-PIDpi)>2)",
                                    "pi0" : self.Pi0MCuts }

        Lc2peepi0M.Combination12Cut  = self.Combination12Cuts
        Lc2peepi0M.Combination123Cut = self.Combination123Cuts
        Lc2peepi0M.CombinationCut    = self.Combination4BodyCutsLoose
        Lc2peepi0M.MotherCut         = self.Mother4BodyCuts #or MotherCuts enough?

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myElectrons = DataOnDemand(Location = "Phys/StdLooseElectrons/Particles")
        _myPi0M = DataOnDemand(Location = "Phys/StdLooseMergedPi0/Particles")

        return Selection (name, Algorithm = Lc2peepi0M, RequiredSelections = [ _myProtons, _myElectrons, _myPi0M ])

#######################################################################################################

    def makeLc2pemupi0R(self,name):

        Lc2pemupi0R = DaVinci__N4BodyDecays("Combine"+name)
        Lc2pemupi0R.DecayDescriptors = [ "[Lambda_c+ -> p+ e+ mu- pi0]cc", "[Lambda_c+ -> p+ e- mu+ pi0]cc" ]

        Lc2pemupi0R.DaughtersCuts = { "p+"  : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                    "e+" : self.TrackCuts + " & ((PIDe-PIDpi)>2)",
                                    "mu-" : self.TrackCuts + " & ((PIDmu-PIDpi)>-5)" + " & ((PIDmu-PIDK)>-5)",
                                    "pi0" : self.Pi0RCuts }

        Lc2pemupi0R.Combination12Cut  = self.Combination12Cuts
        Lc2pemupi0R.Combination123Cut = self.Combination123Cuts
        Lc2pemupi0R.CombinationCut    = self.Combination4BodyCutsLoose
        Lc2pemupi0R.MotherCut         = self.Mother4BodyCuts #or MotherCuts enough?

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myElectrons = DataOnDemand(Location = "Phys/StdLooseElectrons/Particles")
        _myMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
        _myPi0R = DataOnDemand(Location = "Phys/StdLooseResolvedPi0/Particles")

        return Selection (name, Algorithm = Lc2pemupi0R, RequiredSelections = [ _myProtons, _myElectrons, _myMuons, _myPi0R ])

#######################################################################################################

    def makeLc2pemupi0M(self,name):

        Lc2pemupi0M = DaVinci__N4BodyDecays("Combine"+name)
        Lc2pemupi0M.DecayDescriptors = [ "[Lambda_c+ -> p+ e+ mu- pi0]cc", "[Lambda_c+ -> p+ e- mu+ pi0]cc" ]

        Lc2pemupi0M.DaughtersCuts = { "p+"  : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                    "e+" : self.TrackCuts + " & ((PIDe-PIDpi)>2)",
                                    "mu-" : self.TrackCuts + " & ((PIDmu-PIDpi)>-5)" + " & ((PIDmu-PIDK)>-5)",
                                    "pi0" : self.Pi0MCuts }

        Lc2pemupi0M.Combination12Cut  = self.Combination12Cuts
        Lc2pemupi0M.Combination123Cut = self.Combination123Cuts
        Lc2pemupi0M.CombinationCut    = self.Combination4BodyCutsLoose
        Lc2pemupi0M.MotherCut         = self.Mother4BodyCuts #or MotherCuts enough?

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myElectrons = DataOnDemand(Location = "Phys/StdLooseElectrons/Particles")
        _myMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
        _myPi0M = DataOnDemand(Location = "Phys/StdLooseMergedPi0/Particles")

        return Selection (name, Algorithm = Lc2pemupi0M, RequiredSelections = [ _myProtons, _myElectrons, _myMuons, _myPi0M ])

#######################################################################################################

    def makeLc2pKpipi0R(self,name):

        Lc2pKpipi0R = DaVinci__N4BodyDecays("Combine"+name)
        Lc2pKpipi0R.DecayDescriptors = [ "[Lambda_c+ -> p+ K- pi+ pi0]cc" ]

        Lc2pKpipi0R.DaughtersCuts = { "p+"  : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                    "K-"  : self.TrackCuts + " & ((PIDK-PIDpi)>5)" + " & ((PIDK-PIDp)>0)",
                                    "pi+" : self.TrackCuts,
                                    "pi0" : self.Pi0RCuts }

        Lc2pKpipi0R.Combination12Cut  = self.Combination12Cuts
        Lc2pKpipi0R.Combination123Cut = self.Combination123Cuts
        Lc2pKpipi0R.CombinationCut    = self.Combination4BodyCutsLoose
        Lc2pKpipi0R.MotherCut         = self.Mother4BodyCuts #or MotherCuts enough?

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myKaons = DataOnDemand(Location = "Phys/StdLooseKaons/Particles")
        _myPions = DataOnDemand(Location = "Phys/StdLoosePions/Particles")
        _myPi0R = DataOnDemand(Location = "Phys/StdLooseResolvedPi0/Particles")

        return Selection (name, Algorithm = Lc2pKpipi0R, RequiredSelections = [ _myProtons, _myKaons, _myPions, _myPi0R ])

#######################################################################################################

    def makeLc2pKpipi0M(self,name):

        Lc2pKpipi0M = DaVinci__N4BodyDecays("Combine"+name)
        Lc2pKpipi0M.DecayDescriptors = [ "[Lambda_c+ -> p+ K- pi+ pi0]cc" ]

        Lc2pKpipi0M.DaughtersCuts = { "p+"  : self.TrackCuts + " & ((PIDp-PIDpi)>5)" + " & ((PIDp-PIDK)>0)",
                                    "K-"  : self.TrackCuts + " & ((PIDK-PIDpi)>5)" + " & ((PIDK-PIDp)>0)",
                                    "pi+" : self.TrackCuts,
                                    "pi0" : self.Pi0MCuts }

        Lc2pKpipi0M.Combination12Cut  = self.Combination12Cuts
        Lc2pKpipi0M.Combination123Cut = self.Combination123Cuts
        Lc2pKpipi0M.CombinationCut    = self.Combination4BodyCutsLoose
        Lc2pKpipi0M.MotherCut         = self.Mother4BodyCuts #or MotherCuts enough?

        _myProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")
        _myKaons = DataOnDemand(Location = "Phys/StdLooseKaons/Particles")
        _myPions = DataOnDemand(Location = "Phys/StdLoosePions/Particles")
        _myPi0M = DataOnDemand(Location = "Phys/StdLooseMergedPi0/Particles")

        return Selection (name, Algorithm = Lc2pKpipi0M, RequiredSelections = [ _myProtons, _myKaons, _myPions, _myPi0M ])

###tagged###
#######################################################################################################

    def makeSigmac2Lcpi(self, name, inputSel, decDescriptors):

        #SigmacCombinationCuts = '((AM - AM1) < {0[Sigmac_AMDiff_MAX]})'.format(self.config)
        #SigmacMotherCuts = '(VFASPF(VCHI2/VDOF) < {0[Sigmac_VCHI2VDOF_MAX]})'.format(self.config)

        Sigmac2Lcpi = CombineParticles(
            name = "Combine"+name, # name='Combine{0}'.format(name),
            DecayDescriptors = decDescriptors,
            CombinationCut = self.SigmacCombinationCuts,
            MotherCut = self.SigmacMotherCuts
        )
        _mySoftPions = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")

        return Selection(name, Algorithm = Sigmac2Lcpi, RequiredSelections = [inputSel, _mySoftPions])

#######################################################################################################
