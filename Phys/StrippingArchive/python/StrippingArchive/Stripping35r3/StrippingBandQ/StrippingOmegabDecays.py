###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This module supersedes StrippingOmegab2XicKpi.py
Module for construction of Omegab Decays Stripping Selections and StrippingLines.
Provides functions to build Xic, Omegac, Omegab selections.
Provides class OmegabDecaysConf, which constructs the Selections and
StrippingLines given a configuration dictionary.
Exported selection makers: 'makeXic', 'makeOmegac', 'makeXic0', 'makeOmegab2XicKpi', 'makeOmegab2XicgammaKpi', 'makeOmegab2Omegacpi', 'makeOmegab2Omegacpi0pi', 'makeOmegab2Omegacgammapi'
"""

__author__ = ['Marco Pappagallo']
__date__ = '23/01/2019'
__version__ = '$Revision: 1.0 $'

__all__ = ('OmegabDecaysConf',
           'makeXic',
           'makeOmegac',
           'makeXic0',
           'makeOmegab2XicKpi',
           'makeOmegab2XicgammaKpi',
           'makeOmegab2Omegacpi',
           'makeOmegab2Omegacpi0pi',
           'makeOmegab2Omegacgammapi',
           'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

#default_name = ''
default_config = {
    'NAME'              : 'OmegabDecays',
    'BUILDERTYPE'       : 'OmegabDecaysConf',
    'CONFIG'    : {
                   # Omegab
                   'MinOmegabMass'       : 5500.     # MeV
                   ,'MaxOmegabMass'       : 6700.     # MeV
                   ,'Omegab_PT'           : 3500.     # MeV
                   ,'OmegabVertChi2DOF'   : 10        # Dimensionless
                   ,'OmegabLT'            : 0.2       # ps
                   ,'OmegabIPCHI2'        : 25        # Dimensionless
                   ,'OmegabDIRA'          : 0.999     # Dimensionless
                   ,'KaonProbNN'          : 0.1       # Dimensionless
                   ,'PionProbNN'          : 0.1       # Dimensionless
                   ,'ProtProbNN'          : 0.1       # Dimensionless
                   ,'TrackMinIPCHI2'      : 4         # Dimensionless
                   ,'XicPT'               : 1800      # MeV
                   ,'XicBPVVDCHI2'        : 36
                   ,'XicDIRA'             : 0.        # Dimensionless
                   ,'XicDeltaMassWin'     : 100       # MeV
                   ,'XicprDeltaMassWin'   : 250       # MeV
                   ,'OmegacstDeltaMassWin': 500       # MeV
                   ,'MaxXicVertChi2DOF'   : 10        # Dimensionless
                   # Pre- and postscales
                   ,'OmegabDecaysPreScale'   : 1.0
                   ,'OmegabDecaysPostScale'  : 1.0
                  },
    'STREAMS' : [ 'Bhadron' ],
    'WGs'    : [ 'BandQ' ]
    }

class OmegabDecaysConf(LineBuilder):
    """
    Definition of Omegab -> Xic K pi stripping

    Constructs Omegab -> Xic K pi Selections and StrippingLines from
    a configuration dictionary.
    Usage:
    >>> config = { .... }
    >>> gammaConf = StrippingB2XGammaConf('StrippingB2XGammaTest',config)
    >>> gammaLines = gammaConf.lines
    >>> for line in line :
    >>>  print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selXic                       : nominal Xic+   -> p K- pi+ Selection object
    selXic0                      : nominal Xic0   -> p K- K- pi+ Selection object
    selOmegac                    : nominal Omegac -> p K- K- pi+ Selection object
    selOmegab2XicKpi             : nominal Omegab- -> Xic+ K- pi- Selection object
    selOmegab2XicKpiWS           : nominal Omegab- -> Xic+ K- pi+ Selection object
    selOmegab2XicgammaKpi        : nominal Omegab- -> Xic+ gamma K- pi- Selection object
    selOmegab2XicgammaKpiWS      : nominal Omegab- -> Xic+ gamma K- pi+ Selection object
    selOmegab2Xic0piKpi          : nominal Omegab- -> Xic0 pi+ K- pi- Selection object
    selOmegab2Xic0piKpiWS        : nominal Omegab- -> Xic0 pi+ K- pi+ Selection object
    selOmegab2Omegacpi           : nominal Omegab- -> Omegac pi- Selection object
    selOmegab2OmegacpiWS         : nominal Omegab- -> Omegac pi+ Selection object
    selOmegab2Omegacpipipi       : nominal Omegab- -> Omegac pi- pi+ pi- Selection object
    selOmegab2Omegacpi0pi        : nominal Omegab- -> Omegac pi0 pi- Selection object
    selOmegab2Omegacpi0piWS      : nominal Omegab- -> Omegac pi0 pi+ Selection object
    selOmegab2Omegacgammapi      : nominal Omegab- -> Omegac gamma pi- Selection object
    selOmegab2OmegacgammapiWS    : nominal Omegab- -> Omegac gamma pi+ Selection object
    lines                  : List of lines

    Exports as class data member:
    StrippingB2XGammaConf.__configuration_keys__ : List of required configuration parameters.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        # if name not set outside, set it to empty
        #if name == None:
        #    name = ""

        # Selection of Omegab daughters: Xic

        self.selXic = makeXic('XicFor%s' % name,
                              config['KaonProbNN'],
                              config['PionProbNN'],
                              config['ProtProbNN'],
                              config['XicPT'],
                              config['XicDeltaMassWin'],
                              config['MaxXicVertChi2DOF'],
                              config['XicBPVVDCHI2'],
                              config['XicDIRA'],
                              config['TrackMinIPCHI2'])


        self.selOmegac = makeOmegac('OmegacFor%s' % name,
                                    config['KaonProbNN'],
                                    config['PionProbNN'],
                                    config['ProtProbNN'],
                                    config['XicPT'],
                                    config['XicDeltaMassWin'],
                                    config['MaxXicVertChi2DOF'],
                                    config['XicBPVVDCHI2'],
                                    config['XicDIRA'],
                                    config['TrackMinIPCHI2'])

        self.selXic0 = makeXic0('Xic0For%s' % name,
                                config['KaonProbNN'],
                                config['PionProbNN'],
                                config['ProtProbNN'],
                                config['XicPT'],
                                config['XicDeltaMassWin'],
                                config['MaxXicVertChi2DOF'],
                                config['XicBPVVDCHI2'],
                                config['XicDIRA'],
                                config['TrackMinIPCHI2'])

        # Omegab -> Xic K pi selections
        self.selOmegab2XicKpi = makeOmegab2XicKpi('Omegab2XicKpiFor%s' % name,
                                                  self.selXic,
                                                  '[Omega_b- -> Xi_c+ K- pi-]cc',
                                                  config['KaonProbNN'],
                                                  config['PionProbNN'],
                                                  config['MaxOmegabMass'],
                                                  config['MinOmegabMass'],
                                                  config['OmegabVertChi2DOF'],
                                                  config['OmegabIPCHI2'],
                                                  config['OmegabLT'],
                                                  config['Omegab_PT'],
                                                  config['OmegabDIRA'],
                                                  config['TrackMinIPCHI2'])

        self.selOmegab2XicKpiWS = makeOmegab2XicKpi('Omegab2XicKpiWSFor%s' % name,
                                                    self.selXic,
                                                    '[Omega_b- -> Xi_c+ K- pi+]cc',
                                                    config['KaonProbNN'],
                                                    config['PionProbNN'],
                                                    config['MaxOmegabMass'],
                                                    config['MinOmegabMass'],
                                                    config['OmegabVertChi2DOF'],
                                                    config['OmegabIPCHI2'],
                                                    config['OmegabLT'],
                                                    config['Omegab_PT'],
                                                    config['OmegabDIRA'],
                                                    config['TrackMinIPCHI2'])

        # Omegab -> Xic gamma K pi selections
        self.selOmegab2XicgammaKpi = makeOmegab2XicgammaKpi('Omegab2XicgammaKpiFor%s' % name,
                                                            self.selXic,
                                                            '[Omega_b- -> Xi_c+ gamma K- pi-]cc',
                                                            config['KaonProbNN'],
                                                            config['PionProbNN'],
                                                            config['MaxOmegabMass'],
                                                            config['MinOmegabMass'],
                                                            config['OmegabVertChi2DOF'],
                                                            config['OmegabIPCHI2'],
                                                            config['OmegabLT'],
                                                            config['Omegab_PT'],
                                                            config['OmegabDIRA'],
                                                            config['TrackMinIPCHI2'],
                                                            config['XicprDeltaMassWin'])

        self.selOmegab2XicgammaKpiWS = makeOmegab2XicgammaKpi('Omegab2XicgammaKpiWSFor%s' % name,
                                                              self.selXic,
                                                              '[Omega_b- -> Xi_c+ gamma K- pi+]cc',
                                                              config['KaonProbNN'],
                                                              config['PionProbNN'],
                                                              config['MaxOmegabMass'],
                                                              config['MinOmegabMass'],
                                                              config['OmegabVertChi2DOF'],
                                                              config['OmegabIPCHI2'],
                                                              config['OmegabLT'],
                                                              config['Omegab_PT'],
                                                              config['OmegabDIRA'],
                                                              config['TrackMinIPCHI2'],
                                                              config['XicprDeltaMassWin'])

        # Omegab -> Xic0 pi K pi selections
        self.selOmegab2Xic0piKpi = makeOmegab2XicKpi('Omegab2Xic0piKpiFor%s' % name,
                                                     self.selXic0,
                                                     '[Omega_b- -> Xi_c0 pi+ K- pi-]cc',
                                                     config['KaonProbNN'],
                                                     config['PionProbNN'],
                                                     config['MaxOmegabMass'],
                                                     config['MinOmegabMass'],
                                                     config['OmegabVertChi2DOF'],
                                                     config['OmegabIPCHI2'],
                                                     config['OmegabLT'],
                                                     config['Omegab_PT'],
                                                     config['OmegabDIRA'],
                                                     config['TrackMinIPCHI2'])

        self.selOmegab2Xic0piKpiWS = makeOmegab2XicKpi('Omegab2Xic0piKpiWSFor%s' % name,
                                                       self.selXic0,
                                                       '[Omega_b- -> Xi_c0 pi+ K- pi+]cc',
                                                       config['KaonProbNN'],
                                                       config['PionProbNN'],
                                                       config['MaxOmegabMass'],
                                                       config['MinOmegabMass'],
                                                       config['OmegabVertChi2DOF'],
                                                       config['OmegabIPCHI2'],
                                                       config['OmegabLT'],
                                                       config['Omegab_PT'],
                                                       config['OmegabDIRA'],
                                                       config['TrackMinIPCHI2'])

        # Omegab -> Omegac pi selections
        self.selOmegab2Omegacpi = makeOmegab2Omegacpi('Omegab2OmegacpiFor%s' % name,
                                                      self.selOmegac,
                                                      '[Omega_b- -> Omega_c0 pi-]cc',
                                                      config['PionProbNN'],
                                                      config['MaxOmegabMass'],
                                                      config['MinOmegabMass'],
                                                      config['OmegabVertChi2DOF'],
                                                      config['OmegabIPCHI2'],
                                                      config['OmegabLT'],
                                                      config['Omegab_PT'],
                                                      config['OmegabDIRA'],
                                                      config['TrackMinIPCHI2'])

        self.selOmegab2OmegacpiWS = makeOmegab2Omegacpi('Omegab2OmegacpiWSFor%s' % name,
                                                        self.selOmegac,
                                                        '[Omega_b- -> Omega_c0 pi+]cc',
                                                        config['PionProbNN'],
                                                        config['MaxOmegabMass'],
                                                        config['MinOmegabMass'],
                                                        config['OmegabVertChi2DOF'],
                                                        config['OmegabIPCHI2'],
                                                        config['OmegabLT'],
                                                        config['Omegab_PT'],
                                                        config['OmegabDIRA'],
                                                        config['TrackMinIPCHI2'])


        # Omegab -> Omegac pi0 pi selections
        self.selOmegab2Omegacpi0pi = makeOmegab2Omegacpi0pi('Omegab2Omegacpi0piFor%s' % name,
                                                            self.selOmegac,
                                                            '[Omega_b- -> Omega_c0 pi0 pi-]cc',
                                                            config['PionProbNN'],
                                                            config['MaxOmegabMass'],
                                                            config['MinOmegabMass'],
                                                            config['OmegabVertChi2DOF'],
                                                            config['OmegabIPCHI2'],
                                                            config['OmegabLT'],
                                                            config['Omegab_PT'],
                                                            config['OmegabDIRA'],
                                                            config['TrackMinIPCHI2'])

        self.selOmegab2Omegacpi0piWS = makeOmegab2Omegacpi0pi('Omegab2Omegacpi0piWSFor%s' % name,
                                                              self.selOmegac,
                                                              '[Omega_b- -> Omega_c0 pi0 pi+]cc',
                                                              config['PionProbNN'],
                                                              config['MaxOmegabMass'],
                                                              config['MinOmegabMass'],
                                                              config['OmegabVertChi2DOF'],
                                                              config['OmegabIPCHI2'],
                                                              config['OmegabLT'],
                                                              config['Omegab_PT'],
                                                              config['OmegabDIRA'],
                                                              config['TrackMinIPCHI2'])

        # Omegab -> Omegac gamma pi selections
        self.selOmegab2Omegacgammapi = makeOmegab2Omegacgammapi('Omegab2OmegacgammapiFor%s' % name,
                                                                self.selOmegac,
                                                                '[Omega_b- -> Omega_c0 gamma pi-]cc',
                                                                config['PionProbNN'],
                                                                config['MaxOmegabMass'],
                                                                config['MinOmegabMass'],
                                                                config['OmegabVertChi2DOF'],
                                                                config['OmegabIPCHI2'],
                                                                config['OmegabLT'],
                                                                config['Omegab_PT'],
                                                                config['OmegabDIRA'],
                                                                config['TrackMinIPCHI2'],
                                                                config['OmegacstDeltaMassWin'])


        self.selOmegab2OmegacgammapiWS = makeOmegab2Omegacgammapi('Omegab2OmegacgammapiWSFor%s' % name,
                                                                  self.selOmegac,
                                                                  '[Omega_b- -> Omega_c0 gamma pi+]cc',
                                                                  config['PionProbNN'],
                                                                  config['MaxOmegabMass'],
                                                                  config['MinOmegabMass'],
                                                                  config['OmegabVertChi2DOF'],
                                                                  config['OmegabIPCHI2'],
                                                                  config['OmegabLT'],
                                                                  config['Omegab_PT'],
                                                                  config['OmegabDIRA'],
                                                                  config['TrackMinIPCHI2'],
                                                                  config['OmegacstDeltaMassWin'])

        # Omegab -> Omegac pi pi pi selections
        self.selOmegab2Omegacpipipi = makeOmegab2Omegacpi('Omegab2OmegacpipipiFor%s' % name,
                                                          self.selOmegac,
                                                          '[Omega_b- -> Omega_c0 pi+ pi- pi-]cc',
                                                          config['PionProbNN'],
                                                          config['MaxOmegabMass'],
                                                          config['MinOmegabMass'],
                                                          config['OmegabVertChi2DOF'],
                                                          config['OmegabIPCHI2'],
                                                          config['OmegabLT'],
                                                          config['Omegab_PT'],
                                                          config['OmegabDIRA'],
                                                          config['TrackMinIPCHI2'])


        self.selOmegab2OmegacpipipiWS = makeOmegab2Omegacpi('Omegab2OmegacpipipiWSFor%s' % name,
                                                            self.selOmegac,
                                                            '[Omega_b- -> Omega_c0 pi+ pi- pi+]cc',
                                                            config['PionProbNN'],
                                                            config['MaxOmegabMass'],
                                                            config['MinOmegabMass'],
                                                            config['OmegabVertChi2DOF'],
                                                            config['OmegabIPCHI2'],
                                                            config['OmegabLT'],
                                                            config['Omegab_PT'],
                                                            config['OmegabDIRA'],
                                                            config['TrackMinIPCHI2'])

        # Create and register stripping lines
        self.Omegab2XicKpiLine = StrippingLine("%sXicKpiLine" % name,
                                               prescale=config['OmegabDecaysPreScale'],
                                               postscale=config['OmegabDecaysPostScale'],
                                               selection=self.selOmegab2XicKpi)
#                                               selection=self.selXic)
        self.registerLine(self.Omegab2XicKpiLine)

        self.Omegab2XicKpiLineWS = StrippingLine("%sXicKpiWSLine" % name,
                                               prescale=config['OmegabDecaysPreScale'],
                                               postscale=config['OmegabDecaysPostScale'],
                                               selection=self.selOmegab2XicKpiWS)
        self.registerLine(self.Omegab2XicKpiLineWS)

        self.Omegab2XicgammaKpiLine = StrippingLine("%sXicgammaKpiLine" % name,
                                                    prescale=config['OmegabDecaysPreScale'],
                                                    postscale=config['OmegabDecaysPostScale'],
                                                    selection=self.selOmegab2XicgammaKpi)
        self.registerLine(self.Omegab2XicgammaKpiLine)

        self.Omegab2XicgammaKpiLineWS = StrippingLine("%sXicgammaKpiWSLine" % name,
                                                      prescale=config['OmegabDecaysPreScale'],
                                                      postscale=config['OmegabDecaysPostScale'],
                                                      selection=self.selOmegab2XicgammaKpiWS)
        self.registerLine(self.Omegab2XicgammaKpiLineWS)

        self.Omegab2Xic0piKpiLine = StrippingLine("%sXic0piKpiLine" % name,
                                                  prescale=config['OmegabDecaysPreScale'],
                                                  postscale=config['OmegabDecaysPostScale'],
                                                  selection=self.selOmegab2Xic0piKpi)
        self.registerLine(self.Omegab2Xic0piKpiLine)

        self.Omegab2Xic0piKpiLineWS = StrippingLine("%sXic0piKpiWSLine" % name,
                                                    prescale=config['OmegabDecaysPreScale'],
                                                    postscale=config['OmegabDecaysPostScale'],
                                                    selection=self.selOmegab2Xic0piKpiWS)
        self.registerLine(self.Omegab2Xic0piKpiLineWS)

        self.Omegab2OmegacpiLine = StrippingLine("%sOmegacpiLine" % name,
                                                 prescale=config['OmegabDecaysPreScale'],
                                                 postscale=config['OmegabDecaysPostScale'],
                                                 selection=self.selOmegab2Omegacpi)
        self.registerLine(self.Omegab2OmegacpiLine)

        self.Omegab2OmegacpiLineWS = StrippingLine("%sOmegacpiWSLine" % name,
                                                   prescale=config['OmegabDecaysPreScale'],
                                                   postscale=config['OmegabDecaysPostScale'],
                                                   selection=self.selOmegab2OmegacpiWS)
        self.registerLine(self.Omegab2OmegacpiLineWS)


        self.Omegab2Omegacpi0piLine = StrippingLine("%sOmegacpi0piLine" % name,
                                                    prescale=config['OmegabDecaysPreScale'],
                                                    postscale=config['OmegabDecaysPostScale'],
                                                    selection=self.selOmegab2Omegacpi0pi)
        self.registerLine(self.Omegab2Omegacpi0piLine)

        self.Omegab2Omegacpi0piLineWS = StrippingLine("%sOmegacpi0piWSLine" % name,
                                                      prescale=config['OmegabDecaysPreScale'],
                                                      postscale=config['OmegabDecaysPostScale'],
                                                      selection=self.selOmegab2Omegacpi0piWS)
        self.registerLine(self.Omegab2Omegacpi0piLineWS)

        self.Omegab2OmegacgammapiLine = StrippingLine("%sOmegacgammapiLine" % name,
                                                      prescale=config['OmegabDecaysPreScale'],
                                                      postscale=config['OmegabDecaysPostScale'],
                                                      selection=self.selOmegab2Omegacgammapi)
        self.registerLine(self.Omegab2OmegacgammapiLine)

        self.Omegab2OmegacgammapiLineWS = StrippingLine("%sOmegacgammapiWSLine" % name,
                                                        prescale=config['OmegabDecaysPreScale'],
                                                        postscale=config['OmegabDecaysPostScale'],
                                                        selection=self.selOmegab2OmegacgammapiWS)
        self.registerLine(self.Omegab2OmegacgammapiLineWS)

        self.Omegab2OmegacpipipiLine = StrippingLine("%sOmegacpipipiLine" % name,
                                                     prescale=config['OmegabDecaysPreScale'],
                                                     postscale=config['OmegabDecaysPostScale'],
                                                     selection=self.selOmegab2Omegacpipipi)
        self.registerLine(self.Omegab2OmegacpipipiLine)

        self.Omegab2OmegacpipipiLineWS = StrippingLine("%sOmegacpipipiWSLine" % name,
                                                       prescale=config['OmegabDecaysPreScale'],
                                                       postscale=config['OmegabDecaysPostScale'],
                                                       selection=self.selOmegab2OmegacpipipiWS)
        self.registerLine(self.Omegab2OmegacpipipiLineWS)

def makeXic(name, KaonProbNN, PionProbNN, ProtProbNN, XicPT, XicDeltaMassWin, MaxXicVertChi2DOF, XicBPVVDCHI2, XicDIRA, TrackMinIPCHI2):
    """
    Create and return a Xic+ -> p K- pi+ Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg KaonProbNN: PID of Kaon
    @arg PionProbNN: PID of pion
    @arg ProtProbNN: PID of proton
    @arg XicPT:      Min PT of Xic
    @arg XicDeltaMassWin: Mass difference m(pKpi)-m(Xic)
    @arg MaxXicVertChi2DOF: chi2/NDOF
    @arg XicBPVVDCHI2: Flight distance chi2
    @arg XicDIRA:    DIRA of Xic
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @return: Selection object
    """
    _daughterKCut  = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % locals()
    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)& (TRGHOSTPROB < 0.5)" % locals()
    _daughterpCut  = "(PROBNNp  > %(ProtProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (P>10000) & (TRGHOSTPROB < 0.5)" % locals()

    _combinationCut = "(ASUM(PT)> %(XicPT)s*MeV) & (ADAMASS('Xi_c+') < 1.25*%(XicDeltaMassWin)s*MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % locals()
    _motherCut = "(ADMASS('Xi_c+') < %(XicDeltaMassWin)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxXicVertChi2DOF)s) & (BPVVDCHI2>%(XicBPVVDCHI2)s) & (BPVDIRA>%(XicDIRA)s)" % locals()
    _Xic = CombineParticles(DecayDescriptor = "[Xi_c+ -> p+ K- pi+]cc",
                            DaughtersCuts = {"K+"  : _daughterKCut,
                                             "pi+" : _daughterpiCut,
                                             "p+"  : _daughterpCut},
                            CombinationCut = _combinationCut,
                            MotherCut=_motherCut)

    _stdANNKaon    = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
    _stdANNPion    = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
    _stdANNProtons = DataOnDemand(Location="Phys/StdLooseANNProtons/Particles")

    return Selection(name, Algorithm=_Xic, RequiredSelections=[_stdANNKaon, _stdANNPion, _stdANNProtons])


def makeOmegac(name, KaonProbNN, PionProbNN, ProtProbNN, XicPT, XicDeltaMassWin, MaxXicVertChi2DOF, XicBPVVDCHI2, XicDIRA, TrackMinIPCHI2):
    """
    Create and return a Omegac0 -> p K- K- pi+ Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg KaonProbNN: PID of Kaon
    @arg PionProbNN: PID of pion
    @arg ProtProbNN: PID of proton
    @arg XicPT:      Min PT of Xic
    @arg XicDeltaMassWin: Mass difference m(pKpi)-m(Xic)
    @arg MaxXicVertChi2DOF: chi2/NDOF
    @arg XicBPVVDCHI2: Flight distance chi2
    @arg XicDIRA:    DIRA of Xic
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @return: Selection object
    """
    _daughterKCut  = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % locals()
    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % locals()
    _daughterpCut  = "(PROBNNp  > %(ProtProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (P>10000) & (TRGHOSTPROB < 0.5)" % locals()

    _combinationCut = "(ASUM(PT)> %(XicPT)s*MeV) & (ADAMASS('Omega_c0') < 1.25*%(XicDeltaMassWin)s*MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % locals()
    _motherCut = "(ADMASS('Omega_c0') < %(XicDeltaMassWin)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxXicVertChi2DOF)s) & (BPVVDCHI2>%(XicBPVVDCHI2)s) & (BPVDIRA>%(XicDIRA)s)" % locals()
    _Omegac = CombineParticles(DecayDescriptor = "[Omega_c0 -> p+ K- K- pi+]cc",
                            DaughtersCuts = {"K+"  : _daughterKCut,
                                             "pi+" : _daughterpiCut,
                                             "p+"  : _daughterpCut},
                            CombinationCut = _combinationCut,
                            MotherCut=_motherCut)

    _stdANNKaon    = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
    _stdANNPion    = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
    _stdANNProtons = DataOnDemand(Location="Phys/StdLooseANNProtons/Particles")

    return Selection(name, Algorithm=_Omegac, RequiredSelections=[_stdANNKaon, _stdANNPion, _stdANNProtons])


def makeXic0(name, KaonProbNN, PionProbNN, ProtProbNN, XicPT, XicDeltaMassWin, MaxXicVertChi2DOF, XicBPVVDCHI2, XicDIRA, TrackMinIPCHI2):
    """
    Create and return a Xic0 -> p K- K- pi+ Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg KaonProbNN: PID of Kaon
    @arg PionProbNN: PID of pion
    @arg ProtProbNN: PID of proton
    @arg XicPT:      Min PT of Xic
    @arg XicDeltaMassWin: Mass difference m(pKpi)-m(Xic)
    @arg MaxXicVertChi2DOF: chi2/NDOF
    @arg XicBPVVDCHI2: Flight distance chi2
    @arg XicDIRA:    DIRA of Xic
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @return: Selection object
    """
    _daughterKCut  = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % locals()
    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5) " % locals()
    _daughterpCut  = "(PROBNNp  > %(ProtProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (P>10000) & (TRGHOSTPROB < 0.5)" % locals()

    _combinationCut = "(ASUM(PT)> %(XicPT)s*MeV) & (ADAMASS('Xi_c0') < 1.25*%(XicDeltaMassWin)s*MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % locals()
    _motherCut = "(ADMASS('Xi_c0') < %(XicDeltaMassWin)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxXicVertChi2DOF)s) & (BPVVDCHI2>%(XicBPVVDCHI2)s) & (BPVDIRA>%(XicDIRA)s)" % locals()
    _Omegac = CombineParticles(DecayDescriptor = "[Xi_c0 -> p+ K- K- pi+]cc",
                               DaughtersCuts = {"K+"  : _daughterKCut,
                                                "pi+" : _daughterpiCut,
                                                "p+"  : _daughterpCut},
                               CombinationCut = _combinationCut,
                               MotherCut=_motherCut)

    _stdANNKaon    = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
    _stdANNPion    = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
    _stdANNProtons = DataOnDemand(Location="Phys/StdLooseANNProtons/Particles")

    return Selection(name, Algorithm=_Omegac, RequiredSelections=[_stdANNKaon, _stdANNPion, _stdANNProtons])

def makeOmegab2XicKpi(name, XicSel, DecDescr, KaonProbNN, PionProbNN, MaxOmegabMass, MinOmegabMass, OmegabVertChi2DOF, OmegabIPCHI2, OmegabLT, Omegab_PT, OmegabDIRA, TrackMinIPCHI2):
    """
    Create and return a Omegab- -> Xic+ K- pi- or Omegab- -> Xic0 pi+ K- pi- Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg XicSel: Xic selection.
    @arg DecDescr: Decay descriptor
    @arg KaonProbNN: PID of Kaon
    @arg PionProbNN: PID of pion
    @arg MaxOmegabMass: Max value of Omegab mass window
    @arg MinOmegabMass: Max value of Omegab mass window
    @arg OmegabVertChi2DOF: chi2/NDOF
    @arg OmegabIPCHI2:  Omegab IP chi2
    @arg OmegabLT:      Omegab lifetime
    @arg Omegab_PT:     Omegab PT
    @arg OmegabDIRA:    Omegab DIRA
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @return: Selection object
    """

    _stdANNKaonAll    = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
    _stdANNPionAll    = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")

    _daughterKCut  = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % locals()
    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % locals()

    _combinationCut = "(AM < %(MaxOmegabMass)s*MeV+100*MeV) & (AM > %(MinOmegabMass)s*MeV-100*MeV) & (APT>%(Omegab_PT)s*MeV-300*MeV)" % locals()
    #_combinationCut = "(AM < %(MaxOmegabMass)s*MeV) & (AM > %(MinOmegabMass)s*MeV)" % locals()
    _motherCut = "(M < %(MaxOmegabMass)s*MeV) & (M > %(MinOmegabMass)s*MeV) & (VFASPF(VCHI2/VDOF)<%(OmegabVertChi2DOF)s) & (BPVIPCHI2() < %(OmegabIPCHI2)s) & (BPVLTIME()>%(OmegabLT)s*ps) & (PT>%(Omegab_PT)s*MeV) & (BPVDIRA>%(OmegabDIRA)s)" % locals()

    _Omegab = CombineParticles(DecayDescriptor = DecDescr,
                               DaughtersCuts = {"K+"  : _daughterKCut,
                                                "pi+" : _daughterpiCut
                                                },
                               CombinationCut = _combinationCut,
                               MotherCut=_motherCut)

    return Selection(name, Algorithm=_Omegab, RequiredSelections=[XicSel, _stdANNKaonAll, _stdANNPionAll])

def makeOmegab2XicgammaKpi(name, XicSel, DecDescr, KaonProbNN, PionProbNN, MaxOmegabMass, MinOmegabMass, OmegabVertChi2DOF, OmegabIPCHI2, OmegabLT, Omegab_PT, OmegabDIRA, TrackMinIPCHI2, XicprDeltaMassWin):
    """
    Create and return a Omegab- -> Xic+ gamma K- pi- Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg XicSel: Xic selection.
    @arg DecDescr: Decay descriptor
    @arg KaonProbNN: PID of Kaon
    @arg PionProbNN: PID of pion
    @arg MaxOmegabMass: Max value of Omegab mass window
    @arg MinOmegabMass: Max value of Omegab mass window
    @arg OmegabVertChi2DOF: chi2/NDOF
    @arg OmegabIPCHI2:  Omegab IP chi2
    @arg OmegabLT:      Omegab lifetime
    @arg Omegab_PT:     Omegab PT
    @arg OmegabDIRA:    Omegab DIRA
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @arg XicprDeltaMassWin : Delta Mass m(Xic') - m(Xic)
    @return: Selection object
    """

    _stdANNKaonAll    = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
    _stdANNPionAll    = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
    _stdPhotonAll     = DataOnDemand(Location="Phys/StdLooseAllPhotons/Particles")

    _daughterKCut  = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % locals()
    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % locals()

    _combinationCut = "(AM < %(MaxOmegabMass)s*MeV+100*MeV) & (AM > %(MinOmegabMass)s*MeV-100*MeV) & (AM12 - AM1 < %(XicprDeltaMassWin)s) & (APT>%(Omegab_PT)s*MeV-300*MeV)" % locals()
    #_combinationCut = "(AM < %(MaxOmegabMass)s*MeV) & (AM > %(MinOmegabMass)s*MeV)" % locals()
    _motherCut = "(M < %(MaxOmegabMass)s*MeV) & (M > %(MinOmegabMass)s*MeV) & (VFASPF(VCHI2/VDOF)<%(OmegabVertChi2DOF)s) & (BPVIPCHI2() < %(OmegabIPCHI2)s) & (BPVLTIME()>%(OmegabLT)s*ps) & (PT>%(Omegab_PT)s*MeV) & (BPVDIRA>%(OmegabDIRA)s)" % locals()

    _Omegab = CombineParticles(DecayDescriptor = DecDescr,
                               DaughtersCuts = {"K+"  : _daughterKCut,
                                                "pi+" : _daughterpiCut
                                                },
                               CombinationCut = _combinationCut,
                               MotherCut=_motherCut)

    return Selection(name, Algorithm=_Omegab, RequiredSelections=[XicSel, _stdANNKaonAll, _stdANNPionAll, _stdPhotonAll])

def makeOmegab2Omegacpi(name, OmegacSel, DecDescr, PionProbNN, MaxOmegabMass, MinOmegabMass, OmegabVertChi2DOF, OmegabIPCHI2, OmegabLT, Omegab_PT, OmegabDIRA, TrackMinIPCHI2):
    """
    Create and return a Omegab- -> Omegac0 pi- or Omegab- -> Omegac0 pi- pi+ pi- Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg OmegacSel: Xic selection.
    @arg DecDescr: Decay descriptor
    @arg PionProbNN: PID of pion
    @arg MaxOmegabMass: Max value of Omegab mass window
    @arg MinOmegabMass: Max value of Omegab mass window
    @arg OmegabVertChi2DOF: chi2/NDOF
    @arg OmegabIPCHI2:  Omegab IP chi2
    @arg OmegabLT:      Omegab lifetime
    @arg Omegab_PT:     Omegab PT
    @arg OmegabDIRA:    Omegab DIRA
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @return: Selection object
    """

    _stdANNPionAll    = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")

    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % locals()

    _combinationCut = "(AM < %(MaxOmegabMass)s*MeV+100*MeV) & (AM > %(MinOmegabMass)s*MeV-100*MeV) & (APT>%(Omegab_PT)s*MeV-300*MeV)" % locals()

    _motherCut = "(M < %(MaxOmegabMass)s*MeV) & (M > %(MinOmegabMass)s*MeV) & (VFASPF(VCHI2/VDOF)<%(OmegabVertChi2DOF)s) & (BPVIPCHI2() < %(OmegabIPCHI2)s) & (BPVLTIME()>%(OmegabLT)s*ps) & (PT>%(Omegab_PT)s*MeV) & (BPVDIRA>%(OmegabDIRA)s)" % locals()

    _Omegab = CombineParticles(DecayDescriptor = DecDescr,
                               DaughtersCuts = {"pi+" : _daughterpiCut
                                                },
                               CombinationCut = _combinationCut,
                               MotherCut=_motherCut)

    return Selection(name, Algorithm=_Omegab, RequiredSelections=[OmegacSel, _stdANNPionAll])

def makeOmegab2Omegacpi0pi(name, OmegacSel, DecDescr, PionProbNN, MaxOmegabMass, MinOmegabMass, OmegabVertChi2DOF, OmegabIPCHI2, OmegabLT, Omegab_PT, OmegabDIRA, TrackMinIPCHI2):
    """
    Create and return a Omegab- -> Omegac0 pi0 pi- Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg OmegacSel: Xic selection.
    @arg DecDescr: Decay descriptor
    @arg PionProbNN: PID of pion
    @arg MaxOmegabMass: Max value of Omegab mass window
    @arg MinOmegabMass: Max value of Omegab mass window
    @arg OmegabVertChi2DOF: chi2/NDOF
    @arg OmegabIPCHI2:  Omegab IP chi2
    @arg OmegabLT:      Omegab lifetime
    @arg Omegab_PT:     Omegab PT
    @arg OmegabDIRA:    Omegab DIRA
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @return: Selection object
    """

    _stdANNPionAll    = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
    _stdPi0Resolved   = DataOnDemand(Location="Phys/StdLooseResolvedPi0/Particles")

    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % locals()

    _combinationCut = "(AM < %(MaxOmegabMass)s*MeV+100*MeV) & (AM > %(MinOmegabMass)s*MeV-100*MeV) & (APT>%(Omegab_PT)s*MeV-300*MeV)" % locals()

    _motherCut = "(M < %(MaxOmegabMass)s*MeV) & (M > %(MinOmegabMass)s*MeV) & (VFASPF(VCHI2/VDOF)<%(OmegabVertChi2DOF)s) & (BPVIPCHI2() < %(OmegabIPCHI2)s) & (BPVLTIME()>%(OmegabLT)s*ps) & (PT>%(Omegab_PT)s*MeV) & (BPVDIRA>%(OmegabDIRA)s)" % locals()

    _Omegab = CombineParticles(DecayDescriptor = DecDescr,
                               DaughtersCuts = {"pi+" : _daughterpiCut
                                                },
                               CombinationCut = _combinationCut,
                               MotherCut=_motherCut)

    return Selection(name, Algorithm=_Omegab, RequiredSelections=[OmegacSel, _stdANNPionAll, _stdPi0Resolved])

def makeOmegab2Omegacgammapi(name, OmegacSel, DecDescr, PionProbNN, MaxOmegabMass, MinOmegabMass, OmegabVertChi2DOF, OmegabIPCHI2, OmegabLT, Omegab_PT, OmegabDIRA, TrackMinIPCHI2, OmegacstDeltaMassWin):
    """
    Create and return a Omegab- -> Omegac0 gamma pi- Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg OmegacSel: Xic selection.
    @arg DecDescr: Decay descriptor
    @arg PionProbNN: PID of pion
    @arg MaxOmegabMass: Max value of Omegab mass window
    @arg MinOmegabMass: Max value of Omegab mass window
    @arg OmegabVertChi2DOF: chi2/NDOF
    @arg OmegabIPCHI2:  Omegab IP chi2
    @arg OmegabLT:      Omegab lifetime
    @arg Omegab_PT:     Omegab PT
    @arg OmegabDIRA:    Omegab DIRA
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @arg OmegacstDeltaMassWin: Delta Mass m(Omegac*) - m(Omegac)
    @return: Selection object
    """

    _stdANNPionAll    = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
    _stdPhotonAll     = DataOnDemand(Location="Phys/StdLooseAllPhotons/Particles")

    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % locals()

    _combinationCut = "(AM < %(MaxOmegabMass)s*MeV+100*MeV) & (AM > %(MinOmegabMass)s*MeV-100*MeV) & (AM12 - AM1 < %(OmegacstDeltaMassWin)s) & (APT>%(Omegab_PT)s*MeV-300*MeV)" % locals()
    #_combinationCut = "(AM < %(MaxOmegabMass)s*MeV) & (AM > %(MinOmegabMass)s*MeV)" % locals()
    _motherCut = "(M < %(MaxOmegabMass)s*MeV) & (M > %(MinOmegabMass)s*MeV) & (VFASPF(VCHI2/VDOF)<%(OmegabVertChi2DOF)s) & (BPVIPCHI2() < %(OmegabIPCHI2)s) & (BPVLTIME()>%(OmegabLT)s*ps) & (PT>%(Omegab_PT)s*MeV) & (BPVDIRA>%(OmegabDIRA)s)" % locals()

    _Omegab = CombineParticles(DecayDescriptor = DecDescr,
                               DaughtersCuts = {"pi+" : _daughterpiCut
        },
                               CombinationCut = _combinationCut,
                               MotherCut=_motherCut)

    return Selection(name, Algorithm=_Omegab, RequiredSelections=[OmegacSel, _stdANNPionAll, _stdPhotonAll])

# EOF
