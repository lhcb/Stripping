###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = ['Phillip Urquijo, Alessandra Borgia', 'Michel De Cian']
__date__ = '11/01/2019'
__version__ = '$Revision: 1.6 $'

'''
B->Xu mu nu exclusive reconstruction in Xu=pi/rho/omega/phi/K/K*+ channels
'''
# =============================================================================
##
#  B->Xu mu nu exclusive reconstruction in Xu=pi/rho/omega/phi/K/K*+ channels
#
#  Stripping lines for charmless semileptonic B+/B0/Bs decays
#  to final states with hadron+muon, where the hadron is
#  either a charged track or a resonance
#  The charmless hadronic signal modes are:
#
#  B0 -> pi+ mu nu
#  B+ -> rho mu nu
#  B+ -> omega mu nu
#  Bs0 -> K+  mu- nu,
#  B -> phi mu nu
#  Bs0 -> K*+ mu- nu, (with K*+->KS0 Pi)
#
############
#  Also added are 2 new lines for Majorana Neutrino decays.
#  B- -> KS0 mu- nu, with KS0 ->pi+ mu- (SS = Same Sign)
#  B- -> KS0 mu- nu, with KS0 ->pi- mu+ (OS = Opposite Sign)
############
#
#  Stripping XX, with requirements that the
#  rate <0.05% and timing <1ms/evt.
##

"""
##
#  B->Xu mu nu exclusive reconstruction in Xu=pi/rho/omega/phi/K/K*+ channels
#
#  Stripping lines for charmless semileptonic B+/B0/Bs decays
#  to final states with hadron+muon, where the hadron is
#  either a charged track or a resonance
#  The charmless hadronic signal modes are:
#
#  B0 -> pi+ mu nu
#  B+ -> rho mu nu
#  B+ -> omega mu nu
#  Bs0 -> K+  mu- nu,
#  B -> phi mu nu
#  Bs0 -> K*+ mu- nu, (with K*+->KS0 Pi)
#
############
#  Also added are 2 new lines for Majorana Neutrino decays.
#  B- -> KS0 mu- nu, with KS0 ->pi+ mu- (SS = Same Sign)
#  B- -> KS0 mu- nu, with KS0 ->pi- mu+ (OS = Opposite Sign)
############
#
#  Stripping XX, with requirements that the
#  rate <0.05% and timing <1ms/evt.

Last modification $Date: 2018-January-10 $
               by $Author: decianm $
"""


default_config = {
  'NAME'        : 'B2XuMuNu',
  'WGs'         : ['Semileptonic'],
  'BUILDERTYPE' : 'B2XuMuNuBuilder',
  'CONFIG'      :  {
      "GEC_nLongTrk"        : 250.  , #adimensional
      "TRGHOSTPROB"         : 0.5    ,#adimensional
      #Muons
      "MuonGHOSTPROB"       : 0.35  ,#adimensional
      "MuonTRCHI2"          : 4.    ,#adimensional
      "MuonP"               : 3000. ,#MeV
      "MuonPTight"          : 6000. ,#MeV
      "MuonPT"              : 1000. ,#MeV
      "MuonPTTight"         : 1500. ,#MeV
      "MuonPIDK"            : 0.    ,#adimensional
      "MuonPIDmu"           : 3.    ,#adimensional
      "MuonPIDp"            : 0.    ,#adimensional
      "MuonMINIPCHI2"       : 25    ,#adminensional
      #Xu
      #K channel
      "KaonTRCHI2"          : 4.     ,#adimensional
      "KaonP"               : 3000.  ,#MeV
      "KaonPTight"          : 10000. ,#MeV
      "KaonPT"              : 400.   ,#MeV
      "KaonPTTight"         : 1000.   ,#MeV
      "KaonPIDK"            : 5.     ,#adimensional
      "KaonPIDmu"           : 5.     ,#adimensional
      "KaonPIDp"            : 5.     ,#adimensional
      "KaonPIDK_phi"        : 2.     ,#adimensional
      "KaonPIDmu_phi"       : -2.    ,#adimensional
      "KaonPIDp_phi"        : -2.    ,#adimensional
      "KaonMINIPCHI2"       : 36     ,#adminensional
      #Pion channel
      "PionTRCHI2"          : 4.     ,#adimensional
      "PionP"               : 3000.  ,#MeV
      "PionPTight"          : 10000. ,#MeV
      "PionPT"              : 300.   ,#MeV
      "PionPTTight"         : 800.   ,#MeV
      "PionPIDK"            : -2.    ,#adimensional
      "PionMINIPCHI2"       : 36      ,#adminensional

      #phi channel
      "PhiUpperMass"        : 2200. ,#MeV
      "PhiVCHI2DOF"         : 6     ,#adimensional
      "PhiPT"               : 600.  ,#MeV
      "PhiMINIPCHI2"        : 9     ,#adimensional
      "PhiDIRA"             : 0.9   ,#adimensional

      #Rho channel
      "RhoMassWindow"       : 1500. ,#MeV
      "RhoMassWindowMin1SB" : 300.  ,#MeV
      "RhoMassWindowMax1SB" : 620.  ,#MeV
      "RhoMassWindowMin2SB" : 920.  ,#MeV
      "RhoMassWindowMax2SB" : 1200. ,#MeV
      "RhoVCHI2DOF"         : 4     ,#adimensional
      "RhoPT"               : 1000.  ,#MeV
      "RhoLeadingPionPT"    : 900.  ,#MeV
      "RhoLeadingPionP"     : 5000.  ,#MeV
      "RhoMINIPCHI2"        : 50    ,#adimensional
      "RhoChPionPT"         : 400.  ,#MeV
      "RhoChPionTRCHI2"     : 4.   ,#MeV
      "RhoChPionPIDK"       : -2.  ,#adminensional
                   #    "RhoFDCHI2"           : 100.  ,#adimensional
                   #    "RhoFD"               : 6     ,#mm
                   #    "RhoIPMin"            : 0.3   ,#mm
      "RhoDIRA"             : 0.98   ,#adimensional
      "RhoChPionMINIPCHI2"  : 36.    ,#adimensional
      # Omega cuts
      "OmegaChPionPT"       : 300, #MeV
      "OmegaPi0PT"          : 500, #MeV
      "OmegaChPionMINIPCHI2": 25, #adimensional
      "OmegaChPionPIDK"     : 0, #adimensional
      "OmegaMassWindow"     : 150, #MeV
      "OmegaVCHI2DOF"       : 6, #MeV
      #Kshort Daughter Cuts
      "KS0DaugP"            : 2000. ,#MeV
      "KS0DaugPT"           : 100.  ,#MeV
      "KS0DaugTrackChi2"    : 4.    ,#adimensional
      "KS0DaugMIPChi2"      : 50.   ,#adimensional
      #Kshort cuts
      "KSMajoranaCutFDChi2" : 100.  ,#adimensional
      "KS0VertexChi2"       : 4.    ,#adimensional
      "KS0PT"               : 250.  ,#adimensional
      "KsLLMaxDz"           : 650.  ,#mm
      "KsLLCutFD"           : 20.   ,#mm
      "KSLLMassLow"         : 456.  ,#MeV
      "KSLLMassHigh"        : 536.  ,#MeV
      "KSLLCutFDChi2"       : 0.    ,#adimensional
      "KS0MIPChi2"          : 0.    ,#adimensional
      'KS0Z'                : 0.    ,#mm
      #Kstar Cuts
      "KstarMassWindow"     : 1200  ,#MeV
      "KstarPT"             : 800   ,#MeV
      #Kstar Daughter cuts
      "KstarChPionPT"       : 250.  ,#MeV
      "KstarChPionP"        : 3000. ,#MeV
      "KstarChPionTRCHI2"   : 4.    ,#adimensional
      "KstarChPionMINIPCHI2": 25.   ,#adimensional
      "KstarChPionPIDK"     : 2.    ,#adminensional
      #B Mother Cuts
      "BVCHI2DOF"           : 6.    ,#adminensional, for 3- or more-track vertices
      "BVCHI2DOFTight"      : 4.    ,#adminensional, ideally for 2-track vertices
      "BDIRA"               : 0.99  ,#adminensional
      "BDIRAMed"            : 0.994 ,#adminensional
      "BDIRATight"          : 0.999 ,#adminensional
      "BFDCHI2HIGH"         : 100.  ,#adimensional
      "BFDCHI2Tight"        : 120.  ,#adimensional
      "BFDCHI2ForPhi"       : 50.   ,#adimensional
      #B Mass Minima
      "KMuMassLowTight"     : 1500. ,#MeV
      "PhiMu_MCORR"         : 2500. ,#MeV
      "RhoMuMassLowTight"   : 2000. ,#MeV
      "KS0MuMassLowTight"   : 3000. ,#MeV
      "KstarMuMassLowTight" : 2500. ,#MeV
      #B corrected mass limits
      "BCorrMLow"           : 2500. ,#MeV
      "BCorrMHigh"          : 7000. ,#MeV
      #
      "XMuMassUpper"        : 5500. ,#MeV
      "XMuMassUpperHigh"    : 6500. ,#MeV
      "Enu"                 : 1850. ,#MeV
      "EnuK"                : 2000. ,#MeV
      # Prescales
      'NoPrescales'         : False,
      'PiMuNu_prescale'     : 0.2
      },
  'STREAMS'     : ['Semileptonic']
}

from Gaudi.Configuration import *
from StrippingUtils.Utils import LineBuilder

import logging
def makeTOSFilter(name,specs):
    from Configurables import TisTosParticleTagger
    tosFilter = TisTosParticleTagger(name+'TOSFilter')
    tosFilter.TisTosSpecs = specs
    tosFilter.ProjectTracksToCalo = False
    tosFilter.CaloClustForCharged = False
    tosFilter.CaloClustForNeutral = False
    tosFilter.TOSFrac = {4:0.0, 5:0.0}
    #tosFilter.PassOnAll = True
    return tosFilter

def tosSelection(sel,specs):
    from PhysSelPython.Wrappers import Selection
    '''Filters Selection sel to be TOS.'''
    tosFilter = makeTOSFilter(sel.name(),specs)
    return Selection(sel.name()+'TOS', Algorithm=tosFilter,
                     RequiredSelections=[sel])

default_name="B2XuMuNu"

class B2XuMuNuBuilder(LineBuilder):
    """
    Definition of B->Xu mu nu stripping module
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self,name,config):
        LineBuilder.__init__(self, name, config)
        self.config=config
        from PhysSelPython.Wrappers import Selection, DataOnDemand
        self._stdVeryLooseKsLL = DataOnDemand("Phys/StdVeryLooseKsLL/Particles")
        self._stdLooseKsLD = DataOnDemand("Phys/StdLooseKsLD/Particles")
        self._stdLooseKsDD = DataOnDemand("Phys/StdLooseKsDD/Particles")

        self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}

        ## Muons
        self._muonSel=None
        self._muonFilter()

        self._muonSelNoPID=None
        self._muonFilterNoPID()

        self._muonSelTOS=None
        self._muonFilterTOS()

        self._muonSelTOSNoPID=None
        self._muonFilterTOSNoPID()

        self._muonSelTight=None
        self._muonFilterTight()

        self._muonSelTightNoPID=None
        self._muonFilterTightNoPID()

        self._MajoranaLineMuSel=None
        self._MajoranaLineMuFilter()

        ## Pions
        self._pionSel=None
        self._pionFilter()

        self._pionSelNoPID=None
        self._pionFilterNoPID()

        self._pionSelTight=None
        self._pionFilterTight()

        self._pionSelTightNoPID=None
        self._pionFilterTightNoPID()

        ## Kaons
        self._kaonSel=None
        self._kaonFilter()

        self._kaonForPhiSel=None
        self._kaonForPhiFilter()

        self._kaonSelNoPID=None
        self._kaonFilterNoPID()

        self._kaonForPhiSelNoPID=None
        self._kaonForPhiFilterNoPID()

        ## Rhos
        self._rho770Sel=None
        self._Rho02PiPiFilter()

        self._rho770WSSel=None
        self._Rho02PiPiWSFilter()

        self._rho770SBSel=None
        self._Rho02PiPiSBFilter()

        self._rho770SelNoPID=None
        self._Rho02PiPiFilterNoPID()

        ## Omega
        self._omega782Sel=None
        self._Omega2PiPiPi0Filter()

        self._omega782WSSel=None
        self._Omega2PiPiPi0WSFilter()

        self._omega782SelNoPID=None
        self._Omega2PiPiPi0FilterNoPID()

        ## Phi
        self._phi1020Sel=None
        self._Phi2KKFilter()

        self._phi1020SelWS=None
        self._Phi2KKFilterWS()

        self._phi1020SelNoPID=None
        self._Phi2KKFilterNoPID()

        ## KS for Majorana
        self._KshMajoranaSSMuSel=None
        self._KshMajoranaSSMuFilter()

        self._KshMajoranaOSMuSel=None
        self._KshMajoranaOSMuFilter()

        ## KS for other stuff
        self._KSSel=None
        self._KsFilter()

        self._KstarSel=None
        self._Kstar2KPiFilter()

        self._KstarSelNoPID=None
        self._Kstar2KPiFilterNoPID()

        ###########################################
        ##
        ## Register the stripping lines
        ##
        ###########################################
        # pi lines
        self.registerLine(self._Pi_line())
        self.registerLine(self._Pi_NoMuTopo_line())
        self.registerLine(self._Pi_NoPIDmu_line())
        self.registerLine(self._Pi_NoPIDhad_line())
        self.registerLine(self._PiSS_line())
        # K lines
        self.registerLine(self._K_line())
        self.registerLine(self._K_NoMuTopo_line())
        self.registerLine(self._KSS_line())
        self.registerLine(self._K_NoPIDmu_line())
        self.registerLine(self._KSS_NoPIDmu_line())
        self.registerLine(self._K_NoPIDK_line())
        self.registerLine(self._KSS_NoPIDK_line())
        self.registerLine(self._K_NoPIDKmu_line())
        self.registerLine(self._KSS_NoPIDKmu_line())
        # phi lines
        self.registerLine(self._Phi_line())
        self.registerLine(self._Phi_NoMuTopo_line())
        self.registerLine(self._PhiWS_line())
        self.registerLine(self._Phi_NoPIDmu_line())
        self.registerLine(self._Phi_NoPIDhad_line())
        self.registerLine(self._PhiBadVtx_line())
        # rho lines
        self.registerLine(self._Rho_line())
        self.registerLine(self._Rho_NoMuTopo_line())
        self.registerLine(self._Rho_NoPIDmu_line())
        self.registerLine(self._Rho_NoPIDhad_line())
        self.registerLine(self._RhoWS_line())
        self.registerLine(self._RhoBadVtx_line())
        #self.registerLine(self._RhoSB_line()) With a large rho mass window, this is not needed anymore
        # omega lines
        self.registerLine(self._Omega_line())
        self.registerLine(self._Omega_NoMuTopo_line())
        self.registerLine(self._Omega_NoPIDmu_line())
        self.registerLine(self._Omega_NoPIDhad_line())
        self.registerLine(self._OmegaWS_line())
        self.registerLine(self._OmegaBadVtx_line())
        # majorana lines
        self.registerLine(self._KshMajoranaSSMu_line())
        self.registerLine(self._KshMajoranaOSMu_line())
        # Kstar lines
        self.registerLine(self._Kstar_line())
        self.registerLine(self._Kstar_NoMuTopo_line())
        self.registerLine(self._Kstar_NoPIDmu_line())
        self.registerLine(self._Kstar_NoPIDhad_line())
        self.registerLine(self._KstarSS_line())
        self.registerLine(self._KstarBadVtx_line())

    ### Muons
    def _NominalMuSelection( self ):
        return "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonP)s *MeV) &  (PT> %(MuonPT)s* MeV)"\
               "& (TRGHOSTPROB < %(MuonGHOSTPROB)s)"\
               "& (PIDmu > %(MuonPIDmu)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(MuonMINIPCHI2)s )"

    def _NominalMuSelectionNoPID( self ):
        return "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonP)s *MeV) &  (PT> %(MuonPT)s* MeV)"\
               "& (TRGHOSTPROB < %(MuonGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(MuonMINIPCHI2)s )"

    def _NominalMuSelectionTight( self ):
        return "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonPTight)s *MeV) &  (PT> %(MuonPTTight)s* MeV)"\
               "& (TRGHOSTPROB < %(MuonGHOSTPROB)s)"\
               "& (PIDmu > %(MuonPIDmu)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(MuonMINIPCHI2)s )"

    def _NominalMuSelectionTightNoPID( self ):
        return "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonPTight)s *MeV) &  (PT> %(MuonPTTight)s* MeV)"\
               "& (TRGHOSTPROB < %(MuonGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(MuonMINIPCHI2)s )"


    def _MajoranaLineMuSelection( self ):
        return "(P > %(KS0DaugP)s) & (PT > %(KS0DaugPT)s)"\
               "& (TRGHOSTPROB < %(MuonGHOSTPROB)s)"\
               "&(TRCHI2DOF < %(KS0DaugTrackChi2)s ) & (PIDmu-PIDpi> %(MuonPIDmu)s )& (PIDmu-PIDp> %(MuonPIDp)s )"\
               "&(PIDmu-PIDK> %(MuonPIDK)s )&(MIPCHI2DV(PRIMARY) > %(KS0DaugMIPChi2)s)"\

    ### Kaons
    def _NominalKSelection( self ):
        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonPTight)s *MeV) &  (PT> %(KaonPTTight)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK > %(KaonPIDK)s ) "\
               "& (MIPCHI2DV(PRIMARY)> %(KaonMINIPCHI2)s )"

    def _NominalKSelectionNoPID( self ):
        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonPTight)s *MeV) &  (PT> %(KaonPTTight)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(KaonMINIPCHI2)s )"

    def _KforPhiSelection( self ):
        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonP)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK> %(KaonPIDK_phi)s ) "\
               "& (MIPCHI2DV(PRIMARY)> %(KaonMINIPCHI2)s )"

    def _KforPhiSelectionNoPID( self ):
        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonP)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(KaonMINIPCHI2)s )"

    ### Pions
    def _NominalPiSelection( self ):
        return "(TRCHI2DOF < %(PionTRCHI2)s )&  (P> %(PionP)s *MeV) &  (PT> %(PionPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK < %(PionPIDK)s )"\
               "& (PIDmu < 2 ) "\
               "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"

    def _NominalPiSelectionNoPID( self ):
        return "(TRCHI2DOF < %(PionTRCHI2)s )&  (P> %(PionP)s *MeV) &  (PT> %(PionPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK < %(PionPIDK)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"

    def _NominalPiSelectionTight( self ):
        return "(TRCHI2DOF < %(PionTRCHI2)s )&  (P> %(PionPTight)s *MeV) &  (PT> %(PionPTTight)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK < %(PionPIDK)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"

    def _NominalPiSelectionTightNoPID( self ):
        return "(TRCHI2DOF < %(PionTRCHI2)s )&  (P> %(PionPTight)s *MeV) &  (PT> %(PionPTTight)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"

    ### K shorts
    def _NominalKsSelection( self ):
        return " (BPVVD >%(KsLLCutFD)s*mm)& (MM>%(KSLLMassLow)s*MeV)&(MM<%(KSLLMassHigh)s*MeV)  & (BPVVDCHI2> %(KSLLCutFDChi2)s ) & (PT > %(KS0PT)s*MeV) & (VFASPF(VCHI2PDOF) < %(KS0VertexChi2)s) & CHILDCUT((TRCHI2DOF < %(KS0DaugTrackChi2)s),1) & CHILDCUT((TRCHI2DOF < %(KS0DaugTrackChi2)s),2) & CHILDCUT((MIPCHI2DV(PRIMARY) > %(KS0DaugMIPChi2)s),1) & CHILDCUT((MIPCHI2DV(PRIMARY) > %(KS0DaugMIPChi2)s),2) & (MIPCHI2DV(PRIMARY) > %(KS0MIPChi2)s)"

    #####################################

    ## Nomenclature
    ## SS = Same-sign, the same charge for the muon and the hadron (pi+ mu+, K+, mu+, etc.)
    ## WS = wrong-sign, the same chargef for the decay products of the resonance ( rho -> pi+ pi+, omega -> pi+ pi+ pi0, phi -> K+ K+)

    ##### B -> pi mu nu lines
    def _Pi_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Pi_Line', prescale = 1.0 if NoPS else self.config["PiMuNu_prescale"],
                             FILTER=self.GECs,
                             algos = [ self._B2PiMuNu()])

    def _Pi_NoMuTopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Pi_NoMuTopoLine', prescale = 1.0 if NoPS else 0.05,
                             FILTER=self.GECs,
                             algos = [ self._B2PiMuNuNoMuTopo()])

    def _PiSS_line( self ):
        NoPS =  self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Pi_SSLine', prescale =  1.0 if NoPS else self.config["PiMuNu_prescale"],
                             FILTER=self.GECs,
                             algos = [ self._B2PiMuNuSS()])

    def _Pi_NoPIDmu_line( self ):
        NoPS =  self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Pi_NoPIDMuLine', prescale = 1.0 if NoPS else 0.005,
                             FILTER=self.GECs,
                             algos = [ self._B2PiMuNuNoPIDmu()])

    def _Pi_NoPIDhad_line( self ):
        NoPS =  self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Pi_NoPIDPiLine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._B2PiMuNuNoPIDhad()])

    ###### Bs -> K mu nu lines
    def _K_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KMuNu()])

    def _K_NoMuTopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_NoMuTopoLine', prescale =  1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KMuNuNoMuTopo()])

    def _KSS_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_SSLine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KMuNuSS()])

    def _K_NoPIDmu_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_NoPIDMuLine', prescale = 1.0 if NoPS else .02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KMuNu_NoPIDmu()])

    def _KSS_NoPIDmu_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_SSNoPIDMuLine', prescale = 1.0 if NoPS else .02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KMuNuSS_NoPIDmu()])

    def _K_NoPIDK_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_NoPIDKLine', prescale = 1.0 if NoPS else .02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KMuNu_NoPIDK()])

    def _KSS_NoPIDK_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_SSNoPIDKLine', prescale = 1.0 if NoPS else .02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KMuNuSS_NoPIDK()])

    def _K_NoPIDKmu_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_NoPIDKMuLine', prescale = 1.0 if NoPS else .02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KMuNu_NoPIDKmu()])

    def _KSS_NoPIDKmu_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_SSNoPIDKMuLine', prescale = 1.0 if NoPS else .02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KMuNuSS_NoPIDKmu()])

    ##### Bs -> phi/KK mu nu lines
    def _Phi_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiMuNu()])

    def _Phi_NoMuTopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_NoMuTopoLine', prescale = 1.0 if NoPS else 0.2,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiMuNuNoMuTopo()])

    def _PhiWS_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_WSLine', prescale = 1.0 if NoPS else 0.2,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiMuNuWS()])

    def _Phi_NoPIDmu_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_NoPIDMuLine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiMuNu_NoPIDmu()])

    def _Phi_NoPIDhad_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_NoPIDKLine', prescale = 1.0 if NoPS else 0.01,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiMuNu_NoPIDhad()])

    def _PhiBadVtx_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_BadVtxLine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiMuNuBadVtx()])


    ##### B -> rho/pipi mu nu lines
    def _Rho_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoMuNu()])

    def _Rho_NoMuTopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_NoMuTopoLine', prescale =  1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoMuNuNoMuTopo()])

    def _Rho_NoPIDmu_line(self):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_NoPIDMuLine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoMuNu_NoPIDmu()])

    def _Rho_NoPIDhad_line(self):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_NoPIDPiLine', prescale = 1.0 if NoPS else 0.05,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoMuNu_NoPIDhad()])

    def _RhoWS_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_WSLine', prescale = 1.0 if NoPS else 0.2,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoMuNuWS()])

    def _RhoBadVtx_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_BadVtxLine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoMuNuBadVtx()])

    def _RhoSB_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_SBLine', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoMuNuSB()])

    ##### B -> omega mu nu lines
    def _Omega_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaMuNu()])

    def _Omega_NoMuTopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_NoMuTopoLine', prescale =  1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaMuNuNoMuTopo()])

    def _Omega_NoPIDmu_line(self):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_NoPIDMuLine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaMuNu_NoPIDmu()])

    def _Omega_NoPIDhad_line(self):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_NoPIDPiLine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaMuNu_NoPIDhad()])

    def _OmegaWS_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_WSLine', prescale = 1.0 if NoPS else 0.3,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaMuNuWS()])

    def _OmegaBadVtx_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_BadVtxLine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaMuNuBadVtx()])

    ##### Ks majorana lines
    def _KshMajoranaSSMu_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2KshSSMu_SSMuminus_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bu2KshSSMuNu_SSMuminus()])

    def _KshMajoranaOSMu_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2KshOSMu_SSMuplus_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bu2KshOSMuNu_SSMuplus()])

    ##### Bs -> K*/Kspi mu nu lines
    def _Kstar_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarMuNu()])

    def _Kstar_NoMuTopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_NoMuTopoLine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarMuNuNoMuTopo()])

    def _Kstar_NoPIDmu_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_NoPIDMuLine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarMuNu_NoPIDmu()])

    def _Kstar_NoPIDhad_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_NoPIDPiLine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarMuNu_NoPIDhad()])

    def _KstarSS_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_SSLine', prescale = 1.0 if NoPS else 0.3,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarMuNuSS()])

    def _KstarBadVtx_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_BadVtxLine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarMuNuBadVtx()])

    ###############################
    ##
    ## Filters for particles
    ##
    ##############################

    ### Muons
    def _muonFilter( self ):
        if self._muonSel is not None:
            return self._muonSel

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseMuons
        _mu = FilterDesktop( Code = self._NominalMuSelection() % self._config )
        _muSel=Selection("Mu_for"+self._name,
                         Algorithm=_mu,
                         RequiredSelections = [StdLooseMuons])

        self._muonSel=_muSel

        return _muSel

    ######--######
    def _muonFilterNoPID( self ):
        if self._muonSelNoPID is not None:
            return self._muonSelNoPID

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsMuons
        _muNoPID = FilterDesktop( Code = self._NominalMuSelectionNoPID() % self._config )
        _muSelNoPID=Selection("MuNoPID_for"+self._name,
                         Algorithm=_muNoPID,
                         RequiredSelections = [StdNoPIDsMuons])

        self._muonSelNoPID=_muSelNoPID

        return _muSelNoPID


    ######--######
    def _muonFilterTight( self ):
        if self._muonSelTight is not None:
            return self._muonSelTight

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseMuons
        _muTight = FilterDesktop( Code = self._NominalMuSelectionTight() % self._config )
        _muTightSel=Selection("MuTightCuts_for"+self._name,
                         Algorithm=_muTight,
                         RequiredSelections = [StdLooseMuons])

        self._muonSelTight=_muTightSel

        return _muTightSel

    ######--######
    def _muonFilterTightNoPID( self ):
        if self._muonSelTightNoPID is not None:
            return self._muonSelTightNoPID

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsMuons
        _muTightNoPID = FilterDesktop( Code = self._NominalMuSelectionTightNoPID() % self._config )
        _muTightSelNoPID=Selection("MuTightNoPIDCuts_for"+self._name,
                         Algorithm=_muTightNoPID,
                         RequiredSelections = [StdNoPIDsMuons])

        self._muonSelTightNoPID=_muTightSelNoPID

        return _muTightSelNoPID

    ######--######
    def _muonFilterTOS( self ):
        if self._muonSelTOS is not None:
            return self._muonSelTOS

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseMuons
        _muTOS = FilterDesktop( Code = self._NominalMuSelectionTight() % self._config )
        _muSelTOS=Selection("MuL0TOS_for"+self._name,
                            Algorithm=_muTOS,
                            RequiredSelections = [StdLooseMuons])
        _muSelTOS = tosSelection(_muSelTOS,{'L0.*Muon.*Decision%TOS':0})

        self._muonSelTOS=_muSelTOS

        return _muSelTOS

    ######--######
    def _muonFilterTOSNoPID( self ):
        if self._muonSelTOSNoPID is not None:
            return self._muonSelTOSNoPID

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsMuons
        _muTOSNoPID = FilterDesktop( Code = self._NominalMuSelectionTightNoPID() % self._config )
        _muSelTOSNoPID = Selection("MuL0TOSNoPID_for"+self._name,
                                   Algorithm=_muTOSNoPID,
                                   RequiredSelections = [StdNoPIDsMuons])
        _muSelTOSNoPID = tosSelection(_muSelTOSNoPID,{'L0.*Muon.*Decision%TOS':0})

        self._muonSelTOSNoPID=_muSelTOSNoPID

        return _muSelTOSNoPID

    ## Pions
    ######--######
    def _pionFilter( self ):
        if self._pionSel is not None:
            return self._pionSel

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions

        _pi = FilterDesktop( Code = self._NominalPiSelection() % self._config )
        _piSel=Selection("Pi_for"+self._name,
                         Algorithm=_pi,
                         RequiredSelections = [StdLoosePions])
        self._pionSel=_piSel
        return _piSel

    ######--######
    def _pionFilterNoPID( self ):
        if self._pionSelNoPID is not None:
            return self._pionSelNoPID

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsPions

        _piNoPID = FilterDesktop( Code = self._NominalPiSelectionNoPID() % self._config )
        _piSelNoPID=Selection("PiNoPID_for"+self._name,
                         Algorithm=_piNoPID,
                         RequiredSelections = [StdNoPIDsPions])
        self._pionSelNoPID = _piSelNoPID
        return _piSelNoPID

    ######--######
    def _pionFilterTight( self ):
        if self._pionSelTight is not None:
            return self._pionSelTight

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions

        _piTight = FilterDesktop( Code = self._NominalPiSelectionTight() % self._config )
        _piSelTight=Selection("PiTight_for"+self._name,
                         Algorithm=_piTight,
                         RequiredSelections = [StdLoosePions])
        self._pionSelTight=_piSelTight
        return _piSelTight

    ######--######
    def _pionFilterTightNoPID( self ):
        if self._pionSelTightNoPID is not None:
            return self._pionSelTightNoPID

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsPions

        _piNoPIDTight = FilterDesktop( Code = self._NominalPiSelectionTightNoPID() % self._config )
        _piSelNoPIDTight=Selection("PiTightNoPID_for"+self._name,
                         Algorithm=_piNoPIDTight,
                         RequiredSelections = [StdNoPIDsPions])
        self._pionSelTightNoPID=_piSelNoPIDTight
        return _piSelNoPIDTight

    ## Kaons

    ######Kaon Filter######
    def _kaonFilter( self ):
        if self._kaonSel is not None:
            return self._kaonSel

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseKaons

        _ka = FilterDesktop( Code = self._NominalKSelection() % self._config )
        _kaSel=Selection("K_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdLooseKaons])
        self._kaonSel=_kaSel
        return _kaSel

    ######--######
    def _kaonFilterNoPID( self ):
        if self._kaonSelNoPID is not None:
            return self._kaonSelNoPID

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseKaons

        _ka = FilterDesktop( Code = self._NominalKSelectionNoPID() % self._config )
        _kaSelNoPID=Selection("KNoPID_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdLooseKaons])
        self._kaonSelNoPID=_kaSelNoPID
        return _kaSelNoPID

    ######--######
    def _kaonForPhiFilter( self ):
        if self._kaonForPhiSel is not None:
            return self._kaonForPhiSel

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseKaons

        _ka = FilterDesktop( Code = self._KforPhiSelection() % self._config )
        _kaSel=Selection("K_Phi"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdLooseKaons])
        self._kaonForPhiSel=_kaSel
        return _kaSel

    ######--######
    def _kaonForPhiFilterNoPID( self ):
        if self._kaonForPhiSelNoPID is not None:
            return self._kaonForPhiSelNoPID

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsKaons

        _kaNoPID = FilterDesktop( Code = self._KforPhiSelectionNoPID() % self._config )
        _kaSelNoPID=Selection("KNoPID_Phi"+self._name,
                         Algorithm=_kaNoPID,
                         RequiredSelections = [StdNoPIDsKaons])
        self._kaonForPhiSelNoPID = _kaSelNoPID
        return _kaSelNoPID

    ######Majorana Muon Filter######
    def _MajoranaLineMuFilter( self ):
        if self._MajoranaLineMuSel is not None:
            return self._MajoranaLineMuSel

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseMuons
        _mumajorana = FilterDesktop( Code = self._MajoranaLineMuSelection() % self._config )
        _mumajoranaSel=Selection("MuMajorana_for"+self._name,
                                 Algorithm=_mumajorana,
                                 RequiredSelections = [StdLooseMuons])

        self._MajoranaLineMuSel=_mumajoranaSel

        return _mumajoranaSel

    #####
    ## Composites
    #####

    ######--######
    def _KsFilter( self ):
        if self._KSSel is not None:
           return self._KSSel

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
        from PhysSelPython.Wrappers import Selection, DataOnDemand

        _code =  FilterDesktop( Code = self._NominalKsSelection() % self._config )

        _KsSel=Selection("KS02PiPi_for"+self._name,
                         Algorithm= _code,
                         RequiredSelections = [self._stdVeryLooseKsLL,self._stdLooseKsLD,self._stdLooseKsDD])

        self._KSSel = _KsSel

        return _KsSel

    ##### phi -> KK filter (generall di-kaon filter) #####
    def _Phi2KKFilter( self ):
        if self._phi1020Sel is not None:
            return self._phi1020Sel

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _phi1020 = CombineParticles(
            DecayDescriptors = ["phi(1020) -> K- K+"] ,
            CombinationCut = "(AM< %(PhiUpperMass)s)" % self._config,
            MotherCut       = "(VFASPF(VCHI2/VDOF) < %(PhiVCHI2DOF)s ) & (PT > %(PhiPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(PhiMINIPCHI2)s ) & (BPVDIRA> %(PhiDIRA)s)" %self._config
            )
        _phi1020Sel=Selection("PhiKK_for"+self._name,
                             Algorithm=_phi1020,
                             RequiredSelections = [self._kaonForPhiFilter()])

        self._phi1020Sel=_phi1020Sel

        return _phi1020Sel

    ######--######
    def _Phi2KKFilterNoPID( self ):
        if self._phi1020SelNoPID is not None:
            return self._phi1020SelNoPID

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _phi1020NoPID = CombineParticles(
            DecayDescriptors = ["phi(1020) -> K- K+"] ,
            CombinationCut = "(AM< %(PhiUpperMass)s)" % self._config,
            MotherCut       = "(VFASPF(VCHI2/VDOF) < %(PhiVCHI2DOF)s ) & (PT > %(PhiPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(PhiMINIPCHI2)s ) & (BPVDIRA> %(PhiDIRA)s)" %self._config
            )
        _phi1020SelNoPID=Selection("PhiKKNoPID_for"+self._name,
                             Algorithm=_phi1020NoPID,
                             RequiredSelections = [self._kaonForPhiFilterNoPID()])

        self._phi1020SelNoPID=_phi1020SelNoPID

        return _phi1020SelNoPID

    ######--######
    def _Phi2KKFilterWS( self ):
        if self._phi1020SelWS is not None:
            return self._phi1020SelWS

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _phi1020WS = CombineParticles(
            DecayDescriptors = ["[phi(1020) -> K+ K+]cc"] ,
            CombinationCut = "(AM< %(PhiUpperMass)s)" % self._config,
            MotherCut       = "(VFASPF(VCHI2/VDOF) < %(PhiVCHI2DOF)s ) & (PT > %(PhiPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(PhiMINIPCHI2)s ) & (BPVDIRA> %(PhiDIRA)s)" %self._config
            )
        _phi1020SelWS=Selection("PhiKKWS_for"+self._name,
                                Algorithm=_phi1020WS,
                                RequiredSelections = [self._kaonForPhiFilter()])

        self._phi1020SelWS=_phi1020SelWS

        return _phi1020SelWS


    #####Make the Rho######
    def _Rho02PiPiFilter( self ):
        if self._rho770Sel is not None:
            return self._rho770Sel

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions

        _rho770 = CombineParticles(
            DecayDescriptors = ["rho(770)0 -> pi- pi+"] ,
            DaughtersCuts   = {"pi+":"(PT> %(RhoChPionPT)s *MeV) & (TRCHI2DOF < %(RhoChPionTRCHI2)s )"\
                               "& (MIPCHI2DV(PRIMARY)> %(RhoChPionMINIPCHI2)s )"\
                               "& (PIDK < %(RhoChPionPIDK)s ) & (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config
                               },
            CombinationCut = "(ADAMASS('rho(770)0')< %(RhoMassWindow)s)" % self._config,
            MotherCut       = "(MAXTREE('pi+'==ABSID,PT )>%(RhoLeadingPionPT)s *MeV )"\
            "& (MAXTREE('pi+'==ABSID,P )>%(RhoLeadingPionP)s *MeV )"\
            "& (VFASPF(VCHI2/VDOF) < %(RhoVCHI2DOF)s ) & (PT > %(RhoPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(RhoMINIPCHI2)s ) & (BPVDIRA> %(RhoDIRA)s)"\
            "& (DMASS('rho(770)0')< %(RhoMassWindow)s) " %self._config
            )
        _rho770Sel=Selection("Rho02PiPi_for"+self._name,
                             Algorithm=_rho770,
                             RequiredSelections = [self._pionFilter()])

        self._rho770Sel=_rho770Sel

        return _rho770Sel

    ######--######
    def _Rho02PiPiFilterNoPID( self ):
        if self._rho770SelNoPID is not None:
            return self._rho770SelNoPID

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions

        _rho770NoPID = CombineParticles(
            DecayDescriptors = ["rho(770)0 -> pi- pi+"] ,
            DaughtersCuts   = {"pi+":"(PT> %(RhoChPionPT)s *MeV) & (TRCHI2DOF < %(RhoChPionTRCHI2)s )"\
                               "& (MIPCHI2DV(PRIMARY)> %(RhoChPionMINIPCHI2)s )"\
                               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config
                               },
            CombinationCut = "(ADAMASS('rho(770)0')< %(RhoMassWindow)s)" % self._config,
            MotherCut       = "(MAXTREE('pi+'==ABSID,PT )>%(RhoLeadingPionPT)s *MeV )"\
            "& (MAXTREE('pi+'==ABSID,P )>%(RhoLeadingPionP)s *MeV )"\
            "& (VFASPF(VCHI2/VDOF) < %(RhoVCHI2DOF)s ) & (PT > %(RhoPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(RhoMINIPCHI2)s ) & (BPVDIRA> %(RhoDIRA)s)"\
            "& (DMASS('rho(770)0')< %(RhoMassWindow)s) " %self._config
            )
        _rho770SelNoPID=Selection("Rho02PiPiNoPID_for"+self._name,
                                  Algorithm=_rho770NoPID,
                                  RequiredSelections = [self._pionFilterNoPID()])

        self._rho770SelNoPID=_rho770SelNoPID

        return _rho770SelNoPID

    ######--######
    def _Rho02PiPiWSFilter( self ):
        if self._rho770WSSel is not None:
            return self._rho770WSSel

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions

        _rho770WS = CombineParticles(
            DecayDescriptors = ["[rho(770)0 -> pi+ pi+]cc"] ,
            DaughtersCuts   = {"pi+":"(PT> %(RhoChPionPT)s *MeV) & (TRCHI2DOF < %(RhoChPionTRCHI2)s )"\
                               "& (MIPCHI2DV(PRIMARY)> %(RhoChPionMINIPCHI2)s )"\
                               "& (PIDK < %(RhoChPionPIDK)s ) & (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config
                               },
            CombinationCut = "(ADAMASS('rho(770)0')< %(RhoMassWindow)s)" % self._config,
            MotherCut      = "(MAXTREE('pi+'==ABSID,PT )>%(RhoLeadingPionPT)s *MeV )"\
            "& (VFASPF(VCHI2/VDOF) < %(RhoVCHI2DOF)s ) & (PT > %(RhoPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(RhoMINIPCHI2)s ) & (BPVDIRA> %(RhoDIRA)s)"\
            "& (DMASS('rho(770)0')< %(RhoMassWindow)s) " %self._config
            )
        _rho770SelWS=Selection("Rho02PiPiWS_for"+self._name,
                             Algorithm=_rho770WS,
                             RequiredSelections = [self._pionFilter()])

        self._rho770WSSel=_rho770SelWS
        return _rho770SelWS

    ######--######
    def _Rho02PiPiSBFilter( self ):
        if self._rho770SBSel is not None:
            return self._rho770SBSel

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions

        _rho770SB = CombineParticles(
            DecayDescriptors = ["rho(770)0 -> pi+ pi-"] ,
            DaughtersCuts   = {"pi+":"(PT> %(RhoChPionPT)s *MeV) & (TRCHI2DOF < %(RhoChPionTRCHI2)s )"\
                               "& (MIPCHI2DV(PRIMARY)> %(RhoChPionMINIPCHI2)s ) & (PIDpi-PIDK> %(RhoChPionPIDK)s ) & (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config
                               },
            CombinationCut = "((AM > %(RhoMassWindowMin1SB)s) & (AM < %(RhoMassWindowMax1SB)s))"\
            " | ((AM > %(RhoMassWindowMin2SB)s) & (AM < %(RhoMassWindowMax2SB)s))" % self._config,

            MotherCut       = "(MAXTREE('pi+'==ABSID,PT )>%(RhoLeadingPionPT)s *MeV )"\
            "& (VFASPF(VCHI2/VDOF) < %(RhoVCHI2DOF)s ) & (PT > %(RhoPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(RhoMINIPCHI2)s ) & (BPVDIRA> %(RhoDIRA)s)"%self._config
            )
        _rho770SelSB=Selection("Rho02PiPiSB_for"+self._name,
                               Algorithm=_rho770SB,
                               RequiredSelections = [self._pionFilter()])

        self._rho770SBSel=_rho770SelSB
        return _rho770SelSB

    ### Make the omega(782) ###
    def _Omega2PiPiPi0Filter( self ):
        if self._omega782Sel is not None:
            return self._omega782Sel

        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions, StdLooseResolvedPi0, StdLooseMergedPi0

        _omega782 = DaVinci__N3BodyDecays(
            DecayDescriptor  = "omega(782) -> pi+ pi- pi0" ,
            DaughtersCuts    = {"pi+":"(PT> %(OmegaChPionPT)s *MeV)"\
                                "& (MIPCHI2DV(PRIMARY)> %(OmegaChPionMINIPCHI2)s )"\
                                "& (PIDK < %(OmegaChPionPIDK)s ) & (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config,
                                "pi0":"(PT > %(OmegaPi0PT)s *MeV)" % self._config
                                },
            CombinationCut   = "(ADAMASS('omega(782)')< %(OmegaMassWindow)s)" % self._config,
            Combination12Cut = "(AM < 800*MeV)&(ACHI2DOCA(1,2) < 8)",
            MotherCut        = "(VFASPF(VCHI2/VDOF) < %(OmegaVCHI2DOF)s) & (DMASS('omega(782)')< %(OmegaMassWindow)s) " %self._config
            )
        _omega782Sel=Selection("Omega2PiPiPi0_for"+self._name,
                               Algorithm=_omega782,
                               RequiredSelections = [StdLoosePions, StdLooseResolvedPi0, StdLooseMergedPi0])

        self._omega782Sel=_omega782Sel

        return _omega782Sel

    ######--######
    def _Omega2PiPiPi0FilterNoPID( self ):
        if self._omega782SelNoPID is not None:
            return self._omega782SelNoPID

        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsPions, StdLooseResolvedPi0, StdLooseMergedPi0

        _omega782NoPID = DaVinci__N3BodyDecays(
            DecayDescriptor  = "omega(782) -> pi+ pi- pi0" ,
            DaughtersCuts    = {"pi+":"(PT> %(OmegaChPionPT)s *MeV)"\
                                "& (MIPCHI2DV(PRIMARY)> %(OmegaChPionMINIPCHI2)s )"\
                                "& (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config,
                                "pi0":"(PT > %(OmegaPi0PT)s *MeV)" % self._config
                                },
            CombinationCut   = "(ADAMASS('omega(782)')< %(OmegaMassWindow)s)" % self._config,
            Combination12Cut = "(AM < 800*MeV)&(ACHI2DOCA(1,2) < 8)",
            MotherCut        = "(VFASPF(VCHI2/VDOF) < %(OmegaVCHI2DOF)s) & (DMASS('omega(782)')< %(OmegaMassWindow)s) " %self._config
            )
        _omega782SelNoPID=Selection("Omega2PiPiPi0NoPID_for"+self._name,
                                    Algorithm=_omega782NoPID,
                                    RequiredSelections = [StdNoPIDsPions, StdLooseResolvedPi0, StdLooseMergedPi0])

        self._omega782SelNoPID=_omega782SelNoPID

        return _omega782SelNoPID

    ######--######
    def _Omega2PiPiPi0WSFilter( self ):
        if self._omega782WSSel is not None:
            return self._omega782WSSel

        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions, StdLooseResolvedPi0, StdLooseMergedPi0

        _omega782WS = DaVinci__N3BodyDecays(
            DecayDescriptor   = "[omega(782) -> pi+ pi+ pi0]cc" ,
            DaughtersCuts     = {"pi+":"(PT> %(OmegaChPionPT)s *MeV)"\
                                 "& (MIPCHI2DV(PRIMARY)> %(OmegaChPionMINIPCHI2)s )"\
                                 "& (PIDK < %(OmegaChPionPIDK)s ) & (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config,
                                 "pi0":"(PT > %(OmegaPi0PT)s *MeV)" % self._config
                                 },
            CombinationCut    = "(ADAMASS('omega(782)')< %(OmegaMassWindow)s)" % self._config,
            Combination12Cut  = "(AM < 800*MeV)&(ACHI2DOCA(1,2) < 8)",
            MotherCut         = "(VFASPF(VCHI2/VDOF) < %(OmegaVCHI2DOF)s) & (DMASS('omega(782)')< %(OmegaMassWindow)s) " %self._config
            )
        _omega782WSSel=Selection("Omega2PiPiPi0WS_for"+self._name,
                               Algorithm=_omega782WS,
                               RequiredSelections = [StdLoosePions, StdLooseResolvedPi0, StdLooseMergedPi0])

        self._omega782WSSel=_omega782WSSel

        return _omega782WSSel

    ######Make the Kshort->MuPi Same Sign for Majorana####
    def _KshMajoranaSSMuFilter( self ):
        if self._KshMajoranaSSMuSel is not None:
            return self._KshMajoranaSSMuSel

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions, StdLooseMuons

        _Ksh = CombineParticles(
            DecayDescriptor = "KS0 -> mu- pi+",
            DaughtersCuts   = {"pi+":"(P > %(KS0DaugP)s)& (PT > %(KS0DaugPT)s)"\
                               "& (TRCHI2DOF < %(KS0DaugTrackChi2)s)" \
                               "& (MIPCHI2DV(PRIMARY) > %(KS0DaugMIPChi2)s)"   % self._config
                               },
            CombinationCut  = "(ADOCACHI2CUT(25, ''))"% self._config,

            MotherCut       = "(BPVVDCHI2> %(KSMajoranaCutFDChi2)s )& (VFASPF(VCHI2/VDOF) < %(KS0VertexChi2)s)&(PT > %(KS0PT)s*MeV)" % self._config
            )

        #_Ksh.ReFitPVs = True
        _KshMajoranaSSMuSel=Selection("KshMajoranaSSMu_for"+self._name,
                                      Algorithm=_Ksh,
                                      RequiredSelections = [StdLoosePions, self._MajoranaLineMuFilter()])
        self._KshMajoranaSSMuSel=_KshMajoranaSSMuSel

        return _KshMajoranaSSMuSel

    ######Make the Kshort->MuPi Opposite Sign for Majorana####
    def _KshMajoranaOSMuFilter( self ):
        if self._KshMajoranaOSMuSel is not None:
            return self._KshMajoranaOSMuSel

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions, StdLooseMuons

        _Ksh = CombineParticles(
            DecayDescriptor = "KS0 -> mu+ pi-",
            DaughtersCuts   = {"pi-":"(P > %(KS0DaugP)s)& (PT > %(KS0DaugPT)s)"\
                               "&(TRCHI2DOF < %(KS0DaugTrackChi2)s)"\
                               "&(MIPCHI2DV(PRIMARY) > %(KS0DaugMIPChi2)s)"   % self._config
                               },
            CombinationCut  = "(ADOCACHI2CUT(25, ''))"% self._config,

            MotherCut       = "(BPVVDCHI2> %(KSMajoranaCutFDChi2)s )& (VFASPF(VCHI2/VDOF) < %(KS0VertexChi2)s)&(PT > %(KS0PT)s*MeV)" % self._config
            )
        # _Ksh.ReFitPVs = True
        _KshMajoranaOSMuSel=Selection("KshMajoranaOSMu_for"+self._name,
                                      Algorithm=_Ksh,
                                      RequiredSelections = [StdLoosePions, self._MajoranaLineMuFilter()])
        self._KshMajoranaOSMuSel=_KshMajoranaOSMuSel

        return _KshMajoranaOSMuSel

    ## Kstars

    # Make the K*+ out of KS and pi+
    def _Kstar2KPiFilter( self ):
        if self._KstarSel is not None:
            return self._KstarSel

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions

        _Kstar2KsPi = CombineParticles(
            DecayDescriptors = ["[K*(892)+ -> KS0 pi+]cc"],
            CombinationCut = "(ADAMASS('K*(892)+')< %(KstarMassWindow)s )" % self._config,
            DaughtersCuts   = {"pi+":"(PT> %(KstarChPionPT)s*MeV) &(P> %(KstarChPionP)s*MeV)"\
                               "&  (TRCHI2DOF < %(KstarChPionTRCHI2)s )& (MIPCHI2DV(PRIMARY)>%(KstarChPionMINIPCHI2)s)"\
                               "& (PIDK < %(KstarChPionPIDK)s)" % self._config
                               },
            MotherCut = "(PT > %(KstarPT)s)" % self._config,
            ReFitPVs = True
            )
        _KstarSel = Selection("Kstar_for"+self._name,
                              Algorithm = _Kstar2KsPi,
                              RequiredSelections = [StdLoosePions, self._KsFilter()])
        self._KstarSel = _KstarSel

        return _KstarSel

    ######--######
    def _Kstar2KPiFilterNoPID( self ):
        if self._KstarSelNoPID is not None:
            return self._KstarSelNoPID

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsPions

        _Kstar2KsPiNoPID = CombineParticles(
            DecayDescriptors = ["[K*(892)+ -> KS0 pi+]cc"],
            CombinationCut = "(ADAMASS('K*(892)+')< %(KstarMassWindow)s )" % self._config,
            DaughtersCuts   = {"pi+":"(PT> %(KstarChPionPT)s*MeV) &(P> %(KstarChPionP)s*MeV)"\
                               "&  (TRCHI2DOF < %(KstarChPionTRCHI2)s )& (MIPCHI2DV(PRIMARY)>%(KstarChPionMINIPCHI2)s)" % self._config
                               },
            MotherCut = "(PT > %(KstarPT)s)" % self._config,
            ReFitPVs = True
            )
        _KstarSelNoPID = Selection("KstarNoPID_for"+self._name,
                                   Algorithm = _Kstar2KsPiNoPID,
                                   RequiredSelections = [StdNoPIDsPions, self._KsFilter()])
        self._KstarSelNoPID = _KstarSelNoPID

        return _KstarSelNoPID

    ################################
    ##
    ## B -> X mu nu definitions
    ##
    ################################

    ### B+ -> pi+ mu- nu
    def _B2PiMuNu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMu = CombineParticles(
            DecayDescriptors = ["[B0 -> pi+ mu-]cc"],
            CombinationCut = "(AM>%(KMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _KMuSel=Selection("PiMu_for"+self._name,
                         Algorithm=_KMu,
                         RequiredSelections = [self._muonFilterTOS(), self._pionFilterTight()])
        _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSel

    def _B2PiMuNuNoMuTopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuNoMuTopo = CombineParticles(
            DecayDescriptors = ["[B0 -> pi+ mu-]cc"],
            CombinationCut = "(AM>%(KMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _KMuSelNoMuTopo=Selection("PiMuNoMuTopo_for"+self._name,
                         Algorithm=_KMuNoMuTopo,
                         RequiredSelections = [self._muonFilter(), self._pionFilterTight()])
        _KMuSelNoMuTopo = tosSelection(_KMuSelNoMuTopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSelNoMuTopo

    def _B2PiMuNuNoPIDmu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuNoPIDmu = CombineParticles(
            DecayDescriptors = ["[B0 -> pi+ mu-]cc"],
            CombinationCut = "(AM>%(KMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _KMuSelNoPIDmu=Selection("PiMuNoPIDmu_for"+self._name,
                                 Algorithm=_KMuNoPIDmu,
                                 RequiredSelections = [self._muonFilterNoPID(), self._pionFilterTight()])
        _KMuSelNoPIDmu = tosSelection(_KMuSelNoPIDmu,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSelNoPIDmu

    def _B2PiMuNuNoPIDhad( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuNoPIDhad = CombineParticles(
            DecayDescriptors = ["[B0 -> pi+ mu-]cc"],
            CombinationCut = "(AM>%(KMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _KMuSelNoPIDhad=Selection("PiMuNoPIDhad_for"+self._name,
                                  Algorithm=_KMuNoPIDhad,
                                  RequiredSelections = [self._muonFilterTOS(), self._pionFilterTightNoPID()])
        _KMuSelNoPIDhad = tosSelection(_KMuSelNoPIDhad,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSelNoPIDhad

    def _B2PiMuNuSS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuSS = CombineParticles(
            DecayDescriptors = ["[B0 -> pi- mu-]cc"],
            CombinationCut = "(AM>%(KMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = " (VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRATight)s) "\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _KMuSelSS=Selection("PiMuSS_for"+self._name,
                          Algorithm=_KMuSS,
                          RequiredSelections = [self._muonFilterTOS(), self._pionFilterTight()])
        _KMuSelSS = tosSelection(_KMuSelSS,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSelSS


    ######Bs->KMuNu######
    def _Bs2KMuNu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMu = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K+ mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KMuSel=Selection("KMu_for"+self._name,
                         Algorithm=_KMu,
                         RequiredSelections = [self._muonFilterTOS(), self._kaonFilter()])
        _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSel

    def _Bs2KMuNuNoMuTopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuNoMuTopo = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K+ mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KMuSelNoMuTopo=Selection("KMuNoMuTopo_for"+self._name,
                         Algorithm=_KMuNoMuTopo,
                         RequiredSelections = [self._muonFilter(), self._kaonFilter()])
        _KMuSelNoMuTopo = tosSelection(_KMuSelNoMuTopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSelNoMuTopo

    def _Bs2KMuNuSS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuSS = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K- mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KMuSelSS=Selection("KMuSS_for"+self._name,
                          Algorithm=_KMuSS,
                          RequiredSelections = [self._muonFilterTOS(), self._kaonFilter()])
        _KMuSelSS = tosSelection(_KMuSelSS,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSelSS

    ######Bs->KMuNu misid, no PID on mu######
    def _Bs2KMuNu_NoPIDmu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMu = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K+ mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KMuSel=Selection("KMu_NoPIDmu_for"+self._name,
                         Algorithm=_KMu,
                         RequiredSelections = [self._muonFilterNoPID(), self._kaonFilter()])
        _KMuSel = tosSelection(_KMuSel,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSel

    def _Bs2KMuNuSS_NoPIDmu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuSS = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K- mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KMuSelSS=Selection("KMuSS_NoPIDmu_for"+self._name,
                          Algorithm=_KMuSS,
                          RequiredSelections = [self._muonFilterTightNoPID(), self._kaonFilter()])
        _KMuSelSS = tosSelection(_KMuSelSS,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSelSS

    ######Bs->KMuNu misid, no PID on K######
    def _Bs2KMuNu_NoPIDK( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMu = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K+ mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KMuSel=Selection("KMu_NoPIDK_for"+self._name,
                         Algorithm=_KMu,
                         RequiredSelections = [self._muonFilterTOS(), self._kaonFilterNoPID()])
        _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSel

    def _Bs2KMuNuSS_NoPIDK( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuSS = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K- mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KMuSelSS=Selection("KMuSS_NoPIDK_for"+self._name,
                          Algorithm=_KMuSS,
                          RequiredSelections = [self._muonFilterTOS(), self._kaonFilterNoPID()])
        _KMuSelSS = tosSelection(_KMuSelSS,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSelSS

    ######Bs->KMuNu misid, no PID on mu or K######
    def _Bs2KMuNu_NoPIDKmu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMu = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K+ mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KMuSel=Selection("KMu_NoPIDKmu_for"+self._name,
                         Algorithm=_KMu,
                         RequiredSelections = [self._muonFilterNoPID(), self._kaonFilterNoPID()])
        _KMuSel = tosSelection(_KMuSel,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSel

    def _Bs2KMuNuSS_NoPIDKmu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuSS = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K- mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KMuSelSS=Selection("KMuSS_NoPIDKmu_for"+self._name,
                          Algorithm=_KMuSS,
                          RequiredSelections = [self._muonFilterNoPID(), self._kaonFilterNoPID()])
        _KMuSelSS = tosSelection(_KMuSelSS,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KMuSelSS

   ######Bu->PhiMuNu ######
    def _B2PhiMuNu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _PhiMu = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) mu+]cc"],
            CombinationCut = "(AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiMu_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiMuSel=Selection("PhiMu_for"+self._name,
                         Algorithm=_PhiMu,
                         RequiredSelections = [self._muonFilterTOS(), self._Phi2KKFilter()])
            # _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        _PhiMuSel = tosSelection(_PhiMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _PhiMuSel

    def _B2PhiMuNuNoMuTopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _PhiMuNoMuTopo = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) mu+]cc"],
            CombinationCut = "(AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiMu_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiMuSelNoMuTopo=Selection("PhiMuNoMuTopo_for"+self._name,
                         Algorithm=_PhiMuNoMuTopo,
                         RequiredSelections = [self._muonFilter(), self._Phi2KKFilter()])
            # _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        _PhiMuSelNoMuTopo = tosSelection(_PhiMuSelNoMuTopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _PhiMuSelNoMuTopo


    def _B2PhiMuNuWS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _PhiMuWS = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) mu+]cc"],
            CombinationCut = "(AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiMu_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiMuSelWS=Selection("PhiMuWS_for"+self._name,
                         Algorithm=_PhiMuWS,
                         RequiredSelections = [self._muonFilterTOS(), self._Phi2KKFilterWS()])
            # _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        _PhiMuSelWS = tosSelection(_PhiMuSelWS,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _PhiMuSelWS

   ######Bu->PhiMuNu Fake Mu ######

    def _B2PhiMuNu_NoPIDmu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _PhiMu = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) mu+]cc"],
            CombinationCut = "(AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiMu_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiMuNoPIDmuSel=Selection("PhiMu_NoPIDmu_for"+self._name,
                         Algorithm=_PhiMu,
                         RequiredSelections = [self._muonFilterNoPID(), self._Phi2KKFilter()])
            # _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        _PhiMuNoPIDmuSel = tosSelection(_PhiMuNoPIDmuSel,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _PhiMuNoPIDmuSel

    def _B2PhiMuNu_NoPIDhad( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _PhiMuNoPIDHad = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) mu+]cc"],
            CombinationCut = "(AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiMu_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiMuNoPIDhadSel=Selection("PhiMu_NoPIDHad_for"+self._name,
                                    Algorithm=_PhiMuNoPIDHad,
                                    RequiredSelections = [self._muonFilterTightNoPID(), self._Phi2KKFilterNoPID()])
        # _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        _PhiMuNoPIDhadSel = tosSelection(_PhiMuNoPIDhadSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _PhiMuNoPIDhadSel

    def _B2PhiMuNuBadVtx( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _PhiMuBadVtx = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) mu+]cc"],
            CombinationCut = "(AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiMu_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)> %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiMuSelBadVtx=Selection("PhiMuBadVtx_for"+self._name,
                         Algorithm=_PhiMuBadVtx,
                         RequiredSelections = [self._muonFilterTOS(), self._Phi2KKFilter()])
            # _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        _PhiMuSelBadVtx = tosSelection(_PhiMuSelBadVtx,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _PhiMuSelBadVtx



    ######Bu->RhoMuNu ######
    def _Bu2RhoMuNu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _RhoMu = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _RhoMuSel=Selection("RhoMu_for"+self._name,
                            Algorithm=_RhoMu,
                            RequiredSelections = [self._muonFilterTight(), self._Rho02PiPiFilter()])
        _RhoMuSel = tosSelection(_RhoMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _RhoMuSel

    def _Bu2RhoMuNuNoMuTopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _RhoMuNoMuTopo = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _RhoMuSelNoMuTopo=Selection("RhoMuNoMuTopo_for"+self._name,
                            Algorithm=_RhoMuNoMuTopo,
                            RequiredSelections = [self._muonFilterTight(), self._Rho02PiPiFilter()])
        _RhoMuSelNoMuTopo = tosSelection(_RhoMuSelNoMuTopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _RhoMuSelNoMuTopo


    def _Bu2RhoMuNu_NoPIDmu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _RhoMu_NoPIDmu = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _RhoMuSel_NoPIDmu=Selection("RhoMu_NoPIDmu_for"+self._name,
                            Algorithm=_RhoMu_NoPIDmu,
                            RequiredSelections = [self._muonFilterTightNoPID(), self._Rho02PiPiFilter()])
        _RhoMuSel_NoPIDmu = tosSelection(_RhoMuSel_NoPIDmu,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _RhoMuSel_NoPIDmu

    def _Bu2RhoMuNu_NoPIDhad( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _RhoMu_NoPIDhad = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _RhoMuSel_NoPIDhad=Selection("RhoMu_NoPIDHad_for"+self._name,
                            Algorithm=_RhoMu_NoPIDhad,
                            RequiredSelections = [self._muonFilterTight(), self._Rho02PiPiFilterNoPID()])
        _RhoMuSel_NoPIDhad = tosSelection(_RhoMuSel_NoPIDhad,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _RhoMuSel_NoPIDhad


    def _Bu2RhoMuNuWS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _RhoMuWS = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _RhoMuSelWS=Selection("RhoMuWS_for"+self._name,
                         Algorithm=_RhoMuWS,
                         RequiredSelections = [self._muonFilterTight(), self._Rho02PiPiWSFilter()])
        _RhoMuSelWS = tosSelection(_RhoMuSelWS,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _RhoMuSelWS

    def _Bu2RhoMuNuBadVtx( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _RhoMuBadVtx = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)> %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _RhoMuSelBadVtx=Selection("RhoMuBadVtx_for"+self._name,
                                  Algorithm=_RhoMuBadVtx,
                                  RequiredSelections = [self._muonFilterTight(), self._Rho02PiPiFilter()])
        _RhoMuSelBadVtx = tosSelection(_RhoMuSelBadVtx,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _RhoMuSelBadVtx

    ### This is not needed anymore, as long as the pi+pi- mass window is large enough.
    def _Bu2RhoMuNuSB( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _RhoMu = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut ="(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s) & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (ratio >0.4)"% self._config,
            #" ( ((((5279.17*5279.17) -(MASS(1,2))*(MASS(1,2))))/(2*5279.17)) < %(Enu)s*MeV)"\

            ReFitPVs = True
            )

        _RhoMu.Preambulo = [ "from LoKiPhys.decorators import *",
                           "dx = (VFASPF(VX)-BPV(VX))",
                           "dy = (VFASPF(VY)-BPV(VY))",
                           "dz = (VFASPF(VZ)-BPV(VZ))",
                           "norm = (max(sqrt(dx*dx+dy*dy+dz*dz),0))",
                           "B_xdir  = ((dx)/norm)",
                           "B_ydir  = ((dy)/norm)",
                           "B_zdir  = ((dz)/norm)",
                           "Pxrhomu   = (CHILD(PX,1)+CHILD(PX,2))",
                           "Pyrhomu   = (CHILD(PY,1)+CHILD(PY,2))",
                           "Pzrhomu   = (CHILD(PZ,1)+CHILD(PZ,2))",
                           "Bflight = (B_xdir*Pxrhomu + B_ydir*Pyrhomu+ B_zdir*Pzrhomu)",
                           "mB  = 5279.25",
                           "M_2 = (mB*mB + M12*M12)",
                           "rhomu_E  = (CHILD(E,1)+CHILD(E,2))",
                           "quada = (Bflight*Bflight - rhomu_E*rhomu_E)",
                           "quadb = (M_2*Bflight)",
                           "quadc = (((M_2*M_2)/4) - (rhomu_E*rhomu_E*mB*mB))",
                           "Discriminant = ((quadb*quadb)-(4*quada*quadc))",
                           "solution_a = ((-quadb + sqrt(max(Discriminant,0)))/(2*quada))",
                           "solution_b = ((-quadb - sqrt(max(Discriminant,0)))/(2*quada))",
                           "ratio = (solution_a/solution_b)"
                           ]

        _RhoMuSel=Selection("RhoMuSB_for"+self._name,
                         Algorithm=_RhoMu,
                         RequiredSelections = [self._muonFilterTight(), self._Rho02PiPiSBFilter()])
        _RhoMuSel = tosSelection(_RhoMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _RhoMuSel

    ######Bu->OmegaMuNu ######
    def _Bu2OmegaMuNu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _OmegaMu = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _OmegaMuSel=Selection("OmegaMu_for"+self._name,
                              Algorithm=_OmegaMu,
                              RequiredSelections = [self._muonFilterTight(), self._Omega2PiPiPi0Filter()])
        _OmegaMuSel = tosSelection(_OmegaMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _OmegaMuSel

    def _Bu2OmegaMuNuNoMuTopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _OmegaMuNoMuTopo = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _OmegaMuSelNoMuTopo=Selection("OmegaMuNoMuTopo_for"+self._name,
                              Algorithm=_OmegaMuNoMuTopo,
                              RequiredSelections = [self._muonFilterTight(), self._Omega2PiPiPi0Filter()])
        _OmegaMuSelNoMuTopo = tosSelection(_OmegaMuSelNoMuTopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _OmegaMuSelNoMuTopo


    def _Bu2OmegaMuNu_NoPIDmu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _OmegaMu_NoPIDmu = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _OmegaMuSel_NoPIDmu=Selection("OmegaMu_NoPIDmu_for"+self._name,
                                      Algorithm=_OmegaMu_NoPIDmu,
                                      RequiredSelections = [self._muonFilterTightNoPID(), self._Omega2PiPiPi0Filter()])
        _OmegaMuSel_NoPIDmu = tosSelection(_OmegaMuSel_NoPIDmu,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _OmegaMuSel_NoPIDmu

    def _Bu2OmegaMuNu_NoPIDhad( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _OmegaMu_NoPIDhad = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _OmegaMuSel_NoPIDhad=Selection("OmegaMu_NoPIDhad_for"+self._name,
                                      Algorithm=_OmegaMu_NoPIDhad,
                                      RequiredSelections = [self._muonFilterTight(), self._Omega2PiPiPi0FilterNoPID()])
        _OmegaMuSel_NoPIDhad = tosSelection(_OmegaMuSel_NoPIDhad,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _OmegaMuSel_NoPIDhad


    def _Bu2OmegaMuNuWS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _OmegaMuWS = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _OmegaMuSelWS=Selection("OmegaMuWS_for"+self._name,
                                Algorithm=_OmegaMuWS,
                                RequiredSelections = [self._muonFilterTight(), self._Omega2PiPiPi0WSFilter()])
        _OmegaMuSelWS = tosSelection(_OmegaMuSelWS,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _OmegaMuSelWS

    def _Bu2OmegaMuNuBadVtx( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _OmegaMuBadVtx = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)> %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _OmegaMuSelBadVtx=Selection("OmegaMuBadVtx_for"+self._name,
                                    Algorithm=_OmegaMuBadVtx,
                                    RequiredSelections = [self._muonFilterTight(), self._Omega2PiPiPi0Filter()])
        _OmegaMuSelBadVtx = tosSelection(_OmegaMuSelBadVtx,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _OmegaMuSelBadVtx


    ######Bu->KshMuNu SS & OS######
    def _Bu2KshSSMuNu_SSMuminus( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KshSSMu_SSMuminus = CombineParticles(
            DecayDescriptors = ["[B- -> KS0 mu-]cc"],
            CombinationCut = "(AM>%(KS0MuMassLowTight)s*MeV) & (AM<%(XMuMassUpperHigh)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)"\
            "& (MINTREE( (ABSID=='KS0') , VFASPF(VZ))-VFASPF(VZ) > %(KS0Z)s *mm )" % self._config,
            ReFitPVs = True
            )

        _KshSSMu_SSMuminusSel=Selection("KshSSMu_SSMuminus_for"+self._name,
                              Algorithm=_KshSSMu_SSMuminus,
                              RequiredSelections = [self._muonFilter(), self._KshMajoranaSSMuFilter()])
        return _KshSSMu_SSMuminusSel


    def _Bu2KshOSMuNu_SSMuplus( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KshOSMu_SSMuplus = CombineParticles(
            DecayDescriptors = ["[B- -> KS0 mu-]cc"],
            CombinationCut = "(AM>%(KS0MuMassLowTight)s*MeV) & (AM<%(XMuMassUpperHigh)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)"\
            "& (MINTREE( (ABSID=='KS0') , VFASPF(VZ))-VFASPF(VZ) > %(KS0Z)s *mm )" % self._config,
            ReFitPVs = True
            )

        _KshOSMu_SSMuplusSel=Selection("KshOSMu_SSMuplus_for"+self._name,
                              Algorithm=_KshOSMu_SSMuplus,
                              RequiredSelections = [self._muonFilter(), self._KshMajoranaOSMuFilter()])
        return _KshOSMu_SSMuplusSel

#    ###Combine Bs->K*(892)+MuNu, K*->KS0Pi###
    def _Bs2KstarMuNu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KstarMu = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)+ mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )

        _KstarMuSel=Selection("KstarMu_for"+self._name,
                                Algorithm=_KstarMu,
                                RequiredSelections = [self._muonFilter(), self._Kstar2KPiFilter()])
        _KstarMuSel = tosSelection(_KstarMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KstarMuSel

    def _Bs2KstarMuNuNoMuTopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KstarMuNoMuTopo = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)+ mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )

        _KstarMuSelNoMuTopo=Selection("KstarMuNoMuTopo_for"+self._name,
                                Algorithm=_KstarMuNoMuTopo,
                                RequiredSelections = [self._muonFilter(), self._Kstar2KPiFilter()])
        _KstarMuSelNoMuTopo = tosSelection(_KstarMuSelNoMuTopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KstarMuSelNoMuTopo


    def _Bs2KstarMuNu_NoPIDmu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KstarMuNoPIDmu = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)+ mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )

        _KstarMuSelNoPIDmu=Selection("KstarMuNoPIDmu_for"+self._name,
                                Algorithm=_KstarMuNoPIDmu,
                                RequiredSelections = [self._muonFilterNoPID(), self._Kstar2KPiFilter()])
        _KstarMuSelNoPIDmu = tosSelection(_KstarMuSelNoPIDmu,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KstarMuSelNoPIDmu

    def _Bs2KstarMuNu_NoPIDhad( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KstarMuNoPIDhad = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)+ mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )

        _KstarMuSelNoPIDhad=Selection("KstarMuNoPIDHad_for"+self._name,
                                      Algorithm=_KstarMuNoPIDhad,
                                      RequiredSelections = [self._muonFilter(), self._Kstar2KPiFilterNoPID()])
        _KstarMuSelNoPIDhad = tosSelection(_KstarMuSelNoPIDhad,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KstarMuSelNoPIDhad


    def _Bs2KstarMuNuSS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KstarMuSS = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)- mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )

        _KstarMuSSSel=Selection("KstarMuSS_for"+self._name,
                                  Algorithm=_KstarMuSS,
                                  RequiredSelections = [self._muonFilter(), self._Kstar2KPiFilter()])
        _KstarMuSSSel = tosSelection(_KstarMuSSSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KstarMuSSSel

    def _Bs2KstarMuNuBadVtx( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KstarMuBadVtx = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)+ mu-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)> %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )

        _KstarMuSelBadVtx=Selection("KstarMuBadVtx_for"+self._name,
                                Algorithm=_KstarMuBadVtx,
                                RequiredSelections = [self._muonFilter(), self._Kstar2KPiFilter()])
        _KstarMuSelBadVtx = tosSelection(_KstarMuSelBadVtx,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0})
        return _KstarMuSelBadVtx
