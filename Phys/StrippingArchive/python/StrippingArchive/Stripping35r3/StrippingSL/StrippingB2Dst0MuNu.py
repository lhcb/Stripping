###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = ['Michel De Cian']
__date__ = '12/12/2018'
__version__ = '$Revision: 0.0 $'

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLoosePions, StdLooseMuons, StdLooseKaons, StdAllLoosePions, StdAllLooseElectrons, StdLooseResolvedPi0, StdLooseMergedPi0, StdLooseDalitzPi0

__all__ = ('B2Dst0MuNuAllLinesConf',
           'TOSFilter',
           'default_config')

default_config = {
    'NAME'        : 'B2Dst0MuNu',
    'WGs'         : ['Semileptonic'],
    'BUILDERTYPE' : 'B2Dst0MuNuAllLinesConf',
    'CONFIG'      : {
            "TTSpecs"      : {}#{'Hlt1.*Track.*Decision%TOS':0,'Hlt2TopoMu(2|3|4)Body.*Decision%TOS':0,'Hlt2.*SingleMuon.*Decision%TOS':0}
            ,"HLT_FILTER"   : ""#"HLT_PASS_RE('Hlt2.*SingleMuon.*Decision') | HLT_PASS_RE('Hlt2TopoMu(2|3|4)Body.*Decision')"
            },
    'STREAMS'     : ['Semileptonic'],
}

class B2Dst0MuNuAllLinesConf(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()

    __confdict__={}

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)
        self.__confdict__=config

        # The D0
        self.D0 = Selection("D0For"+name,
                            Algorithm=CombineParticles(DecayDescriptors = ["[D0 -> K- pi+]cc"],
                                                       DaughtersCuts = {"K-":"(PIDK > 0) & (PT > 250*MeV)",
                                                                        "pi+":"(PIDK < 2) & (PT > 250*MeV)"},
                                                       CombinationCut = "(AM > 1700*MeV) & (AM < 2000*MeV) & (ADOCACHI2CUT(10, ''))",
                                                       MotherCut =    "(VFASPF(VCHI2/VDOF) < 4) & (BPVVDCHI2 > 100.) & (BPVDIRA> 0.99) & (ADMASS('D0') < 100*MeV)"),
                            RequiredSelections = [StdLooseKaons,StdLoosePions])

        # The Dielectron, detached (called gamma)
        self.gamma = Selection("GammaFor"+name,
                               Algorithm=CombineParticles(DecayDescriptor = "gamma -> e+ e-",
                                                       DaughtersCuts = {"e+":"(PIDe > -2) & (PT > 70*MeV) & (TRGHP < 0.3)",
                                                                        "e-":"(PIDe > -2) & (PT > 70*MeV) & (TRGHP < 0.3)"},
                                                       CombinationCut = "(AM < 300*MeV)",
                                                       MotherCut =    "(VFASPF(VCHI2/VDOF) < 5) & (M < 250)"),
                               RequiredSelections = [StdAllLooseElectrons])

        # The Dielectron, detached and same-sign
        self.gammaSS = Selection("GammaSSFor"+name,
                                 Algorithm=CombineParticles(DecayDescriptor = "[gamma -> e+ e+]cc",
                                                            DaughtersCuts = {"e+":"(PIDe > -2) & (PT > 70*MeV) & (TRGHP < 0.3)"},
                                                            CombinationCut = "(AM < 350*MeV)",
                                                            MotherCut =    "(VFASPF(VCHI2/VDOF) < 5) & (M < 300)"),
                                 RequiredSelections = [StdAllLooseElectrons])

        # The Dielectron, prompt (called Aprime, for a potential short-lived dark photon)
        self.Aprime = Selection("AprimeFor"+name,
                                Algorithm=CombineParticles(DecayDescriptor = "J/psi(1S) -> e+ e-",
                                                       DaughtersCuts = {"e+":"(PIDe > -2) & (PT > 70*MeV) & (TRGHP < 0.3)",
                                                                        "e-":"(PIDe > -2) & (PT > 70*MeV) & (TRGHP < 0.3)",},
                                                       CombinationCut = "(AM < 350*MeV)",
                                                       MotherCut =    "(VFASPF(VCHI2/VDOF) < 5) & (M < 300)"),
                               RequiredSelections = [StdAllLooseElectrons])

        # The Dielectron, prompt and same-sign
        self.AprimeSS = Selection("AprimeSSFor"+name,
                                  Algorithm=CombineParticles(DecayDescriptor = "[J/psi(1S) -> e+ e+]cc",
                                                             DaughtersCuts = {"e+":"(PIDe > -2) & (PT > 70*MeV) & (TRGHP < 0.3)" },
                                                             CombinationCut = "(AM < 350*MeV)",
                                                             MotherCut =    "(VFASPF(VCHI2/VDOF) < 5) & (M < 300)"),
                                  RequiredSelections = [StdAllLooseElectrons])

        # The Dst with detached dielectron
        self.DstD0Gamma = Selection("DstD0GammaFor"+name,
                                    Algorithm=CombineParticles(DecayDescriptors = ["[D*(2007)0 -> D0 gamma]cc"],
                                                        DaughtersCuts = {"D0"    :"ALL",
                                                                         "gamma" : "ALL"},
                                                        CombinationCut = "(AM - ACHILD(1,M) < 350*MeV)",
                                                        MotherCut =    "(VFASPF(VCHI2/VDOF) < 6)"),
                             RequiredSelections = [self.D0,self.gamma])

        # The Dst with detached dielectron, same-sign
        self.DstD0GammaSS = Selection("DstD0GammaSSFor"+name,
                                      Algorithm=CombineParticles(DecayDescriptors = ["[D*(2007)0 -> D0 gamma]cc"],
                                                                 DaughtersCuts = {"D0"    :"ALL",
                                                                                  "gamma" : "ALL"},
                                                                 CombinationCut = "(AM - ACHILD(1,M) < 350*MeV)",
                                                                 MotherCut =    "(VFASPF(VCHI2/VDOF) < 6)"),
                                      RequiredSelections = [self.D0,self.gammaSS])

        # The Dst with prompt dielectron
        self.DstD0Aprime = Selection("DstD0AprimeFor"+name,
                                     Algorithm=CombineParticles(DecayDescriptors = ["[D*(2007)0 -> D0 J/psi(1S)]cc"],
                                                               DaughtersCuts = {"D0"    :"ALL",
                                                                                "J/psi(1S)" : "ALL"},
                                                               CombinationCut = "(AM - ACHILD(1,M) < 350*MeV)",
                                                               MotherCut =    "(VFASPF(VCHI2/VDOF) < 6)"),
                                     RequiredSelections = [self.D0,self.Aprime])

        # The Dst with prompt dielectron, same-sign
        self.DstD0AprimeSS = Selection("DstD0AprimeSSFor"+name,
                                       Algorithm=CombineParticles(DecayDescriptors = ["[D*(2007)0 -> D0 J/psi(1S)]cc"],
                                                                  DaughtersCuts = {"D0"    :"ALL",
                                                                                   "J/psi(1S)" : "ALL"},
                                                                  CombinationCut = "(AM - ACHILD(1,M) < 350*MeV)",
                                                                  MotherCut =    "(VFASPF(VCHI2/VDOF) < 6)"),
                                       RequiredSelections = [self.D0,self.AprimeSS])

        # The Dst with pi0
        self.DstD0Pi0 = Selection("DstD0Pi0For"+name,
                                  Algorithm=CombineParticles(DecayDescriptors = ["[D*(2007)0 -> D0 pi0]cc"],
                                                               DaughtersCuts = {"D0"    :"ALL",
                                                                                "pi0" : "(PT > 300*MeV)"},
                                                               CombinationCut = "(AM - ACHILD(1,M) < 350*MeV)",
                                                               MotherCut =    "(VFASPF(VCHI2/VDOF) < 6)"),
                                     RequiredSelections = [self.D0,StdLooseResolvedPi0, StdLooseMergedPi0])

        # The Dst with pi0 (Dalitz)
        self.DstD0Pi0Dalitz = Selection("DstD0Pi0DalitzFor"+name,
                                        Algorithm=CombineParticles(DecayDescriptors = ["[D*(2007)0 -> D0 pi0]cc"],
                                                                   DaughtersCuts = {"D0"    :"ALL",
                                                                                    "pi0" : "(PT > 300*MeV)"},
                                                                   CombinationCut = "(AM - ACHILD(1,M) < 350*MeV)",
                                                                   MotherCut =    "(VFASPF(VCHI2/VDOF) < 6)"),
                                        RequiredSelections = [self.D0,StdLooseDalitzPi0])

        # The B- with detached dielectron
        self.BDstD0Gamma = Selection("BDstD0GammaFor"+name,
                                     Algorithm=CombineParticles(DecayDescriptors = ["[B- -> D*(2007)0 mu-]cc"],
                                                                DaughtersCuts = {"mu-":"(PT > 1200*MeV) & (MIPCHI2DV(PRIMARY) > 16) & (PIDmu > 0)"},
                                                                CombinationCut = "(AM > 2200*MeV) & (AM < 6000*MeV)",
                                                                MotherCut =    "(VFASPF(VCHI2/VDOF) < 6) & (BPVVDCHI2 > 25.) & (BPVDIRA> 0.99)"),
                                     RequiredSelections = [self.DstD0Gamma,StdLooseMuons])

        # The B- with detached dielectron, same-sign
        self.BDstD0GammaSS = Selection("BDstD0GammaSSFor"+name,
                                     Algorithm=CombineParticles(DecayDescriptors = ["[B- -> D*(2007)0 mu-]cc"],
                                                                DaughtersCuts = {"mu-":"(PT > 1200*MeV) & (MIPCHI2DV(PRIMARY) > 16) & (PIDmu > 0)"},
                                                                CombinationCut = "(AM > 2200*MeV) & (AM < 6000*MeV)",
                                                                MotherCut =    "(VFASPF(VCHI2/VDOF) < 6) & (BPVVDCHI2 > 25.) & (BPVDIRA> 0.99)"),
                                     RequiredSelections = [self.DstD0GammaSS,StdLooseMuons])

        # The B- with prompt dielectron
        self.BDstD0Aprime = Selection("BDstD0AprimeFor"+name,
                                      Algorithm=CombineParticles(DecayDescriptors = ["[B- -> D*(2007)0 mu-]cc"],
                                                                 DaughtersCuts = {"mu-":"(PT > 1200*MeV) & (MIPCHI2DV(PRIMARY) > 16) & (PIDmu > 0)"},
                                                                 CombinationCut = "(AM > 2200*MeV) & (AM < 6000*MeV)",
                                                                 MotherCut =    "(VFASPF(VCHI2/VDOF) < 6) & (BPVVDCHI2 > 25.) & (BPVDIRA> 0.99)"),
                                      RequiredSelections = [self.DstD0Aprime,StdLooseMuons])

        # The B- with prompt dielectron, same-sign
        self.BDstD0AprimeSS = Selection("BDstD0AprimeSSFor"+name,
                                        Algorithm=CombineParticles(DecayDescriptors = ["[B- -> D*(2007)0 mu-]cc"],
                                                                   DaughtersCuts = {"mu-":"(PT > 1200*MeV) & (MIPCHI2DV(PRIMARY) > 16) & (PIDmu > 0)"},
                                                                   CombinationCut = "(AM > 2200*MeV) & (AM < 6000*MeV)",
                                                                   MotherCut =    "(VFASPF(VCHI2/VDOF) < 6) & (BPVVDCHI2 > 25.) & (BPVDIRA> 0.99)"),
                                        RequiredSelections = [self.DstD0AprimeSS,StdLooseMuons])

        # The B- with pi0
        self.BDstD0Pi0 = Selection("BDstD0Pi0For"+name,
                                   Algorithm=CombineParticles(DecayDescriptors = ["[B- -> D*(2007)0 mu-]cc"],
                                                              DaughtersCuts = {"mu-":"(PT > 1200*MeV) & (MIPCHI2DV(PRIMARY) > 16) & (PIDmu > 0)"},
                                                              CombinationCut = "(AM > 2200*MeV) & (AM < 6000*MeV)",
                                                              MotherCut =    "(VFASPF(VCHI2/VDOF) < 6) & (BPVVDCHI2 > 25.) & (BPVDIRA> 0.99)"),
                                   RequiredSelections = [self.DstD0Pi0,StdLooseMuons])

        # The B- with pi0 (Dalitz)
        self.BDstD0Pi0Dalitz = Selection("BDstD0Pi0DalitzFor"+name,
                                         Algorithm=CombineParticles(DecayDescriptors = ["[B- -> D*(2007)0 mu-]cc"],
                                                                    DaughtersCuts = {"mu-":"(PT > 1200*MeV) & (MIPCHI2DV(PRIMARY) > 16) & (PIDmu > 0)"},
                                                                    CombinationCut = "(AM > 2200*MeV) & (AM < 6000*MeV)",
                                                                    MotherCut =    "(VFASPF(VCHI2/VDOF) < 6) & (BPVVDCHI2 > 25.) & (BPVDIRA> 0.99)"),
                                         RequiredSelections = [self.DstD0Pi0Dalitz,StdLooseMuons])


        # Registering the lines
        self.lineDstD0Gamma    =  StrippingLine(name+'Dst0D0GammaLine',
                                                #HLT = config["HLT_FILTER"],
                                                prescale = 1.0,
                                                selection = self.BDstD0Gamma)

        self.lineDstD0GammaSS  =  StrippingLine(name+'Dst0D0GammaSSLine',
                                                #HLT = config["HLT_FILTER"],
                                                prescale = 1.0,
                                                selection = self.BDstD0GammaSS)

        self.lineDstD0Aprime   =  StrippingLine(name+'Dst0D0AprimeLine',
                                                #HLT = config["HLT_FILTER"],
                                                prescale = 1.0,
                                                selection = self.BDstD0Aprime)

        self.lineDstD0AprimeSS =  StrippingLine(name+'Dst0D0AprimeSSLine',
                                                #HLT = config["HLT_FILTER"],
                                                prescale = 1.0,
                                                selection = self.BDstD0AprimeSS)

        self.lineDstD0Pi0      =  StrippingLine(name+'Dst0D0Pi0Line',
                                             #HLT = config["HLT_FILTER"],
                                             prescale = 1.0,
                                             selection = self.BDstD0Pi0)

        self.lineDstD0Pi0Dalitz   =  StrippingLine(name+'Dst0D0Pi0DalitzLine',
                                                   #HLT = config["HLT_FILTER"],
                                                   prescale = 1.0,
                                                   selection = self.BDstD0Pi0Dalitz)

        self.registerLine(self.lineDstD0Gamma)
        self.registerLine(self.lineDstD0GammaSS)
        self.registerLine(self.lineDstD0Aprime)
        self.registerLine(self.lineDstD0AprimeSS)
        self.registerLine(self.lineDstD0Pi0)
        self.registerLine(self.lineDstD0Pi0Dalitz)


def TOSFilter( name = None, sel = None, trigger = None ):
    if len(trigger) == 0:
        return sel
    from Configurables import TisTosParticleTagger
    _filter = TisTosParticleTagger(name+"_TriggerTos")
    _filter.TisTosSpecs = trigger
    _sel = Selection("Sel" + name + "_TriggerTos", RequiredSelections = [ sel ], Algorithm = _filter )
    return _sel
