###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting B0-> pi+ pi- pi+ pi- within the charmless region of the phase space
'''

__author__=['Daniel Vieira']
__date__ = '29/11/2018'
__version__= '$Revision: 1.0 $'


__all__ = (
    'B2hhpipiPhsSpcCutConf',
    'default_config'
    )

default_config = {
    'NAME'                 :  'B2hhpipiPhsSpcCut',
    'BUILDERTYPE'          :  'B2hhpipiPhsSpcCutConf',
    'CONFIG'    : {
        '4PionCuts'         : "(PT > 200*MeV) & (PROBNNpi > 0.03) & (MIPCHI2DV(PRIMARY)> 1.5) & (TRGHOSTPROB < 0.4) & (TRCHI2DOF<3)",
        '2PionCuts'         : "(PT > 200*MeV) & (PROBNNpi > 0.1) & (MIPCHI2DV(PRIMARY)> 2) & (TRGHOSTPROB < 0.4) & (TRCHI2DOF<3)",
        'KaonCuts'          : "(PT > 200*MeV) & (PROBNNk > 0.1) & (MIPCHI2DV(PRIMARY)> 2) & (TRGHOSTPROB < 0.4) & (TRCHI2DOF<3)",
        'B0KKpipiComCuts'   : """
			     ((ADAMASS('B0') < 300 *MeV)
			     | (ADAMASS('B_s0') < 300 *MeV))
			     & (AMAXDOCA('') < 0.2 *mm)
			     & (((AMASS(1,2) < 1864.83 *MeV)
			     & (AMASS(3,4) < 1864.83 *MeV))
			     | ((AMASS(1,4) < 1864.83 *MeV)
			     & (AMASS(2,3) < 1864.83 *MeV))
			     | (AMASS(1,2,3) < 1869.65 *MeV)
			     | (AMASS(1,2,4) < 1869.65 *MeV)
			     | (AMASS(1,3,4) < 1869.65 *MeV)
			     | (AMASS(2,3,4) < 1869.65 *MeV))
                             """,
        'B0ComCuts'        : """
			     (ADAMASS('B0') < 300 *MeV)
			     & (AMAXDOCA('') < 0.2 *mm)
			     & (((AMASS(1,2) < 1864.83 *MeV)
			     & (AMASS(3,4) < 1864.83 *MeV))
			     | ((AMASS(1,4) < 1864.83 *MeV)
		             & (AMASS(2,3) < 1864.83 *MeV))
			     | (AMASS(1,2,3) < 1869.65 *MeV)
			     | (AMASS(1,2,4) < 1869.65 *MeV)
			     | (AMASS(1,3,4) < 1869.65 *MeV)
			     | (AMASS(2,3,4) < 1869.65 *MeV))
                             """,
        'B0MomCuts'        : """
		             (VFASPF(VCHI2) < 30.)
			     & (BPVDIRA> 0.999)
			     & (BPVVDCHI2 > 10.)
			     & (BPVIPCHI2()<30)
			     & (PT > 2.*GeV )
	                     """,
        'B24piMVACut'      : "-.03",
        'B2KKpipiMVACut'   : ".05",
        'B2hhpipiXmlFile'  : "$TMVAWEIGHTSROOT/data/B2hhpipi_v1r0.xml",
        'Prescale'         : 1.
        },
    'STREAMS'              : { 'Bhadron'              : ['StrippingB2hhpipiPhsSpcCut4piLine'],
                               'BhadronCompleteEvent' : ['StrippingB2hhpipiPhsSpcCutKKpipiLine']
                             },
    'WGs'                  : ['BnoC']
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from Configurables import DaVinci__N4BodyDecays

class B2hhpipiPhsSpcCutConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config ):

        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config


        """
        4 Pion
        """
        self.SelBachelorPion = self.createSubSel( OutputList = self.name + "SelBachelorPion",
                                         InputList =  DataOnDemand(Location = 'Phys/StdAllNoPIDsPions/Particles' ),
                                         Cuts = config['4PionCuts']
                                         )
        """
        2 Pion
        """
        self.SelBachelor2Pion = self.createSubSel( OutputList = self.name + "SelBachelor2Pion",
                                         InputList =  DataOnDemand(Location = 'Phys/StdAllNoPIDsPions/Particles' ),
                                         Cuts = config['2PionCuts']
                                         )

        """
        Kaon
        """
        self.SelBachelorKaon = self.createSubSel( OutputList = self.name + "SelBachelorKaon",
                                         InputList =  DataOnDemand(Location = 'Phys/StdAllNoPIDsKaons/Particles' ),
                                         Cuts = config['KaonCuts']
                                         )

        """
        B0 -> pi+ pi- pi+ pi-
        """
        self.SelB24piPhsSpcCut = self.createCombinationSel( OutputList = self.name + "SelB24piPhsSpcCut",
                                                        DecayDescriptor = "[B0 -> pi+ pi- pi+ pi-]cc",
                                                        DaughterLists = [ self.SelBachelorPion ],
                                                        PreVertexCuts  = config['B0ComCuts'],
                                                        PostVertexCuts = config['B0MomCuts'] )

        """
        B0 -> K+ K- pi+ pi-
        """
        self.SelB2KKpipiPhsSpcCut = self.createCombinationSel( OutputList = self.name + "SelB2KKpipiPhsSpcCut",
                                                        DecayDescriptor = "[B0 -> K+ K- pi+ pi-]cc",
                                                        DaughterLists = [ self.SelBachelorKaon, self.SelBachelor2Pion ],
                                                        PreVertexCuts  = config['B0KKpipiComCuts'],
                                                        PostVertexCuts = config['B0MomCuts'] )

        """
        Apply MVA
        """
        self.B2hhpipiVars = {
            'Pion1_IPCHI2_OWNPV'        : " CHILD(MIPCHI2DV(), 1) ",
            'Pion2_IPCHI2_OWNPV'        : " CHILD(MIPCHI2DV(), 2) ",
            'Pion3_IPCHI2_OWNPV'        : " CHILD(MIPCHI2DV(), 3) ",
            'Pion4_IPCHI2_OWNPV'        : " CHILD(MIPCHI2DV(), 4) ",
            'Pion1_PT'                  : " CHILD(PT, 1) ",
            'Pion2_PT'                  : " CHILD(PT, 2) ",
            'Pion3_PT'                  : " CHILD(PT, 3) ",
            'Pion4_PT'                  : " CHILD(PT, 4) ",

            'sqrt(Pion1_IPCHI2_OWNPV+Pion2_IPCHI2_OWNPV+Pion3_IPCHI2_OWNPV+Pion4_IPCHI2_OWNPV)'                 : "sqrt( CHILD(MIPCHI2DV(), 1)+CHILD(MIPCHI2DV(), 2)+CHILD(MIPCHI2DV(), 3)+CHILD(MIPCHI2DV(), 4))",

            'B0_PT'                     : " PT ",
            'B0_IPCHI2_OWNPV'           : " BPVIPCHI2() ",
            'B0_ENDVERTEX_CHI2'         : " VFASPF(VCHI2) ",
            'B0_FDCHI2_OWNPV'           : " VFASPF(VMINVDCHI2DV(PRIMARY)) ",
            'B0_DIRA_OWNPV'             : " BPVDIRA "
            }


        self.MvaB24pi = self.applyMVA( self.name + "MvaB24pi",
                                             SelB        = self.SelB24piPhsSpcCut,
                                             MVAVars     = self.B2hhpipiVars,
                                             MVACutValue = config['B24piMVACut'],
                                             MVAxmlFile  = config['B2hhpipiXmlFile']
                                             )

        self.MvaB2KKpipi = self.applyMVA( self.name + "MvaB2KKpipi",
                                             SelB        = self.SelB2KKpipiPhsSpcCut,
                                             MVAVars     = self.B2hhpipiVars,
                                             MVACutValue = config['B2KKpipiMVACut'],
                                             MVAxmlFile  = config['B2hhpipiXmlFile']
                                             )

        self.B24piPhsSpcCutLine = StrippingLine( self.name + '4piLine',
                                                prescale  = config['Prescale'],
                                                algos     = [ self.MvaB24pi ],
                                                EnableFlavourTagging = True
                                                )
        self.registerLine( self.B24piPhsSpcCutLine )

        self.B2KKpipiPhsSpcCutLine = StrippingLine( self.name + 'KKpipiLine',
                                                prescale  = config['Prescale'],
                                                algos     = [ self.MvaB2KKpipi ],
                                                EnableFlavourTagging = True
                                                )
        self.registerLine( self.B2KKpipiPhsSpcCutLine )


    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)


    def createN4BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N4BodyDecays ( DecayDescriptor = DecayDescriptor,
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts + "&" + "( ACHI2DOCA(1,2)<20 )",
                                           Combination123Cut = ComAMCuts + "&" + "( ACHI2DOCA(1,3)<20 ) & ( ACHI2DOCA(2,3)<20 )",
                                           CombinationCut = "( ACHI2DOCA(1,4)<20 ) & ( ACHI2DOCA(2,4)<20 ) & ( ACHI2DOCA(3,4)<20 )" + " & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )

        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)

    def applyMVA( self, name,
                  SelB,
                  MVAVars,
                  MVAxmlFile,
                  MVACutValue
                  ):
        from MVADictHelpers import addTMVAclassifierValue
        from Configurables import FilterDesktop as MVAFilterDesktop

        _FilterB = MVAFilterDesktop( name + "Filter",
                                     Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )

        addTMVAclassifierValue( Component = _FilterB,
                                XMLFile   = MVAxmlFile,
                                Variables = MVAVars,
                                ToolName  = name )

        return Selection( name,
                          Algorithm =  _FilterB,
                          RequiredSelections = [ SelB ] )
