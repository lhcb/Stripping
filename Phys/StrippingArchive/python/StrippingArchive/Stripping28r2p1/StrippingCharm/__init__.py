###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

_selections = ('StrippingBs2st2pKpipipi', 'StrippingCharmFromBSemi', 'StrippingCharmedBaryonSL', 'StrippingD2HMuNu', 'StrippingD2KPiPiMuNu', 'StrippingDstarD2KSHHPi0', 'StrippingDstarD2XGamma', 'StrippingLambdac2V0H', 'StrippingLambdacForNeutronPID', 'StrippingNeutralCBaryons', 'StrippingXcpToLambdaKSHp', 'StrippingXcpToXiPipHp', 'StrippingXiccSL')

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
