###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
1)Xi_c0->Lz Ks
2)Xi_c0->Lz K- pi+
3)Xi_c0->Xi- pi+, Xi->Lz pi-, Lz->p pi
4)Xi_c0->Xi- K+, Xi->Lz pi-, Lz->p pi
5)Omega_c0 ->Omega- pi+, Omega-> Lz K-

Update on 2021/02/08
Added following decays:
Xi_c0 -> p+ K-
Omega+c0 -> Lambda KS0
Omega_c0 -> p+ K-
Omega_c0 -> Labda K- pi+
'''


__author__ = ['Xuesong Liu, Xiao-Rui Lyu, Zhenwei Yang', 'Miroslav Saur', 'Ziyi Wang']
__date__ = '2021/03/19'
__version__ = '$Revision: 1.1 $'
__all__ = ('StrippingNeutralCBaryonsConf'
           ,'default_config')


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdLoosePions, StdNoPIDsDownPions, StdLooseKaons, StdLooseDownKaons
from StandardParticles import  StdAllLoosePions, StdAllLooseKaons, StdAllLooseProtons

from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from PhysSelPython.Wrappers import MultiSelectionSequence
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, mm, picosecond


default_name='NeutralCBaryons'
    #### This is the dictionary of all tunable cuts ########
default_config={
      'NAME'        :   'NeutralCBaryons',
      'WGs'         :   ['Charm'],
      'BUILDERTYPE' : 'StrippingNeutralCBaryonsConf',
      'STREAMS'     : ['Charm'],
      'CONFIG'      : {
                     'TRCHI2DOFMax'           : 3.0
                   , 'PionPIDK'               :  10.0
                   , 'KaonPIDK'               :  -5.0
                   , 'ProtonPIDp'             : 5 #to be studied
                   , 'ProtonPIDpK'            : -3
                   , 'tight_ProtonPIDp'       : 7 #to be studied
                   , 'tight_ProtonPIDppi'     :  5.0
                   , 'tight_ProtonPIDpK'      :  0.0
                   , 'tight_KaonPIDpi'        :  5.0
                   , 'tight_KaonPIDK'         :  0.0
                   , 'LambdaLLMinDecayTime'   :  5.0 * picosecond
                   , 'LambdaLLVtxChi2Max'     :   5.0
                   , 'LambdaDDVtxChi2Max'     :   5.0
                   , 'LambdaLLMassWin'        : 5.7 * MeV
                   , 'LambdaDDMassWin'        : 5.7 * MeV
                   , 'LambdaLLMinVZ'          : -100. * mm
                   , 'LambdaLLMaxVZ'          :  400. * mm
                   , 'LambdaDDMinVZ'          :  400. * mm
                   , 'LambdaDDMaxVZ'          : 2275. * mm
                   , 'TrGhostProbMax'         :  0.25
                   , 'ProbNNkMin'             :  0.10
                   , 'ProbNNpMinLL'           :  0.10
                   , 'ProbNNpMinDD'           :  0.05
                   , 'Bachelor_PT_MIN'        : 50 * MeV
                   , 'LambdaDeltaZ_MIN'       :  5.0 * mm
                   , 'LambdaPr_PT_MIN'        : 500. * MeV
                   , 'LambdaPi_PT_MIN'        : 100. * MeV
      } ## end of 'CONFIG' 
}  ## end of default_config

#-------------------------------------------------------------------------------------------------------------
class StrippingNeutralCBaryonsConf(LineBuilder) :
        __configuration_keys__ = default_config['CONFIG'].keys()

        def __init__(self, name, config) :
            LineBuilder.__init__(self, name, config)
            self.name = name
            self.config = config

            # take all long tracks - Long Protons
            self.LongProtonsList = MergedSelection("LongProtonsFor" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdAllLooseProtons/Particles")])

            # take all long tracks - Long Pions
            self.LongPionsList = MergedSelection("LongPionsFor" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdAllLoosePions/Particles")])

            # take all long tracks - Long Kaons
            self.LongKaonsList = MergedSelection("LongKaonsFor" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdAllLooseKaons/Particles")])

            # take all long tracks - Down Pions
            self.DownstreamPionsList = MergedSelection("DownstreamPionsFor" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdNoPIDsDownPions/Particles")])

            # take all long tracks = Down Kaons
            self.DownstreamKaonsList = MergedSelection("DownstreamKaonsFor" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdLooseDownKaons/Particles")])

            # Good tracks - Long Protons
            self.GoodLongProtonsList = self.createSubSel( OutputList = "GoodLongProtonsFor" + self.name,
                                                InputList = self.LongProtonsList,
                                                Cuts = "(TRCHI2DOF < %(TRCHI2DOFMax)s )"\
                                                " & (TRGHOSTPROB <%(TrGhostProbMax)s )" \
                                                " & (PIDp > %(ProtonPIDp)s) "\
                                                " & ((PIDp-PIDK)> %(ProtonPIDpK)s) " % self.config )

            # Good tracks - Tight Protons
            self.GoodTightProtonsList = self.createSubSel( OutputList = "GoodTightProtonsFor" + self.name,
                                                InputList = self.LongProtonsList,
                                                Cuts = "(TRCHI2DOF < %(TRCHI2DOFMax)s )"\
                                                " & (TRGHOSTPROB <%(TrGhostProbMax)s )" \
                                                " & (PIDp > %(tight_ProtonPIDp)s) "\
                                                " & ((PIDp-PIDpi) > %(tight_ProtonPIDppi)s) "\
                                                " & ((PIDp-PIDK)  > %(tight_ProtonPIDpK)s) " % self.config )
            # Good tracks - Long Pions
            self.GoodLongPionsList = self.createSubSel( OutputList = "GoodLongPionsFor" + self.name,
                                                InputList = self.LongPionsList,
                                                Cuts = "(TRCHI2DOF < %(TRCHI2DOFMax)s )"\
                                                " & (TRGHOSTPROB <%(TrGhostProbMax)s )" \
                                                " & (PIDK < %(PionPIDK)s )" % self.config )

            # Good tracks - Long Kaons
            self.GoodLongKaonsList = self.createSubSel( OutputList = "GoodLongKaonsFor" + self.name,
                                                InputList = self.LongKaonsList,
                                                Cuts = "(TRCHI2DOF < %(TRCHI2DOFMax)s )"\
                                                " & (TRGHOSTPROB < %(TrGhostProbMax)s )" \
                                                " & (PROBNNk > %(ProbNNkMin)s )" \
                                                " & (PIDK > %(KaonPIDK)s )" % self.config )

            # Good tracks - Tight Kaons
            self.GoodTightKaonsList = self.createSubSel( OutputList = "GoodTightKaonsFor" + self.name,
                                                InputList = self.LongKaonsList,
                                                Cuts = "(TRCHI2DOF < %(TRCHI2DOFMax)s )"\
                                                " & (TRGHOSTPROB < %(TrGhostProbMax)s )" \
                                                " & (PROBNNk > %(ProbNNkMin)s )" \
                                                " & ((PIDK-PIDpi)>%(tight_KaonPIDpi)s)"\
                                                " & (PIDK > %(tight_KaonPIDK)s )" % self.config )

            # Good tracks - Down Pions
            self.GoodDownstreamPionsList = self.createSubSel( OutputList = "GoodDownstreamPionsFor" + self.name,
                                                InputList = self.DownstreamPionsList,
                                                Cuts = "(TRCHI2DOF < %(TRCHI2DOFMax)s )"\
                                                " & (PIDK < %(PionPIDK)s )" % self.config )

            # Good tracks - Down Kaons
            self.GoodDownstreamKaonsList = self.createSubSel( OutputList = "GoodDownstreamKaonsFor" + self.name,
                                                InputList = self.DownstreamKaonsList,
                                                Cuts = "(TRCHI2DOF < %(TRCHI2DOFMax)s )"\
                                                " & (PROBNNk > %(ProbNNkMin)s )" \
                                                " & (PIDK > %(KaonPIDK)s )" % self.config )

            self.LambdaListLooseDD = MergedSelection("StdLooseDDLambdaFor" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdLooseLambdaDD/Particles")])

            self.LambdaListLooseLL = MergedSelection("StdLooseLLLambdaFor" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdLooseLambdaLL/Particles")])

            self.LambdaListLL =  self.createSubSel(OutputList = "LambdaLLFor" + self.name,
                                                 InputList = self.LambdaListLooseLL ,
                                                 Cuts = "(MAXTREE('p+'==ABSID, PT) > %(LambdaPr_PT_MIN)s ) "\
                                                 "& (MAXTREE('pi-'==ABSID, PT) > %(LambdaPi_PT_MIN)s ) " \
                                                 "& (MAXTREE('p+'==ABSID,PROBNNp) > %(ProbNNpMinLL)s ) "\
                                                 "& (MINTREE('pi-'==ABSID, TRGHOSTPROB) < %(TrGhostProbMax)s )"\
                                                 "& (MINTREE('p+'==ABSID, TRGHOSTPROB) < %(TrGhostProbMax)s )"\
                                                 "& (ADMASS('Lambda0') < %(LambdaLLMassWin)s ) "
                                                 "& (VFASPF(VCHI2/VDOF) < %(LambdaLLVtxChi2Max)s ) "\
                                                 "& (VFASPF(VZ) > %(LambdaLLMinVZ)s ) " \
                                                 "& (VFASPF(VZ) < %(LambdaLLMaxVZ)s ) " \
                                                 "& (BPVLTIME() > %(LambdaLLMinDecayTime)s )" % self.config \
                                                 )

            self.LambdaListDD =  self.createSubSel(OutputList = "LambdaDDFor" + self.name,
                                                 InputList = self.LambdaListLooseDD ,
                                                 Cuts = "(MAXTREE('p+'==ABSID, PT) > %(LambdaPr_PT_MIN)s ) "\
                                                 "& (MAXTREE('pi-'==ABSID, PT) > %(LambdaPi_PT_MIN)s ) " \
                                                 "& (MAXTREE('p+'==ABSID, PROBNNp) > %(ProbNNpMinDD)s ) "\
                                                 "& (ADMASS('Lambda0') < %(LambdaDDMassWin)s ) " \
                                                 "& (VFASPF(VCHI2/VDOF) <  %(LambdaDDVtxChi2Max)s ) "\
                                                 "& (VFASPF(VZ) < %(LambdaDDMaxVZ)s ) " \
                                                 "& (VFASPF(VZ) > %(LambdaDDMinVZ)s )" % self.config \
                                                 )

            self.LambdaList = MergedSelection("LambdaFor" + self.name,
                                              RequiredSelections = [self.LambdaListLL, self.LambdaListDD]
                                              )

            self.KS0ListLooseDD = MergedSelection("StdLooseDDKS0For" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdLooseKsDD/Particles")])

            self.KS0ListLooseLL = MergedSelection("StdLooseLLKS0For" + self.name,
                                                   RequiredSelections =  [DataOnDemand(Location = "Phys/StdLooseKsLL/Particles")])

            self.KS0ListLL =  self.createSubSel(OutputList = "KS0LLFor" + self.name,
                                                 InputList = self.KS0ListLooseLL ,
                                                 Cuts = "(BPVVDZ > 0.0 * mm) " \
                                                 "& (BPVVDZ < 2300.0 * mm) " \
                                                 "& (BPVDIRA > 0.99995 ) " \
                                                 "& (ADMASS('KS0') < 40.0 *MeV) " \
                                                 "& (BPVVDCHI2> 100)" % self.config \
                                                 )

            self.KS0ListDD =  self.createSubSel(OutputList = "KS0DDFor" + self.name,
                                                 InputList = self.KS0ListLooseDD ,
                                                 Cuts = "(BPVVDZ > -1000.0 * mm) " \
                                                 "& (BPVVDZ < 650.0 * mm) " \
                                                 "& (BPVDIRA > 0.99997 ) " \
                                                 "& (ADMASS('KS0') < 20.0 *MeV) " \
                                                 "& (BPVVDCHI2> 100)" % self.config \
                                                 )

            self.KS0List = MergedSelection("KS0For" + self.name,
                                              RequiredSelections = [self.KS0ListLL, self.KS0ListDD])

            self.Xic0List = self.makeXic0()
            self.Omegac0List = self.makeOmegac0()

        #------------------------------------------------------------------------------------------
        #------------------------------------------------------------------------------------------

        def createSubSel( self, OutputList, InputList, Cuts ) :
            '''create a selection using a FilterDesktop'''
            filter = FilterDesktop(Code = Cuts)
            return Selection( OutputList,
                             Algorithm = filter,
                             RequiredSelections = [ InputList ] )

        def createCombinationSel( self, OutputList,
                                 DecayDescriptor,
                                 DaughterLists,
                                 DaughterCuts = {} ,
                                 PreVertexCuts = "ALL",
                                 PostVertexCuts = "ALL" ) :
            '''create a selection using a ParticleCombiner with a single decay descriptor'''
            combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                        DaughtersCuts = DaughterCuts,
                                        MotherCut = PostVertexCuts,
                                        CombinationCut = PreVertexCuts,
                                        ReFitPVs = True)
            return Selection ( OutputList,
                              Algorithm = combiner,
                              RequiredSelections = DaughterLists)

        def makeXic0( self ):
            ''' Stripping Xi_c0->Lambda0 KS0'''
            Xic02LambdaKS0 = self.createCombinationSel(OutputList = "Xic02LambdaKS0"+ self.name,
                                                           DecayDescriptor = "[Xi_c0 -> Lambda0 KS0 ]cc",
                                                           DaughterLists   = [self.LambdaList, self.KS0List],
                                                           PreVertexCuts   = "(ADAMASS('Xi_c0') < 120 *MeV)",
                                                           PostVertexCuts  = "(ADMASS('Xi_c0') < 90 *MeV) & (BPVVDCHI2 > 16.0) & (BPVDIRA >0.999) "
                                                           )
            Xic02LambdaKS0Line = StrippingLine( self.name + "Xic02LambdaKS0Line", algos = [ Xic02LambdaKS0 ], EnableFlavourTagging = True )
            self.registerLine (Xic02LambdaKS0Line)

            ''' Stripping Xi_c0->Lambda0 K- pi+'''
            Xic02LambdaKpi = self.createCombinationSel(OutputList = "Xic02LambdaKpi"+ self.name,
                                                           DecayDescriptor = "[Xi_c0 -> Lambda0 K- pi+ ]cc",
                                                           DaughterLists   = [self.LambdaList, self.GoodLongKaonsList, self.GoodLongPionsList],
                                                           DaughterCuts    = {"K-"      : " (MIPCHI2DV(PRIMARY)>4)",
                                                                              "pi+"     : " (MIPCHI2DV(PRIMARY)>4)"},
                                                           PreVertexCuts   = "(ADAMASS('Xi_c0') < 120 *MeV)",
                                                           PostVertexCuts  = "(ADMASS('Xi_c0') < 90 *MeV) &(VFASPF(VCHI2/VDOF)< 12.) & (BPVVDCHI2 > 16.0) & (BPVDIRA >0.999) "
                                                           )
            Xic02LambdaKpiLine = StrippingLine( self.name + "Xic02LambdaKpiLine", algos = [ Xic02LambdaKpi ], EnableFlavourTagging = True )
            self.registerLine (Xic02LambdaKpiLine)

            ''' Stripping Xi_c- -> Lambda0(DD) pi-(Long)'''
            Ximinus2LambdaPiDDL = self.createCombinationSel(OutputList = "Ximinus2LambdaPiDDL"+ self.name,
                                                         DecayDescriptor = "[Xi- -> Lambda0 pi-]cc",
                                                         DaughterLists   = [self.GoodLongPionsList, self.LambdaListDD],
                                                         DaughterCuts    = {"pi-"      : "(MIPCHI2DV(PRIMARY)>4) "},
                                                         PreVertexCuts   = "(ADAMASS('Xi-') < 80*MeV) & (ADOCACHI2CUT(30, ''))",
                                                         PostVertexCuts  = "(ADMASS('Xi-')<64*MeV) & (VFASPF(VCHI2)<25) & (P> 2000*MeV) &(PT > 250*MeV) & (BPVVDCHI2 > 49.0 ) & (VFASPF(VCHI2/VDOF) < 12.0)"
                                                         )

            ''' Stripping Xi_c- -> Lambda0(LL) pi-(Long)'''
            Ximinus2LambdaPiLLL = self.createCombinationSel(OutputList = "Ximinus2LambdaPiLLL"+ self.name,
                                                         DecayDescriptor = "[Xi- -> Lambda0 pi-]cc",
                                                         DaughterLists   = [self.GoodLongPionsList, self.LambdaListLL],
                                                         DaughterCuts    = {"pi-"      : "(MIPCHI2DV(PRIMARY)>9)"},
                                                         PreVertexCuts   = "(ADAMASS('Xi-') < 50*MeV) & (ADOCACHI2CUT(30, ''))",
                                                         PostVertexCuts  = "(ADMASS('Xi-')<35*MeV) & (VFASPF(VCHI2)<25) & (P> 2000*MeV) &(PT > 250*MeV) & (BPVVDCHI2 > 49.0 ) & (VFASPF(VCHI2/VDOF) < 12.0)"
                                                         )

            ''' Stripping Xi_c- -> Lambda0(DD) pi-(Down)'''
            Ximinus2LambdaPiDDD = self.createCombinationSel(OutputList = "Ximinus2LambdaPiDDD"+ self.name,
                                                         DecayDescriptor = "[Xi- -> Lambda0 pi-]cc",
                                                         DaughterLists   = [self.GoodDownstreamPionsList, self.LambdaListDD],
                                                         DaughterCuts    = {"pi-"      : "(MIPCHI2DV(PRIMARY)>4) "},
                                                         PreVertexCuts   = "(ADAMASS('Xi-') < 80*MeV) & (ADOCACHI2CUT(30, ''))",
                                                         PostVertexCuts  = "(ADMASS('Xi-')<64*MeV) & (VFASPF(VCHI2)<25) & (P> 2000*MeV) &(PT > 250*MeV) & (BPVVDCHI2 > 49.0 ) & (VFASPF(VCHI2/VDOF) < 12.0)"
                                                         )

            ## Ximinus2LambdaPi is a "Selection" object; MergedSelection passes everything which gets to it
            ## even when the output list is empty
            Ximinus2LambdaPi = MergedSelection("Ximinus2LambdaPi"+self.name,
                                               RequiredSelections = [Ximinus2LambdaPiLLL,Ximinus2LambdaPiDDL,Ximinus2LambdaPiDDD] )

            ## NullFilter is a "FilterDesktop" object which is a type of "Algorithm"
            ## This one will pass all candidates
            NullFilter= FilterDesktop(Code = "ALL")
            ## Ximinus2Lambda2PiSelection is *also* a Selection, but it is "more selective" 
            ## than  Ximinus2LambdaPi in the sense that it passes only events when something
            ## is in the output list
            Ximinus2LambdaPiSelection = Selection( "Ximinus2LambdaPiSelection"+self.name,
                                                  Algorithm = NullFilter,
                                                  RequiredSelections = [Ximinus2LambdaPi])

            ''' Stripping Xi_c0 -> p+ K-'''
            Xic02pK = self.createCombinationSel(OutputList = "Xic02pK"+ self.name,
                                                         DecayDescriptor = "[Xi_c0 -> p+ K-]cc",
                                                         DaughterLists   = [self.GoodTightProtonsList, self.GoodTightKaonsList],
                                                         DaughterCuts    = {"K-"      : " (MIPCHI2DV(PRIMARY)>4)",
                                                                            "p+"     : " (MIPCHI2DV(PRIMARY)>4)"},
                                                         PreVertexCuts   = "(ADAMASS('Xi_c0') < 120 *MeV)",
                                                         PostVertexCuts  = "(ADMASS('Xi_c0') < 90 *MeV) &(VFASPF(VCHI2/VDOF)< 12.) & (BPVVDCHI2 > 16.0) & (BPVDIRA >0.999) "
                                                         )
            #Xic02pKLine = StrippingLine( self.name + "Xic02pKLine", prescale = 0.5, algos = [ Xic02pK ], EnableFlavourTagging = True )
            Xic02pKLine = StrippingLine( self.name + "Xic02pKLine", algos = [ Xic02pK ], EnableFlavourTagging = True )
            self.registerLine (Xic02pKLine)

            ''' Stripping Xi_c0->Xi- pi+'''
            Xic02Xipi = self.createCombinationSel(OutputList = "Xic02Xipi"+ self.name,
                                                         DecayDescriptor = "[Xi_c0 -> Xi- pi+]cc",
                                                         DaughterLists   = [self.GoodLongPionsList, Ximinus2LambdaPiSelection],
                                                         DaughterCuts    = {"pi+"      : "(PT> %(Bachelor_PT_MIN)s ) "% self.config},
                                                         PreVertexCuts   = "(ADAMASS('Xi_c0') < 120 *MeV)",
                                                         PostVertexCuts  = "(ADMASS('Xi_c0') < 90 *MeV) &(VFASPF(VCHI2/VDOF)< 12.) & (BPVVDCHI2 > 16.0) & (BPVDIRA >0.999) "
                                                         )

            Xic02XipiLine = StrippingLine( self.name + "Xic02XipiLine", algos = [ Xic02Xipi ], EnableFlavourTagging = True )
            self.registerLine (Xic02XipiLine)

            ''' Stripping Xi_c0->Xi- K+'''
            Xic02XiK = self.createCombinationSel(OutputList = "Xic02XiK"+ self.name,
                                                         DecayDescriptor = "[Xi_c0 -> Xi- K+]cc",
                                                         DaughterLists   = [self.GoodLongKaonsList, Ximinus2LambdaPiSelection],
                                                         DaughterCuts    = {"K+"      : "(PT> %(Bachelor_PT_MIN)s ) "% self.config},
                                                         PreVertexCuts   = "(ADAMASS('Xi_c0') < 120 *MeV)",
                                                         PostVertexCuts  = "(ADMASS('Xi_c0') < 90 *MeV) &(VFASPF(VCHI2/VDOF)< 12.) & (BPVVDCHI2 > 16.0) & (BPVDIRA >0.999) "
                                                         )

            Xic02XiKLine = StrippingLine( self.name + "Xic02XiKLine", algos = [ Xic02XiK ], EnableFlavourTagging = True )
            self.registerLine (Xic02XiKLine)

        ##  --------------------  end of makeXiminus  ------------
        #------------------------------------------------------------------------------------------

        def makeOmegac0( self ):

            ''' Make an Omega minus candidate '''
            Omegaminus2LambdaKLLL = self.createCombinationSel(OutputList = "Omegaminus2LambdaKLLL"+ self.name,
                                                           DecayDescriptor = "[Omega- -> Lambda0 K-]cc",
                                                           DaughterLists   = [self.GoodLongKaonsList, self.LambdaListLL],
                                                           DaughterCuts    = {"K-"      : "(MIPCHI2DV(PRIMARY)>9) "},
                                                           PreVertexCuts   = "(ADAMASS('Omega-') < 50*MeV) & (ADOCACHI2CUT(30, ''))",
                                                           PostVertexCuts  = "(ADMASS('Omega-')<35*MeV) & (VFASPF(VCHI2)<25) & (P> 2000*MeV) &(PT > 250*MeV) & (BPVVDCHI2 > 49.0 ) & (VFASPF(VCHI2/VDOF) < 12.0)"
                                                           )

            ''' Make an Omega minus candidate '''
            Omegaminus2LambdaKDDL = self.createCombinationSel(OutputList = "Omegaminus2LambdaKDDL"+ self.name,
                                                           DecayDescriptor = "[Omega- -> Lambda0 K-]cc",
                                                           DaughterLists   = [self.GoodLongKaonsList, self.LambdaListDD],
                                                           DaughterCuts    = {"K-"      : "(MIPCHI2DV(PRIMARY)>4) "},
                                                           PreVertexCuts   = "(ADAMASS('Omega-') < 80*MeV) & (ADOCACHI2CUT(30, ''))",
                                                           PostVertexCuts  = "(ADMASS('Omega-')<64*MeV) & (VFASPF(VCHI2)<25) & (P> 2000*MeV) &(PT > 250*MeV) & (BPVVDCHI2 > 49.0 ) & (VFASPF(VCHI2/VDOF) < 12.0)"
                                                           )

            ''' Make an Omega minus candidate '''
            Omegaminus2LambdaKDDD = self.createCombinationSel(OutputList = "Omegaminus2LambdaKDDD"+ self.name,
                                                         DecayDescriptor = "[Omega- -> Lambda0 K-]cc",
                                                         DaughterLists   = [self.GoodDownstreamKaonsList, self.LambdaListDD],
                                                         DaughterCuts    = {"K-"      : "(MIPCHI2DV(PRIMARY)>4) "},
                                                         PreVertexCuts   = "(ADAMASS('Omega-') < 80*MeV) & (ADOCACHI2CUT(30, ''))",
                                                         PostVertexCuts  = "(ADMASS('Omega-')<64*MeV) & (VFASPF(VCHI2)<25) & (P> 2000*MeV) &(PT > 250*MeV) & (BPVVDCHI2 > 49.0 ) & (VFASPF(VCHI2/VDOF) < 12.0)"
                                                         )


            ## Omegaminus2LambdaK is a "Selection" object; MergedSelection passes everything which gets to it
            ## even when the output list is empty
            Omegaminus2LambdaK = MergedSelection("Omegaminus2LambdaK"+self.name,
                                                 RequiredSelections = [Omegaminus2LambdaKLLL,Omegaminus2LambdaKDDL,Omegaminus2LambdaKDDD] )

            ## NullFilter is a "FilterDesktop" object which is a type of "Algorithm"
            ## This one will pass all candidates
            NullFilter= FilterDesktop(Code = "ALL")
            ## Omegaminus2Lambda2PiSelection is *also* a Selection, but it is "more selective" 
            ## than  Omegaminus2LambdaK in the sense that it passes only events when something
            ## is in the output list
            Omegaminus2LambdaKSelection = Selection( "Omegaminus2LambdaKSelection"+self.name,
                                                    Algorithm = NullFilter,
                                                    RequiredSelections = [Omegaminus2LambdaK])

            ''' Stripping Omega_c0->Omega- pi+'''
            Omegac02Omegapi = self.createCombinationSel(OutputList = "Omegac02Omegapi"+ self.name,
                                                           DecayDescriptor = "[Omega_c0 -> Omega- pi+]cc",
                                                           DaughterLists   = [self.GoodLongPionsList, Omegaminus2LambdaKSelection],
                                                           DaughterCuts    = {"pi+"      : "(PT> %(Bachelor_PT_MIN)s ) "% self.config},
                                                           PreVertexCuts   = "(ADAMASS('Omega_c0') < 120 *MeV)",
                                                           PostVertexCuts  = "(VFASPF(VCHI2/VDOF)< 12.) & (BPVVDCHI2 > 16.0) & (BPVDIRA >0.999) & (ADMASS('Omega_c0') < 90 *MeV)"
                                                           )

            Omegac02OmegapiLine = StrippingLine( self.name + "Omegac02OmegapiLine", algos = [ Omegac02Omegapi ], EnableFlavourTagging = True )
            self.registerLine (Omegac02OmegapiLine)

            ''' Stripping Omega_c0-> p+- K-'''
            Omegac02pK = self.createCombinationSel(OutputList = "Omegac02pK"+ self.name,
                                                           DecayDescriptor = "[Omega_c0 -> p+ K-]cc",
                                                           DaughterLists   = [self.GoodTightProtonsList, self.GoodTightKaonsList],
                                                           DaughterCuts    = {"K-"      : " (MIPCHI2DV(PRIMARY)>4)",
                                                                              "p+"     : " (MIPCHI2DV(PRIMARY)>4)"},
                                                           PreVertexCuts   = "(ADAMASS('Omega_c0') < 120 *MeV)",
                                                           PostVertexCuts  = "(ADMASS('Omega_c0')<35*MeV) & (VFASPF(VCHI2)<25) & (P> 2000*MeV) &(PT > 250*MeV) & (BPVVDCHI2 > 49.0 ) & (VFASPF(VCHI2/VDOF) < 12.0)"
                                                           )

            Omegac02pKLine = StrippingLine( self.name + "Omegac02pKLine", algos = [ Omegac02pK ], EnableFlavourTagging = True )
            self.registerLine (Omegac02pKLine)


            ## Code for Omega_c0 -> Lambda0 KS0; four relevant track classes: LL, LD, DL, DD - those are based on LambdaList and KS0List
            Omegac02LambdaKS0 = self.createCombinationSel(OutputList = "Omegac02LambdaKS0"+ self.name,
                                                         DecayDescriptor = "[Omega_c0 -> Lambda0 KS0 ]cc",
                                                         DaughterLists   = [self.LambdaList, self.KS0List],
                                                         PreVertexCuts   = "(ADAMASS('Omega_c0') < 120 *MeV)",
                                                         PostVertexCuts  = "(ADMASS('Omega_c0') < 90 *MeV) & (BPVVDCHI2 > 16.0) & (BPVDIRA >0.999) "
                                                         )
            Omegac02LambdaKS0Line = StrippingLine( self.name + "Omegac02LambdaKS0Line", algos = [ Omegac02LambdaKS0 ], EnableFlavourTagging = True )
            self.registerLine (Omegac02LambdaKS0Line)


            ## Code for Omega_c0 -> Lambda0 K- pi+, two relevant track classed: LL, DD
            Omegac02LambdaKpi = self.createCombinationSel(OutputList = "Omegac02LambdaKpi"+ self.name,
                                                         DecayDescriptor = "[Omega_c0 -> Lambda0 K- pi+ ]cc",
                                                         DaughterLists   = [self.LambdaList, self.GoodLongKaonsList, self.GoodLongPionsList],
                                                         DaughterCuts    = {"K-"      : " (MIPCHI2DV(PRIMARY)>4)",
                                                                            "pi+"     : " (MIPCHI2DV(PRIMARY)>4)"},
                                                         PreVertexCuts   = "(ADAMASS('Omega_c0') < 120 *MeV)",
                                                         PostVertexCuts  = "(ADMASS('Omega_c0') < 90 *MeV) &(VFASPF(VCHI2/VDOF)< 12.) & (BPVVDCHI2 > 16.0) & (BPVDIRA >0.999) "
                                                         )
            Omegac02LambdaKpiLine = StrippingLine( self.name + "Omegac02LambdaKpiLine", algos = [ Omegac02LambdaKpi ], EnableFlavourTagging = True )
            self.registerLine (Omegac02LambdaKpiLine)
        ## --------------------  end of makeOmegaminus  ------------
        #------------------------------------------------------------------------------------------
