###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of Lb->V0pphh stripping Selections and StrippingLines.
Provides functions to build Lambda0->DD, Lambda0->LL, and Lambda0->LD selections.
Stripping20 with an inclusive approach for Lb->Lambda pp hh modes.
Provides class Lb2V0pphhConf, which constructs the Selections and StrippingLines
given a configuration dictionary.
Selections based on previous version of the V04h line by Xiaokang Zhou, etc.
Exported symbols (use python help!):
   - Lb2V0pphhConf
"""

__author__ = ["Hendrik Jage", "Gediminas Sarpis"]
__date__ = '19/03/2021'
__version__ = 'v1r0'
__all__ = {'Lb2V0pphhConf', 'default_config'}

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

from StandardParticles import StdNoPIDsPions as Pions
from StandardParticles import StdNoPIDsProtons as Protons

default_config = {
    'NAME': 'Lb2V0pphh',
    'WGs': ['IFT'],
    'BUILDERTYPE': 'Lb2V0pphhConf',
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'Trk_Chi2'               : 3.0,
        'Trk_GhostProb'          : 0.4,
        'Trk_IPChi2'             :   9,
   
        'ProtonP'                : 15000,
        'ProtonPT'               : 500,
        'ProtonProbNNp'          : 0.05,
        'ProtonPIDppi'           : 5,
        'ProtonPIDpK'            : 0,
        'PionP'                  : 1500,
        'PionPT'                 :  500,
 
        'Lambda_LL_MassWindow'   : 20.0,
        'Lambda_LL_VtxChi2'      : 15.0,
        'Lambda_LL_FDChi2'       : 80.0,

        'Lb_Mlow'                : 1319.0,
        'Lb_Mhigh'               : 600.0,
        'Lb_APTmin'              : 250.0,
        'Lb_PTmin'               : 250.0,

        'LbDaug_MedPT_PT'        : 800.0,
        'LbDaug_MaxPT_IP'        : 0.05,
        'LbDaug_LL_maxDocaChi2'  : 10.0,
        'LbDaug_LL_PTsum'        : 1500.0,

        'Lbh_LL_PTMin'           : 500.0,
        'Lb_VtxChi2Ndof'         : 12.0,
        'Lb_LL_Dira'             : 0.995,
        'Lb_LL_IPCHI2wrtPV'      : 20.0,
        'Lb_FDwrtPV'             : 1.0,
        'Lb_LL_FDChi2'           : 40.0,

        'GEC_MaxTracks'          : 250,
        # 2015 Triggers
        'HLT1Dec'                : 'Hlt1(Two)?TrackMVADecision',
        'HLT2Dec'                : 'Hlt2Topo[234]BodyDecision',
        'Prescale'               : 1.0,
        'Postscale'              : 1.0,
    },
}


class Lb2V0pphhConf(LineBuilder):
    """
    Builder of Lb->V0pphh stripping Selection and StrippingLine.
    Constructs Lb -> V0 p+ p- h+ h- Selections and StrippingLines from a configuration dictionary.
    Usage:
    >>> config = { .... }
    >>> lb2v0pphhConf = LB2V0pphhConf('Lb2V0pphhTest',config)
    >>> lb2v0pphhLines = lb2v0pphhConf.lines
    >>> for line in line :
    >>>  print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selLambda2DD           : Lambda0 -> Down Down Selection object
    selLambda2LL           : Lambda0 -> Long Long Selection object

    selLb2V0DDpphh           : Lb -> Lambda0(DD) p+ p- h+ h- Selection object
    selLb2V0LLpphh           : Lb -> Lambda0(LL) p+ p- h+ h- Selection object

    Lb_dd_line             : StrippingLine made out of selLb2V0DDpphh
    Lb_ll_line             : StrippingLine made out of selLb2V0LLpphh

    lines                  : List of lines, [Lb_dd_line, Lb_ll_line, Lb_ld_line]

    Exports as class data member:
    Lb2V0pphhConf.__configuration_keys__ : List of required configuration parameters.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        GECCode = {
            'Code':
            "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %s)" %
            config['GEC_MaxTracks'],
            'Preambulo': ["from LoKiTracks.decorators import *"]
        }

        self.hlt1Filter = {
            'Code': "HLT_PASS_RE('%s')" % config['HLT1Dec'],
            'Preambulo': ["from LoKiCore.functions import *"]
        }
        self.hlt2Filter = {
            'Code': "HLT_PASS_RE('%s')" % config['HLT2Dec'],
            'Preambulo': ["from LoKiCore.functions import *"]
        }

        # Tracks
        _trackCuts          =    "(TRCHI2DOF          < %s)"         % config['Trk_Chi2']
        _trackCuts         += " & (TRGHOSTPROB        < %s)"         % config['Trk_GhostProb']
        _trackCuts         += " & (MIPCHI2DV(PRIMARY) > %s)"         % config['Trk_IPChi2']

        # Proton
        #_protonPidCuts      =    "(PROBNNp            > %s)"         % config['ProtonProbNNp']

        _protonKinCuts      =    "(PT                 > %s *MeV)"    % config['ProtonPT']
        _protonKinCuts     += " & (P                  > %s *MeV)"    % config['ProtonP']
        _protonCuts         = _trackCuts + " & " + _protonKinCuts
        #_protonCuts         = _trackCuts + " & " + _protonPidCuts + " & " + _protonKinCuts

        # Pion
        _pionKinCuts        =    "(PT                 > %s *MeV)"    % config['PionPT']
        _pionKinCuts       += " & (P                  > %s *MeV)"    % config['PionP']
        _pionCuts           = _trackCuts + " & " + _pionKinCuts

        self.ProtonForLb2V0pphh = SimpleSelection( "ProtonFor" + name, FilterDesktop,
          [ Protons ],
          Code  = _protonCuts,
        )

        self.PionForLb2V0pphh = SimpleSelection( "PionFor" + name, FilterDesktop,
          [ Pions ],
          Code  = _pionCuts,
        )

        self.makeLambda2LL('Lambda0LLLb2V0pphhLines', config)

        namesSelections = [
            (name + 'LL', self.makeLb2V0LLpphh(name + 'LL', config)),
            (name + 'LLSS', self.makeLb2V0LLpphh(name + 'LLSS', config)),
        ]

        # make lines

        for selName, sel in namesSelections:

            extra = {}

            if 'SS' in selName:
                extra['HLT1'] = self.hlt1Filter
                extra['HLT2'] = self.hlt2Filter

            line = StrippingLine(
                selName + 'Line',
                selection=sel,
                prescale=config['Prescale'],
                postscale=config['Postscale'],
                RequiredRawEvents = ["Rich","Velo","Tracker","Calo"],
                FILTER=GECCode,
                **extra)

            self.registerLine(line)

    def makeLambda2LL(self, name, config):
        # define all the cuts
        _massCut              = "(ADMASS('Lambda0')<%s*MeV)"         % config['Lambda_LL_MassWindow']
        _vtxCut               = "(VFASPF(VCHI2)<%s)"                 % config['Lambda_LL_VtxChi2']
        _trkChi2Cut1          = "(CHILDCUT((TRCHI2DOF<%s),1))"       % config['Trk_Chi2']
        _trkChi2Cut2          = "(CHILDCUT((TRCHI2DOF<%s),2))"       % config['Trk_Chi2']
        _trkGhostProbCut1     = "(CHILDCUT((TRGHOSTPROB<%s),1))"     % config['Trk_GhostProb']
        _trkGhostProbCut2     = "(CHILDCUT((TRGHOSTPROB<%s),2))"     % config['Trk_GhostProb']

        _allCuts = _massCut
        _allCuts += '&' + _trkChi2Cut1
        _allCuts += '&' + _trkChi2Cut2
        _allCuts += '&' + _vtxCut
        _allCuts += '&' + _trkGhostProbCut1
        _allCuts += '&' + _trkGhostProbCut2

        # get the Lambda's to filter
        _stdLambdaLL = DataOnDemand(
            Location="Phys/StdVeryLooseLambdaLL/Particles")

        # make the filter
        _filterLambdaLL = FilterDesktop(Code=_allCuts)

        # make and store the Selection object
        self.selLambda2LL = Selection(
            name, Algorithm=_filterLambdaLL, RequiredSelections=[_stdLambdaLL])

        return self.selLambda2LL

    def makeLb2V0LLpphh(self, name, config):
        """
        Create and store a Lb -> Lambda0(LL) p+ h- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """
        _protonPidCuts     =    "((PIDp-PIDpi)       > %s)"         % config['ProtonPIDppi']
        _protonPidCuts    += " & ((PIDp-PIDK)        > %s)"         % config['ProtonPIDpK']
        #_protonPidCuts      =    "(PROBNNp            > %s)"         % config['ProtonProbNNp']

        _massCutLow     = "(AM>(5619-%s)*MeV)"                   % config['Lb_Mlow']
        _massCutHigh    = "(AM<(5619+%s)*MeV)"                   % config['Lb_Mhigh']
        _aptCut         = "(APT>%s*MeV)"                         % config['Lb_APTmin']
        _daugMedPtCut   = "(ANUM(PT>%s*MeV)>=2)"                 % config['LbDaug_MedPT_PT']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))"                % config['LbDaug_LL_maxDocaChi2']

        _combCuts = _aptCut
        _combCuts += '&' + _daugMedPtCut
        _combCuts += '&' + _massCutLow
        _combCuts += '&' + _massCutHigh
        _combCuts += '&' + _maxDocaChi2Cut

        _ptCut             = "(PT>%s*MeV)"                        % config['Lb_PTmin']
        _vtxChi2Cut        = "(VFASPF(VCHI2/VDOF)<%s)"            % config['Lb_VtxChi2Ndof']
        _diraCut           = "(BPVDIRA>%s)"                       % config['Lb_LL_Dira']
        _ipChi2Cut         = "(BPVIPCHI2()<%s)"                   % config['Lb_LL_IPCHI2wrtPV']
        _fdCut             = "(VFASPF(VMINVDDV(PRIMARY))>%s)"     % config['Lb_FDwrtPV']
        _fdChi2Cut         = "(BPVVDCHI2>%s)"                     % config['Lb_LL_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&' + _vtxChi2Cut
        _motherCuts += '&' + _diraCut
        _motherCuts += '&' + _ipChi2Cut
        _motherCuts += '&' + _fdChi2Cut
        _motherCuts += '&' + _fdCut
        _Lb = CombineParticles()

        if 'SS' in name:  # Same sign

            _Lb.DecayDescriptors = [
                "Lambda_b0  -> p+  p+  pi+ pi+ Lambda0",
                "Lambda_b0  -> p~- p~- pi- pi- Lambda0",
                #"Lambda_b0  -> p+  p+  pi+ pi- Lambda0",
                #"Lambda_b0  -> p~- p~- pi- pi+ Lambda0",
                #"Lambda_b0  -> p+  p+  pi- pi- Lambda0",
                #"Lambda_b0  -> p~- p~- pi+ pi+ Lambda0",
                #"Lambda_b0  -> p+  p~- pi+ pi+ Lambda0",
                #"Lambda_b0  -> p~- p+  pi- pi- Lambda0",
                "Lambda_b~0 -> p+  p+  pi+ pi+ Lambda~0",
                "Lambda_b~0 -> p~- p~- pi- pi- Lambda~0",
                #"Lambda_b~0 -> p+  p+  pi+ pi- Lambda~0",
                #"Lambda_b~0 -> p~- p~- pi- pi+ Lambda~0",
                #"Lambda_b~0 -> p+  p+  pi- pi- Lambda~0",
                #"Lambda_b~0 -> p~- p~- pi+ pi+ Lambda~0",
                #"Lambda_b~0 -> p+  p~- pi+ pi+ Lambda~0",
                #"Lambda_b~0 -> p~- p+  pi- pi- Lambda~0",
            ]

        else:

            _Lb.DecayDescriptors = [
                "Lambda_b0  -> p+  p~- pi+ pi- Lambda0",
                "Lambda_b~0 -> p~- p+  pi- pi+ Lambda~0"
            ]

        _Lb.DaughtersCuts = {"pi+" : "ALL",
                             "pi-" : "ALL",
                             "p+"  : "ALL",
                             "p~-" : _protonPidCuts}

        _Lb.CombinationCut = _combCuts
        _Lb.MotherCut = _motherCuts
        _Lb.ReFitPVs = True

        _LbConf = _Lb.configurable(name + '_combined')

        if 'SS' in name:  # Same sign
            self.selLb2V0LLpphhSS = Selection(
                name,
                Algorithm=_LbConf,
                RequiredSelections=[self.selLambda2LL, self.ProtonForLb2V0pphh, self.PionForLb2V0pphh])
            return self.selLb2V0LLpphhSS
        else:
            self.selLb2V0LLpphh = Selection(
                name,
                Algorithm=_LbConf,
                RequiredSelections=[self.selLambda2LL, self.ProtonForLb2V0pphh, self.PionForLb2V0pphh])
            return self.selLb2V0LLpphh

