###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Set of lines for B -> D0[Kpi] eta/etap
with eta -> pipipi0[gg] / pipig
and etap -> pipieta[gg] / pipig
'''

__author__ = ['Max Chefdeville']
__date__ = '29/11/2021'

__all__ = ('B2D0X0Conf','default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from CommonParticles.Utils import updateDoD
from StandardParticles import StdLoosePions
from StandardParticles import StdLooseKaons
from StandardParticles import StdLooseProtons
from StandardParticles import StdAllLooseKaons
from StandardParticles import StdAllLooseProtons
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV

default_config = {
    'NAME'              : 'B2D0X0',
    'WGs'               : ['B2OC'],
    'BUILDERTYPE'       : 'B2D0X0Conf',
    'CONFIG'            : {

    'DIRACut'                : 0.9995,
    'IPCut'                  : 0.2,
    'IPCHI2Cut'              : 20,
    'VCHI2PDOFCut'           : 10,
    'BMASSLOW'               : 4800,
    'BMASSHIGH'              : 6200,

    'Prescale_B02D0Eta2PiPiGamma'   : 1,
    'Prescale_B02D0Eta2PiPiPi0'     : 1,
    'Prescale_B02D0Etap2RhoGamma'   : 1,
    'Prescale_B02D0Etap2EtaPiPi'    : 1,
    },
    'STREAMS'           :{
    'BhadronCompleteEvent':[
    'StrippingB02D0Eta2PiPiGamma_Beauty2CharmLine',
    'StrippingB02D0Eta2PiPiPi0_Beauty2CharmLine',
    'StrippingB02D0Etap2RhoGamma_Beauty2CharmLine',
    'StrippingB02D0Etap2EtaPiPi_Beauty2CharmLine']},
    }

### Lines stored in this file:

class B2D0X0Conf(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        #get standard containers
        self.LooseD0List = DataOnDemand(Location = "Phys/StdLooseD02KPi/Particles")
        self.GammaList   = DataOnDemand(Location = "Phys/StdLooseAllPhotons/Particles")
        self.Pi0List     = DataOnDemand(Location = "Phys/StdLoosePi02gg/Particles")
        self.EtaList     = DataOnDemand(Location = "Phys/StdLooseEta2gg/Particles")
        self.PionList    = DataOnDemand(Location = "Phys/StdLoosePions/Particles")

        #clean neutrals
        _tightpi0_cut   = "(CHILD(CL,1)>0.05) & (CHILD(CL,2)>0.05) & (PT>1*GeV)"
        _tighteta_cut   = "(CHILD(CL,1)>0.05) & (CHILD(CL,2)>0.05) & (PT>1*GeV)"
        _tightgamma_cut = "(CL>0.05) & (PT>500*MeV)"

        #pipi vertex cuts
        _pipix_cut      = "(BPVVDZ>0) & (VFASPF(VCHI2)<9) & (BPVDIRA>0.95) & (BPVVDCHI2>25)"

        #pipix0 cuts
        _eta_cut        = "(ADAMASS('eta')<100*MeV)       & (APT>1500*MeV)"
        _etap_cut       = "(ADAMASS('eta_prime')<100*MeV) & (APT>1500*MeV)"

        #d0pipix cuts
        _bpre_cut       = "in_range(%(BMASSLOW)s-50,AM,%(BMASSHIGH)s+50)" % self.config
        _bpost_cut      = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s ) & (MM>%(BMASSLOW)s *MeV) & (MM>%(BMASSHIGH)s *MeV)" % self.config

        #tight d0[kpi]
        _tightd0_cut    = "(INTREE(('K+'==ABSID) & (PROBNNk > 0.1) & (PT>100 *MeV) & (MIPCHI2DV(PRIMARY)> 4) & (TRGHP < 0.4)))"
        _tightd0_cut   += "& (INTREE(('pi+'==ABSID) & (PROBNNpi > 0.1) & (PT>100 *MeV) & (MIPCHI2DV(PRIMARY)> 4) & (TRGHP < 0.4)))"
        _tightd0_cut   += "& (ADMASS('D0') < 65 *MeV)& (PT > 1500.0 *MeV)& (MIPCHI2DV(PRIMARY) > 4) & (VFASPF(VCHI2/VDOF) < 10)"

        #filter standard containers
        self.TightD0List    = self.createSubSel( OutputList = self.name + '_TightD0',    InputList  = self.LooseD0List, Cuts = _tightd0_cut)
        self.TightPi0List   = self.createSubSel( OutputList = self.name + '_TightPi0',   InputList  = self.Pi0List,     Cuts = _tightpi0_cut)
        self.TightEtaList   = self.createSubSel( OutputList = self.name + '_TightEta',   InputList  = self.EtaList,     Cuts = _tighteta_cut)
        self.TightGammaList = self.createSubSel( OutputList = self.name + '_TightGamma', InputList  = self.GammaList,   Cuts = _tightgamma_cut)

        #-------------

        self.Eta2PiPiPi0 = self.createCombinationSel( OutputList      = self.name + "_Eta2PiPiPi0",
                                                      DecayDescriptor = "eta -> pi+ pi- pi0",
                                                      DaughterLists   = [ self.PionList, self.TightPi0List],
                                                      PreVertexCuts   = _eta_cut,
                                                      PostVertexCuts  = _pipix_cut)

        B2D0Eta2PiPiPi0 = self.createCombinationSel( OutputList      = "B02D0Eta2PiPiPi0_" + self.name,
                                                     DecayDescriptor = "[B0 -> D0 eta]cc",
                                                     DaughterLists   = [ self.TightD0List, self.Eta2PiPiPi0],
                                                     PreVertexCuts   = _bpre_cut,
                                                     PostVertexCuts  = _bpost_cut)

        B2D0Eta2PiPiPi0Line  = StrippingLine( "B02D0Eta2PiPiPi0_Beauty2CharmLine",
                                              prescale  = config['Prescale_B02D0Eta2PiPiPi0'],
                                              algos = [ B2D0Eta2PiPiPi0 ])

        self.registerLine(B2D0Eta2PiPiPi0Line)

        #-------------

        self.Eta2PiPiGamma = self.createCombinationSel( OutputList      = self.name + "_Eta2PiPiGamma",
                                                        DecayDescriptor = "eta -> pi+ pi- gamma",
                                                        DaughterLists   = [ self.PionList, self.TightGammaList],
                                                        PreVertexCuts   = _eta_cut,
                                                        PostVertexCuts  = _pipix_cut )

        B2D0Eta2PiPiGamma = self.createCombinationSel( OutputList      = "B02D0Eta2PiPiGamma_" + self.name,
                                                       DecayDescriptor = "[B0 -> D0 eta]cc",
                                                       DaughterLists   = [ self.TightD0List, self.Eta2PiPiGamma],
                                                       PreVertexCuts   = _bpre_cut,
                                                       PostVertexCuts  = _bpost_cut)

        B2D0Eta2PiPiGammaLine  = StrippingLine( "B02D0Eta2PiPiGamma_Beauty2CharmLine",
                                                prescale  = config['Prescale_B02D0Eta2PiPiGamma'],
                                                algos = [ B2D0Eta2PiPiGamma ])

        self.registerLine(B2D0Eta2PiPiGammaLine)

        #-------------

        self.Rho2PiPi = self.createCombinationSel( OutputList      = self.name + "_Rho2PiPi",
                                                   DecayDescriptor = "rho(770)0 -> pi+ pi-",
                                                   DaughterLists   = [ self.PionList ],
                                                   PreVertexCuts   = "(AM>600*MeV) & (AM<900*MeV) & (ADOCACHI2CUT(15, ''))",
                                                   PostVertexCuts  = _pipix_cut)

        self.Etap2RhoGamma = self.createCombinationSel( OutputList      =  self.name + "_Etap2RhoGamma",
                                                        DecayDescriptor = "eta_prime -> rho(770)0 gamma",
                                                        DaughterLists   = [ self.Rho2PiPi, self.TightGammaList],
                                                        PreVertexCuts   = _etap_cut,
                                                        PostVertexCuts  = "ALL" )

        B2D0Etap2RhoGamma = self.createCombinationSel( OutputList      = "B02D0Etap2RhoGamma_" + self.name,
                                                       DecayDescriptor = "[B0 -> D0 eta_prime]cc",
                                                       DaughterLists   = [ self.TightD0List, self.Etap2RhoGamma],
                                                       PreVertexCuts   = _bpre_cut,
                                                       PostVertexCuts  = _bpost_cut)

        B2D0Etap2RhoGammaLine  = StrippingLine( "B02D0Etap2RhoGamma_Beauty2CharmLine",
                                                prescale  = config['Prescale_B02D0Etap2RhoGamma'],
                                                algos = [ B2D0Etap2RhoGamma ])

        self.registerLine(B2D0Etap2RhoGammaLine)

        #-------------

        self.Etap2EtaPiPi = self.createCombinationSel( OutputList      = self.name + "_Etap2EtaPiPi",
                                                       DecayDescriptor = "eta_prime -> pi+ pi- eta",
                                                       DaughterLists   = [ self.PionList, self.TightEtaList],
                                                       PreVertexCuts   = _etap_cut,
                                                       PostVertexCuts  = _pipix_cut )

        B2D0Etap2EtaPiPi = self.createCombinationSel( OutputList      = "B02D0Etap2EtaPiPi_" + self.name,
                                                      DecayDescriptor = "[B0 -> D0 eta_prime]cc",
                                                      DaughterLists   = [ self.TightD0List, self.Etap2EtaPiPi],
                                                      PreVertexCuts   = _bpre_cut,
                                                      PostVertexCuts  = _bpost_cut)

        B2D0Etap2EtaPiPiLine  = StrippingLine( "B02D0Etap2EtaPiPi_Beauty2CharmLine",
                                               prescale  = config['Prescale_B02D0Etap2EtaPiPi'],
                                               algos = [ B2D0Etap2EtaPiPi ])

        self.registerLine(B2D0Etap2EtaPiPiLine)

        #-------------

    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = ReFitPVs)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
