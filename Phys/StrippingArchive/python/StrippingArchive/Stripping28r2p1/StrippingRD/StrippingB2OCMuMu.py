###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__  = 'Vitalii Lisovskyi'
__date__    = '05/03/2021'
__version__ = '$Revision: 0 $'

__all__ = ( 'B2OCMuMuConf', 'default_config' )

"""
Selections for B to open charm hadrons decays, but with initial state radiation emission of a dimuon.
B0 -> D- pi mumu
B- -> D0 pi mumu
Bs -> Ds pi mumu
Lb -> Lc pi mumu
Xib- -> Xic0 pi- mumu
B0 -> D- K mumu
B- -> D0 K mumu
B0 -> D- D+ mumu
B0 -> D0 D~0 mumu
B0 -> Lc Lc~ mumu

bonus: B_c -> B+ mumu with B+ -> Dpi or B+ ->JpsiK (two lines: with and without pointing requirements).
"""


default_config = {
    'NAME'                       : 'B2OCMuMu',
    'BUILDERTYPE'                : 'B2OCMuMuConf',
    'CONFIG'                     :
        {
        'BFlightCHI2'            : 50 #100
        , 'BcFlightCHI2'         : 25
        , 'BDIRA'                : 0.999
        , 'BIPCHI2'              : 25
        , 'BVertexCHI2'          : 12
        , 'BVertexCHI2Loose'     : 20
        , 'LeptonIPCHI2'         : 9
        , 'KaonIPCHI2'           : 9
        , 'KaonPT'               : 250
        , 'BMassWindow'          : 1500
        , 'Trk_Chi2'             : 3
        , 'Trk_GhostProb'        : 0.3
        , 'B_MassWindow_Hi'      : 5200
        , 'Bc_MassWindow_Hi'     : 6200
        , 'Lb_MassWindow_Hi'     : 5600
        , 'PTHad'                : 1000
        , 'K1_SumIPChi2Had'      : 48.0
        , 'DiHadronVtxCHI2'      : 10
        , 'MinProbNN'            : 0.02
        , 'MinProbNNTight'       : 0.1
        , 'DiHadronADOCACHI2'    : 30
        , 'DiHadronADOCA'        : 0.75 #0.5
        , 'Bu2mmLinePrescale'    : 1
        },
    'WGs'     : [ 'RD' ],
    'STREAMS' : [ 'Leptonic' ]
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop #as FilterDesktop_GC
from GaudiConfUtils.ConfigurableGenerators import CombineParticles, DaVinci__N3BodyDecays, DaVinci__N4BodyDecays
from Configurables import (ResolvedPi0Maker, PhotonMaker)
#from Configurables import FilterDesktop as FilterDesktop_C
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from CommonParticles.Utils import updateDoD
from GaudiKernel.SystemOfUnits import MeV

class B2OCMuMuConf(LineBuilder) :
    """
    Builder for rare baryonic mumu measurements
    """

    # now just define keys. Default values are fixed later
    __configuration_keys__ = (
        'BFlightCHI2'
        , 'BcFlightCHI2'
        , 'BDIRA'
        , 'BIPCHI2'
        , 'BVertexCHI2'
        , 'BVertexCHI2Loose'
        , 'LeptonIPCHI2'
        , 'KaonIPCHI2'
        , 'KaonPT'
        , 'BMassWindow'
        , 'Trk_Chi2'
        , 'Trk_GhostProb'
        , 'B_MassWindow_Hi'
        , 'Bc_MassWindow_Hi'
        , 'Lb_MassWindow_Hi'
        , 'PTHad'
        , 'K1_SumIPChi2Had'
        , 'DiHadronVtxCHI2'
        , 'MinProbNN'
        , 'MinProbNNTight'
        , 'DiHadronADOCACHI2'
        , 'DiHadronADOCA'
        , 'Bu2mmLinePrescale'
      )

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self._name = name
        mmCharmLine_name   = name
        mmCharmSSLine_name   = name+'SS'
        BcLine_name   = name+"Bc"
        BcNoPointLine_name   = name+"BcNoPoint"

        from StandardParticles import StdLoosePions as Pions
        from StandardParticles import StdLooseKaons as Kaons
        from StandardParticles import StdLooseProtons as Protons
        from StandardParticles import StdAllNoPIDsPions as AllPions
        from StandardParticles import StdAllNoPIDsKaons as AllKaons
        from StandardParticles import StdAllNoPIDsProtons as AllProtons
        from StandardParticles import StdLooseJpsi2MuMu as StdJpsis
        # from StandardParticles import StdVeryLooseLambdaLL as LambdasLL
        # from StandardParticles import StdLooseLambdaDD as LambdasDD
        # from StandardParticles import StdLooseLambdaLD as LambdasLD
        # from StandardParticles import StdVeryLooseKsLL as KsLL
        # from StandardParticles import StdLooseKsDD as KsDD
        # from StandardParticles import StdLooseKsLD as KsLD
        # from StandardParticles import StdLoosePhi2KK as Phis
        # from StandardParticles import StdNoPIDsDownProtons as DownProtons


        # charm hadrons

        SelD0 = self._makeD02Kpi( name   = "D0For" + self._name,
                              params = config )

        SelDp = self._makeDp2Kpipi( name   = "DpFor" + self._name,
                              params = config )

        SelDs = self._makeDs2KKpi( name   = "DsFor" + self._name,
                              params = config )

        SelXic0 = self._makeXic02pKKpi( name   = "Xic0For" + self._name,
                              pions  = AllPions,
                              kaons  = AllKaons,
                              protons = AllProtons,
                              params = config )

        # SelLc = self._makeLc2pKpi( name   = "LcFor" + self._name,
        #                       pions  = AllPions,
        #                       kaons  = AllKaons,
        #                       protons = AllProtons,
        #                       params = config )

        SelLc = self._makeLc2pKpi( name   = "LcFor" + self._name,
                              params = config )

        SelXicp = self._makeXic2pKpi( name   = "XicpFor" + self._name,
                              pions  = AllPions,
                              kaons  = AllKaons,
                              protons = AllProtons,
                              params = config )



        # 2 : Make Dileptons

        SelDiMuon = self._makeDiMuonDetached( "SelDiMuonsFor" + self._name,
                                          params   = config)
        SelDiMuonSS = self._makeDiMuonDetachedSS( "SelDiMuonsSSFor" + self._name,
                                          params   = config)
        SelDiMuonPrompt = self._makeDiMuonPrompt( "SelPromptDiMuonsFor" + self._name,
                                          params   = config)

        # 3: charm + pion
        SelLcpi = self._makeLcpi( name   = "LcpiFor" + self._name,
                              lambdacs = SelLc,
                              pions  = AllPions,
                              params = config )

        SelXic0pi = self._makeXi0cpi( name   = "Xic0piFor" + self._name,
                              xics = SelXic0,
                              pions  = AllPions,
                              params = config )

        SelXicppi = self._makeXipcpi( name   = "XicppiFor" + self._name,
                              xics = SelXicp,
                              pions  = AllPions,
                              params = config )

        SelD0pi = self._makeD0pi( name   = "D0piFor" + self._name,
                              d0s = SelD0,
                              pions  = AllPions,
                              params = config )

        SelDppi = self._makeDppi( name   = "DppiFor" + self._name,
                              dplus = SelDp,
                              pions  = AllPions,
                              params = config )

        SelDspi = self._makeDspi( name   = "DspiFor" + self._name,
                              ds = SelDs,
                              pions  = AllPions,
                              params = config )

        SelDpK = self._makeDmKp( name   = "DpKFor" + self._name,
                              dplus = SelDp,
                              kaons  = Kaons,
                              params = config )

        SelD0K = self._makeD0K( name   = "D0KFor" + self._name,
                              d0s = SelD0,
                              kaons  = Kaons,
                              params = config )

        SelD0D0 = self._makeD0D0( name   = "D0D0For" + self._name,
                              d0s = SelD0,
                              params = config )

        SelDpDp = self._makeDpDm( name   = "DpDmFor" + self._name,
                              dplus = SelDp,
                              params = config )

        SelLcLc = self._makeLcLc( name   = "LcLcFor" + self._name,
                              lcs = SelLc,
                              params = config )

        SelB2Dpi = self._makeB2D0pi( name   = "B2D0piFor" + self._name,
                              d0s = SelD0,
                              pions  = AllPions,
                              params = config )

        SelB2JpsiK = self._makeB2JpsiK( name   = "B2JpsiKFor" + self._name,
                              jpsis = StdJpsis,
                              kaons  = AllKaons,
                              params = config )

        #SS
        SelLcpiSS = self._makeLcpiSS( name   = "LcpiSSFor" + self._name,
                              lambdacs = SelLc,
                              pions  = AllPions,
                              params = config )

        SelD0piSS = self._makeD0piSS( name   = "D0piSSFor" + self._name,
                              d0s = SelD0,
                              pions  = AllPions,
                              params = config )

        SelDppiSS = self._makeDppiSS( name   = "DppiSSFor" + self._name,
                              dplus = SelDp,
                              pions  = AllPions,
                              params = config )

        SelDspiSS = self._makeDspiSS( name   = "DspiSSFor" + self._name,
                              ds = SelDs,
                              pions  = AllPions,
                              params = config )

        # 4 : Combine Particles


        SelB2mmX_charm = self._makeB2LLX_charm(mmCharmLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelLcpi, SelD0pi, SelDppi, SelDspi, SelDpK, SelD0K, SelD0D0, SelDpDp, SelLcLc, SelXic0pi], #, SelXicppi],
                                   params   = config,
                                   masscut  = "ADAMASS('B0') <  %(BMassWindow)s *MeV"% config)

        SelB2mmX_charmSS = self._makeB2LLX_charmSS(mmCharmSSLine_name,
                                   dileptonSS = SelDiMuonSS,
                                   hadrons  = [ SelLcpiSS, SelD0piSS, SelDppiSS, SelDspiSS, SelD0D0, SelDpDp, SelLcLc],
                                   params   = config,
                                   masscut  = "ADAMASS('B0') <  %(BMassWindow)s *MeV"% config)

        SelB2mmX_Bc = self._makeB2LLX_Bc(BcLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelB2Dpi, SelB2JpsiK ],
                                   params   = config,
                                   masscut  = "ADAMASS('B_c+') <  %(BMassWindow)s *MeV"% config)

        SelB2mmX_BcNoPoint = self._makeB2LLX_BcNoPoint(BcNoPointLine_name,
                                   dilepton = SelDiMuonPrompt,
                                   hadrons  = [ SelB2Dpi, SelB2JpsiK ],
                                   params   = config,
                                   masscut  = "(AM>0)"% config)


        # 5 : Declare Lines

        SPDFilter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 600 )" ,
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb" ]
            }


        self.B2mmCharmLine = StrippingLine(mmCharmLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX_charm,
                                       FILTER            = SPDFilter,
                                       #RequiredRawEvents = [],
                                       RelatedInfoTools = [
                                       { "Type" : "RelInfoMuonIDPlus",
                                       "Variables" : ["MU_BDT"],
                                       "DaughterLocations"  : {
                                       "[ Beauty -> X (X -> ^mu+ mu-) ]CC" : "Muon1BDT",
                                       "[ Beauty -> X (X -> mu+ ^mu-) ]CC" : "Muon2BDT",}
                                       },
                                       ],
                                       MDSTFlag          = False )

        self.B2mmCharmSSLine = StrippingLine(mmCharmSSLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX_charmSS,
                                       FILTER            = SPDFilter,
                                       #RequiredRawEvents = [],
                                       RelatedInfoTools = [
                                       { "Type" : "RelInfoMuonIDPlus",
                                       "Variables" : ["MU_BDT"],
                                       "DaughterLocations"  : {
                                       "[ Beauty -> X (X -> ^mu+ mu+) ]CC" : "Muon1BDT",
                                       "[ Beauty -> X (X -> mu+ ^mu+) ]CC" : "Muon2BDT",}
                                       },
                                       ],
                                       MDSTFlag          = False )

        self.BcLine = StrippingLine(BcLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX_Bc,
                                       FILTER            = SPDFilter,
                                       #RequiredRawEvents = [],
                                       RelatedInfoTools = [
                                       { "Type" : "RelInfoMuonIDPlus",
                                       "Variables" : ["MU_BDT"],
                                       "DaughterLocations"  : {
                                       "[ Beauty -> X- (X -> ^mu+ mu-) ]CC" : "Muon1BDT",
                                       "[ Beauty -> X- (X -> mu+ ^mu-) ]CC" : "Muon2BDT",}
                                       },
                                       ],
                                       MDSTFlag          = False )

        self.BcNoPointLine = StrippingLine(BcNoPointLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX_BcNoPoint,
                                       FILTER            = SPDFilter,
                                       #RequiredRawEvents = [],
                                       RelatedInfoTools = [
                                       { "Type" : "RelInfoMuonIDPlus",
                                       "Variables" : ["MU_BDT"],
                                       "DaughterLocations"  : {
                                       "[ Beauty -> X- (X -> ^mu+ mu-) ]CC" : "Muon1BDT",
                                       "[ Beauty -> X- (X -> mu+ ^mu-) ]CC" : "Muon2BDT",}
                                       },
                                       ],
                                       MDSTFlag          = False )

        # 6 : Register Lines

        self.registerLine( self.B2mmCharmLine )
        self.registerLine( self.B2mmCharmSSLine )
        self.registerLine( self.BcLine )
        self.registerLine( self.BcNoPointLine )

    #dimuon builders

    def _makeDiMuonDetached( self, name, params ) :
        """
        Make a dimuon
        rho(770)0 is just a proxy to get the two-body combination
        """

        _Decays = "rho(770)0 -> mu+ mu-"

         # define all the cuts
        _CombCuts    = "(AM > 0*MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2 > 4) & (0.5 < NINTREE((ABSID==13)&(PROBNNmu > %(MinProbNNTight)s)))" % params
        _daughtersCutsmu = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCutsmu }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts

        _stdAllLooseMuons = DataOnDemand(Location = "Phys/StdAllVeryLooseMuons/Particles")
        # make and store the Selection object
        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdAllLooseMuons ] )

    def _makeDiMuonDetachedSS( self, name, params ) :
        """
        Make a dimuon
        rho(770)0 is just a proxy to get the two-body combination
        """

        _Decays = "[rho(770)0 -> mu+ mu+]cc"

         # define all the cuts
        _CombCuts    = "(AM > 0*MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2 > 4) & (0.5 < NINTREE((ABSID==13)&(PROBNNmu > %(MinProbNNTight)s)))" % params
        _daughtersCutsmu = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCutsmu }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts

        _stdAllLooseMuons = DataOnDemand(Location = "Phys/StdAllLooseMuons/Particles")
        # make and store the Selection object
        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdAllLooseMuons ] )

    def _makeDiMuonPrompt( self, name, params ) :
        """
        Make a dimuon
        rho(770)0 is just a proxy to get the two-body combination
        """

        _Decays = "rho(770)0 -> mu+ mu-"

         # define all the cuts
        _CombCuts    = "(AM > 0*MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 9) & (0.5 < NINTREE((ABSID==13)&(PROBNNmu > %(MinProbNNTight)s)))" % params
        _daughtersCutsmu = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCutsmu }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts

        _stdAllLooseMuons = DataOnDemand(Location = "Phys/StdAllVeryLooseMuons/Particles")
        # make and store the Selection object
        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdAllLooseMuons ] )



#####################################################
# builders charm hadrons
#####################################################

    def _makeXic02pKKpi(self, name, pions, kaons, protons, params):
        '''Makes Xic0 -> p K- K- pi+
           borrowed from Xib2Xic0PiXic02PKKPiBeauty2CharmLine, adapted a bit
        '''
        comboCuts12 = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)"% params
        comboCuts123 = "(ADOCA(1,3)<%(DiHadronADOCA)s*mm) & (ADOCA(2,3)<%(DiHadronADOCA)s*mm)"% params
        comboCuts = "(ASUM(PT)>1500*MeV) & (ADAMASS('Xi_c0') < 100*MeV) & (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<3.) & (PT > 500*MeV) & (P > 5000*MeV)))) & (ADOCA(1,4)<%(DiHadronADOCA)s*mm) & (ADOCA(2,4)<%(DiHadronADOCA)s*mm) & (ADOCA(3,4)<%(DiHadronADOCA)s*mm) & (ADOCAMAX('')<0.5*mm)" % params
        momCuts = "(ADMASS('Xi_c0') < 100*MeV) & (VFASPF(VCHI2/VDOF)<10) & (BPVVDCHI2>36) & (BPVDIRA>0.9) & (2 == NINTREE((ABSID==321) & (PROBNNk > %(MinProbNNTight)s))) & (1 == NINTREE((ABSID==2212)&(PROBNNp > %(MinProbNNTight)s)))" % params
        cp = DaVinci__N4BodyDecays(
            Combination12Cut=comboCuts12,
            Combination123Cut=comboCuts123,
            CombinationCut=comboCuts,
            MotherCut=momCuts,
            DecayDescriptors=["[Xi_c0 -> p+ K- K- pi+]cc"])
        cp.DaughtersCuts = {
            "p+"  : "(P > 2*GeV)",
            "K-"  : "(P > 2*GeV)",
            "pi+"  : "(P > 1*GeV)",
            }
        return Selection(
            name+'_Xic02PKKPi',
            Algorithm=cp,
            RequiredSelections=[pions, kaons, protons])
#####################################################
    # def _makeLc2pKpi(self, name, pions, kaons, protons, params):
    #     '''Makes Lc -> p K pi '''
    #     comboCuts12 = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)"% params
    #     comboCuts = "(ASUM(PT)>1800*MeV) & (ADAMASS('Lambda_c+') < 100*MeV) & (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 400*MeV) & (P > 4000*MeV)))) & (ADOCA(1,3)<%(DiHadronADOCA)s*mm) & (ADOCA(2,3)<%(DiHadronADOCA)s*mm)" % params
    #     momCuts = "(ADMASS('Lambda_c+') < 100*MeV) & (VFASPF(VCHI2/VDOF)<25) & (BPVVDCHI2>25) & (BPVDIRA>0) & (1 == NINTREE((ABSID==321) & (PROBNNk > %(MinProbNNTight)s))) & (1 == NINTREE((ABSID==2212)&(PROBNNp > %(MinProbNNTight)s)))" % params
    #     cp = DaVinci__N3BodyDecays(
    #         Combination12Cut=comboCuts12,
    #         CombinationCut=comboCuts,
    #         MotherCut=momCuts,
    #         DecayDescriptors=["[Lambda_c+ -> p+ K- pi+]cc"])
    #     cp.DaughtersCuts = {
    #         "p+"  : "(P > 4*GeV)",
    #         "K-"  : "(P > 2*GeV)",
    #         "pi+"  : "(P > 1*GeV)",
    #         }
    #     return Selection(
    #         name+'_Lc2PKPi',
    #         Algorithm=cp,
    #         RequiredSelections=[pions, kaons, protons])
#####################################################
    def _makeXic2pKpi(self, name, pions, kaons, protons, params):
        '''Makes Xic+ -> p K pi '''
        comboCuts12 = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)"% params
        comboCuts = "(ASUM(PT)>1800*MeV) & (ADAMASS('Xi_c+') < 100*MeV) & (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 400*MeV) & (P > 4000*MeV)))) & (ADOCA(1,3)<%(DiHadronADOCA)s*mm) & (ADOCA(2,3)<%(DiHadronADOCA)s*mm) & (ADOCAMAX('')<0.5*mm)" % params
        momCuts = "(ADMASS('Xi_c+') < 100*MeV) & (VFASPF(VCHI2/VDOF)<10) & (BPVVDCHI2>36) & (BPVDIRA>0.9) & (1 == NINTREE((ABSID==321) & (PROBNNk > %(MinProbNNTight)s))) & (1 == NINTREE((ABSID==2212)&(PROBNNp > %(MinProbNNTight)s)))" % params
        cp = DaVinci__N3BodyDecays(
            Combination12Cut=comboCuts12,
            CombinationCut=comboCuts,
            MotherCut=momCuts,
            DecayDescriptors=["[Xi_c+ -> p+ K- pi+]cc"])
        cp.DaughtersCuts = {
            "p+"  : "(P > 4*GeV)",
            "K-"  : "(P > 2*GeV)",
            "pi+"  : "(P > 1*GeV)",
            }

        return Selection(
            name+'_Xicp2PKPi',
            Algorithm=cp,
            RequiredSelections=[pions, kaons, protons])

#####################################################
    def _makeLc2pKpi(self, name, params):
        '''Makes Lc -> p K- pi+
        '''
        _Code = "(PT > %(KaonPT)s *MeV) & (1== NINTREE((ABSID==321) & (PROBNNk>0.1))) & (1== NINTREE((ABSID==2212) & (PROBNNp>0.1)))"% params
        _Filter = FilterDesktop( Code = _Code )
        _stdLc= DataOnDemand(Location = "Phys/StdLooseLambdac2PKPi/Particles")
        return Selection( name, Algorithm = _Filter, RequiredSelections = [ _stdLc ] )

#####################################################
# for mesons we just recycle the Std particles...
#####################################################
    def _makeD02Kpi(self, name, params):
        '''Makes D0 -> K- pi+
        '''
        _Code = "(PT > %(KaonPT)s *MeV) & (1== NINTREE((ABSID==321) & (PROBNNk>0.1)))"% params
        _Filter = FilterDesktop( Code = _Code )
        _stdD0 = DataOnDemand(Location = "Phys/StdLooseD02KPi/Particles")
        return Selection( name, Algorithm = _Filter, RequiredSelections = [ _stdD0 ] )
#####################################################
    def _makeDp2Kpipi(self, name, params):
        '''Makes D+ -> K- pi+ pi+
        '''
        _Code = "(PT > %(KaonPT)s *MeV) & (1== NINTREE((ABSID==321) & (PROBNNk>0.1)))"% params
        _Filter = FilterDesktop( Code = _Code )
        _stdDp = DataOnDemand(Location = "Phys/StdLooseDplus2KPiPi/Particles")
        return Selection( name, Algorithm = _Filter, RequiredSelections = [ _stdDp ] )
#####################################################
    def _makeDs2KKpi(self, name, params):
        '''Makes Ds+ -> K- K+ pi+
        '''
        _Code = "(PT > %(KaonPT)s *MeV) & (2== NINTREE((ABSID==321) & (PROBNNk>0.1)))"% params
        _Filter = FilterDesktop( Code = _Code )
        _stdDs = DataOnDemand(Location = "Phys/StdLooseDsplus2KKPi/Particles")
        return Selection( name, Algorithm = _Filter, RequiredSelections = [ _stdDs ] )


#####################################################
# now, build the charm+pi combinations
#####################################################
    def _makeLcpi( self, name, lambdacs, pions, params):
        """
        Make a Sigmac0 -> Lc+ pi-
        """

        _Decays = "[Sigma_c0 -> Lambda_c+ pi-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Lb_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < 16) & (BPVVDCHI2>25) & (BPVDIRA>0.99)" #% params

        _LcCut = "(PT > %(PTHad)s * MeV)" %params
        _PionCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "Lambda_c+"  : _LcCut,
            "pi-"  : _PionCut
            }

        _sel_Sigmac = Selection(name+"_all",
            RequiredSelections = [ lambdacs, pions ],
            Algorithm = _Combine)

        return _sel_Sigmac
#####################################################
    def _makeXi0cpi( self, name, xics, pions, params):
        """
        Make a Xi_c(2790)~- -> Xic0 pi-
        """

        _Decays = "[Xi_c(2790)~- -> Xi_c0 pi-]cc"

        _CombinationCut = "(APT > %(PTHad)s *MeV) & " \
                          "(AM < %(B_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2>36) & (BPVDIRA>0.995)" #% params

        _XicCut = "(PT > %(PTHad)s * MeV)" %params
        _PionCut = "(PT > 250 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "Xi_c0"  : _XicCut,
            "pi-"  : _PionCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ xics, pions ],
            Algorithm = _Combine)

        return _sel
#####################################################
    def _makeXipcpi( self, name, xics, pions, params):
        """
        Make a Xi_c(2790)0 -> Xic+ pi-
        """

        _Decays = "[Xi_c(2790)0 -> Xi_c+ pi-]cc"

        _CombinationCut = "(APT > %(PTHad)s *MeV) & " \
                          "(AM < %(B_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2>36) & (BPVDIRA>0.995)" #% params

        _XicCut = "(PT > %(PTHad)s * MeV)" %params
        _PionCut = "(PT > 250 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "Xi_c+"  : _XicCut,
            "pi-"  : _PionCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ xics, pions ],
            Algorithm = _Combine)

        return _sel
#####################################################
    def _makeD0pi( self, name, d0s, pions, params):
        """
        Make a D*(2010)- -> D0 pi-
        """

        _Decays = "[D*(2010)- -> D0 pi-]cc"

        _CombinationCut = "(APT > %(PTHad)s *MeV) & " \
                          "(AM < %(B_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" % params

        _D0Cut = "(PT > %(PTHad)s * MeV)" %params
        _PionCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D0"  : _D0Cut,
            "pi-"  : _PionCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ d0s, pions ],
            Algorithm = _Combine)

        return _sel
#####################################################
    def _makeDppi( self, name, dplus, pions, params):
        """
        Make a D*(2007)0 -> D+ pi-
        """

        _Decays = "[D*(2007)0 -> D+ pi-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(B_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" % params

        _DCut = "(PT > %(PTHad)s * MeV)" %params
        _PionCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D+"  : _DCut,
            "pi-"  : _PionCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ dplus, pions ],
            Algorithm = _Combine)

        return _sel
#####################################################
    def _makeDspi( self, name, ds, pions, params):
        """
        Make a D_1(2420)0 -> Ds+ pi-
        """

        _Decays = "[D_1(2420)0 -> D_s+ pi-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(B_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" % params

        _DCut = "(PT > %(PTHad)s * MeV)" %params
        _PionCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D_s+"  : _DCut,
            "pi-"  : _PionCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ ds, pions ],
            Algorithm = _Combine)

        return _sel

#some SS versions
#####################################################
    def _makeLcpiSS( self, name, lambdacs, pions, params):
        """
        Make a Sigmac0 -> Lc+ pi+
        """

        _Decays = "[Sigma_c0 -> Lambda_c+ pi+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Lb_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < 16) & (BPVVDCHI2>25) & (BPVDIRA>0.99)" #% params

        _LcCut = "(PT > %(PTHad)s * MeV)" %params
        _PionCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "Lambda_c+"  : _LcCut,
            "pi+"  : _PionCut
            }

        _sel_Sigmac = Selection(name+"_all",
            RequiredSelections = [ lambdacs, pions ],
            Algorithm = _Combine)

        return _sel_Sigmac

    def _makeD0piSS( self, name, d0s, pions, params):
        """
        Make a D*(2010)+ -> D0 pi+
        """

        _Decays = "[D*(2010)+ -> D0 pi+]cc"

        _CombinationCut = "(APT > %(PTHad)s *MeV) & " \
                          "(AM < %(B_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" % params

        _D0Cut = "(PT > %(PTHad)s * MeV)" %params
        _PionCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D0"  : _D0Cut,
            "pi+"  : _PionCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ d0s, pions ],
            Algorithm = _Combine)

        return _sel
#####################################################
    def _makeDppiSS( self, name, dplus, pions, params):
        """
        Make a D*(2007)0 -> D+ pi+
        """

        _Decays = "[D*(2007)0 -> D+ pi+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(B_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" % params

        _DCut = "(PT > %(PTHad)s * MeV)" %params
        _PionCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D+"  : _DCut,
            "pi+"  : _PionCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ dplus, pions ],
            Algorithm = _Combine)

        return _sel
#####################################################
    def _makeDspiSS( self, name, ds, pions, params):
        """
        Make a D_1(2420)0 -> Ds+ pi+
        """

        _Decays = "[D_1(2420)0 -> D_s+ pi+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(B_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" % params

        _DCut = "(PT > %(PTHad)s * MeV)" %params
        _PionCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D_s+"  : _DCut,
            "pi+"  : _PionCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ ds, pions ],
            Algorithm = _Combine)

        return _sel

#charm pairs
#####################################################
    def _makeDpDm( self, name, dplus, params):
        """
        Make a psi(3770) -> D+ D-
        """

        _Decays = "psi(3770) -> D+ D-"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Lb_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" % params

        _DCut = "(PT > %(PTHad)s * MeV)" %params
        #_PionCut = "(PT > 100 *MeV) & " \
        #           "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D+"  : _DCut,
            "D-"  : _DCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ dplus ],
            Algorithm = _Combine)

        return _sel
#####################################################
    def _makeD0D0( self, name, d0s, params):
        """
        Make a psi(4040) -> D0 D~0
        """

        _Decays = "psi(4040) -> D0 D~0"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Lb_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" % params

        _D0Cut = "(PT > %(PTHad)s * MeV)" %params
        #_PionCut = "(PT > 100 *MeV) & " \
        #           "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D0"  : _D0Cut,
            "D~0"  : _D0Cut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ d0s ],
            Algorithm = _Combine)

        return _sel
#####################################################
    def _makeLcLc( self, name, lcs, params):
        """
        Make a psi(4415) -> Lambda_c+ Lambda_c~-
        """

        _Decays = "psi(4415) -> Lambda_c+ Lambda_c~-"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Lb_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < 25) & (BPVVDCHI2>10) & (BPVDIRA>0.9)" #% params

        _LcCut = "(PT > %(PTHad)s * MeV)" %params
        #_PionCut = "(PT > 100 *MeV) & " \
        #           "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "Lambda_c+"  : _LcCut,
            "Lambda_c~-"  : _LcCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ lcs ],
            Algorithm = _Combine)

        return _sel

#exotic
#####################################################
    def _makeDmKp( self, name, dplus, kaons, params):
        """
        Make a D*(2640)0 -> D- K+
        """

        _Decays = "[D*(2640)0 -> D- K+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(B_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" % params

        _DCut = "(PT > %(PTHad)s * MeV)" %params
        _KaonCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) & (PROBNNk>0.1)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D-"  : _DCut,
            "K+"  : _KaonCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ dplus, kaons ],
            Algorithm = _Combine)

        return _sel
#####################################################
    def _makeD0K( self, name, d0s, kaons, params):
        """
        Make a D_s1(2460)- -> D0 K-
        """

        _Decays = "[D_s1(2460)- -> D0 K-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(B_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" % params

        _D0Cut = "(PT > %(PTHad)s * MeV)" %params
        _KaonCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) & (PROBNNk>0.1)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D0"  : _D0Cut,
            "K-"  : _KaonCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ d0s, kaons ],
            Algorithm = _Combine)

        return _sel

#B decays for B_c
#####################################################
    def _makeB2D0pi( self, name, d0s, pions, params):
        """
        Make a B- -> D0 pi- (for B_c decays)
        """

        _Decays = "[B- -> D0 pi-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Bc_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        #_MotherCut = "(VFASPF(VCHI2PDOF) < 10) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" #% params
        _MotherCut = "(VFASPF(VCHI2/VDOF)<10) & (INTREE(HASTRACK & (P>5000*MeV) & (PT>800*MeV) & (TRCHI2DOF<4.) & (MIPCHI2DV(PRIMARY)>9) & (MIPDV(PRIMARY)>0.1*mm))) & (NINTREE((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))) > 1) & (BPVLTIME()>0.05*ps) & (BPVDIRA>0.9)"

        _D0Cut = "(PT > %(PTHad)s * MeV)" %params
        _PionCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "D0"  : _D0Cut,
            "pi-"  : _PionCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ d0s, pions ],
            Algorithm = _Combine)

        return _sel

#####################################################
    def _makeB2JpsiK( self, name, jpsis, kaons, params):
        """
        Make a B- -> Jpsi K- (for B_c decays)
        """

        _Decays = "[B- -> J/psi(1S) K-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Bc_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        #_MotherCut = "(VFASPF(VCHI2PDOF) < 10) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" #% params
        _MotherCut = "(VFASPF(VCHI2/VDOF)<10) & (INTREE(HASTRACK & (P>5000*MeV) & (PT>800*MeV) & (TRCHI2DOF<4.) & (MIPCHI2DV(PRIMARY)>9) & (MIPDV(PRIMARY)>0.1*mm))) & (NINTREE((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))) > 1) & (BPVLTIME()>0.05*ps) & (BPVDIRA>0.9)"

        _JpsiCut = "(PT > %(PTHad)s * MeV) & (2 == NINTREE((ABSID==13)&(PROBNNmu > %(MinProbNN)s)))" %params
        _KaonCut = "(PT > %(KaonPT)s *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) & (PROBNNk>%(MinProbNNTight)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "J/psi(1S)"  : _JpsiCut,
            "K-"  : _KaonCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ jpsis, kaons ],
            Algorithm = _Combine)

        return _sel

#####################################################
# final builder
#####################################################

#####################################################
    def _makeB2LLX_charm( self, name, dilepton, hadrons, params, masscut ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [
                    "[ B- -> rho(770)0 D*(2010)- ]cc",
                    "[ B- -> rho(770)0 D_s1(2460)- ]cc",
                    "[ B0 -> rho(770)0 D*(2007)0 ]cc",
                    "[ B0 -> rho(770)0 D*(2640)0 ]cc",
                    "[ B_s0 -> rho(770)0 D_1(2420)0 ]cc",
                    "[ Lambda_b0 -> rho(770)0 Sigma_c0 ]cc",

                    " B0 -> rho(770)0 psi(3770) ",
                    " B0 -> rho(770)0 psi(4040) ",
                    " B0 -> rho(770)0 psi(4415) ",
                    #"[ Xi_b0 -> rho(770)0  Xi_c(2790)0  ]cc",
                    "[ Xi_b- -> rho(770)0  Xi_c(2790)~-  ]cc",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )

#####################################################
    def _makeB2LLX_charmSS( self, name, dileptonSS, hadrons, params, masscut ):
        """
        CombineParticles / Selection for the SS channels (2101.05566)
        """

        _Decays = [
                    "[ B- -> rho(770)0 D*(2010)- ]cc",
                    #"[ B- -> rho(770)0 D_s1(2460)- ]cc",
                    "[ B0 -> rho(770)0 D*(2007)~0 ]cc",
                    #"[ B0 -> rho(770)0 D*(2640)0 ]cc",
                    "[ B_s0 -> rho(770)0 D_1(2420)~0 ]cc",
                    "[ Lambda_b0 -> rho(770)0 Sigma_c~0 ]cc",

                    " B0 -> rho(770)0 psi(3770) ",
                    " B0 -> rho(770)0 psi(4040) ",
                    " B0 -> rho(770)0 psi(4415) ",
                    #"[ Xi_b0 -> rho(770)0  Xi_c(2790)0  ]cc",
                    #"[ Xi_b- -> rho(770)0  Xi_c(2790)~-  ]cc",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dileptonSS, _Merge ] )

#####################################################
    def _makeB2LLX_Bc( self, name, dilepton, hadrons, params, masscut ):
        """
        CombineParticles / Selection for the B_c decays
        """

        _Decays = [
                    "[ B_c- -> rho(770)0 B- ]cc",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BcFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
#####################################################
    def _makeB2LLX_BcNoPoint( self, name, dilepton, hadrons, params, masscut ):
        """
        CombineParticles / Selection for the B_c decays
        """

        _Decays = [
                    "[ B_c- -> rho(770)0 B- ]cc",
                  ]

        _Cut = "(VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) " % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Combine.DaughtersCuts = {
            "rho(770)0"  : "(1 < NINTREE((ABSID==13)&(PROBNNmu > %(MinProbNNTight)s)))" %params,
            }

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
