###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__  = 'Vitalii Lisovskyi'
__date__    = '26/02/2021'
__version__ = '$Revision: 0 $'

__all__ = ( 'RareBaryonicMuMuConf', 'default_config' )

"""
Selections for b-baryons -> strange/charm baryons and dilepton
(omitting the simple channels already present in the B2XMuMu line).
29 channels in total.
Use it wisely.

Contents:
There are 5 separate stripping lines in this module.

---strange (without pi0)--- 15 channels ---StrippingRareBaryonicMuMuLine
+ Xib- -> Lambda K- mumu # Xi(1690)- used as a proxy for hadronic combination
+ Xib0 -> Lambda KS mumu # Xi(1690)0 <...>

+ Lambdab0 -> pK pi+ pi- mumu # Lambda(1830)0 -> Lambda(1520)0 pipi
+ Lambdab0 -> p KS pi- mumu # Lambda(1800)0 -> p K*-
+ Xib0 -> Xi- pi+ mumu # Xi*0
+ Xib- -> p K- K- mumu # Xi(1950)-

+ Lambdab0 -> Lambda0 pi+ pi- mumu # Lambda(1600)0
+ Xib- -> Xi- pi+ pi- mumu # Xi(1820)-
+ Omegab- -> Omega- pi+ pi- mumu # Omega(2250)-

+ Omegab- -> Xi- KS mumu # Xi(2030)- as Omega(2012) is not in particle table
+ Lambdab -> Xi- K+ mumu # Sigma(1915)0
+ Xib0 -> Omega- K+ mumu # Xi(1950)0

+ Lambdab -> Lambda phi mumu #  Lambda(2100)0
+ Xib- -> Xi- phi mumu #Sigma(1940)-
+ Omegab- -> Omega- phi mumu # Sigma(2030)-

---strange (with pi0 = neutral)--- 5 channels ---StrippingRareBaryonicMuMuNeutralLine

+ Xib0 -> Xi0 mumu
+ Lambdab0 -> Sigma+ pi- mumu # Sigma(1750)0
+ Xib0 -> Sigma+ K- mumu # Xi(2030)0
+ Omegab -> Xi0 K- mumu # Sigma(2250)-
+ Xi_bc+ -> Sigma+ mumu

---charm--- 5 channels ---StrippingRareBaryonicMuMuB2CharmLine
+ Lambdab -> Sigmac0 (Lc pi-) mumu
+ Xib0 -> Xic0 mumu
+ Xib0 -> Lc K- mumu # Xi_c(2790)0
+ Xi_bc+ -> Lc mumu
+ Xi_bc+ -> Xic+ mumu

---ground states Xi-, Omega- (extra track type combinations w.r.t. B2XMuMu) --- 2 lines each 2 channels ---
---StrippingRareBaryonicMuMuXiOmMuMuLine, StrippingRareBaryonicMuMuXiOmEELine---
+ Xib- -> Xi- mumu
+ Omegab- -> Omega- mumu
+ Xib- -> Xi- ee
+ Omegab- -> Omega- ee

---added in 2018 restripping (S34r0p2): lines with partial recosntruction (without the soft pion) ---
---StrippingRareBaryonicMuMuXiOmMuMuPRLine, StrippingRareBaryonicMuMuXiOmEEPRLine---
+ Xib- -> Xi- mumu
+ Omegab- -> Omega- mumu
+ Xib- -> Xi- ee
+ Omegab- -> Omega- ee

"""


default_config = {
    'NAME'                       : 'RareBaryonicMuMu',
    'BUILDERTYPE'                : 'RareBaryonicMuMuConf',
    'CONFIG'                     :
        {
        'BFlightCHI2'            : 50 #100
        ,'BFlightCHI2Ch'         : 36
        , 'BDIRA'                : 0.999
        , 'BIPCHI2'              : 25
        , 'BVertexCHI2'          : 12
        , 'BVertexCHI2Loose'     : 20
        , 'DiLeptonPT'           : 0
        , 'DiLeptonFDCHI2'       : 10 #16
        , 'DiLeptonIPCHI2'       : 0
        , 'LeptonIPCHI2'         : 9
        , 'LeptonPT'             : 250
        , 'KaonIPCHI2'           : 9
        , 'KaonPT'               : 250
        , 'UpperMass'            : 6500
        , 'BMassWindow'          : 1600
        , 'Trk_Chi2'             : 3
        , 'Trk_GhostProb'        : 0.5
        , 'KstarPMassWindow'     : 300 #not used
        , 'Hadron_MassWindow_Lo' : 0
        , 'Hadron_MassWindow_Hi' : 6300
        , 'Hadron_VtxChi2'       : 25
        , 'DiHadronVtxCHI2'      : 25
        , 'MinProbNN'            : 0.02
        , 'MinProbNNTight'       : 0.1
        , 'V0PT'                 : 0
        , 'V0TAU'                : 0.0005
        , 'HyperonWindow'        : 60.0
        , 'HyperonCombWindow'    : 75.0
        , 'HyperonMaxDocaChi2'   : 25.0
        , "Lambda_MassWindow_Lo" : 1105
        , "Lambda_MassWindow_Hi" : 1130
        , "Hadron_MinIPCHI2"     : 4.0
        , 'DiHadronMass'         : 3000
        , 'DiHadronADOCACHI2'    : 30
        , 'DiHadronADOCA'        : 0.75 #0.5
        , 'ProtonP'              : 5000
        , 'KstarPVertexCHI2'     : 36
        , 'OmegaPR_M_Lo_comb'    : 1370
        , 'OmegaPR_M_Hi_comb'    : 1700
        , 'XiPR_M_Lo_comb'       : 1000
        , 'XiPR_M_Hi_comb'       : 1350
        , 'DiHadronADOCACHI2_PR' : 225
        , 'DiHadronVtxCHI2_PR'   : 200
        , 'BDIRA_PR'             : 0.999
        , 'OmDIRA_PR'            : 0.999
        , 'MinDLLp'              : -0.5
        , 'MinDLLK'              : -0.5
        , 'BVtxCHI2_PR'          : 225
        , 'BIPCHI2_PR'           : 36
        , 'Bu2mmLinePrescale'    : 1
        },
    'WGs'     : [ 'RD' ],
    'STREAMS' : [ 'Leptonic' ]
    }



from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop as FilterDesktop_GC
from GaudiConfUtils.ConfigurableGenerators import CombineParticles, DaVinci__N3BodyDecays, DaVinci__N4BodyDecays
from Configurables import (ResolvedPi0Maker, PhotonMaker)
#from Configurables import FilterDesktop as FilterDesktop_C
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from CommonParticles.Utils import updateDoD
from GaudiKernel.SystemOfUnits import MeV

class RareBaryonicMuMuConf(LineBuilder) :
    """
    Builder for rare baryonic mumu measurements
    """

    # now just define keys. Default values are fixed later
    __configuration_keys__ = (
        'BFlightCHI2'
        ,'BFlightCHI2Ch'
        , 'BDIRA'
        , 'BIPCHI2'
        , 'BVertexCHI2'
        , 'BVertexCHI2Loose'
        , 'DiLeptonPT'
        , 'DiLeptonFDCHI2'
        , 'DiLeptonIPCHI2'
        , 'LeptonIPCHI2'
        , 'LeptonPT'
        , 'KaonIPCHI2'
        , 'KaonPT'
        , 'UpperMass'
        , 'BMassWindow'
        , 'Trk_Chi2'
        , 'Trk_GhostProb'
        , 'KstarPMassWindow'
        , 'Hadron_MassWindow_Lo'
        , 'Hadron_MassWindow_Hi'
        , 'Hadron_VtxChi2'
        , 'DiHadronVtxCHI2'
        , 'MinProbNN'
        , 'MinProbNNTight'
        , 'V0PT'
        , 'V0TAU'
        , 'HyperonWindow'
        , 'HyperonCombWindow'
        , 'HyperonMaxDocaChi2'
        , "Lambda_MassWindow_Lo"
        , "Lambda_MassWindow_Hi"
        , "Hadron_MinIPCHI2"
        , 'DiHadronMass'
        , 'DiHadronADOCACHI2'
        , 'DiHadronADOCA'
        , 'ProtonP'
        , 'KstarPVertexCHI2'
        , 'OmegaPR_M_Lo_comb'
        , 'OmegaPR_M_Hi_comb'
        , 'XiPR_M_Lo_comb'
        , 'XiPR_M_Hi_comb'
        , 'DiHadronADOCACHI2_PR'
        , 'DiHadronVtxCHI2_PR'
        , 'BDIRA_PR'
        , 'OmDIRA_PR'
        , 'MinDLLp'
        , 'MinDLLK'
        , 'BVtxCHI2_PR'
        , 'BIPCHI2_PR'
        , 'Bu2mmLinePrescale'
      )

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self._name = name
        mmXLine_name   = name
        mmCharmLine_name   = name+'B2Charm'
        mmNeutralLine_name   = name+'Neutral'
        mmNeutralDTFLine_name = name+'NeutralDTF'
        mmGroundLine_name   = name+'XiOmMuMu'
        eeGroundLine_name   = name+'XiOmEE'
        mmGroundPRLine_name   = name+'XiOmMuMuPR'
        eeGroundPRLine_name   = name+'XiOmEEPR'

        from StandardParticles import StdLoosePions as Pions
        from StandardParticles import StdLooseKaons as Kaons
        from StandardParticles import StdLooseProtons as Protons
        from StandardParticles import StdAllNoPIDsPions as AllPions
        from StandardParticles import StdAllNoPIDsKaons as AllKaons
        from StandardParticles import StdAllNoPIDsProtons as AllProtons
        from StandardParticles import StdVeryLooseLambdaLL as LambdasLL
        from StandardParticles import StdLooseLambdaDD as LambdasDD
        from StandardParticles import StdLooseLambdaLD as LambdasLD
        from StandardParticles import StdVeryLooseKsLL as KsLL
        from StandardParticles import StdLooseKsDD as KsDD
        from StandardParticles import StdLooseKsLD as KsLD
        from StandardParticles import StdLoosePhi2KK as Phis
        from StandardParticles import StdNoPIDsDownPions as DownPions
        from StandardParticles import StdNoPIDsDownKaons as DownKaons
        from StandardParticles import StdNoPIDsDownProtons as DownProtons

        # 1 : Make K, Ks, K*, K1, Phi and Lambdas

        # SelKaons  = self._filterHadron( name   = "KaonsFor" + self._name,
        #                                 sel    = Kaons,
        #                                 params = config )
        #
        # SelPions  = self._filterHadron( name   = "PionsFor" + self._name,
        #                                 sel    = Pions,
        #                                 params = config )
        #
        # SelLambdasLL = self._filterLongLivedHadron( name   = "LambdasLLFor" + self._name,
        #                                    sel    =  LambdasLL,
        #                                    params = config )
        #
        # SelLambdasDD = self._filterLongLivedHadron( name   = "LambdasDDFor" + self._name,
        #                                    sel    =  LambdasDD,
        #                                    params = config )
        #
        # SelLambdasLD = self._filterLongLivedHadron( name   = "LambdasLDFor" + self._name,
        #                                    sel    =  LambdasLD,
        #                                    params = config )
        #
        # SelKsLL = self._filterLongLivedHadron( name   = "KsLLFor" + self._name,
        #                                    sel    =  KsLL,
        #                                    params = config )
        #
        # SelKsDD = self._filterLongLivedHadron( name   = "KsDDFor" + self._name,
        #                                    sel    =  KsDD,
        #                                    params = config )
        #
        # SelKsLD = self._filterLongLivedHadron( name   = "KsLDFor" + self._name,
        #                                    sel    =  KsLD,
        #                                    params = config )

        #SelXi = self._makeXi( name   = "XisFor" + self._name, lambdasLL=LambdaLL, lambdasDD=LambdaDD, config)
        #SelOmega = self._makeOmega(self.LambdaLL, self.LambdaDD, config)

        SelXim =  self.__Xi__( name   = "XimFor" + self._name,
                              LambdaLL  = LambdasLL,
                              LambdaDD  = LambdasDD,
                              LambdaLD  = LambdasLD,
                              params = config )

        SelOmega =  self.__Omega__( name   = "OmegaFor" + self._name,
                              LambdaLL  = LambdasLL,
                              LambdaDD  = LambdasDD,
                              LambdaLD  = LambdasLD,
                              params = config )

        SelKstarsplus = self._makeKstarPlus( name   = "KstarplusFor" + self._name,
                               ksLL = KsLL,
                               ksDD = KsDD,
                               pions  = Pions,
                               params = config )

        SelPiPi = self._makePiPi( name   = "PiPiFor" + self._name,
                               pions  = Pions,
                               params = config )

        SelpKs = self._makepK( name   = "pKsFor" + self._name,
                                        protons = Protons,
                                        kaons  = Kaons,
                                        params = config )

        SelpKpipis = self._makepKpipi( name   = "pKpipisFor" + self._name,
                                        lambdas = SelpKs,
                                        rhos  = SelPiPi,
                                        params = config )

        SelLamK = self._makeLamK( name   = "LamKFor" + self._name,
                               lambdasLL  = LambdasLL,
                               lambdasDD  = LambdasDD,
                               lambdasLD  = LambdasLD,
                               kaons  = Kaons,
                               params = config )

        SelLamKS = self._makeLamKS( name   = "LamKSFor" + self._name,
                               lambdasLL  = LambdasLL,
                               lambdasDD  = LambdasDD,
                               lambdasLD  = LambdasLD,
                               ksLL  = KsLL,
                               ksDD  = KsDD,
                               ksLD  = KsLD,
                               params = config )

        SelLamPiPi = self._makeLamPiPi( name   = "LamPiPiFor" + self._name,
                               lambdasLL  = LambdasLL,
                               lambdasDD  = LambdasDD,
                               lambdasLD  = LambdasLD,
                               pipis = SelPiPi,
                               params = config )

        SelXiPiPi = self._makeXiPiPi( name   = "XiPiPiFor" + self._name,
                               xis  = SelXim,
                               pipis = SelPiPi,
                               params = config )

        SelOmegaPiPi = self._makeOmegaPiPi( name   = "OmegaPiPiFor" + self._name,
                               omegas  = SelOmega,
                               pipis = SelPiPi,
                               params = config )

        SelLamPhi = self._makeLamPhi( name   = "LamPhiFor" + self._name,
                               lambdasLL  = LambdasLL,
                               lambdasDD  = LambdasDD,
                               lambdasLD  = LambdasLD,
                               phis = Phis,
                               params = config )

        SelXiPhi = self._makeXiPhi( name   = "XiPhiFor" + self._name,
                               xis = SelXim,
                               phis = Phis,
                               params = config )

        SelOmPhi = self._makeOmegaPhi( name   = "OmPhiFor" + self._name,
                               omegas = SelOmega,
                               phis = Phis,
                               params = config )

        SelpKst = self._makepKst( name   = "pKstFor" + self._name,
                              protons  = Protons,
                              kstars  = SelKstarsplus,
                              params = config )

        SelpKK = self._makepKK( name   = "pKKFor" + self._name,
                              protons  = Protons,
                              kaons  = Kaons,
                              params = config )

        SelXistarz = self._makeXistarz( name   = "XistarzFor" + self._name,
                              xis  = SelXim,
                              pions  = Pions,
                              params = config )

        SelXiK = self._makeXiK( name   = "XiKFor" + self._name,
                              xis  = SelXim,
                              kaons  = Kaons,
                              params = config )

        SelXiKS = self._makeXiKS( name   = "XiKSFor" + self._name,
                              xis  = SelXim,
                              ksLL  = KsLL,
                              ksDD  = KsDD,
                              ksLD  = KsLD,
                              params = config )

        SelOmegaK = self._makeOmegaK( name   = "OmegaKFor" + self._name,
                              omegas  = SelOmega,
                              kaons  = Kaons,
                              params = config )

        # charm configs

        SelXic0 = self._makeXic02pKKpi( name   = "Xic0For" + self._name,
                              pions  = AllPions,
                              kaons  = AllKaons,
                              protons = AllProtons,
                              params = config )

        SelLc = self._makeLc2pKpi( name   = "LcFor" + self._name,
                              pions  = AllPions,
                              kaons  = AllKaons,
                              protons = AllProtons,
                              params = config )

        SelXicp = self._makeXic2pKpi( name   = "XicpFor" + self._name,
                              pions  = AllPions,
                              kaons  = AllKaons,
                              protons = AllProtons,
                              params = config )

        SelSigmac = self._makeSigmac( name   = "SigmacFor" + self._name,
                              lambdacs = SelLc,
                              pions  = AllPions,
                              params = config )

        SelLcK = self._makeLcK( name   = "LcKFor" + self._name,
                              lambdacs = SelLc,
                              kaons  = AllKaons,
                              params = config )

        #added for 2018 restripping: partial recosntruction
        SelXimPR =  self.__XiPR__( name   = "XimPRFor" + self._name,
                              protonsL = AllProtons,
                              protonsD = DownProtons,
                              pionsL = AllPions,
                              pionsD = DownPions,
                              params = config )

        SelOmegaPR =  self.__OmegaPR__( name   = "OmegaPRFor" + self._name,
                              protonsL = AllProtons,
                              protonsD = DownProtons,
                              kaonsL = AllKaons,
                              kaonsD = DownKaons,
                              params = config )


        # 2 : Make Dileptons

        from StandardParticles import StdLooseDiMuon as DiMuons
        MuonID = "(HASMUON)&(ISMUON)"
        DiMuonID     = "(2 == NINTREE((ABSID==13)&(HASMUON)&(ISMUON)))"
        DiElectronID     = "(2 == NINTREE((ABSID==11)&(PIDe>0)))"

        SelDiMuon = self._filterDiLepton( "SelDiMuonsFor" + self._name,
                                          dilepton = DiMuons,
                                          params   = config,
                                          idcut    = DiMuonID )

        SelDiElectronRaw = self._makeDiElectron( "SelDiElectronsRawFor" + self._name,
                                          params   = config )

        SelDiElectron = self._filterDiLepton( "SelDiElectronsFor" + self._name,
                                          dilepton = SelDiElectronRaw,
                                          params   = config,
                                          idcut    = DiElectronID )

        # make pi0s for hyperons
        """
        Make pi0 for hyperon (Xi0, Sigma+) decays.
        Xi0 is very long-lived, therefore pi0 is very displaced. Standard pi0 definitions do not suit us as they assume pi0 poiting to the PV. We use a custom very loose pi0 with extremely loose photon cut and very broad mass window.
        """
        piz_wide = ResolvedPi0Maker('MyVeryLoosePi02gg',
                                   DecayDescriptor = 'Pi0',
                                   MassWindow = 160.* MeV)

        make_loose_photons = PhotonMaker(
            #"LoosePhotonsForPiz",
            PtCut=70.*MeV,
            ConfLevelCut=0.2)
        piz_wide.addTool(make_loose_photons)
        updateDoD(piz_wide)

        # wide.addTool(PhotonMaker)
        # wide.PhotonMaker.PtCut = 70.*MeV
        # wide.PhotonMaker.ConfLevelCut = 0.15

        #updateDoD(piz_wide)
        #MyVeryLoosePi02gg = wide

        # fltr =  FilterDesktop_C (
        #     'MyVeryLooseResolvedPi0',
        #     Code = "ADMASS('pi0') < 130 * MeV ",
        #     Inputs =  ['Phys/%s/Particles'  % wide.name()]
        #     )
        # locations.update ( updateDoD ( fltr ) )
        # MyVeryLooseResolvedPi0 = fltr


        #neutral configs (Xi0, Sigma+)


        SelXi0 = self._makeXi0( name   = "Xi0For" + self._name,
                               lambdasLL  = LambdasLL,
                               lambdasDD  = LambdasDD,
                               lambdasLD  = LambdasLD,
                               pi0s  = AutomaticData("Phys/MyVeryLoosePi02gg/Particles"),
                               params = config )

        SelSigmap = self._makeSigmap( name   = "SigmapFor" + self._name,
                               protonsL  = Protons,
                               protonsD  = DownProtons,
                               pi0s  = AutomaticData("Phys/MyVeryLoosePi02gg/Particles"),
                               params = config )

        SelSigmapTight = self._makeSigmapTight( name   = "SigmapTightFor" + self._name,
                               protonsL  = Protons,
                               protonsD  = DownProtons,
                               pi0s  = AutomaticData("Phys/MyVeryLoosePi02gg/Particles"),
                               params = config )

        SelXi0K = self._makeXi0K( name   = "Xi0KFor" + self._name,
                               xis  = SelXi0,
                               kaons  = Kaons,
                               params = config )

        SelSigmaK = self._makeSigmaK( name   = "SigmaKFor" + self._name,
                               sigmas  = SelSigmap,
                               kaons  = Kaons,
                               params = config )

        SelSigmaPi = self._makeSigmaPi( name   = "SigmaPiFor" + self._name,
                               sigmas  = SelSigmap,
                               pions  = Pions,
                               params = config )

        #neutral configs (Xi0, Sigma+) with the DTF


        SelXi0_DTF = self._makeXi0_DTF( name   = "Xi0DTFFor" + self._name,
                               lambdasLL  = LambdasLL,
                               lambdasDD  = LambdasDD,
                               lambdasLD  = LambdasLD,
                               pi0s  = AutomaticData("Phys/MyVeryLoosePi02gg/Particles"),
                               params = config )

        SelSigmap_DTF = self._makeSigmap_DTF( name   = "SigmapDTFFor" + self._name,
                               protonsL  = Protons,
                               protonsD  = DownProtons,
                               pi0s  = AutomaticData("Phys/MyVeryLoosePi02gg/Particles"),
                               params = config )


        SelXi0K_DTF = self._makeXi0K_DTF( name   = "Xi0KDTFFor" + self._name,
                               xis  = SelXi0_DTF,
                               kaons  = Kaons,
                               params = config )

        SelSigmaK_DTF = self._makeSigmaK_DTF( name   = "SigmaKDTFFor" + self._name,
                               sigmas  = SelSigmap_DTF,
                               kaons  = Kaons,
                               params = config )

        SelSigmaPi_DTF = self._makeSigmaPi_DTF( name   = "SigmaPiDTFFor" + self._name,
                               sigmas  = SelSigmap_DTF,
                               pions  = Pions,
                               params = config )
        # 4 : Combine Particles


        SelB2mmX = self._makeB2LLX(mmXLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelLamK, SelLamKS, SelLamPiPi, SelXiPiPi, SelOmegaPiPi, SelpKst, SelpKK, SelpKpipis, SelXistarz, SelXiK, SelXiKS, SelOmegaK, SelLamPhi, SelXiPhi, SelOmPhi ],
                                   #hadrons  = [ SelPions, SelKaons, SelLambdasLL , SelLambdasDD, SelLambdasLD, SelPiPi, SelLamK, SelLamKS, SelLamPiPi ],
                                   params   = config,
                                   masscut  = "ADAMASS('Xi_b0') <  %(BMassWindow)s *MeV"% config)

        SelB2mmX_charm = self._makeB2LLX_charm(mmCharmLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelXic0, SelSigmac, SelLc, SelXicp, SelLcK ],
                                   params   = config,
                                   masscut  = "ADAMASS('Xi_b0') <  1700 *MeV"% config)

        SelB2mmX_neutral = self._makeB2LLX_neutral(mmNeutralLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelXi0, SelXi0K, SelSigmaK, SelSigmaPi, SelSigmapTight ],
                                   params   = config,
                                   masscut  = "ADAMASS('Xi_b0') <  %(BMassWindow)s *MeV"% config)

        SelB2mmX_neutral_DTF = self._makeB2LLX_neutral_DTF(mmNeutralDTFLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelXi0_DTF, SelXi0K_DTF, SelSigmaK_DTF, SelSigmaPi_DTF], #, SelSigmap_DTF ],
                                   params   = config,
                                   masscut  = "ADAMASS('Xi_b0') <  %(BMassWindow)s *MeV"% config)

        SelB2mmX_ground = self._makeB2LLX_ground(mmGroundLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelXim, SelOmega ],
                                   params   = config,
                                   masscut  = "ADAMASS('Xi_b-') <  %(BMassWindow)s *MeV"% config)

        SelB2eeX_ground = self._makeB2LLX_ground(eeGroundLine_name,
                                   dilepton = SelDiElectron,
                                   hadrons  = [ SelXim, SelOmega ],
                                   params   = config,
                                   masscut  = "ADAMASS('Xi_b-') <  %(BMassWindow)s *MeV"% config)

        SelB2mmX_ground_PR = self._makeB2LLX_ground_PR(mmGroundPRLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelXimPR, SelOmegaPR ],
                                   params   = config,
                                   masscut  = "ADAMASS('Xi_b-') <  %(BMassWindow)s *MeV"% config)

        SelB2eeX_ground_PR = self._makeB2LLX_ground_PR(eeGroundPRLine_name,
                                   dilepton = SelDiElectron,
                                   hadrons  = [ SelXimPR, SelOmegaPR ],
                                   params   = config,
                                   masscut  = "ADAMASS('Xi_b-') <  %(BMassWindow)s *MeV"% config)

        # 5 : Declare Lines

        SPDFilter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 600 )" ,
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb" ]
            }


        self.B2mmXLine = StrippingLine(mmXLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = ["Velo"],
                                       MDSTFlag          = False )

        self.B2mmCharmLine = StrippingLine(mmCharmLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX_charm,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = ["Velo"],
                                       MDSTFlag          = False )

        self.B2mmNeutralLine = StrippingLine(mmNeutralLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX_neutral,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = ["Velo"],
                                       MDSTFlag          = False )

        self.B2mmNeutralDTFLine = StrippingLine(mmNeutralDTFLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX_neutral_DTF,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = ["Velo"],
                                       MDSTFlag          = False )

        self.B2mmGroundLine = StrippingLine(mmGroundLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX_ground,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = ["Velo"],
                                       MDSTFlag          = False )

        self.B2eeGroundLine = StrippingLine(eeGroundLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2eeX_ground,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = ["Velo"],
                                       MDSTFlag          = False )

        self.B2mmGroundPRLine = StrippingLine(mmGroundPRLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX_ground_PR,
                                       #FILTER            = SPDFilter,
                                       RequiredRawEvents = ["Velo"],
                                       MDSTFlag          = False )

        self.B2eeGroundPRLine = StrippingLine(eeGroundPRLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2eeX_ground_PR,
                                       #FILTER            = SPDFilter,
                                       RequiredRawEvents = ["Velo"],
                                       MDSTFlag          = False )

        # 6 : Register Lines


        self.registerLine( self.B2mmXLine )
        self.registerLine( self.B2mmCharmLine )
        self.registerLine( self.B2mmNeutralLine )
        #self.registerLine( self.B2mmNeutralDTFLine )
        self.registerLine( self.B2mmGroundLine )
        self.registerLine( self.B2eeGroundLine )
        self.registerLine( self.B2mmGroundPRLine )
        self.registerLine( self.B2eeGroundPRLine )


# #####################################################
# Define various combinations of hadrons
# #####################################################
# hyperon selections adapted from B2XMuMu, with addition of extra track types
    def __Hyperon__(self, name, LambdaLL, LambdaDD, LambdaLD, LongTracks, DownTracks, UpTracks, hyperon, descriptor, params):
        """
        Copied from B2XMumu lines, but with loosened cuts.
        Assemble Xi- (Omega-) -> Lambda0 pi- (K-) using:
         - The Lambda0. Handle LL, LD, DD Lambda0
           separately because not all the track type combinations make sense (e.g. LL
           Lambda0 combined with a downstream pi- (K-) to make Xi- (Omega-))
         - pi- (K-) can be long or downstream or upstream, depending on what type of Lambda0 is used.

        Variable names mostly correspond to Xi- -> Lambda0 pi- but the code can be reused
        for Omega- -> Lambda0 K-
        """
        _xicuts = "(PT > %(V0PT)s * MeV)" \
            + " & (ADMASS('" + hyperon + "-') < %(HyperonWindow)s * MeV)" \
            + " & (VFASPF(VCHI2) < %(HyperonMaxDocaChi2)s)" \
            + " & (BPVLTIME() > %(V0TAU)s * ps)"
        _xicombcuts = "(ADAMASS('" + hyperon + "-') < %(HyperonCombWindow)s * MeV)" \
            + " & (ADOCACHI2CUT(%(HyperonMaxDocaChi2)s, ''))"

        _xi2lambda0pi = CombineParticles(
            DecayDescriptor = descriptor,
            CombinationCut = _xicombcuts%(params),
            MotherCut = _xicuts%(params)
            )

        _sel_xi2lambda0pi_lll = Selection("Selection_"+name+"_"+hyperon+"LLL",
            RequiredSelections = [ LambdaLL, LongTracks ],
            Algorithm = _xi2lambda0pi)

        _sel_xi2lambda0pi_ddl = Selection("Selection_"+name+"_"+hyperon+"DDL",
            RequiredSelections = [ LambdaDD, LongTracks ],
            Algorithm = _xi2lambda0pi)

        _sel_xi2lambda0pi_dll = Selection("Selection_"+name+"_"+hyperon+"DLL",
            RequiredSelections = [ LambdaLD, LongTracks ],
            Algorithm = _xi2lambda0pi)

        _sel_xi2lambda0pi_ddd = Selection("Selection_"+name+"_"+hyperon+"DDD",
            RequiredSelections = [ LambdaDD, DownTracks ],
            Algorithm = _xi2lambda0pi)

        _sel_xi2lambda0pi_llu = Selection("Selection_"+name+"_"+hyperon+"LLU",
            RequiredSelections = [ LambdaLL, UpTracks ],
            Algorithm = _xi2lambda0pi)

        _sel_xi2lambda0pi_ddu = Selection("Selection_"+name+"_"+hyperon+"DDU",
            RequiredSelections = [ LambdaDD, UpTracks ],
            Algorithm = _xi2lambda0pi)

        _sel = MergedSelection("Selection_"+name+"_"+hyperon,
            RequiredSelections = [ _sel_xi2lambda0pi_lll, _sel_xi2lambda0pi_ddl, _sel_xi2lambda0pi_dll, _sel_xi2lambda0pi_ddd, _sel_xi2lambda0pi_llu, _sel_xi2lambda0pi_ddu ])

        return _sel

    # def __LambdaSeparate__(self, conf):
    #     """
    #     Filter Lambda from MyVeryLooseLambdaLL and StdLooseLambdaDD
    #     """
    #     _lambdadd = AutomaticData(Location = 'Phys/StdLooseLambdaDD/Particles')
    #     _lambdall = AutomaticData(Location = 'Phys/MyVeryLooseLambdaLL/Particles')
    #     _filter_lambdadd = FilterDesktop(Code = self.__LambdaCuts__(conf))
    #     _filter_lambdall = FilterDesktop(Code = self.__LambdaCuts__(conf))
    #     _sellambdadd = Selection("Selection_"+self.name+"_Lambdadd",
    #                          RequiredSelections = [ _lambdadd ] ,
    #                          Algorithm = _filter_lambdadd)
    #     _sellambdall = Selection("Selection_"+self.name+"_Lambdall",
    #                          RequiredSelections = [ _lambdall ] ,
    #                          Algorithm = _filter_lambdall)
    #     return _sellambdall, _sellambdadd

    def __Xi__(self, name, LambdaLL, LambdaDD, LambdaLD, params):
        """
        Use __Hyperon__() to create Xi- candidates.
        """
        _longpions = AutomaticData(Location = 'Phys/StdAllNoPIDsPions/Particles')
        _downpions = AutomaticData(Location = 'Phys/StdNoPIDsDownPions/Particles')
        _uppions = AutomaticData(Location = 'Phys/StdNoPIDsUpPions/Particles')

        #TrackCuts = "(TRGHP < %(Track_GhostProb)s)" %config
        HypBachCuts = "(MIPCHI2DV(PRIMARY) > %(Hadron_MinIPCHI2)s)" %params

        _filter_longpions = FilterDesktop_GC(Code = HypBachCuts)
        _filter_downpions = FilterDesktop_GC(Code = HypBachCuts)
        _filter_uppions = FilterDesktop_GC(Code = HypBachCuts)

        _sel_longpions = Selection("Selection_"+name+"_LongXiPions",
            RequiredSelections = [ _longpions ],
            Algorithm = _filter_longpions)
        _sel_downpions = Selection("Selection_"+name+"_DownXiPions",
            RequiredSelections = [ _downpions ],
            Algorithm = _filter_downpions)
        _sel_uppions = Selection("Selection_"+name+"_UpXiPions",
            RequiredSelections = [ _uppions ],
            Algorithm = _filter_uppions)

        return self.__Hyperon__(name, LambdaLL, LambdaDD, LambdaLD, _sel_longpions, _sel_downpions, _sel_uppions,
            "Xi", "[Xi- -> Lambda0 pi-]cc", params)

    def __Omega__(self, name, LambdaLL, LambdaDD, LambdaLD, params):
        """
        Use __Hyperon__() to create Omega- candidates.
        """

        _longkaons = AutomaticData(Location = 'Phys/StdAllNoPIDsKaons/Particles')
        _downkaons = AutomaticData(Location = 'Phys/StdNoPIDsDownKaons/Particles')
        _upkaons = AutomaticData(Location = 'Phys/StdNoPIDsUpKaons/Particles')

        HypBachCuts = "(MIPCHI2DV(PRIMARY) > %(Hadron_MinIPCHI2)s)" %params

        _filter_longkaons = FilterDesktop_GC(Code = HypBachCuts)
        _filter_downkaons = FilterDesktop_GC(Code = HypBachCuts)
        _filter_upkaons = FilterDesktop_GC(Code = HypBachCuts)

        _sel_longkaons = Selection("Selection_"+name+"_LongOmegaKaons",
            RequiredSelections = [ _longkaons ],
            Algorithm = _filter_longkaons)
        _sel_downkaons = Selection("Selection_"+name+"_DownOmegaKaons",
            RequiredSelections = [ _downkaons ],
            Algorithm = _filter_downkaons)
        _sel_upkaons = Selection("Selection_"+name+"_UpOmegaKaons",
            RequiredSelections = [ _upkaons ],
            Algorithm = _filter_upkaons)


        return self.__Hyperon__(name, LambdaLL, LambdaDD, LambdaLD, _sel_longkaons, _sel_downkaons, _sel_upkaons,
            "Omega", "[Omega- -> Lambda0 K-]cc", params)


#####################################################
    def _filterDiLepton( self, name, dilepton, params, idcut = None ) :
        """
        Handy interface for dilepton filter
        """

        _Code = "(ID=='J/psi(1S)') & "\
                "(PT > %(DiLeptonPT)s *MeV) & "\
                "(MM < %(UpperMass)s *MeV) & "\
                "(MINTREE(ABSID<14,PT) > %(LeptonPT)s *MeV) & "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "\
                "(VFASPF(VCHI2/VDOF) < 10) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "\
                "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params

        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )

        _Filter = FilterDesktop_GC( Code = _Code )

        return Selection(name, Algorithm = _Filter, RequiredSelections = [ dilepton ] )

#####################################################
    def _makeDiElectron( self, name, params ) :
        """
        Make a dielectron
        J/psi(1S) is just a proxy to get the two-body combination
        """

        from Configurables import DiElectronMaker, ProtoParticleCALOFilter
        from CommonParticles.Utils import trackSelector
        dieLL = DiElectronMaker('MyDiElectronFromTracks')
        dieLL.Particle = "J/psi(1S)"
        selector = trackSelector ( dieLL , trackTypes = ["Long"] )
        dieLL.addTool( ProtoParticleCALOFilter, name='Electron' )
        dieLL.Electron.Selection = ["CombDLL(e-pi)>'-2.0'"] # ["RequiresDet='CALO' CombDLL(e-pi)>'1.0'"]
        dieLL.DiElectronMassMax = 6000.*MeV
        dieLL.DiElectronMassMin = 0.*MeV
        dieLL.DiElectronPtMin = 250.*MeV
        #locations.update( updateDoD(dieLL) )
        #MyDiElectronFromTracks = dieLL

        return Selection( name, Algorithm = dieLL )

#####################################################
    def _makePiPi( self, name, pions, params):
        """
        Make a rho -> pi+ pi- in a range below 3000 MeV.
        Should probably also work to do K+K- via misidentifications.
        """

        _Decays = "rho(770)0 -> pi+ pi-"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(DiHadronMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params #UpperBMass

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut = "(ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "pi+"  : _DaughterCut,
            "pi-" : _DaughterCut
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ pions ] )


#####################################################
    def _makepK( self, name, protons, kaons, params):
        """
        Make a Lambda* -> p K- in entire range.
        """

        _Decays = "[Lambda(1520)0 -> p+ K-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Hadron_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_p = "(PT > %(KaonPT)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNp > %(MinProbNNTight)s)" % params
        _DaughterCut_K = "(PT > %(KaonPT)s *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNk > %(MinProbNNTight)s)" % params


        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut_p,
            "K-" : _DaughterCut_K
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons, kaons ] )

#####################################################
    def _makepKpipi( self, name, lambdas, rhos, params):
        """
        Make a Lambda* -> p K- pi+ pi- in entire range.
        """

        _Decays = "[Lambda(1830)0 -> Lambda(1520)0 rho(770)0]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Hadron_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_Lam = "(ALL)"
        _DaughterCut_Rho = "(ALL)"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "Lambda(1520)0"  : _DaughterCut_Lam,
            "rho(770)0" : _DaughterCut_Rho
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ lambdas, rhos ] )

#####################################################
    def _makeKstarPlus( self, name, ksLL, ksDD, pions, params):
        """
        Make a K*(892)+ -> KS0 pi+
        ##Check for list as input for KS0s which use (Very)Loose and Brunel candidates
        """
        # if isinstance(kshorts,list):
        #     sel_list = [MergedSelection("Merged"+name,RequiredSelections= kshorts,Unique=True),pions]
        # else:
        #     sel_list = [kshorts,pions]

        _Decays = "[K*(892)+ -> KS0 pi+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Hadron_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(KstarPVertexCHI2)s)" % params

        _KshortCut = "(PT > %(KaonPT)s *MeV)" % params

        _PionCut = "(PT > %(KaonPT)s *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "KS0"  : _KshortCut,
            "pi+"  : _PionCut
            }

        _sel_KstarplusLL = Selection(name+"_LL",
            RequiredSelections = [ ksLL, pions ],
            Algorithm = _Combine)

        _sel_KstarplusDD = Selection(name+"_DD",
            RequiredSelections = [ ksDD, pions ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_KstarplusLL, _sel_KstarplusDD ])

        return _sel

#####################################################
    def _makepKK( self, name, protons, kaons, params):
        """
        Make a Xi(1950)- -> p+ K- K- in entire range.
        """

        _Decays = "[Xi(1950)- -> p+ K- K-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Hadron_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_k = "(PT > %(KaonPT)s *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s))  & (PROBNNk > %(MinProbNNTight)s)" % params
        _DaughterCut_p = "(PT > %(KaonPT)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s))  & (PROBNNp > %(MinProbNNTight)s)" % params
        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut_p,
            "K-" : _DaughterCut_k
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons, kaons ] )

#####################################################
    def _makeXistarz( self, name, xis, pions, params):
        """
        Make a Xi*0 -> Xi- pi+
        """

        _Decays = "[Xi*0 -> Xi- pi+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Hadron_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(KstarPVertexCHI2)s)" % params

        _XiCut = "(PT > %(KaonPT)s *MeV)" % params

        _PionCut = "(PT > %(KaonPT)s *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "Xi-"  : _XiCut,
            "pi+"  : _PionCut
            }


        _sel_Xistarz = Selection(name+"_all",
            RequiredSelections = [ xis, pions ],
            Algorithm = _Combine)

        #_sel = MergedSelection(name+"_all",
    #        RequiredSelections = [ _sel_KstarplusLL, _sel_KstarplusDD ])

        return _sel_Xistarz

#####################################################
    def _makeXiK( self, name, xis, kaons, params):
        """
        Make a Sigma(1915)0 -> Xi- K+
        arxiv 1507.04640
        """

        _Decays = "[Sigma(1915)0 -> Xi- K+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Hadron_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(KstarPVertexCHI2)s)" % params

        _XiCut = "(PT > %(KaonPT)s *MeV)" % params

        _KaonCut = "(PT > %(KaonPT)s *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) & (PROBNNk > %(MinProbNNTight)s) & (HASRICH)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "Xi-"  : _XiCut,
            "K+"  : _KaonCut
            }

        _sel_XiK = Selection(name+"_all",
            RequiredSelections = [ xis, kaons ],
            Algorithm = _Combine)

        return _sel_XiK

#####################################################
    def _makeOmegaK( self, name, omegas, kaons, params):
        """
        Make a Xi(1950)0 -> Omega- K+
        """

        _Decays = "[Xi(1950)0 -> Omega- K+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(Hadron_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(KstarPVertexCHI2)s)" % params

        _OmCut = "(PT > %(KaonPT)s *MeV)" % params

        _KaonCut = "(PT > %(KaonPT)s *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) & (PROBNNk > %(MinProbNNTight)s) & (HASRICH)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "Omega-"  : _OmCut,
            "K+"  : _KaonCut
            }

        _sel_OmK = Selection(name+"_all",
            RequiredSelections = [ omegas, kaons ],
            Algorithm = _Combine)

        return _sel_OmK


#####################################################
    def _makeXiKS( self, name, xis, ksLL, ksDD, ksLD, params ) :
        """
        Make an Xi(2030)- -> Xi KS0
        Xi(2030)- is just a proxy to get the two-body combination (should in fact be Omega resonance, but they are not in the particle list)
        """

        _Decays = "[Xi(2030)- -> Xi- KS0]cc"

         # define all the cuts

        _CombinationCut    = "(AM < %(Hadron_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsKS = "(ALL)"
        _daughtersCutsXi = "(ALL)"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Xi-"  : _daughtersCutsXi,
            "KS0" : _daughtersCutsKS }

        _sel_KLLXi = Selection(name+"_LL",
            RequiredSelections = [ xis, ksLL ],
            Algorithm = _Combine)

        _sel_KDDXi = Selection(name+"_DD",
            RequiredSelections = [ xis, ksDD ],
            Algorithm = _Combine)

        _sel_KLDXi = Selection(name+"_LD",
            RequiredSelections = [ xis, ksLD ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_KLLXi, _sel_KDDXi, _sel_KLDXi ])

        return _sel


#####################################################
    def _makeLamK( self, name, lambdasLL, lambdasDD, lambdasLD, kaons, params ) :
        """
        Make an Xi(1690)- -> Lambda0 K-
        Xi(1690)- is just a proxy to get the two-body combination
        """

        _Decays = "[Xi(1690)- -> Lambda0 K-]cc"

         # define all the cuts

        _CombinationCut    = "(AM > %(Hadron_MassWindow_Lo)s*MeV) & (AM < %(Hadron_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsK = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNk > %(MinProbNN)s) & (HASRICH)" % params
        _daughtersCutsL = "(M > %(Lambda_MassWindow_Lo)s*MeV) & (M<%(Lambda_MassWindow_Hi)s*MeV)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Lambda0"  : _daughtersCutsL,
            "K-" : _daughtersCutsK }

        _sel_KLambdaLL = Selection(name+"_LL",
            RequiredSelections = [ lambdasLL, kaons ],
            Algorithm = _Combine)

        _sel_KLambdaDD = Selection(name+"_DD",
            RequiredSelections = [ lambdasDD, kaons ],
            Algorithm = _Combine)

        _sel_KLambdaLD = Selection(name+"_LD",
            RequiredSelections = [ lambdasLD, kaons ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_KLambdaLL, _sel_KLambdaDD, _sel_KLambdaLD ])

        return _sel

#####################################################
    def _makeLamKS( self, name, lambdasLL, lambdasDD, lambdasLD, ksLL, ksDD, ksLD, params ) :
        """
        Make an Xi(1690)0 -> Lambda0 KS0
        Xi(1690)- is just a proxy to get the two-body combination
        """

        _Decays = "[Xi(1690)0 -> Lambda0 KS0]cc"

         # define all the cuts

        _CombinationCut    = "(AM > %(Hadron_MassWindow_Lo)s*MeV) & (AM < %(Hadron_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsKS = "(ALL)"
        _daughtersCutsL = "(M > %(Lambda_MassWindow_Lo)s*MeV) & (M<%(Lambda_MassWindow_Hi)s*MeV)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Lambda0"  : _daughtersCutsL,
            "KS0" : _daughtersCutsKS }

        #_Combine.CombinationCut   = _CombinationCut
        #_Combine.MotherCut        = _MotherCut

        #return Selection( name, Algorithm = _Combine, RequiredSelections = [ pions, protons ] )

        _sel_KLLLambdaLL = Selection(name+"_LLLL",
            RequiredSelections = [ lambdasLL, ksLL ],
            Algorithm = _Combine)

        _sel_KLLLambdaDD = Selection(name+"_DDLL",
            RequiredSelections = [ lambdasDD, ksLL ],
            Algorithm = _Combine)

        _sel_KLLLambdaLD = Selection(name+"_LDLL",
            RequiredSelections = [ lambdasLD, ksLL ],
            Algorithm = _Combine)

        _sel_KLDLambdaLL = Selection(name+"_LLLD",
            RequiredSelections = [ lambdasLL, ksLD ],
            Algorithm = _Combine)

        _sel_KLDLambdaDD = Selection(name+"_DDLD",
            RequiredSelections = [ lambdasDD, ksLD ],
            Algorithm = _Combine)

        _sel_KLDLambdaLD = Selection(name+"_LDLD",
            RequiredSelections = [ lambdasLD, ksLD ],
            Algorithm = _Combine)

        _sel_KDDLambdaLL = Selection(name+"_LLDD",
            RequiredSelections = [ lambdasLL, ksDD ],
            Algorithm = _Combine)

        _sel_KDDLambdaDD = Selection(name+"_DDDD",
            RequiredSelections = [ lambdasDD, ksDD ],
            Algorithm = _Combine)

        _sel_KDDLambdaLD = Selection(name+"_LDDD",
            RequiredSelections = [ lambdasLD, ksDD ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_KLLLambdaLL, _sel_KLLLambdaDD, _sel_KLLLambdaLD, _sel_KLDLambdaLL, _sel_KLDLambdaDD, _sel_KLDLambdaLD, _sel_KDDLambdaLL, _sel_KDDLambdaDD, _sel_KDDLambdaLD ])

        return _sel

#####################################################
    def _makeLamPiPi( self, name, lambdasLL, lambdasDD, lambdasLD, pipis, params ) :
        """
        Make an Lambda(1600)0 -> Lambda0 pi+ pi-
        Lambda(1600)0 is just a proxy to get the 3-body combination
        """

        _Decays = "[Lambda(1600)0 -> Lambda0 rho(770)0]cc"

         # define all the cuts

        _CombinationCut    = "(AM > %(Hadron_MassWindow_Lo)s*MeV) & (AM < %(Hadron_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsRho = "(ALL)" #"(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(MinProbNN)s) & (HASRICH)" % params""
        _daughtersCutsL = "(M > %(Lambda_MassWindow_Lo)s*MeV) & (M<%(Lambda_MassWindow_Hi)s*MeV)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Lambda0"  : _daughtersCutsL,
            "rho(770)0" : _daughtersCutsRho }

        #_Combine.CombinationCut   = _CombinationCut
        #_Combine.MotherCut        = _MotherCut

        #return Selection( name, Algorithm = _Combine, RequiredSelections = [ pions, protons ] )

        _sel_PiPiLambdaLL = Selection(name+"_LL",
            RequiredSelections = [ lambdasLL, pipis ],
            Algorithm = _Combine)

        _sel_PiPiLambdaDD = Selection(name+"_DD",
            RequiredSelections = [ lambdasDD, pipis ],
            Algorithm = _Combine)

        _sel_PiPiLambdaLD = Selection(name+"_LD",
            RequiredSelections = [ lambdasLD, pipis ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_PiPiLambdaLL, _sel_PiPiLambdaDD, _sel_PiPiLambdaLD ])

        return _sel

#####################################################
    def _makeLamPhi( self, name, lambdasLL, lambdasDD, lambdasLD, phis, params ) :
        """
        Make an Lambda(2100)0 -> Lambda0 phi
        Lambda(2100)0 is just a proxy to get the 2-body combination
        """

        _Decays = "[Lambda(2100)0 -> Lambda0 phi(1020)]cc"

         # define all the cuts

        _CombinationCut    = "(AM < %(Hadron_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsPhi = "(2 == NINTREE((ABSID==321)&(PROBNNk > %(MinProbNNTight)s)))" % params #"(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(MinProbNN)s) & (HASRICH)" % params""
        _daughtersCutsL = "(M > %(Lambda_MassWindow_Lo)s*MeV) & (M<%(Lambda_MassWindow_Hi)s*MeV)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Lambda0"  : _daughtersCutsL,
            "phi(1020)" : _daughtersCutsPhi }

        #_Combine.CombinationCut   = _CombinationCut
        #_Combine.MotherCut        = _MotherCut

        #return Selection( name, Algorithm = _Combine, RequiredSelections = [ pions, protons ] )

        _sel_PhiLambdaLL = Selection(name+"_LL",
            RequiredSelections = [ lambdasLL, phis ],
            Algorithm = _Combine)

        _sel_PhiLambdaDD = Selection(name+"_DD",
            RequiredSelections = [ lambdasDD, phis ],
            Algorithm = _Combine)

        _sel_PhiLambdaLD = Selection(name+"_LD",
            RequiredSelections = [ lambdasLD, phis ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_PhiLambdaLL, _sel_PhiLambdaDD, _sel_PhiLambdaLD ])

        return _sel

#####################################################
    def _makeXiPhi( self, name, xis, phis, params ) :
        """
        Make an Sigma(1940)- -> Xi- phi
        Sigma(1940)- is just a proxy to get the 2-body combination (must be an excited Xi, but they are not enough in ParticleTable)
        """

        _Decays = "[Sigma(1940)- -> Xi- phi(1020)]cc"

         # define all the cuts

        _CombinationCut    = "(AM < %(Hadron_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsPhi = "(2 == NINTREE((ABSID==321)&(PROBNNk > %(MinProbNNTight)s)))" % params #"(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(MinProbNN)s) & (HASRICH)" % params""
        _daughtersCutsX = "(ALL)"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Xi-"  : _daughtersCutsX,
            "phi(1020)" : _daughtersCutsPhi }

        _sel = Selection(name+"_all",
            RequiredSelections = [ xis, phis ],
            Algorithm = _Combine)

        return _sel

#####################################################
    def _makeOmegaPhi( self, name, omegas, phis, params ) :
        """
        Make an Sigma(2030)- -> Omega- phi
        Sigma(2030)- is just a proxy to get the 2-body combination (must be an excited Xi, but they are not enough in ParticleTable)
        """

        _Decays = "[Sigma(2030)- -> Omega- phi(1020)]cc"

         # define all the cuts

        _CombinationCut    = "(AM < %(Hadron_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsPhi = "(2 == NINTREE((ABSID==321)&(PROBNNk > %(MinProbNNTight)s)))" % params #"(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(MinProbNN)s) & (HASRICH)" % params""
        _daughtersCutsO = "(1 == NINTREE((ABSID==321)&(PIDK > 0)))"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Omega-"  : _daughtersCutsO,
            "phi(1020)" : _daughtersCutsPhi }

        _sel = Selection(name+"_all",
            RequiredSelections = [ omegas, phis ],
            Algorithm = _Combine)

        return _sel

#####################################################
    def _makeXiPiPi( self, name, xis, pipis, params ) :
        """
        Make an Xi(1820)- -> Xi- pi+ pi-
        Xi(1820)- is just a proxy to get the 3-body combination
        """

        _Decays = "[Xi(1820)- -> Xi- rho(770)0]cc"

         # define all the cuts

        _CombinationCut    = "(AM > %(Hadron_MassWindow_Lo)s*MeV) & (AM < %(Hadron_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsRho = "(ALL)"
        _daughtersCutsX = "(ALL)"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Xi-"  : _daughtersCutsX,
            "rho(770)0" : _daughtersCutsRho }


        _sel = Selection(name+"_all",
            RequiredSelections = [ xis, pipis ],
            Algorithm = _Combine)

        return _sel

#####################################################
    def _makeOmegaPiPi( self, name, omegas, pipis, params ) :
        """
        Make an Omega(2250)- -> Omega- pi+ pi-
        Omega(2250)- is just a proxy to get the 3-body combination
        """

        _Decays = "[Omega(2250)- -> Omega- rho(770)0]cc"

         # define all the cuts

        _CombinationCut    = "(AM > %(Hadron_MassWindow_Lo)s*MeV) & (AM < %(Hadron_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsRho = "(ALL)"
        _daughtersCutsO = "(1 == NINTREE((ABSID==321)&(PIDK > 0)))"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Omega-"  : _daughtersCutsO,
            "rho(770)0" : _daughtersCutsRho }


        _sel = Selection(name+"_all",
            RequiredSelections = [ omegas, pipis ],
            Algorithm = _Combine)

        return _sel

#####################################################
    def _makepKst( self, name, protons, kstars, params ) :
        """
        Make an Lambda(1800)0 -> p K*-
        Lambda(1800)0 is just a proxy to get the two-body combination
        """

        _Decays = "[Lambda(1800)0 -> p+ K*(892)-]cc"

         # define all the cuts

        _CombinationCut    = "(AM > %(Hadron_MassWindow_Lo)s*MeV) & (AM < %(Hadron_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsKst = "(ALL)"
        _daughtersCutsP = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNp > %(MinProbNNTight)s) & (P > 3500*MeV) & (HASRICH)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "p+"  : _daughtersCutsP,
            "K*(892)-" : _daughtersCutsKst }

        #_Combine.CombinationCut   = _CombinationCut
        #_Combine.MotherCut        = _MotherCut

        #return Selection( name, Algorithm = _Combine, RequiredSelections = [ pions, protons ] )

        _sel_pKst = Selection(name+"_pKst",
            RequiredSelections = [ protons, kstars ],
            Algorithm = _Combine)

        # _sel = MergedSelection(name+"_all",
        #     RequiredSelections = [ _sel_KLLLambdaLL, _sel_KLLLambdaDD, _sel_KLLLambdaLD, _sel_KLDLambdaLL, _sel_KLDLambdaDD, _sel_KLDLambdaLD, _sel_KDDLambdaLL, _sel_KDDLambdaDD, _sel_KDDLambdaLD ])

        return _sel_pKst

#####################################################
# builders for b->charm mumu
#####################################################

    def _makeXic02pKKpi(self, name, pions, kaons, protons, params):
        '''Makes Xic0 -> p K- K- pi+
           borrowed from Xib2Xic0PiXic02PKKPiBeauty2CharmLine, adapted a bit
        '''
        comboCuts12 = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)"% params
        comboCuts123 = "(ADOCA(1,3)<%(DiHadronADOCA)s*mm) & (ADOCA(2,3)<%(DiHadronADOCA)s*mm)"% params
        comboCuts = "(ASUM(PT)>1500*MeV) & (ADAMASS('Xi_c0') < 115*MeV) & (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > %(ProtonP)s *MeV)))) & (ADOCA(1,4)<%(DiHadronADOCA)s*mm) & (ADOCA(2,4)<%(DiHadronADOCA)s*mm) & (ADOCA(3,4)<%(DiHadronADOCA)s*mm)" % params
        momCuts = "(ADMASS('Xi_c0') < 100*MeV) & (VFASPF(VCHI2/VDOF)<%(Hadron_VtxChi2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.99) & (2 == NINTREE((ABSID==321) & (PROBNNk > %(MinProbNN)s))) & (1 == NINTREE((ABSID==2212)&(PROBNNp > %(MinProbNN)s)))" % params
        cp = DaVinci__N4BodyDecays(
            Combination12Cut=comboCuts12,
            Combination123Cut=comboCuts123,
            CombinationCut=comboCuts,
            MotherCut=momCuts,
            DecayDescriptors=["[Xi_c0 -> p+ K- K- pi+]cc"])
        cp.DaughtersCuts = {
            "p+"  : "(P > 2*GeV)",
            "K-"  : "(P > 2*GeV)",
            "pi+"  : "(P > 1*GeV)",
            }
        return Selection(
            name+'_Xic02PKKPi',
            Algorithm=cp,
            RequiredSelections=[pions, kaons, protons])
#####################################################
    def _makeLc2pKpi(self, name, pions, kaons, protons, params):
        '''Makes Lc -> p K pi '''
        comboCuts12 = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)"% params
        comboCuts = "(ASUM(PT)>1800*MeV) & (ADAMASS('Lambda_c+') < 115*MeV) & (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 400*MeV) & (P > 4000*MeV)))) & (ADOCA(1,3)<%(DiHadronADOCA)s*mm) & (ADOCA(2,3)<%(DiHadronADOCA)s*mm)" % params
        momCuts = "(ADMASS('Lambda_c+') < 100*MeV) & (VFASPF(VCHI2/VDOF)<%(Hadron_VtxChi2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.99) & (1 == NINTREE((ABSID==321) & (PROBNNk > %(MinProbNNTight)s))) & (1 == NINTREE((ABSID==2212)&(PROBNNp > %(MinProbNNTight)s)))" % params
        cp = DaVinci__N3BodyDecays(
            Combination12Cut=comboCuts12,
            CombinationCut=comboCuts,
            MotherCut=momCuts,
            DecayDescriptors=["[Lambda_c+ -> p+ K- pi+]cc"])
        cp.DaughtersCuts = {
            "p+"  : "(P > 2*GeV)",
            "K-"  : "(P > 2*GeV)",
            "pi+"  : "(P > 1*GeV)",
            }
        return Selection(
            name+'_Lc2PKPi',
            Algorithm=cp,
            RequiredSelections=[pions, kaons, protons])
#####################################################
    def _makeXic2pKpi(self, name, pions, kaons, protons, params):
        '''Makes Xic+ -> p K pi '''
        comboCuts12 = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)"% params
        comboCuts = "(ASUM(PT)>1800*MeV) & (ADAMASS('Xi_c+') < 115*MeV) & (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 400*MeV) & (P > 4000*MeV)))) & (ADOCA(1,3)<%(DiHadronADOCA)s*mm) & (ADOCA(2,3)<%(DiHadronADOCA)s*mm)" % params
        momCuts = "(ADMASS('Xi_c+') < 100*MeV) & (VFASPF(VCHI2/VDOF)<%(Hadron_VtxChi2)s) & (BPVVDCHI2>25) & (BPVDIRA>0.99) & (1 == NINTREE((ABSID==321) & (PROBNNk > %(MinProbNNTight)s))) & (1 == NINTREE((ABSID==2212)&(PROBNNp > %(MinProbNNTight)s)))" % params
        cp = DaVinci__N3BodyDecays(
            Combination12Cut=comboCuts12,
            CombinationCut=comboCuts,
            MotherCut=momCuts,
            DecayDescriptors=["[Xi_c+ -> p+ K- pi+]cc"])
        cp.DaughtersCuts = {
            "p+"  : "(P > 2*GeV)",
            "K-"  : "(P > 2*GeV)",
            "pi+"  : "(P > 1*GeV)",
            }
        return Selection(
            name+'_Xicp2PKPi',
            Algorithm=cp,
            RequiredSelections=[pions, kaons, protons])
#####################################################
    def _makeSigmac( self, name, lambdacs, pions, params):
        """
        Make a Sigmac0 -> Lc+ pi-
        """

        _Decays = "[Sigma_c0 -> Lambda_c+ pi-]cc"

        _CombinationCut = "(APT > 2*%(KaonPT)s *MeV) & " \
                          "(AM < %(Hadron_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < 10) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" #% params

        _LcCut = "(ALL)"
        _PionCut = "(PT > 100 *MeV) & " \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "Lambda_c+"  : _LcCut,
            "pi-"  : _PionCut
            }

        _sel_Sigmac = Selection(name+"_all",
            RequiredSelections = [ lambdacs, pions ],
            Algorithm = _Combine)

        #_sel = MergedSelection(name+"_all",
    #        RequiredSelections = [ _sel_KstarplusLL, _sel_KstarplusDD ])

        return _sel_Sigmac
#####################################################
    def _makeLcK( self, name, lambdacs, kaons, params):
        """
        Make a Xi_c(2790)0 -> Lc+ K-
        """

        _Decays = "[Xi_c(2790)0 -> Lambda_c+ K-]cc"

        _CombinationCut = "(APT > 2*%(KaonPT)s *MeV) & " \
                          "(AM < %(Hadron_MassWindow_Hi)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2>25) & (BPVDIRA>0.9)" #% params

        _LcCut = "(ALL)"
        _KaonCut = "(PT > 150 *MeV) & (PROBNNk > %(MinProbNNTight)s) &" \
                   "((ISBASIC) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "Lambda_c+"  : _LcCut,
            "K-"  : _KaonCut
            }

        _sel = Selection(name+"_all",
            RequiredSelections = [ lambdacs, kaons ],
            Algorithm = _Combine)

        #_sel = MergedSelection(name+"_all",
    #        RequiredSelections = [ _sel_KstarplusLL, _sel_KstarplusDD ])

        return _sel


#####################################################
#neutrals
#####################################################
    def _makeXi0( self, name, lambdasLL, lambdasDD, lambdasLD, pi0s, params ) :
        """
        Make an Xi0 -> Lambda0 pi0
        Xi0 is very long-lived, therefore pi0 is very displaced. Standard pi0 definitions do not suit us as they assume pi0 poiting to the PV. We use a cutsom very loose pi0 with extremely loose photon cut and very broad mass window.
        """

        _Decays = "[Xi0 -> Lambda0 pi0]cc"

         # define all the cuts

        _CombinationCut    = "(ADAMASS('Xi0') < 260.0*MeV)" #" & (AMAXDOCA('')< 2 *mm)" #% params
        _MotherCut  = "(ADMASS('Xi0') < 250.0*MeV) & (PT>200.0) & (MTDOCACHI2(1) < 16.0)" #% params #& (BPVLTIME()> 2.0 * ps)
        _daughtersCutsPi0 = "(PT>100 * MeV)" #"  & (2 == NINTREE((ABSID==22)&(CL>0.1)))" #% params
        _daughtersCutsL = "(M > %(Lambda_MassWindow_Lo)s*MeV) & (M<%(Lambda_MassWindow_Hi)s*MeV)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Lambda0"  : _daughtersCutsL,
            "pi0" : _daughtersCutsPi0 }


        _sel_PiLambdaLL = Selection(name+"_LL",
            RequiredSelections = [ lambdasLL, pi0s ],
            Algorithm = _Combine)

        _sel_PiLambdaDD = Selection(name+"_DD",
            RequiredSelections = [ lambdasDD, pi0s ],
            Algorithm = _Combine)

        _sel_PiLambdaLD = Selection(name+"_LD",
            RequiredSelections = [ lambdasLD, pi0s ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_PiLambdaLL, _sel_PiLambdaDD, _sel_PiLambdaLD ])

        return _sel


    def _makeXi0_DTF( self, name, lambdasLL, lambdasDD, lambdasLD, pi0s, params ) :
        """
        Make an Xi0 -> Lambda0 pi0 without vertex fit but with the DTF fit
        Xi0 is very long-lived, therefore pi0 is very displaced. Standard pi0 definitions do not suit us as they assume pi0 poiting to the PV. We use a cutsom very loose pi0 with extremely loose photon cut and very broad mass window.
        """

        _Decays = "[Xi0 -> Lambda0 pi0]cc"

         # define all the cuts

        _CombinationCut    = "(ADAMASS('Xi0') < 260.0*MeV)" #" & (AMAXDOCA('')< 2 *mm)" #% params
        _MotherCut  = "(DTF_FUN(M, True, strings(['pi0', 'Lambda0']) )>1200*MeV) & (DTF_FUN(M, True, strings(['pi0', 'Lambda0']) )<1450*MeV) & (PT>200.0) & (MTDOCACHI2(1) < 16.0) & (DTF_CHI2NDOF(True,strings(['pi0', 'Lambda0']))<9)" #% params
        #ok really the "True" is wrong here, but the DTF does not work oterhwise complaining it needs more geometrical constraints
        _daughtersCutsPi0 = "(PT>100 * MeV)" #"  & (2 == NINTREE((ABSID==22)&(CL>0.1)))" #% params
        _daughtersCutsL = "(M > %(Lambda_MassWindow_Lo)s*MeV) & (M<%(Lambda_MassWindow_Hi)s*MeV)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)
        _Combine.ParticleCombiners = {'' : 'MomentumCombiner:PUBLIC'} #do not run the default vertex fit
        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Lambda0"  : _daughtersCutsL,
            "pi0" : _daughtersCutsPi0 }


        _sel_PiLambdaLL = Selection(name+"_LL",
            RequiredSelections = [ lambdasLL, pi0s ],
            Algorithm = _Combine)

        _sel_PiLambdaDD = Selection(name+"_DD",
            RequiredSelections = [ lambdasDD, pi0s ],
            Algorithm = _Combine)

        _sel_PiLambdaLD = Selection(name+"_LD",
            RequiredSelections = [ lambdasLD, pi0s ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_PiLambdaLL, _sel_PiLambdaDD, _sel_PiLambdaLD ])

        return _sel


    # def _makeXi0_MTDOCA( self, name, lambdasLL, lambdasDD, lambdasLD, pi0s, params ) :
    #     """
    #     Make an Xi0 -> Lambda0 pi0
    #     Xi0 is very long-lived, therefore pi0 is very displaced. Standard pi0 definitions do not suit us as they assume pi0 poiting to the PV. We use a cutsom very loose pi0 with extremely loose photon cut and very broad mass window.
    #     """
    #
    #     _Decays = "[Xi0 -> Lambda0 pi0]cc"
    #
    #      # define all the cuts
    #
    #     _CombinationCut    = "(ADAMASS('Xi0') < 260.0*MeV)" #" & (AMAXDOCA('')< 2 *mm)" #% params
    #     _MotherCut  = "(ADMASS('Xi0') < 250.0*MeV) & (PT>200.0) & (MTDOCACHI2(1) < 16.0)" #% params #& (BPVLTIME()> 2.0 * ps)
    #     _daughtersCutsPi0 = "(PT>100 * MeV)" #"  & (2 == NINTREE((ABSID==22)&(CL>0.1)))" #% params
    #     _daughtersCutsL = "(M > %(Lambda_MassWindow_Lo)s*MeV) & (M<%(Lambda_MassWindow_Hi)s*MeV)" % params
    #
    #     _Combine = CombineParticles( DecayDescriptor = _Decays,
    #                                  CombinationCut  = _CombinationCut,
    #                                  MotherCut       = _MotherCut)
    #
    #     _Combine.DecayDescriptor = _Decays
    #
    #     _Combine.DaughtersCuts = {
    #         "Lambda0"  : _daughtersCutsL,
    #         "pi0" : _daughtersCutsPi0 }
    #
    #     #_Combine.CombinationCut   = _CombinationCut
    #     #_Combine.MotherCut        = _MotherCut
    #
    #     #return Selection( name, Algorithm = _Combine, RequiredSelections = [ pions, protons ] )
    #
    #     _sel_PiLambdaLL = Selection(name+"_LL",
    #         RequiredSelections = [ lambdasLL, pi0s ],
    #         Algorithm = _Combine)
    #
    #     _sel_PiLambdaDD = Selection(name+"_DD",
    #         RequiredSelections = [ lambdasDD, pi0s ],
    #         Algorithm = _Combine)
    #
    #     _sel_PiLambdaLD = Selection(name+"_LD",
    #         RequiredSelections = [ lambdasLD, pi0s ],
    #         Algorithm = _Combine)
    #
    #     _sel = MergedSelection(name+"_all",
    #         RequiredSelections = [ _sel_PiLambdaLL, _sel_PiLambdaDD, _sel_PiLambdaLD ])
    #
    #     return _sel


#####################################################
    def _makeSigmap( self, name, protonsL, protonsD, pi0s, params ) :
        """
        Make an Sigma+ -> p pi0
        Sigma+ is fairly long-lived, therefore pi0 is fairly displaced. It is also very soft. Standard pi0 definitions do not suit us as they assume pi0 poiting to the PV. We use a cutsom very loose pi0 with extremely loose photon cut and very broad mass window.
        """

        _Decays = "[Sigma+ -> p+ pi0]cc"

         # define all the cuts

        _CombinationCut    = "(ADAMASS('Sigma+') < 250.0*MeV)" #" & (AMAXDOCA('')< 1 *mm)" #% params
        _MotherCut  = "(ADMASS('Sigma+') < 240.0*MeV) & (PT>%(KaonPT)s*MeV) & (MTDOCACHI2(1) < 15.0)" % params #& (BPVLTIME()> 2.0 * ps)
        _daughtersCutsPi0 = "(PT>100 * MeV)" #"  & (2 == NINTREE((ABSID==22)&(CL>0.1)))" #% params
        _daughtersCutsP = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNp > 2*%(MinProbNNTight)s) & (P > 3000*MeV) & (PT>250*MeV) & (HASRICH) & (MIPCHI2DV(PRIMARY)>9)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "p+"  : _daughtersCutsP,
            "pi0" : _daughtersCutsPi0 }

        #_Combine.CombinationCut   = _CombinationCut
        #_Combine.MotherCut        = _MotherCut

        #return Selection( name, Algorithm = _Combine, RequiredSelections = [ pions, protons ] )

        _sel_PiPL = Selection(name+"_L",
            RequiredSelections = [ protonsL, pi0s ],
            Algorithm = _Combine)

        _sel_PiPD = Selection(name+"_D",
            RequiredSelections = [ protonsD, pi0s ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_PiPL, _sel_PiPD ])

        return _sel

#####################################################
    def _makeSigmap_DTF( self, name, protonsL, protonsD, pi0s, params ) :
        """
        Make an Sigma+ -> p pi0
        Sigma+ is fairly long-lived, therefore pi0 is fairly displaced. It is also very soft. Standard pi0 definitions do not suit us as they assume pi0 poiting to the PV. We use a cutsom very loose pi0 with extremely loose photon cut and very broad mass window.
        """

        _Decays = "[Sigma+ -> p+ pi0]cc"

         # define all the cuts

        _CombinationCut    = "(ADAMASS('Sigma+') < 350.0*MeV)" #" & (AMAXDOCA('')< 1 *mm)" #% params
        _MotherCut  = "(DTF_FUN(M, True, strings(['pi0']) )>1000*MeV) & (DTF_FUN(M, True, strings(['pi0']) )<1300*MeV) & (PT>200.0) & (MTDOCACHI2(1) < 16.0) & (DTF_CHI2NDOF(True,strings(['pi0']))<9)"
        #ok really the "True" is wrong here, but the DTF does not work oterhwise complaining it needs more geometrical constraints
        #_MotherCut  = "(ADMASS('Sigma+') < 240.0*MeV) & (PT>%(KaonPT)s*MeV) & (MTDOCACHI2(1) < 15.0)" % params #& (BPVLTIME()> 2.0 * ps)
        _daughtersCutsPi0 = "(PT>100 * MeV)" #"  & (2 == NINTREE((ABSID==22)&(CL>0.1)))" #% params
        _daughtersCutsP = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNp > 2*%(MinProbNNTight)s) & (P > 2000*MeV) & (PT>250*MeV) & (HASRICH) & (MIPCHI2DV(PRIMARY)>9)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)
        _Combine.ParticleCombiners = {'' : 'MomentumCombiner:PUBLIC'} #do not run the default vertex fit
        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "p+"  : _daughtersCutsP,
            "pi0" : _daughtersCutsPi0 }

        #_Combine.CombinationCut   = _CombinationCut
        #_Combine.MotherCut        = _MotherCut

        #return Selection( name, Algorithm = _Combine, RequiredSelections = [ pions, protons ] )

        _sel_PiPL = Selection(name+"_L",
            RequiredSelections = [ protonsL, pi0s ],
            Algorithm = _Combine)

        _sel_PiPD = Selection(name+"_D",
            RequiredSelections = [ protonsD, pi0s ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_PiPL, _sel_PiPD ])

        return _sel


#####################################################
    def _makeSigmapTight( self, name, protonsL, protonsD, pi0s, params ) :
        """
        Make an Sigma+ -> p pi0
        Sigma+ is fairly long-lived, therefore pi0 is fairly displaced. It is also very soft. Standard pi0 definitions do not suit us as they assume pi0 poiting to the PV. We use a cutsom very loose pi0 with extremely loose photon cut and very broad mass window.
        """

        _Decays = "[Sigma+ -> p+ pi0]cc"

         # define all the cuts

        _CombinationCut    = "(ADAMASS('Sigma+') < 210.0*MeV)" #" & (AMAXDOCA('')< 1 *mm)" #% params
        _MotherCut  = "(ADMASS('Sigma+') < 160.0*MeV) & (PT>500.0) & (MTDOCACHI2(1) < 9.0)" #% params #& (BPVLTIME()> 2.0 * ps)
        _daughtersCutsPi0 = "(PT>200 * MeV) & (2 == NINTREE((ABSID==22)&(CL>0.4)))" #% params
        _daughtersCutsP = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNp > 3*%(MinProbNNTight)s) & (P > 5000*MeV) & (PT>500*MeV) & (HASRICH) & (MIPCHI2DV(PRIMARY)>16)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "p+"  : _daughtersCutsP,
            "pi0" : _daughtersCutsPi0 }

        #_Combine.CombinationCut   = _CombinationCut
        #_Combine.MotherCut        = _MotherCut

        #return Selection( name, Algorithm = _Combine, RequiredSelections = [ pions, protons ] )

        _sel_PiPL = Selection(name+"_L",
            RequiredSelections = [ protonsL, pi0s ],
            Algorithm = _Combine)

        _sel_PiPD = Selection(name+"_D",
            RequiredSelections = [ protonsD, pi0s ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_PiPL, _sel_PiPD ])

        return _sel

#####################################################
    def _makeXi0K( self, name, xis, kaons, params ) :
        """
        Make an Sigma(2250)- -> Xi0 K-
        Sigma(2250)- is just a proxy to get the two-body combination
        It should be Omega but we don't have enough Omegas in ParticleTable.
        """

        _Decays = "[Sigma(2250)- -> Xi0 K-]cc"

         # define all the cuts
#
        _CombinationCut    = "(AM < 3500*MeV) & (AMAXDOCA('')< 1 *mm)" % params
        _MotherCut  = "(VFASPF(VCHI2PDOF) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsK = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNk > 2*%(MinProbNNTight)s) & (HASRICH) & (PT > 250*MeV)" % params
        _daughtersCutsXi = "(PT > 300*MeV) & (2 == NINTREE((ABSID==22)&(CL>0.4)))"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Xi0"  : _daughtersCutsXi,
            "K-" : _daughtersCutsK }


        _sel = Selection(name+"_all",
            RequiredSelections = [ xis, kaons ],
            Algorithm = _Combine)

        return _sel

#####################################################
    def _makeSigmaK( self, name, sigmas, kaons, params ) :
        """
        Make an Xi(2030)0 -> Sigma+ K-
        Xi(2030)0 is just a proxy to get the two-body combination
        """

        _Decays = "[Xi(2030)0 -> Sigma+ K-]cc"

         # define all the cuts

        _CombinationCut    = "(AM < 3000*MeV) & (AMAXDOCA('')< 1 *mm)" % params
        _MotherCut  = "(VFASPF(VCHI2PDOF) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsK = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNk > 2*%(MinProbNNTight)s) & (HASRICH) & (PT > 250*MeV)" % params
        _daughtersCutsSi = "(PT > 450*MeV) & (2 == NINTREE((ABSID==22)&(CL>0.4)))"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Sigma+"  : _daughtersCutsSi,
            "K-" : _daughtersCutsK }


        _sel = Selection(name+"_all",
            RequiredSelections = [ sigmas, kaons ],
            Algorithm = _Combine)

        return _sel

#####################################################
    def _makeSigmaPi( self, name, sigmas, pions, params ) :
        """
        Make an Sigma(1750)0 -> Sigma+ pi-
        Sigma(1750)0 is just a proxy to get the two-body combination
        """

        _Decays = "[Sigma(1750)0 -> Sigma+ pi-]cc"

         # define all the cuts

        _CombinationCut    = "(AM < 2900*MeV) & (AMAXDOCA('')< 1 *mm)" % params
        _MotherCut  = "(VFASPF(VCHI2PDOF) < %(Hadron_VtxChi2)s)" % params
        _daughtersCutsPi = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(MinProbNNTight)s) & (HASRICH) & (PT > 250*MeV)" % params
        _daughtersCutsSi = "(PT > 500*MeV) & (2 == NINTREE((ABSID==22)&(CL>0.4))) & (1 == NINTREE((ABSID==2212)&(PROBNNp>0.2)&(P>3000)))"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Sigma+"  : _daughtersCutsSi,
            "pi-" : _daughtersCutsPi }


        _sel = Selection(name+"_all",
            RequiredSelections = [ sigmas, pions ],
            Algorithm = _Combine)

        return _sel

#####################################################
    def _makeXi0K_DTF( self, name, xis, kaons, params ) :
        """
        Make an Sigma(2250)- -> Xi0 K-
        Sigma(2250)- is just a proxy to get the two-body combination
        It should be Omega but we don't have enough Omegas in ParticleTable.
        """

        _Decays = "[Sigma(2250)- -> Xi0 K-]cc"

         # define all the cuts
        #_MotherCut  = "(DTF_FUN(M, False, strings(['pi0']) )>1000*MeV) & (DTF_FUN(M, False, strings(['pi0']) )<1300*MeV) & (PT>200.0) & (MTDOCACHI2(1) < 16.0) & (DTF_CHI2NDOF(False,strings(['pi0']))<9)"
        _CombinationCut    = "(AM < 3500*MeV)"#" & (AMAXDOCA('')< 1 *mm)" % params
        _MotherCut  = "(DTF_FUN(VFASPF(VCHI2PDOF), True, strings(['pi0']) ) < 16) & (DTF_FUN(M, True, strings(['pi0']) ) < 3200*MeV)" % params
        _daughtersCutsK = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNk > 2*%(MinProbNNTight)s) & (HASRICH) & (PT > 250*MeV)" % params
        _daughtersCutsXi = "(DTF_FUN(PT, True, strings(['pi0']) ) > 300*MeV) & (2 == NINTREE((ABSID==22)&(CL>0.4)))"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)
        _Combine.ParticleCombiners = {'' : 'MomentumCombiner:PUBLIC'} #do not run the default vertex fit
        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Xi0"  : _daughtersCutsXi,
            "K-" : _daughtersCutsK }


        _sel = Selection(name+"_all",
            RequiredSelections = [ xis, kaons ],
            Algorithm = _Combine)

        return _sel

#####################################################
    def _makeSigmaK_DTF( self, name, sigmas, kaons, params ) :
        """
        Make an Xi(2030)0 -> Sigma+ K-
        Xi(2030)0 is just a proxy to get the two-body combination
        """

        _Decays = "[Xi(2030)0 -> Sigma+ K-]cc"

         # define all the cuts

        _CombinationCut    = "(AM < 3500*MeV)"# & (AMAXDOCA('')< 1 *mm)" % params
        _MotherCut  = "(DTF_FUN(VFASPF(VCHI2PDOF), True, strings(['pi0']) ) < 16) & (DTF_FUN(M, True, strings(['pi0']) ) < 3000*MeV)" % params
        _daughtersCutsK = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNk > 2*%(MinProbNNTight)s) & (HASRICH) & (PT > 250*MeV)" % params
        _daughtersCutsSi = "(DTF_FUN(PT, True, strings(['pi0']) ) > 500*MeV) & (2 == NINTREE((ABSID==22)&(CL>0.4))) & (DTF_FUN(MTDOCACHI2(1), True, strings(['pi0']) ) < 10.0)"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)
        _Combine.ParticleCombiners = {'' : 'MomentumCombiner:PUBLIC'} #do not run the default vertex fit
        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Sigma+"  : _daughtersCutsSi,
            "K-" : _daughtersCutsK }


        _sel = Selection(name+"_all",
            RequiredSelections = [ sigmas, kaons ],
            Algorithm = _Combine)

        return _sel

#####################################################
    def _makeSigmaPi_DTF( self, name, sigmas, pions, params ) :
        """
        Make an Sigma(1750)0 -> Sigma+ pi-
        Sigma(1750)0 is just a proxy to get the two-body combination
        """

        _Decays = "[Sigma(1750)0 -> Sigma+ pi-]cc"

         # define all the cuts

        _CombinationCut    = "(AM < 3000*MeV)"# & (AMAXDOCA('')< 1 *mm)" % params
        _MotherCut  = "(DTF_FUN(VFASPF(VCHI2PDOF), True, strings(['pi0']) ) < 12) & (DTF_FUN(M, True, strings(['pi0']) ) < 2300*MeV)" % params
        _daughtersCutsPi = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(MinProbNNTight)s) & (HASRICH) & (PT > 250*MeV)" % params
        _daughtersCutsSi = "(DTF_FUN(PT, True, strings(['pi0']) ) > 500*MeV) & (2 == NINTREE((ABSID==22)&(CL>0.4))) & (1 == NINTREE((ABSID==2212)&(PROBNNp>0.3)&(P>3500))) & (DTF_FUN(MTDOCACHI2(1), True, strings(['pi0']) ) < 10.0)"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)
        _Combine.ParticleCombiners = {'' : 'MomentumCombiner:PUBLIC'} #do not run the default vertex fit
        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Sigma+"  : _daughtersCutsSi,
            "pi-" : _daughtersCutsPi }


        _sel = Selection(name+"_all",
            RequiredSelections = [ sigmas, pions ],
            Algorithm = _Combine)

        return _sel

# added for 2018 restripping: partially reconstructed Xi or Omega
#####################################################
    def __XiPR__( self, name, protonsL, protonsD, pionsL, pionsD, params):
        """
        Make a Xi- -> p pi- (no soft pion from the Lambda).
        """

        _Decays = "[Xi- -> p+ pi-]cc"

        _CombinationCut = "(AM > %(XiPR_M_Lo_comb)s *MeV) & (AM < %(XiPR_M_Hi_comb)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2_PR)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2_PR)s) & (BPVDIRA > %(OmDIRA_PR)s) & (BPVLTIME() > 1.8*%(V0TAU)s * ps)" % params

        _DaughterCut_p = "(PT > 2*%(KaonPT)s *MeV) & (P > 1.4*%(ProtonP)s *MeV) & (MIPCHI2DV(PRIMARY) > 1.4*%(Hadron_MinIPCHI2)s) & " \
                   "((ISBASIC) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PIDp > %(MinDLLp)s)" % params
        _DaughterCut_pi = " (MIPCHI2DV(PRIMARY) > 1.6*%(Hadron_MinIPCHI2)s) & " \
                   "((ISBASIC) & (TRGHOSTPROB < %(Trk_GhostProb)s)) " % params


        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+" : _DaughterCut_p,
            "pi-" : _DaughterCut_pi
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protonsL, protonsD, pionsL, pionsD ] )
#####################################################
    def __OmegaPR__( self, name, protonsL, protonsD, kaonsL, kaonsD, params):
        """
        Make a Omega- -> p K- (no soft pion from the Lambda).
        """

        _Decays = "[Omega- -> p+ K-]cc"

        _CombinationCut = "(AM > %(OmegaPR_M_Lo_comb)s *MeV) & (AM < %(OmegaPR_M_Hi_comb)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2_PR)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2_PR)s) & (BPVDIRA > %(OmDIRA_PR)s) & (BPVLTIME() > 1.6*%(V0TAU)s * ps)" % params

        _DaughterCut_p = "(PT > 1.6*%(KaonPT)s *MeV) & (P > 1.2*%(ProtonP)s *MeV) & (MIPCHI2DV(PRIMARY) > 1.2*%(Hadron_MinIPCHI2)s) & " \
                   "((ISBASIC) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PIDp > %(MinDLLp)s)" % params
        _DaughterCut_K = " (MIPCHI2DV(PRIMARY) > 1.2*%(Hadron_MinIPCHI2)s) & " \
                   "((ISBASIC) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PIDK > %(MinDLLK)s)" % params


        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+" : _DaughterCut_p,
            "K-" : _DaughterCut_K
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protonsL, protonsD, kaonsL, kaonsD ] )



#####################################################
# final builder
#####################################################
    def _makeB2LLX( self, name, dilepton, hadrons, params, masscut ):
        """
        CombineParticles / Selection for the B baryons
        """

        _Decays = [
                    "[ Xi_b- -> J/psi(1S) Xi(1690)- ]cc",
                    "[ Xi_b0 -> J/psi(1S) Xi(1690)0 ]cc",
                    "[ Xi_b- -> J/psi(1S) Xi(1950)- ]cc",
                    "[ Xi_b0 -> J/psi(1S) Xi(1950)0 ]cc",
                    "[ Xi_b- -> J/psi(1S) Xi(1820)- ]cc",
                    "[ Xi_b0 -> J/psi(1S) Xi*0 ]cc",
                    "[ Omega_b- -> J/psi(1S) Omega(2250)- ]cc",
                    "[ Omega_b- -> J/psi(1S) Xi(2030)- ]cc",
                    "[ Lambda_b0 -> J/psi(1S) Lambda(1600)0 ]cc",
                    "[ Lambda_b0 -> J/psi(1S) Lambda(1800)0 ]cc",
                    "[ Lambda_b0 -> J/psi(1S) Lambda(1830)0 ]cc",
                    "[ Lambda_b0 -> J/psi(1S) Lambda(2100)0 ]cc", # Lb -> L phi mumu
                    "[ Lambda_b0 -> J/psi(1S) Sigma(1915)0 ]cc",
                    "[ Xi_b- -> J/psi(1S) Sigma(1940)- ]cc", # Xib -> Xi phi mumu
                    "[ Omega_b- -> J/psi(1S) Sigma(2030)- ]cc", # Omegab -> Omega phi mumu
                  ]


        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
#####################################################
    def _makeB2LLX_charm( self, name, dilepton, hadrons, params, masscut ):
        """
        CombineParticles / Selection for the B baryons
        """

        _Decays = [
                    "[ Xi_b0 -> J/psi(1S) Xi_c0 ]cc",
                    "[ Lambda_b0 -> J/psi(1S) Sigma_c0 ]cc",
                    "[ Xi_bc+ -> J/psi(1S) Lambda_c+ ]cc",
                    "[ Xi_bc+ -> J/psi(1S) Xi_c+ ]cc",
                    "[ Xi_b0 -> J/psi(1S) Xi_c(2790)0 ]cc",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2Ch)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
####################################################
    def _makeB2LLX_neutral( self, name, dilepton, hadrons, params, masscut  ):
        """
        CombineParticles / Selection for the B baryons
        """

        _Decays = [
                    "[ Xi_b0 -> J/psi(1S) Xi0 ]cc",
                    "[ Xi_b0 -> J/psi(1S) Xi(2030)0 ]cc",
                    "[Omega_b- -> J/psi(1S) Sigma(2250)-]cc",
                    "[Lambda_b0 -> J/psi(1S) Sigma(1750)0]cc",
                    "[Xi_bc+ -> J/psi(1S) Sigma+]cc",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2Loose)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
####################################################
    def _makeB2LLX_neutral_DTF( self, name, dilepton, hadrons, params, masscut  ):
        """
        CombineParticles / Selection for the B baryons
        """

        _Decays = [
                    "[ Xi_b0 -> J/psi(1S) Xi0 ]cc",
                    "[ Xi_b0 -> J/psi(1S) Xi(2030)0 ]cc",
                    "[Omega_b- -> J/psi(1S) Sigma(2250)-]cc",
                    "[Lambda_b0 -> J/psi(1S) Sigma(1750)0]cc",
                    #"[Xi_bc+ -> J/psi(1S) Sigma+]cc",
                  ]

        _Cut = "((DTF_FUN(VFASPF(VCHI2/VDOF), True, strings(['pi0']) ) < %(BVertexCHI2)s) "\
               "& (DTF_FUN(BPVIPCHI2(), True, strings(['pi0']) ) < %(BIPCHI2)s) "\
               "& (DTF_FUN(BPVDIRA, True, strings(['pi0']) ) > %(BDIRA)s) "\
               "& (DTF_FUN(BPVVDCHI2, True, strings(['pi0']) ) > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )
        _Combine.ParticleCombiners = {'' : 'MomentumCombiner:PUBLIC'} #do not run the default vertex fit
        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
#####################################################
    def _makeB2LLX_ground( self, name, dilepton, hadrons, params, masscut  ):
        """
        CombineParticles / Selection for the B baryons
        """

        _Decays = [
                    "[ Xi_b- -> J/psi(1S) Xi- ]cc",
                    "[Omega_b- -> J/psi(1S) Omega-]cc"
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2Loose)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2Ch)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
#####################################################
# added for the 2018 restripping
    def _makeB2LLX_ground_PR( self, name, dilepton, hadrons, params, masscut  ):
        """
        CombineParticles / Selection for the B baryons
        """

        _Decays = [
                    "[ Xi_b- -> J/psi(1S) Xi- ]cc",
                    "[Omega_b- -> J/psi(1S) Omega-]cc"
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVtxCHI2_PR)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2_PR)s) "\
               "& (BPVDIRA > %(BDIRA_PR)s) "\
               "& (2 == NINTREE((ABSID<14)&((ISMUON) | (PIDe>2))))"\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
#####################################################
