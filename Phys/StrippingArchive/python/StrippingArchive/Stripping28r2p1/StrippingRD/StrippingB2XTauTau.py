###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = [' L. Pescatore', 'F. Blanc']
__date__ = '12/03/2019'
__version__ = '$Revision: 0.1$'

# Stripping line for B->Htautau

from GaudiKernel.SystemOfUnits import MeV
from GaudiKernel.SystemOfUnits import mm
"""
  B->KstarTauTau, B->KTauTau, B->PhiTauTau, B->Eta'TauTau
"""
__all__ = ('B2XTauTauConf', 'getRelInfoB2XTauTau', 'default_config')

default_config = {
    'NAME': 'B2XTauTau',
    'BUILDERTYPE': 'B2XTauTauConf',
    'WGs': ['RD'],
    'CONFIG': {
        'SpdMult': '600',
        #
        'UsePID': True,
        #
        'Photon_Res_PT_Min': 400.0 * MeV,
        'Photon_CL_Min': 0.2,
        #
        'FD_B_Max': 100,
        'PT_B': 2000 * MeV,
        'P_B': 10000 * MeV,
        'FDCHI2_B': 16,
        'FDCHI2_BK': 16,
        'MASS_LOW_B': 2000 * MeV,
        'MASS_LOW_BK': 3000 * MeV,
        'MASS_HIGH_B': 6000 * MeV,
        #
        'VCHI2_Eta': 9,
        'VCHI2_Rho': 9,
        'VCHI2_K1': 9,
        'PT_Rho': 800 * MeV,
        'PT_K1': 800 * MeV,
        'PT_K': 800 * MeV,
        'PT_Etap': 800 * MeV,
        'P_K': 3000 * MeV,
        'IPCHI2_Tr': 16,
        'TRACKCHI2_Tr': 6,
        'TRGHOPROB_Tr': 0.5,
        'MASS_LOW_Rho': 280 * MeV,
        'MASS_HIGH_Rho': 1100 * MeV,
        'MASS_LOW_K1': 400 * MeV,
        'MASS_HIGH_K1': 1700 * MeV,
        'MASS_LOW_Etap': 800 * MeV,
        'MASS_HIGH_Etap': 1100 * MeV,
        'MASS_HIGH_Tau': 1800 * MeV,
        #
        'B2HTauTau_LinePrescale': 1,
        'B2HTauTau_LinePostscale': 1,
    },
    'STREAMS': ['Bhadron']
}

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, OfflineVertexFitter, DaVinci__N3BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdNoPIDsPions, StdLoosePions
from StandardParticles import StdLooseKaons, StdNoPIDsKaons
from StandardParticles import StdLooseAllPhotons


class B2XTauTauConf(LineBuilder):
    """
      Builder for B->KstarTauTau, B->KTauTau, B->PhiTauTau, B->Eta'TauTau
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        trackCuts = "(MIPCHI2DV(PRIMARY) > %(IPCHI2_Tr)s) & (TRCHI2DOF < %(TRACKCHI2_Tr)s) & (TRGHOSTPROB < %(TRGHOPROB_Tr)s)" % config
        KCuts = trackCuts + " & (PT > %(PT_K)s) & (P > %(P_K)s)" % config
        if config['UsePID']: KCuts += " & (PROBNNk>0.2)"

        TauCuts = "(M < %(MASS_HIGH_Tau)s)" % config

        self.FilterSPD = {
            'Code':
            " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )"
            % config,
            'Preambulo': [
                "from LoKiNumbers.decorators import *",
                "from LoKiCore.basic import LHCb"
            ]
        }

        self.rawTau = DataOnDemand("Phys/StdTightDetachedTau3pi/Particles")
        self.selTau = SimpleSelection(
            "Tau" + name, FilterDesktop, [self.rawTau], Code=TauCuts)

        self.selKaon = SimpleSelection(
            "Kaon" + name, FilterDesktop, [StdLooseKaons], Code=KCuts)
        self.selTracks = SimpleSelection(
            "Tracks" + name, FilterDesktop, [StdNoPIDsPions], Code=trackCuts)

        self.selRho = self._makeRho("Rho" + name, trackCuts, config)
        self.selK1 = self._makeK1("K1" + name, self.selTracks, config)
        self.selEtap = self._makeEtap("Etap" + name, trackCuts, config)
        #      self.selKstar      = self._makeKstar("Kstar"+name, trackCuts, config)
        #      self.selPhi        = self._makePhi( "Phi"+name, trackCuts, config )

        self.selB2RhoTauTau = self._makeBd2HTauTau(name + "_Rho", self.selTau,
                                                   self.selRho, config)
        self.selB2KTauTau = self._makeBu2KTauTau(name + "_K", self.selTau,
                                                 self.selKaon, config)
        self.selB2K1TauTau = self._makeBu2K1TauTau(name + "_K1", self.selTau,
                                                   self.selK1, config)
        self.selB2EtapTauTau = self._makeBd2HTauTau(
            name + "_Etap", self.selTau, self.selEtap, config)
        #      self.selB2KstTauTau    = self._makeBd2HTauTau( name+"_Kstar", self.selTau, self.selKstar, config )
        #      self.selB2PhiTauTau    = self._makeBd2HTauTau( name+"_Phi", self.selTau, self.selPhi, config )
        self.selB2RhoTauTauSS = self._makeBd2HTauTau(
            name + "_Rho", self.selTau, self.selRho, config, SS=True)
        self.selB2KTauTauSS = self._makeBu2KTauTau(
            name + "_K", self.selTau, self.selKaon, config, SS=True)
        self.selB2K1TauTauSS = self._makeBu2K1TauTau(
            name + "_K1", self.selTau, self.selK1, config, SS=True)
        self.selB2EtapTauTauSS = self._makeBd2HTauTau(
            name + "_Etap", self.selTau, self.selEtap, config, SS=True)
        #      self.selB2KstTauTauSS  = self._makeBd2HTauTau( name+"_Kstar", self.selTau, self.selKstar, config, SS = True )
        #      self.selB2PhiTauTauSS  = self._makeBd2HTauTau( name+"_Phi", self.selTau, self.selPhi, config, SS = True )

        ## Finished making selections build and register lines

        self.RhoTauTau_Line = self._makeLine("B2RhoTauTauLine",
                                             self.selB2RhoTauTau, config)
        self.KTauTau_Line = self._makeLine("B2KTauTauLine", self.selB2KTauTau,
                                           config)
        self.K1TauTau_Line = self._makeLine("B2K1TauTauLine",
                                            self.selB2K1TauTau, config)
        self.EtapTauTau_Line = self._makeLine("B2EtapTauTauLine",
                                              self.selB2EtapTauTau, config)
        #      self.KstarTauTau_Line   = self._makeLine("B2KstarTauTauLine", self.selB2KstTauTau, config)
        #      self.PhiTauTau_Line     = self._makeLine("B2PhiTauTauLine", self.selB2PhiTauTau, config)
        self.RhoTauTauSS_Line = self._makeLine("B2RhoTauTauSSLine",
                                               self.selB2RhoTauTauSS, config)
        self.KTauTauSS_Line = self._makeLine("B2KTauTauSSLine",
                                             self.selB2KTauTauSS, config)
        self.K1TauTauSS_Line = self._makeLine("B2K1TauTauSSLine",
                                              self.selB2K1TauTauSS, config)
        self.EtapTauTauSS_Line = self._makeLine("B2EtapTauTauSSLine",
                                                self.selB2EtapTauTauSS, config)
#      self.KstarTauTauSS_Line = self._makeLine("B2KstarTauTauSSLine", self.selB2KstTauTauSS, config)
#      self.PhiTauTauSS_Line   = self._makeLine("B2PhiTauTauSSLine", self.selB2PhiTauTauSS, config)

#### Make resonances ###################################################

    def _makeRho(self, name, trackSel, config):

        combcut = "in_range ( %(MASS_LOW_Rho)s, AM, %(MASS_HIGH_Rho)s )" % config
        mothercut = " (PT > %(PT_Rho)s) & (VFASPF(VCHI2) < %(VCHI2_Rho)s)" % config

        daucut = {'pi+': trackSel, 'pi-': trackSel}

        return SimpleSelection(
            name + "_Rho",
            CombineParticles, [StdNoPIDsPions],
            DecayDescriptors=["rho(770)0 -> pi+ pi-"],
            CombinationCut=combcut,
            MotherCut=mothercut,
            DaughtersCuts=daucut)

    def _makeK1(self, name, tracks, config):

        combcut = "in_range ( %(MASS_LOW_K1)s, AM, %(MASS_HIGH_K1)s )" % config
        mothercut = " (PT > %(PT_K1)s) & (VFASPF(VCHI2) < %(VCHI2_K1)s)" % config

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=["[K_1(1270)+ -> pi+ pi- pi+]cc"],
            Combination12Cut="AM < 1700",
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            name + "_K1", Algorithm=Combine, RequiredSelections=[tracks])

    def _makeEtap(self, name, trackSel, config):

        combcut = "(APT > %(PT_Etap)s) & (in_range ( %(MASS_LOW_Etap)s, AM, %(MASS_HIGH_Etap)s))" % config
        mothercut = "(VFASPF(VCHI2/VDOF) < %(VCHI2_Eta)s)" % config
        gammacut = "(PT > %(Photon_Res_PT_Min)s) & (CL > %(Photon_CL_Min)s)" % config

        Picuts = trackSel
        if config['UsePID']:
            Picuts += ' & (PROBNNpi>0.2)'
        daucut = {'pi+': Picuts, 'pi-': Picuts, 'gamma': gammacut}

        return SimpleSelection(
            name + "_Etap",
            CombineParticles, [StdLoosePions, StdLooseAllPhotons],
            DecayDescriptors=["eta_prime -> pi+ pi- gamma"],
            CombinationCut=combcut,
            MotherCut=mothercut,
            DaughtersCuts=daucut)

    def _makeKstar(self, name, trackSel, config):

        combcut = "in_range ( %(MASS_LOW_Kst)s, AM, %(MASS_HIGH_Kst)s )" % config
        mothercut = " (PT > %(PT_Kst)s) & (VFASPF(VCHI2) < %(VCHI2_Kst)s) & (BPVVD > %(FD_Kst_Mu_KMM)s) " % config

        Kcuts = trackSel
        Picuts = trackSel
        if config['UsePID']:
            Kcuts += ' & (PROBNNk>0.2)'
            Picuts += ' & (PROBNNpi>0.2)'

        daucut = {'K+': Kcuts, 'pi-': Picuts}

        return SimpleSelection(
            name + "_Kstar",
            CombineParticles, [StdLoosePions, StdNoPIDsKaons],
            DecayDescriptors=["[K*(892)0 -> K+ pi-]cc"],
            CombinationCut=combcut,
            MotherCut=mothercut,
            DaughtersCuts=daucut)

    def _makePhi(self, name, trackSel, config):

        combcut = "in_range ( %(MASS_LOW_Phi)s, AM, %(MASS_HIGH_Phi)s )" % config
        mothercut = " (PT > %(PT_Phi)s) & (VFASPF(VCHI2) < %(VCHI2_Phi)s)  " % config

        Kcuts = trackSel
        if config['UsePID']: Kcuts += ' & (PROBNNk>0.2)'

        daucut = {'K+': Kcuts, 'K-': Kcuts}

        return SimpleSelection(
            name,
            CombineParticles, [StdLooseKaons],
            DecayDescriptors=["phi(1020) -> K+ K-"],
            CombinationCut=combcut,
            MotherCut=mothercut,
            DaughtersCuts=daucut)

#### Make B ###################################################

    def _makeBd2HTauTau(self, name, tauSel, HSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config

        part = "eta_prime"
        n = name + "TauTau"
        if "Phi" in name: part = "phi(1020)"
        elif "Kstar" in name: part = "K*(892)0"
        elif "Rho" in name: part = "rho(770)0"

        descriptors = ["B_s0 -> %s tau+ tau-" % part]

        if "Kstar" in name: descriptors = ["[B_s0 -> %s tau+ tau-]cc" % part]

        if SS:
            n += "SS"
            descriptors = [
                "B_s0 -> %s tau+ tau+" % part,
                "B_s0 -> %s tau- tau-" % part
            ]
            if "Kstar" in name:
                descriptors = [
                    "[B_s0 -> %s tau+ tau+]cc" % part,
                    "[B_s0 -> %s tau- tau-]cc" % part
                ]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM < 6000",
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            n, Algorithm=Combine, RequiredSelections=[tauSel, HSel])

    def _makeBu2KTauTau(self, name, tauSel, KSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_BK)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_BK)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s)" % config

        n = name + "TauTau"
        descriptors = ["[B+ -> K+ tau+ tau-]cc"]
        if SS:
            n = name + "TauTauSS"
            descriptors = ["[B+ -> K- tau+ tau+]cc", "[B+ -> K+ tau+ tau+]cc"]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM<6000",
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            n, Algorithm=Combine, RequiredSelections=[tauSel, KSel])

    def _makeBu2K1TauTau(self, name, tauSel, K1Sel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_BK)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s)" % config

        n = name + "TauTau"
        descriptors = ["[B+ -> K_1(1270)+ tau+ tau-]cc"]
        if SS:
            n = name + "TauTauSS"
            descriptors = [
                "[B+ -> K_1(1270)- tau+ tau+]cc",
                "[B+ -> K_1(1270)+ tau+ tau+]cc"
            ]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM<6000",
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            n, Algorithm=Combine, RequiredSelections=[tauSel, K1Sel])

#### Helpers to make lines

    def _makeLine(self, name, sel, config):

        line = StrippingLine(
            name,
            prescale=config['B2HTauTau_LinePrescale'],
            postscale=config['B2HTauTau_LinePostscale'],
            #MDSTFlag = False,
            FILTER=self.FilterSPD,
            RelatedInfoTools=getRelInfoB2XTauTau(),
            selection=sel,
            MaxCandidates=50)
        self.registerLine(line)
        return line


#### Related Info
def getRelInfoB2XTauTau():
    relInfo = []
    for coneAngle in [0.5, 0.8, 1.0, 1.3, 1.5]:
        conestr = str(coneAngle).replace('.', '')
        relInfo += [{
            "Type":
            "RelInfoConeVariables",
            "IgnoreUnmatchedDescriptors":
            True,
            "ConeAngle":
            coneAngle,
            "Location":
            "VertexConeInfo",
            "Variables": [
                'CONEANGLE', 'CONEMULT', 'CONEPASYM', 'CONEPTASYM',
                'CONEDELTAETA'
            ],
            "DaughterLocations": {
                "^[Beauty -> Hadron  l  l]CC": 'P2ConeVar%s_B' % conestr,
                "[Beauty -> ^Hadron  l  l]CC": 'P2ConeVar%s_X' % conestr,
                "[Beauty ->  Hadron ^l  l]CC": 'P2ConeVar%s_Tau1' % conestr,
                "[Beauty ->  Hadron  l ^l]CC": 'P2ConeVar%s_Tau2' % conestr
            }
        }]
        relInfo += [{
            "Type": "RelInfoVertexIsolation",
            "Location": "VertexIsoInfo"
        }]
        return relInfo
