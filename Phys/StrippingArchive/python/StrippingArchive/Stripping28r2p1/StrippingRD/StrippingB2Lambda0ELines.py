###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
B->Lambda0 E  reconstruction
"""

__author__ = ['Federico Redi']
__date__ = '08/04/2015'
__version__ = '$Revision: 0.2 $'

__all__ = ('B2Lambda0ELines',
           '_Bu2LambdaSSE',
           'default_config')

from StandardParticles import StdLoosePions, StdLooseElectrons, StdNoPIDsDownElectrons, StdNoPIDsDownPions
from Configurables import TisTosParticleTagger
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from Gaudi.Configuration import *
from PhysSelPython.Wrappers import DataOnDemand, Selection, MergedSelection

default_config = {
    'NAME'        : 'B2Lambda0E',
    'WGs'         : ['RD'],
    'BUILDERTYPE' : 'B2Lambda0ELines',
    'CONFIG'      : { "GEC_nLongTrk"          : 300.  ,#adimensional
                      #Electrons
                      "ElectronGHOSTPROB"         : 0.5   ,#adimensional
                      "ElectronTRCHI2"            : 4.    ,#adimensional
                      "ElectronP"                 : 3000. ,#MeV
                      "ElectronPT"                : 250.  ,#MeV
                      "ElectronPIDK"              : 3.0    ,#adimensional
                      "ElectronPIDpi"             : 3.0    ,#adimensional
                      "ElectronPIDp"              : 3.0    ,#adimensional
                      "ElectronMINIPCHI2"         : 12    ,#adminensional
                      #Lambda Daughter Cuts
                      "Lambda0DaugP"          : 2000. ,#MeV
                      "Lambda0DaugPT"         : 250.  ,#MeV
                      "Lambda0DaugTrackChi2"  : 4.    ,#adimensional
                      "Lambda0DaugMIPChi2"    : 10.   ,#adimensional
                      #Lambda cuts
                      "MajoranaCutFDChi2"   : 100.  ,#adimensional
                      "MajoranaCutM"        : 1500. ,#MeV
                      "Lambda0VertexChi2"     : 10.   ,#adimensional
                      "Lambda0PT"             : 700.  ,#adimensional
                      #B Mother Cuts
                      "BVCHI2DOF"             : 4.    ,#adminensional
                      "BDIRA"                 : 0.99  ,#adminensional
                      "LambdaEMassLowTight"  : 1500. ,#MeV
                      "XEMassUpperHigh"      : 6500. ,#MeV
                      'LambdaZ'               : -1.     #mm
                      } ,
    'STREAMS'     : ['EW']
    }

class B2Lambda0ELines( LineBuilder ) :
    """Definition of B ->Lambda0 E (Lambda0->EPi) stripping"""

    __configuration_keys__ = default_config['CONFIG'].keys()


    def __init__(self,name,config):
        LineBuilder.__init__(self, name, config)

        self._stdLooseKsLL = DataOnDemand("Phys/StdLooseKsLL/Particles")
        self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}
        self._electronSel=None
        self._downelectronSel=None
        self._electronFilter()
        self._selE=None
        self.registerLine(self._LambdaMajoranaSSE_line())

    def _NominalESelection( self ):
        return "(TRCHI2DOF < %(ElectronTRCHI2)s ) &  (P> %(ElectronP)s *MeV) &  (PT> %(ElectronPT)s* MeV)"\
               "& (TRGHOSTPROB < %(ElectronGHOSTPROB)s)"\
               "& (PIDe-PIDpi> %(ElectronPIDpi)s )"\
               "& (PIDe-PIDp> %(ElectronPIDp)s )"\
               "& (PIDe-PIDK> %(ElectronPIDK)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(ElectronMINIPCHI2)s )"

    ######LambdaortMajoranaLine######
    def _LambdaMajoranaSSE_line( self ):
        return StrippingLine(self._name+'Bu2LambdaSSELine', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bu2LambdaSSE()])

    ######--######
    def _downElectronFilter( self ):
        if self._downelectronSel is not None:
            return self._downelectronSel

        _e = FilterDesktop( Code = self._NominalESelection() % self._config )
        _eSel=Selection("downElectron_for"+self._name,
                         Algorithm=_e,
                         RequiredSelections = [StdNoPIDsDownElectrons])
        self._downelectronSel=_eSel
        return _eSel

    def _electronFilter( self ):
        if self._electronSel is not None:
            return self._electronSel

        _e = FilterDesktop( Code = self._NominalESelection() % self._config )
        _eSel=Selection("Electron_for"+self._name,
                         Algorithm=_e,
                         RequiredSelections = [StdLooseElectrons])
        self._electronSel=_eSel
        return _eSel

    ######--######

    def _LambdaMajoranaEFilter( self ):
        if self._selE is not None:
          return self._selE

        _Lambda = CombineParticles(
            DecayDescriptors = ["[Lambda0 -> e- pi+]cc"],
            DaughtersCuts   = {"pi+":"(P > %(Lambda0DaugP)s)& (PT > %(Lambda0DaugPT)s)"\
                               "& (TRCHI2DOF < %(Lambda0DaugTrackChi2)s)" \
                               "& (MIPCHI2DV(PRIMARY) > %(Lambda0DaugMIPChi2)s)"   % self._config
                               },
            CombinationCut  = "(ADOCACHI2CUT(25, ''))"% self._config,
            MotherCut       = "( M > %(MajoranaCutM)s*MeV )&( BPVVDCHI2 > %(MajoranaCutFDChi2)s )&( VFASPF(VCHI2/VDOF) < %(Lambda0VertexChi2)s )&( PT > %(Lambda0PT)s*MeV )" % self._config
            )
        _LambdaMajoranaSSESel=Selection("LambdaMajoranaSSE_for"+self._name,
                                      Algorithm=_Lambda,
                                      RequiredSelections = [StdLoosePions, self._electronFilter()])
        _downLambdaMajoranaSSESel=Selection("downLambdaMajoranaSSE_for"+self._name,
                                      Algorithm=_Lambda,
                                      RequiredSelections = [StdNoPIDsDownPions, self._downElectronFilter()])
        self._LambdaMajoranaSSESel=_LambdaMajoranaSSESel
        self._downLambdaMajoranaSSESel=_downLambdaMajoranaSSESel
        _selE = MergedSelection("Selection_"+self._name+"_LambdaMajoranaSSE",
                                      RequiredSelections = [ _LambdaMajoranaSSESel, _downLambdaMajoranaSSESel ])
        return _selE


    ######Bu->Lambdae SS & OS######
    def _Bu2LambdaSSE( self ):
        _LambdaSSE_SSE = CombineParticles(
            DecayDescriptors = ["[B- -> Lambda0 e-]cc"],
            CombinationCut = "(AM>%(LambdaEMassLowTight)s*MeV) & (AM<%(XEMassUpperHigh)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)"\
            "& ( MINTREE((ABSID=='Lambda0'),VFASPF(VZ)) - VFASPF(VZ) > %(LambdaZ)s *mm )" % self._config,
            ReFitPVs = True
            )
        _LambdaSSE_SSESel=Selection("LambdaSSE_SSE_for"+self._name,
                              Algorithm=_LambdaSSE_SSE,
                              RequiredSelections = [self._electronFilter(), self._LambdaMajoranaEFilter()])
        return _LambdaSSE_SSESel

# EOF
