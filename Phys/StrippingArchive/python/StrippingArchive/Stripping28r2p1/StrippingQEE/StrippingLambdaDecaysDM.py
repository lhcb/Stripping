###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""
Lines for searches on DM candidates produced from Lambda decays (arXiv:2101.02706).
"""

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from CommonParticles.Utils import *
from CommonParticles import StdAllNoPIDsPions
from StandardParticles import StdAllLooseKaons, StdAllLooseProtons, StdAllLoosePions

from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, picosecond, millimeter

__author__  = ['Xabier Cid Vidal', 'Carlos Vazquez Sierra', 'Saul Lopez Solino']
__date__    = '18/03/2021'
__all__     = 'LambdaDecaysDMConf', 'default_config'

default_config = {
  'NAME'        : 'LambdaDecaysDM',
  'BUILDERTYPE' : 'LambdaDecaysDMConf',
  'WGs'         : [ 'QEE' ],
  'STREAMS'     : [ 'BhadronCompleteEvent' ],
  'CONFIG'      : {
    'Common': {
              'checkPV'        : False
              },
    'StrippingDMLambda1520Line': {
              'Prescale'            : 1.0,
              'Postscale'           : 1.0,
              'Prescale_control'    : 0.1,
              'Postscale_control'   : 1.0,
              'Lambda1520_PT'       : 1000*MeV,
              'Lambda1520_iso'      : 0.85,
              'Lambda1520_ProbNNK'  : 0.5,
              'Lambda1520_ProbNNp'  : 0.8,
              'Lambda1520_Daug_PT'  : 500*MeV,
              'Lambda1520_GhostProb': 0.3,
              'Lambda1520_IPChi2'   : 25,
              'Lambda1520_VChi2'    :  2,
              'Lambda1520_M_Min'    : 1460,
              'Lambda1520_M_Max'    : 1580,
              'Lambda1520_FDChi2'   : 45,
              'Lambda1520_MaxDoca'  : 0.1*millimeter,
              },
    'StrippingDMLambdaToPiPiLine': {
              'Prescale'            : 1.0,
              'Postscale'           : 1.0,
              'Prescale_control'    : 0.1,
              'Postscale_control'   : 1.0,
              'LambdaToPiPi_PT'          : 1000*MeV,
              'LambdaToPiPi_iso'         : 0.95,
              'LambdaToPiPi_ProbNNpi'    : 0.9,
              'LambdaToPiPi_Daug_PT'     : 1000*MeV,
              'LambdaToPiPi_Daug_IP_min' : 0.1*millimeter,
              'LambdaToPiPi_Daug_IP_max' : 3*millimeter,
              'LambdaToPiPi_Daug_IPChi2' : 12,
              'LambdaToPiPi_GhostProb'   : 0.1,
              'LambdaToPiPi_IPChi2'      : 10,
              'LambdaToPiPi_VChi2'       : 4,
              'LambdaToPiPi_M_Min'       : 1100,
              'LambdaToPiPi_M_Max'       : 5700,
              'LambdaToPiPi_FDChi2'      : 10,
              'LambdaToPiPi_MaxDoca'     : 0.05*millimeter,
              'LambdaToPiPi_DOCAChi2'    : 4,
              'LambdaToPiPi_FD'          : 5.5,
              'LambdaToPiPi_IPdivFD'     : 0.1,
              },
    'StrippingDMLambdaToKPiLine': {
              'Prescale'            : 1.0,
              'Postscale'           : 1.0,
              'Prescale_control'    : 0.1,
              'Postscale_control'   : 1.0,
              'LambdaToKPi_PT'          : 1000*MeV,
              'LambdaToKPi_iso'         : 0.9,
              'LambdaToKPi_ProbNNpi'    : 0.9,
              'LambdaToKPi_Daug_PT'     : 800*MeV,
              'LambdaToKPi_Daug_IP_min' : 0.1*millimeter,
              'LambdaToKPi_Daug_IP_max' : 3*millimeter,
              'LambdaToKPi_Daug_IPChi2' : 20,
              'LambdaToKPi_GhostProb'   : 0.1,
              'LambdaToKPi_ProbNNK'     : 0.9,
              'LambdaToKPi_IPChi2'      : 15,
              'LambdaToKPi_VChi2'       : 4,
              'LambdaToKPi_M_Min'       : 1100,
              'LambdaToKPi_M_Max'       : 5700,
              'LambdaToKPi_FDChi2'      : 40,
              'LambdaToKPi_MaxDoca'     : 0.1*millimeter,
              'LambdaToKPi_DOCAChi2'    : 4,
              'LambdaToKPi_FD'          : 5,
              'LambdaToKPi_IPdivFD'     : 0.1,
              },
    'StrippingDMLambda2595Line': {
              'Prescale'                  : 1.0,
              'Postscale'                 : 1.0,
              'Prescale_control'          : 0.6,
              'Postscale_control'         : 1.0,
              'K_ProbNNghost'             : 0.1,
              'K_ProbNNk'                 : 0.5,
              'K_PT'                      : 250*MeV,
              'p_ProbNNghost'             : 0.1,
              'p_ProbNNp'                 : 0.5,
              'p_PT'                      : 250*MeV,
              'pi_lambda2595_IPchi2'      : 0.5,
              'pi_lambda2595_ProbNNghost' : 0.1,
              'pi_lambda2595_PT'          : 100*MeV,
              'pi_lambdac_ProbNNghost'    : 0.1,
              'pi_lambdac_PT'             : 250*MeV,
              'Lambda_DOCA'               : 1.0*millimeter,
              'Lambda_FDCHI2'             : 50,
              'Lambda_M_Min'              : 2236*MeV,
              'Lambda_M_Max'              : 2336*MeV,
              'Lambda_PT'                 : 1500*MeV,
              'Lambda_VCHI2'              : 4,
              'Lambda2595_DOCA'           : 1.0*millimeter,
              'Lambda2595_FDCHI2'         : 5,
              'Lambda2595_ISO'            : 0.6,
              'Lambda2595_M_Min'          : 2545*MeV,
              'Lambda2595_M_Max'          : 2645*MeV,
              'Lambda2595_PT'             : 1500*MeV,
              'Lambda2595_VCHI2'          : 4
    },
    'StrippingDMLambdaToDPiLine': {
              'Prescale'                  : 1.0,
              'Postscale'                 : 1.0,
              'Prescale_control'          : 0.6,
              'Postscale_control'         : 1.0,
              'K_ProbNNghost'             : 0.1,
              'K_ProbNNk'                 : 0.9,
              'K_PT'                      : 600*MeV,
              'piD_IPchi2'                : 20,
              'piD_ProbNNghost'           : 0.2,
              'piD_ProbNNpi'              : 0.8,
              'piD_PT'                    : 600*MeV,
              'pi_ProbNNghost'            : 0.1,
              'pi_PT'                     : 600*MeV,
              'pi_ProbNNpi'               : 0.8,
              'D_DOCA'                    : 0.3*millimeter,
              'D_FDCHI2'                  : 50,
              'D_M_Min'                   : 1840*MeV,
              'D_M_Max'                   : 1900*MeV,
              'D_PT'                      : 2000*MeV,
              'D_VCHI2'                   : 6,
              'LambdaToDPi_DOCA'          : 0.2*millimeter,
              'LambdaToDPi_DOCACHI2'      : 7,
              'LambdaToDPi_FDCHI2'        : 30,
              'LambdaToDPi_ISO'           : 0.6,
              'LambdaToDPi_M_Min'         : 1100*MeV,
              'LambdaToDPi_M_Max'         : 5700*MeV,
              'LambdaToDPi_PT'            : 3000*MeV,
              'LambdaToDPi_VCHI2'         : 8,
              'LambdaToDPi_IPCHI2'        : 20
    },
    'StrippingDMLambdaToDKLine': {
              'Prescale'                  : 1.0,
              'Postscale'                 : 1.0,
              'Prescale_control'          : 0.6,
              'Postscale_control'         : 1.0,
              'K_ProbNNghost'             : 0.1,
              'K_ProbNNk'                 : 0.8,
              'K_PT'                      : 250*MeV,
              'KD_IPchi2'                 : 20,
              'KD_ProbNNghost'            : 0.2,
              'KD_ProbNNK'                : 0.7,
              'KD_PT'                     : 250*MeV,
              'pi_ProbNNghost'            : 0.1,
              'pi_PT'                     : 250*MeV,
              'pi_ProbNNpi'               : 0.8,
              'D_DOCA'                    : 0.4*millimeter,
              'D_FDCHI2'                  : 50,
              'D_M_Min'                   : 1840*MeV,
              'D_M_Max'                   : 1900*MeV,
              'D_PT'                      : 2000*MeV,
              'D_VCHI2'                   : 6,
              'LambdaToDK_DOCA'           : 0.5*millimeter,
              'LambdaToDK_DOCACHI2'       : 8,
              'LambdaToDK_FDCHI2'         : 30,
              'LambdaToDK_ISO'            : 0.6,
              'LambdaToDK_M_Min'          : 1100*MeV,
              'LambdaToDK_M_Max'          : 5700*MeV,
              'LambdaToDK_PT'             : 2000*MeV,
              'LambdaToDK_VCHI2'          : 8,
              'LambdaToDK_IPCHI2'         : 25
    },
  }
  }

class LambdaDecaysDMConf(LineBuilder) :
  __configuration_keys__ = default_config['CONFIG'].keys()

  def __init__(self, name, config):
    LineBuilder.__init__(self, name, config)

    sel_lambda1520 = self.combineLambda1520(name, config['StrippingDMLambda1520Line'])

    lambda1520line = StrippingLine(name + 'Lambda1520Line',
                                           prescale  = config['StrippingDMLambda1520Line']['Prescale'],
                                           postscale = config['StrippingDMLambda1520Line']['Postscale'],
                                           checkPV   = config['Common']['checkPV'],
                                           selection = sel_lambda1520)
    
    self.registerLine(lambda1520line)

    sel_lambdaToPiPi = self.combineLambdaToPiPi(name, config['StrippingDMLambdaToPiPiLine'])

    lambdaToPiPiline = StrippingLine(name + 'LambdaToPiPiLine',
                                           prescale  = config['StrippingDMLambdaToPiPiLine']['Prescale'],
                                           postscale = config['StrippingDMLambdaToPiPiLine']['Postscale'],
                                           checkPV   = config['Common']['checkPV'],
                                           selection = sel_lambdaToPiPi)
    
    self.registerLine(lambdaToPiPiline)

    sel_lambdaToDPi = self.combineLambdaToDPi(name, config['StrippingDMLambdaToDPiLine'])

    lambdaToDPiline = StrippingLine(name + 'LambdaToDPiLine',
                                           prescale  = config['StrippingDMLambdaToDPiLine']['Prescale'],
                                           postscale = config['StrippingDMLambdaToDPiLine']['Postscale'],
                                           checkPV   = config['Common']['checkPV'],
                                           selection = sel_lambdaToDPi)
    
    self.registerLine(lambdaToDPiline)

    sel_lambdaToDK = self.combineLambdaToDK(name, config['StrippingDMLambdaToDKLine'])

    lambdaToDKline = StrippingLine(name + 'LambdaToDKLine',
                                           prescale  = config['StrippingDMLambdaToDKLine']['Prescale'],
                                           postscale = config['StrippingDMLambdaToDKLine']['Postscale'],
                                           checkPV   = config['Common']['checkPV'],
                                           selection = sel_lambdaToDK)

    self.registerLine(lambdaToDKline)

    sel_lambdaToKPi = self.combineLambdaToKPi(name, config['StrippingDMLambdaToKPiLine'])

    lambdaToKPiline = StrippingLine(name + 'LambdaToKPiLine',
                                           prescale  = config['StrippingDMLambdaToKPiLine']['Prescale'],
                                           postscale = config['StrippingDMLambdaToKPiLine']['Postscale'],
                                           checkPV   = config['Common']['checkPV'],
                                           selection = sel_lambdaToKPi)
    
    self.registerLine(lambdaToKPiline)

    sel_lambda1520_control = self.combineLambda1520(name+'_Control', config['StrippingDMLambda1520Line'], isolation = False)

    lambda1520line_control = StrippingLine(name + 'Lambda1520ControlLine',
                                           prescale  = config['StrippingDMLambda1520Line']['Prescale_control'],
                                           postscale = config['StrippingDMLambda1520Line']['Postscale_control'],
                                           checkPV   = config['Common']['checkPV'],
                                           selection = sel_lambda1520_control)

    self.registerLine(lambda1520line_control)
  
    sel_lambda2595 = self.combineLambda2595(name, config['StrippingDMLambda2595Line'], isolation = True)

    lambda2595line = StrippingLine(name + 'Lambda2595Line',
                                   prescale  = config['StrippingDMLambda2595Line']['Prescale'],
                                   postscale = config['StrippingDMLambda2595Line']['Postscale'],
                                   checkPV   = config['Common']['checkPV'],
                                   selection = sel_lambda2595)

    self.registerLine(lambda2595line)
    
    sel_lambda2595_control = self.combineLambda2595(name+'_control', config['StrippingDMLambda2595Line'], isolation = False)
    
    lambda2595line_control = StrippingLine(name + 'Lambda2595ControlLine',
                                           prescale   = config['StrippingDMLambda2595Line']['Prescale_control'],
                                           postscale = config['StrippingDMLambda2595Line']['Postscale_control'],
                                           checkPV    = config['Common']['checkPV'],
                                           selection  = sel_lambda2595_control)
    
    self.registerLine(lambda2595line_control)


  def combineLambdaToKPi(self, name, config, isolation=True):

    daugh_cut={'pi': ("(PT > {LambdaToKPi_Daug_PT}) " 
                     "& (MIPCHI2DV(PRIMARY) > {LambdaToKPi_Daug_IPChi2}) " 
                     "& (TRGHOSTPROB < {LambdaToKPi_GhostProb}) " 
                     "& (PROBNNpi > {LambdaToKPi_ProbNNpi}) "
                     "& (BPVIP() > {LambdaToKPi_Daug_IP_min}) "
                     "& (BPVIP() < {LambdaToKPi_Daug_IP_max})").format(**config),
               'K': ("(PT > {LambdaToKPi_Daug_PT}) " 
                     "& (MIPCHI2DV(PRIMARY) > {LambdaToKPi_Daug_IPChi2}) " 
                     "& (TRGHOSTPROB < {LambdaToKPi_GhostProb}) " 
                     "& (PROBNNK > {LambdaToKPi_ProbNNK}) "
                     "& (BPVIP() > {LambdaToKPi_Daug_IP_min}) "
                     "& (BPVIP() < {LambdaToKPi_Daug_IP_max})").format(**config)}
    
    comb_cut = ("(APT > {LambdaToKPi_PT}) &"
                "(AM > {LambdaToKPi_M_Min}) & (AM < {LambdaToKPi_M_Max}) &"
                "(AMAXDOCA('') < {LambdaToKPi_MaxDoca}) &"
                "(ACUTDOCACHI2({LambdaToKPi_DOCAChi2},''))".format(**config))

    if not isolation:                
      mother_cut = ("(PT > {LambdaToKPi_PT})"
                  "& (M > {LambdaToKPi_M_Min}) & (M < {LambdaToKPi_M_Max})"
                  "& (HASVERTEX)" 
                  "& (VFASPF(VCHI2PDOF) < {LambdaToKPi_VChi2}) " 
                  "& (BPVIPCHI2() > {LambdaToKPi_IPChi2})" 
                  "& (BPVVDCHI2 > {LambdaToKPi_FDChi2})".format(**config))
    else:
      mother_cut = ("(PT > {LambdaToKPi_PT})"
                    "& (M > {LambdaToKPi_M_Min}) & (M < {LambdaToKPi_M_Max})"
                    "& (HASVERTEX)" 
                    "& (VFASPF(VCHI2PDOF) < {LambdaToKPi_VChi2}) " 
                    "& (BPVIPCHI2() > {LambdaToKPi_IPChi2})" 
                    "& (BPVVDCHI2 > {LambdaToKPi_FDChi2})"
                    "& ((PT/(PT+PTCONE)) > {LambdaToKPi_iso})"
                    "& (BPVVDZ > {LambdaToKPi_FD})"
                    "& ((BPVIP()/BPVVDR) < {LambdaToKPi_IPdivFD})".format(**config))
            
    decay = "[Lambda_b0 -> K+ pi-]cc"
    namesel = name + "LambdaToKPiSel"
  
    _combination = CombineParticles( DecayDescriptor    = decay,
                                     CombinationCut     = comb_cut,
                                     DaughtersCuts      = { 'K+' : daugh_cut["K"],
                                                            'pi-' : daugh_cut["pi"] },
                                     MotherCut          = mother_cut,
                                     Preambulo =  ["PTCONE  = SUMCONE (   0.6**2 , PT , '/Event/Phys/StdAllNoPIDsPions/Particles')"]
                                   )
    
    return Selection ( namesel,
                       Algorithm          = _combination,
                       RequiredSelections = [ StdAllLooseKaons, StdAllLoosePions ]
                     )
    
  def combineLambda1520(self, name, config, isolation=True):

    daugh_cut={'K': ("(PT > {Lambda1520_Daug_PT}) " 
                     "& (MIPCHI2DV(PRIMARY) > {Lambda1520_IPChi2}) " 
                     "& (TRGHOSTPROB < {Lambda1520_GhostProb}) " 
                     "& (PROBNNK > {Lambda1520_ProbNNK}) ").format(**config),
               'p': ("(PT > {Lambda1520_Daug_PT}) " 
                     "& (MIPCHI2DV(PRIMARY) > {Lambda1520_IPChi2}) " 
                     "& (TRGHOSTPROB < {Lambda1520_GhostProb}) " 
                     "& (PROBNNp > {Lambda1520_ProbNNp}) ").format(**config)}
    
    comb_cut = ("(APT > {Lambda1520_PT}) &"
                "(AM > {Lambda1520_M_Min}) & (AM < {Lambda1520_M_Max}) &"
                "(AMAXDOCA('') < {Lambda1520_MaxDoca}) ".format(**config))

    if not isolation:                
      mother_cut = ("(PT > {Lambda1520_PT})"
                  "& (M > {Lambda1520_M_Min}) & (M < {Lambda1520_M_Max})"
                  "& (HASVERTEX)" 
                  "& (VFASPF(VCHI2PDOF) < {Lambda1520_VChi2}) " 
                  "& (BPVIPCHI2() > {Lambda1520_IPChi2})" 
                  "& (BPVVDCHI2 > {Lambda1520_FDChi2})".format(**config))
    else:
      mother_cut = ("(PT > {Lambda1520_PT})"
                    "& (M > {Lambda1520_M_Min}) & (M < {Lambda1520_M_Max})"
                    "& (HASVERTEX)" 
                    "& (VFASPF(VCHI2PDOF) < {Lambda1520_VChi2}) " 
                    "& (BPVIPCHI2() > {Lambda1520_IPChi2})" 
                    "& (BPVVDCHI2 > {Lambda1520_FDChi2})"
                    "& ((PT/(PT+PTCONE)) > {Lambda1520_iso})".format(**config))
            
    decay = "[Lambda(1520)0 -> K- p+]cc"
    namesel = name + "Lambda1520Sel"
  
    _combination = CombineParticles( DecayDescriptor    = decay,
                                     CombinationCut     = comb_cut,
                                     DaughtersCuts      = { 'K-' : daugh_cut["K"],
                                                            'p+' : daugh_cut["p"] },
                                     MotherCut          = mother_cut,
                                     Preambulo =  ["PTCONE  = SUMCONE (   0.6**2 , PT , '/Event/Phys/StdAllNoPIDsPions/Particles')"]
                                   )
    
    return Selection ( namesel,
                       Algorithm          = _combination,
                       RequiredSelections = [ StdAllLooseProtons, StdAllLooseKaons ]
                     )

  def combineLambdaToPiPi(self, name, config, isolation=True):

    daugh_cut={'pi': ("(PT > {LambdaToPiPi_Daug_PT}) " 
                     "& (MIPCHI2DV(PRIMARY) > {LambdaToPiPi_Daug_IPChi2}) " 
                     "& (TRGHOSTPROB < {LambdaToPiPi_GhostProb}) " 
                     "& (PROBNNpi > {LambdaToPiPi_ProbNNpi}) "
                     "& (BPVIP() > {LambdaToPiPi_Daug_IP_min}) "
                     "& (BPVIP() < {LambdaToPiPi_Daug_IP_max})").format(**config)}
    
    comb_cut = ("(APT > {LambdaToPiPi_PT}) &"
                "(AM > {LambdaToPiPi_M_Min}) & (AM < {LambdaToPiPi_M_Max}) &"
                "(AMAXDOCA('') < {LambdaToPiPi_MaxDoca}) &"
                "(ACUTDOCACHI2({LambdaToPiPi_DOCAChi2},''))".format(**config))

    if not isolation:                
      mother_cut = ("(PT > {LambdaToPiPi_PT})"
                  "& (M > {LambdaToPiPi_M_Min}) & (M < {LambdaToPiPi_M_Max})"
                  "& (HASVERTEX)" 
                  "& (VFASPF(VCHI2PDOF) < {LambdaToPiPi_VChi2}) " 
                  "& (BPVIPCHI2() > {LambdaToPiPi_IPChi2})" 
                  "& (BPVVDCHI2 > {LambdaToPiPi_FDChi2})".format(**config))
    else:
      mother_cut = ("(PT > {LambdaToPiPi_PT})"
                    "& (M > {LambdaToPiPi_M_Min}) & (M < {LambdaToPiPi_M_Max})"
                    "& (HASVERTEX)" 
                    "& (VFASPF(VCHI2PDOF) < {LambdaToPiPi_VChi2}) " 
                    "& (BPVIPCHI2() > {LambdaToPiPi_IPChi2})" 
                    "& (BPVVDCHI2 > {LambdaToPiPi_FDChi2})"
                    "& ((PT/(PT+PTCONE)) > {LambdaToPiPi_iso})"
                    "& (BPVVDZ > {LambdaToPiPi_FD})".format(**config))
                    # "& ((BPVIP()/BPVVDR) < {LambdaToPiPi_IPdivFD})".format(**config))
            
    decay = "[Lambda_b0 -> pi+ pi-]cc"
    namesel = name + "LambdaToPiPiSel"
  
    _combination = CombineParticles( DecayDescriptor    = decay,
                                     CombinationCut     = comb_cut,
                                     DaughtersCuts      = { 'pi+' : daugh_cut["pi"],
                                                            'pi-' : daugh_cut["pi"] },
                                     MotherCut          = mother_cut,
                                     Preambulo =  ["PTCONE  = SUMCONE (   0.6**2 , PT , '/Event/Phys/StdAllNoPIDsPions/Particles')"]
                                   )
    
    return Selection ( namesel,
                       Algorithm          = _combination,
                       RequiredSelections = [ StdAllLoosePions ]
                     )

  def combineLambda_c(self, name, config):

    daugh_cut={'K': ("(PT > {K_PT} )"
                     "& (TRGHOSTPROB < {K_ProbNNghost})"
                     "& (PROBNNK > {K_ProbNNk})").format(**config),
               'p': ("(PT > {p_PT} )"
                     "& (TRGHOSTPROB < {p_ProbNNghost})"
                     "& (PROBNNp > {p_ProbNNp})").format(**config),
               'pi': ("(PT > {pi_lambdac_PT} )"
                     "& (TRGHOSTPROB < {pi_lambdac_ProbNNghost})").format(**config)}

    comb_cut = ("(APT > {Lambda_PT}) &"
                "(AM > {Lambda_M_Min}) & (AM < {Lambda_M_Max}) &"
                "(AMAXDOCA('') < {Lambda_DOCA})".format(**config))

    mother_cut = ("(PT > {Lambda_PT} ) &"
                  "( M > {Lambda_M_Min}) & (M < {Lambda_M_Max}) &"
                  "(VFASPF(VCHI2PDOF) < {Lambda_VCHI2}) &"
                  "(BPVVDCHI2 > {Lambda_FDCHI2})".format(**config))

    decay = "[Lambda_c+ -> K- p+ pi+]cc"
    namesel = name+"Lambdac_Lambda2595Sel"

    _combination = CombineParticles( DecayDescriptor    = decay,
                                     CombinationCut     = comb_cut,
                                     DaughtersCuts      = { 'K-' : daugh_cut["K"],
                                                            'p+' : daugh_cut["p"],
                                                            'pi+': daugh_cut["pi"] },
                                     MotherCut          = mother_cut
                                     )
    return Selection ( namesel,
                       Algorithm          = _combination,
                       RequiredSelections = [ StdAllLooseKaons , StdAllLooseProtons, StdAllLoosePions ]
                       )

  def combineLambda2595(self, name, config, isolation = True):

    SelLambda_c = self.combineLambda_c("Lambda_c"+name, config)

    decay = "[Lambda_c(2595)+ -> Lambda_c+ pi+ pi-]cc"

    daugh_cut={'pi1': ("(PT > {pi_lambda2595_PT} )"
                      "& (MIPCHI2DV(PRIMARY) > {pi_lambda2595_IPchi2})"
                      "& (TRGHOSTPROB < {pi_lambda2595_ProbNNghost})").format(**config),
               'pi2': ("(PT > {pi_lambda2595_PT} )"
                       "& (MIPCHI2DV(PRIMARY) > {pi_lambda2595_IPchi2})"
                       "& (TRGHOSTPROB < {pi_lambda2595_ProbNNghost})").format(**config)}

    comb_cut = ("(APT > {Lambda2595_PT} ) &"
                "(AM > {Lambda2595_M_Min}) & (AM < {Lambda2595_M_Max}) &"
                "(AMAXDOCA('') < {Lambda2595_DOCA})".format(**config))
    
    if not isolation:
      
      mother_cut = ("(PT > {Lambda2595_PT} ) &"
                    "( M > {Lambda2595_M_Min}) & (M < {Lambda2595_M_Max}) &"
                    " (VFASPF(VCHI2PDOF) < {Lambda2595_VCHI2}) & "
                    " (BPVVDCHI2 > {Lambda2595_FDCHI2})".format(**config))
    else:

      mother_cut = ("(PT > {Lambda2595_PT} ) &"
                    "( M > {Lambda2595_M_Min}) & (M < {Lambda2595_M_Max}) &"
                    " (VFASPF(VCHI2PDOF) < {Lambda2595_VCHI2}) & "
                    " (BPVVDCHI2 > {Lambda2595_FDCHI2}) & "
                    " ((PT/(PT+PTCONE)) > {Lambda2595_ISO})".format(**config))

    Lambda2595 = CombineParticles( DecayDescriptor    = decay,
                                   CombinationCut     = comb_cut,
                                   DaughtersCuts      = { 'pi+' : daugh_cut["pi1"],
                                                          'pi-' : daugh_cut["pi2"]},
                                   MotherCut          = mother_cut,
                                   Preambulo =  ["PTCONE  = SUMCONE (   0.6**2 , PT , '/Event/Phys/StdAllNoPIDsPions/Particles')"]
                                 )

    return Selection("SelLambda2595"+name,
                     Algorithm = Lambda2595,
                     RequiredSelections = [SelLambda_c, StdAllLoosePions]
                    )

  def combineD(self, name, config):

    daugh_cut={'K': ("(PT > {K_PT} )"
                     "& (TRGHOSTPROB < {K_ProbNNghost})"
                     "& (PROBNNK > {K_ProbNNk})").format(**config),
               'pi': ("(PT > {pi_PT} )"
                      "& (TRGHOSTPROB < {pi_ProbNNghost})"
                      "& (PROBNNpi > {pi_ProbNNpi})").format(**config)}

    comb_cut = ("(APT > {D_PT}) &"
                "(AM > {D_M_Min}) & (AM < {D_M_Max}) &"
                "(AMAXDOCA('') < {D_DOCA})".format(**config))

    mother_cut = ("(PT > {D_PT} ) &"
                  "( M > {D_M_Min}) & (M < {D_M_Max}) &"
                  "(VFASPF(VCHI2PDOF) < {D_VCHI2}) &"
                  "(BPVVDCHI2 > {D_FDCHI2})".format(**config))

    decay = "[D- -> K+ pi- pi-]cc"
    namesel = name+"D_LambdaToDPiSel"

    _combination = CombineParticles( DecayDescriptor    = decay,
                                     CombinationCut     = comb_cut,
                                     DaughtersCuts      = { 'K+' : daugh_cut["K"],
                                                            'pi-': daugh_cut["pi"] },
                                     MotherCut          = mother_cut
                                     )
    return Selection ( namesel,
                       Algorithm          = _combination,
                       RequiredSelections = [ StdAllLooseKaons , StdAllLoosePions ]
                       )

  def combineLambdaToDPi(self, name, config, isolation = True):

    SelD = self.combineD("DPi"+name, config)

    decay = "[Lambda_b0 -> D- pi+]cc"

    daugh_cut={'piD': ("(PT > {piD_PT} )"
                      "& (MIPCHI2DV(PRIMARY) > {piD_IPchi2})"
                      "& (TRGHOSTPROB < {piD_ProbNNghost})"
                      "& (PROBNNpi > {piD_ProbNNpi})").format(**config)}

    comb_cut = ("(APT > {LambdaToDPi_PT} ) &"
                "(AM > {LambdaToDPi_M_Min}) & (AM < {LambdaToDPi_M_Max}) &"
                "(AMAXDOCA('') < {LambdaToDPi_DOCA}) &"
                "(ACUTDOCACHI2({LambdaToDPi_DOCACHI2},''))".format(**config))
    
    if not isolation:
      
      mother_cut = ("(PT > {LambdaToDPi_PT} ) &"
                    "( M > {LambdaToDPi_M_Min}) & (M < {LambdaToDPi_M_Max}) &"
                    " (VFASPF(VCHI2PDOF) < {LambdaToDPi_VCHI2}) & "
                    " (BPVVDCHI2 > {LambdaToDPi_FDCHI2})".format(**config))
    else:

      mother_cut = ("(PT > {LambdaToDPi_PT} ) &"
                    "( M > {LambdaToDPi_M_Min}) & (M < {LambdaToDPi_M_Max}) &"
                    " (VFASPF(VCHI2PDOF) < {LambdaToDPi_VCHI2}) & "
                    " (BPVVDCHI2 > {LambdaToDPi_FDCHI2}) & "
                    " (BPVIPCHI2() > {LambdaToDPi_IPCHI2}) &" 
                    " ((PT/(PT+PTCONE)) > {LambdaToDPi_ISO})".format(**config))

    LambdaToDPi = CombineParticles( DecayDescriptor    = decay,
                                   CombinationCut     = comb_cut,
                                   DaughtersCuts      = { 'pi+' : daugh_cut["piD"]},
                                   MotherCut          = mother_cut,
                                   Preambulo =  ["PTCONE  = SUMCONE (   0.6**2 , PT , '/Event/Phys/StdAllNoPIDsPions/Particles')"]
                                 )

    return Selection("SelLambdaToDPi"+name,
                     Algorithm = LambdaToDPi,
                     RequiredSelections = [SelD, StdAllLoosePions]
                    )

  def combineLambdaToDK(self, name, config, isolation = True):

    SelD = self.combineD("DK"+name, config)

    decay = "[Lambda_b0 -> D- K+]cc"

    daugh_cut={'KD': ("(PT > {KD_PT} )"
                      "& (MIPCHI2DV(PRIMARY) > {KD_IPchi2})"
                      "& (TRGHOSTPROB < {KD_ProbNNghost})"
                      "& (PROBNNK > {KD_ProbNNK})").format(**config)}

    comb_cut = ("(APT > {LambdaToDK_PT} ) &"
                "(AM > {LambdaToDK_M_Min}) & (AM < {LambdaToDK_M_Max}) &"
                "(AMAXDOCA('') < {LambdaToDK_DOCA}) &"
                "(ACUTDOCACHI2({LambdaToDK_DOCACHI2},''))".format(**config))
    
    if not isolation:
      
      mother_cut = ("(PT > {LambdaToDK_PT} ) &"
                    "( M > {LambdaToDK_M_Min}) & (M < {LambdaToDK_M_Max}) &"
                    " (VFASPF(VCHI2PDOF) < {LambdaToDK_VCHI2}) & "
                    " (BPVVDCHI2 > {LambdaToDK_FDCHI2})".format(**config))
    else:

      mother_cut = ("(PT > {LambdaToDK_PT} ) &"
                    "( M > {LambdaToDK_M_Min}) & (M < {LambdaToDK_M_Max}) &"
                    " (VFASPF(VCHI2PDOF) < {LambdaToDK_VCHI2}) & "
                    " (BPVVDCHI2 > {LambdaToDK_FDCHI2}) & "
                    " (BPVIPCHI2() > {LambdaToDK_IPCHI2}) &" 
                    " ((PT/(PT+PTCONE)) > {LambdaToDK_ISO})".format(**config))

    LambdaToDK = CombineParticles( DecayDescriptor    = decay,
                                   CombinationCut     = comb_cut,
                                   DaughtersCuts      = { 'K+' : daugh_cut["KD"]},
                                   MotherCut          = mother_cut,
                                   Preambulo =  ["PTCONE  = SUMCONE (   0.6**2 , PT , '/Event/Phys/StdAllNoPIDsPions/Particles')"]
                                 )

    return Selection("SelLambdaToDK"+name,
                     Algorithm = LambdaToDK,
                     RequiredSelections = [SelD, StdAllLooseKaons]
                    )
