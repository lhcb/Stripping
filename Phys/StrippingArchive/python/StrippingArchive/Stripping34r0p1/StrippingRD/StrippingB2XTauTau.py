###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = [' L. Pescatore', 'F. Blanc']
__date__ = '28/09/2018'
__version__ = '$Revision: 0.1$'

# Stripping line for B->Htautau

from GaudiKernel.SystemOfUnits import MeV
from GaudiKernel.SystemOfUnits import mm

"""
  B->KstarTauTau, B->KTauTau, B->PhiTauTau, B->Eta'TauTau
  """
__all__ = (
  'B2XTauTauConf',  
  'default_config'
  )

default_config =  {
  'NAME'                          : 'B2XTauTau',
  'BUILDERTYPE'                   : 'B2XTauTauConf',
  'WGs'                           : [ 'RD' ],
  'CONFIG'                        : {
  'SpdMult'                       : '600',
  #
  'UsePID'                        : True,
  #
  #'Photon_PT_Min'                 : 1500.0,
  'Photon_Res_PT_Min'             : 400.0,
  'Photon_CL_Min'                 : 0.2,
  #
  'FD_B_Max'                      : 80,
  'PT_B'                          : 2000 * MeV, 
  'P_B'                           : 10000 * MeV,
  'PT_Kst'                        : 1000 * MeV,
  'VCHI2_B'                       : 150, # dimensionless
  'VCHI2_BK'                      : 150, # dimensionless
  'FDCHI2_B'                      : 16,
  'FDCHI2_BK'                     : 16,
  'MASS_LOW_B'                    : 2000 * MeV,
  'MASS_LOW_BK'                   : 3000 * MeV,
  'MASS_HIGH_B'                   : 8000 * MeV,
  #
  'FD_Kst_Mu_KMM'                 : 3,
  'VCHI2_Phi'                     : 16,
  'VCHI2_Eta'                     : 16,
  'VCHI2_Kst'                     : 16,
  'PT_Phi'                        : 1000 * MeV,
  'PT_K'                          : 1000 * MeV,
  'PT_Tr'                         : 400 * MeV,
  'PT_Etap'                       : 1000 * MeV,
  'P_K'                           : 4000 * MeV,
  'IPCHI2_Tr'                     : 16,
  'TRACKCHI2_Tr'                  : 6,
  'TRGHOPROB_Tr'                  : 0.5,
  'MASS_LOW_Kst'                  : 700 * MeV, # MeV
  'MASS_HIGH_Kst'                 : 1100 * MeV, # MeV
  'MASS_LOW_Phi'                  : 1000 * MeV, # MeV
  'MASS_HIGH_Phi'                 : 1100 * MeV, # MeV
  'MASS_LOW_Etap'                 : 800 * MeV, # MeV
  'MASS_HIGH_Etap'                : 1100 * MeV, # MeV
  #
  'B2HTauTau_LinePrescale'        : 1,
  'B2HTauTau_LinePostscale'       : 1,
  'B2KTauTau_LinePrescale'        : 1,
  'B2KTauTau_LinePostscale'       : 1,
  'RelInfoTools' : [
        { "Type" : "RelInfoVertexIsolation",
          "Location" : "BVars_VertexIsoInfo",
           "DaughterLocations" : { "[Beauty -> ^Hadron  l  l]CC" : "H_VertexIsoInfo",
                                   "[Beauty ->  Hadron ^l  l]CC" : "Tau1_VertexIsoInfo",
                                   "[Beauty ->  Hadron  l ^l]CC" : "Tau2_VertexIsoInfo"
                                                                } },
        { "Type" : "RelInfoConeIsolation",
                   "ConeSize" : 0.5,
                   "Variables" : [],
                   "Location" : "BVars_ConeIsoInfo",
                   "DaughterLocations" : { "[Beauty -> ^Hadron  l  l]CC" : "H_ConeIsoInfo",
                                           "[Beauty ->  Hadron ^l  l]CC" : "Tau1_ConeIsoInfo",
                                           "[Beauty ->  Hadron  l ^l]CC" : "Tau2_ConeIsoInfo"
                                                                } }
                                                  ]
  },
  'STREAMS'     : ['Bhadron']
  }
  

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, OfflineVertexFitter, DaVinci__N3BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdNoPIDsPions, StdLoosePions
from StandardParticles import StdLooseKaons, StdNoPIDsKaons
from StandardParticles import StdLooseAllPhotons

class B2XTauTauConf(LineBuilder) :
    
    """
      Builder for B->KstarTauTau, B->KTauTau, B->PhiTauTau, B->Eta'TauTau
      """
    
    __configuration_keys__ = default_config['CONFIG'].keys()  

    
    def __init__(self, name, config):

      LineBuilder.__init__(self, name, config)

      trackCuts     = "(MIPCHI2DV(PRIMARY) > %(IPCHI2_Tr)s) & (TRCHI2DOF < %(TRACKCHI2_Tr)s) & (TRGHOSTPROB < %(TRGHOPROB_Tr)s)" % config
      KCuts         = trackCuts + " & (PT > %(PT_K)s) & (P > %(P_K)s)" % config
      if config['UsePID'] : KCuts += " & (PROBNNk>0.2)" 
      
      self.FilterSPD = {
                  'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config ,
                  'Preambulo' : ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]
                  }
      
      self.rawTau        = DataOnDemand("Phys/StdTightDetachedTau3pi/Particles")
      
      self.selKaon       = SimpleSelection( "Kaon"+name, FilterDesktop, [ StdLooseKaons ], Code = KCuts)
      self.selPhi        = self._makePhi( "Phi"+name, trackCuts, config )
      self.selEtap       = self._makeEtap( "Etap"+name, trackCuts, config )
      self.selKstar      = self._makeKstar("Kstar"+name, trackCuts, config)

      self.selB2KstTauTau    = self._makeBd2HTauTau( name+"_Kstar", self.rawTau, self.selKstar, config )
      self.selB2PhiTauTau    = self._makeBd2HTauTau( name+"_Phi", self.rawTau, self.selPhi, config )
      self.selB2EtapTauTau   = self._makeBd2HTauTau( name+"_Etap", self.rawTau, self.selEtap, config )
      self.selB2KTauTau      = self._makeBu2KTauTau( name+"_K", self.rawTau, self.selKaon, config )
      self.selB2KstTauTauSS  = self._makeBd2HTauTau( name+"_Kstar", self.rawTau, self.selKstar, config, SS = True )
      self.selB2PhiTauTauSS  = self._makeBd2HTauTau( name+"_Phi", self.rawTau, self.selPhi, config, SS = True )
      self.selB2EtapTauTauSS = self._makeBd2HTauTau( name+"_Etap", self.rawTau, self.selEtap, config, SS = True )
      self.selB2KTauTauSS    = self._makeBu2KTauTau( name+"_K", self.rawTau, self.selKaon, config, SS = True )

      ## Finished making selections build and register lines

      self.KstarTauTau_Line   = self._makeLine("B2KstarTauTauLine", self.selB2KstTauTau, config)
      self.PhiTauTau_Line     = self._makeLine("B2PhiTauTauLine", self.selB2PhiTauTau, config)
      self.EtapTauTau_Line    = self._makeLine("B2EtapTauTauLine", self.selB2EtapTauTau, config)
      self.KTauTau_Line       = self._makeLine("B2KTauTauLine", self.selB2KTauTau, config)
      self.PhiTauTauSS_Line   = self._makeLine("B2PhiTauTauSSLine", self.selB2PhiTauTauSS, config)
      self.KstarTauTauSS_Line = self._makeLine("B2KstarTauTauSSLine", self.selB2KstTauTauSS, config)
      self.KTauTauSS_Line     = self._makeLine("B2KTauTauSSLine", self.selB2KTauTauSS, config)
      self.EtapTauTauSS_Line  = self._makeLine("B2EtapTauTauSSLine", self.selB2EtapTauTauSS, config)


#### Make resonances ###################################################

    def _makePhi(self, name, trackSel, config) :
               
       combcut = "in_range ( %(MASS_LOW_Phi)s, AM, %(MASS_HIGH_Phi)s )" % config
       mothercut = " (PT > %(PT_Phi)s*MeV) & (VFASPF(VCHI2) < %(VCHI2_Phi)s)  " % config 

       Kcuts = trackSel 
       if config['UsePID'] : Kcuts += ' & (PROBNNk>0.2)' 

       daucut = { 'K+' : Kcuts, 'K-' : Kcuts }

       return SimpleSelection( name, CombineParticles, [StdLooseKaons],
                               DecayDescriptors = ["phi(1020) -> K+ K-"],
                               CombinationCut   = combcut, 
                               MotherCut        = mothercut,
                               DaughtersCuts    = daucut )

    
    def _makeEtap(self, name, trackSel, config ) :

      combcut   = "(APT> %(PT_Etap)s) & (in_range ( %(MASS_LOW_Etap)s, AM, %(MASS_HIGH_Etap)s))" % config
      mothercut = "(VFASPF(VCHI2/VDOF) < %(VCHI2_Eta)s)" % config
      gammacut  = "(PT > %(Photon_Res_PT_Min)s*MeV) & (CL > %(Photon_CL_Min)s)" % config
      daucut = { 'pi+' : trackSel, 'pi-' : trackSel, 'gamma' : gammacut }

      return SimpleSelection(name+"_Etap", CombineParticles, [ StdNoPIDsPions, StdLooseAllPhotons ],
                             DecayDescriptors = ["eta_prime -> pi+ pi- gamma"],
                             CombinationCut   = combcut,
                             MotherCut        = mothercut,
                             DaughtersCuts    = daucut )

    def _makeKstar(self, name, trackSel, config) :
               
       combcut = "in_range ( %(MASS_LOW_Kst)s, AM, %(MASS_HIGH_Kst)s )" % config 
       mothercut = " (PT > %(PT_Kst)s*MeV) & (VFASPF(VCHI2) < %(VCHI2_Kst)s) & (BPVVD > %(FD_Kst_Mu_KMM)s) " % config 

       Kcuts = trackSel 
       Picuts = trackSel
       if config['UsePID'] : 
           Kcuts += ' & (PROBNNk>0.2)' 
           Picuts += ' & (PROBNNpi>0.2)' 

       daucut = { 'pi+' : Picuts, 'K-' : Kcuts }
       
       return SimpleSelection(name+"_Kstar", CombineParticles, [StdLoosePions,StdNoPIDsKaons],
                              DecayDescriptors = ["[K*(892)0 -> K+ pi-]cc"],
                              CombinationCut   = combcut,
                              MotherCut        = mothercut,
                              DaughtersCuts    = daucut  )


#### Make B ###################################################

    def _makeBd2HTauTau(self, name, tauSel, HSel, config, SS = False):
      
      combcut    = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
      mothercut  = "( VFASPF(VCHI2) < %(VCHI2_B)s ) & ( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
      mothercut  += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config
      
      part = "eta_prime"
      n    = name+"_TauTau"
      if "Phi" in name : part = "phi(1020)"
      elif "Kstar" in name : part = "K*(892)0"

      
      descriptors = ["B_s0 -> %s tau+ tau-" % part ]

      if "Kstar" in name : descriptors = ["[B_s0 -> %s tau+ tau-]cc" % part ]

      if SS : 
        n += "SS"
        descriptors = ["[B_s0 -> %s tau+ tau+]cc" % part,"[B_s0 -> %s tau- tau-]cc" % part]
      
      Combine = DaVinci__N3BodyDecays(
                     DecayDescriptors = descriptors,
                     Combination12Cut = "AM < 7000",
                     CombinationCut = combcut,
                     MotherCut = mothercut
                     )
      
      return Selection(n, Algorithm = Combine, RequiredSelections = [ tauSel , HSel ] )


    def _makeBu2KTauTau(self, name, tauSel, KSel, config, SS = False):
      
      combcut    = "in_range ( %(MASS_LOW_BK)s, AM, %(MASS_HIGH_B)s )" % config
      mothercut  = "( VFASPF(VCHI2) < %(VCHI2_BK)s ) & ( BPVVDCHI2 > %(FDCHI2_BK)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
      mothercut  += " & (PT > %(PT_B)s) & (P > %(P_B)s)" % config
      
      n = name+"TauTau"
      descriptors = ["[B+ -> K+ tau+ tau-]cc"]
      if SS : 
        n = name+"TauTauSS"
        descriptors = ["[B+ -> K- tau+ tau+]cc","[B+ -> K+ tau+ tau+]cc"]

      Combine = DaVinci__N3BodyDecays(
                     DecayDescriptors = descriptors,
                     Combination12Cut = "AM<5000",
                     CombinationCut = combcut,
                     MotherCut = mothercut
                     )
      
      return Selection(n, Algorithm = Combine, RequiredSelections = [ tauSel , KSel ] )

#### Helpers to make lines

    def _makeLine(self, name, sel, config) :
        
        line = StrippingLine(name,
                       prescale    = config['B2HTauTau_LinePrescale'],
                       postscale   = config['B2HTauTau_LinePostscale'],
                       MDSTFlag = False,
                       FILTER = self.FilterSPD,
                       RelatedInfoTools = config['RelInfoTools'],
                       selection = sel,
                       MaxCandidates = 50 
                     )
        self.registerLine( line )
        return line

