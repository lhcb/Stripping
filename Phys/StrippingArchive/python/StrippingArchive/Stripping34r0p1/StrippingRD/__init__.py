###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

_selections = ('StrippingB2EtaMuMu', 'StrippingB2LLXBDT', 'StrippingB2Lambda0ELines', 'StrippingB2Lambda0MuLines', 'StrippingB2Lambda0MuOSLines', 'StrippingB2Lambda0SSMuMu3PiLines', 'StrippingB2XMuMu', 'StrippingB2XTauTau', 'StrippingBeauty2XGamma', 'StrippingBeauty2XGammaExclTDCPV', 'StrippingBs2MuMuLines', 'StrippingBu2LLK', 'StrippingDarkBoson', 'StrippingHypb2L0HGamma', 'StrippingKshort2Leptons', 'StrippingLFVLines', 'StrippingLb2L0Gamma', 'StrippingLc23MuLines', 'StrippingStrangeHadrons', 'StrippingStrangeSL', 'StrippingTau23MuLines')

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
