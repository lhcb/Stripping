###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## #####################################################################
# A stripping selection for VeloMuon J/Psi->mu+mu- decays
# To be used for tracking studies
#
# @authors G. Krocker, P. Seyfert, S. Wandernoth
# @date 2010-Aug-17
#
# @authors P. Seyfert, A. Jaeger
# @date 2011-Mar-17
#
# @author M. Kolpin
# @date 2015-Mar-23
#
# @author R. Kopecna
# @date 2019-Jan-23
#
#######################################################################

__author__ = ['Renata Kopecna']
__date__ = '25/01/2019'
__version__ = '$Revision: 2.0 $'

__all__ = ('StrippingTrackEffVeloMuonConf',
           'default_config',
           'chargeFilter',
           'longtrackFilter',
           'selMuonPParts',
           'makeMyMuons',
           'makeResonanceVeloMuTrackEff',
           'selHlt1Jpsi',
           'selHlt2Jpsi',
           'trackingPreFilter',
           )



from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from Configurables import CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from Configurables import ChargedProtoParticleMaker, NoPIDsParticleMaker, DataOnDemandSvc, DelegatingTrackSelector, TrackSelector, CombinedParticleMaker, BestPIDParticleMaker
from Configurables import FastVeloTracking

from StrippingConf.StrippingLine import StrippingLine
from Configurables import TrackStateInitAlg, TrackEventFitter, TrackPrepareVelo,TrackContainerCopy, Tf__PatVeloSpaceTool, StandaloneMuonRec
from Configurables import TrackCloneFinder
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLooseMuons

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, TisTosParticleTagger

from Configurables import GaudiSequencer
from Configurables import TrackSys
from PhysSelPython.Wrappers import AutomaticData
# Get the fitters
from TrackFitter.ConfiguredFitters import ConfiguredFit

from Configurables import TrackEventCloneKiller,VeloMuonBuilder
from Configurables import TrackEventFitter, TrackMasterFitter
from Configurables import TrackKalmanFilter, TrackMasterExtrapolator
#from TrackFitter.ConfiguredFitters import ConfiguredFastFitter
#from Configurables import TrackCloneFinder

from SelPy.utils import ( UniquelyNamedObject,
                          ClonableObject,
                          SelectionBase )

default_config = {
    'NAME'        : 'TrackEffVeloMuon',
    'WGs'         : ['Calib'],
    'BUILDERTYPE' : 'StrippingTrackEffVeloMuonConf',
    'CONFIG'      : {
            #Mother paramteres
			"JpsiPt":	       0.5	# GeV
		,	"JpsiVertChi2":	       2.	# adimensional
		,	"JpsiMassPreComb":     1000.	# MeV
		,	"JpsiMassPostComb":    500.	# MeV
		,	"UpsilonPt":	       0.5	# GeV
		,	"UpsilonVertChi2":     10000.	# adimensional, dummy
		,	"UpsilonMassPreComb":  100000.  # MeV
		,	"UpsilonMassPostComb": 1500.	# MeV
		,	"ZPt":		       0.5	# GeV
		,	"ZVertChi2":	       10000.	# adimensional, dummy
		,	"ZMassPreComb":	       100000. 	# MeV
		,	"ZMassPostComb":       40000.	# MeV
        #Probe paramteres
		,	"JpsiProbeTrChi2":	5.	# adimensional
		,	"JpsiProbePt":		0.5     # GeV
		,	"JpsiProbeP":		5.      # GeV
		,	"UpsilonProbeTrChi2":   9999.	# adimensional, dummy
		,	"UpsilonProbePt":	0.5   	# GeV
		,	"UpsilonProbeP":	0.      # GeV
		,	"ZProbeTrChi2":		9999.	# adimensional, dummy
		,	"ZProbePt":	        20.     # GeV
		,	"ZProbeP":		0.      # GeV
        #Tag parameters
		,	"JpsiTagMuDLL":		-1.     # adimensional
		,	"JpsiTagP":	        7.	# GeV
		,	"JpsiTagPt":	        0.      # GeV
		,	"JpsiTagMinIP":		0.2     #mm
		,	"JpsiTagMinIPChi2":	0.      # adimensional, dummy
		,	"JpsiTagTrChi2":	3.	# adimensional
		,	"UpsilonTagMuDLL":	-9999.  # adimensional, dummy
		,	"UpsilonTagP":		0.	# GeV
		,	"UpsilonTagPt":	        0.5 	# GeV
		,	"UpsilonTagMinIP":	0.0     #mm, dummy
		,	"UpsilonTagMinIPChi2":	0.  #adimensional, dummy
		,	"UpsilonTagTrChi2":	9999.   #adimensional, dummy
		,	"ZTagMuDLL":		-9999.  # adimensional, dummy
		,	"ZTagP":		0.	# GeV
		,	"ZTagPt":	        20.     # GeV
		,	"ZTagMinIP":		0.      #mm, dummy
		,	"ZTagMinIPChi2":	0.      #mm, dummy
		,	"ZTagTrChi2":	        9999.   #adimensional, dummy
		,	"ZTagMinEta":		2.0	# adimensional
		,	"ZTagMaxEta":		4.5	# adimensional
        #Prescale
		,	"Prescale":		1.	# adimensional
		,	"ZPrescale":		1.	# adimensional
		,	"UpsilonPrescale":	1.	# adimensional
		,	"Postscale":		1.	# adimensional
		,	"ZPostscale":		1.	# adimensional
		,	"UpsilonPostscale":	1.	# adimensional
        #Hlt
       		,   	'JpsiHlt1Filter' : 'Hlt1.*Decision'
	        ,   	'JpsiHlt2Filter' : 'Hlt2.*Decision'
		,	'HLT1TisTosSpecs'	: { "Hlt1TrackMuonDecision%TOS" : 0, "Hlt1SingleMuonNoIPDecision%TOS" : 0 } #no reg. expression allowed(see selHlt1Jpsi )
		,	'ZHLT1TisTosSpecs'	: { "Hlt1SingleMuonHighPTDecision%TOS" : 0 } #no reg. expression allowed(see selHlt1Jpsi )
		,	'UpsilonHLT1TisTosSpecs': { "Hlt1SingleMuonHighPTDecision%TOS" : 0 } #no reg. expression allowed(see selHlt1Jpsi )
		,	'HLT1PassOnAll'		: True
		,	'HLT2TisTosSpecs'	: { "Hlt2SingleMuon.*Decision%TOS" : 0, "Hlt2TrackEffDiMuonVeloMuon.*Decision%TOS" : 0 } #reg. expression allowed
		,	'ZHLT2TisTosSpecs'	: { "Hlt2SingleMuonHighPTDecision%TOS" : 0} #reg. expression allowed
		,	'UpsilonHLT2TisTosSpecs': { "Hlt2SingleMuonLowPTDecision%TOS" : 0} #reg. expression allowed
		,	'HLT2PassOnAll'		: False
         },
    'STREAMS'     : { 'Dimuon' : ['StrippingTrackEffVeloMuonLine1',
                                  'StrippingTrackEffVeloMuonLine2',
                                  'StrippingTrackEffVeloMuonZLine1',
                                  'StrippingTrackEffVeloMuonZLine2',
                                  'StrippingTrackEffVeloMuonUpsilonLine1',
                                  'StrippingTrackEffVeloMuonUpsilonLine2']}
    }



class StrippingTrackEffVeloMuonConf(LineBuilder):
    """
    Definition of tag and probe JPsi stripping.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

	# CHECK TRIGGER
        self.TisTosPreFilter1Jpsi = selHlt1Jpsi('TisTosFilter1Jpsifor'+name, HLT1TisTosSpecs = config['HLT1TisTosSpecs'], HLT1PassOnAll = config['HLT1PassOnAll'])
        self.TisTosPreFilter2Jpsi = selHlt2Jpsi('TisTosFilter2Jpsifor'+name, hlt1Filter = self.TisTosPreFilter1Jpsi, HLT2TisTosSpecs = config['HLT2TisTosSpecs'], HLT2PassOnAll = config['HLT2PassOnAll'])

        self.TisTosPreFilter1Z = selHlt1Jpsi('TisTosFilter1Zfor'+name, HLT1TisTosSpecs = config['ZHLT1TisTosSpecs'], HLT1PassOnAll = config['HLT1PassOnAll'])
        self.TisTosPreFilter2Z = selHlt2Jpsi('TisTosFilter2Zfor'+name, hlt1Filter = self.TisTosPreFilter1Z, HLT2TisTosSpecs = config['ZHLT2TisTosSpecs'], HLT2PassOnAll = config['HLT2PassOnAll'])

        self.TisTosPreFilter1Upsilon = selHlt1Jpsi('TisTosFilter1Upsilonfor'+name, HLT1TisTosSpecs = config['UpsilonHLT1TisTosSpecs'], HLT1PassOnAll = config['HLT1PassOnAll'])
        self.TisTosPreFilter2Upsilon = selHlt2Jpsi('TisTosFilter2Upsilonfor'+name, hlt1Filter = self.TisTosPreFilter1Upsilon, HLT2TisTosSpecs = config['UpsilonHLT2TisTosSpecs'], HLT2PassOnAll = config['HLT2PassOnAll'])

        # CHECK FOR TAG-TRACKS
        tagCut = "((TRCHI2DOF < %(JpsiTagTrChi2)s)) & (PT > %(JpsiTagPt)s*GeV) & (P > %(JpsiTagP)s*GeV) & (PIDmu > %(JpsiTagMuDLL)s) & (MIPDV(PRIMARY)>%(JpsiTagMinIP)s*mm) & (MIPCHI2DV(PRIMARY)>%(JpsiTagMinIPChi2)s)" % config
        probeCut = "((TRCHI2DOF < %(JpsiProbeTrChi2)s)) & (PT > %(JpsiProbePt)s*GeV) & (P > %(JpsiProbeP)s*GeV)" % config

        ZTagCut = "((PT > %(ZTagPt)s*GeV) & (ETA > %(ZTagMinEta)s) & (ETA < %(ZTagMaxEta)s) & (P > %(ZTagP)s*GeV) )" % config
        ZProbeCut = "((PT > %(ZTagPt)s*GeV) & (ETA > %(ZTagMinEta)s) & (ETA < %(ZTagMaxEta)s) & (P > %(ZTagP)s*GeV) )" % config
        #ZProbeCut = "((TRCHI2DOF < %(ZProbeTrChi2)s)) & (PT > %(ZProbePt)s*GeV) & (P > %(ZProbeP)s*GeV)" % config

        UpsilonTagCut = "((PT > %(UpsilonTagPt)s*GeV) & (P > %(UpsilonTagP)s*GeV) )" % config
        UpsilonProbeCut = "((PT > %(UpsilonTagPt)s*GeV) & (P > %(UpsilonTagP)s*GeV) )" % config
        #UpsilonProbeCut = "((TRCHI2DOF < %(UpsilonProbeTrChi2)s)) & (PT > %(UpsilonProbePt)s*GeV) & (P > %(UpsilonProbeP)s*GeV)" % config

        self.longbothJpsi = longtrackFilter( name+'LongJpsiBoth', trackAlgo = 'LongMu', partSource = StdLooseMuons, tagCut = tagCut)
        self.longMinusJpsi = chargeFilter(name+'LongJpsiMinus', trackAlgo = 'LongMu', partSource = self.longbothJpsi, charge = -1, probeCut = probeCut, tagCut = tagCut)
        self.longPlusJpsi = chargeFilter( name+'LongJpsiPlus',  trackAlgo = 'LongMu', partSource = self.longbothJpsi, charge = 1,  probeCut = probeCut, tagCut = tagCut)

        self.longbothZ = longtrackFilter( name+'LongZBoth', trackAlgo = 'LongMu', partSource = StdLooseMuons, tagCut = ZTagCut)
        self.longMinusZ = chargeFilter( name+'LongZMinus', trackAlgo = 'LongMu', partSource = self.longbothZ, charge = -1, probeCut = ZTagCut, tagCut = ZTagCut)
        self.longPlusZ = chargeFilter(  name+'LongZPlus',  trackAlgo = 'LongMu', partSource = self.longbothZ, charge = 1,  probeCut = ZTagCut, tagCut = ZTagCut)

        self.longbothUpsilon = longtrackFilter( name+'LongUpsilonBoth', trackAlgo = 'LongMu', partSource = StdLooseMuons, tagCut = UpsilonTagCut)
        self.longMinusUpsilon = chargeFilter( name+'LongUpsilonMinus', trackAlgo = 'LongMu', partSource = self.longbothUpsilon, charge = -1, probeCut = UpsilonTagCut, tagCut = UpsilonTagCut)
        self.longPlusUpsilon = chargeFilter(  name+'LongUpsilonPlus',  trackAlgo = 'LongMu', partSource = self.longbothUpsilon, charge = 1,  probeCut = UpsilonTagCut, tagCut = UpsilonTagCut)

        # RECONSTRUCT PROBE-TRACKS
        self.TrackingPreFilter = trackingPreFilter(name+"_Jpsi", [self.TisTosPreFilter2Jpsi,self.longbothJpsi])
        self.VeloMuProtoPFilter = selMuonPParts(name+"_Jpsi", self.TrackingPreFilter)
        self.VeloMuPFilter = makeMyMuons(name+"_Jpsi", self.VeloMuProtoPFilter)

        self.ZTrackingPreFilter = trackingPreFilter(name+"_Z", [self.TisTosPreFilter2Z,self.longbothZ])
        self.ZVeloMuProtoPFilter = selMuonPParts(name+"_Z", self.ZTrackingPreFilter)
        self.ZVeloMuPFilter = makeMyMuons(name+"_Z", self.ZVeloMuProtoPFilter)

        self.UpsilonTrackingPreFilter = trackingPreFilter(name+"_Upsilon", [self.TisTosPreFilter2Upsilon,self.longbothUpsilon])
        self.UpsilonVeloMuProtoPFilter = selMuonPParts(name+"_Upsilon", self.UpsilonTrackingPreFilter)
        self.UpsilonVeloMuPFilter = makeMyMuons(name+"_Upsilon", self.UpsilonVeloMuProtoPFilter)

        self.veloMuonMinusJpsi = chargeFilter(name+'MuonVeloJpsiMinus', trackAlgo = 'VeloMuon', partSource = self.VeloMuPFilter, charge = -1, probeCut = probeCut, tagCut = tagCut)
        self.veloMuonPlusJpsi =  chargeFilter(name+'MuonVeloJpsiPlus',  trackAlgo = 'VeloMuon', partSource = self.VeloMuPFilter, charge = 1,  probeCut = probeCut, tagCut = tagCut)

        self.veloMuonMinusZ = chargeFilter(name+'MuonVeloZMinus', trackAlgo = 'VeloMuon', partSource = self.ZVeloMuPFilter, charge = -1, probeCut = ZProbeCut, tagCut = ZTagCut)
        self.veloMuonPlusZ =  chargeFilter(name+'MuonVeloZPlus',  trackAlgo = 'VeloMuon', partSource = self.ZVeloMuPFilter, charge = 1,  probeCut = ZProbeCut, tagCut = ZTagCut)

        self.veloMuonMinusUpsilon = chargeFilter(name+'MuonVeloUpsilonMinus', trackAlgo = 'VeloMuon', partSource = self.UpsilonVeloMuPFilter, charge = -1, probeCut = UpsilonProbeCut, tagCut = UpsilonTagCut)
        self.veloMuonPlusUpsilon =  chargeFilter(name+'MuonVeloUpsilonPlus',  trackAlgo = 'VeloMuon', partSource = self.UpsilonVeloMuPFilter, charge = 1,  probeCut = UpsilonProbeCut, tagCut = UpsilonTagCut)

        # TAG-AND-PROBE
        self.JpsiMuMuTrackEff1 = makeResonanceVeloMuTrackEff(name + "VeloMuJpsiSel1",
        						   resonanceName = 'J/psi(1S)',
        						   decayDescriptor = 'J/psi(1S) -> mu+ mu-',
        						   plusCharge = self.veloMuonPlusJpsi,
        						   minusCharge = self.longMinusJpsi,
        						   mode = 1,
        						   MassPreComb = config['JpsiMassPreComb'],
        						   VertChi2 = config['JpsiVertChi2'],
        						   MassPostComb = config['JpsiMassPostComb'],
        						   MotherPt = config['JpsiPt'])

        self.JpsiMuMuTrackEff2 = makeResonanceVeloMuTrackEff(name + "VeloMuJpsiSel2",
        						   resonanceName = 'J/psi(1S)',
        						   decayDescriptor = 'J/psi(1S) -> mu+ mu-',
        						   plusCharge = self.longPlusJpsi,
        						   minusCharge = self.veloMuonMinusJpsi,
        						   mode = 2,
        						   MassPreComb = config['JpsiMassPreComb'],
        						   VertChi2 = config['JpsiVertChi2'],
        						   MassPostComb = config['JpsiMassPostComb'],
        						   MotherPt = config['JpsiPt'])

        self.ZMuMuTrackEff1 = makeResonanceVeloMuTrackEff(name + "VeloMuZSel1",
        						  resonanceName = 'Z0',
        						  decayDescriptor = 'Z0 -> mu+ mu-',
        						  plusCharge = self.veloMuonPlusZ,
        						  minusCharge = self.longMinusZ,
        						  mode = 1,
        						  MassPreComb = config['ZMassPreComb'],
        						  VertChi2 = config['ZVertChi2'],
        						  MassPostComb = config['ZMassPostComb'],
        						  MotherPt = config['ZPt'])

        self.ZMuMuTrackEff2 = makeResonanceVeloMuTrackEff(name + "VeloMuZSel2",
        						  resonanceName = 'Z0',
        						  decayDescriptor = 'Z0 -> mu+ mu-',
        						  plusCharge = self.longPlusZ,
        						  minusCharge = self.veloMuonMinusZ,
        						  mode = 2,
        						  MassPreComb = config['ZMassPreComb'],
        						  VertChi2 = config['ZVertChi2'],
        						  MassPostComb = config['ZMassPostComb'],
        						  MotherPt = config['ZPt'])

        self.UpsilonMuMuTrackEff1 = makeResonanceVeloMuTrackEff(name + "VeloMuUpsilonSel1",
        						  resonanceName = 'Upsilon(1S)',
        						  decayDescriptor = 'Upsilon(1S) -> mu+ mu-',
        						  plusCharge = self.veloMuonPlusUpsilon,
        						  minusCharge = self.longMinusUpsilon,
        						  mode = 1,
        						  MassPreComb = config['UpsilonMassPreComb'],
        						  VertChi2 = config['UpsilonVertChi2'],
        						  MassPostComb = config['UpsilonMassPostComb'],
        						  MotherPt = config['UpsilonPt'])

        self.UpsilonMuMuTrackEff2 = makeResonanceVeloMuTrackEff(name + "VeloMuUpsilonSel2",
        						  resonanceName = 'Upsilon(1S)',
        						  decayDescriptor = 'Upsilon(1S) -> mu+ mu-',
        						  plusCharge = self.longPlusUpsilon,
        						  minusCharge = self.veloMuonMinusUpsilon,
        						  mode = 2,
        						  MassPreComb = config['UpsilonMassPreComb'],
        						  VertChi2 = config['UpsilonVertChi2'],
        						  MassPostComb = config['UpsilonMassPostComb'],
        						  MotherPt = config['UpsilonPt'])

        self.nominal_line1 =  StrippingLine(name + 'Line1'
                                            , prescale = config['Prescale']
                                            , postscale = config['Postscale']
                                            , algos=[self.JpsiMuMuTrackEff1]
                                            , RequiredRawEvents = ["Trigger","Muon","Velo","Tracker"]
                                            , HLT1 = "HLT_PASS_RE('%(JpsiHlt1Filter)s')" % config
                                            , HLT2 = "HLT_PASS_RE('%(JpsiHlt2Filter)s')" % config
                                            , MDSTFlag = False)

        self.nominal_line2 =  StrippingLine(name + 'Line2'
                                            , prescale = config['Prescale']
                                            , postscale = config['Postscale']
                                            , algos=[self.JpsiMuMuTrackEff2]
                                            , RequiredRawEvents = ["Trigger","Muon","Velo","Tracker"]
                                            , HLT1 = "HLT_PASS_RE('%(JpsiHlt1Filter)s')" % config
                                            , HLT2 = "HLT_PASS_RE('%(JpsiHlt2Filter)s')" % config
                                            , MDSTFlag = False)

        self.Z_line1 =  StrippingLine(name + 'ZLine1',  prescale = config['ZPrescale'], postscale = config['ZPostscale'], algos=[self.ZMuMuTrackEff1], RequiredRawEvents = ["Trigger","Muon","Velo","Tracker"], MDSTFlag = False)
        self.Z_line2 =  StrippingLine(name + 'ZLine2',  prescale = config['ZPrescale'], postscale = config['ZPostscale'], algos=[self.ZMuMuTrackEff2], RequiredRawEvents = ["Trigger","Muon","Velo","Tracker"], MDSTFlag = False)

        self.Upsilon_line1 =  StrippingLine(name + 'UpsilonLine1',  prescale = config['UpsilonPrescale'], postscale = config['UpsilonPostscale'], algos=[self.UpsilonMuMuTrackEff1], RequiredRawEvents = ["Trigger","Muon","Velo","Tracker"], MDSTFlag = False)
        self.Upsilon_line2 =  StrippingLine(name + 'UpsilonLine2',  prescale = config['UpsilonPrescale'], postscale = config['UpsilonPostscale'], algos=[self.UpsilonMuMuTrackEff2], RequiredRawEvents = ["Trigger","Muon","Velo","Tracker"], MDSTFlag = False)

        self.registerLine(self.nominal_line1)
        self.registerLine(self.nominal_line2)

        self.registerLine(self.Z_line1)
        self.registerLine(self.Z_line2)

        self.registerLine(self.Upsilon_line1)
        self.registerLine(self.Upsilon_line2)

# ########################################################################################
# Make the protoparticles
# ########################################################################################
def selMuonPParts(name, trackingSeq):
   """
       Make ProtoParticles out of VeloMuon tracks
   """
   veloprotos = ChargedProtoParticleMaker(name+"ProtoPMaker")
   veloprotos.Inputs = ["Rec/VeloMuon/"+name+"Tracks"]
   veloprotos.Output = "Rec/ProtoP/ProtoPMaker/"+name+"ProtoParticles"
   veloprotos.addTool( DelegatingTrackSelector, name="TrackSelector" )
   tracktypes = [ "Long" ]
   veloprotos.TrackSelector.TrackTypes = tracktypes
   selector = veloprotos.TrackSelector
   for tsname in tracktypes:
   	selector.addTool(TrackSelector,name=tsname)
   	ts = getattr(selector,tsname)
   	# Set Cuts
   	ts.TrackTypes = [tsname]

   veloprotoseq = GaudiSequencer(name+"ProtoPSeq")
   veloprotoseq.Members += [ veloprotos ]

   return GSWrapper(name="WrappedVeloMuonProtoPSeqFor" + name,
                    sequencer=veloprotoseq,
                    output='Rec/ProtoP/ProtoPMaker/'+name+'ProtoParticles',
                    requiredSelections = [ trackingSeq])
#   return Selection(name+"_SelPParts", Algorithm = veloprotos, OutputBranch="Rec/ProtoP", Extension="ProtoParticles",RequiredSelections=[trackingSeq], InputDataSetter=None)

def makeMyMuons(name, protoParticlesMaker):
   """
     Make Particles out of the muon ProtoParticles
   """
   particleMaker =  NoPIDsParticleMaker(name+"ParticleMaker" , Particle = "Muon")
   particleMaker.Input = "Rec/ProtoP/ProtoPMaker/"+name+"ProtoParticles"
   #particleMaker.OutputLevel = 0

   DataOnDemandSvc().AlgMap.update( {
           "/Event/Phys/" + particleMaker.name() + '/Particles' : particleMaker.getFullName(),
           "/Event/Phys/" + particleMaker.name() + '/Vertices'  : particleMaker.getFullName()
   } )


   return Selection(name+"SelVeloMuonParts", Algorithm = particleMaker, RequiredSelections = [protoParticlesMaker], InputDataSetter=None)

def makeResonanceVeloMuTrackEff(name, resonanceName, decayDescriptor, plusCharge, minusCharge,
                              mode, MassPreComb, MassPostComb, VertChi2, MotherPt):

   """
   Create and return a Resonance -> mu mu Selection object, with one track a long track
   and the other a MuonVelo track.
   Arguments:
   name                 : name of the selection
   resonanceName        : name of the resonance
   decayDescriptor      : decayDescriptor of the decay
   plusCharge           : algorithm for selection positvely charged tracks (cuts done at chargeFilter)
   minusCharge          : algorithm for selection negatively charged tracks (cuts done at chargeFilter)
   mode                 : Tag(-)-and-Probe(+) (1) or  Tag(+)-and-Probe(-) (2)
   MassPreComb          : mass cut befor combining
   MassPostComb         : mass cut after combining
   VertChi2             : vertex chi2 cut
   MotherPt             : p_T of the mother cut [GeV]
   """


   MuonVeloResonance = CombineParticles('_'+name)
   MuonVeloResonance.DecayDescriptor = decayDescriptor
   MuonVeloResonance.OutputLevel = 4


   if(mode == 1):
       #MuonVeloResonance.DaughtersCuts = {"mu+": tagCut,
       #                                   "mu-": probeCut}
        #TODO clean up the cuts at some point
       MuonVeloResonance.CombinationCut = "ADAMASS('%(resonanceName)s')<%(MassPreComb)s*MeV"% locals()
       MuonVeloResonance.MotherCut = "(VFASPF(VCHI2/VDOF)< %(VertChi2)s) & (ADMASS('%(resonanceName)s')<%(MassPostComb)s*MeV) & (PT > %(MotherPt)s*GeV)"% locals()

       return Selection( name, Algorithm = MuonVeloResonance, RequiredSelections = [minusCharge, plusCharge] )

   if(mode == 2):
       #MuonVeloResonance.DaughtersCuts = {"mu-": tagCut  % locals(),
       #                                   "mu+": probeCut  % locals() }

       MuonVeloResonance.CombinationCut = "ADAMASS('%(resonanceName)s')<%(MassPreComb)s*MeV"% locals()
       MuonVeloResonance.MotherCut = "(VFASPF(VCHI2/VDOF)< %(VertChi2)s) & (ADMASS('%(resonanceName)s')<%(MassPostComb)s*MeV) & (PT > %(MotherPt)s*GeV)"% locals()

       return Selection( name, Algorithm = MuonVeloResonance, RequiredSelections = [plusCharge, minusCharge] )

# ########################################################################################
# Charge filter, that filters, well, the charge and takes the particles from the right source (long or Velomuon)
# ########################################################################################
def chargeFilter(name, trackAlgo,  partSource, charge, probeCut, tagCut):
    """
        Select plus or minus charge for Velomuon or long track
    """
    Filter = FilterDesktop() #there is maybe a change needed
    myFilter1 = Filter.configurable("myFilter1")

    if(charge == -1):
        myFilter1.Code = "(Q < 0) & "
    if(charge == 1):
        myFilter1.Code = "(Q > 0) & "

    if(trackAlgo == 'VeloMuon'):
        myFilter1.Code += probeCut
    if(trackAlgo == "LongMu"):
        myFilter1.Code += tagCut
    if(trackAlgo == 'VeloMuon'):
        return Selection( name+'_chargeFilter'+'VeloMuon', Algorithm = myFilter1, RequiredSelections = [  partSource ] )
    if(trackAlgo == 'LongMu'):
        return Selection( name+'_chargeFilter'+'LongMu', Algorithm = myFilter1, RequiredSelections = [  partSource ] )
# ################################################################

# ##########################
# high quality muons
# ##########################
def longtrackFilter(name, trackAlgo, partSource, tagCut):
    """
        Select plus or minus charge for longtrack
    """
    Filter = FilterDesktop() #there is maybe a change needed
    myFilter1 = Filter.configurable("mylongFilter1")

    myFilter1.Code = tagCut
    return Selection( name+'_longFilter'+'LongMu', Algorithm = myFilter1, RequiredSelections = [  partSource ] )
# ################################################################

"""
Define TisTos Prefilters

"""
#getMuonParticles = DataOnDemand(Location = 'Phys/StdLooseMuons')


#def selHlt1Jpsi(name, longPartsFilter):
def selHlt1Jpsi(name, HLT1TisTosSpecs, HLT1PassOnAll):
   """
   Filter the long track muon to be TOS on a HLT1 single muon trigger,
   for J/psi selection
   """
   #Hlt1Jpsi = TisTosParticleTagger(name+"Hlt1Jpsi")
   Hlt1Jpsi = TisTosParticleTagger(
   TisTosSpecs = HLT1TisTosSpecs #{ "Hlt1TrackMuonDecision%TOS" : 0, "Hlt1SingleMuonNoIPL0Decision%TOS" : 0}
   ,ProjectTracksToCalo = False
   ,CaloClustForCharged = False
   ,CaloClustForNeutral = False
   ,TOSFrac = { 4:0.0, 5:0.0 }
   ,NoRegex = True
   )
   Hlt1Jpsi.PassOnAll = HLT1PassOnAll
   #Hlt1Jpsi.PassOnAll = True # TESTING!
   #
   return Selection(name+"_SelHlt1Jpsi", Algorithm = Hlt1Jpsi, RequiredSelections = [ StdLooseMuons ])

#########################################################
def selHlt2Jpsi(name, hlt1Filter, HLT2TisTosSpecs, HLT2PassOnAll):
   """
   Filter the long track muon to be TOS on a HLT2 single muon trigger,
   for J/psi selection
   """
   #Hlt2Jpsi = TisTosParticleTagger("Hlt2Jpsi")
   Hlt2Jpsi = TisTosParticleTagger(
   TisTosSpecs =HLT2TisTosSpecs #{ "Hlt2SingleMuon.*Decision%TOS" : 0}
   ,ProjectTracksToCalo = False
   ,CaloClustForCharged = False
   ,CaloClustForNeutral = False
   ,TOSFrac = { 4:0.0, 5:0.0 }
   ,NoRegex = False
   )
   Hlt2Jpsi.PassOnAll = HLT2PassOnAll
   #Hlt2Jpsi.PassOnAll = True # TESTING!
   #
   return Selection(name+"_SelHlt2Jpsi", Algorithm = Hlt2Jpsi, RequiredSelections = [ hlt1Filter ])
##########################################################


def trackingPreFilter(name, prefilter):

   VeloMuonBuilder1 = VeloMuonBuilder("VeloMuonBuilder"+name)
   VeloMuonBuilder1.OutputLevel = 6
   #VeloMuonBuilder1.MuonLocation = "Rec/Track/"+name+"MuonStandalone"
   #VeloMuonBuilder1.MuonLocation = "Hlt1/Track/MuonSeg"
   VeloMuonBuilder1.MuonLocation = "Rec/Track/"+name+"MuonStandalone"
   VeloMuonBuilder1.VeloLocation = "Rec/Track/"+name+"UnFittedVelo"
   VeloMuonBuilder1.lhcbids = 4
   VeloMuonBuilder1.OutputLocation = "Rec/VeloMuon/"+name+"Tracks"

   preve = TrackPrepareVelo(name+"preve")
   preve.inputLocation = "Rec/"+name+"_Track/Velo"
   preve.outputLocation = "Rec/Track/"+name+"UnFittedVelo"
   preve.bestLocation = ""

   #TODO: apparently FastVelo is now (april 2012) run with fixes in the production which don't neccessarily apply to the stripping...
   alg = GaudiSequencer("VeloMuonTrackingFor"+name,
                         Members = [ FastVeloTracking(name+"FastVelo",OutputTracksName="Rec/"+name+"_Track/Velo"),
				 preve,
				 #StandaloneMuonRec(name+"MuonStandalone", OutputMuonTracksName = "Rec/Track/"+name+"MuonStandalone"), VeloMuonBuilder1])
				 StandaloneMuonRec(name+"MuonStandalone",OutputMuonTracksName="Rec/Track/"+name+"MuonStandalone"), VeloMuonBuilder1])

   return GSWrapper(name="WrappedVeloMuonTracking"+name,
                     sequencer=alg,
                     output='Rec/VeloMuon/'+name+'Tracks',
                     requiredSelections =  prefilter)



class GSWrapper(UniquelyNamedObject,
                ClonableObject,
                SelectionBase) :

    def __init__(self, name, sequencer, output, requiredSelections) :
        UniquelyNamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())
        SelectionBase.__init__(self,
                               algorithm = sequencer,
                               outputLocation = output,
                               requiredSelections = requiredSelections )
