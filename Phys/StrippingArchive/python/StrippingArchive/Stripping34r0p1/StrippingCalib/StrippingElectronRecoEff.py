###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping line to mimic Electron Track Reconstruction Efficiency HLT2 line
Tag and probe with VELO info for J/psi -> ee
'''

__author__=['Adam Davis','Laurent Dufour','V. V. Gligorov']
__date__='14/1/2018'
__version__='$Revision: 1.4 $'

__all__ = (
    'StrippingElectronRecoEffLines',
    'default_config',
    'TOSFilter'
    )

from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from StrippingUtils.Utils import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection, Hlt1Selection, Hlt2Selection, L0Selection #now exist
from PhysConf.Selections import Hlt2TOSSelection
from PhysConf.Selections import Hlt1TOSSelection
from PhysConf.Selections import L0TOSSelection
from PhysSelPython.Wrappers import DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

## for velo tracking
from SelPy.utils import ( UniquelyNamedObject,
                          ClonableObject,
                          SelectionBase )
from Configurables import (DecodeVeloRawBuffer, FastVeloTracking, TrackPrepareVelo, 
                           NoPIDsParticleMaker, DataOnDemandSvc, ChargedProtoParticleMaker, 
                           PrTrackAssociator, DelegatingTrackSelector, TrackContainerCopy, TrackAssociator,
                          TrackStateInitAlg, TrackStateInitTool) 
from TrackFitter.ConfiguredFitters import (ConfiguredEventFitter,
                                           ConfiguredForwardStraightLineEventFitter)
 
default_config = {'NAME': 'ElectronRecoEff',
                  'WGs' : ['Calib'],
                  'BUILDERTYPE':'StrippingElectronRecoEffLines',
                  'CONFIG': {# from HLT line
    'TrackGEC'             : 150,
    # #velo options
    'DoVeloDecoding'       : False,
    "VeloFitter"           : "SimplifiedGeometry",
    "VeloMINIP"            : 0.04 * mm,
    "VeloTrackChi2"        : 5.0,
    "EtaMinVelo"           : 1.9,
    "EtaMaxVelo"           : 5.1,
    #
    'SharedChild'          : {'TrChi2Mu'   :   5,
                              'TrChi2Ele'  :   5,
                              'TrChi2Kaon' :   5,
                              'TrChi2Pion' :   5,
                              'IPMu'       :   0.0 * mm,
                              'IPEle'      :   0.0 * mm,
                              'IPKaon'     :   0.0 * mm,
                              'IPPion'     :   0.0 * mm,
                              'IPChi2Mu'   :   12,
                              'IPChi2Ele'  :   12,
                              'IPChi2Kaon' :   12,
                              'IPChi2Pion' :   36,
                              'EtaMinMu'   :   1.8,
                              'EtaMinEle'  :   1.8,
                              'EtaMaxMu'   :   5.1,
                              'EtaMaxEle'  :   5.1,
                              'ProbNNe'    :   0.2,
                              'ProbNNmu'   :   0.5,
                              'ProbNNk'    :   0.2,
                              'ProbNNpi'   :   0.8,
                              'PtMu'       :   1200 * MeV,
                              'PtEle'      :   1200 * MeV,
                              'PtKaon'     :   500 * MeV,
                              'PtPion'     :   1000 * MeV },
    'LooseSharedChild'     : {'TrChi2Mu'   :   5,
                              'TrChi2Ele'  :   5,
                              'TrChi2Kaon' :   5,
                              'TrChi2Pion' :   5,
                              'IPMu'       :   0.0 * mm,
                              'IPEle'      :   0.0 * mm,
                              'IPKaon'     :   0.0 * mm,
                              'IPPion'     :   0.0 * mm,
                              'IPChi2Mu'   :   8,
                              'IPChi2Ele'  :   8,
                              'EtaMinMu'   :   1.8,
                              'EtaMinEle'  :   1.8,
                              'EtaMaxMu'   :   5.1,
                              'EtaMaxEle'  :   5.1,
                              'ProbNNe'    :   0.2,
                              'ProbNNmu'   :   0.2,
                              'ProbNNk'    :   0.1,
                              'PtMu'       :   1000 * MeV,
                              'PtEle'      :   1000 * MeV,
                              'PtKaon'     :   500 * MeV},
    'DetachedPhi':            {"K_IPCHI2":   7,
                               "K_PIDK":   1,
                               "K_PT":   200,
                               "PHI_PT": 400,
                               "AMMIN":   900,
                               "AMMAX":   1100,
                               "MMIN":  980,
                               "MMAX":  1050,
                               "VCHI2":   100,
                               "PHI_IPCHI2": 4
                                },
    "DetachedKstar":         {"K_PT": 250*MeV,
                              "K_PROBNNk": 0.1,
                              "Pi_PROBNNpi": 0.1,
                              "Pi_PT": 250*MeV,
                              "Pi_IPCHI2": 8,
                              "K_IPCHI2": 8,
                              "APTMIN": 500,
                              "AMMIN": 600*MeV,
                              "AMMAX": 1050*MeV,
                              "MMIN": 750*MeV,
                              "MMAX": 1000*MeV,
                              "VCHI2": 20,
                              "KST_IPCHI2": 4
                              
                              },
    'DetachedEEK'          : {'AMTAP'      :   6000*MeV,
                              'VCHI2TAP'   :   22,
                              'MLOW'       :   5000.*MeV,
                              'MHIGH'      :   5700.*MeV,
                              'bmass_ip_constraint': -2.5,
                              'overlapCut' :   0.95,
                              'probePcutMin' : 750*MeV,
                              'probePcutMax' : 150000*MeV,
                              'TisTosSpec' :   {"Hlt1TrackMVA.*Decision%TOS":0}
                              },
    'DetachedEEKstar'          : {'AMTAP'      :   6000*MeV,
                              'VCHI2TAP'   :   22,
                              'MLOW'       :   5000.*MeV,
                              'MHIGH'      :   5700.*MeV,
                              'bmass_ip_constraint': -2.0,
                              'overlapCut' :   0.95,
                              'probePcutMin' : 750*MeV,
                              'probePcutMax' : 150000*MeV,
                              'TisTosSpec' :   {"Hlt1TrackMVA.*Decision%TOS":0}
                              },
    'DetachedEEPhi'          : {'AMTAP'      :   6000*MeV,
                              'VCHI2TAP'   :   22,
                              'MLOW'       :   5000.*MeV,
                              'MHIGH'      :   5700.*MeV,
                              'bmass_ip_constraint': -2.0,
                              'overlapCut' :   0.95,
                              'probePcutMin' : 750*MeV,
                              'probePcutMax' : 150000*MeV,
                              'TisTosSpec' :   {"Hlt1TrackMVA.*Decision%TOS":0}
                              },
    'DetachedMuMuK'        : {'AMTAP'      :   6000*MeV,
                              'VCHI2TAP'   :   20,
                              'MLOW'       :   5000.*MeV,                              
                              'MHIGH'      :   5700.*MeV,
                              'bmass_ip_constraint': -2.75,
                              'probePcutMin' : 750*MeV,
                              'probePcutMax' : 150000*MeV,
                              "POINTINGMASSMAX" : 7200*MeV,
                              "POINTINGMASSMIN": 3200*MeV,
                              'TisTosSpec' :   {"Hlt1Track(Muon)?MVA.*Decision%TOS":0}
                              },
    'DetachedMuMuKstar'        : {'AMTAP'      :   6000*MeV,
                              'VCHI2TAP'   :   20,
                              'MLOW'       :   5000.*MeV,                              
                              'MHIGH'      :   5700.*MeV,
                              'bmass_ip_constraint': -2.5,
                              'probePcutMin' : 750*MeV,
                              'probePcutMax' : 150000*MeV,
                              "POINTINGMASSMAX" : 7000*MeV,
                              "POINTINGMASSMIN": 3500*MeV,
                              'TisTosSpec' :   {"Hlt1Track(Muon)?MVA.*Decision%TOS":0}
                              },
    'DetachedMuMuPhi'        : {'AMTAP'      :   6000*MeV,
                              'VCHI2TAP'   :   20,
                              'MLOW'       :   5000.*MeV,                              
                              'MHIGH'      :   5700.*MeV,
                              'bmass_ip_constraint': -1.0,
                              'probePcutMin' : 750*MeV,
                              'probePcutMax' : 150000*MeV,
                              "POINTINGMASSMAX" : 7000*MeV,
                              "POINTINGMASSMIN": 3500*MeV,
                              'TisTosSpec' :   {"Hlt1Track(Muon)?MVA.*Decision%TOS":0}
                              },
    'DetachedMuK'          : {'AM'         :   5400*MeV,
                              'VCHI2'      :   10,
                              'VDCHI2'     :   100,
                              'DIRA'       :   0.95,
                              "bCandFlightDist"          : 4.0*mm,
                              'TisTosSpec' :   {"Hlt1Track(Muon)?MVA.*Decision%TOS":0}
                              },
    'DetachedMuPhi'          : {'AM'         :   5400*MeV,
                              'VCHI2'      :   10,
                              'VDCHI2'     :   100,
                              'DIRA'       :   0.95,
                              "bCandFlightDist"          : 3.0*mm,
                              'TisTosSpec' :   {"Hlt1Track(Muon)?MVA.*Decision%TOS":0}
                              },
    'DetachedMuKstar'          : {'AM'         :   5400*MeV,
                              'VCHI2'      :   10,
                              'VDCHI2'     :   100,
                              'DIRA'       :   0.95,
                              "bCandFlightDist"          : 3.0*mm,
                              'TisTosSpec' :   {"Hlt1Track(Muon)?MVA.*Decision%TOS":0}
                              },

    'DetachedEK'           : {'AM'         :   5500*MeV,
                              'VCHI2'      :   10,
                              'VDCHI2'     :   36,
                              'DIRA'       :   0.95,
                              "bCandFlightDist"          : 4.0*mm,
                              'TisTosSpec' :   {"Hlt1TrackMVA.*Decision%TOS":0}
                              },
    
    'DetachedEKstar'           : {'AM'         :   5500*MeV,
                              'VCHI2'      :   10,
                              'VDCHI2'     :   36,
                              'DIRA'       :   0.95,
                              "bCandFlightDist"          : 3.0*mm,
                              'TisTosSpec' :   {"Hlt1TrackMVA.*Decision%TOS":0}
                              },
    'DetachedEPhi'           : {'AM'         :   5500*MeV,
                              'VCHI2'      :   10,
                              'VDCHI2'     :   36,
                              'DIRA'       :   0.90,
                              "bCandFlightDist"          : 3.0*mm,
                              'TisTosSpec' :   {"Hlt1TrackMVA.*Decision%TOS":0}
                              },
    'L0TOS'                : {"DetachedMuK": "L0MuonDecision",
                              "DetachedEK": "L0ElectronDecision"},
    # The next decisions are "DECs"
    'L0Req'                : {'DetachedEK'   : "L0_CHANNEL_RE('Electron')", # this is TOS!
                              'DetachedMuK'  : "L0_CHANNEL_RE('Muon|Hadron')", # this is TOS!
                              
                              "DetachedMuKstar": "L0_CHANNEL_RE('Muon|Hadron')", # this is DEC!
                              "DetachedMuPhi": "L0_CHANNEL_RE('Muon|Hadron')", # this is DEC!
                              
                              "DetachedEKstar" :"L0_CHANNEL_RE('Electron')", # this is DEC!
                              "DetachedEPhi" :"L0_CHANNEL_RE('Muon|Electron|Hadron')" }, # this is DEC!
    
    'Hlt1Req'              : {'DetachedEKstar'  : "Hlt1TrackMVA.*Decision",
                              'DetachedEPhi'  : "Hlt1TrackMVA.*Decision",
                              'DetachedEK'   : "Hlt1TrackMVA.*Decision",
                              'DetachedMuPhi': "Hlt1Track*MVA.*Decision",
                              'DetachedMuKstar': "Hlt1Track*MVA.*Decision",
                              'DetachedMuK'  : "Hlt1Track*MVA.*Decision"
                              },
    
    'Hlt2Req'               : {'DetachedEK' : "Hlt2Topo(E)?2BodyDecision",
                               'DetachedEKstar' : "Hlt2Topo(E)?2BodyDecision",
                               'DetachedEPhi' : "Hlt2Topo(E)?2BodyDecision",
                               'DetachedMuK' : "Hlt2Topo(Mu)?2BodyDecision",
                               'DetachedMuKstar' : "Hlt2Topo(Mu)?2BodyDecision",
                               'DetachedMuPhi' : "Hlt2Topo(Mu)?2BodyDecision",
                               }
                  
    },
              'STREAMS' : ['BhadronCompleteEvent']
              }



from StandardParticles import StdNoPIDsMuons as Hlt2Muons
from StandardParticles import StdNoPIDsElectrons as Hlt2Electrons
from StandardParticles import StdAllLooseKaons as Hlt2Kaons
from StandardParticles import StdAllLoosePions as Hlt2Pions

class StrippingElectronRecoEffLines(LineBuilder):
    """
    Stripping 'replica' for HLT electron reconstruction efficiency trigger
    """

    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self,name,config):        
        LineBuilder.__init__(self, name, config)
        
        from PhysSelPython.Wrappers import Selection, DataOnDemand
        self._config = config
        self.name = name
        
        self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(TrackGEC)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}

        self._DetachedElectrons=None
        self._LooseDetachedElectrons=None
        self._LooseDetachedMuons=None
        self._DetachedKaons=None
        self._DetachedMuons = None
        
        # Composite tags
        self._DetachedKstars=False
        self._DetachedPhis=None
        
        ###### the velo tracking (from D2K3pi line from mika)        
        self.VeloProtoOutputLocation = 'Rec/ProtoP/VeloProtosFor%s'%self.name
        self.VeloTrackOutputLocation="Rec/Track/MyVeloFor%s"%self.name
        self.FittedVeloTrackOutputLocation = "Rec/Track/PreparedVeloFor%s"%self.name        
        self.VeloTracks = self.MakeVeloTracks([])
        
        self.Hlt2ProbeElectrons = self.MakeVeloParticles("VeloElectrons","electrons",self.VeloTracks)
        self.Hlt2ProbeMuons = self.MakeVeloParticles("VeloMuons","muons",self.VeloTracks)
        
        ###rest of it.
        self.DetachedEKPair("DetachedEKPair")
        self.DetachedEKstarPair("DetachedEKstarPair")
        self.DetachedEPhiPair("DetachedEPhiPair")
        
        self.DetachedMuKPair("DetachedMuKPair")
        self.DetachedMuKstarPair("DetachedMuKstarPair")
        self.DetachedMuPhiPair("DetachedMuPhiPair")
        
        #actual registration of the lines
        self.registerLine( self.DetachedEEKPair("DetachedEEKPair") )
        self.registerLine( self.DetachedEEKstarPair("DetachedEEKstarPair") )
        self.registerLine( self.DetachedEEPhiPair("DetachedEEPhiPair") );
        
        self.registerLine(self.DetachedMuMuKPair("DetachedMuMuKPair"))
        self.registerLine(self.DetachedMuMuKstarPair("DetachedMuMuKstarPair"))
        self.registerLine(self.DetachedMuMuPhiPair("DetachedMuMuPhiPair"))
        
        self.combine_particles_mumuk = None;
        self.combine_particles_eek = None;


    def _DetachedElectronFilter(self):
        if not self._DetachedElectrons:
            code = "(MIPCHI2DV(PRIMARY)>%(IPChi2Ele)s) & (PT> %(PtEle)s) & \
            (TRCHI2DOF<%(TrChi2Ele)s)  & in_range(%(EtaMinEle)s, ETA, %(EtaMaxEle)s) & (PROBNNe > %(ProbNNe)s)"%self._config['SharedChild']
            _DetachedElectrons = Selection("DetachedElectrons_For_"+self.name,
                                           Algorithm = FilterDesktop(Code = code),
                                           RequiredSelections = [Hlt2Electrons]
                                           )
            self._DetachedElectrons  = _DetachedElectrons
        return self._DetachedElectrons

    def _LooseDetachedElectronFilter(self):
        if not self._LooseDetachedElectrons:
            code = "(MIPCHI2DV(PRIMARY)>%(IPChi2Ele)s) & (PT> %(PtEle)s) & " \
            "(TRCHI2DOF<%(TrChi2Ele)s)  & " \
            "in_range(%(EtaMinEle)s, ETA, %(EtaMaxEle)s) &" \
            " (PROBNNe > %(ProbNNe)s)" % self._config['LooseSharedChild']
            _DetachedElectrons = Selection("LooseDetachedElectrons_For_"+self.name,
                                           Algorithm = FilterDesktop(Code = code),
                                           RequiredSelections = [Hlt2Electrons]
                                           )
            self._LooseDetachedElectrons  = _DetachedElectrons
        return self._LooseDetachedElectrons
    
    def _DetachedKaonFilter(self):
        if not self._DetachedKaons:
            code = ("(MIPCHI2DV(PRIMARY)>%(IPChi2Kaon)s) & (PT> %(PtKaon)s) & \
            (TRCHI2DOF<%(TrChi2Kaon)s) & (PROBNNk > %(ProbNNk)s)")%self._config['SharedChild']
            _DetachedKaons = Selection("DetachedKaons_For_"+self.name,
                                       Algorithm = FilterDesktop(Code = code),
                                       RequiredSelections = [Hlt2Kaons])
            self._DetachedKaons = _DetachedKaons
        return self._DetachedKaons

    def _DetachedKstarFilter(self):
        if not self._DetachedKstars:
            dc = {'K-' : "(MIPCHI2DV(PRIMARY)>%(K_IPCHI2)s) & (PROBNNk>%(K_PROBNNk)s) & (PT>%(K_PT)s)" % (self._config['DetachedKstar']), 
                  'pi+' : "(MIPCHI2DV(PRIMARY)>%(Pi_IPCHI2)s) & (PROBNNpi>%(Pi_PROBNNpi)s) & (PT>%(Pi_PT)s)" % (self._config['DetachedKstar'])}
            cc = ("in_range(%(AMMIN)s, AM, %(AMMAX)s) & (APT>%(APTMIN)s)") % self._config['DetachedKstar']
            mc = ("in_range(%(MMIN)s, M, %(MMAX)s) & (VFASPF(VCHI2) < %(VCHI2)s) & (MIPCHI2DV(PRIMARY)>%(KST_IPCHI2)s)") % self._config['DetachedKstar']        
            _DetachedKstarCreator = CombineParticles(
                DecayDescriptors = ['[K*(892)0 -> K- pi+]cc'],
                DaughtersCuts = dc,
                CombinationCut = cc,
                MotherCut = mc,
                )
            
            _DetachedKstars = Selection("DetachedKstars_For_"+self.name,
                                       Algorithm = _DetachedKstarCreator,
                                       RequiredSelections = [Hlt2Kaons, Hlt2Pions])
            self._DetachedKstars = _DetachedKstars
            
        return self._DetachedKstars
    
    def _DetachedPhiFilter(self):
        if not self._DetachedPhis:
            from PhysSelPython.Wrappers import Selection
            
            dc = {'K-' : ("(MIPCHI2DV(PRIMARY)>%(K_IPCHI2)s) & "\
                         "(PIDK>%(K_PIDK)s) & (PT>%(K_PT)s)") % (self._config['DetachedPhi'])
                  }
            cc = ("in_range(%(AMMIN)s,AM,%(AMMAX)s) & (APT>%(PHI_PT)s)") % (self._config['DetachedPhi'])
            mc = ("in_range(%(MMIN)s,M,%(MMAX)s) & " \
                    "(VFASPF(VCHI2) < %(VCHI2)s) & "\
                    " (MIPCHI2DV(PRIMARY)>%(PHI_IPCHI2)s)") % (self._config['DetachedPhi'])
            _DetachedPhiCreator = CombineParticles(
                DecayDescriptors = ['phi(1020) -> K- K+'],
                DaughtersCuts = dc,
                CombinationCut = cc,
                MotherCut = mc,
                )
            _DetachedPhis = Selection("DetachedPhis_For_"+self.name,
                                       Algorithm = _DetachedPhiCreator,
                                       RequiredSelections = [Hlt2Kaons])
            self._DetachedPhis = _DetachedPhis
        
        return self._DetachedPhis
    
    def DetachedEKPair(self,_name):
        dc = {'K+' : "ALL", 'e+' : "ALL"}
        cc = ("(AM < %(AM)s)")%self._config['DetachedEK']
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) &  (BPVVDCHI2 > %(VDCHI2)s) " \
              " & (VFASPF(VMINVDDV(PRIMARY)) > %(bCandFlightDist)s )") % self._config['DetachedEK']
        
        _DetachedEKPair = CombineParticles(
            DecayDescriptors = ['[J/psi(1S) -> e+ K-]cc',
                                '[J/psi(1S) -> e+ K+]cc'],
            DaughtersCuts = dc,
            CombinationCut = cc,
            MotherCut = mc,
            )
        
        _DetachedEKPairSel = Selection("SelKE_for_"+_name,
                                            Algorithm = _DetachedEKPair,
                                            RequiredSelections = [self._DetachedKaonFilter(),
                                                                  self._DetachedElectronFilter()
                                                                  ]
                                            )
        L0Filtered_DetachedEKPairSel = L0TOSSelection(_DetachedEKPairSel, 
                                            self._config["L0TOS"]['DetachedEK'],
                                            name='EK_L0Decision_for_'+_name)
        
        EK_HLT1Selection = Hlt1TOSSelection(L0Filtered_DetachedEKPairSel, 
                                            self._config["Hlt1Req"]['DetachedEK'],
                                            name='EK_HLT1Selection_for_'+_name)
        EK_HLT2Selection = Hlt2TOSSelection(EK_HLT1Selection,
                                            self._config["Hlt2Req"]['DetachedEK'],
                                            name='EK_HLT2Selection_for_'+_name)
        
        self._DetachedEKPairSel = EK_HLT2Selection;
        
    def DetachedEKstarPair(self,_name):
        dc = {'e+' : "ALL"}
        cc = ("(AM < %(AM)s)") % self._config['DetachedEKstar']
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) " \
              " & (BPVVDCHI2 > %(VDCHI2)s) " \
              " & (VFASPF(VMINVDDV(PRIMARY)) > %(bCandFlightDist)s )") % self._config['DetachedEKstar']

        _DetachedEKstarPair = CombineParticles(
            DecayDescriptors = ["[J/psi(1S) -> e+ K*(892)0]cc",
                                "[J/psi(1S) -> e- K*(892)0]cc"],
            DaughtersCuts = dc,
            CombinationCut = cc,
            MotherCut = mc,            
            )
        EKstar_L0sel = L0Selection('EKstar_L0Selection_for_'+_name,
                                   '%(DetachedEKstar)s' % self._config['L0Req'])
        
        L0Filtered_DetachedEKstarPairSel = Selection("SelEKstar_for_"+_name,
                                             Algorithm = _DetachedEKstarPair,
                                             RequiredSelections = [self._DetachedKstarFilter(),
                                                                   self._DetachedElectronFilter(),
                                                                   EKstar_L0sel
                                                                   ]
                                             )
        
        EKstar_HLT1Selection = Hlt1TOSSelection(L0Filtered_DetachedEKstarPairSel, 
                                            self._config["Hlt1Req"]['DetachedEKstar'],
                                            name='EKstar_HLT1Selection_for_'+_name)
        EKstar_HLT2Selection = Hlt2TOSSelection(EKstar_HLT1Selection,
                                            self._config["Hlt2Req"]['DetachedEKstar'],
                                            name='EKstar_HLT2Selection_for_'+_name)
        
        self._DetachedEKstarPairSel = EKstar_HLT2Selection
    
    
    def DetachedEPhiPair(self,_name):
        dc = {'e+' : "ALL"}
        cc = ("(AM < %(AM)s)") % self._config['DetachedEPhi']
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) " \
              " & (BPVVDCHI2 > %(VDCHI2)s) " \
              " & (VFASPF(VMINVDDV(PRIMARY)) > %(bCandFlightDist)s )") % self._config['DetachedEPhi']

        _DetachedEPhiPair = CombineParticles(
            DecayDescriptors = ["[J/psi(1S) -> e+ phi(1020)]cc",
                                "[J/psi(1S) -> e- phi(1020)]cc"],
            DaughtersCuts = dc,
            CombinationCut = cc,
            MotherCut = mc,
            )
        EPhi_L0sel = L0Selection('EPhi_L0Selection_for_'+_name,
                                 '%(DetachedEPhi)s' % self._config['L0Req'])
        
        L0Filtered_DetachedEPhiPairSel = Selection("SelEPhi_for_"+_name,
                                             Algorithm = _DetachedEPhiPair,
                                             RequiredSelections = [self._DetachedPhiFilter(),
                                                                   self._DetachedElectronFilter(),
                                                                   EPhi_L0sel
                                                                   ]
                                             )
        
        EPhi_HLT1Selection = Hlt1TOSSelection(L0Filtered_DetachedEPhiPairSel, 
                                            self._config["Hlt1Req"]['DetachedEPhi'],
                                            name='EPhi_HLT1Selection_for_'+_name)
        EPhi_HLT2Selection = Hlt2TOSSelection(EPhi_HLT1Selection,
                                            self._config["Hlt2Req"]['DetachedEPhi'],
                                            name='EPhi_HLT2Selection_for_'+_name)
        
        self._DetachedEPhiPairSel = EPhi_HLT2Selection
        
    def getCPForEEK(self, config_key="DetachedEEK"):
        dc = {"e+" : "ALL", "J/psi(1S)" : "ALL"}
        cc = "(AM < %(AMTAP)s)" % self._config[config_key]
        mc = "(VFASPF(VCHI2) < %(VCHI2TAP)s) & " \
             "(log(B_MASS_CONSTRAINT_IP) < %(bmass_ip_constraint)s) & " \
             "in_range(%(MLOW)s, BMassFromConstraint, %(MHIGH)s) & " \
             "in_range(%(probePcutMin)s,Probe_Momentum_From_Mass_constraint,%(probePcutMax)s)" % self._config[config_key]
        # "& (MAXOVERLAP( (ABSID == 'e+') | (ABSID=='K-') ) < %(overlapCut)s)"
        
        preambulo = [  # With thanks to L. Dufour!
            'from numpy import inner',  # if you want you can also calculate all inner products yourself.
            'TagKaonMomentumVector    = [CHILD(CHILD(PX,2), 1), CHILD(CHILD(PY,2),1), CHILD(CHILD(PZ,2),1)]',
            'TagKaonEnergy            = CHILD(CHILD(E,2),1)',
            #
            'TagElectronMomentumVector    = [CHILD(CHILD(PX,1), 1), CHILD(CHILD(PY,1),1), CHILD(CHILD(PZ,1),1)]',
            'TagElectronEnergy = CHILD(CHILD(E,1), 1)',
            #
            'ProbeElectron    = [CHILD(PX,2),CHILD(PY,2),CHILD(PZ,2)]',
            'ProbeUnnormalised = ProbeElectron[:]',
            'ProbeElectron = [ProbeUnnormalised[i]/CHILD(P,2) for i in range (0,3)]',
            #
            'TagPCosineTheta = inner(ProbeElectron, TagElectronMomentumVector)',  # |p_tag| Cos(Theta)
            #
            'Electron_M = 0.511',  # in MeV
            #
            # ideally would replace the 3096.9 with a functor to get the PDG mass for the J/Psi(1S) in MeV
            # (there must be a functor for this PDG mass...)
            'Probe_Momentum_From_Mass_constraint = 0.5*(3096.9**2 - Electron_M**2 - Electron_M**2)/(TagElectronEnergy - TagPCosineTheta)',
            'JPsi_momentum = [ Probe_Momentum_From_Mass_constraint*ProbeElectron[i] + TagElectronMomentumVector[i] for i in range (0,3)]',
            'JPsi_energy = TagElectronEnergy + math.sqrt(Electron_M**2 + Probe_Momentum_From_Mass_constraint**2)',
            'BMassFromConstraint = math.sqrt( (JPsi_energy+TagKaonEnergy)**2 - (JPsi_momentum[0]+TagKaonMomentumVector[0])**2 -(JPsi_momentum[1]+TagKaonMomentumVector[1])**2 - (JPsi_momentum[2]+TagKaonMomentumVector[2])**2)',
            # #new from Laurent
            "mass_constraint_b_momentum_vector = [ TagKaonMomentumVector[i] + JPsi_momentum[i] for i in range (0,3)]",
            "mass_constraint_b_momentum = math.sqrt(mass_constraint_b_momentum_vector[0]**2 + mass_constraint_b_momentum_vector[1]**2 + mass_constraint_b_momentum_vector[2]**2)",
            "normalised_mass_constraint_b_momentum_vector = [mass_constraint_b_momentum_vector[i]/mass_constraint_b_momentum for i in range (0,3)]",
            "B_ENDVERTEX_POSITION = [VFASPF(VX), VFASPF(VY), VFASPF(VZ)]",
            "B_PV_POSITION = [BPV(VX),BPV(VY),BPV(VZ)]",
            "LambdaFactor = - (inner([VFASPF(VX)-BPV(VX), VFASPF(VY)-BPV(VY), VFASPF(VZ)-BPV(VZ)], normalised_mass_constraint_b_momentum_vector))",
            "B_MASS_CONSTRAINT_IP_VECTOR = [(B_ENDVERTEX_POSITION[i] + LambdaFactor * normalised_mass_constraint_b_momentum_vector[i]) - B_PV_POSITION[i] for i in range (0,3)]",
            "B_MASS_CONSTRAINT_IP = math.sqrt(B_MASS_CONSTRAINT_IP_VECTOR[0]**2+B_MASS_CONSTRAINT_IP_VECTOR[1]**2+B_MASS_CONSTRAINT_IP_VECTOR[2]**2)",
            ]
        
        return CombineParticles(
            DecayDescriptors=["B+ -> J/psi(1S) e+", "B- -> J/psi(1S) e-"],
            DaughtersCuts=dc,
            CombinationCut=cc,
            MotherCut=mc,
            Preambulo=preambulo
            )
        

    def DetachedEEKPair(self,_name):
        configuration = self._config[ "DetachedEEK" ]
        
        _DetachedEEKPair= self.getCPForEEK("DetachedEEK")
        _DetachedEEKPair_Sel = Selection("SelEEK_for_"+_name,
                                         Algorithm = _DetachedEEKPair,
                                         RequiredSelections = [self._DetachedEKPairSel,
                                                               self.Hlt2ProbeElectrons
                                                               ]
                                         )
        
        line = StrippingLine(_name+"Line",
                             prescale = 1.0,
                             FILTER = self.GECs,
                             RequiredRawEvents = ["Velo","Calo"],
                             selection = _DetachedEEKPair_Sel
                             )

        return line

    def DetachedEEKstarPair(self,_name):
        configuration = self._config[ "DetachedEEKstar" ]
        
        _DetachedEEKstarPair = self.getCPForEEK("DetachedEEKstar")
        _DetachedEEKstarPair_Sel = Selection("SelEEKstar_for_"+_name,
                                         Algorithm = _DetachedEEKstarPair,
                                         RequiredSelections = [self._DetachedEKstarPairSel,
                                                               self.Hlt2ProbeElectrons
                                                               ]
                                         )
        
        line = StrippingLine(_name+"Line",
                             prescale = 1.0,
                             FILTER = self.GECs,
                             RequiredRawEvents = ["Velo","Calo"],
                             selection = _DetachedEEKstarPair_Sel
                             )

        return line

    def DetachedEEPhiPair(self,_name):
        configuration = self._config[ "DetachedEEPhi" ]
        
        _DetachedEEPhiPair = self.getCPForEEK("DetachedEEPhi")
        _DetachedEEPhiPair_Sel = Selection("SelEEPhi_for_"+_name,
                                         Algorithm = _DetachedEEPhiPair,
                                         RequiredSelections = [self._DetachedEPhiPairSel,
                                                               self.Hlt2ProbeElectrons
                                                               ]
                                         )
        
        line = StrippingLine(_name+"Line",
                             prescale = 1.0,
                             FILTER = self.GECs,
                             RequiredRawEvents = ["Velo","Calo"],
                             selection = _DetachedEEPhiPair_Sel
                             )

        return line

    def _DetachedMuonFilter(self):
        if not self._DetachedMuons:
            from PhysSelPython.Wrappers import Selection
            
            code = ("(MIPDV(PRIMARY)>%(IPMu)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Mu)s) & (PT> %(PtMu)s) & \
            (TRCHI2DOF<%(TrChi2Mu)s)  & in_range(%(EtaMinMu)s, ETA, %(EtaMaxMu)s) & (PROBNNmu > %(ProbNNmu)s)")%self._config['SharedChild']
            
            _DetachedMuons= Selection("DetachedMuons_For_"+self.name,
                                      Algorithm = FilterDesktop(Code = code),
                                      RequiredSelections = [Hlt2Muons])
            self._DetachedMuons = _DetachedMuons
        
        return self._DetachedMuons
    
    def _LooseDetachedMuonFilter(self):
        if not self._LooseDetachedMuons:
            code = ("(MIPDV(PRIMARY)>%(IPMu)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Mu)s) & (PT> %(PtMu)s) & \
            (TRCHI2DOF<%(TrChi2Mu)s)  & in_range(%(EtaMinMu)s, ETA, %(EtaMaxMu)s) " \
            "& (PROBNNmu > %(ProbNNmu)s)")%self._config['LooseSharedChild']
            
            _DetachedMuons = Selection("LooseDetachedMuons_For_"+self.name,
                                      Algorithm = FilterDesktop(Code = code),
                                      RequiredSelections = [Hlt2Muons])
            self._LooseDetachedMuons = _DetachedMuons
        
        return self._LooseDetachedMuons
    
    def DetachedMuKPair(self,_name):
        dc = {'K+' : "ALL", 'mu+' : "ALL"}
        cc = ("(AM < %(AM)s)")%self._config['DetachedMuK']
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) " \
              "& (BPVVDCHI2 > %(VDCHI2)s) & " \
              "(VFASPF(VMINVDDV(PRIMARY)) > %(bCandFlightDist)s )") % self._config['DetachedMuK']

        _DetachedMuKPair = CombineParticles(
            DecayDescriptors = ["[J/psi(1S) -> mu+ K-]cc","[J/psi(1S) -> mu+ K+]cc"],
            DaughtersCuts = dc,
            CombinationCut = cc,
            MotherCut = mc,
            )
        
        NoTrigger_DetachedMuKPairSel = Selection("SelMuK_for_"+_name,
                                             Algorithm = _DetachedMuKPair,
                                             RequiredSelections = [self._DetachedKaonFilter(),
                                                                   self._DetachedMuonFilter()
                                                                   ]
                                             )
        
        MuK_L0TOSSelection = L0TOSSelection(NoTrigger_DetachedMuKPairSel, 
                                            self._config["L0TOS"]['DetachedMuK'],
                                            name='MuK_L0TOSSelection_for_'+_name)
        
        MuK_HLT1Selection = Hlt1TOSSelection(MuK_L0TOSSelection, 
                                            self._config["Hlt1Req"]['DetachedMuK'],
                                            name='MuK_HLT1Selection_for_'+_name)
        
        MuK_HLT2Selection = Hlt2TOSSelection(MuK_HLT1Selection,
                                            self._config["Hlt2Req"]['DetachedMuK'],
                                            name='MuK_HLT2Selection_for_'+_name)
        
        self._DetachedMuKPairSel = MuK_HLT2Selection
       
    def DetachedMuKstarPair(self,_name):
        dc = {'mu+' : "ALL"}
        cc = ("(AM < %(AM)s)") % self._config['DetachedMuKstar']
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) " \
              " & (BPVVDCHI2 > %(VDCHI2)s)  " \
              " & (VFASPF(VMINVDDV(PRIMARY)) > %(bCandFlightDist)s )") % self._config['DetachedMuKstar']

        _DetachedMuKstarPair = CombineParticles(
            DecayDescriptors = ["[J/psi(1S) -> mu+ K*(892)0]cc",
                                "[J/psi(1S) -> mu- K*(892)0]cc"],
            DaughtersCuts = dc,
            CombinationCut = cc,
            MotherCut = mc,            
            )
        MuKstar_L0sel = L0Selection('MuKstar_L0Selection','%(DetachedMuKstar)s' % self._config['L0Req'])
        
        L0Filtered_DetachedMuKstarPairSel = Selection("SelMuKstar_for_"+_name,
                                             Algorithm = _DetachedMuKstarPair,
                                             RequiredSelections = [self._DetachedKstarFilter(),
                                                                   self._DetachedMuonFilter(),
                                                                   MuKstar_L0sel
                                                                   ]
                                             )
        
        MuKstar_HLT1Selection = Hlt1TOSSelection(L0Filtered_DetachedMuKstarPairSel, 
                                            self._config["Hlt1Req"]['DetachedMuKstar'],
                                            name='MuKstar_HLT1Selection')
        
        MuKstar_HLT2Selection = Hlt2TOSSelection(MuKstar_HLT1Selection,
                                            self._config["Hlt2Req"]['DetachedMuKstar'],
                                            name='MuKstar_HLT2Selection')
        
        self._DetachedMuKstarPairSel = MuKstar_HLT2Selection
    
    def DetachedMuPhiPair(self,_name):
        dc = {'mu+' : "ALL"}
        cc = ("(AM < %(AM)s)") % self._config['DetachedMuK']
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & "\
              " (BPVVDCHI2 > %(VDCHI2)s) & " \
              "(VFASPF(VMINVDDV(PRIMARY)) > %(bCandFlightDist)s )") % self._config['DetachedMuPhi']

        _DetachedMuPhiPair = CombineParticles(
            DecayDescriptors = ["[J/psi(1S) -> mu+ phi(1020)]cc",
                                "[J/psi(1S) -> mu- phi(1020)]cc"],
            DaughtersCuts = dc,
            CombinationCut = cc,
            MotherCut = mc,
            )
        
        L0sel = L0Selection('MuPhi_L0Selection','%(DetachedMuPhi)s' % self._config['L0Req'])
        L0Filtered_DetachedMuPhiPairSel = Selection("SelMuPhi_for_"+_name,
                                             Algorithm = _DetachedMuPhiPair,
                                             RequiredSelections = [self._DetachedPhiFilter(),
                                                                   self._LooseDetachedMuonFilter(),
                                                                   L0sel
                                                                   ]
                                             )
        
        MuPhi_HLT1Selection = Hlt1TOSSelection(L0Filtered_DetachedMuPhiPairSel, 
                                            self._config["Hlt1Req"]['DetachedMuPhi'],
                                            name='MuPhi_HLT1Selection_for_'+_name)
        MuPhi_HLT2Selection = Hlt2TOSSelection(MuPhi_HLT1Selection,
                                            self._config["Hlt2Req"]['DetachedMuPhi'],
                                            name='MuPhi_HLT2Selection_for_'+_name)
        
        self._DetachedMuPhiPairSel = MuPhi_HLT2Selection
    
    def getCPForMuMuK(self, config_key="DetachedMuMuK"):
        dc = {"mu+" : "ALL", "J/psi(1S)" : "ALL"}
        cc = "(AM < %(AMTAP)s)"%self._config[config_key]
        mc = "(VFASPF(VCHI2) < %(VCHI2TAP)s) & (log(B_MASS_CONSTRAINT_IP) < %(bmass_ip_constraint)s) & in_range(%(MLOW)s, BMassFromConstraint, %(MHIGH)s) & in_range(%(probePcutMin)s,Probe_Momentum_From_Mass_constraint,%(probePcutMax)s)"%self._config[config_key]        
        preambulo =  [ # With thanks to L. Dufour!
            'from numpy import inner', #if you want you can also calculate all inner products yourself.
            'TagKaonMomentumVector    = [CHILD(CHILD(PX,2), 1), CHILD(CHILD(PY,2),1), CHILD(CHILD(PZ,2),1)]',
            'TagKaonEnergy            = CHILD(CHILD(E,2),1)',
            #
            'TagMuonMomentumVector    = [CHILD(CHILD(PX,1), 1), CHILD(CHILD(PY,1),1), CHILD(CHILD(PZ,1),1)]',
            'TagMuonEnergy = CHILD(CHILD(E,1), 1)',
            #
            'ProbeMuon    = [CHILD(PX,2),CHILD(PY,2),CHILD(PZ,2)]',
            'ProbeUnnormalised = ProbeMuon[:]',
            'ProbeMuon = [ProbeUnnormalised[i]/CHILD(P,2) for i in range (0,3)]',
            #
            'TagPCosineTheta = inner(ProbeMuon, TagMuonMomentumVector)', # |p_tag| Cos(Theta)
            #
            'Muon_M = 105.658', # in MeV
            #
            # ideally would replace the 3096.9 with a functor to get the PDG mass for the J/Psi(1S) in MeV
            # (there must be a functor for this PDG mass...)
            'Probe_Momentum_From_Mass_constraint = 0.5*(3096.9**2 - Muon_M**2 - Muon_M**2)/(TagMuonEnergy - TagPCosineTheta)',
            'JPsi_momentum = [ Probe_Momentum_From_Mass_constraint*ProbeMuon[i] + TagMuonMomentumVector[i] for i in range (0,3)]',
            'JPsi_energy = TagMuonEnergy + math.sqrt(Muon_M**2 + Probe_Momentum_From_Mass_constraint**2)',
            'BMassFromConstraint = math.sqrt( (JPsi_energy+TagKaonEnergy)**2 - (JPsi_momentum[0]+TagKaonMomentumVector[0])**2 -(JPsi_momentum[1]+TagKaonMomentumVector[1])**2 - (JPsi_momentum[2]+TagKaonMomentumVector[2])**2)',
            #
            "mass_constraint_b_momentum_vector = [ TagKaonMomentumVector[i] + JPsi_momentum[i] for i in range (0,3)]",
            "mass_constraint_b_momentum = math.sqrt(mass_constraint_b_momentum_vector[0]**2 + mass_constraint_b_momentum_vector[1]**2 + mass_constraint_b_momentum_vector[2]**2)",
            "normalised_mass_constraint_b_momentum_vector = [mass_constraint_b_momentum_vector[i]/mass_constraint_b_momentum for i in range (0,3)]",
            "B_ENDVERTEX_POSITION = [VFASPF(VX), VFASPF(VY), VFASPF(VZ)]",
            "B_PV_POSITION = [BPV(VX),BPV(VY),BPV(VZ)]",
            "LambdaFactor = - (inner([VFASPF(VX)-BPV(VX), VFASPF(VY)-BPV(VY), VFASPF(VZ)-BPV(VZ)], normalised_mass_constraint_b_momentum_vector))",
            "B_MASS_CONSTRAINT_IP_VECTOR = [(B_ENDVERTEX_POSITION[i] + LambdaFactor * normalised_mass_constraint_b_momentum_vector[i]) - B_PV_POSITION[i] for i in range (0,3)]",
            "B_MASS_CONSTRAINT_IP = math.sqrt(B_MASS_CONSTRAINT_IP_VECTOR[0]**2+B_MASS_CONSTRAINT_IP_VECTOR[1]**2+B_MASS_CONSTRAINT_IP_VECTOR[2]**2)"
            ]

        return CombineParticles(
            DecayDescriptors = ["B+ -> J/psi(1S) mu+",
                                "B- -> J/psi(1S) mu-"],
            DaughtersCuts = dc,
            CombinationCut = cc,
            MotherCut = mc,
            Preambulo = preambulo
            )
    
    def DetachedMuMuKPair(self,_name):
        combine_particles_mumuk = self.getCPForMuMuK("DetachedMuMuK");
        
        _DetatchedMuMuK_Sel = Selection("SelMuMuK_for_"+_name,
                                        Algorithm = combine_particles_mumuk,
                                        RequiredSelections = [self._DetachedMuKPairSel,
                                                              self.Hlt2ProbeMuons])
        
        PVMassFilteredSel = Selection("MassFilterSel"+_name,
                                      Algorithm = FilterDesktop(Code = "(in_range(%(POINTINGMASSMIN)s, POINTINGMASS ( LoKi.Child.Selector(0), LoKi.Child.Selector(1), LoKi.Child.Selector(2) ), %(POINTINGMASSMAX)s))" %self._config["DetachedMuMuK"]) ,
                                      RequiredSelections = [_DetatchedMuMuK_Sel]
                                      )
                                     
        line = StrippingLine(_name+"Line",
                             prescale = 1.0,
                             FILTER = self.GECs,
                             RequiredRawEvents = ["Velo","Muon"],
                             selection = PVMassFilteredSel)

        return line
    
    def DetachedMuMuKstarPair(self,_name):
        combine_particles_mumukstar = self.getCPForMuMuK("DetachedMuMuKstar");
        
        _DetatchedMuMuKstar_Sel = Selection("SelMuMuKstar_for_"+_name,
                                        Algorithm = combine_particles_mumukstar,
                                        RequiredSelections = [self._DetachedMuKstarPairSel,
                                                              self.Hlt2ProbeMuons])
        
        PVMassFilteredSel = Selection("MassFilterSel"+_name,
                                      Algorithm = FilterDesktop(Code = "(in_range(%(POINTINGMASSMIN)s, POINTINGMASS ( LoKi.Child.Selector(0), LoKi.Child.Selector(1), LoKi.Child.Selector(2) ), %(POINTINGMASSMAX)s))" %self._config["DetachedMuMuKstar"]) ,
                                      RequiredSelections = [_DetatchedMuMuKstar_Sel]
                                      )
         
        line = StrippingLine(_name+"Line",
                             prescale = 1.0,
                             FILTER = self.GECs,
                             RequiredRawEvents = ["Velo","Muon"],
                             selection = PVMassFilteredSel)


        return line
    
    def DetachedMuMuPhiPair(self,_name):
        combine_particles_mumuphi = self.getCPForMuMuK("DetachedMuMuPhi");
        
        _DetatchedMuMuPhi_Sel = Selection("SelMuMuPhi_for_"+_name,
                                        Algorithm = combine_particles_mumuphi,
                                        RequiredSelections = [self._DetachedMuPhiPairSel,
                                                              self.Hlt2ProbeMuons])
        
        PVMassFilteredSel = Selection("MassFilterSel"+_name,
                                      Algorithm = FilterDesktop(Code = "(in_range(%(POINTINGMASSMIN)s, POINTINGMASS ( LoKi.Child.Selector(0), LoKi.Child.Selector(1), LoKi.Child.Selector(2) ), %(POINTINGMASSMAX)s))" %self._config["DetachedMuMuPhi"]) ,
                                      RequiredSelections = [_DetatchedMuMuPhi_Sel]
                                      )
         
        line = StrippingLine(_name+"Line",
                             prescale = 1.0,
                             FILTER = self.GECs,
                             RequiredRawEvents = ["Velo","Muon"],
                             selection = PVMassFilteredSel)


        return line

    ##### velo tracking ####
    def MakeVeloTracks(self,prefilter):        
        if self._config["DoVeloDecoding"]:
            from DAQSys.Decoders import DecoderDB
            from DAQSys.DecoderClass import decodersForBank
            decs=[]
            vdec=DecoderDB["DecodeVeloRawBuffer/createBothVeloClusters"]
            vdec.Active=True
            DecoderDB["DecodeVeloRawBuffer/createVeloClusters"].Active=False
            DecoderDB["DecodeVeloRawBuffer/createVeloLiteClusters"].Active=False
            decs=decs+[vdec]
            VeloDecoding = GaudiSequencer("RecoDecodingSeq")
            VeloDecoding.Members += [d.setup() for d in decs ]
        
        MyFastVeloTracking = FastVeloTracking("For%sFastVelo"%self.name,
                                              OutputTracksName=self.VeloTrackOutputLocation)
        MyFastVeloTracking.OnlyForward = True
        MyFastVeloTracking.ResetUsedFlags = True
        ### prepare for fitting
        preve = TrackStateInitAlg("For%sInitSeedFit"%self.name,
                                  TrackLocation = self.VeloTrackOutputLocation)
        preve.StateInitTool.VeloFitterName = "FastVeloFitLHCbIDs"
        copyVelo = TrackContainerCopy( "For%sCopyVelo"%self.name )
        copyVelo.inputLocations = [self.VeloTrackOutputLocation]
        copyVelo.outputLocation = self.FittedVeloTrackOutputLocation
        
        ### fitting
        if self._config["VeloFitter"] == "SimplifiedGeometry":
            MyVeloFit = ConfiguredEventFitter(Name="For%sVeloRefitterAlg"%self.name,
                                              TracksInContainer=self.FittedVeloTrackOutputLocation,
                                              SimplifiedGeometry = True)
        else:
            MyVeloFit = ConfiguredEventFitter(Name="For%sVeloRefitterAlg"%self.name,
                                              TracksInContainer=self.FittedVeloTrackOutputLocation,
                                              SimplifiedGeometry=False)
            
        #### making the proto particles
        MakeVeloProtos = ChargedProtoParticleMaker('For%sVeloProtoMaker'%self.name)
        MakeVeloProtos.Inputs=[self.FittedVeloTrackOutputLocation]
        MakeVeloProtos.Output = self.VeloProtoOutputLocation
        MakeVeloProtos.addTool( DelegatingTrackSelector, name="TrackSelector" )
        MakeVeloProtos.TrackSelector.TrackTypes = [ "Velo" ]
    
        #### the full sequence
        makeparts = GaudiSequencer('For%sMakeVeloTracksGS'%self.name)
        
        if self._config["DoVeloDecoding"]:
            makeparts.Members += [ VeloDecoding ] 
        
        makeparts.Members += [ MyFastVeloTracking ] 
        makeparts.Members += [ preve ] 
        makeparts.Members += [ copyVelo ] 
        makeparts.Members += [ MyVeloFit ] 
        makeparts.Members += [ MakeVeloProtos ] 
    
        #### some python magic to make this appear like a "Selection"
        return GSWrapper(name="For%sWrappedVeloTrackingFor"%self.name,
                         sequencer=makeparts,
                         output=self.VeloProtoOutputLocation,
                         requiredSelections =  prefilter)

    def MakeVeloParticles(self,name,
                          particle, 
                          protoParticlesMaker):        
        particleMaker =  NoPIDsParticleMaker("For%sParticleMaker%s"%(self.name,name) , Particle = particle, AddBremPhotonTo = [])
        particleMaker.Input = self.VeloProtoOutputLocation

        DataOnDemandSvc().AlgMap.update( {
                "/Event/Phys/" + particleMaker.name() + '/Particles' : particleMaker.getFullName(),
                "/Event/Phys/" + particleMaker.name() + '/Vertices'  : particleMaker.getFullName()
                } )

        AllVeloParticles = Selection("For%sSelAllVeloParts%s"%(self.name,name), 
                                     Algorithm = particleMaker, 
                                     RequiredSelections = [protoParticlesMaker], InputDataSetter=None)
        
        ### filter on the IP of the velo tracks
        return Selection("For%sSelVeloParts%s"%(self.name,name), 
                     Algorithm = FilterDesktop(Code="(MIPDV(PRIMARY) > %(VeloMINIP)s) & " \
                                                    "(TRCHI2DOF<%(VeloTrackChi2)s) & " \
                                                    "in_range(%(EtaMinVelo)s, ETA, %(EtaMaxVelo)s)" %self._config),
                     RequiredSelections = [AllVeloParticles])
                             
#####Tos filter from B2DMuNuXUtils
""" Is this still used?? """
def TOSFilter( name = None, sel = None, trigger = None ):
    if len(trigger) == 0:
        return sel
    from Configurables import TisTosParticleTagger
    _filter = TisTosParticleTagger(name+"_TriggerTos")
    _filter.TisTosSpecs = trigger
    _sel = Selection("Sel" + name + "_TriggerTos", RequiredSelections = [ sel ], Algorithm = _filter )
    return _sel
###### OTHER FUNCTIONS ###############
class GSWrapper(UniquelyNamedObject,
                ClonableObject,
                SelectionBase) :
    
    def __init__(self, name, sequencer, output, requiredSelections) :
        UniquelyNamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())
        SelectionBase.__init__(self,
                               algorithm = sequencer,
                               outputLocation = output,
                               requiredSelections = requiredSelections )        
            


###the end


#StrippingElectronRecoEffLines("test", default_config);
