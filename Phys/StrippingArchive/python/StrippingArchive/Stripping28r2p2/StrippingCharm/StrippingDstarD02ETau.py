###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
__author__ = 'Michele Atzeni, Eluned Anne Smith'
__date__ = '2023/06/30'
__version__ = '$Revision: 1.0 $'
__all__ = ('StrippingDstarD02ETauConf',
           'default_config')
#from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
'''
  Stripping for:
  Opposite Sign of the leptons (OS)
  - D*(2010)+ -> pi+ (D0-> tau+ -> (pi+ pi- pi+ anti-nu_tau) e-)
  - D*(2010)+ -> pi+ (D0-> tau- -> (pi+ pi- pi+ nu_tau) e+)
  Same Sign of the leptons (SS)
  - D*(2010)+ -> pi+ (D0-> tau+ -> (pi+ pi- pi+ anti-nu_tau) e+)
  - D*(2010)+ -> pi+ (D0-> tau- -> (pi- pi+ pi- nu_tau) e-)
  Control mode OS:
  - D*(2010)+ -> pi+ (D0-> pi+ pi- pi+ K-)
  Control mode SS:
  - D*(2010)+ -> pi+ (D0-> pi+ pi- pi+ K+)
'''

default_name = "DstarD02ETau"

default_config = {'NAME': 'DstarD02ETau',
                  'WGs': ['Charm'],
                  'BUILDERTYPE': 'StrippingDstarD02ETauConf',
                  'STREAMS': ['CharmCompleteEvent'],
                  'CONFIG': {'PrescaleETauBox': 1.,
                             'PrescaleETauSSBox': 1.,
                             'PrescaleK3piBox': 0.03, #1., #totest locally 0.006,  # needed to keep the rates compatible with DST
                             'PrescaleK3piSSBox': 0.03, #1., #totest locally0.006,  # needed to keep the rates compatible with DST
                             'prefix': '',
                             #
                            #                                                                      
                             'P_THREE_PI': "1000",  # MeV                                          
                             'PT_THREE_PI': "200",  # MeV                                          
                             'TRACKCHI2_THREE_PI': "2",  # dimensionless                           
                             'TRGHOPROB_THREE_PI': "0.1",  # dimensionless                         
                             'MINIPCHI2BPV_THREE_PI': "0.45",  # dimensionless                      
                             'CUTBASED_PROBNNPI_THREE_PI': '0.8', #to reduce rate                  
                             'PROBNNGHOST_THREE_PI': '0.05',                                        
                             # tau                                                                 
                             'TAU_M_LOW_COMB': "200",  # MeV                                       
                             'TAU_M_HIGH_COMB': "2300",  # MeV                                     
                             'TAU_AMAXDOCA_PIS': "0.15",  # mm                                      
                             'TAU_APT_COMB': "500.",  # MeV                                        
                             'TAU_PT': "1500.",  # MeV                                              
                             'TAU_P': "15000.",  # MeV                                             
                             'TAU_M_LOW': "300",  # MeV                                            
                             'TAU_M_HIGH': "2200",  # MeV                                          
                             'TAU_BPVDIRA': "0.9995", #dimensionless                                
                             'CUTBASED_TAU_BPVVDRHO_LOW': "0.1",  # mm                            
                             'CUTBASED_TAU_BPVVDRHO_HIGH': "7",  # mm                              
                             'CUTBASED_TAU_BPVVDZ': "0.0015",  # mm                                
                             'CUTBASED_TAU_VCHI2': "10", #dimensionless                            
                             'CUTBASED_TAU_BPVVDCHI2': "19",  # dimensionless                     
                             # Electron                                                            
                             'P_E': "1000",  # MeV                                                 
                             'PT_E': "200",  # MeV                                                 
                             'TRACKCHI2_E': "2",  # dimensionless                                  
                             'TRGHOPROB_E': "0.1",  # dimensionless                                
                             'MINIPCHI2BPV_E': "0.2",  # dimensionless                            
                             'CUTBASED_PROBNNE_E': '0.8', #dimensionless                           
                             'PROBNNGHOST_E': '0.3', #dimensionless                                
                             #Kaon for CM                                                          
                             'CUTBASED_PROBNNK_K': '0.5', #dimensionless                           
                             # D0                                                                  
                             'P_D0': "20000",  # MeV                                               
                             'PT_D0': "2000",  # MeV                                               
                             'CUTBASED_M_D0_LOW': '300',  # MeV                                    
                             'CUTBASED_M_D0_HIGH': '2200',  # MeV                                  
                             'CUTBASED_AMAXDOCA_D0_COMB': "0.35",  # mm                             
                             'CUTBASED_MCORR_D0_LOW': '1000',  # MeV                               
                             'CUTBASED_MCORR_D0_HIGH': '8000',  # MeV                              
                             'CUTBASED_MISS_MASS_LOW': '0',  # MeV                                 
                             'CUTBASED_MISS_MASS_HIGH': '250',  # MeV                              
                             'CUTBASED_MISS_MASS_HIGH_KTHREEPI': '1500',  # MeV                    
                             'CUTBASED_TAU_TAU_LOW': '-2.5',  # ps                                 
                             'CUTBASED_TAU_TAU_HIGH': '3',  # ps                                   
                             'CUTBASED_BPVVDRHO_D0_LOW': '0.1',  # mm                             
                             'CUTBASED_BPVVDRHO_D0_HIGH': '7',  # mm                               
                             'CUTBASED_VCHI2_D0':   '5',  # dimensionless      
                             'CUTBASED_VCHI2_D0_KTHREEPI':   '25',  # dimensionless                                                                         
                             'CUTBASED_D0_ABS_BPVDIRA': '0.999',  # dimensionless                   
                             'CUTBASED_D0_BPVVDCHI2': "2",  # dimensionless                        
                                                                                                   
                             # pi from Dst                                                         
                             'PT_PI_FROM_DTSM': "200",  # MeV                                      
                             'P_PI_FROM_DTSM': "1000",  # MeV                                      
                             'TRACKCHI2_PI_FROM_DTSM': "2",  # dimensionless                       
                             'TRGHOPROB_PI_FROM_DTSM': "0.1",  # dimensionless                     
                             'PROBNNGHOST_PI_FROM_DTSM': '0.05', #dimensionless                     
                             'CUTBASED_PROBNNPI_PI_FROM_DTSM': '0.8', #dimensionless               
                             # Dstm                                                                
                             'Dst_AMAXDOCA_COMB': "0.2",  # mm                                     
                             'DstMassWin_LOW': "-1450.",  # MeV                                    
                             'DstMassWin_HIGH': "300.",   # MeV                                    
                             'DstD0PIDMWin_LOW': "-120.",    # MeV                                  
                             'DstD0PIDMWin_HIGH': "120.",    # MeV                                 
                             'DstmVtxChi2': "8.",  # dimensionless                                

                             # Isolations
                             'ConeAngles': {"08": 0.8, "10": 1.0, "12": 1.2, "14": 1.4},
                             'ConeVariables': ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                             'RelInfoTools_D02ETau': [
                                 {"Type": "RelInfoVertexIsolation",
                                  "Location": "DstVars_VertexIsoInfo",
                                  "DaughterLocations": {"[D*(2010)+ -> ^(Charm -> l  l) pi+]CC": "D0_VertexIsoInfo",
                                                        "[D*(2010)+ ->  (Charm -> ^l l) pi+]CC": "Tau_VertexIsoInfo",
                                                        "[D*(2010)+ ->  (Charm -> l ^l) pi+]CC": "E_VertexIsoInfo",
                                                        "[D*(2010)+ -> ^(Charm -> l  l) ^pi+]CC": "pi_VertexIsoInfo",
                                                        }},
                                 {"Type": "RelInfoConeIsolation",
                                  "ConeSize": 0.5,
                                  "Variables": [],
                                  "Location": "DstVars_ConeIsoInfo",
                                  "DaughterLocations": {"[D*(2010)+ -> ^(Charm -> l  l) pi+]CC": "D0_ConeIsoInfo",
                                                        "[D*(2010)+ ->  (Charm -> ^l l) pi+]CC": "Tau_ConeIsoInfo",
                                                        "[D*(2010)+ ->  (Charm -> l ^l) pi+]CC": "E_ConeIsoInfo",
                                                        "[D*(2010)+ -> ^(Charm -> l  l) ^pi+]CC": "pi_ConeIsoInfo",
                                                        }},
                             ],
                             'RelInfoTools_D02K3pi': [
                                 {"Type": "RelInfoVertexIsolation",
                                  "Location": "DstVars_VertexIsoInfo",
                                  "DaughterLocations": {"[D*(2010)+ -> ^(Charm -> X X X X) pi+]CC": "D0_VertexIsoInfo",
                                                        "[D*(2010)+ -> ^(Charm -> X X X X) ^pi+]CC": "pi_VertexIsoInfo",
                                                        }},
                                 {"Type": "RelInfoConeIsolation",
                                  "ConeSize": 0.5,
                                  "Variables": [],
                                  "Location": "DstVars_ConeIsoInfo",
                                  "DaughterLocations": {"[D*(2010)+ -> ^(Charm -> X X X X) pi+]CC": "D0_ConeIsoInfo",
                                                        "[D*(2010)+ -> ^(Charm -> X X X X) ^pi+]CC": "pi_ConeIsoInfo",
                                                        }},
                             ]
                             }
                  }


class StrippingDstarD02ETauConf(LineBuilder):
    """
    Builder for  D*(2010)+ -> pi+ (D0-> tau+ (-> pi+ pi- pi+ anti_nutau) e-) stripping lines
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        # Declare line
        line_taue_OS_box = self.baseLineETau(name, config, same_sign=False)
        line_taue_SS_box = self.baseLineETau(name, config, same_sign=True)
        line_K3pi_OS_box = self.baseLineK3pi(name, config, same_sign=False)
        line_K3pi_SS_box = self.baseLineK3pi(name, config, same_sign=True)
        lines_to_be = [line_taue_OS_box, line_taue_SS_box,
                       line_K3pi_OS_box, line_K3pi_SS_box,
                       ]
        # register line
        for i in lines_to_be:
            self.registerLine(i)

    def baseLineETau(self, name, config,  same_sign=False):
        """
        Returns the stripping line for the decay
        D*(2010)+ -> pi+ (D0-> tau+ (-> pi+ pi- pi+ anti_nutau) e-)
        and the decay with tau and electron with the same charge
        """
        # Define the taus
        name += "_SS" if same_sign else ""
        combname = "ETauSS" if same_sign else "ETau"
        selTau = self._makeTau_3pi(name="Tau_3piFor"+name,
                                   pionSel=self._makePions4Tau23pi(
                                       "3PionSelFor"+name, config),
                                   config=config)
        # Define the electron
        # TODO: Should we also include Upstream electrons, as it is done for the pions?
        selElectrons = self._makeElectrons(name="ElectronsFor"+name,
                                           config=config)
        # Combine Tau and Electron to make the D0
        if same_sign:
            selD02ETau_3pi = self._makeD02ETau_3pi(name="D0SSFor"+name,
                                                   tauSel=selTau,
                                                   eleSel=selElectrons,
                                                   config=config,
                                                   DecayDescriptors=["[D0 -> tau+ e+]cc", "[D0 -> tau- e-]cc"])
        else:
            selD02ETau_3pi = self._makeD02ETau_3pi(name="D0For"+name,
                                                   tauSel=selTau,
                                                   eleSel=selElectrons,
                                                   config=config,
                                                   DecayDescriptors=["[D0 -> tau+ e-]cc", "[D0 -> tau- e+]cc"])
        xxCombSel = selD02ETau_3pi

        # Combine D0 and pion to make a Dstm
        dstar_box = self._makeDstar(config)
        dst_req_sel = [DataOnDemand("Phys/StdAllNoPIDsPions/Particles"),
                       DataOnDemand("Phys/StdNoPIDsUpPions/Particles"),
                       xxCombSel]

        pres = "Prescale"+combname+"Box"
        _tag_sel = Selection(name+"_seq_"+combname+"_box",
                             Algorithm=dstar_box,
                             RequiredSelections=dst_req_sel)
        # Add isolation variables
        for conekey, coneitem in (config['ConeAngles']).iteritems():
            config['RelInfoTools_D02ETau'].append({
                'Type': 'RelInfoConeVariables', 'ConeAngle': coneitem, 'Variables': config['ConeVariables'],
                'Location': 'P2CVDst'+conekey,
                            'DaughterLocations': {
                                '[D*(2010)+ -> ^(Charm -> l l ) pi+]CC': 'P2CVD0'+conekey,
                                '[D*(2010)+ -> (Charm -> l l ) ^pi+]CC': 'P2CVpis'+conekey,
                    '[D*(2010)+ -> (Charm -> ^l l ) pi+]CC': 'P2CL1'+conekey,
                    '[D*(2010)+ -> (Charm -> l ^l ) pi+]CC': 'P2CL2'+conekey,
                }
            })
        # Declare line
        line_box = StrippingLine(name+config['prefix']+"Dst2PiD02"+combname+"Box",
                                 prescale=config[pres],
                                 # postscale
                                 MDSTFlag=False,
                                 RelatedInfoTools=config['RelInfoTools_D02ETau'],
                                 algos=[_tag_sel],
                                 MaxCandidates=50
                                 )
        return line_box

    def baseLineK3pi(self, name, config,  same_sign=False):
        """
        Returns the stripping line for the decay
        D*(2010)+ -> pi+ (D0-> K- pi+ pi- pi+ ) and the decay with the K with
        same charge w.r.t. the combination of the three pions
        """
        # Define the taus
        name += "_CM_SS" if same_sign else "_CM"
        combname = "K3piSS" if same_sign else "K3pi"
        selPions = self._makePions4Tau23pi("3PionSelFor"+name, config)
        # TODO: align with electron in case we include Upstream
        selKaon = self._makeKaons("KaonSelFor"+name, config)

        # Combine Tau and Electron to make the D0
        if same_sign:
            selD02ETau_3pi = self._makeD02K3pi(name="D0SSFor"+name,
                                               piSel=selPions,
                                               KSel=selKaon,
                                               config=config,
                                               DecayDescriptors=["[D0 -> K+ pi+ pi+ pi-]cc"])
        else:
            selD02ETau_3pi = self._makeD02K3pi(name="D0For"+name,
                                               piSel=selPions,
                                               KSel=selKaon,
                                               config=config,
                                               DecayDescriptors=["[D0 -> K- pi+ pi+ pi-]cc"])
        xxCombSel =  selD02ETau_3pi

        # Combine D0 and pion to make a Dstm
        dstar_box = self._makeDstar(config)
        dst_req_sel = [DataOnDemand("Phys/StdAllNoPIDsPions/Particles"),
                       DataOnDemand("Phys/StdNoPIDsUpPions/Particles"),
                       xxCombSel]

        pres = "Prescale"+combname+"Box"
        _tag_sel = Selection(name+"_seq_"+combname+"_box",
                             Algorithm=dstar_box,
                             RequiredSelections=dst_req_sel)

        for conekey, coneitem in (config['ConeAngles']).iteritems():
            config['RelInfoTools_D02K3pi'].append({
                'Type': 'RelInfoConeVariables',
                'ConeAngle': coneitem,
                'Variables': config['ConeVariables'],
                'Location': 'P2CVDst'+conekey,
                            'DaughterLocations': {
                                '[D*(2010)+ -> ^(Charm -> X X X X ) pi+]CC': 'P2CVD0'+conekey,
                                '[D*(2010)+ -> (Charm -> X X X X) ^pi+]CC': 'P2CVpis'+conekey, }
            })

        # Declare line
        line_box = StrippingLine(name+config['prefix']+"Dst2PiD02"+combname+"Box",
                                 prescale=config[pres],
                                 # postscale
                                 MDSTFlag=False,
                                 RelatedInfoTools=config['RelInfoTools_D02K3pi'],
                                 algos=[_tag_sel],
                                 MaxCandidates=50
                                 )
        return line_box

    ####################################################
    def _makeElectrons(self, name, config):
        """
        Electron selection
        """
        _code = self._eFinalStateKinematicCuts(config)
        #to reduce rate
        _code += "  & (PROBNNghost < "+config['PROBNNGHOST_E'] + \
            ") & (PROBNNe > " + config['CUTBASED_PROBNNE_E'] + ")"
        _Filter = FilterDesktop(Code=_code)

        return Selection(name,
                         Algorithm=_Filter,
                         RequiredSelections=[DataOnDemand("Phys/StdAllNoPIDsElectrons/Particles"),
                                             DataOnDemand("Phys/StdNoPIDsUpElectrons/Particles")])
    ####################################################

    def _makeKaons(self, name, config):
        """
        Kaons selection
        """
        _code = self._kaonsFinalStateKinematicCuts(config)
        _code += "  & (PROBNNghost < "+config['PROBNNGHOST_E'] + \
            ") & (PROBNNk > " + config['CUTBASED_PROBNNK_K'] + ")"
        _Filter = FilterDesktop(Code=_code)

        return Selection(name,
                         Algorithm=_Filter,
                         RequiredSelections=[DataOnDemand("Phys/StdAllNoPIDsKaons/Particles")])

    #####################################################
    def _eFinalStateKinematicCuts(self, config):
        _code = "(P  > " + config['P_E'] + "*MeV) & "\
                "(PT > " + config['PT_E'] + "*MeV) & "\
                "(MIPCHI2DV(PRIMARY) > " + config['MINIPCHI2BPV_E'] + ")     & "\
                "(TRCHI2DOF < " + config['TRACKCHI2_E'] + ")     & "\
                "(TRGHOSTPROB < " + config['TRGHOPROB_E'] + ")      "
        return _code
    ###########################################################

    def _kaonsFinalStateKinematicCuts(self, config):
        # keep aligned to electron
        _code = "(P  > " + config['P_E'] + "*MeV) & "\
                "(PT > " + config['PT_E'] + "*MeV) & "\
                "(MIPCHI2DV(PRIMARY) > " + config['MINIPCHI2BPV_E'] + ")     & "\
                "(TRCHI2DOF < " + config['TRACKCHI2_E'] + ")     & "\
                "(TRGHOSTPROB < " + config['TRGHOPROB_E'] + ") "
        # _code = "(P  > 0 )"
        return _code
    #############################################################

    def _makeTau_3pi(self, name, pionSel, config):

        # Minimum mass = 3 pions (140 MeV) = 420 MeV
        # This is a variation of the StdTightDetachedTau3pi in https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/CommonParticlesArchive/python/CommonParticlesArchive/Stripping28r2p1/CommonParticles/StdTightDetachedTau.py
        _preambulo = [""]
        _combcut = "(APT>"+config['TAU_APT_COMB'] + "*MeV) & ((AM>"+config['TAU_M_LOW_COMB'] + "*MeV)"\
            " & (AM<"+config['TAU_M_HIGH_COMB'] + "*MeV)) "\
            " & (AMAXDOCA('')<"+config['TAU_AMAXDOCA_PIS'] + "*mm)"\
            " & (ANUM(PT > 500*MeV) >= 1)"
        _mcut = "(PT>"+config['TAU_PT'] + "*MeV) & (P>"+config['TAU_P'] + "*MeV)"\
            "& (M>"+config['TAU_M_LOW'] + "*MeV)"\
            "& (M<"+config['TAU_M_HIGH'] + "*MeV) & (BPVDIRA>"+config['TAU_BPVDIRA'] + ")"\
            "& (VFASPF(VCHI2) < " + config['CUTBASED_TAU_VCHI2'] + ")"\
            "& (BPVVDCHI2>" + config['CUTBASED_TAU_BPVVDCHI2'] + ") "\
            "& (BPVVDRHO>" + config['CUTBASED_TAU_BPVVDRHO_LOW'] + "*mm)"\
            "& (BPVVDRHO<" + config['CUTBASED_TAU_BPVVDRHO_HIGH'] + "*mm)"\
            "& (BPVVDZ>" + config['CUTBASED_TAU_BPVVDZ'] + "*mm)"

        
        _CombineTau = CombineParticles(DecayDescriptors=["[tau+ -> pi+ pi+ pi-]cc"],
                                       CombinationCut=_combcut,
                                       MotherCut=_mcut,
                                       Preambulo=_preambulo)
        return Selection(name,
                         Algorithm=_CombineTau,
                         RequiredSelections=[pionSel]
                         )

    def _makePions4Tau23pi(self, name, config):
        _code = self._hadFinalStateKinematicCuts_3pi(config)
        _code += "  & (PROBNNghost < "+config['PROBNNGHOST_THREE_PI'] + \
            ") & (PROBNNpi > " + config['CUTBASED_PROBNNPI_THREE_PI'] + ")"
        _code += "  & (MIPCHI2DV(PRIMARY) > " + \
            config['MINIPCHI2BPV_THREE_PI'] + ")"
        _Filter = FilterDesktop(Code=_code)

        return Selection(name,
                         Algorithm=_Filter,
                         RequiredSelections=[DataOnDemand("Phys/StdAllNoPIDsPions/Particles")]
                         )

    # Template for combine particles for D* -> D0 pi
    def _makeDstar(self, config):
        # loosks for all D0s in the decay, returns the largest mass (mass of the D0 since there is only one) and computes the mass difference with the mother
        dstcomb_dstcut = "(((M-MAXTREE('D0'==ABSID,M))-145.42) < %(DstD0PIDMWin_HIGH)s *MeV) & (((M-MAXTREE('D0'==ABSID,M))-145.42) > %(DstD0PIDMWin_LOW)s *MeV) & (VFASPF(VCHI2/VDOF)< %(DstmVtxChi2)s)"
        dstcomb_combcut = "( (AM - 2010.26) <  %(DstMassWin_HIGH)s * MeV) & ( (AM - 2010.26) >  %(DstMassWin_LOW)s * MeV)"\
                           " & (AMAXDOCA('')< %(Dst_AMAXDOCA_COMB)s *mm)" 

        dstcomb_picut = "(PT> %(PT_PI_FROM_DTSM)s * MeV) & (TRCHI2DOF<%(TRACKCHI2_PI_FROM_DTSM)s) "
        dstcomb_picut += "& (TRGHOSTPROB< %(TRGHOPROB_PI_FROM_DTSM)s ) & (PROBNNpi> %(CUTBASED_PROBNNPI_PI_FROM_DTSM)s) & (PROBNNghost< %(PROBNNGHOST_PI_FROM_DTSM)s) " #try to reduce the rate
        dstcomb_picut += "& (P> %(P_PI_FROM_DTSM)s )" #try to reduce the rate
        dstcomb_d0cut = "PT>0"
        dstar = CombineParticles(DecayDescriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D~0 pi-'],
                                 DaughtersCuts={"pi+": dstcomb_picut % config,
                                                "D0": dstcomb_d0cut % config
                                                },
                                 CombinationCut=dstcomb_combcut % config,
                                 MotherCut=dstcomb_dstcut % config
                                 )

        return dstar

    ##################################################

    def _hadFinalStateKinematicCuts_3pi(self, config):
        _code = "(P  > " + config['P_THREE_PI'] + "*MeV) & "\
                "(PT > " + config['PT_THREE_PI'] + "*MeV) & "\
                "(TRCHI2DOF < " + config['TRACKCHI2_THREE_PI'] + ")     & "\
                "(TRGHOSTPROB < " + config['TRGHOPROB_THREE_PI'] + ")     "

        return _code
    ##################################################

    def _makeD02ETau_3pi(self, name, tauSel, eleSel, config, DecayDescriptors):

        preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                     "MCOR = sqrt(M**2 + PTRANS**2) + PTRANS",
                     "allpi = ((('pi+') == ABSID) | (('K+') == ABSID)) ",
                     "sumpt    = SUMTREE( allpi , PT )"]

        _combcut = "(AM  > 400 *MeV) & (AM  < 2200 *MeV)"\
                   " & (AMAXDOCA('')<"+config['CUTBASED_AMAXDOCA_D0_COMB'] + "*mm)" 
         #"((CHILD(MIPCHI2DV(PRIMARY),1)) > " + config['MINIPCHI2PV_D0']+")  &  "\
        _mcut = "(P > "+config['P_D0']+"*MeV) & (PT > "+config['PT_D0']+"*MeV) & " \
            "(M > " + config['CUTBASED_M_D0_LOW'] + "*MeV) & "\
            "(M < " + config['CUTBASED_M_D0_HIGH'] + "*MeV) & "\
            "(MCOR < " + config['CUTBASED_MCORR_D0_HIGH'] + "*MeV) & "\
            "(MCOR > " + config['CUTBASED_MCORR_D0_LOW'] + "*MeV) & "\
            "((BPVDIRA > " + config['CUTBASED_D0_ABS_BPVDIRA'] + ") | "\
            "(BPVDIRA < -" + config['CUTBASED_D0_ABS_BPVDIRA'] + ")) & "\
            "(M - CHILD(M,1) - CHILD(M,2) > " + config['CUTBASED_MISS_MASS_LOW'] + "*MeV) & "\
            "(M - CHILD(M,1) - CHILD(M,2) < " + config['CUTBASED_MISS_MASS_HIGH'] + "*MeV) & "\
            "(VFASPF(VCHI2) < " + config['CUTBASED_VCHI2_D0'] + ") &"\
            "(BPVVDCHI2>" + config['CUTBASED_D0_BPVVDCHI2'] + ") &"\
            "(BPVVDRHO>" + config['CUTBASED_BPVVDRHO_D0_LOW'] + "*mm) &"\
            "(BPVVDRHO<" + config['CUTBASED_BPVVDRHO_D0_HIGH'] + "*mm)"\
            #"(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) < " + config['CUTBASED_TAU_TAU_HIGH'] + " * ps) & "\
            #"(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) > " + config['CUTBASED_TAU_TAU_LOW'] + " * ps) "

        _CombineTau = CombineParticles(DecayDescriptors=DecayDescriptors,
                                       CombinationCut=_combcut,
                                       MotherCut=_mcut,
                                       Preambulo=preambulo)

        return Selection(name+"_TauE_3pi",
                         Algorithm=_CombineTau,
                         RequiredSelections=[tauSel, eleSel])
    def _makeD02K3pi(self, name, piSel, KSel, config, DecayDescriptors):

        preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                     "MCOR = sqrt(M**2 + PTRANS**2) + PTRANS",
                     "allpi = ((('pi+') == ABSID) | (('K+') == ABSID)) ",
                     "sumpt    = SUMTREE( allpi , PT )",
                     "THREEPI_PX = (CHILD(PX,2) + CHILD(PX,3) + CHILD(PX,4))",
                     "THREEPI_PY = (CHILD(PY,2) + CHILD(PY,3) + CHILD(PY,4))",
                     "THREEPI_PZ = (CHILD(PZ,2) + CHILD(PZ,3) + CHILD(PZ,4))",
                     "THREEPI_E2 = (CHILD(E,2) + CHILD(E,3) + CHILD(E,4))**2",
                     "THREEPI_P2 = (THREEPI_PX**2 + THREEPI_PY**2 + THREEPI_PZ**2)",
                     "THREEPI_M = sqrt(THREEPI_E2 - THREEPI_P2 )"]

        _combcut = "(AM  > 400 *MeV) & (AM  < 2200 *MeV)"\
                   " & (AMAXDOCA('')<"+config['TAU_AMAXDOCA_PIS'] + "*mm)"  #align cut with tau
         #"((CHILD(MIPCHI2DV(PRIMARY),1)) > " + config['MINIPCHI2PV_D0']+")  &  "\
        _mcut = "(P > "+config['P_D0']+"*MeV) & (PT > "+config['PT_D0']+"*MeV) & " \
            "(M > " + config['CUTBASED_M_D0_LOW'] + "*MeV) & "\
            "(M < " + config['CUTBASED_M_D0_HIGH'] + "*MeV) & "\
            "(MCOR < " + config['CUTBASED_MCORR_D0_HIGH'] + "*MeV) & "\
            "(MCOR > " + config['CUTBASED_MCORR_D0_LOW'] + "*MeV) & "\
            "((BPVDIRA > " + config['CUTBASED_D0_ABS_BPVDIRA'] + ") | "\
            "(BPVDIRA < -" + config['CUTBASED_D0_ABS_BPVDIRA'] + ")) & "\
            "(M - CHILD(M,1) - THREEPI_M > " + config['CUTBASED_MISS_MASS_LOW'] + "*MeV) & "\
            "(M - CHILD(M,1) - THREEPI_M < " + config['CUTBASED_MISS_MASS_HIGH_KTHREEPI'] + "*MeV) & "\
            "(VFASPF(VCHI2) < " + config['CUTBASED_VCHI2_D0_KTHREEPI'] + ") &"\
            "(BPVVDCHI2>" + config['CUTBASED_TAU_BPVVDCHI2'] + ") &"\
            "(BPVVDRHO>" + config['CUTBASED_BPVVDRHO_D0_LOW'] + "*mm) &"\
            "(BPVVDRHO<" + config['CUTBASED_BPVVDRHO_D0_HIGH'] + "*mm)"\
            #"(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) < " + config['CUTBASED_TAU_TAU_HIGH'] + " * ps) & "\
            #"(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) > " + config['CUTBASED_TAU_TAU_LOW'] + " * ps) "

        _CombineTau = CombineParticles(DecayDescriptors=DecayDescriptors,  # TOCHECK
                                       CombinationCut=_combcut,
                                       MotherCut=_mcut,
                                       Preambulo=preambulo)

        return Selection(name+"_K3pi",
                         Algorithm=_CombineTau,
                         RequiredSelections=[piSel, KSel])
