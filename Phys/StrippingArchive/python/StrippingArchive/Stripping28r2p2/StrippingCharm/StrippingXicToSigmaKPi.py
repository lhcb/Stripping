###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Exclusive lines for Xic+ -> Sigma0 KS0 pi+, Xic0 -> Sigma0 K- pi+ and Xic+ -> Sigma+ K- pi+ with Sigma+ from long proton tracks.
(The Sigma+ lifetime is about as long as that of the KS, which has a large portion of long tracks)
"""
__author__ = ["Marian Stahl"]

__all__ = ("XicToSigmaKPiConf", "default_config")

moduleName = "XicToSigmaKPi"

# Import Packages
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from PhysSelPython.Wrappers import AutomaticData
from PhysConf.Selections import (FilterSelection, Combine3BodySelection,
                                 MergedSelection, SimpleSelection)
from Configurables import (FilterDesktop, ResolvedPi0Maker, PhotonMaker)
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from GaudiKernel.SystemOfUnits import MeV
from CommonParticles.Utils import updateDoD

# Default configuration dictionary
default_config = {
    "NAME": "XicToSigmaKPi",
    "BUILDERTYPE": "XicToSigmaKPiConf",
    "CONFIG": {
        "pi": {
            "TES":
            "Phys/StdAllNoPIDsPions/Particles",
            "Filter":
            "(P>2000*MeV) & (PT>150*MeV) & (MIPCHI2DV(PRIMARY)>0.2) & (PROBNNpi>0.1)"
        },
        "K": {
            "TES":
            "Phys/StdAllNoPIDsKaons/Particles",
            "Filter":
            "(P>2000*MeV) & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>0.2) & (PROBNNk>0.1)"
        },
        "p": {
            "TES":
            "Phys/StdAllNoPIDsProtons/Particles",
            "Filter":
            "(P>4000*MeV) & (PT>250*MeV) & (PROBNNp>0.1) & (MIPCHI2DV(PRIMARY)>12)"
        },
        "gamma": {
            "TES": "Phys/StdVeryLooseAllPhotons/Particles",
            "Filter": "CL>0.4"
        },
        "piz": {
            "MassWindow": 60 * MeV,
            "PhotonPT": 100 * MeV,
            "PhotonCL": 0.4,
            "Filter": "(CL>0.4) & (PT>180*MeV)"
        },
        'KShortLL': {
            'TES':
            "Phys/StdVeryLooseKsLL/Particles",
            'Filter':
            "(ADMASS('KS0')<25*MeV) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.05) & (MAXTREE('pi+'==ABSID,MIPCHI2DV(PRIMARY))>6) & (CHI2VXNDF<6) & (BPVVDCHI2>12)"
        },
        'KShortDD': {
            'TES':
            "Phys/StdLooseKsDD/Particles",
            'Filter':
            "(ADMASS('KS0')<25*MeV) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.05) & (CHI2VXNDF<6) & (BPVVDCHI2>12) & (BPVVDZ>500*mm)"
        },
        'LambdaLL': {
            'TES':
            'Phys/StdVeryLooseLambdaLL/Particles',
            'Filter':
            """(ADMASS('Lambda0')<20*MeV) & (MAXTREE('p+'==ABSID,PROBNNp)>0.1) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.05) & (CHI2VXNDF<6) & (BPVVDCHI2>48) &
                     (MAXTREE('p+'==ABSID,PT)>250*MeV) & (MAXTREE('p+'==ABSID,P)>7500*MeV) & (MAXTREE('pi+'==ABSID,TRGHOSTPROB)<0.3) &
                     (MAXTREE('p+'==ABSID,TRGHOSTPROB)<0.3) & (P>12*GeV) & (PT>600*MeV)"""
        },
        'LambdaDD': {
            'TES':
            'Phys/StdLooseLambdaDD/Particles',
            'Filter':
            """(ADMASS('Lambda0')<20*MeV) & (MAXTREE('p+'==ABSID,PROBNNp)>0.25) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.1) & (CHI2VXNDF<6) & (BPVVDZ>500*mm) &
                     (MAXTREE('p+'==ABSID,PT)>300*MeV) & (MAXTREE('p+'==ABSID,P)>7500*MeV) & (MAXTREE('pi+'==ABSID,TRGHOSTPROB)<0.3) &
                     (MAXTREE('p+'==ABSID,TRGHOSTPROB)<0.3) & (BPVVDCHI2>48) & (P>12*GeV) & (PT>600*MeV)"""
        },
        "Sigmap": {
            "DD": ["[Sigma+ -> p+ pi0]cc"],
            "CombCut": "ADAMASS('Sigma+')<100*MeV",
            "MotherCut": "(BPVVDZ>5*mm)"
        },
        "Sigmaz": {
            "DD": ["[Sigma0 -> Lambda0 gamma]cc"],
            "CombCut": "ADAMASS('Sigma0')<100*MeV",
            "MotherCut": "ALL"
        },
        "XicpToSpKmPip": {
            "DD": ["[Xi_c+ -> K- pi+ Sigma+]cc"],
            "Comb12Cut":
            "(ACHI2DOCA(1,2)<8) & (AMASS(1,2)<1500*MeV) & (ASUM(PT)>1800*MeV)",
            "CombCut":
            "ADAMASS('Xi_c+')<80*MeV",
            "MotherCut":
            "(ADMASS('Xi_c+')<52*MeV) & (BPVDIRA>0.999) & (BPVIPCHI2()<12) & (CHI2VXNDF<6) & (BPVVDZ>0*mm) & (CHILD(3,VFASPF(VZ))-VFASPF(VZ)>5*mm)"
        },
        "XicpToSzKzPip": {
            "DD": ["[Xi_c+ -> pi+ KS0 Sigma0]cc"],
            "Comb12Cut":
            "(ACHI2DOCA(1,2)<15) & (AMASS(1,2)<1500*MeV) & (ASUM(PT)>1800*MeV)",
            "CombCut":
            "ADAMASS('Xi_c+')<100*MeV",
            "MotherCut":
            "(ADMASS('Xi_c+')<52*MeV) & (BPVDIRA>0.999) & (BPVIPCHI2()<12) & (CHI2VXNDF<6) & (BPVVDZ>0*mm)"
        },
        "XiczToSzKmPip": {
            "DD": ["[Xi_c0 -> K- pi+ Sigma0]cc"],
            "Comb12Cut":
            "(ACHI2DOCA(1,2)<8) & (AMASS(1,2)<1500*MeV) & (ASUM(PT)>1800*MeV)",
            "CombCut":
            "ADAMASS('Xi_c0')<80*MeV",
            "MotherCut":
            "(ADMASS('Xi_c0')<52*MeV) & (BPVDIRA>0.999) & (BPVIPCHI2()<12) & (CHI2VXNDF<6) & (BPVVDZ>0*mm)"
        },
    },
    "STREAMS": {
        "Charm": [
            "StrippingXicpToSpKmPipLine",
            "StrippingXicpToSzKzPipLine",
            "StrippingXiczToSzKmPipLine",
        ]
    },
    "WGs": ["Charm"]
}


# Configure the LineBuilder
class XicToSigmaKPiConf(LineBuilder):

    __configuration_keys__ = default_config["CONFIG"].keys()

    def __init__(self, moduleName, config):
        LineBuilder.__init__(self, moduleName, config)

        # Stable daughters in the decay
        pi = FilterSelection(
            moduleName + "DetachedLongPions",
            [AutomaticData(config["pi"]["TES"])],
            Code=config["pi"]["Filter"])
        K = FilterSelection(
            moduleName + "DetachedLongKaons",
            [AutomaticData(config["K"]["TES"])],
            Code=config["K"]["Filter"])
        p = FilterSelection(
            moduleName + "DetachedLongProtons",
            [AutomaticData(config["p"]["TES"])],
            Code=config["p"]["Filter"])

        gamma = FilterSelection(
            moduleName + "Gammas", [AutomaticData(config["gamma"]["TES"])],
            Code=config["gamma"]["Filter"])

        # pi0s from Sigma+ decays are very soft. We can gain efficiency by loosening StdResolvedPi0
        pizr = ResolvedPi0Maker(
            moduleName + "VeryLoosePi02gg",
            DecayDescriptor="pi0",
            MassWindow=config["piz"]["MassWindow"])
        make_loose_photons = PhotonMaker(
            moduleName + "LoosePhotons",
            PtCut=config["piz"]["PhotonPT"],
            ConfLevelCut=config["piz"]["PhotonCL"])
        pizr.addTool(make_loose_photons)
        #adding this to DoD since I don't know how to access the output of the Makers directly
        updateDoD(pizr)

        LambdaLL = FilterSelection(
            moduleName + "LambdaLL",
            [AutomaticData(config['LambdaLL']['TES'])],
            Code=config['LambdaLL']['Filter'])
        LambdaDD = FilterSelection(
            moduleName + "LambdaDD",
            [AutomaticData(config['LambdaDD']['TES'])],
            Code=config['LambdaDD']['Filter'])
        KShortLL = FilterSelection(
            moduleName + "KShortLL",
            [AutomaticData(config['KShortLL']['TES'])],
            Code=config['KShortLL']['Filter'])
        KShortDD = FilterSelection(
            moduleName + "KShortDD",
            [AutomaticData(config['KShortDD']['TES'])],
            Code=config['KShortDD']['Filter'])

        Lambda = MergedSelection(
            moduleName + "MergedLambdas",
            RequiredSelections=[LambdaLL, LambdaDD])
        KShort = MergedSelection(
            moduleName + "MergedKShorts",
            RequiredSelections=[KShortLL, KShortDD])

        # When building the Sigma+, we try to cut as loose as possible, because the pi0 still points back to the PV region.
        Sigmap = SimpleSelection(
            moduleName + "Sigmap",
            CombineParticles, [
                p,
                AutomaticData("Phys/" + moduleName +
                              "VeryLoosePi02gg/Particles")
            ],
            DaughtersCuts={
                "pi0": config["piz"]["Filter"],
                "p+": "ALL"
            },
            DecayDescriptors=config["Sigmap"]["DD"],
            CombinationCut=config["Sigmap"]["CombCut"],
            MotherCut=config["Sigmap"]["MotherCut"])

        Sigmaz = SimpleSelection(
            moduleName + "Sigmaz",
            CombineParticles, [Lambda, gamma],
            DecayDescriptors=config["Sigmaz"]["DD"],
            CombinationCut=config["Sigmaz"]["CombCut"],
            MotherCut=config["Sigmaz"]["MotherCut"])

        # Do the 3-body combination. Use DTF offline
        XicpToSpKmPip = Combine3BodySelection(
            moduleName + "XicpToSpKmPip", [Sigmap, K, pi],
            DecayDescriptors=config["XicpToSpKmPip"]["DD"],
            Combination12Cut=config["XicpToSpKmPip"]["Comb12Cut"],
            CombinationCut=config["XicpToSpKmPip"]["CombCut"],
            MotherCut=config["XicpToSpKmPip"]["MotherCut"])

        XicpToSzKzPip = Combine3BodySelection(
            moduleName + "XicpToSzKzPip", [Sigmaz, KShort, pi],
            DecayDescriptors=config["XicpToSzKzPip"]["DD"],
            Combination12Cut=config["XicpToSzKzPip"]["Comb12Cut"],
            CombinationCut=config["XicpToSzKzPip"]["CombCut"],
            MotherCut=config["XicpToSzKzPip"]["MotherCut"])

        XiczToSzKmPip = Combine3BodySelection(
            moduleName + "XiczToSzKmPip", [Sigmaz, K, pi],
            DecayDescriptors=config["XiczToSzKmPip"]["DD"],
            Combination12Cut=config["XiczToSzKmPip"]["Comb12Cut"],
            CombinationCut=config["XiczToSzKmPip"]["CombCut"],
            MotherCut=config["XiczToSzKmPip"]["MotherCut"])

        # Create the stripping lines
        self.registerLine(
            StrippingLine("XicpToSpKmPipLine", algos=[XicpToSpKmPip]))
        self.registerLine(
            StrippingLine("XicpToSzKzPipLine", algos=[XicpToSzKzPip]))
        self.registerLine(
            StrippingLine("XiczToSzKmPipLine", algos=[XiczToSzKmPip]))
