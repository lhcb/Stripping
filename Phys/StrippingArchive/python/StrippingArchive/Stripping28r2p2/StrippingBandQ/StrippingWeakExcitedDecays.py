###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping line for searches for weak decays of low-lying excited states
Author: Vitalii Lisovskyi
'''
from GaudiKernel.SystemOfUnits import MeV
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StandardParticles import StdAllLoosePions, StdAllLooseKaons
from StandardParticles import StdLoosePions, StdNoPIDsDownPions, StdLooseKaons, StdLooseDownKaons
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays, DaVinci__N4BodyDecays
from Gaudi.Configuration import *
__author__ = ['Vitalii Lisovskyi']
__date__ = '23/06/2023'
__version__ = '$Revision: 1 $'
__all__ = ('WeakExcitedDecaysConf',
           'default_config')

default_config = {
    'NAME': 'WeakExcitedDecays',
    'BUILDERTYPE': 'WeakExcitedDecaysConf',
    'CONFIG': {'TRCHI2DOF':       4.,
               'KaonProbNN_Loose': 0.1,
               'KaonProbNN': 0.15,
               'PionProbNN': 0.1,
               'ProtProbNN_Loose': 0.1,
               'ProtProbNN': 0.2,
               'ProtProbNNK': 0.85,
               'OmegacMassMin': 2560,
               'OmegacMassMax': 2900,
               'OmegacMassMinComb': 2520,
               'OmegacMassMaxComb': 2940,
               'XicMassMin': 2350,
               'XicMassMax': 2820,
               'XicMassMax_Tight': 2700,
               'XicMassMinComb': 2320,
               'XicMassMaxComb': 2870,
               'FromB_PT_Loose': 1200,
               'FromB_PT': 1600,
               'FromB_PT_Tight': 2000,
               'FromB_BPVVDCHI2_Loose': 25,
               'FromB_BPVVDCHI2': 60,
               'FromB_BPVVDCHI2_Tight': 100,
               'FromB_DIRA': 0.9,
               'FromB_DIRA_Tight': 0.999,
               'TrackMinIPCHI2': 4,
               'TrackMinIPCHI2_Tight': 6,
               'MaxFromB_VertChi2DOF_Loose': 25,
               'MaxFromB_VertChi2DOF': 10,
               'MaxFromB_VertChi2DOF_Tight': 6,
               'HyperonPT': 500.,
               'ProtonPT': 750.,
               'JpsiMassWindow':      100.,
               'KaonPIDK': -5.,
               'DLSForLongLived':       5.,
               'XiMassWindow':      30.,
               'OmegaMassWindow':      30.,
               'BHadronMassWindow':     500.,
               'BHadronMassWindow_Loose':     900.,
               'DiHadronADOCA': 0.7,
               'DiHadronMass': 3000.,
               'CORRM_MIN_LOOSE':  3200.,
               },
    # 'STREAMS' : [ 'Dimuon' ],
    'WGs': ['BandQ'],
    'STREAMS': {'Dimuon': [
        "StrippingWeakExcitedDecaysOmegabstar2JpsiOmegaLine",
        "StrippingWeakExcitedDecaysBcstar2JpsiDsLine",
    ],
        'Bhadron': [
        "StrippingWeakExcitedDecaysOmegacstar2pKKpi_inclfromBLine",
        "StrippingWeakExcitedDecaysOmegacstar2Omegapi_inclfromBLine",
        "StrippingWeakExcitedDecaysXicstarz2pKKpi_inclfromBLine",
        "StrippingWeakExcitedDecaysXicstarp2pKpi_inclfromBLine",
        "StrippingWeakExcitedDecaysXicstarz2XiPi_inclfromBLine",
        "StrippingWeakExcitedDecaysOmegab2OmegacstarPi_pKKpiLine",
        "StrippingWeakExcitedDecaysOmegab2OmegacstarPi_OmpiLine",
        "StrippingWeakExcitedDecaysXib2XicstarPi_pKpiLine",
        "StrippingWeakExcitedDecaysOmegab2XicstarKPi_pKpiLine",
        "StrippingWeakExcitedDecaysXibm2XicstarzPi_pKKpiLine",
        "StrippingWeakExcitedDecaysOmegab2OmegacstarMu_pKKpiLine",
        "StrippingWeakExcitedDecaysOmegab2OmegacstarE_pKKpiLine",
    ]
    },

}


# -------------------------------------------------------------------------------------------------------------
class WeakExcitedDecaysConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        # define input daughter lists for various selections
        self.WideJpsiList = DataOnDemand(
            Location="Phys/StdLooseJpsi2MuMu/Particles")
        self.JpsiList = self.createSubSel(OutputList='JpsiForHeavyBaryons' + self.name,
                                          InputList=self.WideJpsiList,
                                          # Cuts = "(PFUNA(ADAMASS('J/psi(1S)')) < %(JpsiMassWindow)s)" % self.config)
                                          Cuts="(ADMASS('J/psi(1S)') < %(JpsiMassWindow)s)" % self.config)

        # make a merged list for pions
        self.MergedPionsList = MergedSelection("MergedPionsFor" + self.name,
                                               RequiredSelections=[DataOnDemand(Location="Phys/StdAllLoosePions/Particles"),  # take all long tracks
                                                                   DataOnDemand(Location="Phys/StdNoPIDsDownPions/Particles")])  # take all downstream tracks

        self.MergedKaonsList = MergedSelection("MergedKaonsFor" + self.name,
                                               RequiredSelections=[DataOnDemand(Location="Phys/StdAllLooseKaons/Particles"),  # take all long tracks
                                                                   DataOnDemand(Location="Phys/StdLooseDownKaons/Particles")])  # take all downstream tracks
        # make a merged list for kaons
        self.PionsList = self.createSubSel(OutputList="PionsFor" + self.name,
                                           InputList=self.MergedPionsList,
                                           Cuts="(TRCHI2DOF < %(TRCHI2DOF)s )" % self.config)
        self.KaonsList = self.createSubSel(OutputList="KaonsFor" + self.name,
                                           InputList=self.MergedKaonsList,
                                           Cuts="(TRCHI2DOF < %(TRCHI2DOF)s ) & (PIDK > %(KaonPIDK)s)" % self.config)

        self.LambdaListLoose = MergedSelection("StdLooseLambdaMergedFor" + self.name,
                                               RequiredSelections=[DataOnDemand(Location="Phys/StdLooseLambdaDD/Particles"),
                                                                   DataOnDemand(Location="Phys/StdVeryLooseLambdaLL/Particles")])
        self.LambdaList = self.createSubSel(OutputList="LambdaFor" + self.name,
                                            InputList=self.LambdaListLoose,
                                            Cuts="(MAXTREE('p+'==ABSID, PT) > 400.*MeV) "
                                            "& (MAXTREE('pi-'==ABSID, PT) > 0.*MeV) "
                                            "& (ADMASS('Lambda0') < 15.*MeV) & (VFASPF(VCHI2) < 20 )  & (BPVDLS> %(DLSForLongLived)s ) " % self.config)

        self.XiminusList = self.makeXiminus()
        self.OmegaminusList = self.makeOmegaminus()
        self.DsplusList = self.makeDs2KKpi()

        self.makeOmegabstarminus2JpsiOmega_line()
        self.makeOmegacstar2pKKpi_inclfromB_line()
        self.makeOmegacstar2Omegapi_inclfromB_line()
        self.makeXicstarp2pKpi_inclfromB_line()
        self.makeXicstarz2pKKpi_inclfromB_line()
        self.makeXicstarz2XiPi_inclfromB_line()
        self.makeBcstar2JpsiDs_line()

        self.makeOmegab2OmegacstarPi_pKKpi_line()
        self.makeOmegab2OmegacstarPi_Ompi_line()
        self.makeXib2XicstarPi_pKpi_line()
        self.makeOmegab2XicstarKPi_pKpi_line()
        self.makeXicstarz2pKKpi_exclfromB_line()

        self.makeOmegab2OmegacstarMu_pKKpi_line()
        self.makeOmegab2OmegacstarE_pKKpi_line()

    def createSubSel(self, OutputList, InputList, Cuts):
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code=Cuts)
        return Selection(OutputList,
                         Algorithm=filter,
                         RequiredSelections=[InputList])

    def createCombinationSel(self, OutputList,
                             DecayDescriptor,
                             DaughterLists,
                             DaughterCuts={},
                             PreVertexCuts="ALL",
                             PostVertexCuts="ALL"):
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles(DecayDescriptor=DecayDescriptor,
                                    DaughtersCuts=DaughterCuts,
                                    MotherCut=PostVertexCuts,
                                    CombinationCut=PreVertexCuts,
                                    ReFitPVs=True)
        return Selection(OutputList,
                         Algorithm=combiner,
                         RequiredSelections=DaughterLists)


# ------------------------------------------------------------------------------------------
# Hyperon builders

    def makeXiminus(self):
        ''' Make a Xi minus candidate '''
        Ximinus2LambdaPi = self.createCombinationSel(OutputList="Ximinus2LambdaPi" + self.name,
                                                     DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
                                                     DaughterLists=[
                                                         self.PionsList, self.LambdaList],
                                                     DaughterCuts={
                                                         "pi-": "(BPVIPCHI2()>9)"},
                                                     PreVertexCuts="(ADAMASS('Xi-') < %(XiMassWindow)s*MeV)" % self.config,
                                                     PostVertexCuts="(VFASPF(VCHI2/VDOF)<25) &(BPVDLS > %(DLSForLongLived)s) " % self.config)
        return Ximinus2LambdaPi

    def makeOmegaminus(self):
        ''' Make an Omega minus candidate '''
        Omegaminus2LambdaK = self.createCombinationSel(OutputList="Omegaminus2LambdaK" + self.name,
                                                       DecayDescriptor="[Omega- -> Lambda0 K-]cc",
                                                       DaughterLists=[
                                                           self.KaonsList, self.LambdaList],
                                                       DaughterCuts={
                                                           "K-": "(BPVIPCHI2()>9)"},
                                                       PreVertexCuts="(ADAMASS('Omega-') < %(OmegaMassWindow)s*MeV)" % self.config,
                                                       PostVertexCuts="(VFASPF(VCHI2/VDOF)<25) & (BPVDLS> %(DLSForLongLived)s) " % self.config)

        return Omegaminus2LambdaK

    def makeDs2KKpi(self):
        '''Makes D_s+ -> K- K+ pi+'''
        _Code = "(PT > %(HyperonPT)s *MeV) & (2== NINTREE((ABSID==321) & (PROBNNk>%(KaonProbNN)s)))" % self.config
        _Filter = FilterDesktop(Code=_Code)
        _stdDs = DataOnDemand(Location="Phys/StdLooseDsplus2KKPi/Particles")
        return Selection("Ds2KKPi" + self.name, Algorithm=_Filter, RequiredSelections=[_stdDs])


# ------------------------------------------------------------------------------------------


    def makeOmegabstarminus2JpsiOmega_line(self):
        """
        Create a Omegab(*)- -> J/psi Omega (prompt)
        """
        Omegabminus2JpsiOmegastar = self.createCombinationSel(OutputList="Omegabminus2JpsiOmega" + self.name,
                                                              DecayDescriptor="[Omega_b*- -> Omega- J/psi(1S)]cc",
                                                              DaughterLists=[
                                                                  self.JpsiList, self.OmegaminusList],
                                                              PreVertexCuts="(ADAMASS('Omega_b*-') <600*MeV )",
                                                              PostVertexCuts="(VFASPF(VCHI2/VDOF)<25)  &  (ADMASS('Omega_b*-') <%(BHadronMassWindow)s *MeV)" % self.config)
        Omegabminus2JpsiOmegaLine = StrippingLine(self.name + "Omegabstar2JpsiOmega" + "Line",
                                                  algos=[Omegabminus2JpsiOmegastar])
        self.registerLine(Omegabminus2JpsiOmegaLine)

    def makeOmegacstar2pKKpi_inclfromB(self):
        """
        Create a Omegac(*)0 -> p K- K- pi+ (inclusive detached)
        """
        _daughterKCut = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _daughterpCut = "(PROBNNp  > %(ProtProbNN)s) & (PROBNNk < %(ProtProbNNK)s) & (PIDp > -2.0) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (P>10000) & (TRGHOSTPROB < 0.5)" % self.config

        _comboCuts12 = "(AM < %(DiHadronMass)s * MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % self.config
        _comboCuts123 = "(AM < %(DiHadronMass)s * MeV) & (ADOCA(1,3)<%(DiHadronADOCA)s*mm)" % self.config
        _combinationCut = "(ASUM(PT)> %(FromB_PT)s*MeV) & (AM> %(OmegacMassMinComb)s*MeV) & (AM< %(OmegacMassMaxComb)s*MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(M> %(OmegacMassMin)s*MeV) & (M< %(OmegacMassMax)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2)s) & (BPVDIRA>%(FromB_DIRA)s)" % self.config
        _Omegac = DaVinci__N4BodyDecays(
            DecayDescriptor="[Omega_c*0 -> p+ K- K- pi+]cc",
            DaughtersCuts={
                "K+": _daughterKCut,
                "pi+": _daughterpiCut,
                "p+": _daughterpCut
            },
            Combination12Cut=_comboCuts12,
            Combination123Cut=_comboCuts123,
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        _stdANNKaon = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
        _stdANNPion = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
        _stdANNProtons = DataOnDemand(
            Location="Phys/StdLooseANNProtons/Particles")

        return Selection(
            "SelOmegacstar2pKKpi_inclfromB"+self.name,
            Algorithm=_Omegac,
            RequiredSelections=[_stdANNKaon, _stdANNPion, _stdANNProtons])

    def makeOmegacstar2pKKpi_inclfromB_line(self):

        Omegacstar_inclfromB = self.makeOmegacstar2pKKpi_inclfromB()
        Omegacstar_inclfromBLine = StrippingLine(self.name + "Omegacstar2pKKpi_inclfromB" + "Line",
                                                 algos=[Omegacstar_inclfromB])
        self.registerLine(Omegacstar_inclfromBLine)

    def makeOmegacstar2pKKpi_exclfromB(self, name2):
        """
        Create a Omegac(*)0 -> p K- K- pi+ (for exclusive detached selection)
        """
        _daughterKCut = "(PROBNNk  > %(KaonProbNN_Loose)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _daughterpCut = "(PROBNNp  > %(ProtProbNN_Loose)s) &  (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (P>7500) & (TRGHOSTPROB < 0.5)" % self.config

        _comboCuts12 = "(AM < %(DiHadronMass)s * MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % self.config
        _comboCuts123 = "(AM < %(DiHadronMass)s * MeV) & (ADOCA(1,3)<%(DiHadronADOCA)s*mm)" % self.config
        _combinationCut = "(ASUM(PT)> %(FromB_PT_Loose)s*MeV) & (AM> %(OmegacMassMinComb)s*MeV) & (AM< %(OmegacMassMaxComb)s*MeV) & (ACUTDOCA(0.75*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(M> %(OmegacMassMin)s*MeV) & (M< %(OmegacMassMax)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF_Loose)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2_Loose)s) & (BPVDIRA>%(FromB_DIRA)s)" % self.config
        _Omegac = DaVinci__N4BodyDecays(
            DecayDescriptor="[Omega_c*0 -> p+ K- K- pi+]cc",
            DaughtersCuts={
                "K+": _daughterKCut,
                "pi+": _daughterpiCut,
                "p+": _daughterpCut
            },
            Combination12Cut=_comboCuts12,
            Combination123Cut=_comboCuts123,
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        _stdANNKaon = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
        _stdANNPion = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
        _stdANNProtons = DataOnDemand(
            Location="Phys/StdLooseANNProtons/Particles")

        return Selection(
            "SelOmegacstar2pKKpi_exclfromB"+self.name+name2,
            Algorithm=_Omegac,
            RequiredSelections=[_stdANNKaon, _stdANNPion, _stdANNProtons])

    def makeOmegab2OmegacstarPi_pKKpi(self):
        """
        Create a Omegab -> Omegac* Pi (without pion ID so that it can also be a K)
        """
        _daughterOmegaCut = "(ALL)"
        _daughterpiCut = "(MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _combinationCut = "(ADAMASS('Omega_b-') < 1000 *MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(ADMASS('Omega_b-') <%(BHadronMassWindow_Loose)s *MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2)s) & (BPVDIRA>%(FromB_DIRA_Tight)s)" % self.config
        _Omegab = CombineParticles(
            DecayDescriptor="[Omega_b- -> Omega_c*0 pi-]cc",
            DaughtersCuts={
                "Omega_c*0": _daughterOmegaCut,
                "pi-": _daughterpiCut,
            },
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        Omegacstar_exclfromB = self.makeOmegacstar2pKKpi_exclfromB('Hadronic')
        _stdPion = DataOnDemand(Location="Phys/StdNoPIDsPions/Particles")

        return Selection(
            "SelOmegab2OmegacstarPi_pKKpi"+self.name,
            Algorithm=_Omegab,
            RequiredSelections=[Omegacstar_exclfromB, _stdPion])

    def makeOmegab2OmegacstarPi_pKKpi_line(self):

        Omegacstar_exclfromB = self.makeOmegab2OmegacstarPi_pKKpi()
        Omegacstar_exclfromBLine = StrippingLine(self.name + "Omegab2OmegacstarPi_pKKpi" + "Line",
                                                 algos=[Omegacstar_exclfromB])
        self.registerLine(Omegacstar_exclfromBLine)

    def makeOmegab2OmegacstarMu_pKKpi(self):
        """
        Create a Omegab -> Omegac* munu
        """
        _daughterOmegaCut = "(ALL)"
        _daughterellCut = "(MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5) & (PT > 500 * MeV) & (PROBNNmu > 0.1)" % self.config
        _combinationCut = "(ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(BPVCORRM > %(CORRM_MIN_LOOSE)s *MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF_Loose)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2_Loose)s) & (BPVDIRA>%(FromB_DIRA)s)" % self.config
        _Omegab = CombineParticles(
            DecayDescriptors=["[Omega_b- -> Omega_c*0 mu-]cc",
                              "[Omega_b- -> Omega_c*0 mu+]cc"],
            DaughtersCuts={
                "Omega_c*0": _daughterOmegaCut,
                "mu-": _daughterellCut,
            },
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        Omegacstar_exclfromB = self.makeOmegacstar2pKKpi_exclfromB('SL')
        _stdMuon = DataOnDemand(Location="Phys/StdLooseMuons/Particles")

        return Selection(
            "SelOmegab2OmegacstarMu_pKKpi"+self.name,
            Algorithm=_Omegab,
            RequiredSelections=[Omegacstar_exclfromB, _stdMuon])

    def makeOmegab2OmegacstarMu_pKKpi_line(self):

        Omegacstar_exclfromB = self.makeOmegab2OmegacstarMu_pKKpi()
        Omegacstar_exclfromBLine = StrippingLine(self.name + "Omegab2OmegacstarMu_pKKpi" + "Line",
                                                 algos=[Omegacstar_exclfromB])
        self.registerLine(Omegacstar_exclfromBLine)

    def makeOmegab2OmegacstarE_pKKpi(self):
        """
        Create a Omegab -> Omegac* enu
        """
        _daughterOmegaCut = "(ALL)"
        _daughterellCut = "(MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5) & (PT > 500 * MeV) & (P > 3000 * MeV) & (PIDe > 3) & (PROBNNe > 0.2)" % self.config
        _combinationCut = "(ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(BPVCORRM > %(CORRM_MIN_LOOSE)s *MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2_Loose)s) & (BPVDIRA>%(FromB_DIRA)s)" % self.config
        _Omegab = CombineParticles(
            DecayDescriptors=["[Omega_b- -> Omega_c*0 e-]cc",
                              "[Omega_b- -> Omega_c*0 e+]cc"],
            DaughtersCuts={
                "Omega_c*0": _daughterOmegaCut,
                "e-": _daughterellCut,
            },
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        Omegacstar_exclfromB = self.makeOmegacstar2pKKpi_exclfromB('SL_E')
        _stdElectron = DataOnDemand(
            Location="Phys/StdLooseElectrons/Particles")

        return Selection(
            "SelOmegab2OmegacstarE_pKKpi"+self.name,
            Algorithm=_Omegab,
            RequiredSelections=[Omegacstar_exclfromB, _stdElectron])

    def makeOmegab2OmegacstarE_pKKpi_line(self):

        Omegacstar_exclfromB = self.makeOmegab2OmegacstarE_pKKpi()
        Omegacstar_exclfromBLine = StrippingLine(self.name + "Omegab2OmegacstarE_pKKpi" + "Line",
                                                 algos=[Omegacstar_exclfromB])
        self.registerLine(Omegacstar_exclfromBLine)

    def makeOmegacstar2OmegaPi_inclfromB(self):
        """
        Create a Omegac(*)0 -> Omega- pi+ (inclusive detached)
        """
        _daughterOmegaCut = "(PT > %(HyperonPT)s)" % self.config
        _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _combinationCut = "(ASUM(PT)> %(FromB_PT)s*MeV) & (AM> %(OmegacMassMinComb)s*MeV) & (AM< %(OmegacMassMaxComb)s*MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(M> %(OmegacMassMin)s*MeV) & (M< %(OmegacMassMax)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2)s) & (BPVDIRA>%(FromB_DIRA)s)" % self.config
        _Omegac = CombineParticles(
            DecayDescriptor="[Omega_c*0 -> Omega- pi+]cc",
            DaughtersCuts={
                "Omega-": _daughterOmegaCut,
                "pi+": _daughterpiCut,
            },
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        _stdANNPion = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
        return Selection(
            "SelOmegacstar2Omegapi_inclfromB"+self.name,
            Algorithm=_Omegac,
            RequiredSelections=[self.OmegaminusList, _stdANNPion])

    def makeOmegacstar2Omegapi_inclfromB_line(self):

        Omegacstar_inclfromB = self.makeOmegacstar2OmegaPi_inclfromB()
        Omegacstar_inclfromBLine = StrippingLine(self.name + "Omegacstar2Omegapi_inclfromB" + "Line",
                                                 algos=[Omegacstar_inclfromB])
        self.registerLine(Omegacstar_inclfromBLine)

    def makeOmegacstar2OmegaPi_exclfromB(self):
        """
        Create a Omegac(*)0 -> Omega- pi+ (for exclusive detached)
        """
        _daughterOmegaCut = "(PT > %(HyperonPT)s)" % self.config
        _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _combinationCut = "(ASUM(PT)> %(FromB_PT_Loose)s*MeV) & (AM> %(OmegacMassMinComb)s*MeV) & (AM< %(OmegacMassMaxComb)s*MeV) & (ACUTDOCA(0.75*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(M> %(OmegacMassMin)s*MeV) & (M< %(OmegacMassMax)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF_Loose)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2_Loose)s) & (BPVDIRA>%(FromB_DIRA)s)" % self.config
        _Omegac = CombineParticles(
            DecayDescriptor="[Omega_c*0 -> Omega- pi+]cc",
            DaughtersCuts={
                "Omega-": _daughterOmegaCut,
                "pi+": _daughterpiCut,
            },
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        _stdANNPion = DataOnDemand(Location="Phys/StdNoPIDsPions/Particles")

        return Selection(
            "SelOmegacstar2Omegapi_exclfromB"+self.name,
            Algorithm=_Omegac,
            RequiredSelections=[self.OmegaminusList, _stdANNPion])

    def makeOmegab2OmegacstarPi_Ompi(self):
        """
        Create a Omegab -> Omegac* Pi (without pion ID so that it can also be a K)
        """
        _daughterOmegaCut = "(ALL)"
        _daughterpiCut = "(MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _combinationCut = "(ADAMASS('Omega_b-') < 1000 *MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(ADMASS('Omega_b-') <%(BHadronMassWindow_Loose)s *MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2)s) & (BPVDIRA>%(FromB_DIRA_Tight)s)" % self.config
        _Omegab = CombineParticles(
            DecayDescriptor="[Omega_b- -> Omega_c*0 pi-]cc",
            DaughtersCuts={
                "Omega_c*0": _daughterOmegaCut,
                "pi-": _daughterpiCut,
            },
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        Omegacstar_exclfromB = self.makeOmegacstar2OmegaPi_exclfromB()
        _stdPion = DataOnDemand(Location="Phys/StdNoPIDsPions/Particles")

        return Selection(
            "SelOmegab2OmegacstarPi_Ompi"+self.name,
            Algorithm=_Omegab,
            RequiredSelections=[Omegacstar_exclfromB, _stdPion])

    def makeOmegab2OmegacstarPi_Ompi_line(self):

        Omegacstar_exclfromB = self.makeOmegab2OmegacstarPi_Ompi()
        Omegacstar_exclfromBLine = StrippingLine(self.name + "Omegab2OmegacstarPi_Ompi" + "Line",
                                                 algos=[Omegacstar_exclfromB])
        self.registerLine(Omegacstar_exclfromBLine)

    ### here go the Xic+ selections ###

    def makeXicstarp2pKpi_inclfromB(self):
        """
        Create a Xic*+ -> p K- pi+ (inclusive detached)
        """
        _daughterKCut = "(PROBNNk  > %(KaonProbNN)s) & (PIDK > 2.0) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.35) & (PT > 350.0 * MeV)" % self.config
        _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (PROBNNk < %(ProtProbNNK)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)& (TRGHOSTPROB < 0.35) & (PT > 300.0 * MeV) & (P>2000.0 * MeV)" % self.config
        _daughterpCut = "(PROBNNp  > %(ProtProbNN)s)  & (PROBNNk < %(ProtProbNNK)s) & (PIDp > 5.0) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (P>10000 * MeV) & (PT>%(ProtonPT)s * MeV) & (TRGHOSTPROB < 0.35)" % self.config
        _comboCuts12 = "(AM < %(DiHadronMass)s * MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % self.config
        _combinationCut = "(ASUM(PT)> %(FromB_PT)s*MeV) & (AM>%(XicMassMinComb)s*MeV) & (AM<%(XicMassMaxComb)s*MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(PT > %(FromB_PT_Tight)s*MeV) & (M>%(XicMassMin)s*MeV) & (M<%(XicMassMax_Tight)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF_Tight)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2_Tight)s) & (BPVDIRA>%(FromB_DIRA_Tight)s) & (NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(TrackMinIPCHI2_Tight)s)) > 1)" % self.config
        _Xic = DaVinci__N3BodyDecays(
            DecayDescriptor="[Xi'_c+ -> p+ K- pi+]cc",
            DaughtersCuts={
                "K+": _daughterKCut,
                "pi+": _daughterpiCut,
                "p+": _daughterpCut
            },
            Combination12Cut=_comboCuts12,
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        _stdANNKaon = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
        _stdANNPion = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
        _stdANNProtons = DataOnDemand(
            Location="Phys/StdLooseANNProtons/Particles")

        return Selection(
            "SelXicstarp2pKpi_inclfromB"+self.name,
            Algorithm=_Xic,
            RequiredSelections=[_stdANNKaon, _stdANNPion, _stdANNProtons])

    def makeXicstarp2pKpi_inclfromB_line(self):

        Xicstar_inclfromB = self.makeXicstarp2pKpi_inclfromB()
        Xicstar_inclfromBLine = StrippingLine(self.name + "Xicstarp2pKpi_inclfromB" + "Line",
                                              algos=[Xicstar_inclfromB])
        self.registerLine(Xicstar_inclfromBLine)

    def makeXicstarp2pKpi_exclfromB(self, name2):
        """
        Create a Xic*+ -> p K- pi+ (for exclusive detached)
        """
        _daughterKCut = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _daughterpiCut = "(PROBNNpi > %(PionProbNN)s)  & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)& (TRGHOSTPROB < 0.5)" % self.config
        _daughterpCut = "(PROBNNp  > %(ProtProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (P>10000 * MeV) & (TRGHOSTPROB < 0.5)" % self.config
        _comboCuts12 = "(AM < %(DiHadronMass)s * MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % self.config
        _combinationCut = "(ASUM(PT)> %(FromB_PT)s*MeV) & (AM>%(XicMassMinComb)s*MeV) & (AM<%(XicMassMaxComb)s*MeV) & (ACUTDOCA(0.75*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(PT > %(FromB_PT_Loose)s*MeV) & (M>%(XicMassMin)s*MeV) & (M<%(XicMassMax)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2_Loose)s) & (BPVDIRA>%(FromB_DIRA)s)" % self.config
        _Xic = DaVinci__N3BodyDecays(
            DecayDescriptor="[Xi'_c+ -> p+ K- pi+]cc",
            DaughtersCuts={
                "K+": _daughterKCut,
                "pi+": _daughterpiCut,
                "p+": _daughterpCut
            },
            Combination12Cut=_comboCuts12,
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        _stdANNKaon = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
        _stdANNPion = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
        _stdANNProtons = DataOnDemand(
            Location="Phys/StdLooseANNProtons/Particles")

        return Selection(
            "SelXicstarp2pKpi_exclfromB"+self.name+name2,
            Algorithm=_Xic,
            RequiredSelections=[_stdANNKaon, _stdANNPion, _stdANNProtons])

    def makeXib2XicstarPi_pKpi(self):
        """
        Create a Xib -> Xic' Pi (without pion ID so that it can also be a K)
        NOTE: the transition Xib -> Xic' is very suppressed in the quark model.
        """
        _daughterXiCut = "(ALL)"
        _daughterpiCut = "(MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _combinationCut = "(ADAMASS('Xi_b0') < 1000 *MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(ADMASS('Xi_b0') <%(BHadronMassWindow_Loose)s *MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2)s) & (BPVDIRA>%(FromB_DIRA_Tight)s)" % self.config
        _Xib = CombineParticles(
            DecayDescriptor="[Xi_b0 -> Xi'_c+ pi-]cc",
            DaughtersCuts={
                "Xi'_c+": _daughterXiCut,
                "pi-": _daughterpiCut,
            },
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        Xicstar_exclfromB = self.makeXicstarp2pKpi_exclfromB('Xib')
        _stdPion = DataOnDemand(Location="Phys/StdNoPIDsPions/Particles")

        return Selection(
            "SelXib2XicstarPi_pKpi"+self.name,
            Algorithm=_Xib,
            RequiredSelections=[Xicstar_exclfromB, _stdPion])

    def makeXib2XicstarPi_pKpi_line(self):

        Xicstar_exclfromB = self.makeXib2XicstarPi_pKpi()
        Xicstar_exclfromBLine = StrippingLine(self.name + "Xib2XicstarPi_pKpi" + "Line",
                                              algos=[Xicstar_exclfromB])
        self.registerLine(Xicstar_exclfromBLine)

    def makeOmegab2XicstarKPi_pKpi(self):
        """
        Create a Omegab -> Xic' K Pi (without pion ID so that it can also be a K)
        """
        _daughterXiCut = "(ALL)"
        _daughterKCut = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _daughterpiCut = "(MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _comboCuts12 = "(AM < 6000 * MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % self.config
        _combinationCut = "(ADAMASS('Omega_b-') < 1000 *MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(ADMASS('Omega_b-') <%(BHadronMassWindow)s *MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2_Tight)s) & (BPVDIRA>%(FromB_DIRA_Tight)s)" % self.config
        _Xib = DaVinci__N3BodyDecays(
            DecayDescriptor="[Omega_b- -> Xi'_c+ K- pi-]cc",
            DaughtersCuts={
                "Xi'_c+": _daughterXiCut,
                "pi-": _daughterpiCut,
                "K-": _daughterKCut,
            },
            Combination12Cut=_comboCuts12,
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        Xicstar_exclfromB = self.makeXicstarp2pKpi_exclfromB('Omegab')
        _stdANNKaon = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
        _stdPion = DataOnDemand(Location="Phys/StdNoPIDsPions/Particles")

        return Selection(
            "SelOmegab2XicstarKPi_pKpi"+self.name,
            Algorithm=_Xib,
            RequiredSelections=[Xicstar_exclfromB, _stdANNKaon, _stdPion])

    def makeOmegab2XicstarKPi_pKpi_line(self):

        Xicstar_exclfromB = self.makeOmegab2XicstarKPi_pKpi()
        Xicstar_exclfromBLine = StrippingLine(self.name + "Omegab2XicstarKPi_pKpi" + "Line",
                                              algos=[Xicstar_exclfromB])
        self.registerLine(Xicstar_exclfromBLine)

    ### here go Xic0 selections ###

    def makeXicstarz2pKKpi_inclfromB(self):
        """
        Create a Xic*0 -> p K- K- pi+ (inclusive detached)

        """
        _daughterKCut = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5) " % self.config
        _daughterpCut = "(PROBNNp  > %(ProtProbNN)s) & (PROBNNk < %(ProtProbNNK)s) & (PIDp > 0.0) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (P>10000) & (TRGHOSTPROB < 0.5)" % self.config
        _comboCuts12 = "(AM < %(DiHadronMass)s * MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % self.config
        _comboCuts123 = "(AM < %(DiHadronMass)s * MeV) & (ADOCA(1,3)<%(DiHadronADOCA)s*mm)" % self.config
        _combinationCut = "(ASUM(PT)> %(FromB_PT)s*MeV) & (AM>%(XicMassMinComb)s*MeV) & (AM<%(XicMassMaxComb)s*MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(M>%(XicMassMin)s*MeV) & (M<%(XicMassMax)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF_Tight)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2)s) & (BPVDIRA>%(FromB_DIRA)s) & (NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(TrackMinIPCHI2_Tight)s)) > 1)" % self.config
        _Omegac = DaVinci__N4BodyDecays(
            DecayDescriptor="[Xi'_c0 -> p+ K- K- pi+]cc",
            DaughtersCuts={
                "K+": _daughterKCut,
                "pi+": _daughterpiCut,
                "p+": _daughterpCut
            },
            Combination12Cut=_comboCuts12,
            Combination123Cut=_comboCuts123,
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        _stdANNKaon = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
        _stdANNPion = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
        _stdANNProtons = DataOnDemand(
            Location="Phys/StdLooseANNProtons/Particles")

        return Selection(
            "SelXicstarz2pKKpi_inclfromB"+self.name,
            Algorithm=_Omegac,
            RequiredSelections=[_stdANNKaon, _stdANNPion, _stdANNProtons])

    def makeXicstarz2pKKpi_inclfromB_line(self):

        Xicstar_inclfromB = self.makeXicstarz2pKKpi_inclfromB()
        Xicstar_inclfromBLine = StrippingLine(self.name + "Xicstarz2pKKpi_inclfromB" + "Line",
                                              algos=[Xicstar_inclfromB])
        self.registerLine(Xicstar_inclfromBLine)

    def makeXicstarz2pKKpi_exclfromB(self):
        """
        Create a Xic*0 -> p K- K- pi+ (for exclusive detached)

        """
        _daughterKCut = "(PROBNNk  > %(KaonProbNN_Loose)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5) " % self.config
        _daughterpCut = "(PROBNNp  > %(ProtProbNN_Loose)s) & (PROBNNk < %(ProtProbNNK)s) & (PIDp > 0.0) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (P>10000) & (TRGHOSTPROB < 0.5)" % self.config
        _comboCuts12 = "(AM < %(DiHadronMass)s * MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % self.config
        _comboCuts123 = "(AM < %(DiHadronMass)s * MeV) & (ADOCA(1,3)<%(DiHadronADOCA)s*mm)" % self.config
        _combinationCut = "(ASUM(PT)> %(FromB_PT_Loose)s*MeV) & (AM>%(XicMassMinComb)s*MeV) & (AM<%(XicMassMaxComb)s*MeV) & (ACUTDOCA(0.75*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(M>%(XicMassMin)s*MeV) & (M<%(XicMassMax)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF_Loose)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2_Loose)s) & (BPVDIRA>%(FromB_DIRA)s)" % self.config
        _Omegac = DaVinci__N4BodyDecays(
            DecayDescriptor="[Xi'_c0 -> p+ K- K- pi+]cc",
            DaughtersCuts={
                "K+": _daughterKCut,
                "pi+": _daughterpiCut,
                "p+": _daughterpCut
            },
            Combination12Cut=_comboCuts12,
            Combination123Cut=_comboCuts123,
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        _stdANNKaon = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
        _stdANNPion = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
        _stdANNProtons = DataOnDemand(
            Location="Phys/StdLooseANNProtons/Particles")

        return Selection(
            "SelXicstarz2pKKpi_exclfromB"+self.name,
            Algorithm=_Omegac,
            RequiredSelections=[_stdANNKaon, _stdANNPion, _stdANNProtons])

    def makeXib2XicstarPi_pKKpi(self):
        """
        Create a Xib -> Xic' Pi (without pion ID so that it can also be a K)
        NOTE: the transition Xib -> Xic' is very suppressed in the quark model.
        """
        _daughterXiCut = "(ALL)"
        _daughterpiCut = "(MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _combinationCut = "(ADAMASS('Xi_b0') < 1000 *MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(ADMASS('Xi_b0') <%(BHadronMassWindow)s *MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2_Tight)s) & (BPVDIRA>%(FromB_DIRA_Tight)s)" % self.config
        _Xib = CombineParticles(
            DecayDescriptor="[Xi_b- -> Xi'_c0 pi-]cc",
            DaughtersCuts={
                "Xi'_c0": _daughterXiCut,
                "pi-": _daughterpiCut,
            },
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        Xicstar_exclfromB = self.makeXicstarz2pKKpi_exclfromB()
        _stdPion = DataOnDemand(Location="Phys/StdNoPIDsPions/Particles")

        return Selection(
            "SelXib2XicstarPi_pKKpi"+self.name,
            Algorithm=_Xib,
            RequiredSelections=[Xicstar_exclfromB, _stdPion])

    def makeXicstarz2pKKpi_exclfromB_line(self):

        Xicstar_exclfromB = self.makeXib2XicstarPi_pKKpi()
        Xicstar_exclfromBLine = StrippingLine(self.name + "Xibm2XicstarzPi_pKKpi" + "Line",
                                              algos=[Xicstar_exclfromB])
        self.registerLine(Xicstar_exclfromBLine)

    def makeXicstarz2XiPi_inclfromB(self):
        """
        Create a Xic(*)0 -> Xi- pi+ (inclusive detached)
        """
        _daughterXiCut = "(PT > %(HyperonPT)s)" % self.config
        _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s) & (TRGHOSTPROB < 0.5)" % self.config
        _combinationCut = "(ASUM(PT)> %(FromB_PT)s*MeV) & (AM> %(XicMassMinComb)s*MeV) & (AM< %(XicMassMaxComb)s*MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % self.config
        _motherCut = "(M> %(XicMassMin)s*MeV) & (M< %(XicMassMax)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxFromB_VertChi2DOF)s) & (BPVVDCHI2>%(FromB_BPVVDCHI2)s) & (BPVDIRA>%(FromB_DIRA)s)" % self.config
        _Xic = CombineParticles(
            DecayDescriptor="[Xi'_c0 -> Xi- pi+]cc",
            DaughtersCuts={
                "Xi-": _daughterXiCut,
                "pi+": _daughterpiCut,
            },
            CombinationCut=_combinationCut,
            MotherCut=_motherCut)

        _stdANNPion = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")

        return Selection(
            "SelXicstar2Xipi_inclfromB"+self.name,
            Algorithm=_Xic,
            RequiredSelections=[self.XiminusList, _stdANNPion])

    def makeXicstarz2XiPi_inclfromB_line(self):

        Xicstar_inclfromB = self.makeXicstarz2XiPi_inclfromB()
        Xicstar_inclfromBLine = StrippingLine(self.name + "Xicstarz2XiPi_inclfromB" + "Line",
                                              algos=[Xicstar_inclfromB])
        self.registerLine(Xicstar_inclfromBLine)

    def makeBcstar2JpsiDs_line(self):
        """
        Create a Bc(*)- -> J/psi Ds (prompt)
        """
        Bcstar2JpsiDs = self.createCombinationSel(OutputList="Bcstar2JpsiDs" + self.name,
                                                  DecayDescriptor="[B_c*- -> J/psi(1S) D_s-]cc",
                                                  DaughterLists=[
                                                                  self.JpsiList, self.DsplusList],
                                                  PreVertexCuts="(ADAMASS('B_c*-') <600*MeV )",
                                                  PostVertexCuts="(VFASPF(VCHI2/VDOF)<25)  &  (ADMASS('B_c*-') <%(BHadronMassWindow)s *MeV)" % self.config)
        Bcstar2JpsiDsLine = StrippingLine(self.name + "Bcstar2JpsiDs" + "Line",
                                          algos=[Bcstar2JpsiDs])
        self.registerLine(Bcstar2JpsiDsLine)
