###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""
Lines for searches on DM candidates produced from Lambda decays (arXiv:2101.02706).
"""

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from CommonParticles.Utils import *
from CommonParticles import StdAllNoPIDsPions
from StandardParticles import StdAllLooseKaons, StdAllLoosePions, StdAllLooseProtons, StdLooseJpsi2MuMu

from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, millimeter

__author__ = ['Xabier Cid Vidal']
# inspired by StrippingLambda2DM, from QEE
__date__ = '19/7/2023'
__version__ = '$Revision: 2.0 $'
__all__ = 'SexaquarkConf', 'default_config'

'''
b-> p Ds+ [Xib0**,Lambdab*]
b-> p D0b [Xib-***]
b-> p D0 [To pick up * control mode]
b-> p J/psi [Xib-***]
b-> p K+ [Xib0**,lambdab*]
b-> lambdac(2595) [Xib-***], this is available in StrippingLambda2DM, from QEE
* lambdab-> p D0 pi 10%
** Xib0 -> pD0K 30%, times hadr fraction
*** Xib- -> pKK, 20%, times hadr fraction
## norm lines can be obtained offline, going to full dst! Need control lines to avoid isolation
'''

default_config = {
    'NAME': 'Sexaquark',
    'BUILDERTYPE': 'SexaquarkConf',
    'WGs': ['BandQ'],
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'Common': {'checkPV': False,
                   'KfromD_ProbNNghost': 0.1,
                   'KfromD_ProbNNk': 0.8,
                   'KfromD_PT': 350*MeV,
                   'pifromDProbNNghost': 0.1,
                   'pifromDPT': 350*MeV,
                   'pifromDProbNNpi': 0.8,
                   'D_DOCA': 0.3*millimeter,
                   'D_FDCHI2': 50,
                   'D_M_Min': 1830*MeV,
                   'D_M_Max': 2000*MeV,
                   'D_PT': 2500*MeV,
                   'D_VCHI2': 6,
                   'p_IPchi2': 20,
                   'p_ProbNNghost': 0.2,
                   'p_ProbNNp': 0.8,
                   'p_PT': 600*MeV,
                   'Jpsi_IPchi2': 5,
                   'Jpsi_PT': 100*MeV,
                   'bBaryonToDandp_DOCA': 0.2*millimeter,
                   'bBaryonToDandp_DOCACHI2': 7,
                   'bBaryonToDandp_FDCHI2': 30,
                   'bBaryonToDandp_ISO': 0.0,
                   'bBaryonToDandp_M_Min': 0*MeV,
                   'bBaryonToDandp_M_Max': 5700*MeV,
                   'bBaryonToDandp_PT': 1000*MeV,
                   'bBaryonToDandp_VCHI2': 8,
                   'bBaryonToDandp_IPCHI2': 0,
                   ####
                   'bBaryonToJpsiandp_DOCA': 1*millimeter,
                   'bBaryonToJpsiandp_DOCACHI2': 10,
                   'bBaryonToJpsiandp_FDCHI2': 5,
                   'bBaryonToJpsiandp_ISO': 0.0,
                   'bBaryonToJpsiandp_M_Min': 0*MeV,
                   'bBaryonToJpsiandp_M_Max': 5700*MeV,
                   'bBaryonToJpsiandp_PT': 600*MeV,
                   'bBaryonToJpsiandp_VCHI2': 10,
                   'bBaryonToJpsiandp_IPCHI2': 0},

        'StrippingSQbBaryonToDpAndpLine': {'Prescale': 1.0,
                                           'Postscale': 1.0,
                                           },
        'StrippingSQbBaryonToDzAndpLine': {'Prescale': 1.0,
                                           'Postscale': 1.0,
                                           },
        'StrippingSQbBaryonToDzbAndpLine': {'Prescale': 1.0,
                                           'Postscale': 1.0,
                                           },
        'StrippingSQbBaryonToDsAndpLine': {'Prescale': 1.0,
                                           'Postscale': 1.0,
                                           },
        # tighter cuts are required here...
        'StrippingSQbBaryonToKAndpLine': {'K_IPchi2': 30,
                                          'K_ProbNNghost': 0.2,
                                          'K_ProbNNk': 0.8,
                                          'K_PT': 1500*MeV,
                                          'p_IPchi2': 30,
                                          'p_ProbNNghost': 0.2,
                                          'p_ProbNNp': 0.8,
                                          'p_PT': 1500*MeV,
                                          'bBaryonToKandp_DOCA': 0.2*millimeter,
                                          'bBaryonToKandp_DOCACHI2': 3,
                                          'bBaryonToKandp_FDCHI2': 50,
                                          'bBaryonToKandp_ISO': 0.7,
                                          'bBaryonToKandp_M_Min': 1000*MeV,
                                          'bBaryonToKandp_M_Max': 5700*MeV,
                                          'bBaryonToKandp_PT': 4000*MeV,
                                          'bBaryonToKandp_VCHI2': 5,
                                          'bBaryonToKandp_IPCHI2': 5,
                                          'bBaryonToKandp_CORRM': 2800*MeV,
                                          ####
                                          'Prescale': 1.0,
                                          'Postscale': 1.0,
                                          'Prescale_control': 0.15,
                                          'Postscale_control': 1.0,
                                          },
        'StrippingSQbBaryonToJpsiAndpLine': {'Prescale': 1.0,
                                             'Postscale': 1.0,
                                             },
    }
}


class SexaquarkConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        # here register p+D lines
        mynames = ["Dp", "Dzb", "Ds"]
        # and here p+Jpsi
        mynames.append("Jpsi")
        sels, lines = [], []
        for i1, myname in enumerate(mynames):
            mynamep = name+myname
            if "D" in myname:
                sels.append(self.combinebBaryonToDandp(mynamep, config['Common'],
                                                       whichD=i1+1,
                                                       isolation=False))
            elif "Jpsi" in myname:
                sels.append(self.combinebBaryonToJpsiandp(mynamep, config['Common'],
                                                          isolation=False))

            lines.append(StrippingLine(name + 'bBaryonTo'+myname+'AndpLine',
                                       prescale=config['StrippingSQbBaryonTo' +
                                                       myname+'AndpLine']['Prescale'],
                                       postscale=config['StrippingSQbBaryonTo' +
                                                        myname+'AndpLine']['Postscale'],
                                       checkPV=config['Common']['checkPV'],
                                       selection=sels[-1]))

            self.registerLine(lines[-1])

        # register Lb->Dzp line here
        myname = "Dz"
        sels.append(self.combinebBaryonToDandpcontrol(name+myname, config['Common'],isolation=False))
        lines.append(StrippingLine(name+'bBaryonTo'+myname+'AndpLine',
                                    prescale=config['StrippingSQbBaryonTo' +
                                                    myname+'AndpLine']['Prescale'],
                                    postscale=config['StrippingSQbBaryonTo' +
                                                        myname+'AndpLine']['Postscale'],
                                    checkPV=config['Common']['checkPV'],
                                    selection=sels[-1]))
        self.registerLine(lines[-1])

        myname = "K"
        # now write Kp lines: loop for control lines (no isolation)
        for issig in range(2):
            if issig:
                n1, n2 = "", ""
            else:
                n1, n2 = "Control", "_control"
            mynamep = name+myname+n2
            sels.append(self.combinebBaryonToKandp(mynamep, config['StrippingSQbBaryonTo'+myname+'AndpLine'],
                                                   issig=issig))

            lines.append(StrippingLine(name + 'bBaryonTo'+myname+'Andp'+n1+'Line',
                                       prescale=config['StrippingSQbBaryonTo' +
                                                       myname+'AndpLine']['Prescale'+n2],
                                       postscale=config['StrippingSQbBaryonTo' +
                                                        myname+'AndpLine']['Postscale'+n2],
                                       checkPV=config['Common']['checkPV'],
                                       selection=sels[-1]))
            self.registerLine(lines[-1])

    def combineD(self, name, config, whichD):
        # whichD = 1, is D+
        # whichD = 2, is D0
        # whichD = 3, is Ds

        daugh_cut = {'K': ("(PT > {KfromD_PT} )"
                           "& (TRGHOSTPROB < {KfromD_ProbNNghost})"
                           "& (PROBNNK > {KfromD_ProbNNk})").format(**config),
                     'pi': ("(PT > {pifromDPT} )"
                            "& (TRGHOSTPROB < {pifromDProbNNghost})"
                            "& (PROBNNpi > {pifromDProbNNpi})").format(**config)}

        comb_cut = ("(APT > {D_PT}) &"
                    "(AMAXDOCA('') < {D_DOCA})".format(**config))

        mother_cut = ("(PT > {D_PT} ) &"
                      "(VFASPF(VCHI2PDOF) < {D_VCHI2}) &"
                      "(BPVVDCHI2 > {D_FDCHI2})".format(**config))

        # this mass cut covers the three resonances, but can be made finer if needed
        comb_cut += "& (AM > {D_M_Min}) & (AM < {D_M_Max}) ".format(**config)
        mother_cut += "& ( M > {D_M_Min}) & (M < {D_M_Max}) ".format(**config)

        namesel = "Sel"+name
        if whichD == 1:
            decay = "[D- -> K+ pi- pi-]cc"
        elif whichD == 2:
            decay = "[D0 -> K- pi+]cc"
        elif whichD == 3:
            decay = "[D_s- -> K+ K- pi-]cc"

        _combination = CombineParticles(DecayDescriptor=decay,
                                        CombinationCut=comb_cut,
                                        DaughtersCuts={'K+': daugh_cut["K"],
                                                       'pi-': daugh_cut["pi"]},
                                        MotherCut=mother_cut
                                        )
        return Selection(namesel,
                         Algorithm=_combination,
                         RequiredSelections=[
                             StdAllLooseKaons, StdAllLoosePions]
                         )

    def combinebBaryonToDandp(self, name, config, whichD, isolation=True):
        # whichD = 1, is D+
        # whichD = 2, is D0
        # whichD = 3, is Ds

        SelD = self.combineD("Combine"+name, config, whichD)
        if whichD == 1:
            decay = "[Lambda_b0 -> D+ p~-]cc"
        elif whichD == 2:
            decay = "[Lambda_b0 -> D0 p~-]cc"
        elif whichD == 3:
            decay = "[Lambda_b0 -> D_s+ p~-]cc"

        daugh_cut = {'p': ("(PT > {p_PT} )"
                           "& (MIPCHI2DV(PRIMARY) > {p_IPchi2})"
                           "& (TRGHOSTPROB < {p_ProbNNghost})"
                           "& (PROBNNp > {p_ProbNNp})").format(**config)}

        comb_cut = ("(APT > {bBaryonToDandp_PT} ) &"
                    "(AM > {bBaryonToDandp_M_Min}) & (AM < {bBaryonToDandp_M_Max}) &"
                    "(AMAXDOCA('') < {bBaryonToDandp_DOCA}) &"
                    "(ACUTDOCACHI2({bBaryonToDandp_DOCACHI2},''))".format(**config))

        mother_cut = ("(PT > {bBaryonToDandp_PT} ) &"
                      "( M > {bBaryonToDandp_M_Min}) & (M < {bBaryonToDandp_M_Max}) &"
                      " (VFASPF(VCHI2PDOF) < {bBaryonToDandp_VCHI2}) & "
                      " (BPVVDCHI2 > {bBaryonToDandp_FDCHI2}) & "
                      " (BPVIPCHI2() > {bBaryonToDandp_IPCHI2}) ".format(**config))

        if isolation:
            mother_cut += "& ((PT/(PT+PTCONE)) > {bBaryonToDandp_ISO})".format(
                **config)

        bBaryonToDandp = CombineParticles(DecayDescriptor=decay,
                                          CombinationCut=comb_cut,
                                          DaughtersCuts={'p~-': daugh_cut["p"]},
                                          MotherCut=mother_cut,
                                          Preambulo=[
                                              "PTCONE  = SUMCONE (   0.6**2 , PT , '/Event/Phys/StdAllNoPIDsPions/Particles')"]
                                          )

        return Selection("SelbBaryonToDandp"+name,
                         Algorithm=bBaryonToDandp,
                         RequiredSelections=[SelD, StdAllLooseProtons]
                         )
    
    def combinebBaryonToDandpcontrol(self, name, config, isolation=True):
        SelD = self.combineD("Combine"+name, config, whichD=2)
        decay = "[Lambda_b0 -> D0 p+]cc"

        daugh_cut = {'p': ("(PT > {p_PT} )"
                           "& (MIPCHI2DV(PRIMARY) > {p_IPchi2})"
                           "& (TRGHOSTPROB < {p_ProbNNghost})"
                           "& (PROBNNp > {p_ProbNNp})").format(**config)}

        comb_cut = ("(APT > {bBaryonToDandp_PT} ) &"
                    "(AM > {bBaryonToDandp_M_Min}) & (AM < {bBaryonToDandp_M_Max}) &"
                    "(AMAXDOCA('') < {bBaryonToDandp_DOCA}) &"
                    "(ACUTDOCACHI2({bBaryonToDandp_DOCACHI2},''))".format(**config))

        mother_cut = ("(PT > {bBaryonToDandp_PT} ) &"
                      "( M > {bBaryonToDandp_M_Min}) & (M < {bBaryonToDandp_M_Max}) &"
                      " (VFASPF(VCHI2PDOF) < {bBaryonToDandp_VCHI2}) & "
                      " (BPVVDCHI2 > {bBaryonToDandp_FDCHI2}) & "
                      " (BPVIPCHI2() > {bBaryonToDandp_IPCHI2}) ".format(**config))
        
        if isolation:
            mother_cut += "& ((PT/(PT+PTCONE)) > {bBaryonToDandp_ISO})".format(
                **config)
            
        bBaryonToDandp = CombineParticles(DecayDescriptor=decay,
                                          CombinationCut=comb_cut,
                                          DaughtersCuts={'p+': daugh_cut["p"]},
                                          MotherCut=mother_cut,
                                          Preambulo=[
                                              "PTCONE  = SUMCONE (   0.6**2 , PT , '/Event/Phys/StdAllNoPIDsPions/Particles')"]
                                          )
        return Selection("SelbBaryonToDandp"+name,
                         Algorithm=bBaryonToDandp,
                         RequiredSelections=[SelD, StdAllLooseProtons]
                         )

    def combinebBaryonToKandp(self, name, config, issig=True):

        daugh_cut = {'p~-': ("(PT > {p_PT} )"
                            "& (MIPCHI2DV(PRIMARY) > {p_IPchi2})"
                            "& (TRGHOSTPROB < {p_ProbNNghost})"
                            "& (PROBNNp > {p_ProbNNp})").format(**config),
                     'K+': ("(PT > {K_PT} )"
                            "& (MIPCHI2DV(PRIMARY) > {K_IPchi2})"
                            "& (TRGHOSTPROB < {K_ProbNNghost})"
                            "& (PROBNNk > {K_ProbNNk})").format(**config)}

        comb_cut = ("(APT > {bBaryonToKandp_PT} ) &"
                    "(AM > {bBaryonToKandp_M_Min}) & (AM < {bBaryonToKandp_M_Max}) &"
                    "(AMAXDOCA('') < {bBaryonToKandp_DOCA}) &"
                    "(ACUTDOCACHI2({bBaryonToKandp_DOCACHI2},''))".format(**config))

        mother_cut = ("(PT > {bBaryonToKandp_PT} ) &"
                      "( M > {bBaryonToKandp_M_Min}) & (M < {bBaryonToKandp_M_Max}) &"
                      " (VFASPF(VCHI2PDOF) < {bBaryonToKandp_VCHI2}) & "
                      " (BPVVDCHI2 > {bBaryonToKandp_FDCHI2}) & "
                      " (BPVIPCHI2() > {bBaryonToKandp_IPCHI2}) ".format(**config))

        # apply cut on corrected mass and isolation
        if issig:
            mother_cut += "& ((BPVCORRM > {bBaryonToKandp_CORRM}))".format(**config)
            mother_cut += "& ((PT/(PT+PTCONE)) > {bBaryonToKandp_ISO})".format(
                **config)

        bBaryonToKandp = CombineParticles(DecayDescriptor="[Lambda_b0 -> K+ p~-]cc",
                                          CombinationCut=comb_cut,
                                          DaughtersCuts=daugh_cut,
                                          MotherCut=mother_cut,
                                          Preambulo=[
                                              "PTCONE  = SUMCONE (   0.6**2 , PT , '/Event/Phys/StdAllNoPIDsPions/Particles')"]
                                          )

        return Selection("SelbBaryonToKandp"+name,
                         Algorithm=bBaryonToKandp,
                         RequiredSelections=[
                             StdAllLooseProtons, StdAllLooseKaons]
                         )

    def combinebBaryonToJpsiandp(self, name, config, isolation=True):

        daugh_cut = {'p~-': ("(PT > {p_PT} )"
                            "& (MIPCHI2DV(PRIMARY) > {p_IPchi2})"
                            "& (TRGHOSTPROB < {p_ProbNNghost})"
                            "& (PROBNNp > {p_ProbNNp})").format(**config),
                     'J/psi(1S)': ("(PT > {Jpsi_PT} )"
                                   "& (MIPCHI2DV(PRIMARY) > {Jpsi_IPchi2})").format(**config)}

        comb_cut = ("(APT > {bBaryonToJpsiandp_PT} ) &"
                    "(AM > {bBaryonToJpsiandp_M_Min}) & (AM < {bBaryonToJpsiandp_M_Max}) &"
                    "(AMAXDOCA('') < {bBaryonToJpsiandp_DOCA}) &"
                    "(ACUTDOCACHI2({bBaryonToJpsiandp_DOCACHI2},''))".format(**config))

        mother_cut = ("(PT > {bBaryonToJpsiandp_PT} ) &"
                      "( M > {bBaryonToJpsiandp_M_Min}) & (M < {bBaryonToJpsiandp_M_Max}) &"
                      " (VFASPF(VCHI2PDOF) < {bBaryonToJpsiandp_VCHI2}) & "
                      " (BPVVDCHI2 > {bBaryonToJpsiandp_FDCHI2}) & "
                      " (BPVIPCHI2() > {bBaryonToJpsiandp_IPCHI2}) ".format(**config))

        if isolation:
            mother_cut += "& ((PT/(PT+PTCONE)) > {bBaryonToJpsiandp_ISO})".format(
                **config)

        bBaryonToJpsiandp = CombineParticles(DecayDescriptor="[Lambda_b0 -> J/psi(1S) p~-]cc",
                                             CombinationCut=comb_cut,
                                             DaughtersCuts=daugh_cut,
                                             MotherCut=mother_cut,
                                             Preambulo=[
                                                 "PTCONE  = SUMCONE (   0.6**2 , PT , '/Event/Phys/StdAllNoPIDsPions/Particles')"]
                                             )

        return Selection("SelbBaryonToJpsiandp"+name,
                         Algorithm=bBaryonToJpsiandp,
                         RequiredSelections=[
                             StdAllLooseProtons, StdLooseJpsi2MuMu]
                         )
