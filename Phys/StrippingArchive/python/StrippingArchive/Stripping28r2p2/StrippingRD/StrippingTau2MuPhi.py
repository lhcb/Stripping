###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This line implement the stripping selections looking for tau -> mu phi decay.
"""

__author__ = ['Domenico Riccardi']
__date__ = '04/07/2023'
__version__ = '$Revision: 0.2 $'

__all__ = ('Tau2MuPhiLine',
           'makeTau2MuPhi',
           'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.PhysicalConstants import c_light
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from copy import deepcopy

related_info_tools_Tau2PhiMu = [{'Type': 'RelInfoConeVariables',
                                 'ConeAngle': 0.5,
                                 'Variables': ['CONEANGLE', 'CONEMULT', 'CONEPT', 'CONEPTASYM'],
                                 'Location': 'coneInfoTau05',  # For the tau
                                 'DaughterLocations': {
                                     '[tau+ -> ^(phi(1020)->K+ K-) mu+]CC': 'coneInfoPhi05',
                                     '[tau+ -> (phi(1020)->K+ K-) ^mu+]CC': 'coneInfoMu05',
                                     '[tau+ -> (phi(1020)->^K+ K-) mu+]CC': 'coneInfoKplus05',
                                     '[tau+ -> (phi(1020)->K+ ^K-) mu+]CC': 'coneInfoKminus05'
                                 }},

                                {'Type': 'RelInfoConeVariables',
                                 'ConeAngle': 0.8,
                                 'Variables': ['CONEANGLE', 'CONEMULT', 'CONEPT', 'CONEPTASYM'],
                                 'Location': 'coneInfoTau08',  # For the tau
                                 'DaughterLocations': {
                                     '[tau+ -> ^(phi(1020)->K+ K-) mu+]CC': 'coneInfoPhi08',
                                     '[tau+ -> (phi(1020)->K+ K-) ^mu+]CC': 'coneInfoMu08',
                                     '[tau+ -> (phi(1020)->^K+ K-) mu+]CC': 'coneInfoKplus08',
                                     '[tau+ -> (phi(1020)->K+ ^K-) mu+]CC': 'coneInfoKminus08'
                                 }},
                                {'Type': 'RelInfoConeVariables',
                                 'ConeAngle': 1.0,
                                 'Variables': ['CONEANGLE', 'CONEMULT', 'CONEPT', 'CONEPTASYM'],
                                 'Location': 'coneInfoTau10',  # For the tau
                                 'DaughterLocations': {
                                     '[tau+ -> ^(phi(1020)->K+ K-) mu+]CC': 'coneInfoPhi10',
                                     '[tau+ -> (phi(1020)->K+ K-) ^mu+]CC': 'coneInfoMu10',
                                     '[tau+ -> (phi(1020)->^K+ K-) mu+]CC': 'coneInfoKplus10',
                                     '[tau+ -> (phi(1020)->K+ ^K-) mu+]CC': 'coneInfoKminus10'
                                 }},

                                {'Type': 'RelInfoConeVariables',
                                 'ConeAngle': 1.2,
                                 'Variables': ['CONEANGLE', 'CONEMULT', 'CONEPT', 'CONEPTASYM'],
                                 'Location': 'coneInfoTau12',  # For the tau
                                 'DaughterLocations': {
                                     '[tau+ -> ^(phi(1020)->K+ K-) mu+]CC': 'coneInfoPhi12',
                                     '[tau+ -> (phi(1020)->K+ K-) ^mu+]CC': 'coneInfoMu12',
                                     '[tau+ -> (phi(1020)->^K+ K-) mu+]CC': 'coneInfoKplus12',
                                     '[tau+ -> (phi(1020)->K+ ^K-) mu+]CC': 'coneInfoKminus12'
                                 }},

                                {'Type': 'RelInfoVertexIsolation',
                                 'Location': 'VtxIsoInfo',
                                 'Variables': ['VTXISONUMVTX',
                                               'VTXISODCHI2ONETRACK', 'VTXISODCHI2MASSONETRACK',
                                               'VTXISODCHI2TWOTRACK', 'VTXISODCHI2MASSTWOTRACK'], },
                                {'Type': 'RelInfoTrackIsolationBDT',
                                 'DaughterLocations': {
                                     '[tau+ -> (phi(1020)->K+ K-) ^mu+]CC': 'MuonTrackIsoBDTInfo',
                                     '[tau+ -> (phi(1020)->^K+ K-) mu+]CC': 'KplusTrackIsoBDTInfo',
                                     '[tau+ -> (phi(1020)->K+ ^K-) mu+]CC': 'KminusTrackIsoBDTInfo'
                                 }}
                                ]

default_config = {
    'NAME': 'Tau2MuPhi',
    'WGs': ['RD'],
    'BUILDERTYPE': 'Tau2MuPhiLine',
    'CONFIG': {'Postscale': 1,
               'TauPrescale': 1,
               'RelatedInfoTools_Tau2PhiMu': related_info_tools_Tau2PhiMu
               },  # matches 'CONFIG'
    'STREAMS': ['Leptonic']
}


class Tau2MuPhiLine(LineBuilder):
    """Class defining the tau -> mu phi stripping lines"""

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        # make the various stripping selections
        self.selTau2PhiMu = makeTau2MuPhi(name)

        self.tau2PhiMuLine = StrippingLine(name + 'Line',
                                           prescale=config['TauPrescale'],
                                           postscale=config['Postscale'],
                                           MDSTFlag=False,
                                           selection=self.selTau2PhiMu,
                                           RelatedInfoTools=config['RelatedInfoTools_Tau2PhiMu'],
                                           )

        self.registerLine(self.tau2PhiMuLine)


def makeTau2MuPhi(name):
    Tau2PhiMu = CombineParticles()
    Tau2PhiMu.DecayDescriptor = " [ tau+ -> phi(1020) mu+ ]cc"

    makePhi = CombineParticles()
    makePhi.DecayDescriptor = "phi(1020) -> K+ K-"
    makePhi.DaughtersCuts = {
        "K+": "(ISLONG) & (TRCHI2DOF < 3 ) & (TRGHOSTPROB<0.3) & (PT>300*MeV) & (PIDK > 0) & ( BPVIPCHI2() >  9 )",
        "K-": "(ISLONG) & (TRCHI2DOF < 3 ) & (TRGHOSTPROB<0.3) & (PT>300*MeV) & (PIDK > 0) & ( BPVIPCHI2() >  9 )"}

    _kaons = DataOnDemand(Location='Phys/StdLooseKaons/Particles')

    makePhi.CombinationCut = "(ADAMASS('phi(1020)')<30*MeV)"
    makePhi.MotherCut = " ( VFASPF(VCHI2) < 25 ) & (MIPCHI2DV(PRIMARY)> 9)"

    SelPhi = Selection(name + "SelPhi",
                       Algorithm=makePhi,
                       RequiredSelections=[_kaons])

    Tau2PhiMu.DaughtersCuts = { "mu-": " ( PT > 300 * MeV ) & ( TRCHI2DOF < 3 ) & (TRGHOSTPROB<0.3) & ( BPVIPCHI2 () >  9 )"}
    # Tau2PhiMu.CombinationCut = "(ADAMASS('tau-')<150*MeV)" # old mass window
    # Tau2PhiMu.CombinationCut = "(AM > 1626.86 * MeV) & (AM < 1926.86 * MeV)"
    Tau2PhiMu.CombinationCut = "(AM > 1627 * MeV) & (AM < 2020 * MeV)"

    Tau2PhiMu.MotherCut = "( VFASPF(VCHI2) < 25 ) &  ( (BPVLTIME() * c_light)   > 50 * micrometer ) &  ( BPVIPCHI2() < 100 ) "

    _stdLooseMuons = DataOnDemand(Location="Phys/StdLooseMuons/Particles")

    SelTau = Selection(name + "makeTau",
                       Algorithm=Tau2PhiMu,
                       RequiredSelections=[SelPhi, _stdLooseMuons])

    return SelTau
