###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__  = 'Patrick Koppenburg, Alex Shires, Thomas Blake, Luca Pescatore, Simone Bifani, Yasmine Amhis, Paula Alvarez Cartelle, Harry Cliff, Rafael Silva Coutinho, Guido Andreassi, Mick Mulder, Maarten van Veghel, John Smeaton, Vitalii Lisovskyi, Biplab Dey, Dan Thompson, Christoph Langenbruch, Renato Quagliani'
__date__    = '18/07/2023'
__version__ = '$Revision: 8 $'

__all__ = ( 'Bu2LLKConf', 'default_config' )

"""
  B --> ll K selections for (SS and OS leptons):
  B --s> ee K  versus  B --> mumu K
  B --s> ee pi  versus  B --> mumu pi
  B --> ee K*  versus  B --> mumu K*
  B --> ee K1  versus  B --> mumu K1
  B --> ee K2  versus  B --> mumu K2
  B --> ee phi  versus  B --> mumu phi
  B --> ee Kpipipi  versus B --> mumu Kpipipi
  Lb --> ee Lambda  versus  Lb --> mumu Lambda
  Lb --> ee p K  versus  Lb --> mumu p K (SS, OS hadrons)
  Lb --> ee p pi  versus  Lb --> mumu p pi (SS, OS hadrons)
  B --> ee K pi  versus  B --> mumu K pi (SS, OS hadrons)
  B --> ee pi pi  versus  B --> mumu pi pi (SS, OS hadrons)
  Bs --> ee K K  versus  Bs --> mumu K K (SS, OS hadrons)
  B --> gamma K* , B --> gamma phi  and  Lb --> gamma Lambda(*)  with converted photons
  B --> emu K(+/0), B--> emu K*(0/+) and B--> emu Phi, Lb--> Lambda(*) emu, Lb--> Lambda(*) taumu with OS and SS leptons
"""

# Locations of neutral particles for related info (these only work with ConeIsolation?)
daughter_neutral_locations = {
    # Locations of pi0 in K*+ -> K+ pi0, for opposite- and same-sign leptons.
    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l-)]CC" : "{0}pi0H2",
    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l+)]CC" : "{0}pi0H2",
    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l-  l-)]CC" : "{0}pi0H2",
}

daughter_locations = {
    # OPPOSITE SIGN
    # 3-body
    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC" : "{0}H",
    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC" : "{0}H3",
    # 5-body
    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC" : "{0}H3",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}HH",
    # 4-body with a strange particle in the final state
    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body with two pions in the final state
    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body with p and pi in the final state. Here the names are kept generic (H1,H2,L1,L2), for uniformity with strippingof other years. This is needed for Lb->Lll analyses.
    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body decays involving K*(892)+, with pi0 in final state. (Note: had some trouble with generic X+ in the 3-body descriptors matching the K*(892)+, and getting junk output).
    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l-)]CC" : "{0}pi0H1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l-)]CC" : "{0}pi0L1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l-)]CC" : "{0}pi0L2",
    # 6-body decays with at least one kaon on the final state (quasi 5-body)
    "[Beauty -> ^K+ X- X+ X- (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty -> K+ ^X- X+ X- (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty -> K+ X- ^X+ X- (X0 ->  l+  l-)]CC" : "{0}H3",
    "[Beauty -> K+ X- X+ ^X- (X0 ->  l+  l-)]CC" : "{0}H4",
    "[Beauty -> K+ X- X+ X- (X0 ->  ^l+  l-)]CC" : "{0}L1",
    "[Beauty -> K+ X- X+ X- (X0 ->  l+  ^l-)]CC" : "{0}L2",
    # 7-body (quasi 4-body)
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC" : "{0}L21",
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC" : "{0}L22",
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC" : "{0}L23",

    # SAME SIGN
    # 3-body
    "[Beauty -> ^X+  (X ->  l+  l+)]CC" : "{0}H",
    "[Beauty ->  X+  (X -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  X+  (X ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty ->  X+ ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty -> ^X+  (X ->  l-  l-)]CC" : "{0}H",
    "[Beauty ->  X+  (X -> ^l-  l-)]CC" : "{0}L1",
    "[Beauty ->  X+  (X ->  l- ^l-)]CC" : "{0}L2",
    "[Beauty ->  X+ ^(X ->  l-  l-)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l+  l+)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l+  l+)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l+  l+)]CC" : "{0}H3",
    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l-  l-)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l-  l-)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l-  l-)]CC" : "{0}H3",
    # 5-body
    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l+  l+)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l+  l+)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l+  l+)]CC" : "{0}H3",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l-  l-)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l-  l-)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l-  l-)]CC" : "{0}H3",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l-  l-)]CC" : "{0}L1",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l- ^l-)]CC" : "{0}L2",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l-  l-)]CC" : "{0}HH",
    # 4-body with a strange particle in the final state
    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l+  l+)]CC" : "{0}H1",
    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l-  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l+  l+)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l-  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l-  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l- ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l-  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l-  l-)]CC" : "{0}LL",
    # 4-body with two pions in the final state
    "[Beauty ->  (X0 -> ^pi+  pi-)  (X ->  l+  l+)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X ->  l+  l+)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  pi+  pi-)  (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  pi+  pi-) ^(X ->  l+  l+)]CC" : "{0}LL",
    # 4-body with p and pi in the final state. Here the names are kept generic (H1,H2,L1,L2), for uniformity with strippingof other years. This is needed for Lb->Lll analyses.
    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l+  l+)]CC" : "{0}H1",
    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l-  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l+  l+)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l-  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l-  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l- ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l-  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l-  l-)]CC" : "{0}LL",
    # 4-body decays involving K*(892)+, with pi0 in final state. (Note: had some trouble with generic X+ in the 3-body descriptors matching the K*(892)+, and getting junk output).
    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l+)]CC" : "{0}pi0H1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l+)]CC" : "{0}pi0L1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l+)]CC" : "{0}pi0L2",
    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l-  l-)]CC" : "{0}pi0H1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l-  l-)]CC" : "{0}pi0L1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l-  ^l-)]CC" : "{0}pi0L2",
    # 7-body (quasi 4-body)
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ -> ^X+  X-  X+))]CC" : "{0}L21",
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+ ^X-  X+))]CC" : "{0}L22",
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+  X- ^X+))]CC" : "{0}L23",
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- -> ^X-  X-  X+))]CC" : "{0}L21",
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X- ^X-  X+))]CC" : "{0}L22",
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X-  X- ^X+))]CC" : "{0}L23"
}

daughter_vtx_locations = {
    # OPPOSITE SIGN
    # 3-body
    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> ^(X+ -> X+  X+  X-) (X0 ->  l+  l-)]CC" : "{0}H",
    # 5-body
    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}H",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}HH",
    # 4-body
    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body with neutral in final state
    "[Beauty -> ^(X+ -> X+  X0) (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty -> (X+ -> X+  X0) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 6-body decays with at least one kaon on the final state (quasi 5-body)
    "[Beauty -> K+ X- X+ X- ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 7-body (quasi 4-body)
    "[Beauty ->  X  (X0 ->  l+  ^(l- -> X-  X-  X+))]CC" : "{0}L",

    # SAME SIGN
    # 3-body
    "[Beauty ->  X+ ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty ->  X+ ^(X ->  l-  l-)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> ^(X+ -> X+  X+  X-) (X ->  l+  l+)]CC" : "{0}H",
    "[Beauty -> ^(X+ -> X+  X+  X-) (X ->  l-  l-)]CC" : "{0}H",
    # 5-body
    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X ->  l+  l+)]CC" : "{0}H",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X ->  l-  l-)]CC" : "{0}H",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l-  l-)]CC" : "{0}HH",
    # 4-body
    "[Beauty -> ^(X0 ->  X+  X-)  (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  X+  X-) ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty -> ^(X0 ->  X+  X-)  (X ->  l-  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  X+  X-) ^(X ->  l-  l-)]CC" : "{0}LL",
    # 4-body with neutral in final state
    "[Beauty -> ^(X+ -> X+  X0) (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty -> (X+ -> X+  X0) ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty -> ^(X+ -> X+  X0) (X ->  l-  l-)]CC" : "{0}HH",
    "[Beauty -> (X+ -> X+  X0) ^(X ->  l-  l-)]CC" : "{0}LL",
    # 7-body (quasi 4-body)
    "[Beauty ->  X  (X ->  l+  ^(l+ -> X+  X-  X+))]CC" : "{0}L",
    "[Beauty ->  X  (X ->  l-  ^(l- -> X-  X-  X+))]CC" : "{0}L",
}


default_config = {
    'NAME'                       : 'Bu2LLK',
    'BUILDERTYPE'                : 'Bu2LLKConf',
    'CONFIG'                     :
        {
        'BFlightCHI2'            : 100
        , 'BDIRA'                : 0.9995
        , 'BIPCHI2'              : 25
        , 'BVertexCHI2'          : 9
        , 'DiLeptonPT'           : 0
        , 'DiLeptonFDCHI2'       : 16
        , 'DiLeptonIPCHI2'       : 0
        , 'LeptonIPCHI2'         : 9
        , 'LeptonPT'             : 350
        , 'LeptonPTTight'        : 500
        , 'TauPT'                : 0
        , 'TauVCHI2DOF'          : 150
        , 'KaonIPCHI2'           : 9
        , 'KaonPT'               : 400
        , 'KaonPTLoose'          : 250
        , 'PionPTRho'            : 350
        , 'ProtonP'              : 2000
        , 'KstarPVertexCHI2'     : 25
        , 'KstarPMassWindow'     : 300
        , 'KstarPADOCACHI2'      : 30
        , 'Pi0PT'                : 600
        , 'ProbNNCut'            : 0.05
        , 'ProbNNCutTight'       : 0.1
        , 'DiHadronMass'         : 2600
        , 'DiHadronVtxCHI2'      : 25
        , 'DiHadronADOCACHI2'    : 30
        , 'UpperMass'            : 5500
        , 'BMassWindow'          : 1500
        , 'UpperBMass'           : 5280
        , 'UpperBsMass'          : 5367
        , 'UpperLbMass'          : 5620
        , 'BMassWindowTau'       : 5000
        , 'PIDe'                 : 0
        , 'PIDeInvLow'           : -5
        , 'RICHPIDe_Up'          : -5
        , 'Trk_Chi2'             : 3
        , 'Trk_GhostProb'        : 0.35 #0.4
        , 'K1_MassWindow_Lo'     : 0
        , 'K1_MassWindow_Hi'     : 4200
        , 'K1_VtxChi2'           : 12
        , 'K1_SumPTHad'          : 1200 #800
        , 'K1_SumIPChi2Had'      : 48.0
        , 'B2K3pill_LeptonIPCHI2': 1
        , 'B2K3pill_LeptonIPCHI2_Max' : 7
        , 'B2K3pill_LeptonPT'    : 150
        , 'B2K3pill_DiLeptonPT'  : 250
        , 'B2K3pill_DiLeptonVtxChi2': 10
        , 'B2K3pill_DiLeptonFDChi2' : 25
        , 'B2K3pill_HadIPCHI2'   : 4.5
        , 'B2K3pill_PionPT'      : 150
        , 'B2K3pill_KaonPT'      : 250
        , 'B2K3pill_achi2doca'   : 30
        , 'B2K3pill_BVtxChi2'    : 40
        , 'B2K3pill_BIPDChi2'    : 15 #harder than standard cut
        , 'B2K3pill_BFDChi2'     : 150 #harder than standard cut
        , 'B2K3pill_BFDIRA'      : 0.9999 #harder than standard cut
        , 'B2K3pill_MassWindow_Hi' : 6500
        , 'Bu2eeLinePrescale'    : 1
        , 'Bu2eeLineFullDSTPrescale' : 0.2
        , 'Bu2eeLine2Prescale'   : 1
        , 'Bu2eeLine2FullDSTPrescale' : 0.2
        , 'Bu2eeSSLine2Prescale' : 1
        , 'Bu2eeLine3Prescale'   : 1
        , 'Bu2eeLine4Prescale'   : 1
        , 'Bu2mmLinePrescale'    : 1
        , 'Bu2mmLineFullDSTPrescale' : 0.2
        , 'Bu2mmSSLinePrescale'  : 1
        , 'Bu2meLinePrescale'    : 1
        , 'Bu2meLineFullDSTPrescale' : 0.1
        , 'Bu2meSSLinePrescale'  : 1
        , 'Bu2mtLinePrescale'    : 1
        , 'Bu2mtSSLinePrescale'  : 1
        , 'B2KpipipieeLinePrescale' : 1
        , 'B2KpipipimmLinePrescale' : 1
        , 'RelatedInfoTools'     : [
            {'Type'              : 'RelInfoVertexIsolation',
             'Location'          : 'VertexIsoInfo',
             'IgnoreUnmatchedDescriptors': True,
             'DaughterLocations' : {key: val.format('VertexIsoInfo') for key, val in daughter_vtx_locations.items()}},
            {'Type'              : 'RelInfoVertexIsolationBDT',
             'Location'          : 'VertexIsoBDTInfo',
             'IgnoreUnmatchedDescriptors': True,
             'DaughterLocations' : {key: val.format('VertexIsoBDTInfo') for key, val in daughter_vtx_locations.items()}},
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 0.5,
             'IgnoreUnmatchedDescriptors': True,
             'Location' : 'TrackIsoInfo05',
             'DaughterLocations' : {key: val.format('TrackIsoInfo') for key, val in daughter_locations.items() + daughter_neutral_locations.items()}},
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'          : 0.5,
             'IgnoreUnmatchedDescriptors': True,
             'Location' : 'ConeIsoInfo05',
             'DaughterLocations' : {key: val.format('ConeIsoInfo') for key, val in daughter_locations.items() + daughter_neutral_locations.items()}},
            {'Type'              : 'RelInfoTrackIsolationBDT',
             'IgnoreUnmatchedDescriptors': True,
             # Use the BDT with 9 input variables
             # This requires that the "Variables" value is set to 2
             'Variables' : 2,
             'WeightsFile'  :  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             'Location' : 'TrackIsoBDTInfo',
             'DaughterLocations' : {key: val.format('TrackIsoBDTInfo') for key, val in daughter_locations.items()}},

            {'Type'              : 'RelInfoBs2MuMuTrackIsolations',
             'IgnoreUnmatchedDescriptors': True,
             'Location' : 'TrackIsoBs2MMInfo',
             'DaughterLocations' : {key: val.format('TrackIsoBs2MMInfo') for key, val in daughter_locations.items()}},

            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 1.0,
             'Location'          : 'TrackIsoInfo10'
            },
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 1.5,
             'Location'          : 'TrackIsoInfo15'
            },
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 2.0,
             'Location'          : 'TrackIsoInfo20'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'         : 1.0,
             'Location'          : 'ConeIsoInfo10'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'         : 1.5,
             'Location'          : 'ConeIsoInfo15'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'         : 2.0,
             'Location'          : 'ConeIsoInfo20'
            },
        ],
        },
    'WGs'     : [ 'RD' ],
    'STREAMS' : {
        'Dimuon': [
            'StrippingBu2LLK_mmLine_fulldst',
        ],
        'BhadronCompleteEvent': [
            'StrippingBu2LLK_eeLine_fulldst',
            'StrippingBu2LLK_eeLine2_fulldst',
            'StrippingBu2LLK_meLine_fulldst',
        ],
        'Leptonic': [
            'StrippingBu2LLK_eeLine',
            'StrippingBu2LLK_eeLine2',
            'StrippingBu2LLK_eeLine2_extra',
            'StrippingBu2LLK_eeInvPIDLine2',
            'StrippingBu2LLK_eeInvPIDLine2_extra',
            'StrippingBu2LLK_eeLine3',
            'StrippingBu2LLK_eeLine4',
            'StrippingBu2LLK_eeLine_extra',
            'StrippingBu2LLK_eeSSLine2',
            'StrippingBu2LLK_meLine',
            'StrippingBu2LLK_meSSLine',
            'StrippingBu2LLK_mmLine',
            'StrippingBu2LLK_mmLine_extra',
            'StrippingBu2LLK_mmSSLine',
            'StrippingBu2LLK_mtLine',
            'StrippingBu2LLK_mtSSLine',
            'StrippingBu2LLK_mm_K3piLine',
            'StrippingBu2LLK_ee_K3piLine',
            'StrippingBu2LLK_mm_Lb2pkmmLooseLine',
            'StrippingBu2LLK_me_Lb2pkmeLooseLine',
            'StrippingBu2LLK_meSS_Lb2pkmeSSLooseLine',
            'StrippingBu2LLK_ee_Lb2pkeeLooseLine',
            'StrippingBu2LLK_mm_Lb2KsppimmLooseLine',
            'StrippingBu2LLK_ee_Lb2KsppieeLooseLine',
        ]
    },
}

# This tool applies the BDT track isolation from the 2017 B->mumu analysis to the Jpsi daughters
# It is separated from the other tools, since it cannot be applied to lines containing taus
RelInfoTrackIsolationBDT2 = [
    {'Type' : 'RelInfoTrackIsolationBDT2',
     'Location' : 'TrackIsolationBDT2',
     'Particles' : [1,2]
    }
]


# For Lb->{pK,p pi Ks} ll. All six 2-body combinations 
RelInfoTrackIsolationBDT2_Lb2pKll = [
    {'Type' : 'RelInfoTrackIsolationBDT2',
     'Location' : 'TrackIsolationBDT2_12',
     'Particles' : [1,2]
    },
    {'Type' : 'RelInfoTrackIsolationBDT2',
     'Location' : 'TrackIsolationBDT2_14',
     'Particles' : [1,4]
    },
    #{'Type' : 'RelInfoTrackIsolationBDT2',
    # 'Location' : 'TrackIsolationBDT2_15',
    # 'Particles' : [1,5]
    #},
    #{'Type' : 'RelInfoTrackIsolationBDT2',
    # 'Location' : 'TrackIsolationBDT2_24',
    # 'Particles' : [2,4]
    #},
    {'Type' : 'RelInfoTrackIsolationBDT2',
     'Location' : 'TrackIsolationBDT2_25',
     'Particles' : [2,5]
    },
    {'Type' : 'RelInfoTrackIsolationBDT2',
     'Location' : 'TrackIsolationBDT2_45',
     'Particles' : [4,5]
    }
]

RelInfoTrackIsolationBDT2_Lb2Ksppill = [
    {'Type' : 'RelInfoTrackIsolationBDT2',
     'Location' : 'TrackIsolationBDT2_12',
     'Particles' : [1,2]
    },
    {'Type' : 'RelInfoTrackIsolationBDT2',
     'Location' : 'TrackIsolationBDT2_15',
     'Particles' : [1,5]
    },
    #{'Type' : 'RelInfoTrackIsolationBDT2',
    # 'Location' : 'TrackIsolationBDT2_16',
    # 'Particles' : [1,6]
    #},
    #{'Type' : 'RelInfoTrackIsolationBDT2',
    # 'Location' : 'TrackIsolationBDT2_25',
    # 'Particles' : [2,5]
    #},
    {'Type' : 'RelInfoTrackIsolationBDT2',
     'Location' : 'TrackIsolationBDT2_26',
     'Particles' : [2,6]
    },
    {'Type' : 'RelInfoTrackIsolationBDT2',
     'Location' : 'TrackIsolationBDT2_56',
     'Particles' : [5,6]
    }
]

##=========

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays, DaVinci__N5BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

class Bu2LLKConf(LineBuilder) :
    """
    Builder for R_X measurements
    """

    # now just define keys. Default values are fixed later
    __configuration_keys__ = (
        'BFlightCHI2'
        , 'BDIRA'
        , 'BIPCHI2'
        , 'BVertexCHI2'
        , 'DiLeptonPT'
        , 'DiLeptonFDCHI2'
        , 'DiLeptonIPCHI2'
        , 'LeptonIPCHI2'
        , 'LeptonPT'
        , 'LeptonPTTight'
        , 'TauPT'
        , 'TauVCHI2DOF'
        , 'KaonIPCHI2'
        , 'KaonPT'
        , 'KaonPTLoose'
        , 'PionPTRho'
        , 'ProtonP'
        , 'KstarPVertexCHI2'
        , 'KstarPMassWindow'
        , 'KstarPADOCACHI2'
        , 'Pi0PT'
        , 'ProbNNCut'
        , 'ProbNNCutTight'
        , 'DiHadronMass'
        , 'DiHadronVtxCHI2'
        , 'DiHadronADOCACHI2'
        , 'UpperMass'
        , 'BMassWindow'
        , 'UpperBMass'
        , 'UpperBsMass'
        , 'UpperLbMass'
        , 'BMassWindowTau'
        , 'PIDe'
        , 'PIDeInvLow'
        , 'RICHPIDe_Up'
        , 'Trk_Chi2'
        , 'Trk_GhostProb'
        , 'K1_MassWindow_Lo'
        , 'K1_MassWindow_Hi'
        , 'K1_VtxChi2'
        , 'K1_SumPTHad'
        , 'K1_SumIPChi2Had'
        , 'B2K3pill_LeptonIPCHI2'
        , 'B2K3pill_LeptonIPCHI2_Max'
        , 'B2K3pill_LeptonPT'
        , 'B2K3pill_DiLeptonPT'
        , 'B2K3pill_DiLeptonVtxChi2'
        , 'B2K3pill_DiLeptonFDChi2'
        , 'B2K3pill_HadIPCHI2'
        , 'B2K3pill_PionPT'
        , 'B2K3pill_KaonPT'
        , 'B2K3pill_achi2doca'
        , 'B2K3pill_BVtxChi2'
        , 'B2K3pill_BIPDChi2'
        , 'B2K3pill_BFDChi2'
        , 'B2K3pill_BFDIRA'
        , 'B2K3pill_MassWindow_Hi'
        , 'Bu2eeLinePrescale'
        , 'Bu2eeLineFullDSTPrescale'
        , 'Bu2eeLine2Prescale'
        , 'Bu2eeLine2FullDSTPrescale'
        , 'Bu2eeSSLine2Prescale'
        , 'Bu2eeLine3Prescale'
        , 'Bu2eeLine4Prescale'
        , 'Bu2mmLinePrescale'
        , 'Bu2mmLineFullDSTPrescale'
        , 'Bu2mmSSLinePrescale'
        , 'Bu2meLinePrescale'
        , 'Bu2meLineFullDSTPrescale'
        , 'Bu2meSSLinePrescale'
        , 'Bu2mtLinePrescale'
        , 'Bu2mtSSLinePrescale'
        , 'B2KpipipieeLinePrescale'
        , 'B2KpipipimmLinePrescale'
        , 'RelatedInfoTools'
      )

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self._name = name

        mmXLine_name   = name + "_mm"
        mmXSSLine_name = name + "_mmSS"
        eeXLine_name   = name + "_ee"
        eeXSSLine_name = name + "_eeSS"
        meXLine_name   = name + "_me"
        meXSSLine_name = name + "_meSS"
        mtXLine_name   = name + "_mt"
        mtXSSLine_name = name + "_mtSS"
        busy_name = "_extra"
        fulldst_name = "_fulldst"

        from StandardParticles import StdLoosePions as Pions
        from StandardParticles import StdLooseKaons as Kaons
        from StandardParticles import StdLooseProtons as Protons
        from StandardParticles import StdAllLoosePions as AllPions
        from StandardParticles import StdAllLooseKaons as AllKaons
        from StandardParticles import StdAllLooseProtons as AllProtons
        from StandardParticles import StdLooseKstar2Kpi as Kstars
        from StandardParticles import StdLoosePhi2KK as Phis
        from StandardParticles import StdLooseLambdastar2pK as Lambdastars


        # V0s
        from StandardParticles import StdVeryLooseKsLL as KshortsLL
        from StandardParticles import StdLooseKsDD as KshortsDD
        from StandardParticles import StdVeryLooseLambdaLL as LambdasLL
        from StandardParticles import StdLooseLambdaDD as LambdasDD
        from StandardParticles import StdKs2PiPiLL as BrunelKshortsLL
        from StandardParticles import StdKs2PiPiDD as BrunelKshortsDD
        from StandardParticles import StdLambda2PPiLL as BrunelLambdasLL
        from StandardParticles import StdLambda2PPiDD as BrunelLambdasDD

        # NEUTRALS
        from StandardParticles import StdLooseResolvedPi0 as Pi0sRes
        from StandardParticles import StdLooseMergedPi0 as Pi0sMrg


        # 1 : Make K, Ks, K*, K1, Phi and Lambdas

        ### opposite sign hadrons
        SelKaons  = self._filterHadron( name   = "KaonsFor" + self._name,
                                        sel    = Kaons,
                                        params = config )

        SelPions  = self._filterHadron( name   = "PionsFor" + self._name,
                                        sel    = Pions,
                                        params = config )

        SelKshortsLL = self._filterHadron( name   = "KshortsLLFor" + self._name,
                                           sel    = [KshortsLL,BrunelKshortsLL],
                                           params = config )

        SelKshortsDD = self._filterHadron( name   = "KshortsDDFor" + self._name,
                                           sel    =  [KshortsDD,BrunelKshortsDD],
                                           params = config )

        SelKstars = self._filterHadron( name   = "KstarsFor" + self._name,
                                        sel    =  Kstars,
                                        params = config )

        SelKPis = self._makeKPi( name   = "KPisFor" + self._name,
                                        kaons = Kaons,
                                        pions  = Pions,
                                        params = config )

        SelPiPis = self._makePiPi( name   = "PiPisFor" + self._name,
                                        pions  = Pions,
                                        params = config )

        SelKKs = self._makeKK( name   = "KKsFor" + self._name,
                                        kaons = Kaons,
                                        params = config )

        SelpKs = self._makepK( name   = "pKsFor" + self._name,
                                        protons = AllProtons,
                                        kaons  = AllKaons,
                                        params = config )

        SelpPis = self._makepPi( name  = "pPisFor" + self._name,
                                        protons = Protons,
                                        pions  = Pions,
                                        params = config )

        Selpps = self._makepp( name  = "ppsFor" + self._name,
                                        protons = Protons,
                                        params = config )

        SelKstarsPlusLL = self._makeKstarPlus( name    = "KstarsPlusLLFor" + self._name,
                                               kshorts = [KshortsLL,BrunelKshortsLL],
                                               pions   = Pions,
                                               params  = config )

        SelKstarsPlusDD = self._makeKstarPlus( name    = "KstarsPlusDDFor" + self._name,
                                               kshorts = [KshortsDD,BrunelKshortsDD],
                                               pions   = Pions,
                                               params  = config )

        SelKstarsPlusPi0Res = self._makeKstarPlusPi0( name = "KstarsPlusPi0ResFor" + self._name,
                                                      kaons = Kaons,
                                                      pions = Pi0sRes,
                                                      params = config )

        SelKstarsPlusPi0Mrg = self._makeKstarPlusPi0( name = "KstarsPlusPi0MrgFor" + self._name,
                                                      kaons = Kaons,
                                                      pions = Pi0sMrg,
                                                      params = config )


        SelK1s = self._makeK1( name   = "K1For" + self._name,
                               kaons  = Kaons,
                               pions  = Pions,
                               params = config )

        SelK2s = self._makeK2( name   = "K2For" + self._name,
                               kaons  = Kaons,
                               params = config )

        SelPhis = self._filterHadron( name   = "PhisFor" + self._name,
                                      sel    =  Phis,
                                      params = config )

        SelLambdasLL = self._filterHadron( name   = "LambdasLLFor" + self._name,
                                           sel    =  [LambdasLL,BrunelLambdasLL],
                                           params = config )

        SelLambdasDD = self._filterHadron( name   = "LambdasDDFor" + self._name,
                                           sel    =  [LambdasDD,BrunelLambdasDD],
                                           params = config )

        SelLambdastars = self._filterHadron( name   = "LambdastarsFor" + self._name,
                                             sel    =  Lambdastars,
                                             params = config )
        ### same sign hadrons:

        SelPiPisSS = self._makePiPiSS( name   = "PiPisSSFor" + self._name,
                                        pions  = Pions,
                                        params = config )

        SelKPisSS = self._makeKPiSS( name   = "KPisSSFor" + self._name,
                                        kaons = Kaons,
                                        pions  = Pions,
                                        params = config )

        SelKKsSS = self._makeKKSS( name   = "KKsSSFor" + self._name,
                                        kaons = Kaons,
                                        params = config )

        SelpKsSS = self._makepKSS( name   = "pKsSSFor" + self._name,
                                        protons = Protons,
                                        kaons  = Kaons,
                                        params = config )

        SelpPisSS = self._makepPiSS( name  = "pPisSSFor" + self._name,
                                        protons = Protons,
                                        pions  = Pions,
                                        params = config )

        SelppsSS = self._makeppSS( name  = "ppsSSFor" + self._name,
                                        protons = Protons,
                                        params = config )

        # 2 : Make Dileptons

        #from StandardParticles import StdDiElectronFromTracks as DiElectronsFromTracks
        #reconfigure DiElectronMaker to allow for PIDe lower than -2
        from Configurables import DiElectronMaker,ProtoParticleCALOFilter
        from CommonParticles.Utils import trackSelector
        from GaudiKernel.SystemOfUnits import MeV
        diell = DiElectronMaker('DiElectronsFromTracks' + name)
        diell.Particle = "J/psi(1S)"
        selector = trackSelector(diell, trackTypes=["Long"])
        diell.addTool( ProtoParticleCALOFilter('Electron'))
        diell.Electron.Selection = ["RequiresDet='CALO'"]
        diell.DiElectronMassMin = 0.*MeV
        diell.DiElectronMassMax = 5000.*MeV
        diell.DiElectronPtMin = 200.*MeV
        diell.Electron.Selection = ["RequiresDet='CALO' CombDLL(e-pi)>'%(PIDeInvLow)s'" % config]
        #Di-ElectronFrom Tracks made with PIDe> -5 , filtered afterwards to feed the B maker
        DiElectronsFromTracks = Selection('DiElectrons',Algorithm=diell)

        #we still use the electrons from standard particles for the dst line 
        from StandardParticles import StdDiElectronFromTracks as DiElectronsFromTracksTightElectronPID
        from StandardParticles import StdLooseDetachedDiElectronLU as DiElectronsLU
        from StandardParticles import StdLooseDiElectron as DiElectrons
        from StandardParticles import StdLooseDiMuon as DiMuons

        ElecID = "(PIDe > %(PIDe)s)" % config
        MuonID = "(HASMUON)&(ISMUON)"
        TauID  = "(PT > %(TauPT)s)" % config

        MuMu_SS = self._makeMuMuSS( "MuMuSSFor" + self._name, params = config, muonid = MuonID )
        EE_SS = self._makeEESS( "EESSFor" + self._name, params = config, electronid = ElecID )

        MuE    = self._makeMuE( "MuEFor"   + self._name, params = config, electronid = ElecID, muonid = MuonID )
        MuE_SS = self._makeMuE( "MuESSFor" + self._name, params = config, electronid = ElecID, muonid = MuonID, samesign = True )

        MuTau    = self._makeMuTau( "MuTauFor"   + self._name, params = config, tauid = TauID, muonid = MuonID )
        MuTau_SS = self._makeMuTau( "MuTauSSFor" + self._name, params = config, tauid = TauID, muonid = MuonID, samesign = True )


        DiElectronID = "(2 == NINTREE((ABSID==11)&(PIDe > %(PIDe)s)))" % config
        DiElectronID_InvPID = "(0 < NINTREE((ABSID==11)&(PIDe > %(PIDeInvLow)s) & (PIDe < %(PIDe)s))) & (2 == NINTREE((ABSID==11)&(PIDe > %(PIDeInvLow)s)))" % config
        DiElectronID_InvPID += "& (2 == NINTREE((ABSID==11)&(HASCALOS) & (HASRICH)))" % config

        # DIFFERENT CUTS FOR THE LU LINE:
        # THEY ALWAYS HAVE PID ON THE LONG TRACK (DLLe > PIDe )
        # ONLY LU COMBINATIONS, NO UU COMBINATIONS
        #DiElectronIDLU = "((2 == NINTREE((((TRTYPE==3) & (PIDe > %(PIDe)s)) | (TRTYPE==4)))) & (NINTREE((TRTYPE==4)) == 1))" % config # 0.47 %

        # ONLY LU COMBINATIONS AND RICHPIDe>(RICHPIDe from config) OR HASBREMADDED FOR THE UPSTREAM TRACKS
        DiElectronIDLU = "((2 == NINTREE(((TRTYPE==3) & (PIDe > %(PIDe)s)) | ((TRTYPE==4) & ((INFO(16,-1000)==1) | (PPHASRICH & (PPINFO(100,-10000,-10000) > %(RICHPIDe_Up)s)))) )) & (NINTREE((TRTYPE==4)) == 1))" % config # 0.28%

        # LOOSEST CUT, INCLUDING UU AND NO PID ON UPSTREAM
        #DiElectronIDLU = "((2 == NINTREE((((TRTYPE==3) & (PIDe > %(PIDe)s)) | (TRTYPE==4)))) & (NINTREE((TRTYPE==4)) > 0))" % config # 0.96 %

        # PID ON EITHER LONG OR UPSTREAM TRACK (DLLe > PIDe)
        #DiElectronIDLU = "((NINTREE(TRTYPE==4)>0) & (NINTREE(PIDe > %(PIDe)s)>0))" % config # 0.77%


        DiMuonID     = "(2 == NINTREE((ABSID==13)&(HASMUON)&(ISMUON)))"


        SelDiElectron = self._filterDiLepton( "SelDiElectronFor" + self._name,
                                              dilepton = DiElectrons,
                                              params   = config,
                                              idcut    = DiElectronID )

        SelDiElectronTight = self._filterDiLeptonTight( "SelDiElectronTightFor" + self._name,
                                                                                           dilepton = DiElectrons,
                                                                                           params   = config,
                                                                                           idcut    = DiElectronID )

        SelDiElectronFromTracks = self._filterDiLepton( "SelDiElectronFromTracksFor" + self._name,
                                                        dilepton = DiElectronsFromTracks,
                                                        params   = config,
                                                        idcut    = DiElectronID )
        #### Should be making Di-Electrons with -5,0 range
        SelDiElectronFromTracks_InvPID = self._filterDiLepton( "SelDiElectronInvPIDFromTracksFor" + self._name,
                                                        dilepton = DiElectronsFromTracks,
                                                        params   = config,
                                                        idcut    = DiElectronID_InvPID )
        SelDiElectronFromTracksTightElectronPID = self._filterDiLepton( "SelDiElectronFromTracksTightElectronPIDFor" + self._name,
                                                        dilepton = DiElectronsFromTracksTightElectronPID,
                                                        params   = config,
                                                        idcut    = DiElectronID )

        SelDiElectronFromTracksTight = self._filterDiLeptonTight( "SelDiElectronFromTracksTightFor" + self._name,
                                                        dilepton = DiElectronsFromTracks,
                                                        params   = config,
                                                        idcut    = DiElectronID )
        SelDiElectronFromTracksTight_InvPID = self._filterDiLeptonTight( "SelDiElectronInvPIDFromTracksTightFor" + self._name,
                                                        dilepton = DiElectronsFromTracks,
                                                        params   = config,
                                                        idcut    = DiElectronID_InvPID )
        SelDiElectronFromTracks_SS = self._filterDiLeptonTight( "SelDiElectronFromTracksSSFor" + self._name,
                                                        dilepton = EE_SS,
                                                        params   = config,
                                                        idcut    = DiElectronID )

        SelDiElectronLU = self._filterDiLepton( "SelDiElectronLU" + self._name,
                                                        dilepton = DiElectronsLU,
                                                        params   = config,
                                                        idcut    = DiElectronIDLU )



        SelDiMuon = self._filterDiLepton( "SelDiMuonsFor" + self._name,
                                          dilepton = DiMuons,
                                          params   = config,
                                          idcut    = DiMuonID )

        SelDiMuonTight = self._filterDiLeptonTight( "SelDiMuonsTightFor" + self._name,
                                          dilepton = DiMuons,
                                          params   = config,
                                          idcut    = DiMuonID )


        SelDiMuon_SS = self._filterDiLeptonTight( "SelMuMuSSFor" + self._name,
                                          dilepton = MuMu_SS,
                                          params   = config,
                                          idcut    = DiMuonID )

        SelMuE = self._filterDiLepton( "SelMuEFor" + self._name,
                                       dilepton = MuE,
                                       params   = config,
                                       idcut    = None )

        SelMuE_SS = self._filterDiLepton( "SelMuESSFor" + self._name,
                                          dilepton = MuE_SS,
                                          params   = config,
                                          idcut    = None )

        SelMuTau = self._filterMuTau( "SelMuTauFor" + self._name,
                                      dilepton = MuTau,
                                      params   = config,
                                      idcut    = None )

        SelMuTau_SS = self._filterMuTau( "SelMuTauSSFor" + self._name,
                                         dilepton = MuTau_SS,
                                         params   = config,
                                         idcut    = None )


        # 3 : Make Photons

        from StandardParticles import StdAllLooseGammaLL as PhotonConversion

        SelPhoton = self._filterPhotons( "SelPhotonFor" + self._name,
                                         photons = PhotonConversion )


        # 4 : Combine Particles

        SelB2eeX = self._makeB2LLX_normal(eeXLine_name,
                                   dilepton = SelDiElectron,
                                   hadrons  = [ SelPions, SelKaons, SelKstars, SelPhis, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD, SelKstarsPlusLL, SelKstarsPlusDD, SelKstarsPlusPi0Res, SelKstarsPlusPi0Mrg, SelpKs ],#, SelLambdastars,SelK1s, SelK2s, SelPiPis, SelKPis, SelKKs, SelpPis, Selpps
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )

#separate line for busy combinations
        SelB2eeX_busy = self._makeB2LLX_busy(eeXLine_name + busy_name,
                                              dilepton = SelDiElectronTight,
                                              hadrons  = [ SelK1s, SelK2s, SelPiPis, SelKPis, SelKKs, SelpPis, Selpps ],
                                              params   = config,
                                              masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )

        SelB2eeXFromTracks  = self._makeB2LLX_normal(eeXLine_name + "2",
                                              dilepton = SelDiElectronFromTracks,
                                              hadrons  = [ SelPions, SelKaons, SelKstars, SelPhis, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD, SelKstarsPlusLL, SelKstarsPlusDD, SelKstarsPlusPi0Res, SelKstarsPlusPi0Mrg, SelpKs, SelpKsSS],#, SelLambdastars
                                              params   = config,
                                              masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )

        SelB2eeXFromTracksInvPID = self._makeB2LLX_normal(eeXLine_name + "InvPID2",
                                              dilepton = SelDiElectronFromTracks_InvPID,
                                              hadrons  = [ SelPions, SelKaons, SelKstars, SelPhis, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD, SelKstarsPlusLL, SelKstarsPlusDD, SelKstarsPlusPi0Res, SelKstarsPlusPi0Mrg, SelpKs, SelpKsSS],#, SelLambdastars
                                              params   = config,
                                              masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )

        SelB2eeXFromTracks_fulldst  = self._makeB2LLX_normal(eeXLine_name + "2" + fulldst_name,
                                              dilepton = SelDiElectronFromTracksTightElectronPID,
                                              hadrons  = [ SelPions, SelKaons, SelKstars, SelPhis, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD, SelKstarsPlusLL, SelKstarsPlusDD, SelKstarsPlusPi0Res, SelKstarsPlusPi0Mrg, SelpKs, SelpKsSS],#, SelLambdastars
                                              params   = config,
                                              masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )

        #separate line for busy combinations
        SelB2eeXFromTracks_busy  = self._makeB2LLX_busy(eeXLine_name + "2" + busy_name,
                                              dilepton = SelDiElectronFromTracksTight,
                                              hadrons  = [ SelK1s, SelK2s, SelPiPis, SelKPis, SelKKs, SelpPis, Selpps, SelPiPisSS, SelKPisSS, SelKKsSS, SelpPisSS, SelppsSS ],
                                              params   = config,
                                              masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )
        SelB2eeXFromTracks_busy_InvPID  = self._makeB2LLX_busy(eeXLine_name + "InvPID2" + busy_name,
                                              dilepton = SelDiElectronFromTracksTight_InvPID,
                                              hadrons  = [ SelK1s, SelK2s, SelPiPis, SelKPis, SelKKs, SelpPis, Selpps, SelPiPisSS, SelKPisSS, SelKKsSS, SelpPisSS, SelppsSS ],
                                              params   = config,
                                              masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )


        SelB2eeXFromTracks_SS  = self._makeB2LLX(eeXSSLine_name + "2",
                                              dilepton = SelDiElectronFromTracks_SS,
                                              hadrons  = [ SelPions, SelKaons, SelKstars, SelPhis, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD, SelKstarsPlusLL, SelKstarsPlusDD, SelKstarsPlusPi0Res, SelKstarsPlusPi0Mrg, SelK2s, SelKPis, SelKKs, SelpKs, SelpPis, Selpps, SelKKsSS, SelpKsSS ],#SelK1s,SelPiPisSS, SelpPisSS, SelppsSS, SelKPisSS, SelPiPis
                                              params   = config,
                                              masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )


        SelB2mmX = self._makeB2LLX_normal(mmXLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelPions, SelKaons, SelKstars, SelPhis, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD, SelKstarsPlusLL, SelKstarsPlusDD, SelKstarsPlusPi0Res, SelKstarsPlusPi0Mrg, SelpKs, SelpKsSS ],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)

#separate line for busy combinations
        SelB2mmX_busy = self._makeB2LLX_busy(mmXLine_name + busy_name,
                                   dilepton = SelDiMuonTight,
                                   hadrons  = [ SelK1s, SelK2s, SelPiPis, SelKPis, SelKKs, SelpPis, Selpps, SelPiPisSS, SelKPisSS, SelKKsSS, SelpPisSS, SelppsSS ],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)


        SelB2mmX_SS = self._makeB2LLX(mmXSSLine_name,
                                   dilepton = SelDiMuon_SS,
                                   hadrons  = [ SelPions, SelKaons, SelKstars, SelPhis, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD, SelKstarsPlusLL, SelKstarsPlusDD, SelKstarsPlusPi0Res, SelKstarsPlusPi0Mrg, SelK2s, SelKPis, SelKKs, SelpKs, SelpPis, Selpps,SelKKsSS, SelpKsSS ],#SelK1s,SelpPisSS,SelPiPisSS,SelppsSS,SelKPisSS, SelPiPis
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)


        SelB2meX = self._makeB2LLX(meXLine_name,
                                   dilepton = SelMuE,
                                   hadrons  = [ SelPions, SelKaons, SelKstars, SelPhis, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD, SelKstarsPlusLL, SelKstarsPlusDD, SelKstarsPlusPi0Res, SelKstarsPlusPi0Mrg, SelKKs, SelpKs],#, Selpps],#, SelKPisSS, SelKKsSS, SelpKsSS, SelppsSS ],#SelK1s, SelpPisSS,  SelPiPisSS,SelK2s,SelKPis,SelPiPis,SelpPis,
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )

        SelB2meX_SS = self._makeB2LLX(meXSSLine_name,
                                      dilepton = SelMuE_SS,
                                      hadrons  = [ SelPions, SelKaons, SelKstars, SelPhis, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD, SelKstarsPlusLL, SelKstarsPlusDD, SelKstarsPlusPi0Res, SelKstarsPlusPi0Mrg, SelKKs, SelpKs],#, Selpps],#, SelKPisSS, SelKKsSS, SelpKsSS ],#SelK1s, SelpPisSS, SelPiPisSS, SelppsSS, SelK2s,SelKPis,SelPiPis,SelpPis,
                                      params   = config,
                                      masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )

        SelB2mtX = self._makeB2LLX(mtXLine_name,
                                   dilepton = SelMuTau,
                                   hadrons  = [ SelpKs,SelpKsSS ],#SelLambdastars
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindowTau)s *MeV" % config )

        SelB2mtX_SS = self._makeB2LLX(mtXSSLine_name,
                                      dilepton = SelMuTau_SS,
                                      hadrons  = [ SelpKs,SelpKsSS ],#SelLambdastars
                                      params   = config,
                                      masscut  = "ADAMASS('B+') <  %(BMassWindowTau)s *MeV" % config )

        SelB2gammaX = self._makeB2GammaX(eeXLine_name + "3",
                                         photons = SelPhoton,
                                         hadrons = [ SelKstars, SelPhis, SelLambdasLL,  SelLambdasDD,  SelK1s, SelPiPis, SelKPis, SelKKs, SelpKs, SelpPis, Selpps ],
                                         params  = config,
                                         masscut = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config )

        SelB2eeXLU = self._makeB2LLX(eeXLine_name + "4",
                                              dilepton = SelDiElectronLU,
                                              hadrons  = [ SelPions, SelKaons, SelKstars, SelPhis, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD, SelKstarsPlusLL, SelKstarsPlusDD, SelpKs], #, SelKstarsPlusPi0Res, SelKstarsPlusPi0Mrg, SelK2s, SelPiPis, SelKPis, SelKKs, SelpKs, SelpPis, Selpps ],
                                              params   = config,
                                              masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )

        SelB2Kpipipimm = self._makeB2Kpipipill(mmXLine_name + "_K3pi",
                                                dilepton = DiMuons,
                                                kaons = Kaons,
                                                pions = AllPions,
                                                params = config )

        SelB2Kpipipiee = self._makeB2Kpipipill(eeXLine_name + "_K3pi",
                                                dilepton = DiElectronsFromTracks,
                                                kaons = Kaons,
                                                pions = AllPions,
                                                params = config )


        # selections for Loose Lb->{pK,Ksppi}ll =========================

        Sel_ee_Loose       = self._make_ee_Loose("Sel_ee_Loose_For" + self._name, params = config )

        Sel_mm_Loose       = self._make_mm_Loose("Sel_mm_Loose_For" + self._name, params = config )
        Sel_me_Loose       = self._make_me_Loose("Sel_me_Loose_For" + self._name, params = config )
        Sel_meSS_Loose       = self._make_me_Loose("Sel_meSS_Loose_For" + self._name, params = config , samesign=True)

        SelL1520forpKll    = self._makeL1520forpkll("SelL1520For"  + self._name,params = config )

        SelLb2pkmmLoose    = self._makeLb2pkmm(mmXLine_name + "_pkmmLoose",
                                            l1520    = SelL1520forpKll,
                                            dimuons  = Sel_mm_Loose, 
                                            params   = config )

        SelLb2pkmeLoose    = self._makeLb2pkme(meXLine_name + "_pkmeLoose",
                                            l1520    = SelL1520forpKll,
                                            mue  = Sel_me_Loose, 
                                            params   = config )
        SelLb2pkmeSSLoose    = self._makeLb2pkme(meXSSLine_name + "_pkmeSSLoose",
                                            l1520    = SelL1520forpKll,
                                            mue  = Sel_meSS_Loose, 
                                            params   = config )

        SelLb2pkeeLoose    = self._makeLb2pkee(eeXLine_name + "_pkeeLoose",
                                            l1520    = SelL1520forpKll,
                                            dilepton = Sel_ee_Loose,
                                            params   = config )

        SelKsppi           = self._makeKsppi("SelKsppi" + self._name,
                                            params   = config )

        SelLb2KsppimmLoose = self._makeLb2Ksppimm(mmXLine_name + "_KsppimmLoose",
                                            l1820    = SelKsppi,
                                            dimuons  = Sel_mm_Loose, 
                                            params = config )

        SelLb2KsppieeLoose = self._makeLb2Ksppiee(eeXLine_name + "_KsppieeLoose",
                                            l1820    = SelKsppi,
                                            dilepton = Sel_ee_Loose,
                                            params   = config )
        #=============================================================

        # 5 : Declare Lines

        SPDFilter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 450 )" ,
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb" ]
            }

        SPDFilter_loose = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 600 )" , # B2Xmm
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb" ]
            }



        self.B2eeXLine = StrippingLine(eeXLine_name + "Line",
                                       prescale          = config['Bu2eeLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2eeX,
                                       RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False,
                                       EnableFlavourTagging = True)

        self.B2eeXLine_fulldst = StrippingLine(eeXLine_name + "Line" + fulldst_name,
                                       prescale          = config['Bu2eeLineFullDSTPrescale'],
                                       postscale         = 1,
                                       selection         = SelB2eeX,
                                       RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False,
                                       EnableFlavourTagging = False)

        self.B2eeXLine_busy = StrippingLine(eeXLine_name + "Line" + busy_name,
                                       prescale          = config['Bu2eeLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2eeX_busy,
                                       RelatedInfoTools  = config['RelatedInfoTools'], #+ RelInfoTrackIsolationBDT2,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False,
                                       EnableFlavourTagging = True,
                                       MaxCandidates = 400)

        self.B2eeXFromTracksLine = StrippingLine(eeXLine_name + "Line2",
                                                 prescale          = config['Bu2eeLine2Prescale'],
                                                 postscale         = 1,
                                                 selection         = SelB2eeXFromTracks,
                                                 RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                                 FILTER            = SPDFilter,
                                                 RequiredRawEvents = ["Velo"],
                                                 MDSTFlag          = False,
                                                 EnableFlavourTagging = True)
        self.B2eeXFromTracksInvPIDLine = StrippingLine(eeXLine_name + "InvPIDLine2",
                                                 prescale          = config['Bu2eeLine2Prescale'],
                                                 postscale         = 1,
                                                 selection         = SelB2eeXFromTracksInvPID,
                                                 RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                                 FILTER            = SPDFilter,
                                                 RequiredRawEvents = [],
                                                 MDSTFlag          = False,
                                                 EnableFlavourTagging = True)

        self.B2eeXFromTracksLine_fulldst = StrippingLine(eeXLine_name + "Line2" + fulldst_name,
                                                 prescale          = config['Bu2eeLine2FullDSTPrescale'],
                                                 postscale         = 1,
                                                 selection         = SelB2eeXFromTracks_fulldst,
                                                 RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                                 FILTER            = SPDFilter,
                                                 RequiredRawEvents = ["Velo"],
                                                 MDSTFlag          = False,
                                                 EnableFlavourTagging = False)

        self.B2eeXFromTracksLine_busy = StrippingLine(eeXLine_name + "Line2" + busy_name,
                                                 prescale          = config['Bu2eeLine2Prescale'],
                                                 postscale         = 1,
                                                 selection         = SelB2eeXFromTracks_busy,
                                                 RelatedInfoTools  = config['RelatedInfoTools'], #+ RelInfoTrackIsolationBDT2,
                                                 FILTER            = SPDFilter,
                                                 RequiredRawEvents = [],
                                                 MDSTFlag          = False,
                                                 EnableFlavourTagging = True,
                                                 MaxCandidates = 400)
        self.B2eeXFromTracksLine_busy_InvPID = StrippingLine(eeXLine_name + "InvPIDLine2" + busy_name,
                                                 prescale          = config['Bu2eeLine2Prescale'],
                                                 postscale         = 1,
                                                 selection         = SelB2eeXFromTracks_busy_InvPID,
                                                 RelatedInfoTools  = config['RelatedInfoTools'], #+ RelInfoTrackIsolationBDT2,
                                                 FILTER            = SPDFilter,
                                                 RequiredRawEvents = [],
                                                 MDSTFlag          = False,
                                                 EnableFlavourTagging = True,
                                                 MaxCandidates = 400)

        self.B2eeX_SSFromTracksLine = StrippingLine(eeXSSLine_name + "Line2",
                                                 prescale          = config['Bu2eeSSLine2Prescale'],
                                                 postscale         = 1,
                                                 selection         = SelB2eeXFromTracks_SS,
                                                 RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                                 FILTER            = SPDFilter,
                                                 RequiredRawEvents = [],
                                                 MDSTFlag          = False,
                                                 EnableFlavourTagging = True,
                                                 MaxCandidates = 300)

        self.B2mmXLine = StrippingLine(mmXLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX,
                                       RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False,
                                       EnableFlavourTagging = True)

        self.B2mmXLine_fulldst = StrippingLine(mmXLine_name + "Line" + fulldst_name,
                                       prescale          = config['Bu2mmLineFullDSTPrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX,
                                       RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False,
                                       EnableFlavourTagging = False)

        self.B2mmXLine_busy = StrippingLine(mmXLine_name + "Line" + busy_name,
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX_busy,
                                       RelatedInfoTools  = config['RelatedInfoTools'], # + RelInfoTrackIsolationBDT2,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False,
                                       EnableFlavourTagging = True,
                                       MaxCandidates = 400)


        self.B2mmX_SSLine = StrippingLine(mmXSSLine_name + "Line",
                                       prescale          = config['Bu2mmSSLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX_SS,
                                       RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False,
                                       EnableFlavourTagging = True,                                       
                                       MaxCandidates = 300)
        self.B2meXLine = StrippingLine(meXLine_name + "Line",
                                       prescale          = config['Bu2meLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2meX,
                                       RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False,
                                       EnableFlavourTagging = True,                                       
                                       MaxCandidates = 400)
        
        self.B2meXLine_fulldst = StrippingLine(meXLine_name + "Line" + fulldst_name,
                                       prescale          = config['Bu2meLineFullDSTPrescale'],
                                       postscale         = 1,
                                       selection         = SelB2meX,
                                       RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False,
                                       EnableFlavourTagging = False,
                                       MaxCandidates = 400)

        self.B2meX_SSLine = StrippingLine(meXSSLine_name + "Line",
                                          prescale          = config['Bu2meSSLinePrescale'],
                                          postscale         = 1,
                                          selection         = SelB2meX_SS,
                                          RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                          FILTER            = SPDFilter,
                                          RequiredRawEvents = [],
                                          MDSTFlag          = False,
                                          EnableFlavourTagging = True,                                       
                                          MaxCandidates = 300)

        self.B2mtXLine = StrippingLine(mtXLine_name + "Line",
                                       prescale          = config['Bu2mtLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mtX,
                                       RelatedInfoTools  = config['RelatedInfoTools'],
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False,
                                       MaxCandidates     = 30 )

        self.B2mtX_SSLine = StrippingLine(mtXSSLine_name + "Line",
                                          prescale          = config['Bu2mtSSLinePrescale'],
                                          postscale         = 1,
                                          selection         = SelB2mtX_SS,
                                          RelatedInfoTools  = config['RelatedInfoTools'],
                                          FILTER            = SPDFilter,
                                          RequiredRawEvents = [],
                                          MDSTFlag          = False,
                                          MaxCandidates     = 30)

        self.B2gammaXLine = StrippingLine(eeXLine_name + "Line3",
                                          prescale          = config['Bu2eeLine3Prescale'],
                                          postscale         = 1,
                                          selection         = SelB2gammaX,
                                          RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                          FILTER            = SPDFilter,
                                          RequiredRawEvents = [],
                                          MDSTFlag          = False,
                                          EnableFlavourTagging = True,                                       
                                          MaxCandidates = 300)

        self.B2eeXLULine  = StrippingLine(eeXLine_name + "Line4",
                                                 prescale          = config['Bu2eeLine4Prescale'],
                                                 postscale         = 1,
                                                 selection         = SelB2eeXLU,
                                                 postselalg        = self._storeInputBremPhotons(eeXLine_name + "Line4", outputlocprefix="Leptonic/Phys/"),
                                                 RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                                 FILTER            = SPDFilter,
                                                 RequiredRawEvents = [],
                                                 MDSTFlag          = False,
                                                 EnableFlavourTagging = True,                                       
                                                 MaxCandidates = 300)

        self.B2KpipipimmLine = StrippingLine( mmXLine_name + "_K3pi" + "Line",
                                                prescale          = config['B2KpipipimmLinePrescale'],
                                                postscale         = 1,
                                                selection         = SelB2Kpipipimm,
                                                RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                                FILTER            = SPDFilter,
                                                RequiredRawEvents = [],
                                                MDSTFlag          = False,
                                                EnableFlavourTagging = True,                                       
                                                MaxCandidates = 400)

        self.B2KpipipieeLine = StrippingLine( eeXLine_name + "_K3pi" + "Line",
                                                prescale          = config['B2KpipipieeLinePrescale'],
                                                postscale         = 1,
                                                selection         = SelB2Kpipipiee,
                                                RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                                FILTER            = SPDFilter,
                                                RequiredRawEvents = [],
                                                MDSTFlag          = False,
                                                EnableFlavourTagging = True,                             
                                                MaxCandidates = 400)

        # Loose lines for Lb->{pK,Ksppi}ll
        self.Lb2pkmmLooseLine    = StrippingLine( mmXLine_name + "_Lb2pkmmLoose" + "Line",
                                                  selection         = SelLb2pkmmLoose,
                                                  RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2_Lb2pKll,
                                                  FILTER            = SPDFilter_loose, # SPD < 600
                                                  MDSTFlag          = False,
                                                  MaxCandidates = 4000)

        self.Lb2pkmeLooseLine    = StrippingLine( meXLine_name + "_Lb2pkmeLoose" + "Line",
                                                  selection         = SelLb2pkmeLoose,
                                                  RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2_Lb2pKll,
                                                  FILTER            = SPDFilter_loose, # SPD < 600
                                                  MDSTFlag          = False,
                                                  MaxCandidates = 4000)

        self.Lb2pkmeSSLooseLine    = StrippingLine( meXSSLine_name + "_Lb2pkmeSSLoose" + "Line",
                                                  selection         = SelLb2pkmeSSLoose,
                                                  RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2_Lb2pKll,
                                                  FILTER            = SPDFilter_loose, # SPD < 600
                                                  MDSTFlag          = False,
                                                  MaxCandidates = 4000)

        self.Lb2pkeeLooseLine    = StrippingLine( eeXLine_name + "_Lb2pkeeLoose" + "Line",
                                                  selection         = SelLb2pkeeLoose,
                                                  RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2_Lb2pKll,
                                                  FILTER            = SPDFilter_loose, # SPD < 600
                                                  MDSTFlag          = False,
                                                  MaxCandidates = 4000)

        self.Lb2KsppimmLooseLine = StrippingLine( mmXLine_name + "_Lb2KsppimmLoose" + "Line",
                                                  selection         = SelLb2KsppimmLoose,
                                                  RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2_Lb2Ksppill,
                                                  FILTER            = SPDFilter_loose, # SPD < 600 
                                                  MDSTFlag          = False,
                                                  MaxCandidates = 4000)

        self.Lb2KsppieeLooseLine = StrippingLine( eeXLine_name + "_Lb2KsppieeLoose" + "Line",
                                                  selection         = SelLb2KsppieeLoose,
                                                  RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2_Lb2Ksppill,
                                                  FILTER            = SPDFilter_loose, # SPD < 600
                                                  MDSTFlag          = False,
                                                  MaxCandidates = 4000)

        # 6 : Register Lines

        self.registerLine( self.B2eeXLine )
        self.registerLine( self.B2eeXLine_fulldst )
        self.registerLine( self.B2eeXLine_busy )
        self.registerLine( self.B2eeXFromTracksLine )
        self.registerLine( self.B2eeXFromTracksInvPIDLine)
        self.registerLine( self.B2eeXFromTracksLine_fulldst )
        self.registerLine( self.B2eeXFromTracksLine_busy )
        self.registerLine( self.B2eeXFromTracksLine_busy_InvPID )

        self.registerLine( self.B2eeX_SSFromTracksLine)
        self.registerLine( self.B2mmXLine )
        self.registerLine( self.B2mmXLine_fulldst )
        self.registerLine( self.B2mmXLine_busy )
        self.registerLine( self.B2mmX_SSLine )
        self.registerLine( self.B2meXLine )
        self.registerLine( self.B2meXLine_fulldst )
        self.registerLine( self.B2meX_SSLine )
        self.registerLine( self.B2mtXLine )
        self.registerLine( self.B2mtX_SSLine )
        self.registerLine( self.B2gammaXLine )
        self.registerLine( self.B2eeXLULine )
        self.registerLine( self.B2KpipipimmLine )
        self.registerLine( self.B2KpipipieeLine )
        self.registerLine( self.Lb2pkmmLooseLine )
        self.registerLine( self.Lb2pkmeLooseLine )
        self.registerLine( self.Lb2pkmeSSLooseLine )
        self.registerLine( self.Lb2pkeeLooseLine )
        self.registerLine( self.Lb2KsppimmLooseLine )
        self.registerLine( self.Lb2KsppieeLooseLine )

#####################################################
    def _filterHadron( self, name, sel, params ):
        """
        Filter for some of hadronic final states
        Check for list as input for V0s which use (Very)Loose and Brunel candidates
        """
        if isinstance(sel,list):
            sel_list = [MergedSelection("Merged"+name,RequiredSelections= sel,Unique=False)]
        else:
            sel_list = [sel]


        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        # need to add the ID here
        _Code = "(PT > %(KaonPT)s *MeV) & " \
                "(M < %(DiHadronMass)s*MeV) & " \
                "((ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) | " \
                "(NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))))" % params

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = sel_list )
#####################################################
    def _filterDiLepton( self, name, dilepton, params, idcut = None ) :
        """
        Handy interface for dilepton filter
        """

        _Code = "(ID=='J/psi(1S)') & "\
                "(PT > %(DiLeptonPT)s *MeV) & "\
                "(MM < %(UpperMass)s *MeV) & "\
                "(MINTREE(ABSID<14,PT) > %(LeptonPT)s *MeV) & "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "\
                "(VFASPF(VCHI2/VDOF) < 9) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "\
                "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params

        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )

        _Filter = FilterDesktop( Code = _Code )

        return Selection(name, Algorithm = _Filter, RequiredSelections = [ dilepton ] )

#####################################################
    def _filterDiLeptonTight( self, name, dilepton, params, idcut = None ) :
        """
        Handy interface for dilepton filter
        """

        _Code = "(ID=='J/psi(1S)') & "\
            "(PT > %(DiLeptonPT)s *MeV) & "\
            "(MM < %(UpperMass)s *MeV) & "\
            "(MINTREE(ABSID<14,PT) > %(LeptonPTTight)s *MeV) & "\
            "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "\
            "(VFASPF(VCHI2/VDOF) < 9) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "\
            "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params

        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )

        _Filter = FilterDesktop( Code = _Code )

        return Selection(name, Algorithm = _Filter, RequiredSelections = [ dilepton ] )


#####################################################
    def _filterMuTau( self, name, dilepton, params, idcut = None ) :
        """
        Handy interface for mutau filter
        """

        _Code = "(ID=='J/psi(1S)') & "\
                "(PT > %(DiLeptonPT)s *MeV) & "\
                "(MM < %(UpperMass)s *MeV) & "\
                "(MINTREE(ABSID<14,PT) > %(LeptonPT)s *MeV) & "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "\
                "(VFASPF(VCHI2/VDOF) < %(TauVCHI2DOF)s) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "\
                "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params

        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )

        _Filter = FilterDesktop( Code = _Code )

        return Selection(name, Algorithm = _Filter, RequiredSelections = [ dilepton ] )
#####################################################
    def _filterPhotons( self, name, photons ):
        """
        Filter photon conversions
        """

        _Code = "(PT > 1000*MeV) & (HASVERTEX) & (VFASPF(VCHI2/VDOF) < 9)"

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = [ photons ] )
#####################################################
    def _makeKstarPlus( self, name, kshorts, pions, params):
        """
        Make a K*(892)+ -> KS0 pi+
        Check for list as input for KS0s which use (Very)Loose and Brunel candidates
        """
        if isinstance(kshorts,list):
            sel_list = [MergedSelection("Merged"+name,RequiredSelections= kshorts,Unique=False),pions]
        else:
            sel_list = [kshorts,pions]

        _Decays = "[K*(892)+ -> KS0 pi+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(ADAMASS('K*(892)+') < %(KstarPMassWindow)s *MeV) & " \
                          "(ADOCACHI2CUT( %(KstarPADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(KstarPVertexCHI2)s)" % params

        _KshortCut = "(PT > %(KaonPT)s *MeV) & " \
                     "(M < %(DiHadronMass)s*MeV) & " \
                     "(NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)))" % params

        _PionCut = "(PT > %(KaonPT)s *MeV) & " \
                   "(M < %(DiHadronMass)s*MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "KS0"  : _KshortCut,
            "pi+"  : _PionCut
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = sel_list )
#####################################################
    def _makeKstarPlusPi0( self, name, kaons, pions, params):
        """
        Make a K*+ -> K+ pi0. Note that either merged or resolved pi0s may be used as daughter.
        """

        _Decays = "[K*(892)+ -> K+ pi0]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(ADAMASS('K*(892)+') < %(KstarPMassWindow)s *MeV)" % params

        _MotherCut = "(ADMASS('K*(892)+') < %(KstarPMassWindow)s *MeV) " % params

        _KaonCut = "(PT > %(KaonPT)s *MeV) & " \
            "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params

        _PionCut = "(PT > %(Pi0PT)s * MeV)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "K+"  : _KaonCut,
            "pi0"  : _PionCut
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ kaons, pions ] )

#####################################################
    def _makePiPi( self, name, pions, params):
        """
        Make a rho -> pi+ pi- in a range below 2600 MeV.
        """

        _Decays = "rho(770)0 -> pi+ pi-"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(DiHadronMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params #UpperBMass

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut = "(PT > %(PionPTRho)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(ProbNNCutTight)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "pi+"  : _DaughterCut,
            "pi-" : _DaughterCut
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ pions ] )
#####################################################
    def _makePiPiSS( self, name, pions, params):
        """
        Make a same-sign rho -> pi+ pi+ in a range below 2600 MeV.
        """

        _Decays = "[rho(770)0 -> pi+ pi+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(DiHadronMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut = "(PT > %(PionPTRho)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)  & (PROBNNpi > %(ProbNNCutTight)s))" % params
        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "pi+"  : _DaughterCut,
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ pions ] )
#####################################################
    def _makeKPi( self, name, kaons, pions, params):
        """
        Make a K* -> K+ pi- in entire range below 2600 MeV.
        """

        _Decays = "[K*_0(1430)0 -> K+ pi-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(DiHadronMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_k = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s))  & (PROBNNk > %(ProbNNCutTight)s)" % params
        _DaughterCut_pi = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s))  & (PROBNNpi > %(ProbNNCutTight)s)" % params
        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "K+"  : _DaughterCut_k,
            "pi-" : _DaughterCut_pi
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ kaons, pions ] )
#####################################################
    def _makeKPiSS( self, name, kaons, pions, params):
        """
        Make a same-sign K* -> K+ pi+ in entire range below 2600 MeV.
        """

        _Decays = "[K*_0(1430)0 -> K+ pi+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(DiHadronMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_k = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s))  & (PROBNNk > %(ProbNNCutTight)s)" % params
        _DaughterCut_pi = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s))  & (PROBNNpi > %(ProbNNCutTight)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "K+"  : _DaughterCut_k,
            "pi+" : _DaughterCut_pi
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ kaons, pions ] )
#####################################################
    def _makeKK( self, name, kaons, params):
        """
        Make an f2(1525) -> K+ K- in entire range.
        """

        _Decays = "f'_2(1525) -> K+ K-"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(UpperBsMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s))  & (PROBNNk > %(ProbNNCutTight)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "K+"  : _DaughterCut,
            "K-" : _DaughterCut
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ kaons ] )
#####################################################
    def _makeKKSS( self, name, kaons, params):
        """
        Make a same-sign f2(1525) -> K+ K+ in entire range.
        """

        _Decays = "[f'_2(1525) -> K+ K+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(UpperBsMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s))  & (PROBNNk > %(ProbNNCutTight)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "K+"  : _DaughterCut,
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ kaons ] )
#####################################################
    def _makepK( self, name, protons, kaons, params):
        """
        Make a Lambda* -> p K- in entire range.
        """

        _Decays = "[Lambda(1520)0 -> p+ K-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(UpperLbMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params


        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_p = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNp > %(ProbNNCut)s)" % params
        _DaughterCut_K = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNk > %(ProbNNCut)s)" % params


        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut_p,
            "K-" : _DaughterCut_K
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons, kaons ] )
#####################################################
    def _makepKSS( self, name, protons, kaons, params):
        """
        Make a same-sign Lambda* -> p K+ in entire range.
        """

        _Decays = "[Lambda(1520)0 -> p+ K+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(UpperLbMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_p = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNp > %(ProbNNCut)s)" % params
        _DaughterCut_K = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNk > %(ProbNNCut)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut_p,
            "K+" : _DaughterCut_K
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons, kaons ] )
#####################################################
    def _makepPi( self, name, protons, pions, params):
        """
        Make a N* -> p pi- in entire range until 2600 MeV.
        """

        _Decays = "[N(1440)0 -> p+ pi-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(DiHadronMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_p = "(PT > %(PionPTRho)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                         "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNp > %(ProbNNCutTight)s)" % params
        _DaughterCut_pi = "(PT > %(KaonPTLoose)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(ProbNNCutTight)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut_p,
            "pi-" : _DaughterCut_pi
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons, pions ] )
#####################################################
    def _makepPiSS( self, name, protons, pions, params):
        """
        Make a same-sign N* -> p pi+ in entire range below 2600 MeV.
        """

        _Decays = "[N(1440)0 -> p+ pi+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(DiHadronMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_p = "(PT > %(PionPTRho)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNp > %(ProbNNCutTight)s)" % params
        _DaughterCut_pi = "(PT > %(KaonPTLoose)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(ProbNNCutTight)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut_p,
            "pi+" : _DaughterCut_pi
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons, pions ] )
#####################################################
    def _makepp( self, name, protons, params):
        """
        Make a f2 -> p pbar in entire range.
        """

        _Decays = "f_2(1950) -> p+ p~-"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(UpperLbMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV)  &" \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNp > %(ProbNNCutTight)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut,
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons ] )
#####################################################
    def _makeppSS( self, name, protons, params):
        """
        Make a same-sign f2 -> p p in entire range.
        """

        _Decays = "[f_2(1950) -> p+ p+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(UpperLbMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNp > %(ProbNNCutTight)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut,
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons ] )
#####################################################
    def _makeK1( self, name, kaons, pions, params ) :
        """
        Make a K1 -> K+pi-pi+
        """

        _Decays = "[K_1(1270)+ -> K+ pi+ pi-]cc"

         # define all the cuts
        _K1Comb12Cuts  = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV)" % params
        _K1CombCuts    = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV) & ((APT1+APT2+APT3) > %(K1_SumPTHad)s*MeV)" % params

        _K1MotherCuts  = "(VFASPF(VCHI2) < %(K1_VtxChi2)s) & (SUMTREE(MIPCHI2DV(PRIMARY),((ABSID=='K+') | (ABSID=='K-') | (ABSID=='pi+') | (ABSID=='pi-')),0.0) > %(K1_SumIPChi2Had)s)" % params
        _daughtersCuts_K = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNk > %(ProbNNCut)s)" % params
        _daughtersCuts_pi = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(ProbNNCut)s)" % params

        _Combine = DaVinci__N3BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "K+"  : _daughtersCuts_K,
            "pi+" : _daughtersCuts_pi }

        _Combine.Combination12Cut = _K1Comb12Cuts
        _Combine.CombinationCut   = _K1CombCuts
        _Combine.MotherCut        = _K1MotherCuts

        # make and store the Selection object
        return Selection( name, Algorithm = _Combine, RequiredSelections = [ kaons, pions ] )
#####################################################
    def _makeK2( self, name, kaons, params ) :
        """
        Make a K2 -> K+K+K-
        """

        _Decays = "[K_2(1770)+ -> K+ K+ K-]cc"

         # define all the cuts
        _K2Comb12Cuts  = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV) & (ACHI2DOCA(1,2) < 8)" % params
        _K2CombCuts    = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV) & ((APT1+APT2+APT3) > %(K1_SumPTHad)s*MeV)" % params

        _K2MotherCuts  = "(VFASPF(VCHI2) < %(K1_VtxChi2)s) & (SUMTREE(MIPCHI2DV(PRIMARY),((ABSID=='K+') | (ABSID=='K-')),0.0) > %(K1_SumIPChi2Had)s)" % params
        _daughtersCuts = "(P > %(ProtonP)s *MeV) & (TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNk > %(ProbNNCut)s)" % params

        _Combine = DaVinci__N3BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "K+"  : _daughtersCuts,
            "K-"  : _daughtersCuts,
         }

        _Combine.Combination12Cut = _K2Comb12Cuts
        _Combine.CombinationCut   = _K2CombCuts
        _Combine.MotherCut        = _K2MotherCuts

        # make and store the Selection object
        return Selection( name, Algorithm = _Combine, RequiredSelections = [ kaons ] )
####################################################
    def _makeMuE( self, name, params, electronid = None, muonid = None , samesign = False):
        """
        Makes MuE combinations
        """

        from StandardParticles import StdLooseMuons as Muons
        from StandardParticles import StdLooseElectrons as Electrons

        _DecayDescriptor = "[J/psi(1S) -> mu+ e-]cc"
        if samesign : _DecayDescriptor = "[J/psi(1S) -> mu+ e+]cc"

        _MassCut = "(AM > 100*MeV)"

        _MotherCut = "(VFASPF(VCHI2/VDOF) < 9)"

        _DaughtersCut = "(PT > %(LeptonPT)s) & " \
                        "(MIPCHI2DV(PRIMARY) > %(LeptonIPCHI2)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _DecayDescriptor,
                                     CombinationCut  = _MassCut,
                                     MotherCut       = _MotherCut )

        _MuonCut     = _DaughtersCut
        _ElectronCut = _DaughtersCut

        if muonid     : _MuonCut     += ( "&" + muonid )
        if electronid : _ElectronCut += ( "&" + electronid )

        _Combine.DaughtersCuts = {
            "mu+" : _MuonCut,
            "e+"  : _ElectronCut
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ Muons, Electrons ] )
####################################################
    def _makeMuMuSS( self, name, params, muonid = None):
        """
        Makes MuMu same sign combinations
        """
        from StandardParticles import StdLooseMuons as Muons

        _DecayDescriptor = "[J/psi(1S) -> mu+ mu+]cc"

        _MassCut = "(AM > 100*MeV)"

        _MotherCut = "(VFASPF(VCHI2/VDOF) < 9)"

        _DaughtersCut = "(PT > %(LeptonPT)s) & " \
                        "(MIPCHI2DV(PRIMARY) > %(LeptonIPCHI2)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _DecayDescriptor,
                                     CombinationCut  = _MassCut,
                                     MotherCut       = _MotherCut )

        _MuonCut     = _DaughtersCut

        if muonid     : _MuonCut     += ( "&" + muonid )

        _Combine.DaughtersCuts = {
            "mu+" : _MuonCut,
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ Muons ] )
####################################################
    def _makeEESS( self, name, params, electronid = None):
        """
        Makes EE same-sign combinations
        """
        from Configurables import DiElectronMaker,ProtoParticleCALOFilter
        from CommonParticles.Utils import trackSelector
        from GaudiKernel.SystemOfUnits import MeV
        ee = DiElectronMaker('DiElectronsSS' + name)
        ee.Particle = "J/psi(1S)"
        #ee.DecayDescriptor = "[J/psi(1S) -> e+ e+]cc"
        selector = trackSelector(ee, trackTypes=["Long"])

        ee.addTool( ProtoParticleCALOFilter('Electron'))
        ee.Electron.Selection = ["RequiresDet='CALO'"]
        ee.DiElectronMassMin = 0.*MeV
        ee.DiElectronMassMax = 5000.*MeV
        ee.DiElectronPtMin = 200.*MeV
        ee.OppositeSign = 0

        if electronid : ee.Electron.Selection = ["RequiresDet='CALO' CombDLL(e-pi)>'%(PIDe)s'" % params]

        return Selection(name+'eeSelection',Algorithm=ee)

#####################################################
    def _makeMuTau( self, name, params, tauid = None, muonid = None , samesign = False):
        """
        Makes MuTau combinations
        """

        from StandardParticles import StdLooseMuons as Muons
        #from CommonParticles import StdLooseDetachedTau
        #Taus = DataOnDemand(Location = "Phys/StdLooseDetachedTau3pi/Particles")
        from CommonParticles import StdTightDetachedTau
        Taus = DataOnDemand(Location = "Phys/StdTightDetachedTau3pi/Particles")

        _DecayDescriptor = "[J/psi(1S) -> mu+ tau-]cc"
        if samesign : _DecayDescriptor = "[J/psi(1S) -> mu+ tau+]cc"

        _MassCut = "(AM > 100*MeV)"

        _MotherCut = "(VFASPF(VCHI2/VDOF) < %(TauVCHI2DOF)s)" % params

        _DaughtersCut = "(PT > %(LeptonPT)s) & " \
                        "(MIPCHI2DV(PRIMARY) > %(LeptonIPCHI2)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _DecayDescriptor,
                                     CombinationCut  = _MassCut,
                                     MotherCut       = _MotherCut )

        _MuonCut = _DaughtersCut
        _TauCut  = _DaughtersCut

        if muonid : _MuonCut += ( "&" + muonid )
        if tauid  : _TauCut  += ( "&" + tauid )

        _Combine.DaughtersCuts = {
            "mu+"  : _MuonCut,
            "tau+" : _TauCut
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ Muons, Taus ] )
#####################################################
    def _makeB2LLX( self, name, dilepton, hadrons, params, masscut = "(ADAMASS('B+')< 1500 *MeV" ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [ "[ B+ -> J/psi(1S) K+ ]cc",
                    "[ B+ -> J/psi(1S) pi+ ]cc",
                    "[ B+ -> J/psi(1S) K*(892)+ ]cc",
                    "[ B+ -> J/psi(1S) K_1(1270)+ ]cc",
                    "[ B+ -> J/psi(1S) K_2(1770)+ ]cc",
                    " B0 -> J/psi(1S) KS0 ",
                    " B0 -> J/psi(1S) rho(770)0 ",
                    "[ B0 -> J/psi(1S) K*(892)0 ]cc",
                    "[ B0 -> J/psi(1S) K*_0(1430)0 ]cc",
                    " B_s0 -> J/psi(1S) phi(1020) ",
                    " B_s0 -> J/psi(1S) f'_2(1525) ",
                    " B_s0 -> J/psi(1S) f_2(1950) ",
                    "[ Lambda_b0 -> J/psi(1S) Lambda0 ]cc",
                    "[ Lambda_b0 -> J/psi(1S) N(1440)0 ]cc",
                    "[ Lambda_b0 -> J/psi(1S) Lambda(1520)0 ]cc" ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
#####################################################
    def _makeB2LLX_normal( self, name, dilepton, hadrons, params, masscut = "(ADAMASS('B+')< 1500 *MeV" ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [ "[ B+ -> J/psi(1S) K+ ]cc",
                    "[ B+ -> J/psi(1S) pi+ ]cc",
                    "[ B+ -> J/psi(1S) K*(892)+ ]cc",
                    " B0 -> J/psi(1S) KS0 ",
                    "[ B0 -> J/psi(1S) K*(892)0 ]cc",
                    " B_s0 -> J/psi(1S) phi(1020) ",
                    "[ Lambda_b0 -> J/psi(1S) Lambda0 ]cc",
                    "[ Lambda_b0 -> J/psi(1S) Lambda(1520)0 ]cc" ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
#####################################################
    def _makeB2LLX_busy( self, name, dilepton, hadrons, params, masscut = "(ADAMASS('B+')< 1500 *MeV" ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [
                    "[ B+ -> J/psi(1S) K_1(1270)+ ]cc",
                    "[ B+ -> J/psi(1S) K_2(1770)+ ]cc",
                    " B0 -> J/psi(1S) rho(770)0 ",
                    "[ B0 -> J/psi(1S) K*_0(1430)0 ]cc",
                    " B_s0 -> J/psi(1S) f'_2(1525) ",
                    " B_s0 -> J/psi(1S) f_2(1950) ",
                    "[ Lambda_b0 -> J/psi(1S) N(1440)0 ]cc", ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
#####################################################
    def _makeB2GammaX( self, name, photons, hadrons, params, masscut = "(ADAMASS('B+')< 1500 *MeV" ):
        """
        CombineParticles / Selection for the B
        """

        _Decays =  [ "[ B0   -> gamma K*(892)0 ]cc",
                     "[ B0 -> gamma K*_0(1430)0 ]cc",
                     " B0 -> gamma rho(770)0 ",
                     " B_s0 -> gamma phi(1020) ",
                     " B_s0 -> gamma f'_2(1525) ",
                     " B_s0 -> gamma f_2(1950) ",
                     "[ Lambda_b0 -> gamma Lambda0 ]cc",
                     "[ Lambda_b0 -> gamma N(1440)0 ]cc",
                     "[ Lambda_b0 -> gamma Lambda(1520)0 ]cc" ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ _Merge, photons ] )
#################################################################################################################################
    def _makeB2Kpipipill( self, name, dilepton, kaons, pions, params ) :
        """
        Make a B0 -> J/psi(1S) K+pi-pi+pi+
        """

        _Decays = "[B0 -> J/psi(1S) K+ pi- pi+ pi-]cc"

        # define all the cuts
        # daughter cuts
        _daughtersCuts_dilepton =   "(ID=='J/psi(1S)') & "\
                                    "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(B2K3pill_LeptonIPCHI2)s) & "\
                                    "(MAXTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(B2K3pill_LeptonIPCHI2_Max)s) & "\
                                    "(MINTREE(ABSID<14,PT) > %(B2K3pill_LeptonPT)s *MeV) & "\
                                    "(PT > %(B2K3pill_DiLeptonPT)s *MeV) & "\
                                    "(MM < %(UpperMass)s *MeV) & "\
                                    "(VFASPF(VCHI2) < %(B2K3pill_DiLeptonVtxChi2)s) & (BPVVDCHI2 > %(B2K3pill_DiLeptonFDChi2)s) & "\
                                    "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params
        _daughtersCuts_K        = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) &"\
                                  "(MIPCHI2DV(PRIMARY) > %(B2K3pill_HadIPCHI2)s) & (PT > %(B2K3pill_KaonPT)s *MeV)" % params
        _daughtersCuts_pi       = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & "\
                                  "(MIPCHI2DV(PRIMARY) > %(B2K3pill_HadIPCHI2)s) & (PT > %(B2K3pill_PionPT)s *MeV)" % params



        _Comb12Cuts  = "(ACHI2DOCA(1,2) < %(B2K3pill_achi2doca)s ) & (AM < %(B2K3pill_MassWindow_Hi)s*MeV)" % params
        _Comb123Cuts = "(ACHI2DOCA(1,3) < %(B2K3pill_achi2doca)s )  & (ACHI2DOCA(2,3) < %(B2K3pill_achi2doca)s) & (AM < %(B2K3pill_MassWindow_Hi)s*MeV) & (ACHILD(PIDK,2) >ACHILD(PIDK,3))" % params
        _Comb1234Cuts = "(ACHI2DOCA(1,4) < %(B2K3pill_achi2doca)s )  & (ACHI2DOCA(2,4) < %(B2K3pill_achi2doca)s) & (ACHI2DOCA(3,4) < %(B2K3pill_achi2doca)s) & (AM < %(B2K3pill_MassWindow_Hi)s*MeV) & (ACHILD(PIDK,2) > ACHILD(PIDK,4))" % params
        _CombCuts = "(ACHI2DOCA(1,5) < %(B2K3pill_achi2doca)s )  & (ACHI2DOCA(2,5) < %(B2K3pill_achi2doca)s) & (ACHI2DOCA(3,5) < %(B2K3pill_achi2doca)s) & (ACHI2DOCA(4,5) < %(B2K3pill_achi2doca)s) & (ADAMASS('B+')< %(BMassWindow)s*MeV) & (ACHILD(PIDK,2) >ACHILD(PIDK,5))" % params

        _MotherCuts  = "(VFASPF(VCHI2) < %(B2K3pill_BVtxChi2)s) & (BPVIPCHI2() < %(B2K3pill_BIPDChi2)s) & (BPVDIRA > %(B2K3pill_BFDIRA)s) & (BPVVDCHI2 > %(B2K3pill_BFDChi2)s) " % params

        _Combine = DaVinci__N5BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "J/psi(1S)" : _daughtersCuts_dilepton,
            "K+"        : _daughtersCuts_K,
            "pi+"       : _daughtersCuts_pi }

        _Combine.Combination12Cut   = _Comb12Cuts
        _Combine.Combination123Cut  = _Comb123Cuts
        _Combine.Combination1234Cut = _Comb1234Cuts
        _Combine.CombinationCut     = _CombCuts
        _Combine.MotherCut          = _MotherCuts
        #_Combine.ReFitPVs = True

        # make and store the Selection object
        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dilepton, kaons, pions ] )

#################################################################################################################################
    def _make_ee_Loose( self, name, params ):
        """
        Make dielectrons for Loose Lb->{pK, Ksppi}ll
        """
        # same as in Bu2LLKee_Line2, w/ looser MINIPCHI2 
        _Code = "(ID=='J/psi(1S)') & "\
                "(MM < 5500 *MeV) & "\
                "(MINTREE(ABSID<14,PT) > 300 *MeV) & "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > 5) & "\
                "(VFASPF(VCHI2/VDOF) < 9) & (BPVVDCHI2 > 16) & (2 == NINTREE((ABSID==11)&(PIDe > 0)) )"

        ee_filter = FilterDesktop(Code = _Code)
        return Selection("sel_ee_Loose", Algorithm = ee_filter, RequiredSelections = [DataOnDemand(Location="Phys/StdDiElectronFromTracks/Particles")])

    def _make_mm_Loose( self, name, params ) :
        """
        Make dimuons for Loose Lb->{pK, Ksppi}ll
        """
        # Form the dimuon (B2XMuMu-like) w/ looser MINIPCHI2 
        _dauCuts_mu     = "(MIPCHI2DV(PRIMARY) > 5) & (TRGHP < 0.6) & (PIDmu> -3.0)"
        _momCuts_dimu   = "(VFASPF(VCHI2/VDOF) < 12.0) & (BPVDIRA> -0.9) & (BPVVDCHI2 > 9.0)"
        _combCut_dimu   = "(AM < 7100*MeV)"

        comb_dimuon = CombineParticles(DecayDescriptor = "[J/psi(1S) -> mu+ mu-]cc",
                                       DaughtersCuts   = {"mu+":_dauCuts_mu,"mu-":_dauCuts_mu},
                                       CombinationCut  = _combCut_dimu,
                                       MotherCut       = _momCuts_dimu)
        return Selection("sel_mm_Loose", Algorithm=comb_dimuon, RequiredSelections=[DataOnDemand(Location="Phys/StdAllLooseMuons/Particles")])

    def _make_me_Loose( self, name, params, samesign = False ) :
        """
        Make dimuons for Loose Lb->{pK, Ksppi}ll
        """
        # Form the dimuon (B2XMuMu-like) w/ looser MINIPCHI2 
        _dauCuts_mu     = "(MIPCHI2DV(PRIMARY) > 5) & (TRGHP < 0.6) & (PIDmu> -3.0)" 
        _dauCuts_e     = "(MIPCHI2DV(PRIMARY) > 5) & (PT > 300 *MeV) & (ABSID==11) & (PIDe > 0)" #overwrites particle constructor cuts
        _momCuts_mue   = "(VFASPF(VCHI2/VDOF) < 12.0) & (BPVDIRA> -0.9) & (BPVVDCHI2 > 9.0)"
        _combCut_mue   = "(AM < 7100*MeV)"

        comb_mue = CombineParticles(DecayDescriptor = "[J/psi(1S) -> mu+ e-]cc" if not samesign else "[J/psi(1S) -> mu+ e+]cc",
                                       DaughtersCuts   = {"mu+":_dauCuts_mu,"e-":_dauCuts_e},
                                       CombinationCut  = _combCut_mue,
                                       MotherCut       = _momCuts_mue)
        return Selection(name, Algorithm=comb_mue, RequiredSelections=[DataOnDemand(Location="Phys/StdAllLooseMuons/Particles"),
                                                                                    DataOnDemand(Location="Phys/StdLooseElectrons/Particles")])


    def _makeL1520forpkll( self, name, params ) :
        """
        Make a Lambda(1520)0 -> p K
        """
        # Protons/Kaons are StdNoPIDs now. 
        # IPCHI2 is >6 for B2Xmm and >9 for Bu2LLK. Loosened to > 5 
        _kaonCut         = "(MIPCHI2DV(PRIMARY) > 5) & (TRGHOSTPROB < 0.6) & (HASRICH) & (PT > 250*MeV) & (P > 2*GeV) & (PROBNNk>0.05)"
        _protonCut       = "(MIPCHI2DV(PRIMARY) > 5) & (TRGHOSTPROB < 0.6) & (HASRICH) & (PT > 300*MeV) "# & (P > 1*GeV) & (PROBNNp>0.05)"
        _combCut_L1520   = "(AM < 5000*MeV) & (ADOCACHI2CUT(30, ''))" # DOCA same as Bu2LLK. <30 for B2XMM
        _momCuts_L1520   = "(MAXTREE(ISBASIC,TRCHI2DOF) < 5) & (VFASPF(VCHI2PDOF) < 25.0) & (MM < 4200)" # same as Bu2LLK. <8 for B2XMM

        comb_L1520 = CombineParticles(DecayDescriptor = "[Lambda(1520)0 -> K- p+]cc",
                                      DaughtersCuts   = {"K-":_kaonCut,"K+":_kaonCut,"p+":_protonCut,"p~-":_protonCut},
                                      CombinationCut  = _combCut_L1520,
                                      MotherCut       = _momCuts_L1520)

        return Selection("sel_L1520", Algorithm=comb_L1520, RequiredSelections=[DataOnDemand(Location="Phys/StdAllNoPIDsKaons/Particles"),
                                                                                DataOnDemand(Location="Phys/StdAllNoPIDsProtons/Particles")])

    def _makeKsppi( self, name, params ) :
        """
        Make a L** -> Ksppi
        """
        _diTrackPreVertexCuts  = " (AM < 2500)" # m(ppi)
        _diTrackPostVertexCuts = " (HASVERTEX) & (VFASPF(VCHI2/VDOF) < 25) "
        _pionCut   = " (HASRICH) & (MIPCHI2DV(PRIMARY) > 5) & (TRGHOSTPROB < 0.6) & (PT > 250)"# & (PROBNNpi>0.05)"
        _protonCut = " (HASRICH) & (MIPCHI2DV(PRIMARY) > 5) & (TRGHOSTPROB < 0.6) & (PT > 300)"# & (PROBNNp >0.05)"
        _combineDiTrack = CombineParticles( DecayDescriptor="[N(1520)0 -> p+ pi-]cc",
                                            CombinationCut = _diTrackPreVertexCuts,
                                            MotherCut      = _diTrackPostVertexCuts,
                                            DaughtersCuts  = {"pi-":_pionCut,"pi+":_pionCut,"p+":_protonCut,"p~-":_protonCut})
        sel_ppi = Selection("ppi_comb", Algorithm=_combineDiTrack, RequiredSelections=[DataOnDemand(Location="Phys/StdAllNoPIDsPions/Particles"),
                                                                                       DataOnDemand(Location="Phys/StdAllNoPIDsProtons/Particles")])

        _TriTrackPreVertexCuts  = "( AM < 5000 )"
        _TriTrackPostVertexCuts = " (VFASPF(VCHI2/VDOF) < 30) & (MM < 4200)" 

        _combineTriTrack = CombineParticles( DecayDescriptor ="[Lambda(1820)0 -> N(1520)0 KS0]cc",
                                             CombinationCut  = _TriTrackPreVertexCuts,
                                             MotherCut       = _TriTrackPostVertexCuts )
        merged_Ks = MergedSelection(name,RequiredSelections = [ DataOnDemand(Location="Phys/StdVeryLooseKsLL/Particles"),DataOnDemand(Location="Phys/StdLooseKsDD/Particles")])

        return Selection("sel_Ksppi", Algorithm = _combineTriTrack, RequiredSelections = [ merged_Ks, sel_ppi ] )

    def _makeLb2pkmm( self, name, l1520, dimuons, params ) :
        """
        Make the Lb -> J/psi(1s) Lambda(1520)0
        """
        combB_Lb2pKmm  = CombineParticles(DecayDescriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc",
                                          CombinationCut  = "in_range(4000,AM,7100)",
                                          MotherCut       = "(in_range(4000,M,7100)) &  (VFASPF(VCHI2PDOF) < 30) & (PT>200*MeV)")
        sel_Lb2pKmm_init = Selection("selection_Lb2pKmm_init",
                                     Algorithm=combB_Lb2pKmm,
                                     RequiredSelections=[dimuons,l1520])
        sel_Lb2pKmm_filter = FilterDesktop(Code = "(in_range(5000,mB,6500)) & (dtf_signif > 3.5) & (dtf_prob > 1.0E-6) & (mpK<4*GeV)")
        sel_Lb2pKmm_filter.Preambulo = [
                                         "dtf_prob   = DTF_PROB(True)", 
                                         "dtf_signif = DTF_CTAUSIGNIFICANCE(0, True)",
                                         "mB         = DTF_FUN(M, True)",
                                         "mpK        = DTF_FUN(CHILD(2, M),  True)"
                                       ]
        return Selection("sel_Lb2pKmm", Algorithm = sel_Lb2pKmm_filter, RequiredSelections = [sel_Lb2pKmm_init])

    def _makeLb2pkme( self, name, l1520, mue, params ) :
        """
        Make the Lb -> J/psi(1s) Lambda(1520)0
        """
        combB_Lb2pKme  = CombineParticles(DecayDescriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc",
                                          CombinationCut  = "in_range(4000,AM,7100)",
                                          MotherCut       = "(in_range(4000,M,7100)) &  (VFASPF(VCHI2PDOF) < 30) & (PT>200*MeV)")
        sel_Lb2pKme_init = Selection("selection_Lb2pKmeSS_init" if "SS" in name else "selection_Lb2pKme_init",
                                     Algorithm=combB_Lb2pKme,
                                     RequiredSelections=[mue,l1520])
        sel_Lb2pKme_filter = FilterDesktop(Code = "(in_range(5000,mB,6500)) & (dtf_signif > 3.5) & (dtf_prob > 1.0E-6) & (mpK<4*GeV)")
        sel_Lb2pKme_filter.Preambulo = [
                                         "dtf_prob   = DTF_PROB(True)", 
                                         "dtf_signif = DTF_CTAUSIGNIFICANCE(0, True)",
                                         "mB         = DTF_FUN(M, True)",
                                         "mpK        = DTF_FUN(CHILD(2, M),  True)"
                                       ]
        return Selection("sel_Lb2pKmeSS" if "SS" in name else "sel_Lb2pKme",
                          Algorithm = sel_Lb2pKme_filter, RequiredSelections = [sel_Lb2pKme_init])


    def _makeLb2pkee( self, name, l1520, dilepton, params ) :
        """
        Make the Lb -> J/psi(1S) Lambda(1520)0
        """
        combB_Lb2pKee  = CombineParticles(DecayDescriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc",
                                          CombinationCut  = "in_range(3800,AM,7000)",
                                          MotherCut       = "(in_range(4500,M,6700)) & ((VFASPF(VCHI2/VDOF) < 9) & (BPVIPCHI2() < 30) & (BPVDIRA > 0.9995) & (BPVVDCHI2 > 100))")

        return Selection("sel_Lb2pKee", Algorithm=combB_Lb2pKee,RequiredSelections=[dilepton, l1520])


    def _makeLb2Ksppimm( self, name, l1820, dimuons, params ) :
        """
        Make the Lb -> J/psi(1S) Lambda(1820)0
        """
        combB_LbKsppimm  = CombineParticles(DecayDescriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1820)0]cc",
                                              CombinationCut  = "in_range(4000,AM,7100)",
                                              MotherCut       = "(in_range(4000,M,7100)) &  (VFASPF(VCHI2PDOF) < 30) & (PT>200*MeV)")
        sel_LbKsppimm_init = Selection("selection_LbKsppimm_init",
                                     Algorithm=combB_LbKsppimm,
                                     RequiredSelections=[dimuons,l1820])
        sel_LbKsppimm_filter = FilterDesktop(Code = "(in_range(5000,mB,6400)) & (dtf_signif > 3) & (dtf_prob > 1.0E-6) & (mKsppi<4*GeV)")
        sel_LbKsppimm_filter.Preambulo = [
                                         "dtf_prob   = DTF_PROB(True)", # DTF-based, with PV constraints.
                                         "dtf_signif = DTF_CTAUSIGNIFICANCE(0, True)",
                                         "mB         = DTF_FUN(M, True)",
                                         "mKsppi     = DTF_FUN(CHILD(2, M),  True)"
                                       ]
        return Selection("sel_LbKsppimm", Algorithm = sel_LbKsppimm_filter, RequiredSelections = [sel_LbKsppimm_init])

    def _makeLb2Ksppiee( self, name, l1820, dilepton, params ) :
        """
        Make the Lb -> J/psi(1S) Lambda(1820)0
        """
        combB_LbKsppiee  = CombineParticles(DecayDescriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1820)0]cc",
                                            CombinationCut  = "in_range(3800,AM,7000)",
                                            MotherCut       = "(in_range(4500,M,6700)) & ((VFASPF(VCHI2/VDOF) < 9) & (BPVIPCHI2() < 25) & (BPVDIRA > 0.9995) & (BPVVDCHI2 > 100))")
        return Selection("sel_LbKsppiee", Algorithm=combB_LbKsppiee,RequiredSelections=[dilepton, l1820])


#################################################################################################################################
    def _storeInputBremPhotons( self, linename, outputlocprefix = "Leptonic/Phys/", photonloc = "Phys/StdVeryLooseAllPhotons/Particles" ):
        from PhysSelPython.Wrappers import FilterSelection, DataOnDemand, SelectionSequence
        from Configurables import ( CopyParticles, ParticleCloner,
                                    ProtoParticleCloner, CaloHypoCloner,
                                    CaloClusterCloner )

        cloner = CopyParticles( name = "BremPhotonClonerFor%s"%linename )
        cloner.OutputPrefix = outputlocprefix+linename
        cloner.InputLocation = photonloc

        cloner.addTool(ParticleCloner, name="ParticleCloner")
        cloner.ParticleCloner.ICloneProtoParticle = "ProtoParticleCloner"

        cloner.addTool(ProtoParticleCloner,name="ProtoParticleCloner")

        cloner.addTool(CaloHypoCloner,name="CaloHypoCloner")
        cloner.CaloHypoCloner.CloneClustersNeuP = True
        cloner.CaloHypoCloner.CloneDigitsNeuP   = True

        cloner.addTool(CaloClusterCloner,name="CaloClusterCloner")
        cloner.CaloClusterCloner.CloneEntriesNeuP = True

        ############################################################
        makePhotons = GaudiSequencer("For%sBremPhotonMakerAlg"%linename)
        makePhotons.Members += [cloner]

        return makePhotons
#####################################################
