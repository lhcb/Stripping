###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = [' A. Venkateswaran']
__date__ = '17/07/2023'
__version__ = '$Revision: 0.2$'

from GaudiKernel.SystemOfUnits import MeV
from GaudiKernel.SystemOfUnits import mm

from StandardParticles import StdNoPIDsKaons, StdNoPIDsPions, StdLoosePions

from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection, CombineSelection
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles

__all__ = (
    'default_config',
    'B2STauTauInclusiveConf'
)

default_config = {
    'NAME': 'B2STauTauInclusive',
    'BUILDERTYPE': 'B2STauTauInclusiveConf',
    'WGs': ['RD'],
    'CONFIG': {
        'SpdMult': '600',
        #
        'UsePID': True,
        #
        'FD_B_Max': 100 * mm,
        'PT_B': 2000 * MeV,
        'P_B': 10000 * MeV,
        'FDCHI2_B': 16,
        'MASS_LOW_B': 1000 * MeV,
        'MASS_HIGH_B': 8000 * MeV,
        'VCHI2_B': 150,
        'M_TAU_MIN': 600 * MeV,  # mod from 500 MeV
        'TAU_VCHI2_MAX': 9,  # mod from 16
        #
        'IPCHI2_Tr': 16,
        'TRGHOPROB_Tr': 0.3,
        'TRACKCHI2DOF_Tr': 3,

        # DDK cuts
        'DDK_TRK_PT_MIN': 250 * MeV,
        'DDK_TRK_P_MIN': 2000 * MeV,
        'DDK_TRK_IPCHI2_MIN': 16.0,
        'DDK_TRK_CHI2DOF_MAX': 4,
        'DDK_TRK_TRGHOSTPROB_MAX': 0.4,
        'DDK_TRK_PROBNNPI_MIN': 0.55,
        'DDK_COMB_APT_MIN': 800 * MeV,
        'DDK_COMB_AM_MIN': 400 * MeV,
        'DDK_COMB_AM_MAX': 2100 * MeV,
        'DDK_COMB_AMAXDOCA_MAX': 0.2 * mm,
        'DDK_COMB_ANUMPT_CUTMIN': 800 * MeV,
        'DDK_COMB_ANUMPT_MINNUM': 1,
        'DDK_MOTHER_PT_MIN': 1000 * MeV,
        'DDK_MOTHER_M_MIN': 600 * MeV,  # mod from 500 MeV
        'DDK_MOTHER_M_MAX': 2000 * MeV,
        'DDK_MOTHER_DIRA_MIN': 0.99,
        'DDK_MOTHER_VCHI2_MAX': 9,  # mod from 16
        'DDK_MOTHER_VDCHI2_MIN': 16,
        'DDK_MOTHER_VDRHO_MIN': 0.1 * mm,
        'DDK_MOTHER_VDRHO_MAX': 7.0 * mm,
        'DDK_MOTHER_VDZ_MIN': 5.0 * mm,
        #
        'B2STauTauInclusive_LinePrescale': 1,
        'B2STauTauInclusive_LinePostscale': 1,
        'RelInfoTools': [{"Type": "RelInfoVertexIsolation",
                          "Location": "BVars_VertexIsoInfo",
                          "IgnoreUnmatchedDescriptors": True,
                          "DaughterLocations": {"[X0 -> ^Hadron Hadron  tau+  tau-]CC": "H1_VertexIsoInfo",
                                                "[X0 -> Hadron ^Hadron  tau+  tau-]CC": "H2_VertexIsoInfo",
                                                "[X0 ->  Hadron Hadron ^tau+  tau-]CC": "Taup_VertexIsoInfo",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau-]CC": "Taum_VertexIsoInfo",
                                                "[X+ -> ^Hadron tau+  tau-]CC": "H1_VertexIsoInfo",
                                                "[X+ ->  Hadron ^tau+ tau-]CC": "Taup_VertexIsoInfo",
                                                "[X+ ->  Hadron tau+ ^tau-]CC": "Taum_VertexIsoInfo",
                                                #SS
                                                "[X0 -> ^Hadron Hadron  tau+  tau+]CC": "H1_VertexIsoInfo",
                                                "[X0 -> Hadron ^Hadron  tau+  tau+]CC": "H2_VertexIsoInfo",
                                                "[X0 ->  Hadron Hadron ^tau+  tau+]CC": "Taup_VertexIsoInfo",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau+]CC": "Taum_VertexIsoInfo",
                                                "[X+ -> ^Hadron tau+  tau+]CC": "H1_VertexIsoInfo",
                                                "[X+ ->  Hadron ^tau+ tau+]CC": "Taup_VertexIsoInfo",
                                                "[X+ ->  Hadron tau+ ^tau+]CC": "Taum_VertexIsoInfo",
                                                #DDK
                                                "[X0 -> ^Hadron Hadron  D+  D-]CC": "H1_VertexIsoInfo",
                                                "[X0 -> Hadron ^Hadron  D+  D-]CC": "H2_VertexIsoInfo",
                                                "[X0 ->  Hadron Hadron ^D+  D-]CC": "Taup_VertexIsoInfo",
                                                "[X0 ->  Hadron  Hadron D+ ^D-]CC": "Taum_VertexIsoInfo",
                                                "[X+ -> ^Hadron D+  D-]CC": "H1_VertexIsoInfo",
                                                "[X+ ->  Hadron ^D+ D-]CC": "Taup_VertexIsoInfo",
                                                "[X+ ->  Hadron D+ ^D-]CC": "Taum_VertexIsoInfo",
                                                #DDK SS
                                                "[X0 -> ^Hadron Hadron  D+  D+]CC": "H1_VertexIsoInfo",
                                                "[X0 -> Hadron ^Hadron  D+  D+]CC": "H2_VertexIsoInfo",
                                                "[X0 ->  Hadron Hadron ^D+  D+]CC": "Taup_VertexIsoInfo",
                                                "[X0 ->  Hadron  Hadron D+ ^D+]CC": "Taum_VertexIsoInfo",
                                                "[X+ -> ^Hadron D+  D+]CC": "H1_VertexIsoInfo",
                                                "[X+ ->  Hadron ^D+ D+]CC": "Taup_VertexIsoInfo",
                                                "[X+ ->  Hadron D+ ^D+]CC": "Taum_VertexIsoInfo",
                                                }},
                         {"Type": "RelInfoConeIsolation",
                          "ConeSize": 0.5,
                          "Variables": [],
                          "Location": "BVars_ConeIsoInfo_Cone05",
                          "IgnoreUnmatchedDescriptors": True,
                          "DaughterLocations": {"[X0 -> ^Hadron Hadron  tau+  tau-]CC": "H1_ConeIsoInfo_Cone05",
                                                "[X0 -> Hadron ^Hadron  tau+  tau-]CC": "H2_ConeIsoInfo_Cone05",
                                                "[X0 ->  Hadron Hadron ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone05",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone05",
                                                "[X+ -> ^Hadron tau+  tau-]CC": "H1_ConeIsoInfo_Cone05",
                                                "[X+ ->  Hadron ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone05",
                                                "[X+ ->  Hadron tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone05",
                                                #SS
                                                "[X0 -> ^Hadron Hadron  tau+  tau+]CC": "H1_ConeIsoInfo_Cone05",
                                                "[X0 -> Hadron ^Hadron  tau+  tau+]CC": "H2_ConeIsoInfo_Cone05",
                                                "[X0 ->  Hadron Hadron ^tau+  tau+]CC": "Taup_ConeIsoInfo_Cone05",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau+]CC": "Taum_ConeIsoInfo_Cone05",
                                                "[X+ -> ^Hadron tau+  tau+]CC": "H1_ConeIsoInfo_Cone05",
                                                "[X+ ->  Hadron ^tau+  tau+]CC": "Taup_ConeIsoInfo_Cone05",
                                                "[X+ ->  Hadron tau+ ^tau+]CC": "Taum_ConeIsoInfo_Cone05",
                                                #DDK
                                                "[X0 -> ^Hadron Hadron  D+  D-]CC": "H1_ConeIsoInfo_Cone05",
                                                "[X0 -> Hadron ^Hadron  D+  D-]CC": "H2_ConeIsoInfo_Cone05",
                                                "[X0 ->  Hadron Hadron ^D+  D-]CC": "Taup_ConeIsoInfo_Cone05",
                                                "[X0 ->  Hadron  Hadron D+ ^D-]CC": "Taum_ConeIsoInfo_Cone05",
                                                "[X+ -> ^Hadron D+  D-]CC": "H1_ConeIsoInfo_Cone05",
                                                "[X+ ->  Hadron ^D+  D-]CC": "Taup_ConeIsoInfo_Cone05",
                                                "[X+ ->  Hadron D+ ^D-]CC": "Taum_ConeIsoInfo_Cone05",
                                                #DDK SS
                                                "[X0 -> ^Hadron Hadron  D+  D+]CC": "H1_ConeIsoInfo_Cone05",
                                                "[X0 -> Hadron ^Hadron  D+  D+]CC": "H2_ConeIsoInfo_Cone05",
                                                "[X0 ->  Hadron Hadron ^D+  D+]CC": "Taup_ConeIsoInfo_Cone05",
                                                "[X0 ->  Hadron  Hadron D+ ^D+]CC": "Taum_ConeIsoInfo_Cone05",
                                                "[X+ -> ^Hadron D+  D+]CC": "H1_ConeIsoInfo_Cone05",
                                                "[X+ ->  Hadron ^D+  D+]CC": "Taup_ConeIsoInfo_Cone05",
                                                "[X+ ->  Hadron D+ ^D+]CC": "Taum_ConeIsoInfo_Cone05",
                                                }},
                         {"Type": "RelInfoConeIsolation",
                          "ConeSize": 1.0,
                          "Variables": [],
                          "Location": "BVars_ConeIsoInfo_Cone10",
                          "IgnoreUnmatchedDescriptors": True,
                          "DaughterLocations": {"[X0 -> ^Hadron Hadron  tau+  tau-]CC": "H1_ConeIsoInfo_Cone10",
                                                "[X0 -> Hadron ^Hadron  tau+  tau-]CC": "H2_ConeIsoInfo_Cone10",
                                                "[X0 ->  Hadron Hadron ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone10",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone10",
                                                "[X+ -> ^Hadron tau+  tau-]CC": "H1_ConeIsoInfo_Cone10",
                                                "[X+ ->  Hadron ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone10",
                                                "[X+ ->  Hadron tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone10",
                                                #SS
                                                "[X0 -> ^Hadron Hadron  tau+  tau+]CC": "H1_ConeIsoInfo_Cone10",
                                                "[X0 -> Hadron ^Hadron  tau+  tau+]CC": "H2_ConeIsoInfo_Cone10",
                                                "[X0 ->  Hadron Hadron ^tau+  tau+]CC": "Taup_ConeIsoInfo_Cone10",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau+]CC": "Taum_ConeIsoInfo_Cone10",
                                                "[X+ -> ^Hadron tau+  tau+]CC": "H1_ConeIsoInfo_Cone10",
                                                "[X+ ->  Hadron ^tau+  tau+]CC": "Taup_ConeIsoInfo_Cone10",
                                                "[X+ ->  Hadron tau+ ^tau+]CC": "Taum_ConeIsoInfo_Cone10",
                                                #DDK
                                                "[X0 -> ^Hadron Hadron  D+  D-]CC": "H1_ConeIsoInfo_Cone10",
                                                "[X0 -> Hadron ^Hadron  D+  D-]CC": "H2_ConeIsoInfo_Cone10",
                                                "[X0 ->  Hadron Hadron ^D+  D-]CC": "Taup_ConeIsoInfo_Cone10",
                                                "[X0 ->  Hadron  Hadron D+ ^D-]CC": "Taum_ConeIsoInfo_Cone10",
                                                "[X+ -> ^Hadron D+  D-]CC": "H1_ConeIsoInfo_Cone10",
                                                "[X+ ->  Hadron ^D+  D-]CC": "Taup_ConeIsoInfo_Cone10",
                                                "[X+ ->  Hadron D+ ^D-]CC": "Taum_ConeIsoInfo_Cone10",
                                                #DDK SS
                                                "[X0 -> ^Hadron Hadron  D+  D+]CC": "H1_ConeIsoInfo_Cone10",
                                                "[X0 -> Hadron ^Hadron  D+  D+]CC": "H2_ConeIsoInfo_Cone10",
                                                "[X0 ->  Hadron Hadron ^D+  D+]CC": "Taup_ConeIsoInfo_Cone10",
                                                "[X0 ->  Hadron  Hadron D+ ^D+]CC": "Taum_ConeIsoInfo_Cone10",
                                                "[X+ -> ^Hadron D+  D+]CC": "H1_ConeIsoInfo_Cone10",
                                                "[X+ ->  Hadron ^D+  D+]CC": "Taup_ConeIsoInfo_Cone10",
                                                "[X+ ->  Hadron D+ ^D+]CC": "Taum_ConeIsoInfo_Cone10",
                                                }
                          },
                         {"Type": "RelInfoConeIsolation",
                          "ConeSize": 1.5,
                          "Variables": [],
                          "Location": "BVars_ConeIsoInfo_Cone15",
                          "IgnoreUnmatchedDescriptors": True,
                          "DaughterLocations": {"[X0 -> ^Hadron Hadron  tau+  tau-]CC": "H1_ConeIsoInfo_15",
                                                "[X0 -> Hadron ^Hadron  tau+  tau-]CC": "H2_ConeIsoInfo_15",
                                                "[X0 ->  Hadron Hadron ^tau+  tau-]CC": "Taup_ConeIsoInfo_15",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau-]CC": "Taum_ConeIsoInfo_15",
                                                "[X+ -> ^Hadron tau+  tau-]CC": "H1_ConeIsoInfo_15",
                                                "[X+ ->  Hadron ^tau+  tau-]CC": "Taup_ConeIsoInfo_15",
                                                "[X+ ->  Hadron tau+ ^tau-]CC": "Taum_ConeIsoInfo_15",
                                                #SS
                                                "[X0 -> ^Hadron Hadron  tau+  tau+]CC": "H1_ConeIsoInfo_15",
                                                "[X0 -> Hadron ^Hadron  tau+  tau+]CC": "H2_ConeIsoInfo_15",
                                                "[X0 ->  Hadron Hadron ^tau+  tau+]CC": "Taup_ConeIsoInfo_15",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau+]CC": "Taum_ConeIsoInfo_15",
                                                "[X+ -> ^Hadron tau+  tau+]CC": "H1_ConeIsoInfo_15",
                                                "[X+ ->  Hadron ^tau+  tau+]CC": "Taup_ConeIsoInfo_15",
                                                "[X+ ->  Hadron tau+ ^tau+]CC": "Taum_ConeIsoInfo_15",
                                                #DDK
                                                "[X0 -> ^Hadron Hadron  D+  D-]CC": "H1_ConeIsoInfo_15",
                                                "[X0 -> Hadron ^Hadron  D+  D-]CC": "H2_ConeIsoInfo_15",
                                                "[X0 ->  Hadron Hadron ^D+  D-]CC": "Taup_ConeIsoInfo_15",
                                                "[X0 ->  Hadron  Hadron D+ ^D-]CC": "Taum_ConeIsoInfo_15",
                                                "[X+ -> ^Hadron D+  D-]CC": "H1_ConeIsoInfo_15",
                                                "[X+ ->  Hadron ^D+  D-]CC": "Taup_ConeIsoInfo_15",
                                                "[X+ ->  Hadron D+ ^D-]CC": "Taum_ConeIsoInfo_15",
                                                #DDK SS
                                                "[X0 -> ^Hadron Hadron  D+  D+]CC": "H1_ConeIsoInfo_15",
                                                "[X0 -> Hadron ^Hadron  D+  D+]CC": "H2_ConeIsoInfo_15",
                                                "[X0 ->  Hadron Hadron ^D+  D+]CC": "Taup_ConeIsoInfo_15",
                                                "[X0 ->  Hadron  Hadron D+ ^D+]CC": "Taum_ConeIsoInfo_15",
                                                "[X+ -> ^Hadron D+  D+]CC": "H1_ConeIsoInfo_15",
                                                "[X+ ->  Hadron ^D+  D+]CC": "Taup_ConeIsoInfo_15",
                                                "[X+ ->  Hadron D+ ^D+]CC": "Taum_ConeIsoInfo_15",
                                                }
                          },
                         {"Type": "RelInfoConeIsolation",
                          "ConeSize": 2.0,
                          "Variables": [],
                          "Location": "BVars_ConeIsoInfo_Cone20",
                          "IgnoreUnmatchedDescriptors": True,
                          "DaughterLocations": {"[X0 -> ^Hadron Hadron  tau+  tau-]CC": "H1_ConeIsoInfo_Cone20",
                                                "[X0 -> Hadron ^Hadron  tau+  tau-]CC": "H2_ConeIsoInfo_Cone20",
                                                "[X0 ->  Hadron Hadron ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone20",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone20",
                                                "[X+ -> ^Hadron tau+  tau-]CC": "H1_ConeIsoInfo_Cone20",
                                                "[X+ ->  Hadron ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone20",
                                                "[X+ ->  Hadron tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone20",
                                                #SS
                                                "[X0 -> ^Hadron Hadron  tau+  tau+]CC": "H1_ConeIsoInfo_Cone20",
                                                "[X0 -> Hadron ^Hadron  tau+  tau+]CC": "H2_ConeIsoInfo_Cone20",
                                                "[X0 ->  Hadron Hadron ^tau+  tau+]CC": "Taup_ConeIsoInfo_Cone20",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau+]CC": "Taum_ConeIsoInfo_Cone20",
                                                "[X+ -> ^Hadron tau+  tau+]CC": "H1_ConeIsoInfo_Cone20",
                                                "[X+ ->  Hadron ^tau+  tau+]CC": "Taup_ConeIsoInfo_Cone20",
                                                "[X+ ->  Hadron tau+ ^tau+]CC": "Taum_ConeIsoInfo_Cone20",
                                                #DDK
                                                "[X0 -> ^Hadron Hadron  D+  D-]CC": "H1_ConeIsoInfo_Cone20",
                                                "[X0 -> Hadron ^Hadron  D+  D-]CC": "H2_ConeIsoInfo_Cone20",
                                                "[X0 ->  Hadron Hadron ^D+  D-]CC": "Taup_ConeIsoInfo_Cone20",
                                                "[X0 ->  Hadron  Hadron D+ ^D-]CC": "Taum_ConeIsoInfo_Cone20",
                                                "[X+ -> ^Hadron D+  D-]CC": "H1_ConeIsoInfo_Cone20",
                                                "[X+ ->  Hadron ^D+  D-]CC": "Taup_ConeIsoInfo_Cone20",
                                                "[X+ ->  Hadron D+ ^D-]CC": "Taum_ConeIsoInfo_Cone20",
                                                #DDK SS
                                                "[X0 -> ^Hadron Hadron  D+  D+]CC": "H1_ConeIsoInfo_Cone20",
                                                "[X0 -> Hadron ^Hadron  D+  D+]CC": "H2_ConeIsoInfo_Cone20",
                                                "[X0 ->  Hadron Hadron ^D+  D+]CC": "Taup_ConeIsoInfo_Cone20",
                                                "[X0 ->  Hadron  Hadron D+ ^D+]CC": "Taum_ConeIsoInfo_Cone20",
                                                "[X+ -> ^Hadron D+  D+]CC": "H1_ConeIsoInfo_Cone20",
                                                "[X+ ->  Hadron ^D+  D+]CC": "Taup_ConeIsoInfo_Cone20",
                                                "[X+ ->  Hadron D+ ^D+]CC": "Taum_ConeIsoInfo_Cone20",
                                                }
                          },
                         {'Type': 'RelInfoVertexIsolationBDT',
                          'Location': 'BVars_VertexIsoBDTInfo',
                          "IgnoreUnmatchedDescriptors": True,
                          'DaughterLocations': {"[X0 -> ^Hadron Hadron  tau+  tau-]CC": "H1_VertexIsoBDTInfo",
                                                "[X0 -> Hadron ^Hadron  tau+  tau-]CC": "H2_VertexIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron ^tau+  tau-]CC": "Taup_VertexIsoBDTInfo",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau-]CC": "Taum_VertexIsoBDTInfo",
                                                "[X+ -> ^Hadron tau+  tau-]CC": "H1_VertexIsoBDTInfo",
                                                "[X+ ->  Hadron ^tau+  tau-]CC": "Taup_VertexIsoBDTInfo",
                                                "[X+ ->  Hadron tau+ ^tau-]CC": "Taum_VertexIsoBDTInfo",
                                                #SS
                                                "[X0 -> ^Hadron Hadron  tau+  tau+]CC": "H1_VertexIsoBDTInfo",
                                                "[X0 -> Hadron ^Hadron  tau+  tau+]CC": "H2_VertexIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron ^tau+  tau+]CC": "Taup_VertexIsoBDTInfo",
                                                "[X0 ->  Hadron  Hadron tau+ ^tau+]CC": "Taum_VertexIsoBDTInfo",
                                                "[X+ -> ^Hadron tau+  tau+]CC": "H1_VertexIsoBDTInfo",
                                                "[X+ ->  Hadron ^tau+  tau+]CC": "Taup_VertexIsoBDTInfo",
                                                "[X+ ->  Hadron tau+ ^tau+]CC": "Taum_VertexIsoBDTInfo",
                                                #DDK
                                                "[X0 -> ^Hadron Hadron  D+  D-]CC": "H1_VertexIsoBDTInfo",
                                                "[X0 -> Hadron ^Hadron  D+  D-]CC": "H2_VertexIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron ^D+  D-]CC": "Taup_VertexIsoBDTInfo",
                                                "[X0 ->  Hadron  Hadron D+ ^D-]CC": "Taum_VertexIsoBDTInfo",
                                                "[X+ -> ^Hadron D+  D-]CC": "H1_VertexIsoBDTInfo",
                                                "[X+ ->  Hadron ^D+  D-]CC": "Taup_VertexIsoBDTInfo",
                                                "[X+ ->  Hadron D+ ^D-]CC": "Taum_VertexIsoBDTInfo",
                                                #DDK SS
                                                "[X0 -> ^Hadron Hadron  D+  D+]CC": "H1_VertexIsoBDTInfo",
                                                "[X0 -> Hadron ^Hadron  D+  D+]CC": "H2_VertexIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron ^D+  D+]CC": "Taup_VertexIsoBDTInfo",
                                                "[X0 ->  Hadron  Hadron D+ ^D+]CC": "Taum_VertexIsoBDTInfo",
                                                "[X+ -> ^Hadron D+  D+]CC": "H1_VertexIsoBDTInfo",
                                                "[X+ ->  Hadron ^D+  D+]CC": "Taup_VertexIsoBDTInfo",
                                                "[X+ ->  Hadron D+ ^D+]CC": "Taum_VertexIsoBDTInfo",
                                                }
                          },
                         {'Type': 'RelInfoTrackIsolationBDT',
                          'Variables': 2,
                          'WeightsFile':  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
                          "IgnoreUnmatchedDescriptors": True,
                          'DaughterLocations': {"[X0 -> ^Hadron Hadron  tau+  tau-]CC": "H1_TrackIsoBDTInfo",
                                                "[X0 -> Hadron ^Hadron  tau+  tau-]CC": "H2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (tau+ -> ^X+ X- X+) tau-]CC": "Taup_pi1_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (tau+ -> X+ ^X- X+) tau-]CC": "Taup_pi2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (tau+ -> X+ X- ^X+) tau-]CC": "Taup_pi3_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron tau+ (tau- -> ^X- X+ X-)]CC": "Taum_pi1_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron tau+ (tau- -> X- ^X+ X-)]CC": "Taum_pi2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron tau+ (tau- -> X- X+ ^X-)]CC": "Taum_pi3_TrackIsoBDTInfo",
                                                "[X+ -> ^Hadron  tau+  tau-]CC": "H1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (tau+ -> ^X+ X- X+) tau-]CC": "Taup_pi1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (tau+ -> X+ ^X- X+) tau-]CC": "Taup_pi2_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (tau+ -> X+ X- ^X+) tau-]CC": "Taup_pi3_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron tau+ (tau- -> ^X- X+ X-)]CC": "Taum_pi1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron tau+ (tau- -> X- ^X+ X-)]CC": "Taum_pi2_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron tau+ (tau- -> X- X+ ^X-)]CC": "Taum_pi3_TrackIsoBDTInfo",
                                                #SS
                                                "[X0 -> ^Hadron Hadron  tau+  tau+]CC": "H1_TrackIsoBDTInfo",
                                                "[X0 -> Hadron ^Hadron  tau+  tau+]CC": "H2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (tau+ -> ^X+ X- X+) tau+]CC": "Taup_pi1_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (tau+ -> X+ ^X- X+) tau+]CC": "Taup_pi2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (tau+ -> X+ X- ^X+) tau+]CC": "Taup_pi3_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron tau+ (tau+ -> ^X+ X- X+)]CC": "Taum_pi1_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron tau+ (tau+ -> X+ ^X- X+)]CC": "Taum_pi2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron tau+ (tau+ -> X+ X- ^X+)]CC": "Taum_pi3_TrackIsoBDTInfo",
                                                "[X+ -> ^Hadron  tau+  tau+]CC": "H1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (tau+ -> ^X+ X- X+) tau-]CC": "Taup_pi1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (tau+ -> X+ ^X- X+) tau-]CC": "Taup_pi2_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (tau+ -> X+ X- ^X+) tau-]CC": "Taup_pi3_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron tau+ (tau+ -> ^X+ X- X+)]CC": "Taum_pi1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron tau+ (tau+ -> X+ ^X- X+)]CC": "Taum_pi2_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron tau+ (tau+ -> X+ X- ^X+)]CC": "Taum_pi3_TrackIsoBDTInfo",
                                                #DDK
                                                "[X0 -> ^Hadron Hadron  D+  D-]CC": "H1_TrackIsoBDTInfo",
                                                "[X0 -> Hadron ^Hadron  D+  D-]CC": "H2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (D+ -> ^X+ X- X+) D-]CC": "Taup_pi1_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (D+ -> X+ ^X- X+) D-]CC": "Taup_pi2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (D+ -> X+ X- ^X+) D-]CC": "Taup_pi3_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron D+ (D- -> ^X- X+ X-)]CC": "Taum_pi1_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron D+ (D- -> X- ^X+ X-)]CC": "Taum_pi2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron D+ (D- -> X- X+ ^X-)]CC": "Taum_pi3_TrackIsoBDTInfo",
                                                "[X+ -> ^Hadron  D+  D-]CC": "H1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (D+ -> ^X+ X- X+) D-]CC": "Taup_pi1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (D+ -> X+ ^X- X+) D-]CC": "Taup_pi2_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (D+ -> X+ X- ^X+) D-]CC": "Taup_pi3_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron D+ (D- -> ^X- X+ X-)]CC": "Taum_pi1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron D+ (D- -> X- ^X+ X-)]CC": "Taum_pi2_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron D+ (D- -> X- X+ ^X-)]CC": "Taum_pi3_TrackIsoBDTInfo",
                                                #DDK SS
                                                "[X0 -> ^Hadron Hadron  D+  D+]CC": "H1_TrackIsoBDTInfo",
                                                "[X0 -> Hadron ^Hadron  D+  D+]CC": "H2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (D+ -> ^X+ X- X+) D+]CC": "Taup_pi1_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (D+ -> X+ ^X- X+) D+]CC": "Taup_pi2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron (D+ -> X+ X- ^X+) D+]CC": "Taup_pi3_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron D+ (D+ -> ^X+ X- X+)]CC": "Taum_pi1_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron D+ (D+ -> X+ ^X- X+)]CC": "Taum_pi2_TrackIsoBDTInfo",
                                                "[X0 ->  Hadron Hadron D+ (D+ -> X+ X- ^X+)]CC": "Taum_pi3_TrackIsoBDTInfo",
                                                "[X+ -> ^Hadron  D+  D+]CC": "H1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (D+ -> ^X+ X- X+) D+]CC": "Taup_pi1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (D+ -> X+ ^X- X+) D+]CC": "Taup_pi2_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron (D+ -> X+ X- ^X+) D+]CC": "Taup_pi3_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron D+ (D+ -> ^X+ X- X+)]CC": "Taum_pi1_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron D+ (D+ -> X+ ^X- X+)]CC": "Taum_pi2_TrackIsoBDTInfo",
                                                "[X+ ->  Hadron D+ (D+ -> X+ X- ^X+)]CC": "Taum_pi3_TrackIsoBDTInfo",
                                                }},
                         {"Type": "RelInfoBKsttautauTauIsolationBDT",
                          "Location": "B2KstTauTau_TauIsolationBDT"
                          },
                         ]
    },
    'STREAMS': ['Bhadron']
}


class B2STauTauInclusiveConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.FilterSPD = {
            'Code':
            " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )"
            % config,
            'Preambulo': [
                "from LoKiNumbers.decorators import *",
                "from LoKiCore.basic import LHCb"
            ]
        }

        trackCuts = "(TRCHI2DOF < %(TRACKCHI2DOF_Tr)s) & (MIPCHI2DV(PRIMARY) > %(IPCHI2_Tr)s) & (TRGHOSTPROB < %(TRGHOPROB_Tr)s)" % config

        self.selKaons = SimpleSelection(
            "Kaons_" + name, FilterDesktop, [StdNoPIDsKaons], Code=trackCuts)

        self.selPions = SimpleSelection(
            "Pions_" + name, FilterDesktop, [StdNoPIDsPions], Code=trackCuts)

        self.rawTau = DataOnDemand("Phys/StdTightDetachedTau3pi/Particles")

        self.selTau = SimpleSelection(
            "SelTau_" + name, FilterDesktop, [self.rawTau], Code="(M > %(M_TAU_MIN)s) & (VFASPF(VCHI2) < %(TAU_VCHI2_MAX)s)" % config)

        self.selTau_DDK = CombineSelection(
            'Combine_Tau3pi_DDK_B2STauTauIncl',
            [StdNoPIDsKaons, StdLoosePions],
            DecayDescriptor='[D+ -> K- pi+ pi+]cc',
            DaughtersCuts={
                'pi+': '(PT > %(DDK_TRK_PT_MIN)s) & (P > %(DDK_TRK_P_MIN)s) & (MIPCHI2DV(PRIMARY) > %(DDK_TRK_IPCHI2_MIN)s) & (TRCHI2DOF < %(DDK_TRK_CHI2DOF_MAX)s) & (TRGHOSTPROB < %(DDK_TRK_TRGHOSTPROB_MAX)s) & (PROBNNpi > %(DDK_TRK_PROBNNPI_MIN)s)' % config,
                'pi-': '(PT > %(DDK_TRK_PT_MIN)s) & (P > %(DDK_TRK_P_MIN)s) & (MIPCHI2DV(PRIMARY) > %(DDK_TRK_IPCHI2_MIN)s) & (TRCHI2DOF < %(DDK_TRK_CHI2DOF_MAX)s) & (TRGHOSTPROB < %(DDK_TRK_TRGHOSTPROB_MAX)s) & (PROBNNpi > %(DDK_TRK_PROBNNPI_MIN)s)' % config,
                'K+': '(PT > %(DDK_TRK_PT_MIN)s) & (P > %(DDK_TRK_P_MIN)s) & (MIPCHI2DV(PRIMARY) > %(DDK_TRK_IPCHI2_MIN)s) & (TRCHI2DOF < %(DDK_TRK_CHI2DOF_MAX)s) & (TRGHOSTPROB < %(DDK_TRK_TRGHOSTPROB_MAX)s)' % config,
                'K-': '(PT > %(DDK_TRK_PT_MIN)s) & (P > %(DDK_TRK_P_MIN)s) & (MIPCHI2DV(PRIMARY) > %(DDK_TRK_IPCHI2_MIN)s) & (TRCHI2DOF < %(DDK_TRK_CHI2DOF_MAX)s) & (TRGHOSTPROB < %(DDK_TRK_TRGHOSTPROB_MAX)s)' % config
            },
            CombinationCut="(APT > %(DDK_COMB_APT_MIN)s) & ((AM > %(DDK_COMB_AM_MIN)s) & (AM < %(DDK_COMB_AM_MAX)s)) & (AMAXDOCA('') < %(DDK_COMB_AMAXDOCA_MAX)s) & (ANUM(PT > %(DDK_COMB_ANUMPT_CUTMIN)s) >= %(DDK_COMB_ANUMPT_MINNUM)s)" % config,
            MotherCut='(PT > %(DDK_MOTHER_PT_MIN)s) & (M > %(DDK_MOTHER_M_MIN)s) & (M < %(DDK_MOTHER_M_MAX)s) & (BPVDIRA > %(DDK_MOTHER_DIRA_MIN)s) & (VFASPF(VCHI2) < %(DDK_MOTHER_VCHI2_MAX)s) & (BPVVDCHI2 > %(DDK_MOTHER_VDCHI2_MIN)s) & (BPVVDRHO > %(DDK_MOTHER_VDRHO_MIN)s) & (BPVVDRHO < %(DDK_MOTHER_VDRHO_MAX)s) & (BPVVDZ > %(DDK_MOTHER_VDZ_MIN)s)' % config
        )

        self.B2STauTauInclusive = self._makeB2STauTauInclusive(
            name+"_Sel", self.selKaons, self.selPions, self.selTau, config)

        self.B2STauTauSSInclusive = self._makeB2STauTauInclusive(
            name+"_SS_Sel", self.selKaons, self.selPions, self.selTau, config, SS=True)

        self.B2STauTauInclusive_DDK = self._makeB2STauTauInclusive(
            name+"_DDK_Sel", self.selKaons, self.selPions, self.selTau_DDK, config)

        self.B2STauTauInclusive_DDKSS = self._makeB2STauTauInclusive(
            name+"_DDK_SS_Sel", self.selKaons, self.selPions, self.selTau_DDK, config, SS=True)

        self.B2STauTauInclusive_Line = self._makeLine(
            "B2STauTauInclusiveLine", self.B2STauTauInclusive, config)
        self.B2STauTauSSInclusive_Line = self._makeLine(
            "B2STauTauSSInclusiveLine", self.B2STauTauSSInclusive, config)

        self.B2STauTauInclusive_DDK_Line = self._makeLine(
            "B2STauTau_DDK_InclusiveLine", self.B2STauTauInclusive_DDK, config)
        self.B2STauTauInclusive_DDKSS_Line = self._makeLine(
            "B2STauTau_DDKSSInclusiveLine", self.B2STauTauInclusive_DDKSS, config)

    def _makeB2STauTauInclusive(self, name, kaonSel, pionSel, TauSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "(  VFASPF(VCHI2) < %(VCHI2_B)s ) & ( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config

        decays = ["[B+ -> K+ tau+ tau-]cc",
                  "[B0 -> K+ pi- tau+ tau-]cc"]

        if SS and 'DDK' not in name:
            decays = ["[B+ -> K+ tau+ tau+]cc",
                      "[B+ -> K- tau+ tau+]cc",
                      "[B0 -> K+ pi- tau+ tau+]cc"]

        if 'DDK' in name:
            decays = ["[B+ -> K+ D+ D-]cc",
                      "[B0 -> K+ pi- D+ D-]cc"]
            if SS:
                decays = ["[B+ -> K+ D+ D+]cc",
                          "[B+ -> K- D+ D+]cc",
                          "[B0 -> K+ pi- D+ D+]cc"]

        Combine = CombineParticles(DecayDescriptors=decays,
                                   CombinationCut=combcut,
                                   MotherCut=mothercut
                                   )

        return Selection(name,
                         Algorithm=Combine,
                         RequiredSelections=[kaonSel,
                                             pionSel,
                                             TauSel])

    # Helpers to make lines

    def _makeLine(self, name, sel, config):

        line = StrippingLine(
            name,
            prescale=config['B2STauTauInclusive_LinePrescale'],
            postscale=config['B2STauTauInclusive_LinePostscale'],
            MDSTFlag=True,
            FILTER=self.FilterSPD,
            RelatedInfoTools=config['RelInfoTools'],
            selection=sel,
            MaxCandidates=50)
        self.registerLine(line)
        return line
