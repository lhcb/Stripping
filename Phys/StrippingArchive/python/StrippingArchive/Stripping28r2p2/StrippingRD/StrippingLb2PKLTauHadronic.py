
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for
    Lb -> p K- mu+ tau-
    Lb -> p K- mu- tau+
    Lb -> p K- e+ tau-
    Lb -> p K- e- tau+
    where tau -> 3pi
Same-sign combinations (l- tau-, l+ tau+) are included. Lines with reversed PID cuts are included
"""

from StandardParticles import StdLooseProtons, StdNoPIDsProtons, StdLooseKaons, StdNoPIDsKaons, StdAllLooseMuons, StdAllNoPIDsMuons, StdAllLooseElectrons, StdAllNoPIDsElectrons
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine
from PhysSelPython.Wrappers import Selection, DataOnDemand
from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop, DaVinci__N3BodyDecays
__author__ = 'Federico Betti, Lakshan Madhan'
__date__ = '10/07/2023'
__version__ = '$Revision: 1.0 $'

__all__ = ('Lb2PKLTauHadronicLines',
           'makeProtons',
           'makeFakeProtons',
           'makeKaons',
           'makeFakeKaons',
           'makeMuons',
           'makeFakeMuons',
           'makeElectrons',
           'makeFakeElectrons',
           'makePKls',
           'makeTaus',
           'makeLambdabs',
           'default_config')

# from Gaudi.Configuration import *
# from LHCbKernel.Configuration import *


default_config = {
    'NAME': 'Lb2PKLTauHadronic',
    'BUILDERTYPE': 'Lb2PKLTauHadronicLines',
    'CONFIG':
        {
            "Track_GhostProb": 0.3,
            "Track_TRCHI2": 3.0,
            "Track_MinIPCHI2": 9.0,
            "Hadron_P": 3000.0,
            "Proton_PID": 5.0,
            "Kaon_PID": 4.0,
            "UseNoPIDsHadrons": False,
            "UseNoPIDsMuons": False,
            "UseNoPIDsElectrons": False,
            "HLT1_FILTER": None,
            "HLT2_FILTER": None,
            "L0DU_FILTER": None,
            "SpdMult": 600,
            "Muon_PID": -5.0,
            "Muon_PROBNN": 0.0,
            "Muon_PT": 500.0,
            "Muon_P": 3000.0,
            "Electron_PID": 2.0,
            "Electron_PROBNN": 0.0,
            "Electron_PT": 500.0,
            "Electron_P": 3000.0,
            "pKmu_AM_min": 1400.0,
            "pKmu_AM_max": 4000.0,
            "pKmu_ADOCA_max": 0.8,
            "pKmu_ACHI2DOCA_max": 8.0,
            "pKmu_PT": 1250.0,
            "pKmu_VTXCHI2DOF": 8.0,
            "pKmu_BPVVDCHI2": 25.0,
            "pKe_AM_min": 2000.0,
            "pKe_AM_max": 4000.0,
            "pKe_ADOCA_max": 0.8,
            "pKe_ACHI2DOCA_max": 8.0,
            "pKe_PT": 1250.0,
            "pKe_VTXCHI2DOF": 8.0,
            "pKe_BPVVDCHI2": 25.0,
            "Lb_AM_max": 7250.0,
            "Lb_BPVDIRA": 0.999,
            "Lb_VTXCHI2DOF": 100.0,
            "Lb_BPVVDCHI2": 16.0,
            "Tau_IPCHI2_min": 9.0
        },

    'WGs': ['RD'],
    'STREAMS': ['Semileptonic']
}


class Lb2PKLTauHadronicLines(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name

        self.Protons = makeProtons(self.name + "_Protons",
                                   config['Track_GhostProb'],
                                   config['Track_TRCHI2'],
                                   config['Track_MinIPCHI2'],
                                   config['Hadron_P'],
                                   config['Proton_PID'],
                                   config['UseNoPIDsHadrons'])

        self.FakeProtons = makeFakeProtons(self.name + "_FakeProtons",
                                           config['Track_GhostProb'],
                                           config['Track_TRCHI2'],
                                           config['Track_MinIPCHI2'],
                                           config['Hadron_P'],
                                           config['Proton_PID'])

        self.Kaons = makeKaons(self.name + "_Kaons",
                               config['Track_GhostProb'],
                               config['Track_TRCHI2'],
                               config['Track_MinIPCHI2'],
                               config['Hadron_P'],
                               config['Kaon_PID'],
                               config['UseNoPIDsHadrons'])

        self.FakeKaons = makeFakeKaons(self.name + "_FakeKaons",
                                       config['Track_GhostProb'],
                                       config['Track_TRCHI2'],
                                       config['Track_MinIPCHI2'],
                                       config['Hadron_P'],
                                       config['Kaon_PID'])

        self.Muons = makeMuons(self.name + "_Muons",
                               config['Track_GhostProb'],
                               config['Track_TRCHI2'],
                               config['Track_MinIPCHI2'],
                               config['Muon_PT'],
                               config['Muon_P'],
                               config['Muon_PID'],
                               config['Muon_PROBNN'],
                               config['UseNoPIDsMuons'])

        self.FakeMuons = makeFakeMuons(self.name + "_FakeMuons",
                                       config['Track_GhostProb'],
                                       config['Track_TRCHI2'],
                                       config['Track_MinIPCHI2'],
                                       config['Muon_PT'],
                                       config['Muon_P'],
                                       config['Muon_PID'],
                                       config['Muon_PROBNN'])

        self.Electrons = makeElectrons(self.name + "_Electrons",
                                       config['Track_GhostProb'],
                                       config['Track_TRCHI2'],
                                       config['Track_MinIPCHI2'],
                                       config['Electron_PT'],
                                       config['Electron_P'],
                                       config['Electron_PID'],
                                       config['Electron_PROBNN'],
                                       config['UseNoPIDsElectrons'])

        self.FakeElectrons = makeFakeElectrons(self.name + "_FakeElectrons",
                                               config['Track_GhostProb'],
                                               config['Track_TRCHI2'],
                                               config['Track_MinIPCHI2'],
                                               config['Electron_PT'],
                                               config['Electron_P'],
                                               config['Electron_PID'],
                                               config['Electron_PROBNN'])

        self.X2pKmus = makePKls(self.name + "_X2pKmu",
                                self.Protons,
                                self.Kaons,
                                self.Muons,
                                config['pKmu_AM_min'],
                                config['pKmu_AM_max'],
                                config['pKmu_ADOCA_max'],
                                config['pKmu_ACHI2DOCA_max'],
                                config['pKmu_PT'],
                                config['pKmu_VTXCHI2DOF'],
                                config['pKmu_BPVVDCHI2'],
                                'mu')

        self.X2pKmus_fakep = makePKls(self.name + "_X2pKmu_fakep",
                                      self.FakeProtons,
                                      self.Kaons,
                                      self.Muons,
                                      config['pKmu_AM_min'],
                                      config['pKmu_AM_max'],
                                      config['pKmu_ADOCA_max'],
                                      config['pKmu_ACHI2DOCA_max'],
                                      config['pKmu_PT'],
                                      config['pKmu_VTXCHI2DOF'],
                                      config['pKmu_BPVVDCHI2'],
                                      'mu')

        self.X2pKmus_fakeK = makePKls(self.name + "_X2pKmu_fakeK",
                                      self.Protons,
                                      self.FakeKaons,
                                      self.Muons,
                                      config['pKmu_AM_min'],
                                      config['pKmu_AM_max'],
                                      config['pKmu_ADOCA_max'],
                                      config['pKmu_ACHI2DOCA_max'],
                                      config['pKmu_PT'],
                                      config['pKmu_VTXCHI2DOF'],
                                      config['pKmu_BPVVDCHI2'],
                                      'mu')

        self.X2pKmus_fakemu = makePKls(self.name + "_X2pKmu_fakemu",
                                       self.Protons,
                                       self.Kaons,
                                       self.FakeMuons,
                                       config['pKmu_AM_min'],
                                       config['pKmu_AM_max'],
                                       config['pKmu_ADOCA_max'],
                                       config['pKmu_ACHI2DOCA_max'],
                                       config['pKmu_PT'],
                                       config['pKmu_VTXCHI2DOF'],
                                       config['pKmu_BPVVDCHI2'],
                                       'mu')

        self.X2pKes = makePKls(self.name + "_X2pKe",
                               self.Protons,
                               self.Kaons,
                               self.Electrons,
                               config['pKe_AM_min'],
                               config['pKe_AM_max'],
                               config['pKe_ADOCA_max'],
                               config['pKe_ACHI2DOCA_max'],
                               config['pKe_PT'],
                               config['pKe_VTXCHI2DOF'],
                               config['pKe_BPVVDCHI2'],
                               'e')

        self.X2pKes_fakep = makePKls(self.name + "_X2pKe_fakep",
                                     self.FakeProtons,
                                     self.Kaons,
                                     self.Electrons,
                                     config['pKe_AM_min'],
                                     config['pKe_AM_max'],
                                     config['pKe_ADOCA_max'],
                                     config['pKe_ACHI2DOCA_max'],
                                     config['pKe_PT'],
                                     config['pKe_VTXCHI2DOF'],
                                     config['pKe_BPVVDCHI2'],
                                     'e')

        self.X2pKes_fakeK = makePKls(self.name + "_X2pKe_fakeK",
                                     self.Protons,
                                     self.FakeKaons,
                                     self.Electrons,
                                     config['pKe_AM_min'],
                                     config['pKe_AM_max'],
                                     config['pKe_ADOCA_max'],
                                     config['pKe_ACHI2DOCA_max'],
                                     config['pKe_PT'],
                                     config['pKe_VTXCHI2DOF'],
                                     config['pKe_BPVVDCHI2'],
                                     'e')

        self.X2pKes_fakee = makePKls(self.name + "_X2pKe_fakee",
                                     self.Protons,
                                     self.Kaons,
                                     self.FakeElectrons,
                                     config['pKe_AM_min'],
                                     config['pKe_AM_max'],
                                     config['pKe_ADOCA_max'],
                                     config['pKe_ACHI2DOCA_max'],
                                     config['pKe_PT'],
                                     config['pKe_VTXCHI2DOF'],
                                     config['pKe_BPVVDCHI2'],
                                     'e')

        StdTaus2pipipi = DataOnDemand(
            Location="Phys/StdTightDetachedTau3pi/Particles")

        self.Taus = makeTaus(self.name + "_Taus",
                             StdTaus2pipipi,
                             config['Tau_IPCHI2_min'])

        self.Lambdab02pKmutau_tau23pi = makeLambdabs(self.name + "_Lambdab02pKmutau_tau23pi",
                                                     self.X2pKmus,
                                                     self.Taus,
                                                     config['Lb_AM_max'],
                                                     config['Lb_BPVDIRA'],
                                                     config['Lb_VTXCHI2DOF'],
                                                     config['Lb_BPVVDCHI2'],
                                                     same_sign=False)

        self.Lambdab02pKetau_tau23pi = makeLambdabs(self.name + "_Lambdab02pKetau_tau23pi",
                                                    self.X2pKes,
                                                    self.Taus,
                                                    config['Lb_AM_max'],
                                                    config['Lb_BPVDIRA'],
                                                    config['Lb_VTXCHI2DOF'],
                                                    config['Lb_BPVVDCHI2'],
                                                    same_sign=False)

        self.Lambdab02pKmutau_tau23pi_SS = makeLambdabs(self.name + "_Lambdab02pKmutau_tau23pi_SS",
                                                        self.X2pKmus,
                                                        self.Taus,
                                                        config['Lb_AM_max'],
                                                        config['Lb_BPVDIRA'],
                                                        config['Lb_VTXCHI2DOF'],
                                                        config['Lb_BPVVDCHI2'],
                                                        same_sign=True)

        self.Lambdab02pKetau_tau23pi_SS = makeLambdabs(self.name + "_Lambdab02pKetau_tau23pi_SS",
                                                       self.X2pKes,
                                                       self.Taus,
                                                       config['Lb_AM_max'],
                                                       config['Lb_BPVDIRA'],
                                                       config['Lb_VTXCHI2DOF'],
                                                       config['Lb_BPVVDCHI2'],
                                                       same_sign=True)

        self.Lambdab02pKmutau_tau23pi_fakep = makeLambdabs(self.name + "_Lambdab02pKmutau_tau23pi_fakep",
                                                           self.X2pKmus_fakep,
                                                           self.Taus,
                                                           config['Lb_AM_max'],
                                                           config['Lb_BPVDIRA'],
                                                           config['Lb_VTXCHI2DOF'],
                                                           config['Lb_BPVVDCHI2'],
                                                           same_sign=False)

        self.Lambdab02pKmutau_tau23pi_fakeK = makeLambdabs(self.name + "_Lambdab02pKmutau_tau23pi_fakeK",
                                                           self.X2pKmus_fakeK,
                                                           self.Taus,
                                                           config['Lb_AM_max'],
                                                           config['Lb_BPVDIRA'],
                                                           config['Lb_VTXCHI2DOF'],
                                                           config['Lb_BPVVDCHI2'],
                                                           same_sign=False)

        self.Lambdab02pKmutau_tau23pi_fakemu = makeLambdabs(self.name + "_Lambdab02pKmutau_tau23pi_fakemu",
                                                            self.X2pKmus_fakemu,
                                                            self.Taus,
                                                            config['Lb_AM_max'],
                                                            config['Lb_BPVDIRA'],
                                                            config['Lb_VTXCHI2DOF'],
                                                            config['Lb_BPVVDCHI2'],
                                                            same_sign=False)

        self.Lambdab02pKetau_tau23pi_fakep = makeLambdabs(self.name + "_Lambdab02pKetau_tau23pi_fakep",
                                                          self.X2pKes_fakep,
                                                          self.Taus,
                                                          config['Lb_AM_max'],
                                                          config['Lb_BPVDIRA'],
                                                          config['Lb_VTXCHI2DOF'],
                                                          config['Lb_BPVVDCHI2'],
                                                          same_sign=False)

        self.Lambdab02pKetau_tau23pi_fakeK = makeLambdabs(self.name + "_Lambdab02pKetau_tau23pi_fakeK",
                                                          self.X2pKes_fakeK,
                                                          self.Taus,
                                                          config['Lb_AM_max'],
                                                          config['Lb_BPVDIRA'],
                                                          config['Lb_VTXCHI2DOF'],
                                                          config['Lb_BPVVDCHI2'],
                                                          same_sign=False)

        self.Lambdab02pKetau_tau23pi_fakee = makeLambdabs(self.name + "_Lambdab02pKetau_tau23pi_fakee",
                                                          self.X2pKes_fakee,
                                                          self.Taus,
                                                          config['Lb_AM_max'],
                                                          config['Lb_BPVDIRA'],
                                                          config['Lb_VTXCHI2DOF'],
                                                          config['Lb_BPVVDCHI2'],
                                                          same_sign=False)

        self.FilterSPD = {'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                          'Preambulo': ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]}

        self.line_Lambdab02pKmutau_tau23pi = StrippingLine(self.name+'_Lambdab02pKmutau_tau23pi_Line',
                                                           HLT2=config['HLT2_FILTER'], HLT1=config[
                                                               'HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
                                                           FILTER=self.FilterSPD,
                                                           selection=self.Lambdab02pKmutau_tau23pi,
                                                           MDSTFlag=False,
                                                           MaxCandidates=1000)
        self.registerLine(self.line_Lambdab02pKmutau_tau23pi)

        self.line_Lambdab02pKetau_tau23pi = StrippingLine(self.name+'_Lambdab02pKetau_tau23pi_Line',
                                                          HLT2=config['HLT2_FILTER'], HLT1=config[
                                                              'HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
                                                          FILTER=self.FilterSPD,
                                                          selection=self.Lambdab02pKetau_tau23pi,
                                                          MDSTFlag=False,
                                                          MaxCandidates=1000)
        self.registerLine(self.line_Lambdab02pKetau_tau23pi)

        self.line_Lambdab02pKmutau_tau23pi_SS = StrippingLine(self.name+'_Lambdab02pKmutau_tau23pi_SS_Line',
                                                              HLT2=config['HLT2_FILTER'], HLT1=config[
                                                                  'HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
                                                              FILTER=self.FilterSPD,
                                                              selection=self.Lambdab02pKmutau_tau23pi_SS,
                                                              MDSTFlag=False,
                                                              MaxCandidates=1000)
        self.registerLine(self.line_Lambdab02pKmutau_tau23pi_SS)

        self.line_Lambdab02pKetau_tau23pi_SS = StrippingLine(self.name+'_Lambdab02pKetau_tau23pi_SS_Line',
                                                             HLT2=config['HLT2_FILTER'], HLT1=config[
                                                                 'HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
                                                             FILTER=self.FilterSPD,
                                                             selection=self.Lambdab02pKetau_tau23pi_SS,
                                                             MDSTFlag=False,
                                                             MaxCandidates=1000)
        self.registerLine(self.line_Lambdab02pKetau_tau23pi_SS)

        self.line_Lambdab02pKmutau_tau23pi_fakep = StrippingLine(self.name+'_Lambdab02pKmutau_tau23pi_fakep_Line',
                                                                 prescale=0.3,
                                                                 HLT2=config['HLT2_FILTER'], HLT1=config[
                                                                     'HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
                                                                 FILTER=self.FilterSPD,
                                                                 selection=self.Lambdab02pKmutau_tau23pi_fakep,
                                                                 MDSTFlag=False,
                                                                 MaxCandidates=1000)
        self.registerLine(self.line_Lambdab02pKmutau_tau23pi_fakep)

        self.line_Lambdab02pKmutau_tau23pi_fakeK = StrippingLine(self.name+'_Lambdab02pKmutau_tau23pi_fakeK_Line',
                                                                 prescale=0.3,
                                                                 HLT2=config['HLT2_FILTER'], HLT1=config[
                                                                     'HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
                                                                 FILTER=self.FilterSPD,
                                                                 selection=self.Lambdab02pKmutau_tau23pi_fakeK,
                                                                 MDSTFlag=False,
                                                                 MaxCandidates=1000)
        self.registerLine(self.line_Lambdab02pKmutau_tau23pi_fakeK)

        self.line_Lambdab02pKmutau_tau23pi_fakemu = StrippingLine(self.name+'_Lambdab02pKmutau_tau23pi_fakemu_Line',
                                                                  prescale=0.1,
                                                                  HLT2=config['HLT2_FILTER'], HLT1=config[
                                                                      'HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
                                                                  FILTER=self.FilterSPD,
                                                                  selection=self.Lambdab02pKmutau_tau23pi_fakemu,
                                                                  MDSTFlag=False,
                                                                  MaxCandidates=1000)
        self.registerLine(self.line_Lambdab02pKmutau_tau23pi_fakemu)

        self.line_Lambdab02pKetau_tau23pi_fakep = StrippingLine(self.name+'_Lambdab02pKetau_tau23pi_fakep_Line',
                                                                prescale=0.25,
                                                                HLT2=config['HLT2_FILTER'], HLT1=config[
                                                                    'HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
                                                                FILTER=self.FilterSPD,
                                                                selection=self.Lambdab02pKetau_tau23pi_fakep,
                                                                MDSTFlag=False,
                                                                MaxCandidates=1000)
        self.registerLine(self.line_Lambdab02pKetau_tau23pi_fakep)

        self.line_Lambdab02pKetau_tau23pi_fakeK = StrippingLine(self.name+'_Lambdab02pKetau_tau23pi_fakeK_Line',
                                                                prescale=0.3,
                                                                HLT2=config['HLT2_FILTER'], HLT1=config[
                                                                    'HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
                                                                FILTER=self.FilterSPD,
                                                                selection=self.Lambdab02pKetau_tau23pi_fakeK,
                                                                MDSTFlag=False,
                                                                MaxCandidates=1000)
        self.registerLine(self.line_Lambdab02pKetau_tau23pi_fakeK)

        self.line_Lambdab02pKetau_tau23pi_fakee = StrippingLine(self.name+'_Lambdab02pKetau_tau23pi_fakee_Line',
                                                                prescale=0.1,
                                                                HLT2=config['HLT2_FILTER'], HLT1=config[
                                                                    'HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
                                                                FILTER=self.FilterSPD,
                                                                selection=self.Lambdab02pKetau_tau23pi_fakee,
                                                                MDSTFlag=False,
                                                                MaxCandidates=1000)
        self.registerLine(self.line_Lambdab02pKetau_tau23pi_fakee)


def makeProtons(name,
                track_GhostProb,
                track_TRCHI2,
                track_MinIPCHI2,
                proton_P,
                protonPID,
                UseNoPIDsHadrons):
    input_protons = StdNoPIDsProtons if UseNoPIDsHadrons else StdLooseProtons
    cuts = "(TRGHP < %(track_GhostProb)s) & (TRCHI2DOF < %(track_TRCHI2)s) & (MIPCHI2DV(PRIMARY) > %(track_MinIPCHI2)s) & (P > %(proton_P)s * MeV) & (PIDp > %(protonPID)s) & (~ISMUON)" % locals()
    _filter = FilterDesktop(Code=cuts)
    return Selection(name,
                     Algorithm=_filter,
                     RequiredSelections=[input_protons])


def makeFakeProtons(name,
                    track_GhostProb,
                    track_TRCHI2,
                    track_MinIPCHI2,
                    proton_P,
                    protonPID):
    input_protons = StdNoPIDsProtons
    cuts = "(TRGHP < %(track_GhostProb)s) & (TRCHI2DOF < %(track_TRCHI2)s) & (MIPCHI2DV(PRIMARY) > %(track_MinIPCHI2)s) & (P > %(proton_P)s * MeV) & (PIDp < %(protonPID)s)" % locals()
    _filter = FilterDesktop(Code=cuts)
    return Selection(name,
                     Algorithm=_filter,
                     RequiredSelections=[input_protons])


def makeKaons(name,
              track_GhostProb,
              track_TRCHI2,
              track_MinIPCHI2,
              kaon_P,
              kaonPID,
              UseNoPIDsHadrons):
    input_kaons = StdNoPIDsKaons if UseNoPIDsHadrons else StdLooseKaons
    cuts = "(TRGHP < %(track_GhostProb)s) & (TRCHI2DOF < %(track_TRCHI2)s) & (MIPCHI2DV(PRIMARY) > %(track_MinIPCHI2)s) & (P > %(kaon_P)s * MeV) & (PIDK > %(kaonPID)s) & (~ISMUON)" % locals()
    _filter = FilterDesktop(Code=cuts)
    return Selection(name,
                     Algorithm=_filter,
                     RequiredSelections=[input_kaons])


def makeFakeKaons(name,
                  track_GhostProb,
                  track_TRCHI2,
                  track_MinIPCHI2,
                  kaon_P,
                  kaonPID):
    input_kaons = StdNoPIDsKaons
    cuts = "(TRGHP < %(track_GhostProb)s) & (TRCHI2DOF < %(track_TRCHI2)s) & (MIPCHI2DV(PRIMARY) > %(track_MinIPCHI2)s) & (P > %(kaon_P)s * MeV) & (PIDK < %(kaonPID)s)" % locals()
    _filter = FilterDesktop(Code=cuts)
    return Selection(name,
                     Algorithm=_filter,
                     RequiredSelections=[input_kaons])


def makeMuons(name,
              track_GhostProb,
              track_TRCHI2,
              track_MinIPCHI2,
              muon_PT,
              muon_P,
              muon_PID,
              muon_PROBNN,
              UseNoPIDsMuons):
    input_muons = StdAllNoPIDsMuons if UseNoPIDsMuons else StdAllLooseMuons
    cuts = "(TRGHP < %(track_GhostProb)s) & (TRCHI2DOF < %(track_TRCHI2)s) & (MIPCHI2DV(PRIMARY) > %(track_MinIPCHI2)s) & (PT > %(muon_PT)s * MeV) & (P > %(muon_P)s * MeV) & (PIDmu > %(muon_PID)s) & (PROBNNmu > %(muon_PROBNN)s) & (ISMUON)" % locals()
    _filter = FilterDesktop(Code=cuts)
    return Selection(name,
                     Algorithm=_filter,
                     RequiredSelections=[input_muons])


def makeFakeMuons(name,
                  track_GhostProb,
                  track_TRCHI2,
                  track_MinIPCHI2,
                  muon_PT,
                  muon_P,
                  muon_PID,
                  muon_PROBNN):
    input_muons = StdAllNoPIDsMuons
    cuts = "(TRGHP < %(track_GhostProb)s) & (TRCHI2DOF < %(track_TRCHI2)s) & (MIPCHI2DV(PRIMARY) > %(track_MinIPCHI2)s) & (PT > %(muon_PT)s * MeV) & (P > %(muon_P)s * MeV) & ~( (PIDmu > %(muon_PID)s) & (PROBNNmu > %(muon_PROBNN)s) )" % locals()
    _filter = FilterDesktop(Code=cuts)
    return Selection(name,
                     Algorithm=_filter,
                     RequiredSelections=[input_muons])


def makeElectrons(name,
                  track_GhostProb,
                  track_TRCHI2,
                  track_MinIPCHI2,
                  electron_PT,
                  electron_P,
                  electron_PID,
                  electron_PROBNN,
                  UseNoPIDsElectrons):
    input_electrons = StdAllNoPIDsElectrons if UseNoPIDsElectrons else StdAllLooseElectrons
    cuts = "(TRGHP < %(track_GhostProb)s) & (TRCHI2DOF < %(track_TRCHI2)s) & (MIPCHI2DV(PRIMARY) > %(track_MinIPCHI2)s) & (PT > %(electron_PT)s * MeV) & (P > %(electron_P)s * MeV) & (PIDe > %(electron_PID)s) & (PROBNNe > %(electron_PROBNN)s) & (~ISMUON) & (HASCALOS) & (HASRICH)" % locals()
    _filter = FilterDesktop(Code=cuts)
    return Selection(name,
                     Algorithm=_filter,
                     RequiredSelections=[input_electrons])


def makeFakeElectrons(name,
                      track_GhostProb,
                      track_TRCHI2,
                      track_MinIPCHI2,
                      electron_PT,
                      electron_P,
                      electron_PID,
                      electron_PROBNN):
    input_electrons = StdAllNoPIDsElectrons
    cuts = "(TRGHP < %(track_GhostProb)s) & (TRCHI2DOF < %(track_TRCHI2)s) & (MIPCHI2DV(PRIMARY) > %(track_MinIPCHI2)s) & (PT > %(electron_PT)s * MeV) & (P > %(electron_P)s * MeV) & ~( (PIDe > %(electron_PID)s) & (PROBNNe > %(electron_PROBNN)s) ) & (HASCALOS) & (HASRICH)" % locals()
    _filter = FilterDesktop(Code=cuts)
    return Selection(name,
                     Algorithm=_filter,
                     RequiredSelections=[input_electrons])


def makePKls(name,
             protons,
             kaons,
             leptons,
             aM_min,
             aM_max,
             aDOCA_max,
             ACHI2DOCA_max,
             mother_PT,
             mother_VTXCHI2DOF,
             mother_BPVVDCHI2,
             lepton_name):
    combination12_cuts = "(AM < %(aM_max)s * MeV) & (ADOCA(1,2) < %(aDOCA_max)s * mm) & (ACHI2DOCA(1,2) < %(ACHI2DOCA_max)s)" % locals()
    combination_cuts = "(AM < %(aM_max)s * MeV) & (AM > %(aM_min)s * MeV) & (ADOCA(1,3) < %(aDOCA_max)s * mm) & (ACHI2DOCA(1,3) < %(ACHI2DOCA_max)s) & (ADOCA(2,3) < %(aDOCA_max)s * mm) & (ACHI2DOCA(2,3) < %(ACHI2DOCA_max)s)" % locals()
    mother_cuts = "(PT > %(mother_PT)s * MeV) & (VFASPF(VCHI2/VDOF) < %(mother_VTXCHI2DOF)s) & (BPVVDCHI2 > %(mother_BPVVDCHI2)s)" % locals()

    decayDescriptors = ['[B+ -> p+ K- %s+]cc' % lepton_name,
                        '[B- -> p+ K- %s-]cc' % lepton_name]
    Combine3Body = DaVinci__N3BodyDecays(DecayDescriptors=decayDescriptors,
                                         Combination12Cut=combination12_cuts,
                                         CombinationCut=combination_cuts,
                                         MotherCut=mother_cuts)
    return Selection(name,
                     Algorithm=Combine3Body,
                     RequiredSelections=[protons, kaons, leptons])


def makeTaus(name,
             input_taus,
             ipchi2_min):
    cuts = "(MIPCHI2DV(PRIMARY) > %(ipchi2_min)s)" % locals()
    _filter = FilterDesktop(Code=cuts)
    return Selection(name,
                     Algorithm=_filter,
                     RequiredSelections=[input_taus])


def makeLambdabs(name,
                 pKls,
                 taus,
                 aM_max,
                 mother_BPVDIRA,
                 mother_VTXCHI2DOF,
                 mother_BPVVDCHI2,
                 same_sign):
    combination_cuts = "(AM < %(aM_max)s * MeV)" % locals()
    mother_cuts = "(BPVDIRA > %(mother_BPVDIRA)s) & (VFASPF(VCHI2/VDOF) < %(mother_VTXCHI2DOF)s) & (BPVVDCHI2 > %(mother_BPVVDCHI2)s)" % locals()

    if same_sign:
        decayDescriptors = ['[Lambda_b0 -> B+ tau+]cc',
                            '[Lambda_b0 -> B- tau-]cc']
    else:
        decayDescriptors = ['[Lambda_b0 -> B+ tau-]cc',
                            '[Lambda_b0 -> B- tau+]cc']

    Combine2Body = CombineParticles(DecayDescriptors=decayDescriptors,
                                    CombinationCut=combination_cuts,
                                    MotherCut=mother_cuts)
    return Selection(name,
                     Algorithm=Combine2Body,
                     RequiredSelections=[pKls, taus])
