###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###
### Selection code for Xibm charmless decays to Lambda0 3h and p+ KS0 2h; where h = K/pi and lines implemented as LL and DD
###

__author__ = ['Miroslav Saur']
__date__ = '2023/07/07'
__version__ = '$Revision: 0.1 $'
__all__ = ('StrippingXibm2V03h', 'default_config')

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, DaVinci__N4BodyDecays
from StandardParticles import StdNoPIDsDownPions, StdLooseDownKaons, StdAllLoosePions, StdAllLooseKaons, StdAllLooseProtons
from PhysSelPython.Wrappers import Selection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond

#### This is the dictionary of all tunable cuts ########
default_config = {
    'NAME': 'Xibm2V03h',
    'WGs': ['BnoC'],
    'BUILDERTYPE': 'StrippingXibm2V03h',
    'STREAMS': ['Bhadron'],
    'CONFIG': {
        'TRCHI2DOFMax':
        4.0,
        'TrGhostProbMax':
        0.3,  # same for all particles
        'MINIPCHI2':
        8.0,  # adimensiional, orig. 9 
        'pion_p_min':
        2.0 * GeV,
        'pion_pt_min':
        200 * MeV,
        'pion_pidk_max':
        10.0,
        'pion_pidpi_pidk_max':
        0.0,
        'kaon_p_min':
        2.0 * GeV,
        'kaon_pt_min':
        200 * MeV,
        'kaon_pidk_min':
        10.0,
        'kaon_pidk_pidpi_max':
        5.0,
        'proton_p_min':
        2.0 * GeV,
        'proton_pt_min':
        200 * MeV,
        'proton_pidp_pidpi_max':
        5.0,
        'proton_pidp_pidk_max':
        0.0,
        'ProbNNp':
        0.2,
        'ProbNNk':
        0.2,
        'ProbNNpi':
        0.2,
        'ProbNNpiMax':
        0.9,
        'ProbNNkMax':
        0.9,
        'lambda_ll_p_min':
        2500.0 * MeV,
        'lambda_ll_pt_min':
        100.0 * MeV,
        'lambda_ll_mass_window':
        25.0 * MeV,
        'lambda_ll_fdchi2_min':
        30.0,
        'lambda_dd_p_min':
        2000.0 * MeV,
        'lambda_dd_pt_min':
        100.0 * MeV,
        'lambda_dd_mass_window':
        20.0 * MeV,
        'lambda_dd_fdchi2_min':
        50.0,  ## unitless
        'LambdaLDPMin':
        3000.0 * MeV,
        'LambdaLDPTMin':
        200.0 * MeV,
        'LambdaLDCutMass':
        20.0 * MeV,
        'lambda_ld_fdchi2_min':
        100.0,  ## unitless
        'lambda_trackchi2_min':
        4.0,  ## unitless
        'lambda_vertexchi2_min':
        5.0,  ## max chi2/ndf for Lambda0 vertex
        'lambda_ll_dira_min':
        0.99,  ## unitless
        'lambda_dd_dira_min':
        0.9,  ## unitless
        'lambda_ll_vz_min':
        -100.0 * mm,
        'lambda_ll_vz_max':
        400.0 * mm,
        'lambda_dd_vz_min':
        400.0 * mm,
        'lambda_dd_vz_max':
        2275.0 * mm,
        'lambda_pp_p_min':
        0.0 * MeV,
        'lambda_pim_p_min':
        0.0 * MeV,
        'Xibm_vertexchi2_max':
        25.0,
        'Xibm_BPVVDCHI2_Min':
        25.0,
        'Xibm_BPVDIRA_Min':
        0.999,
        'Xibm_AM_Min':
        5300.0 * MeV,
        'Xibm_AM_Max':
        6350.0 * MeV,
        'Xibm_ADOCAMAXCHI2_max':
        25,
        'Xibm_APT_min':
        1200 * MeV,
        'RelatedInfoTools':
        [{
            "Type": "RelInfoConeVariables",
            "ConeAngle": 1.7,
            "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
            "Location": 'ConeVar17'
        },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 1.5,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar15'
         },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 1.0,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar10'
         },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 0.8,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar08'
         }, {
             "Type": "RelInfoVertexIsolation",
             "Location": "VtxIsolationVar"
         }]
    }  ## end of 'CONFIG' 
}  ## end of default_config


#-------------------------------------------------------------------------------------------------------------
class StrippingXibm2V03h(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        ##########################################################################
        ## Basic particles: pi, K, p
        ##########################################################################
        self.selPion = Selection(
            "sel_pi_for" + name,
            Algorithm=self._pionFilter("pi_for_" + name),
            RequiredSelections=[StdAllLoosePions])

        self.selPion_down = Selection(
            "sel_pi_down_for" + name,
            Algorithm=self._pionFilter("pi_down_for_" + name),
            RequiredSelections=[StdNoPIDsDownPions])

        self.selKaon = Selection(
            "sel_k_for" + name,
            Algorithm=self._kaonFilter("kaon_for_" + name),
            RequiredSelections=[StdAllLooseKaons])

        self.selKaon_down = Selection(
            "sel_k_down_for" + name,
            Algorithm=self._kaonFilter("kaon_down_for" + name),
            RequiredSelections=[StdLooseDownKaons])

        self.selProton = Selection(
            "sel_p_for" + name,
            Algorithm=self._protonFilter("proton_for_" + name),
            RequiredSelections=[StdAllLooseProtons])

        ##########################################################################
        ## KS0 -> pi+ pi-
        ##########################################################################
        _stdLooseKS0LL = AutomaticData("Phys/StdVeryLooseKsLL/Particles")
        _stdLooseKS0DD = AutomaticData("Phys/StdLooseKsDD/Particles")
        _stdLooseKS0LD = AutomaticData("Phys/StdLooseKsLD/Particles")

        self.selKS0LL = Selection(
            "SelKS0LLfor" + name,
            Algorithm=self._KS0_LL_Filter("KS0LLfor" + name),
            RequiredSelections=[_stdLooseKS0LL])

        self.selKS0DD = Selection(
            "SelKS0DDfor" + name,
            Algorithm=self._KS0_DD_Filter("KS0DDfor" + name),
            RequiredSelections=[_stdLooseKS0DD])

        self.selKS0LD = Selection(
            "SelKS0LDfor" + name,
            Algorithm=self._KS0_LD_Filter("KS0LDfor" + name),
            RequiredSelections=[_stdLooseKS0LD])

        ##########################################################################
        ## Lambda0 -> p+ pi-
        ##########################################################################
        _stdLooseLambdaLL = AutomaticData(
            "Phys/StdVeryLooseLambdaLL/Particles")
        _stdLooseLambdaDD = AutomaticData("Phys/StdLooseLambdaDD/Particles")
        _stdLooseLambdaLD = AutomaticData("Phys/StdLooseLambdaLD/Particles")

        self.selLambdaLL = Selection(
            "SelLambdaLLfor" + name,
            Algorithm=self._LambdaLLFilter("LambdaLLfor" + name),
            RequiredSelections=[_stdLooseLambdaLL])

        self.selLambdaDD = Selection(
            "SelLambdaDDfor" + name,
            Algorithm=self._LambdaDDFilter("LambdaDDfor" + name),
            RequiredSelections=[_stdLooseLambdaDD])

        self.selLambdaLD = Selection(
            "SelLambdaLDfor" + name,
            Algorithm=self._LambdaLDFilter("LambdaLDfor" + name),
            RequiredSelections=[_stdLooseLambdaLD])

        self.XibmList = self.make_Xibm()

    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    # Sub Function
    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    def _pionFilter(self, _name):
        _code = "(TRCHI2DOF < %(TRCHI2DOFMax)s) & (TRGHOSTPROB < %(TrGhostProbMax)s) & (P>%(pion_p_min)s) & (PT > %(pion_pt_min)s)"\
                "& (~ISMUON) & (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PROBNNpi > %(ProbNNpi)s)" % self.config
        _pion = FilterDesktop(Code=_code)
        return _pion

    def _kaonFilter(self, _name):
        _code = "(TRCHI2DOF < %(TRCHI2DOFMax)s) & (TRGHOSTPROB < %(TrGhostProbMax)s) & (P>%(kaon_p_min)s) & (PT > %(kaon_pt_min)s)"\
                "& (~ISMUON) & (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PROBNNk > %(ProbNNk)s)" % self.config
        _kaon = FilterDesktop(Code=_code)
        return _kaon

    def _protonFilter(self, _name):
        _code = "(TRCHI2DOF < %(TRCHI2DOFMax)s) & (TRGHOSTPROB < %(TrGhostProbMax)s) & (PT > %(proton_pt_min)s) & (P>%(proton_p_min)s)"\
                "& (PROBNNp > %(ProbNNp)s) "\
                "& (~ISMUON) & (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)" % self.config
        _proton = FilterDesktop(Code=_code)
        return _proton

    def _LambdaLLFilter(self, _name):
        _code = " (P > %(lambda_ll_p_min)s) & (PT > %(lambda_ll_pt_min)s)" \
                " & (ADMASS('Lambda0')<%(lambda_ll_mass_window)s)" \
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 1) )"\
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 2) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 1) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 2) )" \
                " & (VFASPF(VCHI2/VDOF) < %(lambda_vertexchi2_min)s)" \
                " & (BPVDIRA > %(lambda_ll_dira_min)s )" % self.config
        _l0LL = FilterDesktop(Code=_code)
        return _l0LL

    def _LambdaDDFilter(self, _name):
        _code = " (P> %(lambda_dd_p_min)s) & (PT> %(lambda_dd_pt_min)s)" \
                " & (ADMASS('Lambda0') < %(lambda_dd_mass_window)s)" \
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 1) )"\
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 2) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 1) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 2) )" \
                " & (VFASPF(VCHI2/VDOF) < %(lambda_vertexchi2_min)s)" \
                " & (BPVDIRA > %(lambda_ll_dira_min)s )" % self.config
        _l0DD = FilterDesktop(Code=_code)
        return _l0DD

    def _LambdaLDFilter(self, _name):
        _code = " (P> %(LambdaLDPMin)s) & (PT> %(LambdaLDPTMin)s)" \
                " & (ADMASS('Lambda0') < %(LambdaLDCutMass)s)" \
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 1) )"\
                " & (CHILDCUT( (TRGHOSTPROB < %(TrGhostProbMax)s), 2) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 1) )"\
                " & (CHILDCUT( (TRCHI2DOF < %(TRCHI2DOFMax)s), 2) )" \
                " & (VFASPF(VCHI2/VDOF) < %(lambda_vertexchi2_min)s)" \
                " & (BPVDIRA > %(lambda_dd_dira_min)s )" % self.config
        _l0LD = FilterDesktop(Code=_code)
        return _l0LD

    def _KS0_LL_Filter(self, name):
        _code = "(ADMASS('KS0') < 40*MeV ) & (BPVLTIME() > 0.0005 * ps) & (BPVDIRA > %(lambda_ll_dira_min)s )" % self.config
        _ksLL = FilterDesktop(Code=_code)
        return _ksLL

    def _KS0_LD_Filter(self, name):
        _code = "(ADMASS('KS0') < 45*MeV) & (BPVLTIME() > 0.0005 * ps) & (BPVDIRA > %(lambda_ll_dira_min)s )" % self.config
        _ksLD = FilterDesktop(Code=_code)
        return _ksLD

    def _KS0_DD_Filter(self, name):
        _code = "(ADMASS('KS0') < 50*MeV) & (BPVLTIME() > 0.0005 * ps) & (BPVDIRA > %(lambda_dd_dira_min)s )" % self.config
        _ksDD = FilterDesktop(Code=_code)
        return _ksDD

    ##------------------------------------------------------------------------------------------
    ## --------------------  Begin to make_Xibm  ------------
    def make_Xibm(self):
        #Cut for Basic
        basic_prevertex_cut = "( ACHI2DOCA(1,2) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(1,3) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(1,4) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(2,3) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(2,4) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ( ACHI2DOCA(3,4) <%(Xibm_ADOCAMAXCHI2_max)s)" \
        "& ((APT1 + APT2 + APT3 + APT4)>%(Xibm_APT_min)s)" % self.config

        combination12_cut = "( ACHI2DOCA(1,2) <%(Xibm_ADOCAMAXCHI2_max)s)" % self.config
        combination123_cut = "( ACHI2DOCA(1,3) <%(Xibm_ADOCAMAXCHI2_max)s)" \
                             "& ( ACHI2DOCA(2,3) <%(Xibm_ADOCAMAXCHI2_max)s)" % self.config

        basic_xibm_prevertex_cut = "(AM>%(Xibm_AM_Min)s) & (AM<%(Xibm_AM_Max)s)" % self.config

        basic_postvertex_cut = "( (VFASPF(VCHI2)<%(Xibm_vertexchi2_max)s)" \
                         "& (BPVVDCHI2>%(Xibm_BPVVDCHI2_Min)s)" \
                         "& (BPVDIRA>%(Xibm_BPVDIRA_Min)s) )" % self.config

        xibm_prevertex_cut = basic_prevertex_cut + '&' + basic_xibm_prevertex_cut
        xibm_postvertex_cut = basic_postvertex_cut

        ### Stripping Xi_b- -> Lambda0 K+ K- K- ###
        Xibm2LambdaKpKmKm_LL = self.createCombination4body(
            OutputList="Xibm2LambdaKpKmKm_LL" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K+ K- K-]cc",
            DaughterLists=[self.selLambdaLL, self.selKaon],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKpKmKmLL_Line = StrippingLine(
            self.name + "Xibm2LambdaKpKmKmLL_Line",
            algos=[Xibm2LambdaKpKmKm_LL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKpKmKmLL_Line)

        Xibm2LambdaKpKmKm_LD = self.createCombination4body(
            OutputList="Xibm2LambdaKpKmKm_LD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K+ K- K-]cc",
            DaughterLists=[self.selLambdaLD, self.selKaon],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKpKmKmLD_Line = StrippingLine(
            self.name + "Xibm2LambdaKpKmKmLD_Line",
            algos=[Xibm2LambdaKpKmKm_LD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKpKmKmLD_Line)

        Xibm2LambdaKpKmKm_DD = self.createCombination4body(
            OutputList="Xibm2LambdaKpKmKm_DD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K+ K- K-]cc",
            DaughterLists=[self.selLambdaDD, self.selKaon],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKpKmKmDD_Line = StrippingLine(
            self.name + "Xibm2LambdaKpKmKmDD_Line",
            algos=[Xibm2LambdaKpKmKm_DD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKpKmKmDD_Line)

        ### Stripping Xi_b- -> Lambda0 K+ pi- pi- ###
        Xibm2LambdaKpPimPim_LL = self.createCombination4body(
            OutputList="Xibm2LambdaKpPimPim_LL" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K+ pi- pi-]cc",
            DaughterLists=[self.selLambdaLL, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKpPimPimLL_Line = StrippingLine(
            self.name + "Xibm2LambdaKpPimPimLL_Line",
            algos=[Xibm2LambdaKpPimPim_LL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKpPimPimLL_Line)

        Xibm2LambdaKpPimPim_LD = self.createCombination4body(
            OutputList="Xibm2LambdaKpPimPim_LD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K+ pi- pi-]cc",
            DaughterLists=[self.selLambdaLD, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKpPimPimLD_Line = StrippingLine(
            self.name + "Xibm2LambdaKpPimPimLD_Line",
            algos=[Xibm2LambdaKpPimPim_LD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKpPimPimLD_Line)

        Xibm2LambdaKpPimPim_DD = self.createCombination4body(
            OutputList="Xibm2LambdaKpPimPim_DD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K+ pi- pi-]cc",
            DaughterLists=[self.selLambdaDD, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKpPimPimDD_Line = StrippingLine(
            self.name + "Xibm2LambdaKpPimPimDD_Line",
            algos=[Xibm2LambdaKpPimPim_DD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKpPimPimDD_Line)

        ### Stripping Xi_b- -> Lambda0 K- pi+ pi- ###
        Xibm2LambdaKmPipPim_LL = self.createCombination4body(
            OutputList="Xibm2LambdaKmPipPim_LL" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K- pi+ pi-]cc",
            DaughterLists=[self.selLambdaLL, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKmPipPimLL_Line = StrippingLine(
            self.name + "Xibm2LambdaKmPipPimLL_Line",
            algos=[Xibm2LambdaKmPipPim_LL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKmPipPimLL_Line)

        Xibm2LambdaKmPipPim_LD = self.createCombination4body(
            OutputList="Xibm2LambdaKmPipPim_LD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K- pi+ pi-]cc",
            DaughterLists=[self.selLambdaLD, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKmPipPimLD_Line = StrippingLine(
            self.name + "Xibm2LambdaKmPipPimLD_Line",
            algos=[Xibm2LambdaKmPipPim_LD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKmPipPimLD_Line)

        Xibm2LambdaKmPipPim_DD = self.createCombination4body(
            OutputList="Xibm2LambdaKmPipPim_DD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K- pi+ pi-]cc",
            DaughterLists=[self.selLambdaDD, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKmPipPimDD_Line = StrippingLine(
            self.name + "Xibm2LambdaKmPipPimDD_Line",
            algos=[Xibm2LambdaKmPipPim_DD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKmPipPimDD_Line)

        ### Stripping Xi_b- -> Lambda0 pi+ pi- pi- ###
        Xibm2LambdaPipPimPim_LL = self.createCombination4body(
            OutputList="Xibm2LambdaPipPimPim_LL" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 pi+ pi- pi-]cc",
            DaughterLists=[self.selLambdaLL, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaPipPimPimLL_Line = StrippingLine(
            self.name + "Xibm2LambdaPipPimPimLL_Line",
            algos=[Xibm2LambdaPipPimPim_LL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaPipPimPimLL_Line)

        Xibm2LambdaPipPimPim_LD = self.createCombination4body(
            OutputList="Xibm2LambdaPipPimPim_LD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 pi+ pi- pi-]cc",
            DaughterLists=[self.selLambdaLD, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaPipPimPimLD_Line = StrippingLine(
            self.name + "Xibm2LambdaPipPimPimLD_Line",
            algos=[Xibm2LambdaPipPimPim_LD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaPipPimPimLD_Line)

        Xibm2LambdaPipPimPim_DD = self.createCombination4body(
            OutputList="Xibm2LambdaPipPimPim_DD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 pi+ pi- pi-]cc",
            DaughterLists=[self.selLambdaDD, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaPipPimPimDD_Line = StrippingLine(
            self.name + "Xibm2LambdaPipPimPimDD_Line",
            algos=[Xibm2LambdaPipPimPim_DD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaPipPimPimDD_Line)

        ### Stripping Xi_b- -> Lambda0 K- K- pi+ ###
        Xibm2LambdaKmKmPip_LL = self.createCombination4body(
            OutputList="Xibm2LambdaKmKmPip_LL" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K- K- pi+]cc",
            DaughterLists=[self.selLambdaLL, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKmKmPipLL_Line = StrippingLine(
            self.name + "Xibm2LambdaKmKmPipLL_Line",
            algos=[Xibm2LambdaKmKmPip_LL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKmKmPipLL_Line)

        Xibm2LambdaKmKmPip_LD = self.createCombination4body(
            OutputList="Xibm2LambdaKmKmPip_LD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K- K- pi+]cc",
            DaughterLists=[self.selLambdaLD, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKmKmPipLD_Line = StrippingLine(
            self.name + "Xibm2LambdaKmKmPipLD_Line",
            algos=[Xibm2LambdaKmKmPip_LD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKmKmPipLD_Line)

        Xibm2LambdaKmKmPip_DD = self.createCombination4body(
            OutputList="Xibm2LambdaKmKmPip_DD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K- K- pi+]cc",
            DaughterLists=[self.selLambdaDD, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKmKmPipDD_Line = StrippingLine(
            self.name + "Xibm2LambdaKmKmPipDD_Line",
            algos=[Xibm2LambdaKmKmPip_DD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKmKmPipDD_Line)

        ### Stripping Xi_b- -> Lambda0 K+ K- pi- ###
        Xibm2LambdaKpKmPim_LL = self.createCombination4body(
            OutputList="Xibm2LambdaKpKmPim_LL" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K+ K- pi-]cc",
            DaughterLists=[self.selLambdaLL, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKpKmPimLL_Line = StrippingLine(
            self.name + "Xibm2LambdaKpKmPimLL_Line",
            algos=[Xibm2LambdaKpKmPim_LL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKpKmPimLL_Line)

        Xibm2LambdaKpKmPim_LD = self.createCombination4body(
            OutputList="Xibm2LambdaKpKmPim_LD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K+ K- pi-]cc",
            DaughterLists=[self.selLambdaLD, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKpKmPimLD_Line = StrippingLine(
            self.name + "Xibm2LambdaKpKmPimLD_Line",
            algos=[Xibm2LambdaKpKmPim_LD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKpKmPimLD_Line)

        Xibm2LambdaKpKmPim_DD = self.createCombination4body(
            OutputList="Xibm2LambdaKpKmPim_DD" + self.name,
            DecayDescriptor="[Xi_b- -> Lambda0 K+ K- pi-]cc",
            DaughterLists=[self.selLambdaDD, self.selKaon, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2LambdaKpKmPimDD_Line = StrippingLine(
            self.name + "Xibm2LambdaKpKmPimDD_Line",
            algos=[Xibm2LambdaKpKmPim_DD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2LambdaKpKmPimDD_Line)

        ### Stripping Xi_b- -> p+ KS0 pi- pi- ###
        Xibm2PpKS0PimPim_LL = self.createCombination4body(
            OutputList="Xibm2PpKS0PimPim_LL" + self.name,
            DecayDescriptor="[Xi_b- -> p+ KS0 pi- pi-]cc",
            DaughterLists=[self.selProton, self.selKS0LL, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2PpKS0PimPimLL_Line = StrippingLine(
            self.name + "Xibm2PpKS0PimPimLL_Line",
            algos=[Xibm2PpKS0PimPim_LL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2PpKS0PimPimLL_Line)

        Xibm2PpKS0PimPim_LD = self.createCombination4body(
            OutputList="Xibm2PpKS0PimPim_LD" + self.name,
            DecayDescriptor="[Xi_b- -> p+ KS0 pi- pi-]cc",
            DaughterLists=[self.selProton, self.selKS0LD, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2PpKS0PimPimLD_Line = StrippingLine(
            self.name + "Xibm2PpKS0PimPimLD_Line",
            algos=[Xibm2PpKS0PimPim_LD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2PpKS0PimPimLD_Line)

        Xibm2PpKS0PimPim_DD = self.createCombination4body(
            OutputList="Xibm2PpKS0PimPim_DD" + self.name,
            DecayDescriptor="[Xi_b- -> p+ KS0 pi- pi-]cc",
            DaughterLists=[self.selProton, self.selKS0DD, self.selPion],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2PpKS0PimPimDD_Line = StrippingLine(
            self.name + "Xibm2PpKS0PimPimDD_Line",
            algos=[Xibm2PpKS0PimPim_DD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2PpKS0PimPimDD_Line)
        ### Stripping Xi_b- -> p+ KS0 K- pi- ###
        Xibm2PpKS0KmPim_LL = self.createCombination4body(
            OutputList="Xibm2PpKS0KmPim_LL" + self.name,
            DecayDescriptor="[Xi_b- -> p+ KS0 K- pi-]cc",
            DaughterLists=[
                self.selProton, self.selKS0LL, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2PpKS0KmPimLL_Line = StrippingLine(
            self.name + "Xibm2PpKS0KmPimLL_Line",
            algos=[Xibm2PpKS0KmPim_LL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2PpKS0KmPimLL_Line)

        Xibm2PpKS0KmPim_LD = self.createCombination4body(
            OutputList="Xibm2PpKS0KmPim_LD" + self.name,
            DecayDescriptor="[Xi_b- -> p+ KS0 K- pi-]cc",
            DaughterLists=[
                self.selProton, self.selKS0LD, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2PpKS0KmPimLD_Line = StrippingLine(
            self.name + "Xibm2PpKS0KmPimLD_Line",
            algos=[Xibm2PpKS0KmPim_LD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2PpKS0KmPimLD_Line)

        Xibm2PpKS0KmPim_DD = self.createCombination4body(
            OutputList="Xibm2PpKS0KmPim_DD" + self.name,
            DecayDescriptor="[Xi_b- -> p+ KS0 K- pi-]cc",
            DaughterLists=[
                self.selProton, self.selKS0DD, self.selKaon, self.selPion
            ],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2PpKS0KmPimDD_Line = StrippingLine(
            self.name + "Xibm2PpKS0KmPimDD_Line",
            algos=[Xibm2PpKS0KmPim_DD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2PpKS0KmPimDD_Line)

        ### Stripping Xi_b- -> p+ KS0 K- K- ###
        Xibm2PpKS0KmKm_LL = self.createCombination4body(
            OutputList="Xibm2PpKS0KmKm_LL" + self.name,
            DecayDescriptor="[Xi_b- -> p+ KS0 K- K-]cc",
            DaughterLists=[self.selProton, self.selKS0LL, self.selKaon],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2PpKS0KmKmLL_Line = StrippingLine(
            self.name + "Xibm2PpKS0KmKmLL_Line",
            algos=[Xibm2PpKS0KmKm_LL],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2PpKS0KmKmLL_Line)

        Xibm2PpKS0KmKm_LD = self.createCombination4body(
            OutputList="Xibm2PpKS0KmKm_LD" + self.name,
            DecayDescriptor="[Xi_b- -> p+ KS0 K- K-]cc",
            DaughterLists=[self.selProton, self.selKS0LD, self.selKaon],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2PpKS0KmKmLD_Line = StrippingLine(
            self.name + "Xibm2PpKS0KmKmLD_Line",
            algos=[Xibm2PpKS0KmKm_LD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2PpKS0KmKmLD_Line)

        Xibm2PpKS0KmKm_DD = self.createCombination4body(
            OutputList="Xibm2PpKS0KmKm_DD" + self.name,
            DecayDescriptor="[Xi_b- -> p+ KS0 K- K-]cc",
            DaughterLists=[self.selProton, self.selKS0DD, self.selKaon],
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            PreVertexCuts=xibm_prevertex_cut,
            PostVertexCuts=xibm_postvertex_cut)
        Xibm2PpKS0KmKmDD_Line = StrippingLine(
            self.name + "Xibm2PpKS0KmKmDD_Line",
            algos=[Xibm2PpKS0KmKm_DD],
            EnableFlavourTagging=False,
            RelatedInfoTools=self.config['RelatedInfoTools'])
        self.registerLine(Xibm2PpKS0KmKmDD_Line)

    ##  --------------------  end of make_Xibm  ------------
    ##------------------------------------------------------------------------------------------

    ##########################################################################
    ## Combiners
    ##########################################################################

    def createCombination4body(
            self, OutputList, DecayDescriptor, DaughterLists, Combination12Cut,
            Combination123Cut, PreVertexCuts, PostVertexCuts):
        ###create a selection using a DaVinci__N4BodyDecays with a single decay descriptor###
        combiner = DaVinci__N4BodyDecays(
            DecayDescriptor=DecayDescriptor,
            Combination12Cut=Combination12Cut,
            Combination123Cut=Combination123Cut,
            CombinationCut=PreVertexCuts,
            MotherCut=PostVertexCuts,
            ReFitPVs=True)
        return Selection(
            OutputList, Algorithm=combiner, RequiredSelections=DaughterLists)
