###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""
B->Lambda0 Mu  reconstruction
 - Lambda0 -> MuPiX
 - Lambda0 -> MuMuX 
 - Lambda0 -> MuEX

Lambda0 == HNL
Lines stored in this file:
- StrippingB2MuHNL2MuPiXLine SS and OS
- StrippingB2MuHNL2MuMuXLine
- StrippingB2MuHNL2MuEXLine  SS and OS
"""

__author__ = ['Serhii Cholak','Martino Borsato', 'Federico Redi']
__date__ = '16/03/2021'
__version__ = '$Revision: 2.0 $'

__all__ = ('B2MuHNLLines','default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdAllNoPIDsPions, StdLoosePions, StdLooseMuons, StdLooseDownMuons, StdNoPIDsDownPions, StdLooseElectrons, StdNoPIDsDownElectrons
from PhysSelPython.Wrappers import CombineSelection, DataOnDemand, Selection, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME'        : 'B2MuHNL',
    'WGs'         : ['QEE'],
    'BUILDERTYPE' : 'B2MuHNLLines',
    'CONFIG'      : { "GEC_nLongTrk"          : 300.  ,#adimensional
                      #Muons
                      "MuonGHOSTPROB"         : 0.5   ,#adimensional
                      "MuonTRCHI2"            : 4.    ,#adimensional
                      "MuonP"                 : 3000. ,#MeV
                      "MuonPT"                : 250.  ,#MeV
                      "MuonPIDmu"             : 0.    ,#adimensional
                      "MuonMINIPCHI2"         : 12    ,#adminensional
                      #Lambda Daughter Cuts
                      "ElectronPIDe"          : 3.    ,#adimensional
                      "Lambda0DaugP"          : 2000. ,#MeV
                      "Lambda0DaugPT"         : 250.  ,#MeV
                      "Lambda0DaugTrackChi2"  : 4.    ,#adimensional
                      "Lambda0DaugMIPChi2"    : 10.   ,#adimensional
                      #Lambda cuts
                      "MajoranaCutFDChi2"     : 100.  ,#adimensional
                      "MajoranaCutBPVCORRMin"   : 1000. ,#MeV
                      "MajoranaCutBPVCORRMax"   : 6500. ,#MeV

                      "Lambda0VertexChi2"     : 16.   ,#adimensional
                      "Lambda0PT"             : 500.  ,#adimensional
                      #B Mother Cuts
                      "LambdaMuMassLowTight"  : 1500. ,#MeV
                      "XMuMassUpperHigh"      : 6500. ,#MeV
                      'LambdaZ_prompt'        : -1.,     #mm
                      'LambdaMuPiOSZ_displ'   : 15.,     #mm
                      'LambdaMuPiSSZ_displ'   : 15.,     #mm
                      'LambdaMuEZ_displ'      : -1.,     #mm

                      "HLT1"      : "HLT_PASS_RE('Hlt1.*TrackMuonDecision')",
                      "HLT2"      : "HLT_PASS_RE('Hlt2TopoMu2BodyDecision')"\
                                  "| HLT_PASS_RE('Hlt2Topo2BodyDecision')"\
                                  "| HLT_PASS_RE('Hlt2MajoranaBLambdaMuDDDecision')"\
                                  "| HLT_PASS_RE('Hlt2ExoticaRHNuDecision')", 



                      } ,
    'STREAMS' : ['Bhadron']
    }

class B2MuHNLLines(LineBuilder) :
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)
        self._name = name
        self._config = config

        self._stdLooseKsLL = DataOnDemand("Phys/StdLooseKsLL/Particles")
        self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}
        self.makeHNL2MuXX()

    ### Stripping lines:

    def makeHNL2MuXX( self ):

        HNL2MuXX_LLDD = {}
        HNL2MuXX_LL = {}
        HNL2MuXX_DD = {}

        decaysList ={
            ## mumuX
            "MuX":{ "tr_name"    : 'mu',
                   "hnldecays"   : ["Lambda0 -> mu- mu+"],
                   "bdecays"     : ["B- -> Lambda0 mu-", "B+ -> Lambda0 mu+"],
                   "tr_Long"     : self._muonFilter("Long_HNL2MuMu"),
                   "tr_Down"     : self._downMuonFilter("Down_HNL2MuMu"),
                   "HNL_FDZ"     : "%(LambdaZ_prompt)s" % self._config, 
                   "HNL_daugPID" : "",
                   },
            ## mupiX SS/OS 
            "PiXSS":{ "tr_name"  : 'pi',
                   "hnldecays"   : ["Lambda0 -> mu- pi+", "Lambda~0 -> mu+ pi-"],
                   "bdecays"  : ["[B- -> Lambda0 mu-]cc"],
                   "tr_Long"  : StdLoosePions,
                   "tr_Down"  : StdNoPIDsDownPions,
                   "HNL_FDZ"  : "%(LambdaMuPiSSZ_displ)s" % self._config,
                   "HNL_daugPID" : "", 
                   },
            "PiXOS":{ "tr_name"  : 'pi',
                   "hnldecays"   : ["Lambda0 -> mu+ pi-", "Lambda~0 -> mu- pi+"],
                   "bdecays"     : ["[B- -> Lambda0 mu-]cc"],
                   "tr_Long"     : StdLoosePions,
                   "tr_Down"     : StdNoPIDsDownPions,
                   "HNL_FDZ"     : "%(LambdaMuPiOSZ_displ)s" % self._config,
                   "HNL_daugPID" : "", 
                   },
            ## mueX SS/OS
            "EXSS":{ "tr_name"  : 'e',
                   "hnldecays"   : ["Lambda0 -> mu- e+", "Lambda~0 -> mu+ e-"],
                   "bdecays"     : ["[B- -> Lambda0 mu-]cc"],
                   "tr_Long"     : StdLooseElectrons,
                   "tr_Down"     : StdNoPIDsDownElectrons,
                   "HNL_FDZ"     : "%(LambdaZ_prompt)s" % self._config,
                   "HNL_daugPID" : " & (PIDe> %(ElectronPIDe)s)" % self._config, 
                   },
            "EXOS":{ "tr_name"   : 'e',
                   "hnldecays"   : ["Lambda0 -> mu+ e-", "Lambda~0 -> mu- e+"],
                   "bdecays"     : ["[B- -> Lambda0 mu-]cc"],
                   "tr_Long"     : StdLooseElectrons,
                   "tr_Down"     : StdNoPIDsDownElectrons,
                   "HNL_FDZ"     : "%(LambdaMuEZ_displ)s" % self._config, 
                   "HNL_daugPID" : " & (PIDe> %(ElectronPIDe)s)" % self._config,
                   },

        }

        for nn in decaysList:
            ## common algorithm for all HNLs
            _Lambda = CombineParticles(
                DecayDescriptors = decaysList[nn]['hnldecays'],
                DaughtersCuts   = {"%s+" %decaysList[nn]["tr_name"] :"(P > %(Lambda0DaugP)s)& (PT > %(Lambda0DaugPT)s)"\
                                   "& (TRCHI2DOF < %(Lambda0DaugTrackChi2)s)" \
                                   "& (MIPCHI2DV(PRIMARY) > %(Lambda0DaugMIPChi2)s)"   % self._config
                                   },
                CombinationCut  = "(ADOCACHI2CUT(25, ''))"% self._config,
                MotherCut       = "( BPVCORRM < %(MajoranaCutBPVCORRMax)s*MeV )&( BPVCORRM > %(MajoranaCutBPVCORRMin)s*MeV )&( BPVVDCHI2 > %(MajoranaCutFDChi2)s )&( VFASPF(VCHI2/VDOF) < %(Lambda0VertexChi2)s )&( PT > %(Lambda0PT)s*MeV )" % self._config
                )
            _Lambda.DaughtersCuts["%s+" %decaysList[nn]["tr_name"]]+= decaysList[nn]["HNL_daugPID"]



            ## Combine HNL from the LongLong and DownDown tracks
            HNL2MuXX_LL[nn]=Selection("LongLong_HNL2Mu"+nn+self._name,
                                          Algorithm=_Lambda,
                                          RequiredSelections = [decaysList[nn]["tr_Long"], self._muonFilter("LongLong_HNL2Mu"+nn)])
            HNL2MuXX_DD[nn]=Selection("DownDown_HNL2Mu"+nn+self._name,
                                          Algorithm=_Lambda,
                                          RequiredSelections = [decaysList[nn]["tr_Down"], self._downMuonFilter("DownDown_HNL2Mu"+nn)])
            
            HNL2MuXX_LLDD[nn] = MergedSelection("HNL2MuXX_LLDD"+nn + self._name,
                                      RequiredSelections = [ HNL2MuXX_LL[nn], HNL2MuXX_DD[nn] ])
            
        B2MuHNLs,_B2MuHNLLines = {},{}
        for key in HNL2MuXX_LLDD:

            B2MuHNLs[key] = self.makeB2MuHNL( name = "B2MuHNL2Mu"+key + "X"+"Selection", 
                                              bdecays = decaysList[key]['bdecays'], 
                                              HNL_FDZ = decaysList[key]["HNL_FDZ"],
                                              inputs = [self._muonFilter("B2MuHNL2Mu"+key), HNL2MuXX_LLDD[key]])

            if decaysList[key]['tr_name'] == 'pi':
                _B2MuHNLLines[key] = StrippingLine( self._name + "2Mu" + key + "Line", 
                                                algos = [ B2MuHNLs[key] ],
                                                FILTER=self.GECs,
                                                HLT1 = self._config['HLT1'],
                                                HLT2 = self._config['HLT2'],
                                                prescale = 1.0, )
            else:
                _B2MuHNLLines[key] = StrippingLine( self._name + "2Mu" + key + "Line", 
                                                algos = [ B2MuHNLs[key] ],
                                                FILTER=self.GECs,
                                                prescale = 1.0, )

        for key in _B2MuHNLLines: self.registerLine(_B2MuHNLLines[key])

    def makeB2MuHNL(self, name, bdecays, HNL_FDZ,inputs):
        
        decay = bdecays
        comboCuts = "(AM>%(LambdaMuMassLowTight)s*MeV) & (AM<%(XMuMassUpperHigh)s*MeV)" % self._config
        momCuts = "( MINTREE((ABSID=='Lambda0'),VFASPF(VZ)) - VFASPF(VZ) > %s *mm )" % HNL_FDZ

        return  CombineSelection( name,
                                  inputs,
                                  DecayDescriptors = decay,
                                  # DaughtersCuts  = daughterCuts,
                                  CombinationCut = comboCuts,
                                  MotherCut = momCuts,
                                  ReFitPVs = True,
                                  )




    def _NominalMuSelection( self ):
        return "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonP)s *MeV) &  (PT> %(MuonPT)s* MeV)"\
               "& (TRGHOSTPROB < %(MuonGHOSTPROB)s)"\
               "& (PIDmu> %(MuonPIDmu)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(MuonMINIPCHI2)s )"
  
               
    def _downMuonFilter( self, name ):
        _mu = FilterDesktop( Code = self._NominalMuSelection() % self._config )
        _muSel=Selection("downMuon_for"+name,
                         Algorithm=_mu,
                         RequiredSelections = [StdLooseDownMuons])
        return _muSel

    def _muonFilter( self, name ):
        _mu = FilterDesktop( Code = self._NominalMuSelection() % self._config )
        _muSel=Selection("Muon_for"+name,
                         Algorithm=_mu,
                         RequiredSelections = [StdLooseMuons])
        return _muSel
# EOF
