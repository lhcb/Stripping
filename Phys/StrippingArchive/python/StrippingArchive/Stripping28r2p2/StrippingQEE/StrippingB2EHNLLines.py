###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""
B->Lambda0 E  reconstruction
 - Lambda0 -> EPiX
 - Lambda0 -> EEX 
 - Lambda0 -> MuEX

Lambda0 == HNL
Lines stored in this file:
- StrippingB2EHNL2EPiXLine SS and OS
- StrippingB2EHNL2EEXLine
- StrippingB2EHNL2MuEXLine  SS and OS
"""

__author__ = ['Serhii Cholak','Martino Borsato', 'Federico Redi']
__date__ = '16/03/2021'
__version__ = '$Revision: 2.0 $'

__all__ = ('B2EHNLLines','default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdAllNoPIDsPions, StdLoosePions, StdLooseMuons, StdLooseDownMuons, StdNoPIDsDownPions, StdLooseElectrons, StdNoPIDsDownElectrons
from PhysSelPython.Wrappers import CombineSelection, DataOnDemand, Selection, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME'        : 'B2EHNL',
    'WGs'         : ['QEE'],
    'BUILDERTYPE' : 'B2EHNLLines',
    'CONFIG'      : { "GEC_nLongTrk"          : 300.  ,#adimensional
                      #Muons
                      "MuonGHOSTPROB"         : 0.5   ,#adimensional
                      "MuonTRCHI2"            : 4.    ,#adimensional
                      "MuonP"                 : 3000. ,#MeV
                      "MuonPT"                : 250.  ,#MeV
                      "MuonPIDmu"             : 0.    ,#adimensional
                      "MuonMINIPCHI2"         : 12    ,#adminensional

                      "ElectronGHOSTPROB"         : 0.5   ,#adimensional
                      "ElectronTRCHI2"            : 4.    ,#adimensional
                      "ElectronP"                 : 3000. ,#MeV
                      "ElectronPT"                : 250.  ,#MeV
                      "ElectronPIDK"              : 3.0    ,#adimensional
                      "ElectronPIDpi"             : 3.0    ,#adimensional
                      "ElectronPIDp"              : 3.0    ,#adimensional
                      "ElectronMINIPCHI2"         : 12    ,#adminensional

                      #Lambda Daughter Cuts
                      "ElectronPIDe"          : 3.    ,#adimensional
                      "Lambda0DaugP"          : 2000. ,#MeV
                      "Lambda0DaugPT"         : 250.  ,#MeV
                      "Lambda0DaugTrackChi2"  : 4.    ,#adimensional
                      "Lambda0DaugMIPChi2"    : 10.   ,#adimensional
                      #Lambda cuts
                      "MajoranaCutFDChi2"     : 100.  ,#adimensional
                      "MajoranaCutBPVCORRMin"   : 1000. ,#MeV
                      "MajoranaCutBPVCORRMax"   : 6500. ,#MeV

                      "Lambda0VertexChi2"     : 16.   ,#adimensional
                      "Lambda0PT"             : 500.  ,#adimensional
                      #B Mother Cuts
                      "LambdaMuMassLowTight"  : 1500. ,#MeV
                      "BDIRA"                 : 0.99  ,#adminensional
                      "BVCHI2DOF"             : 16.    ,#adminensional
                      "XMuMassUpperHigh"      : 6500. ,#MeV
                      'LambdaZ_prompt'        : -1.,     #mm
                      'LambdaEPiOSZ_displ'   : 20.,     #mm
                      'LambdaEPiSSZ_displ'   : 20.,     #mm
                      'LambdaEEZ_displ'      : -1.,     #mm

                      "HLT1"      : "HLT_PASS_RE('Hlt1.*SingleElectronNoIPDecision')", #Hlt1SingleElectronNoIPDecision
                      "HLT2"      : "HLT_PASS_RE('Hlt2TopoE2BodyDecision')",
                      } ,
    'STREAMS' : ['Bhadron']
    }

class B2EHNLLines(LineBuilder) :
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)
        self._name = name
        self._config = config

        self._stdLooseKsLL = DataOnDemand("Phys/StdLooseKsLL/Particles")
        self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}
        self.makeHNL2EXX()

    ### Stripping lines:

    def makeHNL2EXX( self ):

        HNL2EXX_LLDD = {}
        HNL2EXX_LL = {}
        HNL2EXX_DD = {}

        decaysList ={
            ## eeX
            "EX":{ "tr_name"    : 'e',
                   "hnldecays"   : ["Lambda0 -> e- e+"],
                   "bdecays"     : ["B- -> Lambda0 e-", "B+ -> Lambda0 e+"],
                   "tr_Long"     : self._electronFilter("Long_HNL2EE"),
                   "tr_Down"     : self._downElectronFilter("Down_HNL2EE"),
                   "HNL_FDZ"     : "%(LambdaZ_prompt)s" % self._config, 
                   "HNL_daugPID" : "",
                   },
            ## epiX SS/OS 
            "PiXSS":{ "tr_name"  : 'pi',
                   "hnldecays"   : ["Lambda0 -> e- pi+", "Lambda~0 -> e+ pi-"],
                   "bdecays"  : ["[B- -> Lambda0 e-]cc"],
                   "tr_Long"  : StdLoosePions,
                   "tr_Down"  : StdNoPIDsDownPions,
                   "HNL_FDZ"  : "%(LambdaEPiSSZ_displ)s" % self._config,
                   "HNL_daugPID" : "", 
                   },
            "PiXOS":{ "tr_name"  : 'pi',
                   "hnldecays"   : ["Lambda0 -> e+ pi-", "Lambda~0 -> e- pi+"],
                   "bdecays"     : ["[B- -> Lambda0 e-]cc"],
                   "tr_Long"     : StdLoosePions,
                   "tr_Down"     : StdNoPIDsDownPions,
                   "HNL_FDZ"     : "%(LambdaEPiOSZ_displ)s" % self._config,
                   "HNL_daugPID" : "", 
                   },
            ## mueX SS/OS
            "MuXSS":{ "tr_name"  : 'e',
                   "hnldecays"   : ["Lambda0 -> mu- e+", "Lambda~0 -> mu+ e-"],
                   "bdecays"     : ["[B- -> Lambda0 e-]cc"],
                   "tr_Long"     : self._muonFilter("Long_HNL2EMuSS"),
                   "tr_Down"     : self._downMuonFilter("Down_HNL2EMuSS"),
                   "HNL_FDZ"     : "%(LambdaZ_prompt)s" % self._config,
                   "HNL_daugPID" : "", 

                   },
            "MuXOS":{ "tr_name"   : 'e',
                   "hnldecays"   : ["Lambda0 -> mu- e+", "Lambda~0 -> mu+ e-"],
                   "bdecays"     : ["[B- -> Lambda0 e-]cc"],
                   "tr_Long"     : self._muonFilter("Long_HNL2EMuOS"),
                   "tr_Down"     : self._downMuonFilter("Down_HNL2EMuOS"),
                   "HNL_FDZ"     : "%(LambdaEEZ_displ)s" % self._config, 
                   "HNL_daugPID" : "",
                   },

        }

        for nn in decaysList:
            ## common algorithm for all HNLs
            _Lambda = CombineParticles(
                DecayDescriptors = decaysList[nn]['hnldecays'],
                DaughtersCuts   = {"%s+" %decaysList[nn]["tr_name"] :"(P > %(Lambda0DaugP)s)& (PT > %(Lambda0DaugPT)s)"\
                                   "& (TRCHI2DOF < %(Lambda0DaugTrackChi2)s)" \
                                   "& (MIPCHI2DV(PRIMARY) > %(Lambda0DaugMIPChi2)s)"   % self._config
                                   },
                CombinationCut  = "(ADOCACHI2CUT(25, ''))"% self._config,
                MotherCut       = "( BPVCORRM < %(MajoranaCutBPVCORRMax)s*MeV )&( BPVCORRM > %(MajoranaCutBPVCORRMin)s*MeV )&( BPVVDCHI2 > %(MajoranaCutFDChi2)s )&( VFASPF(VCHI2/VDOF) < %(Lambda0VertexChi2)s )&( PT > %(Lambda0PT)s*MeV )" % self._config
                )
            _Lambda.DaughtersCuts["%s+" %decaysList[nn]["tr_name"]]+= decaysList[nn]["HNL_daugPID"]



            ## Combine HNL from the LongLong and DownDown tracks
            HNL2EXX_LL[nn]=Selection("LongLong_HNL2E"+nn+self._name,
                                          Algorithm=_Lambda,
                                          RequiredSelections = [decaysList[nn]["tr_Long"], self._electronFilter("LongLong_HNL2E"+nn)])
            HNL2EXX_DD[nn]=Selection("DownDown_HNL2E"+nn+self._name,
                                          Algorithm=_Lambda,
                                          RequiredSelections = [decaysList[nn]["tr_Down"], self._downElectronFilter("DownDown_HNL2E"+nn)])
            
            HNL2EXX_LLDD[nn] = MergedSelection("HNL2EXX_LLDD"+nn + self._name,
                                      RequiredSelections = [ HNL2EXX_LL[nn], HNL2EXX_DD[nn] ])
            
        B2EHNLs,_B2EHNLLines = {},{}
        for key in HNL2EXX_LLDD:

            B2EHNLs[key] = self.makeB2EHNL( name = "B2EHNL2E"+key + "X"+"Selection", 
                                              bdecays = decaysList[key]['bdecays'], 
                                              HNL_FDZ = decaysList[key]["HNL_FDZ"],
                                              inputs = [self._electronFilter("B2EHNL2E"+key), HNL2EXX_LLDD[key]])

            _B2EHNLLines[key] = StrippingLine( self._name + "2E" + key + "Line", 
                                                algos = [ B2EHNLs[key] ],
                                                FILTER=self.GECs,

                                                prescale = 1.0, )

        for key in _B2EHNLLines: self.registerLine(_B2EHNLLines[key])

    def makeB2EHNL(self, name, bdecays, HNL_FDZ,inputs):
        
        decay = bdecays
        comboCuts = "(AM>%(LambdaMuMassLowTight)s*MeV) & (AM<%(XMuMassUpperHigh)s*MeV)" % self._config
        momCuts = "( MINTREE((ABSID=='Lambda0'),VFASPF(VZ)) - VFASPF(VZ) > %s *mm )" % HNL_FDZ
        momCuts += "& (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)" % self._config


        return  CombineSelection( name,
                                  inputs,
                                  DecayDescriptors = decay,
                                  # DaughtersCuts  = daughterCuts,
                                  CombinationCut = comboCuts,
                                  MotherCut = momCuts,
                                  ReFitPVs = True,
                                  )



    ## Muons
    def _NominalMuSelection( self ):
        return "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonP)s *MeV) &  (PT> %(MuonPT)s* MeV)"\
               "& (TRGHOSTPROB < %(MuonGHOSTPROB)s)"\
               "& (PIDmu> %(MuonPIDmu)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(MuonMINIPCHI2)s )"
  
               
    def _downMuonFilter( self, name ):
        _mu = FilterDesktop( Code = self._NominalMuSelection() % self._config )
        _muSel=Selection("downMuon_for"+name,
                         Algorithm=_mu,
                         RequiredSelections = [StdLooseDownMuons])
        return _muSel

    def _muonFilter( self, name ):
        _mu = FilterDesktop( Code = self._NominalMuSelection() % self._config )
        _muSel=Selection("Muon_for"+name,
                         Algorithm=_mu,
                         RequiredSelections = [StdLooseMuons])
        return _muSel

    ## Electrons
    def _NominalESelection( self ):
            return "(TRCHI2DOF < %(ElectronTRCHI2)s ) &  (P> %(ElectronP)s *MeV) &  (PT> %(ElectronPT)s* MeV)"\
                   "& (TRGHOSTPROB < %(ElectronGHOSTPROB)s)"\
                   "& (PIDe > %(ElectronPIDe)s )"\
                   "& (MIPCHI2DV(PRIMARY)> %(ElectronMINIPCHI2)s )"
    def _downElectronFilter( self, name ):
            
        _e = FilterDesktop( Code = self._NominalESelection() % self._config )
        _eSel=Selection("downElectron_for"+name,
                             Algorithm=_e,
                             RequiredSelections = [StdNoPIDsDownElectrons])
        return _eSel

    def _electronFilter( self, name ):
        
        _e = FilterDesktop( Code = self._NominalESelection() % self._config )
        _eSel=Selection("Electron_for"+name,
                         Algorithm=_e,
                         RequiredSelections = [StdLooseElectrons])
        return _eSel
# EOF
