###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from StandardParticles import StdLooseAllPhotons
from StandardParticles import StdLooseKaons, StdLoosePions
from StandardParticles import StdNoPIDsPions
from StandardParticles import StdNoPIDsKaons
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection, CombineSelection
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays

from GaudiKernel.SystemOfUnits import MeV
from GaudiKernel.SystemOfUnits import mm

__author__ = [' L. Pescatore', 'F. Blanc', 'A.Venkateswaran']
__date__ = '14/07/2023'
__version__ = '$Revision: 0.3$'

"""
  B->KTauTau, B->K1TauTau, B->RhoTauTau, B->Eta'TauTau, B -> D+ D- K+
  """
__all__ = (
    'B2XTauTauConf',
    'default_config'
)

default_config = {
    'NAME': 'B2XTauTau',
    'BUILDERTYPE': 'B2XTauTauConf',
    'WGs': ['RD'],
    'CONFIG': {
        'SpdMult': '600',
        #
        'UsePID': True,
        # Photon properties
        # 'Photon_PT_Min'                 : 1500.0,
        'Photon_Res_PT_Min': 400.0,
        'Photon_CL_Min': 0.2,
        # B properties
        'FD_B_Max': 80 * mm,
        'PT_B': 2000 * MeV,
        'P_B': 10000 * MeV,
        'VCHI2_B': 150,
        'FDCHI2_B': 16,
        'MASS_LOW_B': 1000 * MeV,
        'MASS_HIGH_B': 8000 * MeV,
        'M12_HIGH_BD2HTAUTAU': 7000 * MeV,
        'M12_HIGH_BU2K1TAUTAU': 6000 * MeV,
        'M12_HIGH_BU2KTAUTAU': 5000 * MeV,
        # Resonance properties
        'VCHI2_Etap': 16,
        'VCHI2_Rho': 16,
        'VCHI2_K1': 9,
        'PT_Etap': 1000 * MeV,
        'PT_Rho': 800 * MeV,
        'PT_K1': 800 * MeV,
        'MASS_LOW_Etap': 800 * MeV,
        'MASS_HIGH_Etap': 1100 * MeV,
        'MASS_LOW_Rho': 280 * MeV,
        'MASS_HIGH_Rho': 1100 * MeV,
        'MASS_LOW_K1': 400 * MeV,
        'MASS_HIGH_K1': 1700 * MeV,
        'M12_LOW_K1': 1700 * MeV,
        # final state track properties
        'PT_K': 1000 * MeV,
        'P_K': 4000 * MeV,
        'IPCHI2_Tr': 16,
        'TRGHOPROB_Tr': 0.5,
        # PID cuts
        'PROBNNK_K': 0.2,
        # DDK cuts
        'DDK_TRK_PT_MIN': 250 * MeV,
        'DDK_TRK_P_MIN': 2000 * MeV,
        'DDK_TRK_IPCHI2_MIN': 16.0,
        'DDK_TRK_CHI2DOF_MAX': 4,
        'DDK_TRK_TRGHOSTPROB_MAX': 0.4,
        'DDK_TRK_PROBNNPI_MIN': 0.55,
        'DDK_COMB_APT_MIN': 800 * MeV,
        'DDK_COMB_AM_MIN': 400 * MeV,
        'DDK_COMB_AM_MAX': 2100 * MeV,
        'DDK_COMB_AMAXDOCA_MAX': 0.2 * mm,
        'DDK_COMB_ANUMPT_CUTMIN': 800 * MeV,
        'DDK_COMB_ANUMPT_MINNUM': 1,
        'DDK_MOTHER_PT_MIN': 1000 * MeV,
        'DDK_MOTHER_M_MIN': 500 * MeV,
        'DDK_MOTHER_M_MAX': 2000 * MeV,
        'DDK_MOTHER_DIRA_MIN': 0.99,
        'DDK_MOTHER_VCHI2_MAX': 16,
        'DDK_MOTHER_VDCHI2_MIN': 16,
        'DDK_MOTHER_VDRHO_MIN': 0.1 * mm,
        'DDK_MOTHER_VDRHO_MAX': 7.0 * mm,
        'DDK_MOTHER_VDZ_MIN': 5.0 * mm,
        # prescales and postscales
        'B2HTauTau_LinePrescale': 1,
        'B2HTauTau_LinePostscale': 1,
        'B2KTauTau_LinePrescale': 1,
        'B2KTauTau_LinePostscale': 1,
        # DDK RelInfo
        'RelInfoTools_DDK':
        [
            {"Type": "RelInfoVertexIsolation",
             "Location": "BVars_VertexIsoInfo",
             "DaughterLocations": {  # "[Beauty -> ^K+  D+  D-]CC": "H_VertexIsoInfo",
                 "[Beauty ->  K+ ^D+  D-]CC": "Dp_VertexIsoInfo",
                 "[Beauty ->  K+  D+ ^D-]CC": "Dm_VertexIsoInfo"
             }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 0.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone05",
             "DaughterLocations": {"[Beauty -> ^K+  D+  D-]CC": "H_ConeIsoInfo_Cone05",
                                   "[Beauty ->  K+ ^D+  D-]CC": "Dp_ConeIsoInfo_Cone05",
                                   "[Beauty ->  K+  D+ ^D-]CC": "Dm_ConeIsoInfo_Cone05"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone10",
             "DaughterLocations": {"[Beauty -> ^K+  D+  D-]CC": "H_ConeIsoInfo_Cone10",
                                   "[Beauty ->  K+ ^D+  D-]CC": "Dp_ConeIsoInfo_Cone10",
                                   "[Beauty ->  K+  D+ ^D-]CC": "Dm_ConeIsoInfo_Cone10"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone15",
             "DaughterLocations": {"[Beauty -> ^K+  D+  D-]CC": "H_ConeIsoInfo_Cone15",
                                   "[Beauty ->  K+ ^D+  D-]CC": "Dp_ConeIsoInfo_Cone15",
                                   "[Beauty ->  K+  D+ ^D-]CC": "Dm_ConeIsoInfo_Cone15"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 2.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone20",
             "DaughterLocations": {"[Beauty -> ^K+  D+  D-]CC": "H_ConeIsoInfo_Cone20",
                                   "[Beauty ->  K+ ^D+  D-]CC": "Dp_ConeIsoInfo_Cone20",
                                   "[Beauty ->  K+  D+ ^D-]CC": "Dm_ConeIsoInfo_Cone20"
                                   }
             },
            {'Type': 'RelInfoVertexIsolationBDT',
             'Location': 'BVars_VertexIsoBDTInfo',
             'DaughterLocations': {"[Beauty -> ^K+  D+  D-]CC": "H_VertexIsoBDTInfo",
                                   "[Beauty ->  K+ ^D+  D-]CC": "Dp_VertexIsoBDTInfo",
                                   "[Beauty ->  K+  D+ ^D-]CC": "Dm_VertexIsoBDTInfo"
                                   }
             },
            {'Type': 'RelInfoTrackIsolationBDT',
             'Variables': 2,
             'WeightsFile':  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             'DaughterLocations': {"[Beauty ->  ^K+  D+  D-]CC": "H_TrackIsoBDTInfo",
                                   "[Beauty ->  K+  (D+ -> ^X+ X- X+)  D-]CC": "Dp_pi1_TrackIsoBDTInfo",
                                   "[Beauty ->  K+  (D+ -> X+ ^X- X+)  D-]CC": "Dp_pi2_TrackIsoBDTInfo",
                                   "[Beauty ->  K+  (D+ -> X+ X- ^X+)  D-]CC": "Dp_pi3_TrackIsoBDTInfo",
                                   "[Beauty ->  K+  D+  (D- -> ^X- X+ X-)]CC": "Dm_pi1_TrackIsoBDTInfo",
                                   "[Beauty ->  K+  D+  (D- -> X- ^X+ X-)]CC": "Dm_pi2_TrackIsoBDTInfo",
                                   "[Beauty ->  K+  D+  (D- -> X- X+ ^X-)]CC": "Dm_pi3_TrackIsoBDTInfo",
                                   }},

            {"Type": "RelInfoBKsttautauTauIsolationBDT",
             "Location": "B2KstTauTau_TauIsolationBDT"
             },
            # {"Type": "RelInfoBKsttautauTrackIsolationBDT",
            #  "Location": "B2KstTauTau_TrackIsolationBDT"
            # }

        ],
        # DDK SS RelInfo
        'RelInfoTools_DDKSS':
        [
            {"Type": "RelInfoVertexIsolation",
             "Location": "BVars_VertexIsoInfo",
             "DaughterLocations": {  # "[Beauty -> ^X  D+  D+]CC": "H_VertexIsoInfo",
                 "[Beauty ->  X ^D+  D+]CC": "Dp_VertexIsoInfo",
                 "[Beauty ->  X  D+ ^D+]CC": "Dm_VertexIsoInfo"
             }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 0.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone05",
             "DaughterLocations": {"[Beauty -> ^X  D+  D+]CC": "H_ConeIsoInfo_Cone05",
                                   "[Beauty ->  X ^D+  D+]CC": "Dp_ConeIsoInfo_Cone05",
                                   "[Beauty ->  X  D+ ^D+]CC": "Dm_ConeIsoInfo_Cone05"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone10",
             "DaughterLocations": {"[Beauty -> ^X  D+  D+]CC": "H_ConeIsoInfo_Cone10",
                                   "[Beauty ->  X ^D+  D+]CC": "Dp_ConeIsoInfo_Cone10",
                                   "[Beauty ->  X  D+ ^D+]CC": "Dm_ConeIsoInfo_Cone10"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone15",
             "DaughterLocations": {"[Beauty -> ^X  D+  D+]CC": "H_ConeIsoInfo_Cone15",
                                   "[Beauty ->  X ^D+  D+]CC": "Dp_ConeIsoInfo_Cone15",
                                   "[Beauty ->  X  D+ ^D+]CC": "Dm_ConeIsoInfo_Cone15"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 2.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone20",
             "DaughterLocations": {"[Beauty -> ^X  D+  D+]CC": "H_ConeIsoInfo_Cone20",
                                   "[Beauty ->  X ^D+  D+]CC": "Dp_ConeIsoInfo_Cone20",
                                   "[Beauty ->  X  D+ ^D+]CC": "Dm_ConeIsoInfo_Cone20"
                                   }
             },
            {'Type': 'RelInfoVertexIsolationBDT',
             'Location': 'BVars_VertexIsoBDTInfo',
             'DaughterLocations': {"[Beauty -> ^X  D+  D+]CC": "H_VertexIsoBDTInfo",
                                   "[Beauty ->  X ^D+  D+]CC": "Dp_VertexIsoBDTInfo",
                                   "[Beauty ->  X  D+ ^D+]CC": "Dm_VertexIsoBDTInfo"
                                   }
             },
            {'Type': 'RelInfoTrackIsolationBDT',
             'Variables': 2,
             'WeightsFile':  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             'DaughterLocations': {"[Beauty ->  ^X  D+  D+]CC": "H_TrackIsoBDTInfo",
                                   "[Beauty ->  X  (D+ -> ^X+ X- X+)  D+]CC": "Dp_pi1_TrackIsoBDTInfo",
                                   "[Beauty ->  X  (D+ -> X+ ^X- X+)  D+]CC": "Dp_pi2_TrackIsoBDTInfo",
                                   "[Beauty ->  X  (D+ -> X+ X- ^X+)  D+]CC": "Dp_pi3_TrackIsoBDTInfo",
                                   "[Beauty ->  X  D+  (D+ -> ^X+ X- X+)]CC": "Dm_pi1_TrackIsoBDTInfo",
                                   "[Beauty ->  X  D+  (D+ -> X+ ^X- X+)]CC": "Dm_pi2_TrackIsoBDTInfo",
                                   "[Beauty ->  X  D+  (D+ -> X+ X- ^X+)]CC": "Dm_pi3_TrackIsoBDTInfo",
                                   }},

            {"Type": "RelInfoBKsttautauTauIsolationBDT",
             "Location": "B2KstTauTau_TauIsolationBDT"
             },
            # {"Type": "RelInfoBKsttautauTrackIsolationBDT",
            #  "Location": "B2KstTauTau_TrackIsolationBDT"
            # }

        ],

        # XpTauTau RelInfo (cover K+TauTau, K1+TauTau)
        'RelInfoTools_XpTauTau':
        [
            {"Type": "RelInfoVertexIsolation",
             "Location": "BVars_VertexIsoInfo",
             "DaughterLocations": {"[Beauty ->  X+ ^tau+  tau-]CC": "Taup_VertexIsoInfo",
                                   "[Beauty ->  X+  tau+ ^tau-]CC": "Taum_VertexIsoInfo"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 0.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone05",
             "DaughterLocations": {"[Beauty -> ^X+  tau+  tau-]CC": "H_ConeIsoInfo_Cone05",
                                   "[Beauty ->  X+ ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone05",
                                   "[Beauty ->  X+  tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone05"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone10",
             "DaughterLocations": {"[Beauty -> ^X+  tau+  tau-]CC": "H_ConeIsoInfo_Cone10",
                                   "[Beauty ->  X+ ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone10",
                                   "[Beauty ->  X+  tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone10"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone15",
             "DaughterLocations": {"[Beauty -> ^X+  tau+  tau-]CC": "H_ConeIsoInfo_Cone15",
                                   "[Beauty ->  X+ ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone15",
                                   "[Beauty ->  X+  tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone15"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 2.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone20",
             "DaughterLocations": {"[Beauty -> ^X+  tau+  tau-]CC": "H_ConeIsoInfo_Cone20",
                                   "[Beauty ->  X+ ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone20",
                                   "[Beauty ->  X+  tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone20"
                                   }
             },
            {'Type': 'RelInfoVertexIsolationBDT',
             'Location': 'BVars_VertexIsoBDTInfo',
             'DaughterLocations': {"[Beauty -> ^X+  tau+  tau-]CC": "H_VertexIsoBDTInfo",
                                   "[Beauty -> X+  ^tau+  tau-]CC": "Taup_VertexIsoBDTInfo",
                                   "[Beauty -> X+  tau+  ^tau-]CC": "Taum_VertexIsoBDTInfo"
                                   }
             },
            {'Type': 'RelInfoTrackIsolationBDT',
             'Variables': 2,
             'WeightsFile':  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             'DaughterLocations': {"[Beauty ->  ^X+  tau+  tau-]CC": "H_TrackIsoBDTInfo",
                                   "[Beauty ->  X+  (tau+ -> ^pi+ pi- pi+)  tau-]CC": "Taup_pi1_TrackIsoBDTInfo",
                                   "[Beauty ->  X+  (tau+ -> pi+ ^pi- pi+)  tau-]CC": "Taup_pi2_TrackIsoBDTInfo",
                                   "[Beauty ->  X+  (tau+ -> pi+ pi- ^pi+)  tau-]CC": "Taup_pi3_TrackIsoBDTInfo",
                                   "[Beauty ->  X+  tau+  (tau- -> ^pi- pi+ pi-)]CC": "Taum_pi1_TrackIsoBDTInfo",
                                   "[Beauty ->  X+  tau+  (tau- -> pi- ^pi+ pi-)]CC": "Taum_pi2_TrackIsoBDTInfo",
                                   "[Beauty ->  X+  tau+  (tau- -> pi- pi+ ^pi-)]CC": "Taum_pi3_TrackIsoBDTInfo",
                                   }},

            {"Type": "RelInfoBKsttautauTauIsolationBDT",
             "Location": "B2KstTauTau_TauIsolationBDT"
             },
            # {"Type": "RelInfoBKsttautauTrackIsolationBDT",
            #  "Location": "B2KstTauTau_TrackIsolationBDT"
            # }

        ],
        # X0TauTau RelInfo (cover (phi/K*/rho)TauTau)
        'RelInfoTools_X0TauTau':
        [
            {"Type": "RelInfoVertexIsolation",
             "Location": "BVars_VertexIsoInfo",
             "DaughterLocations": {"[Beauty ->  X0 ^tau+  tau-]CC": "Taup_VertexIsoInfo",
                                   "[Beauty ->  X0  tau+ ^tau-]CC": "Taum_VertexIsoInfo"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 0.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone05",
             "DaughterLocations": {"[Beauty -> ^X0  tau+  tau-]CC": "H_ConeIsoInfo_Cone05",
                                   "[Beauty ->  X0 ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone05",
                                   "[Beauty ->  X0  tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone05"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone10",
             "DaughterLocations": {"[Beauty -> ^X0  tau+  tau-]CC": "H_ConeIsoInfo_Cone10",
                                   "[Beauty ->  X0 ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone10",
                                   "[Beauty ->  X0  tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone10"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone15",
             "DaughterLocations": {"[Beauty -> ^X0  tau+  tau-]CC": "H_ConeIsoInfo_Cone15",
                                   "[Beauty ->  X0 ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone15",
                                   "[Beauty ->  X0  tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone15"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 2.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone20",
             "DaughterLocations": {"[Beauty -> ^X0  tau+  tau-]CC": "H_ConeIsoInfo_Cone20",
                                   "[Beauty ->  X0 ^tau+  tau-]CC": "Taup_ConeIsoInfo_Cone20",
                                   "[Beauty ->  X0  tau+ ^tau-]CC": "Taum_ConeIsoInfo_Cone20"
                                   }
             },
            {'Type': 'RelInfoVertexIsolationBDT',
             'Location': 'BVars_VertexIsoBDTInfo',
             'DaughterLocations': {"[Beauty -> ^X0  tau+  tau-]CC": "H_VertexIsoBDTInfo",
                                   "[Beauty -> X0  ^tau+  tau-]CC": "Taup_VertexIsoBDTInfo",
                                   "[Beauty -> X0  tau+  ^tau-]CC": "Taum_VertexIsoBDTInfo"
                                   }
             },
            {'Type': 'RelInfoTrackIsolationBDT',
             'Variables': 2,
             'WeightsFile':  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             'DaughterLocations': {"[Beauty ->  ^X0  tau+  tau-]CC": "H_TrackIsoBDTInfo",
                                   "[Beauty ->  X0  (tau+ -> ^pi+ pi- pi+)  tau-]CC": "Taup_pi1_TrackIsoBDTInfo",
                                   "[Beauty ->  X0  (tau+ -> pi+ ^pi- pi+)  tau-]CC": "Taup_pi2_TrackIsoBDTInfo",
                                   "[Beauty ->  X0  (tau+ -> pi+ pi- ^pi+)  tau-]CC": "Taup_pi3_TrackIsoBDTInfo",
                                   "[Beauty ->  X0  tau+  (tau- -> ^pi- pi+ pi-)]CC": "Taum_pi1_TrackIsoBDTInfo",
                                   "[Beauty ->  X0  tau+  (tau- -> pi- ^pi+ pi-)]CC": "Taum_pi2_TrackIsoBDTInfo",
                                   "[Beauty ->  X0  tau+  (tau- -> pi- pi+ ^pi-)]CC": "Taum_pi3_TrackIsoBDTInfo",
                                   }},

            {"Type": "RelInfoBKsttautauTauIsolationBDT",
             "Location": "B2KstTauTau_TauIsolationBDT"
             },
        ],
        # XTauTau SS RelInfo
        'RelInfoTools_XTauTau_SS':
        [
            {"Type": "RelInfoVertexIsolation",
             "Location": "BVars_VertexIsoInfo",
             "DaughterLocations": {"[Beauty ->  X ^tau+  tau+]CC": "Taup_VertexIsoInfo",
                                   "[Beauty ->  X  tau+ ^tau+]CC": "Taum_VertexIsoInfo"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 0.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone05",
             "DaughterLocations": {"[Beauty -> ^X  tau+  tau+]CC": "H_ConeIsoInfo_Cone05",
                                   "[Beauty ->  X ^tau+  tau+]CC": "Taup_ConeIsoInfo_Cone05",
                                   "[Beauty ->  X  tau+ ^tau+]CC": "Taum_ConeIsoInfo_Cone05"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone10",
             "DaughterLocations": {"[Beauty -> ^X  tau+  tau+]CC": "H_ConeIsoInfo_Cone10",
                                   "[Beauty ->  X ^tau+  tau+]CC": "Taup_ConeIsoInfo_Cone10",
                                   "[Beauty ->  X  tau+ ^tau+]CC": "Taum_ConeIsoInfo_Cone10"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone15",
             "DaughterLocations": {"[Beauty -> ^X  tau+  tau+]CC": "H_ConeIsoInfo_Cone15",
                                   "[Beauty ->  X ^tau+  tau+]CC": "Taup_ConeIsoInfo_Cone15",
                                   "[Beauty ->  X  tau+ ^tau+]CC": "Taum_ConeIsoInfo_Cone15"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 2.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone20",
             "DaughterLocations": {"[Beauty -> ^X  tau+  tau+]CC": "H_ConeIsoInfo_Cone20",
                                   "[Beauty ->  X ^tau+  tau+]CC": "Taup_ConeIsoInfo_Cone20",
                                   "[Beauty ->  X  tau+ ^tau+]CC": "Taum_ConeIsoInfo_Cone20"
                                   }
             },
            {'Type': 'RelInfoVertexIsolationBDT',
             'Location': 'BVars_VertexIsoBDTInfo',
             'DaughterLocations': {"[Beauty -> ^X  tau+  tau+]CC": "H_VertexIsoBDTInfo",
                                   "[Beauty -> X  ^tau+  tau+]CC": "Taup_VertexIsoBDTInfo",
                                   "[Beauty -> X  tau+  ^tau+]CC": "Taum_VertexIsoBDTInfo"
                                   }
             },
            {'Type': 'RelInfoTrackIsolationBDT',
             'Variables': 2,
             'WeightsFile':  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             'DaughterLocations': {"[Beauty ->  ^X  tau+  tau+]CC": "H_TrackIsoBDTInfo",
                                   "[Beauty -> X  (tau+ -> ^X+ X- X+)  tau+]CC": "Taup_pi1_TrackIsoBDTInfo",
                                   "[Beauty -> X  (tau+ -> X+ ^X- X+)  tau+]CC": "Taup_pi2_TrackIsoBDTInfo",
                                   "[Beauty -> X  (tau+ -> X+ X- ^X+)  tau+]CC": "Taup_pi3_TrackIsoBDTInfo",
                                   "[Beauty -> X  tau+  (tau+ -> ^X+ X- X+)]CC": "Taum_pi1_TrackIsoBDTInfo",
                                   "[Beauty -> X  tau+  (tau+ -> X+ ^X- X+)]CC": "Taum_pi2_TrackIsoBDTInfo",
                                   "[Beauty -> X  tau+  (tau+ -> X+ X- ^X+)]CC": "Taum_pi3_TrackIsoBDTInfo",
                                   }},

            {"Type": "RelInfoBKsttautauTauIsolationBDT",
             "Location": "B2KstTauTau_TauIsolationBDT"
             },
        ],
    },
    'STREAMS': ['Bhadron']
}


class B2XTauTauConf(LineBuilder):

    """
      Builder for B->KstarTauTau, B->KTauTau, B->Eta'TauTau, B->K1TauTau
      """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        trackCuts = "(MIPCHI2DV(PRIMARY) > %(IPCHI2_Tr)s) & (TRGHOSTPROB < %(TRGHOPROB_Tr)s)" % config
        KCuts = trackCuts + " & (PT > %(PT_K)s) & (P > %(P_K)s)" % config
        if config['UsePID']:
            KCuts += " & (PROBNNk > %(PROBNNK_K)s)" % config

        self.FilterSPD = {
            'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                    'Preambulo': ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]
        }

        self.rawTau = DataOnDemand("Phys/StdTightDetachedTau3pi/Particles")

        self.rawTau_DDK = CombineSelection(
            'Combine_Tau3pi_DDK_B2XTauTau',
            [StdNoPIDsKaons, StdLoosePions],
            DecayDescriptor='[D+ -> K- pi+ pi+]cc',
            DaughtersCuts={
                'pi+': '(PT > %(DDK_TRK_PT_MIN)s) & (P > %(DDK_TRK_P_MIN)s) & (MIPCHI2DV(PRIMARY) > %(DDK_TRK_IPCHI2_MIN)s) & (TRCHI2DOF < %(DDK_TRK_CHI2DOF_MAX)s) & (TRGHOSTPROB < %(DDK_TRK_TRGHOSTPROB_MAX)s) & (PROBNNpi > %(DDK_TRK_PROBNNPI_MIN)s)' % config,
                'pi-': '(PT > %(DDK_TRK_PT_MIN)s) & (P > %(DDK_TRK_P_MIN)s) & (MIPCHI2DV(PRIMARY) > %(DDK_TRK_IPCHI2_MIN)s) & (TRCHI2DOF < %(DDK_TRK_CHI2DOF_MAX)s) & (TRGHOSTPROB < %(DDK_TRK_TRGHOSTPROB_MAX)s) & (PROBNNpi > %(DDK_TRK_PROBNNPI_MIN)s)' % config,
                'K+': '(PT > %(DDK_TRK_PT_MIN)s) & (P > %(DDK_TRK_P_MIN)s) & (MIPCHI2DV(PRIMARY) > %(DDK_TRK_IPCHI2_MIN)s) & (TRCHI2DOF < %(DDK_TRK_CHI2DOF_MAX)s) & (TRGHOSTPROB < %(DDK_TRK_TRGHOSTPROB_MAX)s)' % config,
                'K-': '(PT > %(DDK_TRK_PT_MIN)s) & (P > %(DDK_TRK_P_MIN)s) & (MIPCHI2DV(PRIMARY) > %(DDK_TRK_IPCHI2_MIN)s) & (TRCHI2DOF < %(DDK_TRK_CHI2DOF_MAX)s) & (TRGHOSTPROB < %(DDK_TRK_TRGHOSTPROB_MAX)s)' % config
            },
            CombinationCut="(APT > %(DDK_COMB_APT_MIN)s) & ((AM > %(DDK_COMB_AM_MIN)s) & (AM < %(DDK_COMB_AM_MAX)s)) & (AMAXDOCA('') < %(DDK_COMB_AMAXDOCA_MAX)s) & (ANUM(PT > %(DDK_COMB_ANUMPT_CUTMIN)s) >= %(DDK_COMB_ANUMPT_MINNUM)s)" % config,
            MotherCut='(PT > %(DDK_MOTHER_PT_MIN)s) & (M > %(DDK_MOTHER_M_MIN)s) & (M < %(DDK_MOTHER_M_MAX)s) & (BPVDIRA > %(DDK_MOTHER_DIRA_MIN)s) & (VFASPF(VCHI2) < %(DDK_MOTHER_VCHI2_MAX)s) & (BPVVDCHI2 > %(DDK_MOTHER_VDCHI2_MIN)s) & (BPVVDRHO > %(DDK_MOTHER_VDRHO_MIN)s) & (BPVVDRHO < %(DDK_MOTHER_VDRHO_MAX)s) & (BPVVDZ > %(DDK_MOTHER_VDZ_MIN)s)' % config
        )

        self.selKaon = SimpleSelection(
            "Kaon"+name, FilterDesktop, [StdLooseKaons], Code=KCuts)

        self.selTracks = SimpleSelection(
            "Tracks" + name, FilterDesktop, [StdNoPIDsPions], Code=trackCuts)

        # make resonances
        self.selEtap = self._makeEtap("Etap"+name, trackCuts, config)
        self.selRho = self._makeRho("Rho" + name, trackCuts, config)
        self.selK1 = self._makeK1("K1" + name, trackCuts, config)
        # self.selK1         = self._makeK1("K1" + name, self.selTracks, config)

        # make combinations for B decays
        self.selB2DDK = self._makeBu2KTauTau(
            name + "_DDK", self.rawTau_DDK, self.selKaon, config)
        self.selB2DDKSS = self._makeBu2KTauTau(
            name + "_DDK", self.rawTau_DDK, self.selKaon, config, SS=True)

        self.selB2RhoTauTau = self._makeBd2HTauTau(
            name + "_Rho", self.rawTau, self.selRho, config)
        self.selB2EtapTauTau = self._makeBd2HTauTau(
            name+"_Etap", self.rawTau, self.selEtap, config)
        self.selB2KTauTau = self._makeBu2KTauTau(
            name+"_K", self.rawTau, self.selKaon, config)
        self.selB2K1TauTau = self._makeBu2K1TauTau(
            name + "_K1", self.rawTau, self.selK1, config)

        self.selB2RhoTauTauSS = self._makeBd2HTauTau(
            name + "_Rho", self.rawTau, self.selRho, config, SS=True)
        self.selB2EtapTauTauSS = self._makeBd2HTauTau(
            name+"_Etap", self.rawTau, self.selEtap, config, SS=True)
        self.selB2KTauTauSS = self._makeBu2KTauTau(
            name+"_K", self.rawTau, self.selKaon, config, SS=True)
        self.selB2K1TauTauSS = self._makeBu2K1TauTau(
            name + "_K1", self.rawTau, self.selK1, config, SS=True)

        # Finished making selections build and register lines
        self.DDK_Line = self._makeLine(
            "B2KTauTau_DDKLine", self.selB2DDK, config)
        self.DDKSS_Line = self._makeLine(
            "B2KTauTau_DDKSSLine", self.selB2DDKSS, config)

        self.RhoTauTau_Line = self._makeLine(
            "B2RhoTauTauLine", self.selB2RhoTauTau, config)
        self.EtapTauTau_Line = self._makeLine(
            "B2EtapTauTauLine", self.selB2EtapTauTau, config)
        self.KTauTau_Line = self._makeLine(
            "B2KTauTauLine", self.selB2KTauTau, config)
        self.K1TauTau_Line = self._makeLine(
            "B2K1TauTauLine", self.selB2K1TauTau, config)

        self.RhoTauTauSS_Line = self._makeLine(
            "B2RhoTauTauSSLine", self.selB2RhoTauTauSS, config)
        self.EtapTauTauSS_Line = self._makeLine(
            "B2EtapTauTauSSLine", self.selB2EtapTauTauSS, config)
        self.KTauTauSS_Line = self._makeLine(
            "B2KTauTauSSLine", self.selB2KTauTauSS, config)
        self.K1TauTauSS_Line = self._makeLine(
            "B2K1TauTauSSLine", self.selB2K1TauTauSS, config)

    # Make resonances ###################################################

    def _makeRho(self, name, trackSel, config):

        combcut = "in_range ( %(MASS_LOW_Rho)s, AM, %(MASS_HIGH_Rho)s )" % config
        mothercut = " (PT > %(PT_Rho)s) & (VFASPF(VCHI2) < %(VCHI2_Rho)s)" % config

        daucut = {'pi+': trackSel, 'pi-': trackSel}

        return SimpleSelection(name + "_Rho",
                               CombineParticles, [StdNoPIDsPions],
                               DecayDescriptors=["rho(770)0 -> pi+ pi-"],
                               CombinationCut=combcut,
                               MotherCut=mothercut,
                               DaughtersCuts=daucut)

    def _makeK1(self, name, trackSel, config):

        combcut = "in_range ( %(MASS_LOW_K1)s, AM, %(MASS_HIGH_K1)s )" % config
        mothercut = " (PT > %(PT_K1)s) & (VFASPF(VCHI2) < %(VCHI2_K1)s)" % config

        tracks = SimpleSelection(
            "Tracks" + name, FilterDesktop, [StdNoPIDsPions], Code=trackSel)

        Combine = DaVinci__N3BodyDecays(DecayDescriptors=["[K_1(1270)+ -> pi+ pi- pi+]cc"],
                                        Combination12Cut="AM < %(M12_LOW_K1)s" % config,
                                        CombinationCut=combcut,
                                        MotherCut=mothercut)

        return Selection(name + "_K1", Algorithm=Combine, RequiredSelections=[tracks])

    def _makeEtap(self, name, trackSel, config):

        combcut = "(APT> %(PT_Etap)s) & (in_range ( %(MASS_LOW_Etap)s, AM, %(MASS_HIGH_Etap)s))" % config
        mothercut = "(VFASPF(VCHI2/VDOF) < %(VCHI2_Etap)s)" % config
        gammacut = "(PT > %(Photon_Res_PT_Min)s*MeV) & (CL > %(Photon_CL_Min)s)" % config
        daucut = {'pi+': trackSel, 'pi-': trackSel, 'gamma': gammacut}

        return SimpleSelection(name+"_Etap", CombineParticles, [StdNoPIDsPions, StdLooseAllPhotons],
                               DecayDescriptors=["eta_prime -> pi+ pi- gamma"],
                               CombinationCut=combcut,
                               MotherCut=mothercut,
                               DaughtersCuts=daucut)

# Make B ###################################################

    def _makeBd2HTauTau(self, name, tauSel, HSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( VFASPF(VCHI2) < %(VCHI2_B)s ) & ( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config

        part = "eta_prime"
        n = name+"_TauTau"
        if "Phi" in name:
            part = "phi(1020)"
        elif "Kstar" in name:
            part = "K*(892)0"
        elif "Rho" in name:
            part = "rho(770)0"

        descriptors = ["B_s0 -> %s tau+ tau-" % part]

        if "Kstar" in name:
            descriptors = ["[B_s0 -> %s tau+ tau-]cc" % part]

        if SS:
            n += "SS"
            descriptors = ["[B_s0 -> %s tau+ tau+]cc" %
                           part, "[B_s0 -> %s tau- tau-]cc" % part]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM < %(M12_HIGH_BD2HTAUTAU)s" % config,
            CombinationCut=combcut,
            MotherCut=mothercut
        )

        return Selection(n, Algorithm=Combine, RequiredSelections=[tauSel, HSel])

    def _makeBu2K1TauTau(self, name, tauSel, K1Sel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s)" % config

        n = name + "TauTau"
        descriptors = ["[B+ -> K_1(1270)+ tau+ tau-]cc"]
        if SS:
            n = name + "TauTauSS"
            descriptors = [
                "[B+ -> K_1(1270)- tau+ tau+]cc",
                "[B+ -> K_1(1270)+ tau+ tau+]cc"
            ]

        Combine = DaVinci__N3BodyDecays(DecayDescriptors=descriptors,
                                        Combination12Cut="AM < %(M12_HIGH_BU2K1TAUTAU)s" % config,
                                        CombinationCut=combcut,
                                        MotherCut=mothercut)

        return Selection(n, Algorithm=Combine, RequiredSelections=[tauSel, K1Sel])

    def _makeBu2KTauTau(self, name, tauSel, KSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( VFASPF(VCHI2) < %(VCHI2_B)s ) & ( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s)" % config

        n = name+"TauTau"
        descriptors = ["[B+ -> K+ tau+ tau-]cc"]
        if SS and "DDK" not in name:
            n = name+"TauTauSS"
            descriptors = ["[B+ -> K- tau+ tau+]cc",
                           "[B+ -> K+ tau+ tau+]cc"]

        if "DDK" in name:
            n = name+"DDK"
            descriptors = ["[B+ -> K+ D+ D-]cc"]
            if SS:
                n = name+"DDKSS"
                descriptors = ["[B+ -> K+ D+ D+]cc",
                               "[B+ -> K- D+ D+]cc"]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM < %(M12_HIGH_BU2KTAUTAU)s" % config,
            CombinationCut=combcut,
            MotherCut=mothercut
        )

        return Selection(n, Algorithm=Combine, RequiredSelections=[tauSel, KSel])

    # Helpers to make lines

    def _makeLine(self, name, sel, config):

        line = StrippingLine(name,
                             prescale=config['B2HTauTau_LinePrescale'],
                             postscale=config['B2HTauTau_LinePostscale'],
                             MDSTFlag=False,  # <====
                             FILTER=self.FilterSPD,
                             selection=sel,
                             MaxCandidates=200
                             )
        if 'DDKSS' in name:
            line.RelatedInfoTools = config['RelInfoTools_DDKSS']
        elif 'DDK' in name:
            line.RelatedInfoTools = config['RelInfoTools_DDK']
        elif "SS" in name:
            line.RelatedInfoTools = config['RelInfoTools_XTauTau_SS']
        elif "Rho" in name or "Etap" in name:
            line.RelatedInfoTools = config['RelInfoTools_X0TauTau']
        elif "K1TauTau" in name or "KTauTau" in name:
            line.RelatedInfoTools = config['RelInfoTools_XpTauTau']

        self.registerLine(line)
        return line
