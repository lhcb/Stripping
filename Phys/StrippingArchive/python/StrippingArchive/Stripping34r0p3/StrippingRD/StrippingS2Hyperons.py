###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping line for Omega- --> Lambda pi-, Xi0 --> p pi-, and Xi-/Omega- --> p pi- pi- (Delta S >= 2 forbidden modes, dubbed in the PDG as "S2 modes").
Also included Lambda0 --> p+ pi-, and Xi- -->Lambda0 pi- for normalisation. Adapted from the ChargedHyperons module (Mike Sokoloff, Laurence Carson).
'''
__author__ = ['Vitalii Lisovskyi', 'Frank Liu']
__date__ = '10/07/2023'
__version__ = '$Revision: 2.0 $'
__all__ = ('StrippingS2HyperonsConf', 'default_config')


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdLoosePions, StdNoPIDsDownPions, StdLooseKaons, StdLooseDownKaons
from StandardParticles import StdAllLoosePions, StdAllLooseKaons

from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from PhysSelPython.Wrappers import MultiSelectionSequence
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, mm, picosecond


default_name = 'S2Hyperons'
#### This is the dictionary of all tunable cuts ########
default_config = {
    'NAME':   'S2Hyperons',
    'WGs':   ['RD'],
    'BUILDERTYPE': 'StrippingS2HyperonsConf',
    'STREAMS': ['Charm'],
    'CONFIG': {
        'TRCHI2DOFMax': 3.0          # , 'PionPIDK'               :  5.0
        , 'ProtonProbNNp':  0.3, 'XiMassWindow':  25 * MeV, 'OmegaMassWindow':  30 * MeV, 'LambdaLLMinDecayTime':  2.0 * picosecond, 'LambdaLLVtxChi2Max':   5.0, 'LambdaDDVtxChi2Max':   5.0, 'LambdaLLMassWin': 8 * MeV, 'LambdaDDMassWin': 8 * MeV, 'LambdaLLMinVZ': -100. * mm, 'LambdaLLMaxVZ':  400. * mm, 'LambdaDDMinVZ':  400. * mm, 'LambdaDDMaxVZ': 2275. * mm, 'TrGhostProbMax':  0.25, 'KaonPIDK':  0, 'ProbNNpMinLL':  0.20, 'ProbNNpMinDD':  0.05, 'Bachelor_PT_MIN': 100 * MeV, 'Proton_PT_MIN': 1000 * MeV, 'Proton_P_MIN': 3000 * MeV, 'Bachelor_BPVIPCHI2_MIN': 40., 'XiDaughter_BPVIPCHI2_MIN': 50., 'LooseDaughter_BPVIPCHI2_MIN': 20., 'LambdaDeltaZ_MIN':  5.0 * mm, 'Hyperon_BPVLTIME_MIN':  5.0 * picosecond, 'PostVertexChi2_MIN':  5.0, 'LambdaPr_PT_MIN': 500. * MeV, 'LambdaPi_PT_MIN': 100. * MeV, 'CommonRelInfoTools': [{"Type": "RelInfoVertexIsolation", "Location": "VtxIsoInfo"}], 'PrescaleLambda02PPi': 0.05
    }  # end of 'CONFIG'
}  # end of default_config
# -------------------------------------------------------------------------------------------------------------


class StrippingS2HyperonsConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        self.LongPionsList = MergedSelection("LongPionsFor" + self.name,
                                             RequiredSelections=[DataOnDemand(Location="Phys/StdAllLoosePions/Particles")])

        self.DownstreamPionsList = MergedSelection("DownstreamPionsFor" + self.name,
                                                   RequiredSelections=[DataOnDemand(Location="Phys/StdNoPIDsDownPions/Particles")])

        self.LongProtonsList = MergedSelection("LongProtonsFor" + self.name,
                                               RequiredSelections=[DataOnDemand(Location="Phys/StdAllLooseProtons/Particles")])

        self.DownstreamProtonsList = MergedSelection("DownstreamProtonsFor" + self.name,
                                                     RequiredSelections=[DataOnDemand(Location="Phys/StdNoPIDsDownProtons/Particles")])

        self.LongKaonsList = MergedSelection("LongKaonsFor" + self.name,
                                             RequiredSelections=[DataOnDemand(Location="Phys/StdAllLooseKaons/Particles")])

        self.DownstreamKaonsList = MergedSelection("DownstreamKaonsFor" + self.name,
                                                   RequiredSelections=[DataOnDemand(Location="Phys/StdNoPIDsDownKaons/Particles")])

        self.GoodLongPionsList = self.createSubSel(OutputList="GoodLongPionsFor" + self.name,
                                                   InputList=self.LongPionsList,
                                                   Cuts="(TRCHI2DOF < %(TRCHI2DOFMax)s )"
                                                   " & (TRGHOSTPROB <%(TrGhostProbMax)s )" % self.config)  # \
        # " & (PIDK < %(PionPIDK)s )" % self.config )

        self.GoodDownstreamPionsList = self.createSubSel(OutputList="GoodDownstreamPionsFor" + self.name,
                                                         InputList=self.DownstreamPionsList,
                                                         Cuts="(TRCHI2DOF < %(TRCHI2DOFMax)s)" % self.config)  # \
        # " & (PIDK < %(PionPIDK)s )" % self.config )

        self.GoodLongProtonsList = self.createSubSel(OutputList="GoodLongProtonsFor" + self.name,
                                                     InputList=self.LongProtonsList,
                                                     Cuts="(TRCHI2DOF < %(TRCHI2DOFMax)s )"
                                                     " & (TRGHOSTPROB <%(TrGhostProbMax)s )" % self.config)  # \
        # " & (PIDK < %(PionPIDK)s )" % self.config )

        self.GoodDownstreamProtonsList = self.createSubSel(OutputList="GoodDownstreamProtonsFor" + self.name,
                                                           InputList=self.DownstreamProtonsList,
                                                           Cuts="(TRCHI2DOF < %(TRCHI2DOFMax)s)" % self.config)  # \
        # " & (PIDK < %(PionPIDK)s )" % self.config )

        self.GoodLongKaonsList = self.createSubSel(OutputList="GoodLongKaonsFor" + self.name,
                                                   InputList=self.LongKaonsList,
                                                   Cuts="(TRCHI2DOF < %(TRCHI2DOFMax)s )"
                                                   " & (TRGHOSTPROB < %(TrGhostProbMax)s )" % self.config)  # \
        # " & (PIDK > %(KaonPIDK)s )" % self.config )

        self.GoodDownstreamKaonsList = self.createSubSel(OutputList="GoodDownstreamKaonsFor" + self.name,
                                                         InputList=self.DownstreamKaonsList,
                                                         Cuts="(TRCHI2DOF < %(TRCHI2DOFMax)s )" % self.config)  # \
        # " & (PIDK > %(KaonPIDK)s )" % self.config )

        self.LambdaListLooseDD = MergedSelection("StdLooseDDLambdaFor" + self.name,
                                                 RequiredSelections=[DataOnDemand(Location="Phys/StdLooseLambdaDD/Particles")])

        self.LambdaListLooseLL = MergedSelection("StdLooseLLLambdaFor" + self.name,
                                                 RequiredSelections=[DataOnDemand(Location="Phys/StdLooseLambdaLL/Particles")])

        self.LambdaListLL = self.createSubSel(OutputList="LambdaLLFor" + self.name,
                                              InputList=self.LambdaListLooseLL,
                                              Cuts="(MAXTREE('p+'==ABSID, PT) > %(LambdaPr_PT_MIN)s ) "
                                              "& (MAXTREE('pi-'==ABSID, PT) > %(LambdaPi_PT_MIN)s ) "
                                              "& (MAXTREE('p+'==ABSID,PROBNNp) > %(ProbNNpMinLL)s ) "
                                              "& (MINTREE('pi-'==ABSID, TRGHOSTPROB) < %(TrGhostProbMax)s )"
                                              "& (MINTREE('p+'==ABSID, TRGHOSTPROB) < %(TrGhostProbMax)s )"
                                              "& (ADMASS('Lambda0') < %(LambdaLLMassWin)s ) "
                                              "& (VFASPF(VCHI2/VDOF) < %(LambdaLLVtxChi2Max)s ) "
                                              "& (VFASPF(VZ) > %(LambdaLLMinVZ)s ) "
                                              "& (VFASPF(VZ) < %(LambdaLLMaxVZ)s ) "
                                              "& (BPVLTIME() > %(LambdaLLMinDecayTime)s )" % self.config
                                              )

        self.LambdaListDD = self.createSubSel(OutputList="LambdaDDFor" + self.name,
                                              InputList=self.LambdaListLooseDD,
                                              Cuts="(MAXTREE('p+'==ABSID, PT) > %(LambdaPr_PT_MIN)s ) "
                                              "& (MAXTREE('pi-'==ABSID, PT) > %(LambdaPi_PT_MIN)s ) "
                                              "& (MAXTREE('p+'==ABSID, PROBNNp) > %(ProbNNpMinDD)s ) "
                                              "& (ADMASS('Lambda0') < %(LambdaDDMassWin)s ) "
                                              "& (VFASPF(VCHI2/VDOF) <  %(LambdaDDVtxChi2Max)s ) "
                                              "& (VFASPF(VZ) < %(LambdaDDMaxVZ)s ) "
                                              "& (VFASPF(VZ) > %(LambdaDDMinVZ)s )" % self.config
                                              )

        self.LambdaList = MergedSelection("LambdaFor" + self.name,
                                          RequiredSelections=[
                                              self.LambdaListLL, self.LambdaListDD]
                                          )

        self.XizeroList = self.makeXizero()
        self.LambdazeroList = self.makeLambdazero()
        self.XiminusControlList = self.makeXiminusControl()
        self.XiminusList = self.makeXiminus()
        self.OmegaminusList = self.makeOmegaminus()
        self.OmegaS3List = self.makeOmegaS3()

    def createSubSel(self, OutputList, InputList, Cuts):
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code=Cuts)
        return Selection(OutputList,
                         Algorithm=filter,
                         RequiredSelections=[InputList])

    def createCombinationSel(self, OutputList,
                             DecayDescriptor,
                             DaughterLists,
                             DaughterCuts={},
                             PreVertexCuts="AALL",
                             PostVertexCuts="ALL"):
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles(DecayDescriptor=DecayDescriptor,
                                    DaughtersCuts=DaughterCuts,
                                    MotherCut=PostVertexCuts,
                                    CombinationCut=PreVertexCuts,
                                    ReFitPVs=True)
        return Selection(OutputList,
                         Algorithm=combiner,
                         RequiredSelections=DaughterLists)


# ------------------------------------------------------------------------------------------

    def makeXizero(self):
        ''' Make a Xi0 candidate from long tracks '''
        Xi02PPiLL = self.createCombinationSel(OutputList="Xi02PPiLL" + self.name,
                                              DecayDescriptor="[Xi0 -> p+ pi-]cc",
                                              DaughterLists=[
                                                  self.GoodLongPionsList, self.GoodLongProtonsList],
                                              DaughterCuts={"pi-": "(PT> 1.5*%(Bachelor_PT_MIN)s ) &"
                                                            " (BPVIPCHI2() > %(XiDaughter_BPVIPCHI2_MIN)s )" % self.config,
                                                            "p+": "(PT> %(Proton_PT_MIN)s ) & (P> %(Proton_P_MIN)s ) & (PROBNNp > %(ProtonProbNNp)s) & "
                                                            " (BPVIPCHI2() > %(XiDaughter_BPVIPCHI2_MIN)s )" % self.config},
                                              PreVertexCuts="(ADAMASS('Xi0') < 2*%(XiMassWindow)s*MeV)" % self.config,
                                              PostVertexCuts="(VFASPF(VCHI2/VDOF)<%(PostVertexChi2_MIN)s ) & "
                                              " (BPVLTIME() > %(Hyperon_BPVLTIME_MIN)s)  & "
                                              "(ADMASS('Xi0') < %(XiMassWindow)s*MeV) & "
                                              "(BPVVDZ>0) " % self.config
                                              )

        ''' Make a Xi0 candidate  from downstream tracks'''
        Xi02PPiDD = self.createCombinationSel(OutputList="Xi02PPiDD" + self.name,
                                              DecayDescriptor="[Xi0 -> p+ pi-]cc",
                                              DaughterLists=[
                                                  self.GoodDownstreamPionsList, self.GoodDownstreamProtonsList],
                                              DaughterCuts={"pi-": "(PT> 2.5*%(Bachelor_PT_MIN)s ) &"
                                                            " (BPVIPCHI2() > %(XiDaughter_BPVIPCHI2_MIN)s )" % self.config,
                                                            "p+": "(PT> %(Proton_PT_MIN)s ) & (P> %(Proton_P_MIN)s ) & (PROBNNp > %(ProtonProbNNp)s ) & "
                                                            " (BPVIPCHI2() > %(XiDaughter_BPVIPCHI2_MIN)s )" % self.config},
                                              PreVertexCuts="(ADAMASS('Xi0') < 2*%(XiMassWindow)s*MeV)" % self.config,
                                              PostVertexCuts="(VFASPF(VCHI2/VDOF)< %(PostVertexChi2_MIN)s ) &"
                                              "(BPVLTIME() >  %(Hyperon_BPVLTIME_MIN)s ) & "
                                              "(ADMASS('Xi0') < %(XiMassWindow)s*MeV) & "
                                              "(BPVVDZ>0) " % self.config

                                              )
# Xi02PPi is a "Selection" object; MergedSelection passes everything which gets to it
# even when the output list is empty
        Xi02PPi = MergedSelection("Xi02PPi"+self.name,
                                  RequiredSelections=[Xi02PPiDD, Xi02PPiLL])

# NullFilter is a "FilterDesktop" object which is a type of "Algorithm"
# This one will pass all candidates
        NullFilter = FilterDesktop(Code="ALL")

# this is *also* a Selection, but it is "more selective"
# than  Xi02PPi in the sense that it passes only events when something
# is in the output list
        Xi02PPiSelection = Selection("Xi02PPiSelection"+self.name,
                                     Algorithm=NullFilter,
                                     RequiredSelections=[Xi02PPi])
        Xi02PPiLine = StrippingLine(self.name + "Xi02PPi",
                                    algos=[Xi02PPiSelection],
                                    RelatedInfoTools=self.config['CommonRelInfoTools'])
        self.registerLine(Xi02PPiLine)

# --------------------  end of makeXizero  ------------
# ------------------------------------------------------------------------------------------

    def makeLambdazero(self):
        ''' Make a Lambda0 candidate from long tracks '''
        Lambda02PPiLL = self.createCombinationSel(OutputList="Lambda02PPiLL" + self.name,
                                                  DecayDescriptor="[Lambda0 -> p+ pi-]cc",
                                                  DaughterLists=[
                                                      self.GoodLongPionsList, self.GoodLongProtonsList],
                                                  DaughterCuts={"pi-": "(PT> 1.5*%(Bachelor_PT_MIN)s ) &"
                                                                " (BPVIPCHI2() > %(XiDaughter_BPVIPCHI2_MIN)s )" % self.config,
                                                                "p+": "(PT> %(Proton_PT_MIN)s ) & (P> %(Proton_P_MIN)s ) & (PROBNNp > %(ProtonProbNNp)s) & "
                                                                " (BPVIPCHI2() > %(XiDaughter_BPVIPCHI2_MIN)s )" % self.config},
                                                  PreVertexCuts="(ADAMASS('Lambda0') < 2*%(XiMassWindow)s*MeV)" % self.config,
                                                  PostVertexCuts="(VFASPF(VCHI2/VDOF)<%(PostVertexChi2_MIN)s ) & "
                                                  " (BPVLTIME() > %(LambdaLLMinDecayTime)s)  & "
                                                  "(ADMASS('Lambda0') < %(XiMassWindow)s*MeV) & "
                                                  "(BPVVDZ>0) " % self.config
                                                  )

        ''' Make a Lambda0 candidate  from downstream tracks'''
        Lambda02PPiDD = self.createCombinationSel(OutputList="Lambda02PPiDD" + self.name,
                                                  DecayDescriptor="[Lambda0 -> p+ pi-]cc",
                                                  DaughterLists=[
                                                      self.GoodDownstreamPionsList, self.GoodDownstreamProtonsList],
                                                  DaughterCuts={"pi-": "(PT> 2.5*%(Bachelor_PT_MIN)s ) &"
                                                                " (BPVIPCHI2() > %(XiDaughter_BPVIPCHI2_MIN)s )" % self.config,
                                                                "p+": "(PT> %(Proton_PT_MIN)s ) & (P> %(Proton_P_MIN)s ) & (PROBNNp > %(ProtonProbNNp)s ) & "
                                                                " (BPVIPCHI2() > %(XiDaughter_BPVIPCHI2_MIN)s )" % self.config},
                                                  PreVertexCuts="(ADAMASS('Lambda0') < 2*%(XiMassWindow)s*MeV)" % self.config,
                                                  PostVertexCuts="(VFASPF(VCHI2/VDOF)< %(PostVertexChi2_MIN)s ) &"
                                                  "(BPVLTIME() >  %(LambdaLLMinDecayTime)s ) & "
                                                  "(ADMASS('Lambda0') < %(XiMassWindow)s*MeV) & "
                                                  "(BPVVDZ>0) " % self.config

                                                  )
# Lambda02PPi is a "Selection" object; MergedSelection passes everything which gets to it
# even when the output list is empty
        Lambda02PPi = MergedSelection("Lambda02PPi"+self.name,
                                      RequiredSelections=[Lambda02PPiDD, Lambda02PPiLL])

# NullFilter is a "FilterDesktop" object which is a type of "Algorithm"
# This one will pass all candidates
        NullFilter = FilterDesktop(Code="ALL")

# this is *also* a Selection, but it is "more selective"
# than  Lambda02PPi in the sense that it passes only events when something
# is in the output list
        Lambda02PPiSelection = Selection("Lambda02PPiSelection"+self.name,
                                         Algorithm=NullFilter,
                                         RequiredSelections=[Lambda02PPi])
        Lambda02PPiLine = StrippingLine(self.name + "Lambda02PPi",
                                        algos=[Lambda02PPiSelection],
                                        RelatedInfoTools=self.config['CommonRelInfoTools'],
                                        prescale=self.config['PrescaleLambda02PPi'])
        self.registerLine(Lambda02PPiLine)

# --------------------  end of makeLambdazero  ------------
# ------------------------------------------------------------------------------------------

    def makeXiminusControl(self):
        ''' Make a Xi minus candidate from long tracks '''
        Xim2LambdaPiLL = self.createCombinationSel(OutputList="Xim2LambdaPiLL" + self.name,
                                                   DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
                                                   DaughterLists=[
                                                       self.GoodLongPionsList, self.LambdaListLL],
                                                   DaughterCuts={"pi-": "(PT> %(Bachelor_PT_MIN)s ) &"
                                                                 " (BPVIPCHI2() > %(LooseDaughter_BPVIPCHI2_MIN)s )" % self.config},
                                                   PreVertexCuts="(ADAMASS('Xi-') < 2*%(XiMassWindow)s*MeV)" % self.config,
                                                   PostVertexCuts="(VFASPF(VCHI2/VDOF)<%(PostVertexChi2_MIN)s ) & "
                                                   " (BPVLTIME() > %(Hyperon_BPVLTIME_MIN)s)  & "
                                                   "(ADMASS('Xi-') < %(XiMassWindow)s*MeV) & "
                                                   "(BPVVDZ>0) " % self.config
                                                   )

        ''' Make a Xi minus candidate  from downstream tracks'''
        Xim2LambdaPiDD = self.createCombinationSel(OutputList="Xim2LambdaPiDD" + self.name,
                                                   DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
                                                   DaughterLists=[
                                                       self.GoodDownstreamPionsList, self.LambdaListDD],
                                                   DaughterCuts={"pi-": "(PT> %(Bachelor_PT_MIN)s ) &"
                                                                 " (BPVIPCHI2() > %(LooseDaughter_BPVIPCHI2_MIN)s )" % self.config},
                                                   PreVertexCuts="(ADAMASS('Xi-') < 2*%(XiMassWindow)s*MeV)" % self.config,
                                                   PostVertexCuts="(VFASPF(VCHI2/VDOF)< %(PostVertexChi2_MIN)s ) &"
                                                   "(BPVLTIME() >  %(Hyperon_BPVLTIME_MIN)s ) & "
                                                   "(ADMASS('Xi-') < %(XiMassWindow)s*MeV) & "
                                                   "(BPVVDZ>0) " % self.config

                                                   )

        ''' Make a Xi minus candidate  from long pi tracks and downstream lambda0 tracks'''
        Xim2LambdaPiLD = self.createCombinationSel(OutputList="Xim2LambdaPiLD" + self.name,
                                                   DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
                                                   DaughterLists=[
                                                       self.GoodLongPionsList, self.LambdaListDD],
                                                   DaughterCuts={"pi-": "(PT> %(Bachelor_PT_MIN)s ) &"
                                                                 " (BPVIPCHI2() > %(LooseDaughter_BPVIPCHI2_MIN)s )" % self.config},
                                                   PreVertexCuts="(ADAMASS('Xi-') < 2*%(XiMassWindow)s*MeV)" % self.config,
                                                   PostVertexCuts="(VFASPF(VCHI2/VDOF)< %(PostVertexChi2_MIN)s ) &"
                                                   "(BPVLTIME() >  %(Hyperon_BPVLTIME_MIN)s ) & "
                                                   "(ADMASS('Xi-') < %(XiMassWindow)s*MeV) & "
                                                   "(BPVVDZ>0) " % self.config

                                                   )

# Xim2LambdaPi is a "Selection" object; MergedSelection passes everything which gets to it
# even when the output list is empty
        Xim2LambdaPi = MergedSelection("Xim2LambdaPi"+self.name,
                                       RequiredSelections=[Xim2LambdaPiDD, Xim2LambdaPiLL, Xim2LambdaPiLD])

# NullFilter is a "FilterDesktop" object which is a type of "Algorithm"
# This one will pass all candidates
        NullFilter = FilterDesktop(Code="ALL")

# this is *also* a Selection, but it is "more selective"
# than  Xim2LambdaPi in the sense that it passes only events when something
# is in the output list
        Xim2LambdaPiSelection = Selection("Xim2LambdaPiSelection"+self.name,
                                          Algorithm=NullFilter,
                                          RequiredSelections=[Xim2LambdaPi])
        Xim2LambdaPiLine = StrippingLine(self.name + "Xim2LambdaPi",
                                         algos=[Xim2LambdaPiSelection],
                                         RelatedInfoTools=self.config['CommonRelInfoTools'])
        self.registerLine(Xim2LambdaPiLine)

# --------------------  end of makeXiminusControl  ------------
# ------------------------------------------------------------------------------------------

    def makeOmegaminus(self):
        ''' Make an Omega minus candidate '''
        Omegaminus2LambdaPiLLL = self.createCombinationSel(OutputList="Omegaminus2LambdaPiLLL" + self.name,
                                                           DecayDescriptor="[Omega- -> Lambda0 pi-]cc",
                                                           DaughterLists=[
                                                               self.GoodLongPionsList, self.LambdaListLL],
                                                           DaughterCuts={"pi-": "(PT> %(Bachelor_PT_MIN)s ) &"
                                                                         " (BPVIPCHI2() > %(Bachelor_BPVIPCHI2_MIN)s )" % self.config},
                                                           PreVertexCuts="(ADAMASS('Omega-') < %(OmegaMassWindow)s*MeV)" % self.config,
                                                           PostVertexCuts="(VFASPF(VCHI2/VDOF)< %(PostVertexChi2_MIN)s) &"
                                                           "(BPVLTIME() > %(Hyperon_BPVLTIME_MIN)s) & "
                                                           "(BPVVDZ>0) & "
                                                           "(CHILD(VFASPF(VZ),1)-VFASPF(VZ) > %(LambdaDeltaZ_MIN)s )" % self.config
                                                           )

        ''' Make an Omega minus candidate '''
        Omegaminus2LambdaPiDDL = self.createCombinationSel(OutputList="Omegaminus2LambdaPiDDL" + self.name,
                                                           DecayDescriptor="[Omega- -> Lambda0 pi-]cc",
                                                           DaughterLists=[
                                                               self.GoodLongPionsList, self.LambdaListDD],
                                                           DaughterCuts={"pi-": "(PT> %(Bachelor_PT_MIN)s ) &"
                                                                         " (BPVIPCHI2() > %(Bachelor_BPVIPCHI2_MIN)s )" % self.config},
                                                           PreVertexCuts="(ADAMASS('Omega-') < %(OmegaMassWindow)s*MeV)" % self.config,
                                                           PostVertexCuts="(VFASPF(VCHI2/VDOF)< %(PostVertexChi2_MIN)s) &"
                                                           "(BPVLTIME() > %(Hyperon_BPVLTIME_MIN)s) & "
                                                           "(BPVVDZ>0) & "
                                                           "(CHILD(VFASPF(VZ),1)-VFASPF(VZ) > %(LambdaDeltaZ_MIN)s )" % self.config
                                                           )

        ''' Make an Omega minus candidate '''
        Omegaminus2LambdaPiDDD = self.createCombinationSel(OutputList="Omegaminus2LambdaPiDDD" + self.name,
                                                           DecayDescriptor="[Omega- -> Lambda0 pi-]cc",
                                                           DaughterLists=[
                                                               self.GoodDownstreamPionsList, self.LambdaListDD],
                                                           DaughterCuts={"pi-": "(PT> %(Bachelor_PT_MIN)s ) &"
                                                                         " (BPVIPCHI2() > %(Bachelor_BPVIPCHI2_MIN)s )" % self.config},
                                                           PreVertexCuts="(ADAMASS('Omega-') < %(OmegaMassWindow)s*MeV)" % self.config,
                                                           PostVertexCuts="(VFASPF(VCHI2/VDOF)< %(PostVertexChi2_MIN)s) &"
                                                           "(BPVLTIME() > %(Hyperon_BPVLTIME_MIN)s) & "
                                                           "(BPVVDZ>0) & "
                                                           "(CHILD(VFASPF(VZ),1)-VFASPF(VZ) > %(LambdaDeltaZ_MIN)s )" % self.config
                                                           )


# Omegaminus2LambdaPi is a "Selection" object; MergedSelection passes everything which gets to it
# even when the output list is empty

        Omegaminus2LambdaPi = MergedSelection("Omegaminus2LambdaPi"+self.name,
                                              # RequiredSelections = [Omegaminus2LambdaPiLLL,Omegaminus2LambdaPiDDL,Omegaminus2LambdaPiDDD] )
                                              RequiredSelections=[Omegaminus2LambdaPiDDL, Omegaminus2LambdaPiDDD, Omegaminus2LambdaPiLLL])
# NullFilter is a "FilterDesktop" object which is a type of "Algorithm"
# This one will pass all candidates
        NullFilter = FilterDesktop(Code="ALL")

# this is *also* a Selection, but it is "more selective"
# than  Omegaminus2LambdaPi in the sense that it passes only events when something
# is in the output list
        Omegaminus2LambdaPiSelection = Selection("Omegaminus2LambdaPiSelection"+self.name,
                                                 Algorithm=NullFilter,
                                                 RequiredSelections=[Omegaminus2LambdaPi])
        Omegaminus2LambdaPiLine = StrippingLine(self.name + "Omegaminus2LambdaPi",
                                                algos=[Omegaminus2LambdaPiSelection])
        self.registerLine(Omegaminus2LambdaPiLine)

# --------------------  end of makeOmegaminus  ------------
# ------------------------------------------------------------------------------------------

    def makeXiminus(self):
        ''' Make a Xi minus candidate from long tracks '''
        Xim2PPiPiLLL = self.createCombinationSel(OutputList="Xim2PPiPiLLL" + self.name,
                                                 DecayDescriptor="[Xi- -> p+ pi- pi-]cc",
                                                 DaughterLists=[
                                                     self.GoodLongPionsList, self.GoodLongProtonsList],
                                                 DaughterCuts={"pi-": "(PT> 1.5*%(Bachelor_PT_MIN)s ) &"
                                                               " (BPVIPCHI2() > %(LooseDaughter_BPVIPCHI2_MIN)s )" % self.config,
                                                               "p+": "(PT> %(Proton_PT_MIN)s ) & (P> %(Proton_P_MIN)s ) & (PROBNNp > %(ProtonProbNNp)s) & "
                                                               " (BPVIPCHI2() > %(LooseDaughter_BPVIPCHI2_MIN)s )" % self.config},
                                                 PreVertexCuts="(ADAMASS('Xi-') < 2*%(XiMassWindow)s*MeV)" % self.config,
                                                 PostVertexCuts="(VFASPF(VCHI2/VDOF)<%(PostVertexChi2_MIN)s ) & "
                                                 " (BPVLTIME() > %(Hyperon_BPVLTIME_MIN)s)  & "
                                                 "(ADMASS('Xi-') < %(XiMassWindow)s*MeV) & "
                                                 "(BPVVDZ>0) " % self.config
                                                 )

        ''' Make a Xi minus candidate  from downstream tracks'''
        Xim2PPiPiDDD = self.createCombinationSel(OutputList="Xim2PPiPiDDD" + self.name,
                                                 DecayDescriptor="[Xi- -> p+ pi- pi-]cc",
                                                 DaughterLists=[
                                                     self.GoodDownstreamPionsList, self.GoodDownstreamProtonsList],
                                                 DaughterCuts={"pi-": "(PT> 2.5*%(Bachelor_PT_MIN)s ) &"
                                                               " (BPVIPCHI2() > %(LooseDaughter_BPVIPCHI2_MIN)s )" % self.config,
                                                               "p+": "(PT> %(Proton_PT_MIN)s ) & (P> %(Proton_P_MIN)s ) & (PROBNNp > %(ProtonProbNNp)s ) & "
                                                               " (BPVIPCHI2() > %(LooseDaughter_BPVIPCHI2_MIN)s )" % self.config},
                                                 PreVertexCuts="(ADAMASS('Xi-') < 2*%(XiMassWindow)s*MeV)" % self.config,
                                                 PostVertexCuts="(VFASPF(VCHI2/VDOF)< %(PostVertexChi2_MIN)s ) &"
                                                 "(BPVLTIME() >  %(Hyperon_BPVLTIME_MIN)s ) & "
                                                 "(ADMASS('Xi-') < %(XiMassWindow)s*MeV) & "
                                                 "(BPVVDZ>0) " % self.config

                                                 )
# Xim2PPiPi is a "Selection" object; MergedSelection passes everything which gets to it
# even when the output list is empty
        Xim2PPiPi = MergedSelection("Xim2PPiPi"+self.name,
                                    RequiredSelections=[Xim2PPiPiDDD, Xim2PPiPiLLL])

# NullFilter is a "FilterDesktop" object which is a type of "Algorithm"
# This one will pass all candidates
        NullFilter = FilterDesktop(Code="ALL")

# this is *also* a Selection, but it is "more selective"
# than  Xim2PPiPi in the sense that it passes only events when something
# is in the output list
        Xim2PPiPiSelection = Selection("Xim2PPiPiSelection"+self.name,
                                       Algorithm=NullFilter,
                                       RequiredSelections=[Xim2PPiPi])
        Xim2PPiPiLine = StrippingLine(self.name + "Xim2PPiPi",
                                      algos=[Xim2PPiPiSelection])
        self.registerLine(Xim2PPiPiLine)

# --------------------  end of makeXiminus  ------------
# ------------------------------------------------------------------------------------------

    def makeOmegaS3(self):
        ''' Make an Omega minus candidate from long tracks '''
        Om2PPiPiLLL = self.createCombinationSel(OutputList="Om2PPiPiLLL" + self.name,
                                                DecayDescriptor="[Omega- -> p+ pi- pi-]cc",
                                                DaughterLists=[
                                                    self.GoodLongPionsList, self.GoodLongProtonsList],
                                                DaughterCuts={"pi-": "(PT> 1.5*%(Bachelor_PT_MIN)s ) &"
                                                              " (BPVIPCHI2() > %(LooseDaughter_BPVIPCHI2_MIN)s )" % self.config,
                                                              "p+": "(PT> %(Proton_PT_MIN)s ) & (P> %(Proton_P_MIN)s ) & (PROBNNp > %(ProtonProbNNp)s) & "
                                                              " (BPVIPCHI2() > %(LooseDaughter_BPVIPCHI2_MIN)s )" % self.config},
                                                PreVertexCuts="(ADAMASS('Omega-') < 2*%(OmegaMassWindow)s*MeV)" % self.config,
                                                PostVertexCuts="(VFASPF(VCHI2/VDOF)<%(PostVertexChi2_MIN)s ) & "
                                                " (BPVLTIME() > %(Hyperon_BPVLTIME_MIN)s)  & "
                                                "(ADMASS('Omega-') < %(OmegaMassWindow)s*MeV) & "
                                                "(BPVVDZ>0) " % self.config
                                                )

        ''' Make an Omega minus candidate  from downstream tracks'''
        Om2PPiPiDDD = self.createCombinationSel(OutputList="Om2PPiPiDDD" + self.name,
                                                DecayDescriptor="[Omega- -> p+ pi- pi-]cc",
                                                DaughterLists=[
                                                    self.GoodDownstreamPionsList, self.GoodDownstreamProtonsList],
                                                DaughterCuts={"pi-": "(PT> 2.5*%(Bachelor_PT_MIN)s ) &"
                                                              " (BPVIPCHI2() > %(LooseDaughter_BPVIPCHI2_MIN)s )" % self.config,
                                                              "p+": "(PT> %(Proton_PT_MIN)s ) & (P> %(Proton_P_MIN)s ) & (PROBNNp > %(ProtonProbNNp)s ) & "
                                                              " (BPVIPCHI2() > %(LooseDaughter_BPVIPCHI2_MIN)s )" % self.config},
                                                PreVertexCuts="(ADAMASS('Omega-') < 2*%(OmegaMassWindow)s*MeV)" % self.config,
                                                PostVertexCuts="(VFASPF(VCHI2/VDOF)< %(PostVertexChi2_MIN)s ) &"
                                                "(BPVLTIME() >  %(Hyperon_BPVLTIME_MIN)s ) & "
                                                "(ADMASS('Omega-') < %(OmegaMassWindow)s*MeV) & "
                                                "(BPVVDZ>0) " % self.config

                                                )
# Om2PPiPi is a "Selection" object; MergedSelection passes everything which gets to it
# even when the output list is empty
        Om2PPiPi = MergedSelection("Om2PPiPi"+self.name,
                                   RequiredSelections=[Om2PPiPiDDD, Om2PPiPiLLL])

# NullFilter is a "FilterDesktop" object which is a type of "Algorithm"
# This one will pass all candidates
        NullFilter = FilterDesktop(Code="ALL")

# Ximinus2Lambda2PiSelection is *also* a Selection, but it is "more selective"
# than  Om2PPiPi in the sense that it passes only events when something
# is in the output list
        Om2PPiPiSelection = Selection("Om2PPiPiSelection"+self.name,
                                      Algorithm=NullFilter,
                                      RequiredSelections=[Om2PPiPi])
        Om2PPiPiLine = StrippingLine(self.name + "Om2PPiPi",
                                     algos=[Om2PPiPiSelection])
        self.registerLine(Om2PPiPiLine)

# --------------------  end of makeOmegaS3  ------------
