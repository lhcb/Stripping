###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


"""
Stripping options for selecting B-> (1,2,3h) gamma(->ee conversion) with electrons in
 1. e+e- , Long-Long, 
 2. e+e- , Long-Upstream, 
 3. e+e- , Upstream-Upstream 
 4. e+e- , Downstream-Long     [ non negligible from K* gamma]
 5. e+e- , Downstream-Upstream [ non negligible from K* gamma]
 6. To keep rate as low as possible we always do cut MEDIANDEDX when a Velo segment is present. 
 4. (mirrored with Same Sign electrons)
 5. Differently from the Bu2LLK  , a DiElectronMaker supplying the NoPIDLong and NoPIDUpstream tracks are passed, and mass windows are relaxed fully in the 3h and B mass 
 6. For the LU pair, differently to what is present in Bu2LLK, we have m(ee) < 20 MeV, the one in Bu2LLK uses > 50 MeV and by using the DiElectronMaker 
    it should ensure that overlapping Brem are not added twice
 7. The Bu2LLK mirrors with 2 electrons goes to Leptonic MDST 
 8. Another full inclusive di-electron displaced case is used as well going to full DST. This should allow any analysis a posteriori to be used. 
    In this case also a MEDIANDEDX cut is applied.

Single Long track filtered by MedianDEDX
"""


#The author names should be the ones responsible for the lines, which will be contacted if some issue arises with the stripping line:
__author__  = 'Christoph Lagenbruch, Renato Quagliani'
__date__    = '19/06/2023'
__version__ = '$Revision: ? 0$'


__all__ = ( 'Hb2XGammaConvertedLines', 'default_config' )

# Locations of neutral particles for related info (these only work with ConeIsolation?)
daughter_neutral_locations = {
    # Locations of pi0 in K*+ -> K+ pi0, for opposite- and same-sign Electrons.
    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l-)]CC" : "{0}pi0H2",
    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l+)]CC" : "{0}pi0H2",
    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l-  l-)]CC" : "{0}pi0H2",
}

daughter_locations = {
    # OPPOSITE SIGN
    # 3-body
    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC" : "{0}H",
    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC" : "{0}H3",
    # 5-body
    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC" : "{0}H3",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}HH",
    # 4-body with a strange particle in the final state
    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body with two pions in the final state
    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body with p and pi in the final state. Here the names are kept generic (H1,H2,L1,L2), for uniformity with strippingof other years. This is needed for Lb->Lll analyses.
    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body decays involving K*(892)+, with pi0 in final state. (Note: had some trouble with generic X+ in the 3-body descriptors matching the K*(892)+, and getting junk output).
    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l-)]CC" : "{0}pi0H1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l-)]CC" : "{0}pi0L1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l-)]CC" : "{0}pi0L2",
    # 6-body decays with at least one kaon on the final state (quasi 5-body)
    "[Beauty -> ^K+ X- X+ X- (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty -> K+ ^X- X+ X- (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty -> K+ X- ^X+ X- (X0 ->  l+  l-)]CC" : "{0}H3",
    "[Beauty -> K+ X- X+ ^X- (X0 ->  l+  l-)]CC" : "{0}H4",
    "[Beauty -> K+ X- X+ X- (X0 ->  ^l+  l-)]CC" : "{0}L1",
    "[Beauty -> K+ X- X+ X- (X0 ->  l+  ^l-)]CC" : "{0}L2",
    # 7-body (quasi 4-body)
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC" : "{0}L21",
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC" : "{0}L22",
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC" : "{0}L23",

    # SAME SIGN
    # 3-body
    "[Beauty -> ^X+  (X ->  l+  l+)]CC" : "{0}H",
    "[Beauty ->  X+  (X -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  X+  (X ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty ->  X+ ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty -> ^X+  (X ->  l-  l-)]CC" : "{0}H",
    "[Beauty ->  X+  (X -> ^l-  l-)]CC" : "{0}L1",
    "[Beauty ->  X+  (X ->  l- ^l-)]CC" : "{0}L2",
    "[Beauty ->  X+ ^(X ->  l-  l-)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l+  l+)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l+  l+)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l+  l+)]CC" : "{0}H3",
    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l-  l-)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l-  l-)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l-  l-)]CC" : "{0}H3",
    # 5-body
    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l+  l+)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l+  l+)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l+  l+)]CC" : "{0}H3",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l-  l-)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l-  l-)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l-  l-)]CC" : "{0}H3",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l-  l-)]CC" : "{0}L1",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l- ^l-)]CC" : "{0}L2",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l-  l-)]CC" : "{0}HH",
    # 4-body with a strange particle in the final state
    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l+  l+)]CC" : "{0}H1",
    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l-  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l+  l+)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l-  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l-  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l- ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l-  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l-  l-)]CC" : "{0}LL",
    # 4-body with two pions in the final state
    "[Beauty ->  (X0 -> ^pi+  pi-)  (X ->  l+  l+)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X ->  l+  l+)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  pi+  pi-)  (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  pi+  pi-) ^(X ->  l+  l+)]CC" : "{0}LL",
    # 4-body with p and pi in the final state. Here the names are kept generic (H1,H2,L1,L2), for uniformity with strippingof other years. This is needed for Lb->Lll analyses.
    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l+  l+)]CC" : "{0}H1",
    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l-  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l+  l+)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l-  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l-  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l- ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l-  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l-  l-)]CC" : "{0}LL",
    # 4-body decays involving K*(892)+, with pi0 in final state. (Note: had some trouble with generic X+ in the 3-body descriptors matching the K*(892)+, and getting junk output).
    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l+)]CC" : "{0}pi0H1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l+)]CC" : "{0}pi0L1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l+)]CC" : "{0}pi0L2",
    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l-  l-)]CC" : "{0}pi0H1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l-  l-)]CC" : "{0}pi0L1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l-  ^l-)]CC" : "{0}pi0L2",
    # 7-body (quasi 4-body)
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ -> ^X+  X-  X+))]CC" : "{0}L21",
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+ ^X-  X+))]CC" : "{0}L22",
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+  X- ^X+))]CC" : "{0}L23",
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- -> ^X-  X-  X+))]CC" : "{0}L21",
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X- ^X-  X+))]CC" : "{0}L22",
    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X-  X- ^X+))]CC" : "{0}L23"
}


daughter_vtx_locations = {
    # OPPOSITE SIGN
    # 3-body
    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> ^(X+ -> X+  X+  X-) (X0 ->  l+  l-)]CC" : "{0}H",
    # 5-body
    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}H",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}HH",
    # 4-body
    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body with neutral in final state
    "[Beauty -> ^(X+ -> X+  X0) (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty -> (X+ -> X+  X0) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 6-body decays with at least one kaon on the final state (quasi 5-body)
    "[Beauty -> K+ X- X+ X- ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 7-body (quasi 4-body)
    "[Beauty ->  X  (X0 ->  l+  ^(l- -> X-  X-  X+))]CC" : "{0}L",

    # SAME SIGN
    # 3-body
    "[Beauty ->  X+ ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty ->  X+ ^(X ->  l-  l-)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> ^(X+ -> X+  X+  X-) (X ->  l+  l+)]CC" : "{0}H",
    "[Beauty -> ^(X+ -> X+  X+  X-) (X ->  l-  l-)]CC" : "{0}H",
    # 5-body
    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X ->  l+  l+)]CC" : "{0}H",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X ->  l-  l-)]CC" : "{0}H",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l-  l-)]CC" : "{0}HH",
    # 4-body
    "[Beauty -> ^(X0 ->  X+  X-)  (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  X+  X-) ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty -> ^(X0 ->  X+  X-)  (X ->  l-  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  X+  X-) ^(X ->  l-  l-)]CC" : "{0}LL",
    # 4-body with neutral in final state
    "[Beauty -> ^(X+ -> X+  X0) (X ->  l+  l+)]CC" : "{0}HH",
    "[Beauty -> (X+ -> X+  X0) ^(X ->  l+  l+)]CC" : "{0}LL",
    "[Beauty -> ^(X+ -> X+  X0) (X ->  l-  l-)]CC" : "{0}HH",
    "[Beauty -> (X+ -> X+  X0) ^(X ->  l-  l-)]CC" : "{0}LL",
    # 7-body (quasi 4-body)
    "[Beauty ->  X  (X ->  l+  ^(l+ -> X+  X-  X+))]CC" : "{0}L",
    "[Beauty ->  X  (X ->  l-  ^(l- -> X-  X-  X+))]CC" : "{0}L",
}

default_config = {
    'NAME'                       : 'Hb2XGammaConverted',
    'BUILDERTYPE'                : 'Hb2XGammaConvertedLines',
    'CONFIG'                     :
        {
          'BFlightCHI2'          : 100         #B FDCHI2 cut
        , 'BDIRA'                : 0.9995      #DIRA B 
        , 'BIPCHI2'              : 25          #IPCHI2 B 
        , 'BVertexCHI2'          : 9           #VTX CHI2 B 
        , 'DiElectronPT'         : 500         #ee - PT
        , 'DiElectronFDCHI2'     : 16          #ee - FDCHI2
        , 'DiElectronIPCHI2'     : 0           #ee - IPCHI2
        , 'DiElectronMassMin'    : 0
        , 'DiElectronMassMax'      :150        #used in combiner
        , 'ElectronIPCHI2'         : 16        #electron IPCHI2 > 20  [ on those di-electron pair is the same as the DiElectron]        
        , 'ElectronIPCHI2_LongLong': 25        #electron IPCHI2 > 20  [ on those di-electron pair is the same as the DiElectron]        
        , 'ElectronPT'           : 500         #long can be calibrated in PIDCalib only from pT > 500 (long, not sure on upstream...)
        , 'ElectronP'            : 2700        #long can be calibrated in PIDCalib only from pT > 500 (long, not sure on upstream...)
        , 'MEDIANDEDX_MIN'       : 50         #TODO : single electron DEDX cut to apply
        , 'MEDIANDEDX_MAX'       : 120        #TODO : single electron DEDX cut to apply 
        #, 'MEDIANDEDX_MIN_SingleTrackLine'  : 50         #TODO : single electron DEDX cut to apply
        #, 'MEDIANDEDX_MAX_SingleTrackLine'  : 120        #TODO : single electron DEDX cut to apply 
        , 'KaonIPCHI2'           : 9           #kaon IPCHI2 
        , 'KaonPT'               : 400         #kaon PT
        , 'KaonPTLoose'          : 250         #kaon PT loose
        , 'PionPTRho'            : 350         #pion PT when from Rho 
        , 'ProtonP'              : 2000        #proton P cut * used around also for kaons *
        , 'KstarPVertexCHI2'     : 25          #
        , 'KstarPMassWindow'     : 300         #mass window K* -> Kpi 
        , 'KstarPADOCACHI2'      : 30          #DOCA Kstar 
        , 'Pi0PT'                : 600         #pi0 PT 
        , 'ProbNNCut'            : 0.05        #ProbNNcut on particles ( when cutted with a given ID)
        , 'ProbNNCutTight'       : 0.1         #Tighter ProbNN cut 
        , 'DiHadronMass'         : 8000        #m(hh) max value , should be up to the Bc mass?  TODO:  check
        , 'DiHadronVtxCHI2'      : 25          #hh VTXCHI2
        , 'DiHadronADOCACHI2'    : 30          #ADOCACHI2 
        , 'UpperMass'            : 18          #Upper mass di-electron (any)
        , 'UpperMassLongLong'    : 12          #Upper Mass long long category
        , 'UpperMassLongDown'    : 100          #Upper Mass long long category
        , 'BMassWindow'          : 5000        #B Mass Window, With converted photon single track DEDX cutted have to check , maybe no range at all?
        , 'UpperBsMass'          : 5367        #used for KK pair full range (bs mass max)
        , 'UpperLbMass'          : 5620        #used for pK pair full range (Lb mass max)
        , 'PIDe'                 :  3          #PIDe cut applied on long tracks 
        , 'RICHPIDe_Up'          : -5          #RICH Dll applied on electrons upstream when used
        , 'Trk_Chi2'             : 3           #Chi2 track 
        , 'Trk_GhostProb'        : 0.35        #0.4   (ghost prob of track ) [applied to any hadron]
        , 'K1_MassWindow_Lo'     : 0           #used for KKK and Kpipi final state 
        , 'K1_MassWindow_Hi'     : 8000        #used for KKK and Kpipi final state (3h gammaConv) m(3h)-mass from 0 to 8 GeV 
        , 'K1_VtxChi2'           : 12          #
        , 'K1_SumPTHad'          : 1200        #800 ? TODO : maybe tweak rate 
        , 'K1_SumIPChi2Had'      : 48.0       
        #
        , 'VisibleMassMin_SingleConversions' : 3000.0
        , 'VisibleMassMax_SingleConversions' : 6000.0
        , 'HOPMin_SingleConversions' :         4500.0
        , 'HOPMax_SingleConversions' :         7000.0
        , 'BFlightCHI2_SingleConversions' :    121.0
        , 'BVertexCHI2_SingleConversions' : 5.0
        , 'ElectronIPCHI2_SingleConversions' : 9.0
        , 'PIDe_SingleConversions' :  3.0
        , 'SingleConversionsLinePrescale' : 1.0
        , 'B2XSingleConversionsLinePrescale' : 1.0
        #
        , 'Hb2XGammaConvertedLinePrescale'       : 1
        , 'Hb2XGammaConvertedSSLinePrescale'     : 1
        , 'AnyGammaConvertedLinePrescale'        : 1
        , 'RelatedInfoTools'     : [
            {'Type'              : 'RelInfoVertexIsolation',
             'Location'          : 'VertexIsoInfo',
             'IgnoreUnmatchedDescriptors': True,
             'DaughterLocations' : {key: val.format('VertexIsoInfo') for key, val in daughter_vtx_locations.items()}},
            {'Type'              : 'RelInfoVertexIsolationBDT',
             'Location'          : 'VertexIsoBDTInfo',
             'IgnoreUnmatchedDescriptors': True,
             'DaughterLocations' : {key: val.format('VertexIsoBDTInfo') for key, val in daughter_vtx_locations.items()}},
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 0.5,
             'IgnoreUnmatchedDescriptors': True,
             'Location' : 'TrackIsoInfo05',
             'DaughterLocations' : {key: val.format('TrackIsoInfo') for key, val in daughter_locations.items() + daughter_neutral_locations.items()}},
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'          : 0.5,
             'IgnoreUnmatchedDescriptors': True,
             'Location' : 'ConeIsoInfo05',
             'DaughterLocations' : {key: val.format('ConeIsoInfo') for key, val in daughter_locations.items() + daughter_neutral_locations.items()}},
            {'Type'              : 'RelInfoTrackIsolationBDT',
             'IgnoreUnmatchedDescriptors': True,
             # Use the BDT with 9 input variables
             # This requires that the "Variables" value is set to 2
             'Variables' : 2,
             'WeightsFile'  :  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
             'Location' : 'TrackIsoBDTInfo',
             'DaughterLocations' : {key: val.format('TrackIsoBDTInfo') for key, val in daughter_locations.items()}},

            {'Type'              : 'RelInfoBs2MuMuTrackIsolations',
             'IgnoreUnmatchedDescriptors': True,
             'Location' : 'TrackIsoBs2MMInfo',
             'DaughterLocations' : {key: val.format('TrackIsoBs2MMInfo') for key, val in daughter_locations.items()}},

            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 1.0,
             'Location'          : 'TrackIsoInfo10'
            },
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 1.5,
             'Location'          : 'TrackIsoInfo15'
            },
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 2.0,
             'Location'          : 'TrackIsoInfo20'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'         : 1.0,
             'Location'          : 'ConeIsoInfo10'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'         : 1.5,
             'Location'          : 'ConeIsoInfo15'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'         : 2.0,
             'Location'          : 'ConeIsoInfo20'
            },
        ],
        },
    'WGs'     : [ 'RD' ],
    'STREAMS': {
        'BhadronCompleteEvent': 
            ['StrippingHb2XGammaConverted_ee_LL_anyconvertedLine',
             'StrippingHb2XGammaConverted_ee_LD_anyconvertedLine',
             'StrippingHb2XGammaConverted_ee_UU_anyconvertedLine',
             'StrippingHb2XGammaConverted_ee_LU_anyconvertedLine',
             'StrippingHb2XGammaConverted_ee_UD_anyconvertedLine',
             'StrippingHb2XGammaConverted_eeLine', 
             'StrippingHb2XGammaConverted_eeSSLine'],
        'Leptonic': 
            ['StrippingHb2XGammaConverted_b2xsingleconversionsLine']
    }
}

# This tool applies the BDT track isolation from the 2017 B->mumu analysis to the Jpsi daughters
# It is separated from the other tools, since it cannot be applied to lines containing taus
RelInfoTrackIsolationBDT2 = [
    {'Type' : 'RelInfoTrackIsolationBDT2',
     'Location' : 'TrackIsolationBDT2',
     'Particles' : [1,2]
    }
]




##=========

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import  CombineParticles, DaVinci__N3BodyDecays, DaVinci__N5BodyDecays
from GaudiConfUtils.ConfigurableGenerators import  FilterDesktop as FilterDesktop_CG

from Configurables import FilterDesktop
from CommonParticles.Utils import updateDoD        


from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from CommonParticles.Utils import trackSelector
from GaudiKernel.SystemOfUnits import MeV, mm , GeV

from Configurables import DiElectronMaker, ParticleTransporter
from Configurables import LoKi__VertexFitter 
class Hb2XGammaConvertedLines(LineBuilder) :
    """
    Builder for Converted Photon with Long-Long, Long-Upstream, Upstream-Upstream pairs
    Semi-inclusive line for 
        + B -> h gamma(->ee) [h = pi , K]
        + B -> hh gamma(->ee) [hh = piK, pipi, kk, pp, pip ]
        + B -> hhh gamma(->ee) 
    Full inclusive to DST for 
        + gamma(->ee) with the MEDIANDEDX cut applied [i.e Velo segment shared on the 2 tracks]
    """    
    # now just define keys. Default values are fixed later
    __configuration_keys__ = (
          'BFlightCHI2'        #FD CHI2 
        , 'BDIRA'            #B Dira 
        , 'BIPCHI2'          #B IPCHI2
        , 'BVertexCHI2'      #B VTX CHI2 
        , 'DiElectronPT'       #DiElectronPT
        , 'DiElectronFDCHI2'   #DiElectronFDCHI2
        , 'DiElectronIPCHI2'     
        , 'ElectronIPCHI2'
        , 'ElectronIPCHI2_LongLong'
        , 'DiElectronMassMin'
        , 'DiElectronMassMax'
        , 'ElectronPT'
        , 'ElectronP'
        , 'MEDIANDEDX_MIN'
        , 'MEDIANDEDX_MAX'
        #, 'MEDIANDEDX_MIN_SingleTrackLine' #unused
        #, 'MEDIANDEDX_MAX_SingleTrackLine' #unused
        , 'KaonIPCHI2'
        , 'KaonPT'
        , 'KaonPTLoose'
        , 'PionPTRho'
        , 'ProtonP'
        , 'KstarPVertexCHI2'
        , 'KstarPMassWindow'
        , 'KstarPADOCACHI2'
        , 'Pi0PT'
        , 'ProbNNCut'
        , 'ProbNNCutTight'
        , 'DiHadronMass'
        , 'DiHadronVtxCHI2'
        , 'DiHadronADOCACHI2'
        , 'UpperMass'
        , 'UpperMassLongLong'
        , 'UpperMassLongDown'
        , 'BMassWindow'
        , 'UpperBsMass'
        , 'UpperLbMass'
        , 'PIDe'        # for long electrons 
        , 'RICHPIDe_Up' # for upstream electrons 
        , 'Trk_Chi2'
        , 'Trk_GhostProb'
        , 'K1_MassWindow_Lo'
        , 'K1_MassWindow_Hi'
        , 'K1_VtxChi2'
        , 'K1_SumPTHad'
        , 'K1_SumIPChi2Had'
        #
        , 'VisibleMassMin_SingleConversions'
        , 'VisibleMassMax_SingleConversions'
        , 'HOPMin_SingleConversions'
        , 'HOPMax_SingleConversions'
        , 'BFlightCHI2_SingleConversions'
        , 'BVertexCHI2_SingleConversions'
        , 'ElectronIPCHI2_SingleConversions'
        , 'PIDe_SingleConversions'
        , 'SingleConversionsLinePrescale'
        , 'B2XSingleConversionsLinePrescale'
        #
        , "Hb2XGammaConvertedLinePrescale"
        , "Hb2XGammaConvertedSSLinePrescale"
        , "AnyGammaConvertedLinePrescale"
        , 'RelatedInfoTools'
      )      
    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self._name = name
        # StrippingHb2XGammaConverted_anyconvertedLine
        eeXLine_name      = name + "_ee"
        eeXLine_name_SS   = name + "_eeSS"    
        eeAnyEELine_name  = name + "_ee_[CATEGORY]_anyconverted"
        SingleConversionsLine_name  = name + "_singleconversions"
        B2XSingleConversionsLine_name  = name + "_b2xsingleconversions"
        #basic pions , kaons, protons , kshortLL, kshortDD
        from StandardParticles import StdLoosePions         as Pions
        from StandardParticles import StdLooseKaons         as Kaons
        from StandardParticles import StdLooseProtons       as Protons
        from StandardParticles import StdAllLoosePions      as AllPions
        from StandardParticles import StdAllLooseKaons      as AllKaons
        from StandardParticles import StdAllLooseProtons    as AllProtons
        from StandardParticles import StdLooseKstar2Kpi     as Kstars
        from StandardParticles import StdLoosePhi2KK        as Phis
        from StandardParticles import StdLooseLambdastar2pK as Lambdastars    
        # V0s
        from StandardParticles import StdVeryLooseKsLL as KshortsLL
        from StandardParticles import StdLooseKsDD as KshortsDD
        from StandardParticles import StdVeryLooseLambdaLL as LambdasLL
        from StandardParticles import StdLooseLambdaDD as LambdasDD
        from StandardParticles import StdKs2PiPiLL as BrunelKshortsLL
        from StandardParticles import StdKs2PiPiDD as BrunelKshortsDD
        from StandardParticles import StdLambda2PPiLL as BrunelLambdasLL
        from StandardParticles import StdLambda2PPiDD as BrunelLambdasDD

        #neutrals pi0 
        from StandardParticles import StdLooseResolvedPi0 as Pi0sRes
        from StandardParticles import StdLooseMergedPi0 as Pi0sMrg
        
        # 1 : Make K, Ks, K*, K1, Phi and Lambdas

        # 1 hadron 
        SelKaons     = self._filterHadron(            name = "KaonsFor"            + self._name, sel    = Kaons, params = config )
        SelPions     = self._filterHadron(            name = "PionsFor"            + self._name, sel    = Pions, params = config )    
        SelKshortsLL = self._filterHadron(            name = "KshortsLLFor"        + self._name, sel    = [KshortsLL,BrunelKshortsLL], params = config )
        SelKshortsDD = self._filterHadron(            name = "KshortsDDFor"        + self._name, sel    = [KshortsDD,BrunelKshortsDD], params = config )

        # 2 hadrons (HH) resonant 
        SelKstars           = self._filterHadron(  name = "KstarsFor"           + self._name, sel    =  Kstars,params = config )
        SelKstarsPlusLL     = self._makeKstarPlus( name = "KstarsPlusLLFor"     + self._name, kshorts = [KshortsLL,BrunelKshortsLL], pions   = Pions, params  = config )
        SelKstarsPlusDD     = self._makeKstarPlus( name = "KstarsPlusDDFor"     + self._name, kshorts = [KshortsDD,BrunelKshortsDD], pions   = Pions, params  = config )
        SelKstarsPlusPi0Res = self._makeKstarPlusPi0( name = "KstarsPlusPi0ResFor" + self._name, kaons = Kaons, pions = Pi0sRes, params = config )
        SelKstarsPlusPi0Mrg = self._makeKstarPlusPi0( name = "KstarsPlusPi0MrgFor" + self._name, kaons = Kaons, pions = Pi0sMrg, params = config )

        # 2 hadrons full mass range 
        SelKPis  = self._makeKPi(       name = "KPisFor" + self._name, kaons  =  Kaons, pions  = Pions,         params = config )
        SelPiPis = self._makePiPi(      name = "PiPisFor"+ self._name, pions  =  Pions,                         params = config )
        SelKKs   = self._makeKK(        name = "KKsFor"  + self._name, kaons  =  Kaons,                         params = config )
        SelpKs   = self._makepK(        name = "pKsFor"  + self._name, protons = AllProtons, kaons  = AllKaons, params = config )
        SelpPis  = self._makepPi(       name = "pPisFor" + self._name, protons = Protons,    pions  = Pions,    params = config )
        Selpps   = self._makepp(        name = "ppsFor"  + self._name, protons = Protons,                       params = config )


        # 3 hadrons mass cutted 
        SelK1s         = self._makeK1(        name   = "K1For"           + self._name, kaons  = Kaons, pions  = Pions,        params = config )
        SelK2s         = self._makeK2(        name   = "K2For"           + self._name, kaons  = Kaons,                        params = config )
        SelPhis        = self._filterHadron(  name   = "PhisFor"         + self._name, sel    =  Phis,                        params = config )
        SelLambdasLL   = self._filterHadron(  name   = "LambdasLLFor"    + self._name, sel    =  [LambdasLL,BrunelLambdasLL], params = config )
        SelLambdasDD   = self._filterHadron(  name   = "LambdasDDFor"    + self._name, sel    =  [LambdasDD,BrunelLambdasDD], params = config )
        SelLambdastars = self._filterHadron(  name   = "LambdastarsFor"  + self._name, sel    =  Lambdastars,                 params = config )
        #same sign hadrons dropped here     
        from StandardParticles import StdNoPIDsElectrons     as NoPIDsElectrons
        
        SINGLETRACKCUT            = "(TRVELOCLUSTERDEDXMEDIAN > %(MEDIANDEDX_MIN)s) & (MIPCHI2DV(PRIMARY) > %(ElectronIPCHI2_SingleConversions)s) & (PT > %(ElectronPT)s) & (HASRICH) & (HASCALOS) & (PIDe> %(PIDe_SingleConversions)s)"%config
        SingleElectronsFilter     = FilterDesktop_CG( Code = SINGLETRACKCUT )
        SingleElectronConversions = Selection('SingleElectronConversions', Algorithm=SingleElectronsFilter, RequiredSelections=[NoPIDsElectrons])
        
        """
        Upstream tracks usually have very low PT if reconstructed ! 
        """        
        #on 2018-patches branch
        #https://gitlab.cern.ch/lhcb/LHCb/-/blob/2018-patches/Event/RecEvent/xml/ProtoParticle.xml
        #on run2-patches branch
        #https://gitlab.cern.ch/lhcb/LHCb/-/blob/run2-patches/Event/RecEvent/include/Event/ProtoParticle.h
        INACCECAL = "( PPINFO(302,0) >0.5 )" #InAccECal flag
        CUT_LONGElectrons   = "(MIPCHI2DV(PRIMARY) > %(ElectronIPCHI2)s) & (P>%(ElectronP)s) & (PT > %(ElectronPT)s) & (ISLONG)&(HASRICH)  & (HASCALOS)&(PIDe> %(PIDe)s)      & (TRVELOCLUSTERDEDXMEDIAN > %(MEDIANDEDX_MIN)s)"%config 
        CUT_LONGElectrons  += "& {}".format(INACCECAL)
        CUT_UPElectrons     = "(MIPCHI2DV(PRIMARY) > %(ElectronIPCHI2)s) &                                 (ISUP)  &(PPHASRICH)& (PPINFO(100,-10000,-10000)> %(RICHPIDe_Up)s) & (TRVELOCLUSTERDEDXMEDIAN > %(MEDIANDEDX_MIN)s)"%config
        CUT_DOWNElectrons   = "(MIPCHI2DV(PRIMARY) > %(ElectronIPCHI2)s) & (P>%(ElectronP)s) & (PT > %(ElectronPT)s) & (ISDOWN)&(HASRICH)  & (HASCALOS)&(PIDe> %(PIDe)s)"%config
        CUT_DOWNElectrons  += "& {}".format(INACCECAL)

        LongElectronFilter  = FilterDesktop('Hb2XGammaConverted_LongElectrons',Inputs = ["Phys/StdAllNoPIDsElectrons/Particles"] ,  Code = CUT_LONGElectrons  )
        UpElectronFilter    = FilterDesktop('Hb2XGammaConverted_UpElectrons'  ,Inputs = ["Phys/StdNoPIDsUpElectrons/Particles"],    Code = CUT_UPElectrons      )
        DownElectronsFilter = FilterDesktop('Hb2XGammaConverted_DownElectrons',Inputs = ["Phys/StdNoPIDsDownElectrons/Particles"],  Code = CUT_DOWNElectrons   )
        Locations = [ "Phys/Hb2XGammaConverted_LongElectrons/Particles",
                      "Phys/Hb2XGammaConverted_UpElectrons/Particles", 
                      "Phys/Hb2XGammaConverted_DownElectrons/Particles",
        ]
        updateDoD(LongElectronFilter)
        updateDoD(UpElectronFilter)
        updateDoD(DownElectronsFilter)
        
        _Code_NOTDownDown = "(2!=NINTREE( (ABSID==11) & (ISDOWN)))"
        _Code_LongLong    = "(2==NINTREE( (ABSID==11) & (ISLONG)))"
        # _Code_DownDown    = "(2==NINTREE( (ABSID==11) & (ISDOWN)))"
        _Code_UpUp        = "(2==NINTREE( (ABSID==11) & (ISUP)))"
        _Code_LongDown    = "(1==NINTREE( (ABSID==11) & (ISLONG))) & (1==NINTREE( (ABSID==11) & (ISDOWN)))"
        _Code_LongUp      = "(1==NINTREE( (ABSID==11) & (ISLONG))) & (1==NINTREE( (ABSID==11) & (ISUP)))"
        _Code_UpDown      = "(1==NINTREE( (ABSID==11) & (ISUP)))   & (1==NINTREE( (ABSID==11) & (ISDOWN)))"

        #Make Di-Electron combining ALL pre-filtered electron tracks.
        DiElectronsFromTracks     = self._makeDiElectron( "EEFor"+self._name ,  params = config, sameSign= False,
                                                        locations =  Locations,
                                                        trtypes=    [ "Long","Upstream", "Downstream"])
        DiElectronsFromTracksSS   = self._makeDiElectron( "EESSFor"+ self._name, params = config , sameSign = True,
                                                        locations =  Locations,
                                                        trtypes= [ "Long","Upstream", "Downstream"])
        dielectrons_split = {}
        # NOTE : DD is skipped , as it won't benefit of the 2x VeloCharge
        for Category, CodeFilter in zip( [ "LL", "UU", "LD", "LU", "UD"] , [_Code_LongLong, _Code_UpUp, _Code_LongDown, _Code_LongUp, _Code_UpDown]):
            if Category == "LL" : 
                dielectrons_split[Category]       =  self._filterDiElectronConvertedLongLong( "SelDiElectronFromTracksFor" + self._name+"_{}".format(Category),
                                                            DiElectron = DiElectronsFromTracks,
                                                            params     = config,
                                                            idcut      = CodeFilter )
            elif Category == "LD":
                dielectrons_split[Category]       =  self._filterDiElectronConvertedLongDown( "SelDiElectronFromTracksFor" + self._name+"_{}".format(Category),
                                                            DiElectron = DiElectronsFromTracks,
                                                            params     = config,
                                                            idcut      = CodeFilter )
            else :
                dielectrons_split[Category]       =  self._filterDiElectronConverted( "SelDiElectronFromTracksFor" + self._name+"_{}".format(Category),
                                                            DiElectron = DiElectronsFromTracks,
                                                            params     = config,
                                                            idcut      = CodeFilter )
        dielectrons_split["all_notDD"] = self._filterDiElectronConverted( "SelDiElectronFromTracksFor" + self._name+"_excludeDD",
                                                        DiElectron = DiElectronsFromTracks,
                                                        params     = config,
                                                        idcut      = _Code_NOTDownDown )      

        dielectrons_split["all_notDD_SS"] = self._filterDiElectronConverted( "SelDiElectronFromTracksSSFor" + self._name+"_excludeDD",
                                                        DiElectron = DiElectronsFromTracksSS,
                                                        params     = config,
                                                        idcut      = _Code_NOTDownDown )
        # # 4 : Combine Particles (make a B)    
        SelB2eeX = self._makeB2GammaConvertedX(eeXLine_name,  DiElectron = dielectrons_split["all_notDD"], 
                                                                hadrons  = [ SelPions,     #pi gamma
                                                                            SelKaons,     #K  gamma
                                                                            SelKstars,    #K* gamma
                                                                            SelPhis,      #phi gamma
                                                                            SelKshortsLL, #KshortLL gamma
                                                                            SelKshortsDD, #KshortDD gamma
                                                                            SelLambdasLL, #LambdaLL gamma
                                                                            SelLambdasDD, #LambdaDD gamma
                                                                            SelKstarsPlusLL,    #Kst+(Ks LL )
                                                                            SelKstarsPlusDD,    #Kst+(Ks DD)
                                                                            SelKstarsPlusPi0Res,#Kst+(pi0 K)
                                                                            SelKstarsPlusPi0Mrg,#Kst+(pi0 K+)
                                                                            SelpKs,             #pK
                                                                            SelLambdastars,
                                                                            SelK1s,
                                                                            SelK2s,
                                                                            SelPiPis,
                                                                            SelKPis,
                                                                            SelKKs,
                                                                            SelpPis,
                                                                            Selpps
                                                                            ],
                                                                            params   = config,
                                                                            masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )
        SelB2eeX_SS= self._makeB2GammaConvertedX(eeXLine_name_SS,  DiElectron = dielectrons_split["all_notDD_SS"], 
                                                                    hadrons  = [ SelPions,  #pi gamma
                                                                                SelKaons,  #K  gamma
                                                                                SelKstars, #K* gamma
                                                                                SelPhis,   #phi gamma
                                                                                SelKshortsLL, #KshortLL gamma
                                                                                SelKshortsDD, #KshortDD gamma
                                                                                SelLambdasLL, #LambdaLL gamma
                                                                                SelLambdasDD, #LambdaDD gamma
                                                                                SelKstarsPlusLL,    #Kst+(Ks LL )
                                                                                SelKstarsPlusDD,    #Kst+(Ks DD)
                                                                                SelKstarsPlusPi0Res,#Kst+(pi0 K)
                                                                                SelKstarsPlusPi0Mrg,#Kst+(pi0 K+)
                                                                                SelpKs, #pK
                                                                                SelLambdastars,
                                                                                SelK1s,
                                                                                SelK2s,
                                                                                SelPiPis, 
                                                                                SelKPis, 
                                                                                SelKKs, 
                                                                                SelpPis, 
                                                                                Selpps
                                                                                ],
                                                                    params   = config,
                                                                    masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )
                
        #    def _makeB2SingleConversionsX( self, name, DiElectron, hadrons, params, masscut = "(ADAMASS('B+')< 5000 *MeV" ):
        SelB2XSingleConversions = self._makeB2XSingleConversions(B2XSingleConversionsLine_name, SingleElectron = SingleElectronConversions, 
                                                                 hadrons  = [ SelPions,     #pi gamma
                                                                              SelKaons,     #K  gamma
                                                                              SelKstars,    #K* gamma
                                                                              SelPhis,      #phi gamma
                                                                              SelKshortsLL, #KshortLL gamma
                                                                              SelKshortsDD, #KshortDD gamma
                                                                              SelLambdasLL, #LambdaLL gamma
                                                                              SelLambdasDD, #LambdaDD gamma
                                                                              SelKstarsPlusLL,    #Kst+(Ks LL )
                                                                              SelKstarsPlusDD,    #Kst+(Ks DD)
                                                                              SelKstarsPlusPi0Res,#Kst+(pi0 K)
                                                                              SelKstarsPlusPi0Mrg,#Kst+(pi0 K+)
                                                                              SelpKs,             #pK
                                                                              SelLambdastars,
                                                                              SelK1s,
                                                                              SelK2s,
                                                                              SelPiPis,
                                                                              SelKPis,
                                                                              SelKKs,
                                                                              SelpPis,
                                                                              Selpps
                                                                          ],
                                                                 params   = config,
                                                                 combinationcut  = "(ADOCACHI2CUT(25.,'')) & (AM > %(VisibleMassMin_SingleConversions)s) & (AM < %(VisibleMassMax_SingleConversions)s)" % config)    
        #=============================================================

        # 5 : Declare Lines
        SPDFilter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 450 )" ,
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb" ]
        }       
        self.Hb2XGammaConvertedLine = StrippingLine(eeXLine_name + "Line",
                                                 prescale          = config['Hb2XGammaConvertedLinePrescale'],
                                                 postscale         = 1,
                                                 selection         = SelB2eeX,
                                                 RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                                 FILTER            = SPDFilter,
                                                 RequiredRawEvents = [],
                                                 MDSTFlag          = False)
        self.Hb2XGammaConvertedLine_SS = StrippingLine(eeXLine_name_SS + "Line",
                                                 prescale          = config['Hb2XGammaConvertedSSLinePrescale'],
                                                 postscale         = 1,
                                                 selection         = SelB2eeX_SS,
                                                 RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                                 FILTER            = SPDFilter,
                                                 RequiredRawEvents = [], #we want to do something special?
                                                 MDSTFlag          = False)
        self.AnyGammaConvertedLine = {} 
        for Category in [ "LL", "UU", "LD", "LU", "UD"] : 
            self.AnyGammaConvertedLine[Category] = StrippingLine(eeAnyEELine_name.replace("[CATEGORY]",Category) + "Line",
                                                                prescale          = config['AnyGammaConvertedLinePrescale'],
                                                                postscale         = 1,
                                                                selection         = dielectrons_split[Category],
                                                                #RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                                                FILTER            = SPDFilter,
                                                                RequiredRawEvents = [],
                                                                MDSTFlag          = False)
                    
        # self.SingleConversionsLine = StrippingLine(SingleConversionsLine_name + "Line",
        #                                          prescale          = config['SingleConversionsLinePrescale'],
        #                                          postscale         = 1,
        #                                          selection         = SingleElectronConversions,
        #                                          #RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
        #                                          FILTER            = SPDFilter,
        #                                          RequiredRawEvents = [],
        #                                          MDSTFlag          = False)
        self.B2XSingleConversionsLine = StrippingLine(B2XSingleConversionsLine_name + "Line",
                                                 prescale          = config['B2XSingleConversionsLinePrescale'],
                                                 postscale         = 1,
                                                 selection         = SelB2XSingleConversions,
                                                 #RelatedInfoTools  = config['RelatedInfoTools'] + RelInfoTrackIsolationBDT2,
                                                 FILTER            = SPDFilter,
                                                 RequiredRawEvents = [],
                                                 MDSTFlag          = False,
                                                 EnableFlavourTagging = True)

        # 6 : Register Lines
        self.registerLine( self.Hb2XGammaConvertedLine )
        self.registerLine( self.Hb2XGammaConvertedLine_SS )        
        for Category in [ "LL","UU", "LD", "LU", "UD"]:
            self.registerLine( self.AnyGammaConvertedLine[Category])
        # self.registerLine( self.AnyGammaConvertedLine)
        # self.registerLine( self.SingleConversionsLine)
        self.registerLine( self.B2XSingleConversionsLine)

    #### Di-Electron for LL, LU, UU pairs with gamma -> e+ e-
    def _makeDiElectron(self ,name , params,  locations= [ "Phys/StdAllNoPIDsElectrons/Particles","Phys/StdNoPIDsUpElectrons/Particles", "Phys/StdNoPIDsDownElectrons/Particles" ],
                                              trtypes =  [ "Long","Upstream", "Downstream"], 
                                              sameSign = False ): 
        if sameSign : 
            DiElectronsFromTracks = DiElectronMaker(name+ 'GammaDiElectronFromAllLUDTracksSS')
            DiElectronsFromTracks.DecayDescriptor   = "[gamma -> e+ e+]cc" #done in the StandardConteiner maker of electrons for Gamma particles... maybe not needed! 
            DiElectronsFromTracks.OppositeSign      = 0
        else :
            DiElectronsFromTracks = DiElectronMaker(name+ 'GammaDiElectronFromAllLUDTracksOS')
            DiElectronsFromTracks.DecayDescriptor   = "gamma -> e+ e-" #done in the StandardConteiner maker of electrons for Gamma particles... maybe not needed!                 
            DiElectronsFromTracks.OppositeSign      = 1
        DiElectronsFromTracks.Particle          = "gamma" #it's a string flag enabling some special features... unsure what the X -> ee will be in the string output
        DiElectronsFromTracks.ElectronInputs    = locations #[ "Phys/StdAllNoPIDsElectrons/Particles","Phys/StdNoPIDsUpElectrons/Particles", "Phys/StdNoPIDsDownElectrons/Particles" ]
        selector = trackSelector ( DiElectronsFromTracks , trackTypes = trtypes ) #[ "Long","Upstream", "Downstream"])
        DiElectronsFromTracks.DiElectronPtMin   = params['DiElectronPT']*MeV  #pt Min > 200 MeV 
        DiElectronsFromTracks.ElectronPtMin     = 0.0*MeV #already CUTTED before entering here, except Upstream 
        DiElectronsFromTracks.DiElectronMassMax = params['DiElectronMassMax']*MeV #tighter? 
        DiElectronsFromTracks.DiElectronMassMin = params['DiElectronMassMin']*MeV
        #no cuts at all are done with this options ( negative values )
        DiElectronsFromTracks.ElectronMaxPIDcut =-99999. #for LU , UU , LL it should do nothing ( hopefully )
        DiElectronsFromTracks.ElectronPIDcut    =-99999. #for LU , UU , LL it should do nothing ( hopefully )
        # if( pid < m_eidmin)continue, is -99999. keeping all 
        DiElectronsFromTracks.DiElectronPIDcut  =-99999. #for LU , UU , LL it should do nothing ( hopefully )
        DiElectronsFromTracks.ElectronCLcut     =-1.     #for LU , UU , LL it should do nothing ( hopefully )
        DiElectronsFromTracks.DiElectronCLcut   =-1.     #for LU , UU , LL it should do nothing ( hopefully )        
        DiElectronsFromTracks.eOpMax            =-1.     #Cut on E/p [ requires CALO ], cutted only if this flag is > 0 
        DiElectronsFromTracks.eOpMin            =-1.     #Cut on E/p [ requires CALO ], cutted only if this flag is > 0 
        DiElectronsFromTracks.ADAMASSFactor     = 1.5    #should be ok to enlarge a little bit the window...               
        #### seems to be harmless to have it, should be aliging things to what the DiElectronMaker DD does.
        DiElectronsFromTracks.DeltaY            =3
        DiElectronsFromTracks.DeltaYmax         =200*mm
        ### we don't use Down-Down, does it help in Down-Long? NOT sure.
        ### Claimed to have better performance for Down-Long, but here we have Long-Down only DD is not ok
        DiElectronsFromTracks.addTool( ParticleTransporter, name='MyTransporterDiElectron' )
        DiElectronsFromTracks.MyTransporterDiElectron.TrackExtrapolator = "TrackRungeKuttaExtrapolator"
        DiElectronsFromTracks.ParticleCombiners.update( { "" : "LoKi::VertexFitter"} )
        DiElectronsFromTracks.addTool( LoKi__VertexFitter )
        DiElectronsFromTracks.LoKi__VertexFitter.addTool( ParticleTransporter, name='Transporter' )
        DiElectronsFromTracks.LoKi__VertexFitter.Transporter.TrackExtrapolator = "TrackRungeKuttaExtrapolator"
        DiElectronsFromTracks.LoKi__VertexFitter.DeltaDistance = 10 * mm #bigger than default

        # dieLL.DeltaY is used for the LL gamma AllLooseGamma
        # for the DD gamma conversion the runkekutta extrapolator is also configured, no need here? but what about UP UP ? 
        #from StdAllLooseGamma this is done
        # dieDD.DeltaY = 3.
        # dieDD.DeltaYmax = 200 * mm
        # dieDD.DiElectronMassMax = 100.*MeV
        # dieDD.DiElectronPtMin = 200.*MeV
        # dieLL.DeltaY = 3.
        # dieLL.DeltaYmax = 200 * mm
        # dieLL.DiElectronMassMax = 100.*MeV
        # dieLL.DiElectronPtMin = 200.*MeV   
        """
        See https://gitlab.cern.ch/lhcb/Phys/-/blob/2018-patches/Phys/ParticleMaker/src/DiElectronMaker.cpp?ref_type=heads#L253 
        // check that the 2 electrons have the same Y within 3 sigma and maximum within 20cm (gamma conversion only)
        if(m_pid == "gamma"){
            if( m_deltaY> 0 && fabs(ye1-ye2) > m_deltaY*std::sqrt(sprye1+sprye2))continue;
            if( m_deltaYmax > 0 &&  fabs(ye1-ye2)>m_deltaYmax)continue;
        }
        """
        # DiElectronsFromTracks.UseEcalEnergy          , m_ecalE=40*GeV  #unused in the code it seems 
        DiElectronsFromTracks.VeloCnvThreshold      =  m_vc=1.5 #TODO: unsure if good or not, to check on MC
        DiElectronsFromTracks.AddBrem               =  True
        DiElectronsFromTracks.DistMaxpair           =  10* mm  #TODO: unsure if good or not, to check on MC
        DiElectronsFromTracks.UseCombinePair        =  True
        return Selection(name+'eeSelection', Algorithm=  DiElectronsFromTracks)
    #####################################################
    def _filterHadron( self, name, sel, params ):
        """
        Filter for some of hadronic final states
        Check for list as input for V0s which use (Very)Loose and Brunel candidates
        """
        if isinstance(sel,list): sel_list = [MergedSelection("Merged"+name,RequiredSelections= sel,Unique=False)]
        else:                    sel_list = [sel]

        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        # need to add the ID here
        _Code = "(PT > %(KaonPT)s *MeV) & " \
                "(M < %(DiHadronMass)s*MeV) & " \
                "((ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) | " \
                "(NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))))" % params

        _Filter = FilterDesktop_CG( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = sel_list )
    #####################################################
    def _filterDiElectronConverted( self, name, DiElectron, params, idcut = None ) :
        """
        Handy interface for DiElectron filter Converted one, if there is an upstream electron we take it with basically no PT cut
        """
        "(MINTREE(ABSID<14,PT)) > %(ElectronPT)s *MeV & "
        _Code = "( (ID=='gamma') & "\
                "(PT > %(DiElectronPT)s *MeV) & "\
                "(  (MM <  %(UpperMass)s *MeV) |"\
                  "((MM < %(UpperMassLongDown)s *MeV) & (1==NINTREE( (ABSID==11) & (ISLONG))) & (1==NINTREE( (ABSID==11) & (ISDOWN))))"\
                ")& "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(ElectronIPCHI2)s)   & "\
                "(VFASPF(VCHI2/VDOF) < 4) & (BPVVDCHI2 > %(DiElectronFDCHI2)s) & "\
                "(MIPCHI2DV(PRIMARY) > %(DiElectronIPCHI2)s))" % params
        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )
        _Filter = FilterDesktop_CG( Code = _Code )
        return Selection(name, Algorithm = _Filter, RequiredSelections = [ DiElectron ] )
    def _filterDiElectronConvertedLongLong( self, name, DiElectron, params, idcut = None ) :
        """
        Handy interface for DiElectron filter Converted one, if there is an upstream electron we take it with basically no PT cut
        Long-Long implementation for Inclusive case. 
        """
        "(MINTREE(ABSID<14,PT)) > %(ElectronPT)s *MeV & "
        _Code = "( (ID=='gamma') & "\
                "(PT > %(DiElectronPT)s *MeV) & "\
                "(MM < %(UpperMassLongLong)s *MeV) & "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(ElectronIPCHI2_LongLong)s)   & "\
                "(VFASPF(VCHI2/VDOF) < 4) & (BPVVDCHI2 > %(DiElectronFDCHI2)s) & "\
                "(MIPCHI2DV(PRIMARY) > %(DiElectronIPCHI2)s))" % params
        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )
        _Filter = FilterDesktop_CG( Code = _Code )
        return Selection(name, Algorithm = _Filter, RequiredSelections = [ DiElectron ] )
    def _filterDiElectronConvertedLongDown( self, name, DiElectron, params, idcut = None ) :
        """
        Handy interface for DiElectron filter Converted one, if there is an upstream electron we take it with basically no PT cut 
        Long-Long implementation for Inclusive case. 
        """
        "(MINTREE(ABSID<14,PT)) > %(ElectronPT)s *MeV & "
        _Code = "( (ID=='gamma') & "\
                "(PT > %(DiElectronPT)s *MeV) & "\
                "(MM < %(UpperMassLongDown)s *MeV) & "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(ElectronIPCHI2_LongLong)s)   & "\
                "(VFASPF(VCHI2/VDOF) < 4) & (BPVVDCHI2 > %(DiElectronFDCHI2)s) & "\
                "(MIPCHI2DV(PRIMARY) > %(DiElectronIPCHI2)s))" % params
        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )
        _Filter = FilterDesktop_CG( Code = _Code )
        return Selection(name, Algorithm = _Filter, RequiredSelections = [ DiElectron ] )        

    #####################################################
    def _makeKstarPlus( self, name, kshorts, pions, params):
        """
        Make a K*(892)+ -> KS0 pi+
        Check for list as input for KS0s which use (Very)Loose and Brunel candidates
        Original Bu2LLK line from which it is taken from has Unique=True , but it is not reproducible
        """
        if isinstance(kshorts,list): sel_list = [MergedSelection("Merged"+name,RequiredSelections= kshorts,Unique=False),pions]
        else:                        sel_list = [kshorts,pions]
        _Decays = "[K*(892)+ -> KS0 pi+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(ADAMASS('K*(892)+') < %(KstarPMassWindow)s *MeV) & " \
                          "(ADOCACHI2CUT( %(KstarPADOCACHI2)s  , ''))" % params
        _MotherCut = "(VFASPF(VCHI2) < %(KstarPVertexCHI2)s)" % params
        _KshortCut = "(PT > %(KaonPT)s *MeV) & " \
                     "(M < %(DiHadronMass)s*MeV) & " \
                     "(NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)))" % params
        _PionCut = "(PT > %(KaonPT)s *MeV) & " \
                   "(M < %(DiHadronMass)s*MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params
        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)
        _Combine.DaughtersCuts = {
            "KS0"  : _KshortCut,
            "pi+"  : _PionCut
        }
        return Selection(name, Algorithm = _Combine, RequiredSelections = sel_list )
#####################################################
    def _makeKstarPlusPi0( self, name, kaons, pions, params):
        """
        Make a K*+ -> K+ pi0. Note that either merged or resolved pi0s may be used as daughter.
        """
        _Decays = "[K*(892)+ -> K+ pi0]cc"
        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(ADAMASS('K*(892)+') < %(KstarPMassWindow)s *MeV)" % params
        _MotherCut = "(ADMASS('K*(892)+') < %(KstarPMassWindow)s *MeV) " % params
        _KaonCut = "(PT > %(KaonPT)s *MeV) & " \
            "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))" % params
        _PionCut = "(PT > %(Pi0PT)s * MeV)" % params
        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)
        _Combine.DaughtersCuts = {
            "K+"  : _KaonCut,
            "pi0"  : _PionCut
        }
        return Selection(name, Algorithm = _Combine, RequiredSelections = [ kaons, pions ] )
#####################################################
    def _makePiPi( self, name, pions, params):
        """
        Make a rho -> pi+ pi- in a range below 2600 MeV.
        """

        _Decays = "rho(770)0 -> pi+ pi-"
        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(DiHadronMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut = "(PT > %(PionPTRho)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(ProbNNCutTight)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)
        _Combine.DaughtersCuts = {
            "pi+"  : _DaughterCut,
            "pi-"  : _DaughterCut
        }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ pions ] )

    #####################################################
    # - Full Range Kpi 
    # - Full Range KK 
    # - Full Range 
    #####################################################

    #####################################################
    # Make a K* -> K+ pi- in entire range below 2600 MeV ( DiHadronMass range )
    # Descriptor used [K*_0(1430)0 -> K+ pi-]cc"
    #####################################################
    def _makeKPi( self, name, kaons, pions, params):
        _Decays = "[K*_0(1430)0 -> K+ pi-]cc"        
        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(DiHadronMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params
        #TODO: ProtonP ? Copied from Bu2LLK 
        _DaughterCut_k = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s))  & (PROBNNk > %(ProbNNCutTight)s)" % params
        _DaughterCut_pi = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s))  & (PROBNNpi > %(ProbNNCutTight)s)" % params
        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "K+"  : _DaughterCut_k,
            "pi-" : _DaughterCut_pi
        }
        return Selection(name, Algorithm = _Combine, RequiredSelections = [ kaons, pions ] )

    #####################################################
    # Make a "f'_2(1525) -> K+ K-" in entire range below UpperBsMass parameter (ProbNNCutTight cut applied here on kaons)
    # Descriptor used f'_2(1525) -> K+ K-
    #####################################################
    def _makeKK( self, name, kaons, params):
        """
        Make an f2(1525) -> K+ K- in entire range.
        """
        _Decays = "f'_2(1525) -> K+ K-"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(UpperBsMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params
        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params
        _DaughterCut = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s))  & (PROBNNk > %(ProbNNCutTight)s)" % params
        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "K+"  : _DaughterCut,
            "K-" : _DaughterCut
        }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ kaons ] )    
    #####################################################
    # Make a "[Lambda(1520)0 -> p+ K-]cc" in entire range below UpperLbMass parameter (PROBNNp, PROBNNk cut applied here on proton/kaon
    # Descriptor used "[Lambda(1520)0 -> p+ K-]cc"
    #####################################################
    def _makepK( self, name, protons, kaons, params):
        _Decays = "[Lambda(1520)0 -> p+ K-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(UpperLbMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params


        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_p = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNp > %(ProbNNCut)s)" % params
        _DaughterCut_K = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNk > %(ProbNNCut)s)" % params


        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut_p,
            "K-" : _DaughterCut_K
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons, kaons ] )

    #####################################################
    # Make a "[N(1440)0 -> p+ pi-]cc" in entire range below DiHadronMass parameter (PROBNNp, PROBNNpi cut applied here on proton/pion pair
    # Descriptor used "[N(1440)0 -> p+ pi-]cc"
    #####################################################
    def _makepPi( self, name, protons, pions, params):
        """
        Make a N* -> p pi- in entire range until 2600 MeV.
        """
        _Decays = "[N(1440)0 -> p+ pi-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(DiHadronMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_p = "(PT > %(PionPTRho)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                         "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNp > %(ProbNNCutTight)s)" % params
        _DaughterCut_pi = "(PT > %(KaonPTLoose)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(ProbNNCutTight)s))" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)
        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut_p,
            "pi-" : _DaughterCut_pi
        }
        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons, pions ] )    
    #####################################################    
    # Make a "f_2(1950) -> p+ p~- in entire range below UpperLbMass parameter (PROBNNp, PROBNNpi cut applied here on proton/pion pair)
    # Descriptor used "f_2(1950) -> p+ p~-"
    #####################################################    
    def _makepp( self, name, protons, params):
        """
        Make a f2 -> p pbar in entire range.
        """
        _Decays = "f_2(1950) -> p+ p~-"
        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(UpperLbMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params
        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params
        _DaughterCut = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV)  &" \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNp > %(ProbNNCutTight)s)" % params
        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)
        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut,
            }
        
        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons ] )
    #####################################################
    # Make a "[K_1(1270)+ -> K+ pi+ pi-]cc in range [K1_MassWindow_Lo, K1_MassWindow_Hi]
    #####################################################
    def _makeK1( self, name, kaons, pions, params ) :
        _Decays = "[K_1(1270)+ -> K+ pi+ pi-]cc"
         # define all the cuts
        _K1Comb12Cuts  = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV)" % params
        _K1CombCuts    = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV) & ((APT1+APT2+APT3) > %(K1_SumPTHad)s*MeV)" % params

        _K1MotherCuts  = "(VFASPF(VCHI2) < %(K1_VtxChi2)s) & (SUMTREE(MIPCHI2DV(PRIMARY),((ABSID=='K+') | (ABSID=='K-') | (ABSID=='pi+') | (ABSID=='pi-')),0.0) > %(K1_SumIPChi2Had)s)" % params
        _daughtersCuts_K = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNk > %(ProbNNCut)s)" % params
        _daughtersCuts_pi = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNpi > %(ProbNNCut)s)" % params
        _Combine = DaVinci__N3BodyDecays()
        _Combine.DecayDescriptor = _Decays
        _Combine.DaughtersCuts = {
            "K+"  : _daughtersCuts_K,
            "pi+" : _daughtersCuts_pi }
        _Combine.Combination12Cut = _K1Comb12Cuts
        _Combine.CombinationCut   = _K1CombCuts
        _Combine.MotherCut        = _K1MotherCuts
        # make and store the Selection object
        return Selection( name, Algorithm = _Combine, RequiredSelections = [ kaons, pions ] )
    #####################################################
    # Make a "[K_2(1770)+ -> K+ K+ K-]cc in range [K1_MassWindow_Lo, K1_MassWindow_Hi]
    #####################################################
    def _makeK2( self, name, kaons, params ) :
        _Decays = "[K_2(1770)+ -> K+ K+ K-]cc"
         # define all the cuts
        _K2Comb12Cuts  = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV) & (ACHI2DOCA(1,2) < 8)" % params
        _K2CombCuts    = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV) & ((APT1+APT2+APT3) > %(K1_SumPTHad)s*MeV)" % params
        _K2MotherCuts  = "(VFASPF(VCHI2) < %(K1_VtxChi2)s) & (SUMTREE(MIPCHI2DV(PRIMARY),((ABSID=='K+') | (ABSID=='K-')),0.0) > %(K1_SumIPChi2Had)s)" % params
        _daughtersCuts = "(P > %(ProtonP)s *MeV) & (TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNk > %(ProbNNCut)s)" % params
        _Combine = DaVinci__N3BodyDecays()
        _Combine.DecayDescriptor = _Decays
        _Combine.DaughtersCuts = {
            "K+"  : _daughtersCuts,
            "K-"  : _daughtersCuts,
        }
        _Combine.Combination12Cut = _K2Comb12Cuts
        _Combine.CombinationCut   = _K2CombCuts
        _Combine.MotherCut        = _K2MotherCuts
        # make and store the Selection object
        return Selection( name, Algorithm = _Combine, RequiredSelections = [ kaons ] )


#####################################################
    def _makeB2GammaConvertedX( self, name, DiElectron, hadrons, params, masscut = "(ADAMASS('B+')< 1500 *MeV" ):
        """
        CombineParticles / Selection for the B
        """
        _Decays = [ "[ B+ -> gamma K+ ]cc",
                    "[ B+ -> gamma pi+ ]cc",
                    "[ B+ -> gamma K*(892)+ ]cc",
                    " B0 -> gamma KS0 ",
                    "[ B0 -> gamma K*(892)0 ]cc",
                    " B_s0 -> gamma phi(1020) ",
                    "[ Lambda_b0 -> gamma Lambda0 ]cc",
                    "[ Lambda_b0 -> gamma Lambda(1520)0 ]cc", 
                     "[ B+ -> gamma K_1(1270)+ ]cc",
                    "[ B+ -> gamma K_2(1770)+ ]cc",
                    " B0 -> gamma rho(770)0 ",
                    "[ B0 -> gamma K*_0(1430)0 ]cc",
                    " B_s0 -> gamma f'_2(1525) ",
                    " B_s0 -> gamma f_2(1950) ",
                    "[ Lambda_b0 -> gamma N(1440)0 ]cc"
        ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ DiElectron, _Merge ] )

#####################################################
    def _makeB2XSingleConversions( self, name, SingleElectron, hadrons, params, combinationcut): #(ADAMASS('B+')< 5000 *MeV)  = "(ADOCACHI2CUT(25.,'')) & (AM > 3000.0 *MeV) & (AM < 7000.0 *MeV)" 
        """
        CombineParticles / Selection for the B
        """
        _Decays = [ "[ B+ -> e+ K+ ]cc",
                    "[ B+ -> e+ pi+ ]cc",
                    "[ B+ -> e+ K*(892)+ ]cc",
                    " B0 -> e+ KS0 ",
                    "[ B0 -> e+ K*(892)0 ]cc",
                    " B_s0 -> e+ phi(1020) ",
                    "[ Lambda_b0 -> e+ Lambda0 ]cc",
                    "[ Lambda_b0 -> e+ Lambda(1520)0 ]cc", 
                     "[ B+ -> e+ K_1(1270)+ ]cc",
                    "[ B+ -> e+ K_2(1770)+ ]cc",
                    " B0 -> e+ rho(770)0 ",
                    "[ B0 -> e+ K*_0(1430)0 ]cc",
                    " B_s0 -> e+ f'_2(1525) ",
                    " B_s0 -> e+ f_2(1950) ",
                    "[ Lambda_b0 -> e+ N(1440)0 ]cc",
                    #
                    "[ B+ -> e- K+ ]cc",
                    "[ B+ -> e- pi+ ]cc",
                    "[ B+ -> e- K*(892)+ ]cc",
                    " B0 -> e- KS0 ",
                    "[ B0 -> e- K*(892)0 ]cc",
                    " B_s0 -> e- phi(1020) ",
                    "[ Lambda_b0 -> e- Lambda0 ]cc",
                    "[ Lambda_b0 -> e- Lambda(1520)0 ]cc", 
                     "[ B+ -> e- K_1(1270)+ ]cc",
                    "[ B+ -> e- K_2(1770)+ ]cc",
                    " B0 -> e- rho(770)0 ",
                    "[ B0 -> e- K*_0(1430)0 ]cc",
                    " B_s0 -> e- f'_2(1525) ",
                    " B_s0 -> e- f_2(1950) ",
                    "[ Lambda_b0 -> e- N(1440)0 ]cc"
        ]
        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2_SingleConversions)s) & (BPVVDCHI2 > %(BFlightCHI2_SingleConversions)s) & (BPVHOPM() > %(HOPMin_SingleConversions)s) & (BPVHOPM() < %(HOPMax_SingleConversions)s))" % params
        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = combinationcut,
                                     MotherCut        = _Cut )
        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )
        return Selection(name, Algorithm = _Combine, RequiredSelections = [ SingleElectron, _Merge ] )