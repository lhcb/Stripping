###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of B- ->Lambda0 p~- gamma stripping Selections and StrippingLines.
Provides functions to build Lambda0->DD, Lambda0->LL, and Lambda0->LD selections.
Provides class B2LambdapbargammaLinesConf, which constructs the Selections and StrippingLines
given a configuration dictionary.
Exported symbols (use python help!):
   - B2LambdapbargammaLinesConf
"""

__author__ = ["Xingyu Tong"]
__date__ = '10/07/2023'
__version__ = '$Revision: 1.0 $'
__all__ = 'StrippingB2LambdapbargammaLinesConf'

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

from StandardParticles import StdLooseProtons as Protons
from StandardParticles import StdLooseAllPhotons as Photons

'''
2016:
StrippingReport                                                INFO Event 20000, Good event 20000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |_StrippingGlobal_                                            |  0.4350|        87|       |   6.131|
 |_StrippingSequenceStreamBhadron_                             |  0.4350|        87|       |   6.119|
 |!StrippingB2LambdapbargammaDDLine                            |  0.0900|        18|  1.611|   3.799|
 |!StrippingB2LambdapbargammaLLLine                            |  0.2800|        56|  1.661|   0.911|
 |!StrippingB2LambdapbargammaLDLine                            |  0.0700|        14|  1.500|   0.644|
 |!StrippingB2LambdapbargammaDDLine_TIMING                     |  0.0900|        18|  1.611|   0.047|
 |!StrippingB2LambdapbargammaLLLine_TIMING                     |  0.2800|        56|  1.661|   0.058|
 |!StrippingB2LambdapbargammaLDLine_TIMING                     |  0.0700|        14|  1.500|   0.035|
2017:
StrippingReport                                                INFO Event 20000, Good event 20000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |_StrippingGlobal_                                            |  0.5650|       113|       |   7.367|
 |_StrippingSequenceStreamBhadron_                             |  0.5650|       113|       |   7.356|
 |!StrippingB2LambdapbargammaDDLine                            |  0.1800|        36|  1.528|   4.564|
 |!StrippingB2LambdapbargammaLLLine                            |  0.3050|        61|  1.721|   1.121|
 |!StrippingB2LambdapbargammaLDLine                            |  0.0950|        19|  1.684|   0.788|
 |!StrippingB2LambdapbargammaDDLine_TIMING                     |  0.1800|        36|  1.528|   0.056|
 |!StrippingB2LambdapbargammaLLLine_TIMING                     |  0.3050|        61|  1.721|   0.061|
 |!StrippingB2LambdapbargammaLDLine_TIMING                     |  0.0950|        19|  1.684|   0.041|
2018:
StrippingReport                                                INFO Event 20000, Good event 20000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |_StrippingGlobal_                                            |  0.6600|       132|       |   7.870|
 |_StrippingSequenceStreamBhadron_                             |  0.6600|       132|       |   7.855|
 |!StrippingB2LambdapbargammaDDLine                            |  0.2000|        40|  1.450|   4.869|
 |!StrippingB2LambdapbargammaLLLine                            |  0.3600|        72|  1.694|   1.227|
 |!StrippingB2LambdapbargammaLDLine                            |  0.1250|        25|  1.240|   0.828|
 |!StrippingB2LambdapbargammaDDLine_TIMING                     |  0.2000|        40|  1.450|   0.051|
 |!StrippingB2LambdapbargammaLLLine_TIMING                     |  0.3600|        72|  1.694|   0.055|
 |!StrippingB2LambdapbargammaLDLine_TIMING                     |  0.1250|        25|  1.240|   0.043|
'''

default_config = {
    'NAME': 'B2Lambdapbargamma',
    'WGs': ['RD'],
    'BUILDERTYPE': 'B2LambdapbargammaLinesConf',
    'CONFIG': {'Trk_Chi2': 4.0,
               'Trk_GhostProb': 0.4,
               'gamma_PT_MIN': 1000.0,
               'gamma_CL_MIN': 0.25,
               'gamma_ISNOTE_MIN': -999.0,
               'ProtonP': 5000.,  # MeV
               'ProtonPT': 500.,  # MeV
               'ProtonPIDp': 0.,
               'ProtonDLLpK': 0.,
               'Lambda_DD_MassWindow': 20.0,
               'Lambda_DD_VtxChi2': 9.0,
               'Lambda_DD_FDChi2': 50.0,
               'Lambda_DD_Pmin': 5000.0,
               'Lambda_LL_MassWindow': 20.0,
               'Lambda_LL_VtxChi2': 9.0,
               'Lambda_LD_MassWindow': 25.0,
               'Lambda_LD_VtxChi2': 16.0,
               'Lambda_LD_FDChi2': 50.0,
               'Lambda_LD_Pmin': 5000.0,
               'B0_Mlow': 779.0,
               'B0_Mhigh': 1921.0,
               'B0_APTmin': 0.0,
               'B0_PTmin': 0.0,
               'B0Daug_DD_maxDocaChi2': 5.0,
               'B0Daug_LL_maxDocaChi2': 5.0,
               'B0Daug_LD_maxDocaChi2': 5.0,
               'B0Daug_DD_PTsum': 0.0,
               'B0Daug_LL_PTsum': 0.0,
               'B0Daug_LD_PTsum': 0.0,
               'B0_VtxChi2': 16.0,
               'B0_Dira': 0.9990,
               'B0_DD_IPCHI2wrtPV': 16.0,
               'B0_LL_IPCHI2wrtPV': 16.0,
               'B0_LD_IPCHI2wrtPV': 16.0,
               'B0_FDwrtPV': 0.8,
               'B0_DD_FDChi2': 25,
               'B0_LL_FDChi2': 25,
               'B0_LD_FDChi2': 25,
               'GEC_MaxTracks': 250,
               'Prescale': 1.0,
               'Postscale': 1.0,
               'RelatedInfoTools': [{"Type": "RelInfoConeVariables",
                                     "ConeAngle": 1.7,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar17'
                                     },
                                    {"Type": "RelInfoConeVariables",
                                     "ConeAngle": 1.5,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar15'
                                     },
                                    {"Type": "RelInfoConeVariables",
                                     "ConeAngle": 1.0,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar10'
                                     },
                                    {"Type": "RelInfoConeVariables",
                                     "ConeAngle": 0.8,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar08'
                                     },
                                    {"Type": "RelInfoVertexIsolation",
                                     "Location": "VtxIsolationVar"
                                     }
                                    ]
               },
    'STREAMS': ['Bhadron']
}


class B2LambdapbargammaLinesConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        GECCode = {'Code': "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %s)" % config['GEC_MaxTracks'],
                   'Preambulo': ["from LoKiTracks.decorators import *"]}

        self.protons = Protons
        self.photons = Photons

        self.makeLambda2DD('Lambda0DD', config)
        self.makeLambda2LL('Lambda0LL', config)
        self.makeLambda2LD('Lambda0LD', config)

        namesSelections = [(name + 'DD', self.makeB2LambdapbargammaDD(name + 'DD', config)),
                           (name + 'LL', self.makeB2LambdapbargammaLL(name + 'LL', config)),
                           (name + 'LD', self.makeB2LambdapbargammaLD(name + 'LD', config)),
                           ]

        # make lines

        for selName, sel in namesSelections:

            extra = {}

            line = StrippingLine(selName + 'Line',
                                 selection=sel,
                                 prescale=config['Prescale'],
                                 postscale=config['Postscale'],
                                 RelatedInfoTools=config['RelatedInfoTools'],
                                 FILTER=GECCode,
                                 **extra)

            self.registerLine(line)

    def makeLambda2DD(self, name, config):
        # define all the cuts
        _massCut = "(ADMASS('Lambda0')<%s*MeV)" % config['Lambda_DD_MassWindow']
        _vtxCut = "(VFASPF(VCHI2)<%s)   " % config['Lambda_DD_VtxChi2']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['Lambda_DD_FDChi2']
        _momCut = "(P>%s*MeV)" % config['Lambda_DD_Pmin']

        _allCuts = _momCut
        _allCuts += '&'+_massCut
        _allCuts += '&'+_vtxCut
        _allCuts += '&'+_fdChi2Cut

        # get the Lambda0's to filter
        _stdLambdaDD = DataOnDemand(Location="Phys/StdLooseLambdaDD/Particles")

        # make the filter
        _filterLambdaDD = FilterDesktop(Code=_allCuts)

        # make and store the Selection object
        self.selLambda2DD = Selection(
            name, Algorithm=_filterLambdaDD, RequiredSelections=[_stdLambdaDD])

        return self.selLambda2DD

    def makeLambda2LL(self, name, config):
        # define all the cuts
        _massCut = "(ADMASS('Lambda0')<%s*MeV)" % config['Lambda_LL_MassWindow']
        _vtxCut = "(VFASPF(VCHI2)<%s)" % config['Lambda_LL_VtxChi2']
        _trkChi2Cut1 = "(CHILDCUT((TRCHI2DOF<%s),1))" % config['Trk_Chi2']
        _trkChi2Cut2 = "(CHILDCUT((TRCHI2DOF<%s),2))" % config['Trk_Chi2']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))" % config['Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))" % config['Trk_GhostProb']

        _allCuts = _massCut
        _allCuts += '&'+_trkChi2Cut1
        _allCuts += '&'+_trkChi2Cut2
        _allCuts += '&'+_vtxCut
        _allCuts += '&'+_trkGhostProbCut1
        _allCuts += '&'+_trkGhostProbCut2

        # get the Lambda's to filter
        _stdLambdaLL = DataOnDemand(
            Location="Phys/StdVeryLooseLambdaLL/Particles")

        # make the filter
        _filterLambdaLL = FilterDesktop(Code=_allCuts)

        # make and store the Selection object
        self.selLambda2LL = Selection(
            name, Algorithm=_filterLambdaLL, RequiredSelections=[_stdLambdaLL])

        return self.selLambda2LL

    def makeLambda2LD(self, name, config):
        # define all the cuts
        _massCut = "(ADMASS('Lambda0')<%s*MeV)" % config['Lambda_LD_MassWindow']
        _vtxCut = "(VFASPF(VCHI2)<%s)   " % config['Lambda_LD_VtxChi2']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['Lambda_LD_FDChi2']
        _momCut = "(P>%s*MeV)" % config['Lambda_LD_Pmin']

        _allCuts = _momCut
        _allCuts += '&'+_massCut
        _allCuts += '&'+_vtxCut
        _allCuts += '&'+_fdChi2Cut

        # get the Lambda0's to filter
        _stdLambdaLD = DataOnDemand(Location="Phys/StdLooseLambdaLD/Particles")

        # make the filter
        _filterLambdaLD = FilterDesktop(Code=_allCuts)

        # make and store the Selection object
        self.selLambda2LD = Selection(
            name, Algorithm=_filterLambdaLD, RequiredSelections=[_stdLambdaLD])

        return self.selLambda2LD

    def makeB2LambdapbargammaDD(self, name, config):
        """
        Create and store a B- ->Lambda0(DD) p~- gamma Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _gammacut = "(PT > %s*MeV)" % config['gamma_PT_MIN']
        _gammacut += '&'+"(CL > %f)" % config['gamma_CL_MIN']
        _gammacut += '&' + \
            "(PPINFO(LHCb.ProtoParticle.IsNotE,-1) > %s)" % config['gamma_ISNOTE_MIN']

        _massCutLow = "(AM>(5279-%s)*MeV)" % config['B0_Mlow']
        _massCutHigh = "(AM<(5279+%s)*MeV)" % config['B0_Mhigh']
        _aptCut = "(APT>%s*MeV)" % config['B0_APTmin']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))" % config['B0Daug_DD_maxDocaChi2']
        _daugPtSumCut = "((APT1+APT2+APT3)>%s*MeV)" % config['B0Daug_DD_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut = "(PT>%s*MeV)" % config['B0_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)" % config['B0_VtxChi2']
        _diraCut = "(BPVDIRA>%s)" % config['B0_Dira']
        _ipChi2Cut = "(MIPCHI2DV(PRIMARY)<%s)" % config['B0_DD_IPCHI2wrtPV']
        _fdCut = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B0_FDwrtPV']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['B0_DD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdCut  # lookhere
        _motherCuts += '&'+_fdChi2Cut

        _B0 = CombineParticles()

        _B0.DecayDescriptors = ["[B- -> Lambda0 p~- gamma]cc"]

        _trkGhostProbCut = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut = "(TRCHI2DOF<%s)" % config['Trk_Chi2']
        _protonPCut = "(P>%s)" % config["ProtonP"]
        _protonPTCut = "(PT>%s)" % config["ProtonPT"]
        _protonPIDpCut = "(PIDp-PIDpi> %s ) " % config["ProtonPIDp"]
        _protonDLLpKCut = "(PIDp-PIDK> %s ) " % config["ProtonDLLpK"]

        _daughtersCuts = _trkChi2Cut
        _daughtersCuts += '&'+_trkGhostProbCut
        _daughtersCuts += '&' + _protonPCut
        _daughtersCuts += '&' + _protonPTCut
        _daughtersCuts += '&' + _protonPIDpCut
        _daughtersCuts += '&' + _protonDLLpKCut

        _B0.DaughtersCuts = {"p~-": _daughtersCuts,
                             "gamma": _gammacut,
                             }
        _B0.CombinationCut = _combCuts
        _B0.MotherCut = _motherCuts
        _B0.ReFitPVs = True

        _B0Conf = _B0.configurable(name + '_combined')

        self.selB2LambdapbargammaDD = Selection(name, Algorithm=_B0Conf, RequiredSelections=[
                                                self.selLambda2DD, self.protons, self.photons])
        return self.selB2LambdapbargammaDD

    def makeB2LambdapbargammaLL(self, name, config):
        """
        Create and store a B- ->Lambda0(LL) p~- gamma Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _gammacut = "(PT > %s*MeV)" % config['gamma_PT_MIN']
        _gammacut += '&'+"(CL > %f)" % config['gamma_CL_MIN']
        _gammacut += '&' + \
            "(PPINFO(LHCb.ProtoParticle.IsNotE,-1) > %s)" % config['gamma_ISNOTE_MIN']

        _massCutLow = "(AM>(5279-%s)*MeV)" % config['B0_Mlow']
        _massCutHigh = "(AM<(5279+%s)*MeV)" % config['B0_Mhigh']
        _aptCut = "(APT>%s*MeV)" % config['B0_APTmin']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))" % config['B0Daug_LL_maxDocaChi2']
        _daugPtSumCut = "((APT1+APT2+APT3)>%s*MeV)" % config['B0Daug_LL_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut = "(PT>%s*MeV)" % config['B0_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)" % config['B0_VtxChi2']
        _diraCut = "(BPVDIRA>%s)" % config['B0_Dira']
        _ipChi2Cut = "(MIPCHI2DV(PRIMARY)<%s)" % config['B0_LL_IPCHI2wrtPV']
        _fdCut = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B0_FDwrtPV']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['B0_LL_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdChi2Cut
        _motherCuts += '&'+_fdCut

        _B0 = CombineParticles()

        _B0.DecayDescriptors = ["[B- -> Lambda0 p~- gamma]cc"]

        _trkGhostProbCut = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut = "(TRCHI2DOF<%s)" % config['Trk_Chi2']
        _protonPCut = "(P>%s)" % config["ProtonP"]
        _protonPTCut = "(PT>%s)" % config["ProtonPT"]
        _protonPIDpCut = "(PIDp-PIDpi> %s ) " % config["ProtonPIDp"]
        _protonDLLpKCut = "(PIDp-PIDK> %s ) " % config["ProtonDLLpK"]

        _daughtersCuts = _trkChi2Cut
        _daughtersCuts += '&'+_trkGhostProbCut
        _daughtersCuts += '&' + _protonPCut
        _daughtersCuts += '&' + _protonPTCut
        _daughtersCuts += '&' + _protonPIDpCut
        _daughtersCuts += '&' + _protonDLLpKCut

        _B0.DaughtersCuts = {"p~-": _daughtersCuts,
                             "gamma": _gammacut,
                             }
        _B0.CombinationCut = _combCuts
        _B0.MotherCut = _motherCuts
        _B0.ReFitPVs = True

        _B0Conf = _B0.configurable(name + '_combined')

        self.selB2LambdapbargammaLL = Selection(
            name, Algorithm=_B0Conf, RequiredSelections=[self.selLambda2LL, self.protons, self.photons])
        return self.selB2LambdapbargammaLL

    def makeB2LambdapbargammaLD(self, name, config):
        """
        Create and store a B- ->Lambda0(LD) p~- gamma Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """
        _gammacut = "(PT > %s*MeV)" % config['gamma_PT_MIN']
        _gammacut += '&'+"(CL > %f)" % config['gamma_CL_MIN']
        _gammacut += '&' + \
            "(PPINFO(LHCb.ProtoParticle.IsNotE,-1) > %s)" % config['gamma_ISNOTE_MIN']

        _massCutLow = "(AM>(5279-%s)*MeV)" % config['B0_Mlow']
        _massCutHigh = "(AM<(5279+%s)*MeV)" % config['B0_Mhigh']
        _aptCut = "(APT>%s*MeV)" % config['B0_APTmin']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))" % config['B0Daug_LD_maxDocaChi2']
        _daugPtSumCut = "((APT1+APT2+APT3)>%s*MeV)" % config['B0Daug_LD_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut = "(PT>%s*MeV)" % config['B0_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)" % config['B0_VtxChi2']
        _diraCut = "(BPVDIRA>%s)" % config['B0_Dira']
        _ipChi2Cut = "(MIPCHI2DV(PRIMARY)<%s)" % config['B0_LD_IPCHI2wrtPV']
        _fdCut = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B0_FDwrtPV']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['B0_LD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdChi2Cut
        _motherCuts += '&'+_fdCut

        _B0 = CombineParticles()

        _B0.DecayDescriptors = ["[B- -> Lambda0 p~- gamma]cc"]

        _trkGhostProbCut = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut = "(TRCHI2DOF<%s)" % config['Trk_Chi2']
        _protonPCut = "(P>%s)" % config["ProtonP"]
        _protonPTCut = "(PT>%s)" % config["ProtonPT"]
        _protonPIDpCut = "(PIDp-PIDpi> %s ) " % config["ProtonPIDp"]
        _protonDLLpKCut = "(PIDp-PIDK> %s ) " % config["ProtonDLLpK"]

        _daughtersCuts = _trkChi2Cut
        _daughtersCuts += '&'+_trkGhostProbCut
        _daughtersCuts += '&' + _protonPCut
        _daughtersCuts += '&' + _protonPTCut
        _daughtersCuts += '&' + _protonPIDpCut
        _daughtersCuts += '&' + _protonDLLpKCut

        _B0.DaughtersCuts = {"p~-": _daughtersCuts,
                             "gamma": _gammacut,
                             }
        _B0.CombinationCut = _combCuts
        _B0.MotherCut = _motherCuts
        _B0.ReFitPVs = True

        _B0Conf = _B0.configurable(name + '_combined')

        self.selB2LambdapbargammaLD = Selection(name, Algorithm=_B0Conf, RequiredSelections=[
                                                self.selLambda2LD, self.protons, self.photons])
        return self.selB2LambdapbargammaLD
