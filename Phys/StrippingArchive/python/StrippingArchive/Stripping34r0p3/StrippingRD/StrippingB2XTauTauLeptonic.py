###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for
    Bs to K K tau tau
    Bs to K* K* tau tau
    B0 to K pi tau tau
    Lb to p K tau tau
Contains tau decays to muons or electrons
Same-sign combinations and reverse PID lines are included.
"""

__author__ = 'H. Tilquin, Lakshan Madhan, Josh Bex'
__date__ = '09/06/2023'
__version__ = '$Revision: 1.1 $'

__all__ = ('B2XTauTauLeptonicConf', 'default_config')

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop, DaVinci__N3BodyDecays

from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME': 'B2XTauTauLeptonic',
    'BUILDERTYPE': 'B2XTauTauLeptonicConf',
    'CONFIG':
        {
            "Bs_Comb_MassHigh": 7000.0,
            "Bd_Comb_MassHigh": 6500.0,
            "Lb_Comb_MassHigh": 7500.0,
            "Bs_VertDist": -15, 
            "Bs_VertDist_E": -4, 
            "Bd_VertDist": -15, 
            "Bd_VertDist_E": -2.5,  
            "Lb_VertDist": -15, 
            "Lb_VertDist_E": -3, 
            "B_DIRA": 0.999,
            "B_DIRA_B2Kstar_E": 0.9993,
            "Lb_DIRA": 0.999, 
            "Bs_VertexCHI2": 150.0,
            "B0_VertexCHI2": 81.0,
            "Lb_VertexCHI2": 150.0,
            "Dau_DIRA": 0.99,
            "Dimuon_DIRA": 0.9,
            "Dielectron_DIRA": 0.97, 
            "DiMuE_DIRA": 0.97,
            "Hadron_MinIPCHI2": 16.0, 
            "Muon_MinIPCHI2": 6.0,
            "Electron_MinIPCHI2": 12.0, 
            "DimuonUPPERMASS": 4350.0, 
            "DielectronUPPERMASS": 4350.0,
            "DiMuEUPPERMASS": 4350.0,
            "Phi_FlightChi2": 25.0,
            "Phi_Comb_MassHigh": 1850.0,
            "Phi_PT": 600,
            "Phi_VertexCHI2": 3,
            "Kstar_for_B2Kstar_FlightChi2": 300.0,
            "Kstar_for_B2Kstar_Comb_MassHigh": 1750.0,
            "Kstar_for_B2Kstar_Comb_MassLow": 795.0,
            "Kstar_for_B2Kstar_PT": 1050,
            "Kstar_for_B2Kstar_VertexCHI2": 3,
            "Kstar_for_B2KstarKstar_MassWindow": 100,
            "Kstar_for_B2KstarKstar_PT": 400,
            "Kstar_for_B2KstarKstar_VertexCHI2": 6,
            "KstarKstar_DOCACHI2": 50,
            "KstarKstar_Comb_MassHigh": 2400,
            "Lambdastar_FlightChi2": 25.0,
            "Lambdastar_Comb_MassHigh": 2500.0,
            "Lambdastar_Comb_MassLow": 1400.0,
            "Lambdastar_PT": 1400, 
            "Lambdastar_VertexCHI2": 3,
            "Dimuon_VertexCHI2": 20, 
            "Dielectron_VertexCHI2": 20,
            "DiMuE_VertexCHI2": 20,
            "MuonPID": 0.0,
            "ElectronPID": 4.0, 
            "KaonPID": 4.0,
            "ProtonPID": 5.0, 
            "Pion_ProbNN_B2Kstar": 0.85, 
            "Kaon_Pion_ProbNN_B2Kstar": 0.8,
            "Pion_Electron_ProbNN_B2Kstar_E": 0.8,
            "Hadron_P": 3000, 
            "Muon_PT": 250,
            "Electron_P": 3000, 
            "Electron_PT": 250,
            "SpdMult": 600,
            "Track_GhostProb": 0.3,
            "Track_TRCHI2": 3,
            "UseNoPIDsHadrons": False,
            "UseNoPIDsLeptons": False,
            "HLT1_FILTER": None,
            "HLT2_FILTER": None,
            "L0DU_FILTER": None,
        },

    'WGs': ['RD'],
    'STREAMS': ['Semileptonic']
}


class B2XTauTauLeptonicConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name

        self.BsCombCut = "(AM < %(Bs_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bs_VertDist)s * mm)" % config
        self.BsCombCut_E = "(AM < %(Bs_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bs_VertDist_E)s * mm)" % config
        self.Bs_for_B2KstarKstarCombCut = "(AM < %(Bs_Comb_MassHigh)s * MeV)" % config
        self.BsCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Bs_VertexCHI2)s)" % config

        self.BdCombCut = "(AM < %(Bd_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bd_VertDist)s * mm)" % config
        self.BdCombCut_E = "(AM < %(Bd_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bd_VertDist_E)s * mm)" % config
        self.BdCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(B0_VertexCHI2)s)" % config
        self.BdCut_E = "(BPVDIRA > %(B_DIRA_B2Kstar_E)s) & (VFASPF(VCHI2/VDOF) < %(B0_VertexCHI2)s)" % config

        self.LambdaBCombCut = "(AM < %(Lb_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Lb_VertDist)s * mm)" % config
        self.LambdaBCombCut_E = "(AM < %(Lb_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Lb_VertDist_E)s * mm)" % config
        self.LambdaBCut = "(BPVDIRA> %(Lb_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Lb_VertexCHI2)s)" % config

        DaughterCuts = "(BPVDIRA> %(Dau_DIRA)s)" % config

        self.PhiCombCut = "(AM < %(Phi_Comb_MassHigh)s * MeV)" % config
        self.PhiCut = DaughterCuts + " & (VFASPF(VCHI2/VDOF) < %(Phi_VertexCHI2)s) & (PT > %(Phi_PT)s * MeV) & " \
                                     "(BPVVDCHI2 > %(Phi_FlightChi2)s)" % config

        self.Kstar_for_B2KstarCombCut = "(AM < %(Kstar_for_B2Kstar_Comb_MassHigh)s*MeV) & (AM > %(Kstar_for_B2Kstar_Comb_MassLow)s*MeV)" % config
        self.Kstar_for_B2KstarCut = DaughterCuts + " & (VFASPF(VCHI2/VDOF) < %(Kstar_for_B2Kstar_VertexCHI2)s) & " \
                                                   "(PT > %(Kstar_for_B2Kstar_PT)s * MeV) & (BPVVDCHI2 > %(Kstar_for_B2Kstar_FlightChi2)s)" % config

        self.Kstar_for_B2KstarKstarCombCut = "(ADAMASS('K*(892)0')< %(Kstar_for_B2KstarKstar_MassWindow)s * MeV )" % config
        self.Kstar_for_B2KstarKstarCut = DaughterCuts + " &  (VFASPF(VCHI2/VDOF) < %(Kstar_for_B2KstarKstar_VertexCHI2)s) & " \
                                                        "(PT > %(Kstar_for_B2KstarKstar_PT)s * MeV)" % config

        self.LambdaStarCombCut = "(AM < %(Lambdastar_Comb_MassHigh)s * MeV) & (AM > %(Lambdastar_Comb_MassLow)s * MeV)" % config
        self.LambdaStarCut = DaughterCuts + \
            " & (PT > %(Lambdastar_PT)s * MeV) & (VFASPF(VCHI2/VDOF) < %(Lambdastar_VertexCHI2)s) & " \
                 "(BPVVDCHI2 > %(Lambdastar_FlightChi2)s)" % config

        self.DiMuonCombCut = "(AM < %(DimuonUPPERMASS)s * MeV) " % config
        self.DiMuonCut = "(BPVDIRA> %(Dimuon_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Dimuon_VertexCHI2)s)" % config

        self.DiElectronCombCut = "(AM < %(DielectronUPPERMASS)s * MeV) " % config
        self.DiElectronCut = "(BPVDIRA> %(Dielectron_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Dielectron_VertexCHI2)s)" % config

        self.DiMuECombCut = "(AM < %(DiMuEUPPERMASS)s * MeV) " % config
        self.DiMuECut = "(BPVDIRA> %(DiMuE_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(DiMuE_VertexCHI2)s)" % config

        self.TrackCuts = "(TRGHP < %(Track_GhostProb)s) & (TRCHI2DOF < %(Track_TRCHI2)s)" % config

        self.HadronCuts = "(MIPCHI2DV(PRIMARY) > %(Hadron_MinIPCHI2)s)" % config

        self.KaonCutBase = self.TrackCuts + " & " + \
            self.HadronCuts + " & (P > %(Hadron_P)s * MeV)" % config
        self.KaonCut = self.KaonCutBase + \
            " & (PIDK > %(KaonPID)s) & (~ISMUON)" % config
        self.KaonCut_B2Kstar = self.KaonCutBase + \
            " & (PROBNNK * (1-PROBNNpi) > %(Kaon_Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.KaonCutReversePID_B2Kstar = self.KaonCutBase + \
            " & (PROBNNK * (1-PROBNNpi) < %(Kaon_Pion_ProbNN_B2Kstar)s)" % config
        self.KaonCutReversePID = self.KaonCutBase + \
            " & (PIDK < %(KaonPID)s)" % config

        self.PionCutBase = self.TrackCuts + " & " + self.HadronCuts
        self.PionCut_B2Kstar = self.PionCutBase + \
            " & (PROBNNpi > %(Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.PionCut_B2Kstar_E = self.PionCutBase + \
            " & (PROBNNpi > %(Pion_ProbNN_B2Kstar)s) & (~ISMUON) & " + \
                " (PROBNNpi * (1-PROBNNe) > %(Pion_Electron_ProbNN_B2Kstar_E)s) " % config
        self.PionCutReversePID_B2Kstar = self.PionCutBase + \
            " & (PROBNNpi < %(Pion_ProbNN_B2Kstar)s)" % config

        self.MuonCutBase = self.TrackCuts + \
            " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s) & (PT> %(Muon_PT)s * MeV)" % config
        self.MuonCut = self.MuonCutBase + \
            " & (PIDmu> %(MuonPID)s) & (ISMUON)" % config
        self.MuonCutReversePID = self.MuonCutBase + \
            " & (PIDmu < %(MuonPID)s) " % config

        self.ElectronCutBase = self.TrackCuts + \
            " & (MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s) & (P> %(Electron_P)s * MeV) & (PT> %(Electron_PT)s * MeV)" % config
        self.ElectronCut = self.ElectronCutBase + \
            " & (PIDe> %(ElectronPID)s) & (~ISMUON)" % config
        self.ElectronCutReversePID = self.ElectronCutBase + \
            " & (PIDe < %(ElectronPID)s) " % config

        self.ProtonCutBase = self.TrackCuts + " & " + \
            self.HadronCuts + " & (P > %(Hadron_P)s * MeV)" % config
        self.ProtonCut = self.ProtonCutBase + \
            " & (PIDp > %(ProtonPID)s)  & (~ISMUON)" % config
        self.ProtonCutReversePID = self.ProtonCutBase + \
            " & (PIDp < %(ProtonPID)s)" % config

        self.Kaons = self.__Kaons__(config)
        self.FakeKaons = self.__FakeKaons__()

        self.Kaons_Kstar = self.__Kaons__(config, sel_name="_B2Kstar")
        self.FakeKaons_Kstar = self.__FakeKaons__(sel_name="_B2Kstar")

        self.Pions_Kstar = self.__Pions__(config, sel_name="_B2Kstar")
        self.Pions_Kstar_E = self.__Pions__(config, sel_name="_B2Kstar_E")
        self.FakePions_Kstar = self.__FakePions__(sel_name="_B2Kstar")

        self.Muons = self.__Muons__(config)
        self.FakeMuons = self.__FakeMuons__()

        self.Electrons = self.__Electrons__(config)
        self.FakeElectrons = self.__FakeElectrons__()

        self.Protons = self.__Protons__(config)
        self.FakeProtons = self.__FakeProtons__()

        self.Dimuon = self.__Dimuon__(self.Muons)
        self.FakeDimuon = self.__FakeDimuon__(
            self.Muons, self.FakeMuons, conf=config)

        self.Dielectron = self.__Dielectron__(self.Electrons)
        self.FakeDielectron = self.__FakeDielectron__(
            self.Electrons, self.FakeElectrons, conf=config)

        self.DiMuE = self.__DiMuE__(
            self.Muons, self.Electrons, pid_selection="")
        self.DiMuE_fakeelectron = self.__DiMuE__(
            self.Muons, self.FakeElectrons,  pid_selection="fakeelectron")
        self.DiMuE_fakemuon = self.__DiMuE__(
            self.FakeMuons, self.Electrons, pid_selection="fakemuon")

        self.Phi = self.__Phi__(self.Kaons, conf=config)
        self.FakePhi = self.__Phi__(self.Kaons, self.FakeKaons, conf=config)

        self.Kstar_for_B2KstarKstar = self.__Kstar__(
            self.Kaons_Kstar, self.Pions_Kstar, sel_name="_B2KstarKstar")
        self.FakePionKstar_for_B2KstarKstar = self.__Kstar__(self.Kaons_Kstar, self.FakePions_Kstar, pid_selection="ReversePIDPi_",
                                                             sel_name="_B2KstarKstar")
        self.FakeKaonKstar_for_B2KstarKstar = self.__Kstar__(self.FakeKaons_Kstar, self.Pions_Kstar, pid_selection="ReversePIDK_",
                                                             sel_name="_B2KstarKstar")

        self.Kstar_for_B2Kstar = self.__Kstar__(
            self.Kaons_Kstar, self.Pions_Kstar, sel_name="_B2Kstar")
        self.Kstar_for_B2Kstar_E = self.__Kstar__(
            self.Kaons_Kstar, self.Pions_Kstar_E, sel_name="_B2Kstar_E")
        self.FakePionKstar_for_B2Kstar = self.__Kstar__(self.Kaons_Kstar, self.FakePions_Kstar,
                                                        pid_selection="ReversePIDPi_", sel_name="_B2Kstar")
        self.FakeKaonKstar_for_B2Kstar = self.__Kstar__(self.FakeKaons_Kstar, self.Pions_Kstar,
                                                        pid_selection="ReversePIDK_", sel_name="_B2Kstar")

        self.LambdaStar = self.__Lambdastar__(self.Protons, self.Kaons)
        self.FakeProtonLambdaStar = self.__Lambdastar__(
            self.FakeProtons, self.Kaons, pid_selection="ReversePIDp_")
        self.FakeKaonLambdaStar = self.__Lambdastar__(
            self.Protons, self.FakeKaons, pid_selection="ReversePIDK_")

        # K K algorithms
        self.Bs_Mu = self.__Bs_Phi__(
            daughters=[self.Phi, self.Dimuon], pid_selection="Mu")
        self.Bs_Mu_ReversePIDmu = self.__Bs_Phi__(
            daughters=[self.Phi, self.FakeDimuon], pid_selection="Mu_ReversePIDmu_")
        self.Bs_Mu_ReversePIDK = self.__Bs_Phi__(
            daughters=[self.FakePhi, self.Dimuon], pid_selection="Mu_ReversePIDK_")
        self.Bs_E = self.__Bs_Phi__(
            daughters=[self.Phi, self.Dielectron], pid_selection="E", e_mode=True)
        self.Bs_E_ReversePIDe = self.__Bs_Phi__(
            daughters=[self.Phi, self.FakeDielectron], pid_selection="E_ReversePIDe_", e_mode=True)
        self.Bs_E_ReversePIDK = self.__Bs_Phi__(
            daughters=[self.FakePhi, self.Dielectron], pid_selection="E_ReversePIDK_", e_mode=True)
        self.Bs_MuE = self.__Bs_Phi__(
            daughters=[self.Phi, self.DiMuE], pid_selection="MuE", e_mode=True)
        self.Bs_MuE_ReversePIDe = self.__Bs_Phi__(
            daughters=[self.Phi, self.DiMuE_fakeelectron], pid_selection="MuE_ReversePIDe_", e_mode=True)
        self.Bs_MuE_ReversePIDmu = self.__Bs_Phi__(
            daughters=[self.Phi, self.DiMuE_fakemuon], pid_selection="MuE_ReversePIDmu_", e_mode=True)
        self.Bs_MuE_ReversePIDK = self.__Bs_Phi__(
            daughters=[self.FakePhi, self.DiMuE], pid_selection="MuE_ReversePIDK_", e_mode=True)

        # Kst Kst algorithms
        self.Bs_kstarkstar_Mu = self.__Bs_KstarKstar__(
            [self.Kstar_for_B2KstarKstar, self.Dimuon], conf=config, pid_selection="Mu")
        self.Bs_kstarkstar_Mu_ReversepidK = self.__Bs_KstarKstar__(
            daughters=[self.FakeKaonKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.Dimuon], conf=config, pid_selection="Mu_ReversePIDK_")
        self.Bs_kstarkstar_Mu_Reversepidpi = self.__Bs_KstarKstar__(
            daughters=[self.FakePionKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.Dimuon], conf=config, pid_selection="Mu_ReversePIDpi_")
        self.Bs_kstarkstar_Mu_Reversepidmu = self.__Bs_KstarKstar__(
            daughters=[self.Kstar_for_B2KstarKstar, self.FakeDimuon], conf=config, pid_selection="Mu_ReversePIDmu_")
        self.Bs_kstarkstar_E = self.__Bs_KstarKstar__(
            [self.Kstar_for_B2KstarKstar, self.Dielectron], conf=config, pid_selection="E")
        self.Bs_kstarkstar_E_ReversepidK = self.__Bs_KstarKstar__(
            daughters=[self.FakeKaonKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.Dielectron], conf=config, pid_selection="E_ReversePIDK_")
        self.Bs_kstarkstar_E_Reversepidpi = self.__Bs_KstarKstar__(
            daughters=[self.FakePionKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.Dielectron], conf=config, pid_selection="E_ReversePIDpi_")
        self.Bs_kstarkstar_E_Reversepide = self.__Bs_KstarKstar__(
            daughters=[self.Kstar_for_B2KstarKstar, self.FakeDielectron], conf=config, pid_selection="E_ReversePIDe_")
        self.Bs_kstarkstar_MuE = self.__Bs_KstarKstar__(
            [self.Kstar_for_B2KstarKstar, self.DiMuE], conf=config, pid_selection="MuE")
        self.Bs_kstarkstar_MuE_ReversepidK = self.__Bs_KstarKstar__(
            daughters=[self.FakeKaonKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.DiMuE], conf=config, pid_selection="MuE_ReversePIDK_")
        self.Bs_kstarkstar_MuE_Reversepidpi = self.__Bs_KstarKstar__(
            daughters=[self.FakePionKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.DiMuE], conf=config, pid_selection="MuE_ReversePIDpi_")
        self.Bs_kstarkstar_MuE_Reversepide = self.__Bs_KstarKstar__(
            daughters=[self.Kstar_for_B2KstarKstar, self.DiMuE_fakeelectron], conf=config, pid_selection="MuE_ReversePIDe_")
        self.Bs_kstarkstar_MuE_Reversepidmu = self.__Bs_KstarKstar__(
            daughters=[self.Kstar_for_B2KstarKstar, self.DiMuE_fakemuon], conf=config, pid_selection="MuE_ReversePIDmu_")

        # K pi algorithms
        self.Bd_kstar_Mu = self.__B0_Kstar__(
            daughters=[self.Kstar_for_B2Kstar, self.Dimuon], pid_selection="Mu")
        self.Bd_kstar_Mu_ReversepidK = self.__B0_Kstar__(
            daughters=[self.FakeKaonKstar_for_B2Kstar, self.Dimuon], pid_selection="Mu_ReversePIDK_")
        self.Bd_kstar_Mu_Reversepidpi = self.__B0_Kstar__(
            daughters=[self.FakePionKstar_for_B2Kstar, self.Dimuon], pid_selection="Mu_ReversePIDpi_")
        self.Bd_kstar_Mu_Reversepidmu = self.__B0_Kstar__(
            daughters=[self.Kstar_for_B2Kstar, self.FakeDimuon], pid_selection="Mu_ReversePIDmu_")
        self.Bd_kstar_E = self.__B0_Kstar__(
            daughters=[self.Kstar_for_B2Kstar, self.Dielectron], pid_selection="E", e_mode=True)
        self.Bd_kstar_E_ReversepidK = self.__B0_Kstar__(
            daughters=[self.FakeKaonKstar_for_B2Kstar, self.Dielectron], pid_selection="E_ReversePIDK_", e_mode=True)
        self.Bd_kstar_E_Reversepidpi = self.__B0_Kstar__(
            daughters=[self.FakePionKstar_for_B2Kstar, self.Dielectron], pid_selection="E_ReversePIDpi_", e_mode=True)
        self.Bd_kstar_E_Reversepide = self.__B0_Kstar__(
            daughters=[self.Kstar_for_B2Kstar, self.FakeDielectron], pid_selection="E_ReversePIDe_", e_mode=True)
        self.Bd_kstar_MuE = self.__B0_Kstar__(
            daughters=[self.Kstar_for_B2Kstar, self.DiMuE], pid_selection="MuE", e_mode=True)
        self.Bd_kstar_MuE_ReversepidK = self.__B0_Kstar__(
            daughters=[self.FakeKaonKstar_for_B2Kstar, self.DiMuE], pid_selection="MuE_ReversePIDK_", e_mode=True)
        self.Bd_kstar_MuE_Reversepidpi = self.__B0_Kstar__(
            daughters=[self.FakePionKstar_for_B2Kstar, self.DiMuE], pid_selection="MuE_ReversePIDpi_", e_mode=True)
        self.Bd_kstar_MuE_Reversepide = self.__B0_Kstar__(daughters=[
                                                     self.Kstar_for_B2Kstar, self.DiMuE_fakeelectron], pid_selection="MuE_ReversePIDe_", e_mode=True)
        self.Bd_kstar_MuE_Reversepidmu = self.__B0_Kstar__(
            daughters=[self.Kstar_for_B2Kstar, self.DiMuE_fakemuon], pid_selection="MuE_ReversePIDmu_", e_mode=True)

        # p K algorithms
        self.LambdaB_pk_Mu = self.__LambdaB_pK__(
            daughters=[self.Dimuon, self.LambdaStar], pid_selection="Mu")
        self.LambdaB_pk_Mu_ReversePIDK = self.__LambdaB_pK__(
            daughters=[self.Dimuon, self.FakeKaonLambdaStar], pid_selection="Mu_ReversePIDK_")
        self.LambdaB_pk_Mu_ReversePIDp = self.__LambdaB_pK__(
            daughters=[self.Dimuon, self.FakeProtonLambdaStar], pid_selection="Mu_ReversePIDp_")
        self.LambdaB_pk_Mu_ReversePIDmu = self.__LambdaB_pK__(
            daughters=[self.FakeDimuon, self.LambdaStar], pid_selection="Mu_ReversePIDmu_")
        self.LambdaB_pk_E = self.__LambdaB_pK__(
            daughters=[self.Dielectron, self.LambdaStar], pid_selection="E", e_mode=True)
        self.LambdaB_pk_E_ReversePIDK = self.__LambdaB_pK__(
            daughters=[self.Dielectron, self.FakeKaonLambdaStar], pid_selection="E_ReversePIDK_", e_mode=True)
        self.LambdaB_pk_E_ReversePIDp = self.__LambdaB_pK__(
            daughters=[self.Dielectron, self.FakeProtonLambdaStar], pid_selection="E_ReversePIDp_", e_mode=True)
        self.LambdaB_pk_E_ReversePIDe = self.__LambdaB_pK__(
            daughters=[self.FakeDielectron, self.LambdaStar], pid_selection="E_ReversePIDe_", e_mode=True)
        self.LambdaB_pk_MuE = self.__LambdaB_pK__(
            daughters=[self.DiMuE, self.LambdaStar], pid_selection="MuE", e_mode=True)
        self.LambdaB_pk_MuE_ReversePIDK = self.__LambdaB_pK__(
            daughters=[self.DiMuE, self.FakeKaonLambdaStar], pid_selection="MuE_ReversePIDK_", e_mode=True)
        self.LambdaB_pk_MuE_ReversePIDp = self.__LambdaB_pK__(
            daughters=[self.DiMuE, self.FakeProtonLambdaStar], pid_selection="MuE_ReversePIDp_", e_mode=True)
        self.LambdaB_pk_MuE_ReversePIDe = self.__LambdaB_pK__(
            daughters=[self.DiMuE_fakeelectron, self.LambdaStar], pid_selection="MuE_ReversePIDe_", e_mode=True)
        self.LambdaB_pk_MuE_ReversePIDmu = self.__LambdaB_pK__(
            daughters=[self.DiMuE_fakemuon, self.LambdaStar], pid_selection="MuE_ReversePIDmu_", e_mode=True)

        self.FilterSPD = {'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                          'Preambulo': ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]}
        # K K lines
        self.Bs2phi_Mu_line = StrippingLine(
            self.name + "_Bs2Phi_Mu_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_Mu], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phi_Mu_line)

        self.Bs2Phi_Mu_ReversePIDmu_line = StrippingLine(
            self.name + "_Bs2Phi_Mu_ReversePIDMu_line", prescale=0.06,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_Mu_ReversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_Mu_ReversePIDmu_line)

        self.Bs2Phi_Mu_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2Phi_Mu_ReversePIDK_line", prescale=0.13,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_Mu_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_Mu_ReversePIDK_line)

        self.Bs2phi_E_line = StrippingLine(
            self.name + "_Bs2Phi_E_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_E], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phi_E_line)

        self.Bs2Phi_E_ReversePIDe_line = StrippingLine(
            self.name + "_Bs2Phi_E_ReversePIDE_line", prescale=0.067,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_E_ReversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_E_ReversePIDe_line)

        self.Bs2Phi_E_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2Phi_E_ReversePIDK_line", prescale=0.16,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_E_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_E_ReversePIDK_line)

        self.Bs2phi_MuE_line = StrippingLine(
            self.name + "_Bs2Phi_MuE_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_MuE], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phi_MuE_line)

        self.Bs2Phi_MuE_ReversePIDe_line = StrippingLine(
            self.name + "_Bs2Phi_MuE_ReversePIDE_line", prescale=0.09,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_MuE_ReversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_MuE_ReversePIDe_line)
        self.Bs2Phi_MuE_ReversePIDmu_line = StrippingLine(
            self.name + "_Bs2Phi_MuE_ReversePIDMu_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_MuE_ReversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_MuE_ReversePIDmu_line)

        self.Bs2Phi_MuE_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2Phi_MuE_ReversePIDK_line", prescale=0.15,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_MuE_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_MuE_ReversePIDK_line)

        # Kst Kst lines
        self.Bs2KstarKstar_Mu_line = StrippingLine(
            self.name + "_Bs2KstarKstar_Mu_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_Mu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_Mu_line)

        self.Bs2KstarKstar_Mu_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2KstarKstar_Mu_ReversePIDK_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_Mu_ReversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_Mu_ReversePIDK_line)

        self.Bs2KstarKstar_Mu_ReversePIDpi_line = StrippingLine(
            self.name + "_Bs2KstarKstar_Mu_ReversePIDPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_Mu_Reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_Mu_ReversePIDpi_line)

        self.Bs2KstarKstar_Mu_ReversePIDmu_line = StrippingLine(
            self.name + "_Bs2KstarKstar_Mu_ReversePIDMu_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_Mu_Reversepidmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_Mu_ReversePIDmu_line)

        self.Bs2KstarKstar_E_line = StrippingLine(
            self.name + "_Bs2KstarKstar_E_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_E], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_E_line)

        self.Bs2KstarKstar_E_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2KstarKstar_E_ReversePIDK_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_E_ReversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_E_ReversePIDK_line)

        self.Bs2KstarKstar_E_ReversePIDpi_line = StrippingLine(
            self.name + "_Bs2KstarKstar_E_ReversePIDPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_E_Reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_E_ReversePIDpi_line)

        self.Bs2KstarKstar_E_ReversePIDe_line = StrippingLine(
            self.name + "_Bs2KstarKstar_E_ReversePIDE_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_E_Reversepide], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_E_ReversePIDe_line)

        self.Bs2KstarKstar_MuE_line = StrippingLine(
            self.name + "_Bs2KstarKstar_MuE_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_MuE], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_MuE_line)

        self.Bs2KstarKstar_MuE_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2KstarKstar_MuE_ReversePIDK_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_MuE_ReversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_MuE_ReversePIDK_line)

        self.Bs2KstarKstar_MuE_ReversePIDpi_line = StrippingLine(
            self.name + "_Bs2KstarKstar_MuE_ReversePIDPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_MuE_Reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_MuE_ReversePIDpi_line)

        self.Bs2KstarKstar_MuE_ReversePIDe_line = StrippingLine(
            self.name + "_Bs2KstarKstar_MuE_ReversePIDE_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_MuE_Reversepide], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_MuE_ReversePIDe_line)
        self.Bs2KstarKstar_MuE_ReversePIDmu_line = StrippingLine(
            self.name + "_Bs2KstarKstar_MuE_ReversePIDMu_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_MuE_Reversepidmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_MuE_ReversePIDmu_line)

        # K pi lines
        self.Bd2Kstar_Mu_line = StrippingLine(
            self.name + "_B02Kstar_Mu_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_Mu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_Mu_line)

        self.Bd2Kstar_Mu_ReversePIDK_line = StrippingLine(
            self.name + "_B02Kstar_Mu_ReversePIDK_line", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_Mu_ReversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_Mu_ReversePIDK_line)

        self.Bd2Kstar_Mu_ReversePIDpi_line = StrippingLine(
            self.name + "_B02Kstar_Mu_ReversePIDPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_Mu_Reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_Mu_ReversePIDpi_line)

        self.Bd2Kstar_Mu_ReversePIDmu_line = StrippingLine(
            self.name + "_B02Kstar_Mu_ReversePIDMu_line", prescale=0.03,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_Mu_Reversepidmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_Mu_ReversePIDmu_line)

        self.Bd2Kstar_E_line = StrippingLine(
            self.name + "_B02Kstar_E_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_E], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_E_line)

        self.Bd2Kstar_E_ReversePIDK_line = StrippingLine(
            self.name + "_B02Kstar_E_ReversePIDK_line", prescale=0.3,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_E_ReversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_E_ReversePIDK_line)

        self.Bd2Kstar_E_ReversePIDpi_line = StrippingLine(
            self.name + "_B02Kstar_E_ReversePIDPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_E_Reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_E_ReversePIDpi_line)

        self.Bd2Kstar_E_ReversePIDe_line = StrippingLine(
            self.name + "_B02Kstar_E_ReversePIDE_line", prescale=0.04,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_E_Reversepide], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_E_ReversePIDe_line)

        self.Bd2Kstar_MuE_line = StrippingLine(
            self.name + "_B02Kstar_MuE_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_MuE], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_MuE_line)

        self.Bd2Kstar_MuE_ReversePIDK_line = StrippingLine(
            self.name + "_B02Kstar_MuE_ReversePIDK_line", prescale=0.25,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_MuE_ReversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_MuE_ReversePIDK_line)

        self.Bd2Kstar_MuE_ReversePIDpi_line = StrippingLine(
            self.name + "_B02Kstar_MuE_ReversePIDPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_MuE_Reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_MuE_ReversePIDpi_line)

        self.Bd2Kstar_MuE_ReversePIDe_line = StrippingLine(
            self.name + "_B02Kstar_MuE_ReversePIDE_line", prescale=0.08,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_MuE_Reversepide], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_MuE_ReversePIDe_line)

        self.Bd2Kstar_MuE_ReversePIDmu_line = StrippingLine(
            self.name + "_B02Kstar_MuE_ReversePIDMu_line", prescale=0.09,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_MuE_Reversepidmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_MuE_ReversePIDmu_line)

        # p K lines
        self.Lb2pK_Mu_line = StrippingLine(
            self.name + "_Lb2pK_Mu_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_Mu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_Mu_line)

        self.Lb2pK_Mu_ReversePIDK_line = StrippingLine(
            self.name + "_Lb2pK_Mu_ReversePIDK_line", prescale=0.26,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_Mu_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_Mu_ReversePIDK_line)

        self.Lb2pK_Mu_ReversePIDp_line = StrippingLine(
            self.name + "_Lb2pK_Mu_ReversePIDp_line", prescale=0.18,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_Mu_ReversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_Mu_ReversePIDp_line)

        self.Lb2pK_Mu_ReversePIDmu_line = StrippingLine(
            self.name + "_Lb2pK_Mu_ReversePIDmu_line", prescale=0.03,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_Mu_ReversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_Mu_ReversePIDmu_line)

        self.Lb2pK_E_line = StrippingLine(
            self.name + "_Lb2pK_E_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_E], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_E_line)

        self.Lb2pK_E_ReversePIDK_line = StrippingLine(
            self.name + "_Lb2pK_E_ReversePIDK_line", prescale=0.32,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_E_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_E_ReversePIDK_line)

        self.Lb2pK_E_ReversePIDp_line = StrippingLine(
            self.name + "_Lb2pK_E_ReversePIDp_line", prescale=0.25,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_E_ReversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_E_ReversePIDp_line)

        self.Lb2pK_E_ReversePIDe_line = StrippingLine(
            self.name + "_Lb2pK_E_ReversePIDe_line", prescale=0.04,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_E_ReversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_E_ReversePIDe_line)

        self.Lb2pK_MuE_line = StrippingLine(
            self.name + "_Lb2pK_MuE_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_MuE], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_MuE_line)

        self.Lb2pK_MuE_ReversePIDK_line = StrippingLine(
            self.name + "_Lb2pK_MuE_ReversePIDK_line", prescale=0.4,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_MuE_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_MuE_ReversePIDK_line)

        self.Lb2pK_MuE_ReversePIDp_line = StrippingLine(
            self.name + "_Lb2pK_MuE_ReversePIDp_line", prescale=0.3,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_MuE_ReversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_MuE_ReversePIDp_line)

        self.Lb2pK_MuE_ReversePIDe_line = StrippingLine(
            self.name + "_Lb2pK_MuE_ReversePIDe_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_MuE_ReversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_MuE_ReversePIDe_line)

        self.Lb2pK_MuE_ReversePIDmu_line = StrippingLine(
            self.name + "_Lb2pK_MuE_ReversePIDmu_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_MuE_ReversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_MuE_ReversePIDmu_line)

    def __Muons__(self, conf):
        from StandardParticles import StdAllLooseMuons, StdAllNoPIDsMuons
        muons = StdAllNoPIDsMuons if conf['UseNoPIDsLeptons'] else StdAllLooseMuons
        muon_cut = self.MuonCutBase if conf['UseNoPIDsLeptons'] else self.MuonCut
        _filter = FilterDesktop(Code=muon_cut)
        _sel = Selection("Selection_" + self.name + "_StdLooseMuons", RequiredSelections=[muons],
                         Algorithm=_filter)
        return _sel

    def __FakeMuons__(self):
        from StandardParticles import StdAllNoPIDsMuons
        _filter = FilterDesktop(Code=self.MuonCutReversePID)
        _sel = Selection("Selection_" + self._name + "StdAllNoPIDsMuons", Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsMuons])
        return _sel

    def __Electrons__(self, conf):
        from StandardParticles import StdAllLooseElectrons, StdAllNoPIDsElectrons
        electrons = StdAllNoPIDsElectrons if conf['UseNoPIDsLeptons'] else StdAllLooseElectrons
        electron_cut = self.ElectronCutBase if conf['UseNoPIDsLeptons'] else self.ElectronCut
        _filter = FilterDesktop(Code=electron_cut)
        _sel = Selection("Selection_" + self.name + "_StdAllLooseElectrons", RequiredSelections=[electrons],
                         Algorithm=_filter)
        return _sel

    def __FakeElectrons__(self):
        from StandardParticles import StdAllNoPIDsElectrons
        _filter = FilterDesktop(Code=self.ElectronCutReversePID)
        _sel = Selection("Selection_" + self._name + "StdAllNoPIDsElectrons", Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsElectrons])
        return _sel

    def __Protons__(self, conf):
        from StandardParticles import StdLooseProtons, StdNoPIDsProtons
        protons = StdNoPIDsProtons if conf['UseNoPIDsHadrons'] else StdLooseProtons
        proton_cuts = self.ProtonCutBase if conf['UseNoPIDsHadrons'] else self.ProtonCut
        _filter = FilterDesktop(Code=proton_cuts)
        _sel = Selection("Selection_" + self.name + "_StdLooseProtons",
                         RequiredSelections=[protons], Algorithm=_filter)
        return _sel

    def __FakeProtons__(self):
        from StandardParticles import StdNoPIDsProtons
        _filter = FilterDesktop(Code=self.ProtonCutReversePID)
        _sel = Selection("Selection_" + self._name + "_StdAllNoPIDsProtons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsProtons])
        return _sel

    def __Kaons__(self, conf, sel_name="_"):
        from StandardParticles import StdLooseKaons, StdNoPIDsKaons
        kaons = StdNoPIDsKaons if conf['UseNoPIDsHadrons'] else StdLooseKaons
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.KaonCutBase)
        elif "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCut_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCut)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLooseKaons", RequiredSelections=[kaons],
                         Algorithm=_filter)
        return _sel

    def __FakeKaons__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsKaons
        if "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCutReversePID_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCutReversePID)
        _sel = Selection("Selection_" + self._name + sel_name + "StdAllNoPIDsKaons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsKaons])
        return _sel

    def __Pions__(self, conf, sel_name="_"):
        from StandardParticles import StdLoosePions, StdNoPIDsPions
        pions = StdNoPIDsPions if conf['UseNoPIDsHadrons'] else StdLoosePions
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.PionCutBase)
        elif "Kstar_E" in sel_name:
            _filter = FilterDesktop(Code=self.PionCut_B2Kstar_E)
        else:
            _filter = FilterDesktop(Code=self.PionCut_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLoosePions", RequiredSelections=[pions],
                         Algorithm=_filter)
        return _sel

    def __FakePions__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsPions
        _filter = FilterDesktop(Code=self.PionCutReversePID_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdAllNoPIDsPions", RequiredSelections=[StdNoPIDsPions],
                         Algorithm=_filter)
        return _sel

    def __Dimuon__(self, Muons):
        CombineDiMuon = CombineParticles()
        CombineDiMuon.DecayDescriptors = [
            "D0 -> mu- mu+", "D0 -> mu+ mu+", "D0 -> mu- mu-"]
        CombineDiMuon.CombinationCut = self.DiMuonCombCut
        CombineDiMuon.MotherCut = self.DiMuonCut
        _sel = Selection("Selection_" + self.name + "_DiMuon",
                         Algorithm=CombineDiMuon, RequiredSelections=[Muons])
        return _sel

    def __FakeDimuon__(self, muons, fakemuons, conf):
        CombineDiMuon = CombineParticles()
        CombineDiMuon.DecayDescriptors = [
            "D0 -> mu- mu+", "D0 -> mu+ mu+", "D0 -> mu- mu-"]
        CombineDiMuon.CombinationCut = self.DiMuonCombCut + \
            " & (AHASCHILD((PIDmu < %(MuonPID)s)))" % conf
        CombineDiMuon.MotherCut = self.DiMuonCut
        _sel = Selection("Selection_" + self.name + "_DiMuonReversePID", Algorithm=CombineDiMuon,
                         RequiredSelections=[muons, fakemuons])
        return _sel

    def __Dielectron__(self, Electrons):
        CombineDiElectron = CombineParticles()
        CombineDiElectron.DecayDescriptors = [
            "D0 -> e- e+", "D0 -> e+ e+", "D0 -> e- e-"]
        CombineDiElectron.CombinationCut = self.DiElectronCombCut
        CombineDiElectron.MotherCut = self.DiElectronCut
        _sel = Selection("Selection_" + self.name + "_DiElectron",
                         Algorithm=CombineDiElectron, RequiredSelections=[Electrons])
        return _sel

    def __FakeDielectron__(self, electrons, fakeelectrons, conf):
        CombineDiElectron = CombineParticles()
        CombineDiElectron.DecayDescriptors = [
            "D0 -> e- e+", "D0 -> e+ e+", "D0 -> e- e-"]
        CombineDiElectron.CombinationCut = self.DiElectronCombCut + \
            " & (AHASCHILD((PIDe < %(ElectronPID)s)))" % conf
        CombineDiElectron.MotherCut = self.DiElectronCut
        _sel = Selection("Selection_" + self.name + "_DiElectronReversePID", Algorithm=CombineDiElectron,
                         RequiredSelections=[electrons, fakeelectrons])
        return _sel

    def __DiMuE__(self, Muons, Electrons, pid_selection):
        CombineDiMuE = CombineParticles()
        CombineDiMuE.DecayDescriptors = [
            "D0 -> mu- e+", "D0 -> mu+ e-", "D0 -> mu+ e+", "D0 -> mu- e-"]
        CombineDiMuE.CombinationCut = self.DiMuonCombCut
        CombineDiMuE.MotherCut = self.DiMuECut
        _sel = Selection("Selection_" + self.name + "_DiMuE" + pid_selection,
                         Algorithm=CombineDiMuE, RequiredSelections=[Muons, Electrons])
        return _sel

    def __Phi__(self, Kaons, fakekaon=None, conf=None):
        _phi2kk = CombineParticles()
        _phi2kk.DecayDescriptors = [
            "phi(1020) -> K+ K-", "phi(1020) -> K+ K+", "phi(1020) -> K- K-"]
        _phi2kk.MotherCut = self.PhiCut
        if fakekaon is None:
            _phi2kk.CombinationCut = self.PhiCombCut
            _sel = Selection("Phi_selection_for" + self.name,
                             Algorithm=_phi2kk, RequiredSelections=[Kaons])
        else:
            _phi2kk.CombinationCut = self.PhiCombCut + \
                " & (AHASCHILD((PIDK < %(KaonPID)s)))" % conf
            _sel = Selection("Phi_ReversePIDK_selection_for" + self.name, Algorithm=_phi2kk,
                             RequiredSelections=[Kaons, fakekaon])
        return _sel

    def __Kstar__(self, Kaons, Pions, pid_selection="_", sel_name="_B2KstarKstar"):

        _kstar2kpi = CombineParticles()
        if sel_name == "_B2KstarKstar":
            _kstar2kpi.DecayDescriptors = ["[K*(892)0 -> K+ pi-]cc"]
            _kstar2kpi.CombinationCut = self.Kstar_for_B2KstarKstarCombCut
            _kstar2kpi.MotherCut = self.Kstar_for_B2KstarKstarCut
        else:
            _kstar2kpi.DecayDescriptors = [
                "[K*(892)0 -> K+ pi-]cc", "K*(892)0 -> K+ pi+", "K*(892)0 -> K- pi-"]
            _kstar2kpi.CombinationCut = self.Kstar_for_B2KstarCombCut
            _kstar2kpi.MotherCut = self.Kstar_for_B2KstarCut
        _sel = Selection("Kstar_for" + sel_name + pid_selection + "selection_for" + self.name, Algorithm=_kstar2kpi,
                         RequiredSelections=[Kaons, Pions])
        return _sel

    def __Lambdastar__(self, Protons, Kaons, pid_selection="_"):
        _lstar2pk = CombineParticles()
        _lstar2pk.DecayDescriptors = [
            "[Lambda(1520)0 -> p+ K-]cc", "Lambda(1520)0 -> p+ K+", "Lambda(1520)0 -> p~- K-"]
        _lstar2pk.CombinationCut = self.LambdaStarCombCut
        _lstar2pk.MotherCut = self.LambdaStarCut
        _sel = Selection("Lambdastar" + pid_selection + "selection_for" + self.name, Algorithm=_lstar2pk,
                         RequiredSelections=[Protons, Kaons])
        return _sel

    def __Bs_Phi__(self, daughters, pid_selection="_", e_mode=False):
        comb_cut = self.BsCombCut if not e_mode else self.BsCombCut_E
        _b2phitautau = CombineParticles(DecayDescriptors=["B_s0 -> phi(1020) D0"],
                                        MotherCut=self.BsCut, CombinationCut=comb_cut)
        sel = Selection("Phi" + pid_selection + "for" + self.name,
                        Algorithm=_b2phitautau, RequiredSelections=daughters)
        return sel

    def __Bs_KstarKstar__(self, daughters, conf, pid_selection="_"):
        _b2kstarkstartautau = DaVinci__N3BodyDecays()
        _b2kstarkstartautau.DecayDescriptors = [
            "B_s0 -> K*(892)0 K*(892)~0 D0", "B_s0 -> K*(892)0 K*(892)0 D0", "B_s0 -> K*(892)~0 K*(892)~0 D0"]
        _b2kstarkstartautau.CombinationCut = self.Bs_for_B2KstarKstarCombCut
        _b2kstarkstartautau.MotherCut = self.BsCut
        _b2kstarkstartautau.Combination12Cut = "(ACHI2DOCA(1,2) < %(KstarKstar_DOCACHI2)s) & (AM < %(KstarKstar_Comb_MassHigh)s * MeV)" % conf
        sel = Selection("KstarKstar" + pid_selection + "for" + self.name + "_daughters", Algorithm=_b2kstarkstartautau,
                        RequiredSelections=daughters)
        return sel

    def __B0_Kstar__(self, daughters, pid_selection="_", e_mode=False):
        comb_cut = self.BdCombCut if not e_mode else self.BdCombCut_E
        mother_cut = self.BdCut if not e_mode else self.BdCut_E
        _b2kstartautau = CombineParticles(DecayDescriptors=["B0 -> K*(892)0 D0", "B~0 -> K*(892)~0 D0"],
                                          MotherCut=mother_cut, CombinationCut=comb_cut)
        sel = Selection("Kstar" + pid_selection + "for" + self.name, Algorithm=_b2kstartautau,
                        RequiredSelections=daughters)
        return sel

    def __LambdaB_pK__(self, daughters, pid_selection="_", e_mode=False):
        comb_cut = self.LambdaBCombCut if not e_mode else self.LambdaBCombCut_E
        _b2lambdatautau = CombineParticles(DecayDescriptors=["Lambda_b0 -> Lambda(1520)0 D0", "Lambda_b~0 -> Lambda(1520)~0 D0"],
                                           MotherCut=self.LambdaBCut, CombinationCut=comb_cut)
        sel = Selection("Lambda" + pid_selection + "for" + self.name, Algorithm=_b2lambdatautau,
                        RequiredSelections=daughters)
        return sel
