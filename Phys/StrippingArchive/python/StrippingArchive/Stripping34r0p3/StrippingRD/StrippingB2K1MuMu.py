###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""
Stripping options for "B0 -> ( Jpsi -> mu+ mu- ) ( K1(1270) -> ( K*(892) -> K+ pi- ) pi0 )CC" - reconstructed as Bd -> Jpsi K* for efficiency reasons
Also contains the SS muons line: "B0 -> ( Jpsi -> mu+ mu+ ) ( K1(1270) -> ( K*(892) -> K+ pi- ) pi0 )CC"

Author: James Brown
"""

########################################################################
__author__ = ['James Brown']
__date__ = '23/06/2023'
__version__ = '$ 1.1 $'

__all__ = ('Bd2K1MuMuLine',
           'makeBdK1Jpsi',
           'makeJpsis',
           'makeKstars',
           'default_config')

from Gaudi.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from StandardParticles import StdLooseMuons, StdAllLooseKaons, StdAllLoosePions

from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder, checkConfig


daughter_locations =    {
                        '[B0 -> (J/psi(1S) -> mu+ mu-) (K*(892)0 -> K+ ^pi-)]CC': '{0}_pim',
                        '[B0 -> (J/psi(1S) -> mu+ mu-) (K*(892)0 -> ^K+ pi-)]CC': '{0}_Kp',
                        '[B0 -> (J/psi(1S) -> mu+ ^mu-) (K*(892)0 -> K+ pi-)]CC': '{0}_mum',
                        '[B0 -> (J/psi(1S) -> ^mu+ mu-) (K*(892)0 -> K+ pi-)]CC': '{0}_mup',
                        '[B0 -> ^(J/psi(1S) -> mu+ mu-) (K*(892)0 -> K+ pi-)]CC': '{0}_Jpsi',
                        '[B0 -> (J/psi(1S) -> mu+ mu-) ^(K*(892)0 -> K+ pi-)]CC': '{0}_Kst',
                        }

default_config = {
    'NAME': 'Bd2K1MuMu',
    'WGs': ['RD'],
    'BUILDERTYPE': 'Bd2K1MuMuLine',
    'CONFIG': {'PrescaleBd2K1MuMuLine': 1,
               'PrescaleBd2K1MuMuSSLine': 1,
               # B0
               'BMassWindow': 2500,
               'BDIRA': 0.9995,
               'BIPCHI2': 100,
               'BVertexCHI2': 9,
               'BVDCHI2': 100,
               # Intermediates - Jpsi/Kst
               "InterMIPCHI2DV": 0,
               "InterVertexCHI2": 9,
               "JpsiUpperM": 9000,
               "JpsiLowerM": 1000,
               "KstUpperM": 1100,
               "KstLowerM": 700,
               "Kst_PT": 800,
               # Final states
               "fsPT": 300,
               "fsMIPCHI2DV": 4,
               "fsTRCHI2": 4,
               "fsTRGHOSTPROB": 0.4,
               "fsPROBNN": 0.4,
               "muPROBpi": 0.95,
               "fsM": 2600,

               "RelatedInfoTools": [
                {'Type'             : 'RelInfoVertexIsolationBDT',
                'Location'         : 'VertexIsoBDTInfo',
                'IgnoreUnmatchedDescriptors': True,
                'DaughterLocations'   : {key: val.format('VertexIsoBDTInfo') for key, val in daughter_locations.items()}
                },
                {'Type'             : 'RelInfoTrackIsolationBDT',
                'IgnoreUnmatchedDescriptors': True,
                'DaughterLocations'   : {key: val.format('TrackIsoBDTInfo') for key, val in daughter_locations.items()}
                },

                {'Type'             : 'RelInfoVertexIsolation',
                'Location'         : 'VertexIsoInfo',
                'IgnoreUnmatchedDescriptors': True,
                'DaughterLocations'   : {key: val.format('VertexIsoInfo') for key, val in daughter_locations.items()}
                },

                {'Type'             : 'RelInfoConeVariables',
                'ConeAngle'        : 0.5,
                'IgnoreUnmatchedDescriptors': True,
                'Location'         : 'ConeVarsInfo05',
                'DaughterLocations'   : {key: val.format('ConeVarsInfo05') for key, val in daughter_locations.items()}
                },
                {'Type'              : 'RelInfoConeVariables',
                'ConeAngle'         : 1.0,
                'Location'          : 'ConeVarsInfo10'
                },
                {'Type'              : 'RelInfoConeVariables',
                'ConeAngle'         : 1.5,
                'Location'          : 'ConeVarsInfo15'
                },
                {'Type'              : 'RelInfoConeVariables',
                'ConeAngle'         : 2.0,
                'Location'          : 'ConeVarsInfo20'
                },

                {'Type'              : 'RelInfoConeIsolation',
                'ConeSize'          : 0.5,
                'IgnoreUnmatchedDescriptors': True,
                'Location'         : 'ConeIsoInfo05',
                'DaughterLocations' : {key: val.format('ConeIsoInfo05') for key, val in daughter_locations.items()}
                },
                {'Type'              : 'RelInfoConeIsolation',
                'ConeSize'         : 1.0,
                'Location'          : 'ConeIsoInfo10'
                },
                {'Type'              : 'RelInfoConeIsolation',
                'ConeSize'         : 1.5,
                'Location'          : 'ConeIsoInfo15'
                        },
                {'Type'              : 'RelInfoConeIsolation',
                'ConeSize'         : 2.0,
                'Location'          : 'ConeIsoInfo20'
                },

                ],
               },
    'STREAMS': ['Leptonic']
}


class Bd2K1MuMuLine(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        Bd2K1MuMuName = name

        # Default SPD hits filter
        SPDFilter = {
            'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 600 )",
            'Preambulo': ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]
        }

        # Make parents
        # Mu+Mu-/Mu-Mu+
        self.Jpsi = makeJpsis("JpsisForBd2K1MuMu",
                              config['InterMIPCHI2DV'], config['InterVertexCHI2'], config['JpsiLowerM'], config['JpsiUpperM'],
                              config['fsPT'], config['fsMIPCHI2DV'], config['fsTRCHI2'], config[
                                  'fsTRGHOSTPROB'], config['fsPROBNN'], config['muPROBpi'], config['fsM'],
                              sameSign=False)

        # Mu+Mu+/Mu-Mu- (SS)
        self.JpsiSS = makeJpsis("SSJpsisForBd2K1MuMu",
                                config['InterMIPCHI2DV'], config['InterVertexCHI2'], config['JpsiLowerM'], config['JpsiUpperM'],
                                config['fsPT'], config['fsMIPCHI2DV'], config['fsTRCHI2'], config[
                                    'fsTRGHOSTPROB'], config['fsPROBNN'], config['muPROBpi'], config['fsM'],
                                sameSign=True)
        # Kstar candidates
        self.Kst = makeKstars("KstForBd2K1MuMu",
                              config['InterMIPCHI2DV'], config['InterVertexCHI2'], config['KstLowerM'], config['KstUpperM'], config['Kst_PT'],
                              config['fsPT'], config['fsMIPCHI2DV'], config['fsTRCHI2'], config['fsTRGHOSTPROB'], config['fsPROBNN'], config['fsM'])

        # Make B0
        self.BdK1Jpsi = makeBdK1Jpsi(name, self.Jpsi, self.Kst,
                                     config['BMassWindow'], config['BVertexCHI2'], config['BIPCHI2'], config['BDIRA'], config['BVDCHI2'])

        # Make B0 with SS
        self.BdK1JpsiSS = makeBdK1Jpsi(name+'SS', self.JpsiSS, self.Kst,
                                       config['BMassWindow'], config['BVertexCHI2'], config['BIPCHI2'], config['BDIRA'], config['BVDCHI2'])

        self.lineBd2K1MuMu = StrippingLine(Bd2K1MuMuName+"Line",
                                           prescale= config['PrescaleBd2K1MuMuLine'],
                                           RelatedInfoTools = config['RelatedInfoTools'],
                                           selection=self.BdK1Jpsi,
                                           MDSTFlag=False,
                                           FILTER=SPDFilter,
                                           EnableFlavourTagging=False,
                                           RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"])

        self.lineBd2K1MuMuSS = StrippingLine(Bd2K1MuMuName+"SSLine",
                                             prescale= config['PrescaleBd2K1MuMuSSLine'],
                                             RelatedInfoTools = config['RelatedInfoTools'],
                                             selection=self.BdK1JpsiSS,
                                             MDSTFlag=False,
                                             FILTER=SPDFilter,
                                             EnableFlavourTagging=False,
                                             RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"])

        self.registerLine(self.lineBd2K1MuMu)
        self.registerLine(self.lineBd2K1MuMuSS)

# FUNCTIONS


def makeBdK1Jpsi(name, Jpsi, K1,
                 BMassWindow, BVertexCHI2, BIPCHI2, BDIRA, BVDCHI2):

    _combination_cuts = "( ADAMASS('B0') < %(BMassWindow)s * MeV )" % locals()

    _mother_cuts = "( (VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s ) & (BPVIPCHI2() < %(BIPCHI2)s ) & (BPVDIRA > %(BDIRA)s ) & (BPVVDCHI2 > %(BVDCHI2)s ) )" % locals()

    CombineBdK1Jpsi = CombineParticles(DecayDescriptor="[B0 -> J/psi(1S) K*(892)0]cc",
                                       CombinationCut=_combination_cuts,
                                       MotherCut=_mother_cuts)

    return Selection(name,
                     Algorithm=CombineBdK1Jpsi,
                     RequiredSelections=[Jpsi, K1])


def makeJpsis(name,
              InterMIPCHI2DV, InterVertexCHI2, JpsiLowerM, JpsiUpperM,
              fsPT, fsMIPCHI2DV, fsTRCHI2, fsTRGHOSTPROB, fsPROBNN, muPROBpi, fsM,
              sameSign):

    _daughters_cuts = '(HASMUON) & (ISMUON) & ( PT > %(fsPT)s ) & ( MIPCHI2DV(PRIMARY) > %(fsMIPCHI2DV)s ) & ( TRGHOSTPROB < %(fsTRGHOSTPROB)s ) & ( TRCHI2DOF < %(fsTRCHI2)s ) & ( PROBNNpi < %(muPROBpi)s ) & ( PROBNNmu > %(fsPROBNN)s )' % locals()

    _combination_cuts = "(AM > 100*MeV)"

    _mother_cuts = "(MM > %(JpsiLowerM)s *MeV) & (MM < %(JpsiUpperM)s *MeV) & (VFASPF(VCHI2/VDOF)<%(InterVertexCHI2)s) & (MIPCHI2DV(PRIMARY) > %(InterMIPCHI2DV)s )" % locals()

    # identical cuts, just with the muon sign either the same or opposite
    if sameSign:
        decDesc = "[J/psi(1S) -> mu+ mu+]cc"
    else:
        decDesc = "J/psi(1S) -> mu+ mu-"

    CombineDiMuons = CombineParticles(DecayDescriptor=decDesc,
                                      DaughtersCuts={
                                          "mu+": _daughters_cuts, "mu-": _daughters_cuts},
                                      CombinationCut=_combination_cuts,
                                      MotherCut=_mother_cuts)

    DiMuonConf = CombineDiMuons.configurable("CombineDiMuons")

    return Selection(name,
                     Algorithm=CombineDiMuons,
                     RequiredSelections=[StdLooseMuons])


def makeKstars(name,
               InterMIPCHI2DV, InterVertexCHI2, KstLowerM, KstUpperM, Kst_PT,
               fsPT, fsMIPCHI2DV, fsTRCHI2, fsTRGHOSTPROB, fsPROBNN, fsM):

    _daughters_cuts = "(PT > %(fsPT)s) & (MIPCHI2DV(PRIMARY)>%(fsMIPCHI2DV)s) & (TRGHOSTPROB<%(fsTRGHOSTPROB)s) & (TRCHI2DOF<%(fsTRCHI2)s) & (M<%(fsM)s)" % locals()

    _combination_cuts = "(APT > 500 *MeV) & (ADAMASS('K*(892)0') < 300 *MeV) & (ADOCACHI2CUT(30, ''))"

    _mother_cuts = "(MM > %(KstLowerM)s *MeV) & (MM < %(KstUpperM)s *MeV) & (VFASPF(VCHI2/VDOF)<%(InterVertexCHI2)s) & (MIPCHI2DV(PRIMARY) > %(InterMIPCHI2DV)s ) & (PT>%(Kst_PT)s *MeV)" % locals()

    CombineKst = CombineParticles(DecayDescriptor="[K*(892)0 -> K+ pi-]cc",
                                  DaughtersCuts={"K+": _daughters_cuts + " & (PROBNNk>%(fsPROBNN)s)" % locals(
                                  ), "pi-": _daughters_cuts + " & (PROBNNpi>%(fsPROBNN)s)" % locals()},
                                  CombinationCut=_combination_cuts,
                                  MotherCut=_mother_cuts)

    KstConf = CombineKst.configurable("CombineKst")

    return Selection(name,
                     Algorithm=CombineKst,
                     RequiredSelections=[StdAllLooseKaons, StdAllLoosePions])