###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
from CommonParticles import StdAllNoPIDsPions
from StrippingUtils.Utils import LineBuilder

'''
Stripping lines for ALP -> tautau searches (both displaced and prompt). 
Various decay mode combinations of the tau leptons are considered.
Authors: Xabier Cid Vidal, Carlos Vazquez Sierra.
'''

__all__ = ('ALP2TauTauConf', 'default_config')

default_config =  {
  'NAME'                 : 'ALP2TauTau',
  'BUILDERTYPE'          : 'ALP2TauTauConf',
  'WGs'                  : [ 'QEE' ],
  'CONFIG'               : {
                           'SpdMult'                   : '6000',
                           # Leptons (e, mu) [tighter cuts with respect to arXiv:xxxx.xxxx - no max(e_ISO, mu_ISO) for emu mode]:
                           'TAUE_PROBNN_EMU'           : 0.5,
                           'TAUE_PT'                   : 3500 * MeV,
                           'TAUE_P'                    : 10000 * MeV,
                           'TAUE_IP'                   : 0.03 * mm,
                           'TAUE_PROBNN'               : 0.25,
                           'TAUE_ISO'                  : .99,
                           'TAUMU_PT'                  : 3500 * MeV,
                           'TAUMU_P'                   : 10000 * MeV,
                           'TAUMU_IP'                  : 0.01 * mm,
                           'TAUMU_PROBNN'              : 0.8,
                           'TAUMU_ISO'                 : .99,
                           # Tau leptons and pion daughters:
                           'TAUCOMB_DOCA'              : 0.05*mm,
                           'TAU_MASS_HIGH'             : 1700.*MeV,
                           'TAU_MASS_LOW'              : 0.*MeV,
                           'TAUPI_MINPT'               : 1000 * MeV,
                           'TAUPI_MINP'                : 2000 * MeV,
                           'TAUPI_MINIP'               : 0.01 * mm,
                           'TAUPI_MINPID'              : 0.,
                           'TAU_IPMAX'                 : 0.2* mm,
                           'TAU_MCORR_MAX'             : 2500.*MeV,
                           'TAU_MCORR_MIN'             : 1200.*MeV,
                           'TAU_PT'                    : 10000.*MeV,
                           'TAU_ISO'                   : .15, # this cut kills signal but also helps to reduce background.
                           ## Specific cuts for full hadronic modes:
                           'TAUPI_MINPT_FH'            : 500 * MeV,
                           'TAU_IPMAX_FH'              : 0.1* mm,
                           'TAU_RHO_MIN_FH'            : 0.1* mm,
                           'TAU_RHO_MAX_FH'            : 5.* mm,
                           'TAU_PT_FH'                 : 2500.*MeV,
                           ## ALP combination:
                           'HTAU_IPMAX'                : 0.2*mm,
                           'HTAU_DOCA'                 : 0.4*mm,
                           'HTAU_FDMAX'                : 1*mm,
                           'HTAU_DOCA_FH'              : 0.1*mm,
                           'HTAU_FDMAX_FH'             : 0.25*mm,
                           'HTAU_IPMAX_FH'             : 0.1*mm,
                           'HTAU_MASS_LOW'             : 0.*MeV,
                           'HTAU_MASS_HIGH'            : 1e6*MeV,
                           'HTAU_ETA_MIN'              : 2,
                           'HTAU_ETA_MAX'              : 4.5,
                           'HTAU_PT_MIN'               : 15*GeV,
                           'HTAU_PT_MAX'               : 150*GeV,
                           'HTAU_PT_DAUG_MIN'          : 5.*GeV,
                           'HTAU_PT_DAUG_MAX'          : 7.5*GeV,
                           'HTAU_ETA_DAUG_MIN'         : 1.5,
                           'HTAU_ETA_DAUG_MAX'         : 5,
                           ## Prescales and postscales:
                           'HTau3HTau3H_LinePrescale'  : 1,
                           'HTau3HTau3H_LinePostscale' : 1,
                           'HTau3HTauMu_LinePrescale'  : 1,
                           'HTau3HTauMu_LinePostscale' : 1,
                           'HTau3HTauE_LinePrescale'   : 1,
                           'HTau3HTauE_LinePostscale'  : 1,
                           'HTauMuTauE_LinePrescale'   : 1,
                           'HTauMuTauE_LinePostscale'  : 1
                           },
                           'STREAMS'     : ['BhadronCompleteEvent']
                           }

##########################################################################

class ALP2TauTauConf(LineBuilder) :
  ''' Build all the ALP lines for different combinations of tau lepton decays'''

  __configuration_keys__ = default_config['CONFIG'].keys()  

  def __init__(self, name, config):
    LineBuilder.__init__(self, name, config)

    self.FilterSPD = {'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config ,
                      'Preambulo' : ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]}
    
    # Build the input objects for the lines:

    self.config = config

    self.taus3H    = self._makeMyH3Tau( name+"_Tau3HSel" )
    self.taus3H_FH = self._makeMyH3TauFullHadronic( name+"_Tau3HSel_for3H3H" )
    self.tausMu    = self._makeTau2Lep( name+"_TauMuSel", "muon" )
    self.tausE     = self._makeTau2Lep( name+"_TauESel", "electron" )
    self.tausE_Emu = self._makeTau2Lep( name+"_TauEForEMuSel", "electron_emu")

    self.selH2Tau3HTau3H = self._makeHLepLep( name+"_H2tau3Htau3H", [self.taus3H_FH],  channel = 0 )
    self.selH2Tau3HTauMu = self._makeHLepLep( name+"_H2tau3HtauMu", [self.tausMu, self.taus3H],  channel = 1 )
    self.selH2Tau3HTauE  = self._makeHLepLep( name+"_H2tau3HtauE",  [self.tausE, self.taus3H],  channel = 2 )
    self.selH2TauMuTauE  = self._makeHLepLep( name+"_H2tauMutauE",  [self.tausE_Emu, self.tausMu],  channel = 3 )

    ## Finished making selections build and register lines

    self.HTau3HTau3H_Line  = self._makeLine("HTau3HTau3HLine", self.selH2Tau3HTau3H,
                                            config['HTau3HTau3H_LinePrescale'],
                                            config['HTau3HTau3H_LinePostscale'])

    self.HTau3HTauMu_Line  = self._makeLine("HTau3HTauMuLine", self.selH2Tau3HTauMu,
                                            config['HTau3HTauMu_LinePrescale'],
                                            config['HTau3HTauMu_LinePostscale'])
      
    self.HTau3HTauE_Line  = self._makeLine("HTau3HTauELine", self.selH2Tau3HTauE,
                                           config['HTau3HTauE_LinePrescale'],
                                           config['HTau3HTauE_LinePostscale'])

    self.HTauMuTauE_Line  = self._makeLine("HTauMuTauELine", self.selH2TauMuTauE,
                                           config['HTauMuTauE_LinePrescale'],
                                           config['HTauMuTauE_LinePostscale'])

    ## Displaces lines

    self.taus3H_Dis    = self._makeMyH3Tau( name+"_Tau3HSel_Dis", displaced = True )
    self.taus3H_FH_Dis = self._makeMyH3TauFullHadronic( name+"_Tau3HSel_for3H3H_Dis", displaced = True)

    self.selH2Tau3HTau3H_Dis = self._makeHLepLep( name+"_H2tau3Htau3H_Dis", [self.taus3H_FH_Dis],  channel = 0, displaced = True)
    self.selH2Tau3HTauMu_Dis = self._makeHLepLep( name+"_H2tau3HtauMu_Dis", [self.tausMu, self.taus3H_Dis],  channel = 1, displaced = True)
    self.selH2Tau3HTauE_Dis = self._makeHLepLep( name+"_H2tau3HtauE_Dis",  [self.tausE, self.taus3H_Dis],  channel = 2, displaced = True)
    self.selH2TauMuTauE_Dis  = self._makeHLepLep( name+"_H2tauMutauE_Dis",  [self.tausE_Emu, self.tausMu],  channel = 3, displaced = True)

    self.HTau3HTau3HDis_Line  = self._makeLine("HTau3HTau3HDisplacedLine", self.selH2Tau3HTau3H_Dis,
                                               config['HTau3HTau3H_LinePrescale'],
                                               config['HTau3HTau3H_LinePostscale'])

    self.HTau3HTauMuDis_Line  = self._makeLine("HTau3HTauMuDisplacedLine", self.selH2Tau3HTauMu_Dis,
                                               config['HTau3HTauMu_LinePrescale'],
                                               config['HTau3HTauMu_LinePostscale'])
      
    self.HTau3HTauEDis_Line  = self._makeLine("HTau3HTauEDisplacedLine", self.selH2Tau3HTauE_Dis,
                                              config['HTau3HTauE_LinePrescale'],
                                              config['HTau3HTauE_LinePostscale'])
      
    self.HTauMuTauEDis_Line  = self._makeLine("HTauMuTauEDisplacedLine", self.selH2TauMuTauE_Dis,
                                              config['HTauMuTauE_LinePrescale'],
                                              config['HTauMuTauE_LinePostscale'])

  ##########################################################################

  def _makeTau2Lep(self, name, daugh):
    ''' Filter muons and electrons to build the ALP objects'''

    if daugh=="muon":
      mysel = DataOnDemand("Phys/StdAllNoPIDsMuons/Particles")
      _code = "( PT > %(TAUMU_PT)s )" \
              "& ( P > %(TAUMU_P)s )" \
              "& ( MIPDV(PRIMARY)> %(TAUMU_IP)s )" \
              "& ( PT/(PT+PTCONE) > %(TAUMU_ISO)s )" \
              "& ( PROBNNmu > %(TAUMU_PROBNN)s )" % self.config

    if "electron" in daugh:
      mysel = DataOnDemand("Phys/StdAllNoPIDsElectrons/Particles")
      _code = "( PT > %(TAUE_PT)s )" \
              "& ( P > %(TAUE_P)s )" \
              "& ( MIPDV(PRIMARY)> %(TAUE_IP)s )" \
              "& ( PT/(PT+PTCONE) > %(TAUE_ISO)s )" % self.config

      if daugh=="electron_emu": _code += "& ( PROBNNe > %(TAUE_PROBNN_EMU)s )" % self.config
      else: _code += "& ( PROBNNe > %(TAUE_PROBNN)s )" % self.config
 
    _Filter = FilterDesktop(Code = _code, Preambulo = ["PTCONE  = SUMCONE (0.05, PT, '/Event/Phys/StdAllNoPIDsPions/Particles')"])

    return Selection(name, Algorithm = _Filter, RequiredSelections = [ mysel ] )

  ##########################################################################

  def _makeMyH3Tau(self, name, displaced = False):
    ''' Build tau leptons from 3-prong modes'''

    pions = DataOnDemand("Phys/StdAllNoPIDsPions/Particles")

    combcut   = " (AMAXDOCA('')<%(TAUCOMB_DOCA)s) & (AM<%(TAU_MASS_HIGH)s) & (AM>%(TAU_MASS_LOW)s) "\
                "& (AMINCHILD(PT, 211==ABSID) > %(TAUPI_MINPT)s) & (AMINCHILD(P, 211==ABSID) > %(TAUPI_MINP)s)"\
                "& (AMINCHILD(MIPDV(PRIMARY),211==ABSID) > %(TAUPI_MINIP)s) & (AMINCHILD(PROBNNpi,211==ABSID) > %(TAUPI_MINPID)s)" % self.config

    if displaced: # inverted IPmax cut, looser requirement.
      mothercut = "( MIPDV(PRIMARY) > %(TAU_IPMAX_FH)s) "\
                  "& ( BPVCORRM < %(TAU_MCORR_MAX)s )" \
                  "& ( BPVCORRM > %(TAU_MCORR_MIN)s )" \
                  "& ( PT/(PT+PTCONE) > %(TAU_ISO)s )" \
                  "& (M<%(TAU_MASS_HIGH)s) & (M>%(TAU_MASS_LOW)s) & (PT>%(TAU_PT)s)" % self.config
    else:
      mothercut = "( MIPDV(PRIMARY) < %(TAU_IPMAX)s) "\
                  "& ( BPVCORRM < %(TAU_MCORR_MAX)s )" \
                  "& ( BPVCORRM > %(TAU_MCORR_MIN)s )" \
                  "& ( PT/(PT+PTCONE) > %(TAU_ISO)s )" \
                  "& (M<%(TAU_MASS_HIGH)s) & (M>%(TAU_MASS_LOW)s) & (PT>%(TAU_PT)s)" % self.config

    return SimpleSelection(name, 
                           CombineParticles,
                           [pions],
                           DecayDescriptors = ["[tau+ -> pi+ pi- pi+]cc"],
                           Preambulo        = ["PTCONE  = SUMCONE (10., PT, '/Event/Phys/StdAllNoPIDsPions/Particles')"],
                           CombinationCut   = combcut,
                           MotherCut        = mothercut )

  ##########################################################################

  def _makeMyH3TauFullHadronic(self, name, displaced = False):
    ''' Build tau leptons from 3-prong modes, for ALP modes to both tau with 3-prongs'''

    pions = DataOnDemand("Phys/StdAllNoPIDsPions/Particles")

    combcut   = " (AMAXDOCA('')<%(TAUCOMB_DOCA)s) & (AM<%(TAU_MASS_HIGH)s) & (AM>%(TAU_MASS_LOW)s) "\
                "& (AMINCHILD(PT, 211==ABSID) > %(TAUPI_MINPT_FH)s) & (AMINCHILD(P, 211==ABSID) > %(TAUPI_MINP)s)"\
                "& (AMINCHILD(MIPDV(PRIMARY), 211==ABSID) > %(TAUPI_MINIP)s) & (AMINCHILD(PROBNNpi, 211==ABSID) > %(TAUPI_MINPID)s) " % self.config

    if displaced: # inverted IPmax cut, and removed RHO cuts.
      mothercut = "( MIPDV(PRIMARY) > %(TAU_IPMAX_FH)s) "\
                  "& ( BPVCORRM < %(TAU_MCORR_MAX)s )" \
                  "& ( BPVCORRM > %(TAU_MCORR_MIN)s )" \
                  "& ( PT/(PT+PTCONE) > %(TAU_ISO)s )" \
                  "& (M<%(TAU_MASS_HIGH)s) & (M>%(TAU_MASS_LOW)s) & (PT>%(TAU_PT_FH)s)" % self.config
                  #"& (M<%(TAU_MASS_HIGH)s) & (BPVVDRHO>%(TAU_RHO_MAX_FH)s) & (M>%(TAU_MASS_LOW)s) & (PT>%(TAU_PT_FH)s)" % self.config
    else:
      mothercut = "( MIPDV(PRIMARY) < %(TAU_IPMAX_FH)s) "\
                  "& ( BPVCORRM < %(TAU_MCORR_MAX)s )" \
                  "& ( BPVCORRM > %(TAU_MCORR_MIN)s )" \
                  "& ( PT/(PT+PTCONE) > %(TAU_ISO)s )" \
                  "& (M<%(TAU_MASS_HIGH)s) & (BPVVDRHO>%(TAU_RHO_MIN_FH)s) & (BPVVDRHO<%(TAU_RHO_MAX_FH)s) & (M>%(TAU_MASS_LOW)s) & (PT>%(TAU_PT_FH)s)" % self.config

    return SimpleSelection(name, 
                           CombineParticles, 
                           [pions],
                           DecayDescriptors = ["[tau+ -> pi+ pi- pi+]cc"],
                           Preambulo        = ["PTCONE  = SUMCONE (10., PT, '/Event/Phys/StdAllNoPIDsPions/Particles')"],
                           CombinationCut   = combcut,
                           MotherCut        = mothercut )

  ##########################################################################

  def _makeHLepLep(self, name, inps, channel, displaced = False) :
    ''' Build the full ALP from the different combinations of tau decay modes'''

    preambulo  = ["from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"]

    combcut    = "( (AMAXDOCA('')<%(HTAU_DOCA)s) )"  % self.config
    if displaced: mothercut  = "( ( BPVVD > %(HTAU_FDMAX_FH)s ) & ( MIPDV(PRIMARY) < %(HTAU_IPMAX)s ) )" % self.config # invertex FD, looser requirements.
    else: mothercut  = "( ( BPVVD < %(HTAU_FDMAX)s ) & ( MIPDV(PRIMARY) < %(HTAU_IPMAX)s ) )" % self.config

    if channel==0: # tau3htau3h, replace cuts with FH ones:
      combcut    = "( (AMAXDOCA('')<%(HTAU_DOCA_FH)s) )"  % self.config
      if not displaced: mothercut  = "( ( BPVVD < %(HTAU_FDMAX_FH)s ) & ( MIPDV(PRIMARY) < %(HTAU_IPMAX_FH)s ) )" % self.config
      else: mothercut  = "( ( BPVVD > %(HTAU_FDMAX_FH)s ) & ( MIPDV(PRIMARY) < %(HTAU_IPMAX_FH)s ) )" % self.config
      mydecay   = ["H_30 -> tau+ tau-"]
    elif channel==1: mydecay = ["[H_30 -> tau+ mu-]cc"] # tau3htauMu
    elif channel==2: mydecay = ["[H_30 -> tau+ e-]cc"] # tau3htauE
    elif channel==3: # tauMutauE, add sanity mass cuts:
      mydecay = ["[H_30 -> mu+ e-]cc"]
      combcut += " & ( (AM>%(HTAU_MASS_LOW)s) & (AM<%(HTAU_MASS_HIGH)s) )" % self.config
      mothercut += " & ( (M>%(HTAU_MASS_LOW)s) & (M<%(HTAU_MASS_HIGH)s) )" % self.config
    
    mothercut += " & (PT > %(HTAU_PT_MIN)s) & (PT < %(HTAU_PT_MAX)s)"\
                 " & (ETA > %(HTAU_ETA_MIN)s) & (ETA < %(HTAU_ETA_MAX)s)" % self.config

    combcut   += " & (AMAXCHILD(PT) > %(HTAU_PT_DAUG_MAX)s) "\
                 " & (AMINCHILD(PT) > %(HTAU_PT_DAUG_MIN)s) "\
                 " & (AMAXCHILD(ETA) < %(HTAU_ETA_DAUG_MAX)s) "\
                 " & (AMINCHILD(ETA) > %(HTAU_ETA_DAUG_MIN)s) " % self.config

    return SimpleSelection(name, 
                           CombineParticles, 
                           inps,
                           DecayDescriptors = mydecay,
                           Preambulo        = preambulo,
                           CombinationCut   = combcut,
                           MotherCut        = mothercut)

  ##########################################################################

  def _makeLine(self, name, sel, presc, postsc):

    line = StrippingLine(name,
                         prescale    = presc,
                         postscale   = postsc,
                         FILTER = self.FilterSPD,
                         selection = sel,
                         MaxCandidates = 50 )

    self.registerLine( line )

    return line

  ##########################################################################
