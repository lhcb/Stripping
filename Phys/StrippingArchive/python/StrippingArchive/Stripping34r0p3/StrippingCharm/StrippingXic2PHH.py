###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = ['Hong-Jian Wang']
__date__ = '07/03/2023'
__version__ = '$Revision: 0.1 $'
__all__ = ('StrippingXic2PHH'
           ,'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdLoosePions, StdLooseKaons, StdLooseProtons
from StandardParticles import StdLooseKsLL, StdLooseKsDD
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from PhysSelPython.Wrappers import MultiSelectionSequence
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond
import sys

default_name='Xic2PHH'
    #### This is the dictionary of all tunable cuts ########
default_config={
      'NAME'        : 'Xic2PHH',
      'WGs'         : ['Charm'],
      'BUILDERTYPE' : 'StrippingXic2PHH',
      'STREAMS'     : ['Charm'],
      'CONFIG'      : {
          'GEC_nLongTrk' : 250    # adimensional
        , 'signalPrescaleViaXicPKK'     :   1.0
        , 'signalPrescaleViaXicPPhi2KK' :   1.0
        , 'signalPrescaleViaXicPPiPi'   :   1.0
        , 'signalPrescaleViaXicPKS0LL'  :   1.0
        , 'signalPrescaleViaXicPKS0DD'  :   1.0
        , 'TRCHI2DOFMax'            :   3.0
        , 'GhostProb'               :   0.3
        , 'TrGhostProbMax'          :   0.25 # same for all particles
        , 'MINIPCHI2'               :   4.0  # adimensiional, orig. 9
        , 'PionP'                   :   3.0*GeV
        , 'PionPT'                  :   500*MeV
        , 'PionPIDK'                :  10.0
        , 'Pion_PIDpiPIDK_Min'      :   0.0
        , 'KaonP'                   :   2.0*GeV
        , 'KaonPT'                  :   200*MeV
        , 'KaonPIDK'                :  10.0
        , 'Kaon_PIDKPIDpi_Min'      :   5.0
        , 'ProtonP'                 :   3.0*GeV
        , 'ProtonPT'                :   500*MeV
        , 'Proton_PIDpPIDpi_Min'    :   5.0
        , 'Proton_PIDpPIDK_Min'     :   0.0
        , 'ProbNNpMin'              :   0.2 #ProbNNp cut for proton in L0, to suppress the bkg of L0
        , 'ProbNNp'                 :   0.4
        , 'ProbNNk'                 :   0.4
        , 'ProbNNpi'                :   0.5
        , 'ProbNNpiMax'             :   0.95
        , 'Xic_ADAMASS_HalfWin'     : 170.0*MeV
        , 'Xic_ADMASS_HalfWin'      : 120.0*MeV
        , 'Xic_BPVVDCHI2_Min'       :  25.0
        , 'Xic_BPVDIRA_Min'         :   0.9
        , 'pKK_AM_Min'              :  2380.0*MeV
        , 'ppipi_AM_Min'            :  2380.0*MeV
        , 'Xic_AM_Max'              :  2550.0*MeV
        # KS (DD)
        , 'MinKsPT_DD'              : 200 * MeV
        , 'MaxKsVCHI2NDOF_DD'       : 20.0
        , 'MinDz_DD'                : 250. * mm
        , 'MaxDz_DD'                : 9999 * mm
        , 'MinKsIpChi2_DD'          : 3
        , 'KSCutDIRA_DD'            : 0.999
        , 'KSCutMass_DD'            : 50 * MeV
        , 'KSCutFDChi2_DD'          : 5
        # KS (LL)
        , 'MinKsPT_LL'              : 200 * MeV
        , 'MaxKsVCHI2NDOF_LL'       : 20.0
        , 'MinDz_LL'                : 0 * mm
        , 'MaxDz_LL'                : 9999 * mm
        , 'MinKsIpChi2_LL'          : 3
        , 'KSCutDIRA_LL'            : 0.999
        , 'KSCutMass_LL'            : 35 * MeV
        , 'KSCutFDChi2_LL'          : 5
        # phi1020
        , 'MinPhiPT'                : 200 * MeV
        , 'MinPhiP'                 : 3 * GeV
        , 'MinPhiEta'               : 2.0
        , 'MaxPhiEta'               : 5.0
        , 'BPVIPCHI2_Phi'           : 4
        , 'PhiCutMass'              : 20 * MeV
      } ## end of 'CONFIG'
}  ## end of default_config

#-------------------------------------------------------------------------------------------------------------
class StrippingXic2PHH(LineBuilder) :
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config
       
        GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                     "Preambulo": ["from LoKiTracks.decorators import *"]}
        
        ##########################################################################
        ## Basic particles: K, pi, p
        ##########################################################################
        self.selKaon = Selection( "SelKfor" + name,
                Algorithm = self._kaonFilter("Kfor"+name),
                RequiredSelections = [StdLooseKaons])

        self.selPion = Selection( "SelPifor" + name,
                Algorithm = self._pionFilter("Pifor"+name),
                RequiredSelections = [StdLoosePions])

        self.selProton = Selection( "SelPfor" + name,
                Algorithm = self._protonFilter("Pfor"+name),
                RequiredSelections = [StdLooseProtons])
        ##########################################################################
        ## KS0 -> pi+ pi-
        ##########################################################################
        _stdLooseKs0LL = DataOnDemand("Phys/StdLooseKsLL/Particles")
        _stdLooseKs0DD = DataOnDemand("Phys/StdLooseKsDD/Particles")

        self.selKs0LL = Selection("SelKsLL0for"+name,
                Algorithm = self._Ks0LLFilter("Ks0LLfor"+name),
                RequiredSelections = [_stdLooseKs0LL])

        self.selKs0DD = Selection("SelKs0DDfor"+name,
                Algorithm = self._Ks0DDFilter("Ks0DDfor"+name),
                RequiredSelections = [_stdLooseKs0DD])
        ##########################################################################
        ## Xi- -> Lambda0 pi- 
        ##########################################################################
        self.phi2KK = self.createCombinationSel(OutputList = "phi2KKfor"+ self.name,
                DecayDescriptor = "[phi(1020) -> K+ K-]cc",
                DaughterLists   = [self.selKaon],
                DaughterCuts    = {"K+" : "(MIPCHI2DV(PRIMARY)>4) "},
                PreVertexCuts   = "(ADAMASS('phi(1020)') < 40*MeV) & (ADOCACHI2CUT(30, ''))",
                PostVertexCuts  = "  (PT > %(MinPhiPT)s) & (P > %(MinPhiP)s)"\
                                  "& (BPVIPCHI2() > %(BPVIPCHI2_Phi)s)"\
                                  "& (in_range(%(MinPhiEta)s, ETA, %(MaxPhiEta)s))"\
                                  "& (ADMASS('phi(1020)') < %(PhiCutMass)s)" % self.config )
        self.Xic2PHHList = self.makeXic2PHH()

    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    # Sub Function
    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    def _pionFilter( self , _name):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (P>%(PionP)s) & (PT > %(PionPT)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PIDK< %(PionPIDK)s)"\
                "& (PROBNNpi > %(ProbNNpi)s)" % self.config
        _pi = FilterDesktop(Code = _code )
        return _pi

    def _kaonFilter( self , _name ):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (P>%(KaonP)s) & (PT > %(KaonPT)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PIDK> %(KaonPIDK)s) & (PROBNNpi < %(ProbNNpiMax)s)"\
                "& (PROBNNk > %(ProbNNk)s)"\
                "& (HASRICH)&(PIDK-PIDpi>%(Kaon_PIDKPIDpi_Min)s)"% self.config
        _ka = FilterDesktop(Code = _code )
        return _ka

    def _protonFilter( self, _name ):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (PT > %(ProtonPT)s) & (P>%(ProtonP)s)"\
                "& (TRGHOSTPROB < %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PROBNNp > %(ProbNNp)s) & (PROBNNpi < %(ProbNNpiMax)s)"\
                "& (HASRICH)&(PIDp-PIDpi > %(Proton_PIDpPIDpi_Min)s)"\
                "& (HASRICH)&(PIDp-PIDK > %(Proton_PIDpPIDK_Min)s)"% self.config
        _pr = FilterDesktop(Code = _code)
        return _pr

    def _Ks0LLFilter( self, _name):
        _code = "  (PT > %(MinKsPT_LL)s)" \
                "& (BPVVDZ > %(MinDz_LL)s ) " \
                "& (BPVVDZ < %(MaxDz_LL)s ) " \
                "& (MIPCHI2DV(PRIMARY) > %(MinKsIpChi2_LL)s) "\
                "& (BPVDIRA > %(KSCutDIRA_LL)s )" \
                "& (ADMASS('KS0') < %(KSCutMass_LL)s)" \
                "& (BPVVDCHI2 > %(KSCutFDChi2_LL)s)" % self.config
        _Ks0LL = FilterDesktop(Code = _code)
        return _Ks0LL

    def _Ks0DDFilter( self , _name):
        _code = "   (PT > %(MinKsPT_DD)s)" \
                " & (BPVVDZ > %(MinDz_DD)s ) " \
                " & (BPVVDZ < %(MaxDz_DD)s ) " \
                " & (MIPCHI2DV(PRIMARY) > %(MinKsIpChi2_DD)s) "\
                " & (BPVDIRA > %(KSCutDIRA_DD)s )" \
                " & (ADMASS('KS0') < %(KSCutMass_DD)s)" \
                " & (BPVVDCHI2 > %(KSCutFDChi2_DD)s)" % self.config
        _Ks0DD = FilterDesktop(Code = _code)
        return _Ks0DD

    ##------------------------------------------------------------------------------------------
    ## --------------------  Begin to makeXic2PHH  ------------
    def makeXic2PHH( self ):
        #Cut for Basic

        _PreVertexCuts   = "(ADAMASS('Xi_c+') < 120*MeV) & (ADOCACHI2CUT(30, ''))"
        _PostVertexCuts  = "(ADMASS('Xi_c+')<90*MeV)"\
                           "& (VFASPF(VCHI2)<25)"\
                           "& (P> 1000*MeV) & (PT > 250*MeV)"\
                           "& (BPVVDCHI2 > 49.0 )"\
                           "& (VFASPF(VCHI2/VDOF) < 12.0)"

        _strCutMothfor = "(BPVVDCHI2>%(Xic_BPVVDCHI2_Min)s)" \
                         "& (BPVDIRA>%(Xic_BPVDIRA_Min)s)" % self.config

        _strCutComb_XicMass = "(ADAMASS('Xi_c+') < %(Xic_ADAMASS_HalfWin)s )" % self.config
        _strCutMoth_XicMass = "(ADMASS('Xi_c+') < %(Xic_ADMASS_HalfWin)s)" % self.config
        
        _strCutMothforXicPlus   = _strCutMothfor + '&' + _strCutMoth_XicMass + '&' + _PostVertexCuts

        _strCutComb_MassPKK   = "(AM>%(pKK_AM_Min)s) & (AM<%(Xic_AM_Max)s)" % self.config
        _strCutComb_MassPPiPi = "(AM>%(ppipi_AM_Min)s) & (AM<%(Xic_AM_Max)s)" % self.config

        _strCutComb_Xic2PKK   = _strCutComb_XicMass + '&' + _strCutComb_MassPKK   + '&' + _PreVertexCuts
        _strCutComb_Xic2PPiPi = _strCutComb_XicMass + '&' + _strCutComb_MassPPiPi + '&' + _PreVertexCuts
        _strCutMothPHH = _strCutMothforXicPlus

        ''' Stripping Xi_c+ -> p+ K+ K- '''
        Xic2PKK = self.createCombinationSel(OutputList = "Xic2PKK" + self.name,
                DecayDescriptor = "[Xi_c+ -> p+ K+ K-]cc",
                DaughterLists   = [self.selProton, self.selKaon],
                PreVertexCuts   = _strCutComb_Xic2PKK,
                PostVertexCuts  = _strCutMothPHH )
        Xic2PKKLine = StrippingLine( self.name + "Xicp2PKKLine",
                prescale = self.config['signalPrescaleViaXicPKK'],
                algos = [ Xic2PKK ],
                EnableFlavourTagging = False,
                RelatedInfoTools = self._getRelInfoXic2PKK() )
        self.registerLine (Xic2PKKLine)

        ''' Stripping Xi_c+ -> p+ (phi(1020) -> K+ K-) '''
        Xic2PPhi2KK = self.createCombinationSel(OutputList = "Xic2PPhi2KK" + self.name,
                DecayDescriptor = "[Xi_c+ -> p+ phi(1020)]cc",
                DaughterLists   = [self.selProton, self.phi2KK],
                PreVertexCuts   = _strCutComb_Xic2PKK,
                PostVertexCuts  = _strCutMothPHH )
        Xic2PPhi2KKLine = StrippingLine( self.name + "Xicp2PPhi2KKLine",
                prescale = self.config['signalPrescaleViaXicPPhi2KK'],
                algos = [ Xic2PPhi2KK ],
                EnableFlavourTagging = False,
                RelatedInfoTools = self._getRelInfoXic2PPhi2KK() )
        self.registerLine (Xic2PPhi2KKLine)

        ''' Stripping Xi_c+ -> p+ pi+ pi-'''
        Xic2PPiPi = self.createCombinationSel(OutputList = "Xic2PPiPi" + self.name,
                DecayDescriptor = "[Xi_c+ -> p+ pi+ pi-]cc",
                DaughterLists   = [self.selProton, self.selPion],
                PreVertexCuts   = _strCutComb_Xic2PPiPi,
                PostVertexCuts  = _strCutMothPHH )
        Xic2PPiPiLine = StrippingLine( self.name + "Xicp2PPiPiLine",
                prescale = self.config['signalPrescaleViaXicPPiPi'],
                algos = [ Xic2PPiPi ],
                EnableFlavourTagging = False,
                RelatedInfoTools = self._getRelInfoXic2PPiPi() )
        self.registerLine (Xic2PPiPiLine)

        ''' Stripping Xi_c+ -> p+ KS0(LL)'''
        Xic2PKS0LL = self.createCombinationSel(OutputList = "Xic2PKS0LL" + self.name,
                DecayDescriptor = "[Xi_c+ -> p+ KS0]cc",
                DaughterLists   = [self.selProton, self.selKs0LL],
                PreVertexCuts   = _strCutComb_Xic2PPiPi,
                PostVertexCuts  = _strCutMothPHH )
        Xic2PKS0LLLine = StrippingLine( self.name + "Xicp2PKS0LLLine",
                prescale = self.config['signalPrescaleViaXicPKS0LL'],
                algos = [ Xic2PKS0LL ],
                EnableFlavourTagging = False,
                RelatedInfoTools = self._getRelInfoXic2PKS0() )
        self.registerLine (Xic2PKS0LLLine)

        ''' Stripping Xi_c+ -> p+ KS0(DD)'''
        Xic2PKS0DD = self.createCombinationSel(OutputList = "Xic2PKS0D" + self.name,
                DecayDescriptor = "[Xi_c+ -> p+ KS0]cc",
                DaughterLists   = [self.selProton, self.selKs0DD],
                PreVertexCuts   = _strCutComb_Xic2PPiPi,
                PostVertexCuts  = _strCutMothPHH )
        Xic2PKS0DDLine = StrippingLine( self.name + "Xicp2PKS0DDLine",
                prescale = self.config['signalPrescaleViaXicPKS0DD'],
                algos = [ Xic2PKS0DD ],
                EnableFlavourTagging = False,
                RelatedInfoTools = self._getRelInfoXic2PKS0() )
        self.registerLine (Xic2PKS0DDLine)

    ##  --------------------  end of makeXic2PHH  ------------
    ##------------------------------------------------------------------------------------------

    ##########################################################################
    ## Basic Function
    ##########################################################################
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                Algorithm = filter,
                RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
            DecayDescriptor,
            DaughterLists,
            DaughterCuts = {} ,
            PreVertexCuts = "ALL",
            PostVertexCuts = "ALL") :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                DaughtersCuts = DaughterCuts,
                MotherCut = PostVertexCuts,
                CombinationCut = PreVertexCuts,
                ReFitPVs = True)
        return Selection ( OutputList,
                Algorithm = combiner,
                RequiredSelections = DaughterLists)
    #print "DEBUG here is :",__file__,sys._getframe().f_lineno

    def _getRelInfoXic2PKK(self):
        relInfo = []
        for coneAngle in [0.8,1.0,1.3,1.7]:
            conestr = str(coneAngle).replace('.','')
            relInfo += [
            { "Type"         : "RelInfoConeVariables",
            "ConeAngle"    : coneAngle,
            "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
            "DaughterLocations" : {
                "^[Xi_c+ -> p+ K+ K-]CC" : 'P2ConeVar%s_Xic' % conestr,
                "[Xi_c+ -> ^p+ K+ K-]CC" : 'P2ConeVar%s_proton' % conestr,
                "[Xi_c+ -> p+ ^K+ K-]CC" : 'P2ConeVar%s_K1' % conestr,
                "[Xi_c+ -> p+ K+ ^K-]CC" : 'P2ConeVar%s_K2' % conestr } }
                       ]
        relInfo += [ { "Type" : "RelInfoVertexIsolation",
            "DaughterLocations" : {
                "^[Xi_c+ -> p+ K+ K-]CC" : 'VertexIsoInfo_Xic'} } ]
        return relInfo

    def _getRelInfoXic2PPhi2KK(self):
        relInfo = []
        for coneAngle in [0.8,1.0,1.3,1.7]:
            conestr = str(coneAngle).replace('.','')
            relInfo += [
            { "Type"         : "RelInfoConeVariables",
            "ConeAngle"    : coneAngle,
            "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
            "DaughterLocations" : {
                "^[Xi_c+ -> p+ (phi(1020) -> K+ K-)]CC" : 'P2ConeVar%s_Xic' % conestr,
                "[Xi_c+ -> ^p+ (phi(1020) -> K+ K-)]CC" : 'P2ConeVar%s_proton' % conestr,
                "[Xi_c+ -> p+ ^(phi(1020) -> K+ K-)]CC" : 'P2ConeVar%s_phi' % conestr,
                "[Xi_c+ -> p+ (phi(1020) -> ^K+ K-)]CC" : 'P2ConeVar%s_K1' % conestr,
                "[Xi_c+ -> p+ (phi(1020) -> K+ ^K-)]CC" : 'P2ConeVar%s_K2' % conestr } }
                       ]
        relInfo += [ { "Type" : "RelInfoVertexIsolation",
            "DaughterLocations" : {
                "^[Xi_c+ -> p+ (phi(1020) -> K+ K-)]CC" : 'VertexIsoInfo_Xic',
                "[Xi_c+ -> p+ ^(phi(1020) -> K+ K-)]CC" : 'VertexIsoInfo_phi'} } ]
        return relInfo

    def _getRelInfoXic2PPiPi(self):
        relInfo = []
        for coneAngle in [0.8,1.0,1.3,1.7]:
            conestr = str(coneAngle).replace('.','')
            relInfo += [
            { "Type"         : "RelInfoConeVariables",
            "ConeAngle"    : coneAngle,
            "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
            "DaughterLocations" : {
                "^[Xi_c+ -> p+ pi+ pi-]CC" : 'P2ConeVar%s_Xic' % conestr,
                "[Xi_c+ -> ^p+ pi+ pi-]CC" : 'P2ConeVar%s_proton' % conestr,
                "[Xi_c+ -> p+ ^pi+ pi-]CC" : 'P2ConeVar%s_pi1' % conestr,
                "[Xi_c+ -> p+ pi+ ^pi-]CC" : 'P2ConeVar%s_pi2' % conestr } }
                       ]
        relInfo += [ { "Type" : "RelInfoVertexIsolation",
            "DaughterLocations" : {
                "^[Xi_c+ -> p+ pi+ pi-]CC" : 'VertexIsoInfo_Xic'} } ]
        return relInfo

    def _getRelInfoXic2PKS0(self):
        relInfo = []
        for coneAngle in [0.8,1.0,1.3,1.7]:
            conestr = str(coneAngle).replace('.','')
            relInfo += [
            { "Type"         : "RelInfoConeVariables",
            "ConeAngle"    : coneAngle,
            "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
            "DaughterLocations" : {
                "^[Xi_c+ -> p+ (KS0 -> pi+ pi-)]CC" : 'P2ConeVar%s_Xic' % conestr,
                "[Xi_c+ -> ^p+ (KS0 -> pi+ pi-)]CC" : 'P2ConeVar%s_proton' % conestr,
                "[Xi_c+ -> p+ ^(KS0 -> pi+ pi-)]CC" : 'P2ConeVar%s_KS0' % conestr,
                "[Xi_c+ -> p+ (KS0 -> ^pi+ pi-)]CC" : 'P2ConeVar%s_pi1' % conestr,
                "[Xi_c+ -> p+ (KS0 -> pi+ ^pi-)]CC" : 'P2ConeVar%s_pi2' % conestr } }
                       ]
        relInfo += [ { "Type" : "RelInfoVertexIsolation",
            "DaughterLocations" : {
                "^[Xi_c+ -> p+ (KS0 -> pi+ pi-)]CC" : 'VertexIsoInfo_Xic',
                "[Xi_c+ -> p+ ^(KS0 -> pi+ pi-)]CC" : 'VertexIsoInfo_KS0'} } ]
        return relInfo
