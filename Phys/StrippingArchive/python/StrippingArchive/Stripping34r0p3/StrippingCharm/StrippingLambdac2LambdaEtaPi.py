###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
The code is for analysis of the decay
  Lambda_c+ -> Lambda0 eta pi+
where the daughter baryon is reconstructed via:
  Lambda0 -> p pi- (from StdLooseLambdaLL/DD)

A bachelor pi from Lambda_b0 is tagged for reducing the backgrond rate i.e.
  Lambda_b0 -> Lambda_c+ pi-
'''


__author__ = ['Xiao-Rui Lyu', 'Yangjie Su']
__date__ = '2023/06/30'
__version__ = '$Revision: 0.1 $'
__all__ = ('StrippingLambdac2LambdaEtaPi'
           ,'default_config')


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdLoosePions, StdNoPIDsDownPions, StdLooseKaons, StdLooseDownKaons
from StandardParticles import  StdAllLoosePions, StdAllLooseKaons, StdLooseMuons, StdAllLooseProtons, StdAllLooseMuons, StdAllVeryLooseMuons
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from PhysSelPython.Wrappers import MultiSelectionSequence
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond
import sys


default_name='Lambdac2LambdaEtaPi'
    #### This is the dictionary of all tunable cuts ########
default_config={
      'NAME'        : 'Lambdac2LambdaEtaPi',
      'WGs'         : ['Charm'],
      'BUILDERTYPE' : 'StrippingLambdac2LambdaEtaPi',
      'STREAMS'     : ['Charm'],
      'CONFIG'      : {
          'GEC_nLongTrk' : 160    # adimensional
        , 'signalPrescale'       :   1.0
        , 'TRCHI2DOFMax'            :   3.0
        , 'GhostProb'               :   0.3
        , 'TrGhostProbMax'          :   0.25 # same for all particles
        , 'MINIPCHI2'               :   3.0  # adimensiional
        , 'MuonP'                   :   3*GeV
        , 'MuonPT'                  :   250*MeV
        , 'MuonIPCHI2'              :   4.0 
        , 'MuonPIDmu'               :   0.0
        , 'PionP'                   :   3*GeV
        , 'PionPT'                  :   250*MeV
        , 'PionPIDK'                :  5.0
        , 'Pion_PIDpiPIDK_Min'      :   0.0
        , 'KaonP'                   :   3*GeV
        , 'KaonPT'                  :   250*MeV
        , 'KaonPIDK'                :  10.0
        , 'Kaon_PIDKPIDpi_Min'      :   5.0
        , 'ProbNNpMin'              :   0.2 #ProbNNp cut for proton in L0, to suppress the bkg of L0 
        , 'ProbNNp'                 :   0.4
        , 'ProbNNk'                 :   0.4
        , 'ProbNNpi'                :   0.4
        , 'ProbNNpiMax'             :   0.9
        , 'LambdaLLPMin'            :2000.0*MeV
        , 'LambdaLLPTMin'           : 500.0*MeV
        , 'LambdaLLCutMass'         :  50.0*MeV
        , 'LambdaLLCutFDChi2'       : 100.0 ## unitless
        , 'LambdaDDPMin'            :2000.0*MeV
        , 'LambdaDDPTMin'           : 500.0*MeV
        , 'LambdaDDCutMass'         :  50.0*MeV
        , 'LambdaDDCutFDChi2'       : 100.0## unitless
        , 'LambdaVertexChi2'        :   10.0## max chi2/ndf for Lambda0 vertex

        , 'Lc_M_HalfWin'           :   200.0*MeV
        , 'Lb_M_HalfWin'           :   200.0*MeV
        , 'Lc_Daug_1of3_MIPCHI2DV_Min': 3.0
        , 'Lc_ADOCAMAX_Max'         :   0.15*mm
        , 'Lc_APT_Min'              :   1.2*GeV
        , 'Lc_AP_Min'               :   15*GeV
        , 'Lc_VCHI2_Max'            :  30.0
        , 'Lc_BPVVDCHI2_Min'        :  16.0
        , 'Lc_BPVVDZ_Min'           :  0.5*mm
        , 'Lc_BPVDIRA_Min'          :   0.95

        #For Lb
        , 'Lb_AP_Min': 20*GeV 
        , 'Lb_APT_Min': 1.5*GeV 
        , 'Lb_VCHI2_Max': 30 
        , 'Lb_BPVVDCHI2_Min': 16 
        , 'Lb_BPVDIRA_Min': 0.95 
      } ## end of 'CONFIG' 
}  ## end of default_config

#-------------------------------------------------------------------------------------------------------------
class StrippingLambdac2LambdaEtaPi(LineBuilder) : 
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config
       
        GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                     "Preambulo": ["from LoKiTracks.decorators import *"]}
        
        ##########################################################################
        ## Basic particles: mu, K, pi, eta
        ##########################################################################
        self.selMuon = Selection( "SelMufor" + name,
                Algorithm = self._muonFilter("Mufor"+name),
                RequiredSelections = [StdLooseMuons])

        self.selKaon = Selection( "SelKfor" + name,
                Algorithm = self._kaonFilter("Kfor"+name),
                RequiredSelections = [StdAllLooseKaons])

        self.selPion = Selection( "SelPifor" + name,
                Algorithm = self._pionFilter("Pifor"+name),
                RequiredSelections = [StdAllLoosePions])

        self.makeEta()
        # then use self.Eta2PiPiPi0 and self.Eta2PiPiGamma

        ##########################################################################
        ## Lambda0 -> p+ pi- 
        ##########################################################################
        _stdLooseLambdaLL = DataOnDemand("Phys/StdVeryLooseLambdaLL/Particles")
        _stdLooseLambdaDD = DataOnDemand("Phys/StdLooseLambdaDD/Particles")

        self.selLambdaLL = Selection("SelLambdaLLfor"+name,
                Algorithm = self._LambdaLLFilter("LambdaLLfor"+name),
                RequiredSelections = [_stdLooseLambdaLL])

        self.selLambdaDD = Selection("SelLambdaDDfor"+name,
                Algorithm = self._LambdaDDFilter("LambdaDDfor"+name),
                RequiredSelections = [_stdLooseLambdaDD])

        ##########################################################################
        ## Charmed Meson SL lines
        ##########################################################################
        self.DmesonSLList = self.makeLine()


    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    # Sub Function
    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    def _muonFilter( self , _name):
        _code = "  (TRCHI2DOF< %(TRCHI2DOFMax)s)"\
                "& (PT>%(MuonPT)s) & (P>%(MuonP)s)"\
                "& (TRGHOSTPROB< %(GhostProb)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MuonIPCHI2)s)"\
                "& (PIDmu > %(MuonPIDmu)s) & ISMUON" % self.config
        _mu = FilterDesktop(Code = _code )
        return _mu

    def _pionFilter( self , _name):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (P>%(PionP)s) & (PT > %(PionPT)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PIDK< %(PionPIDK)s) "\
                "& (HASRICH)&(PROBNNpi > %(ProbNNpi)s) " % self.config
        _pi = FilterDesktop(Code = _code )
        return _pi

    def _kaonFilter( self , _name ):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (P>%(KaonP)s) & (PT > %(KaonPT)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PIDK> %(KaonPIDK)s) & (PROBNNpi < %(ProbNNpiMax)s)"\
                "& (PROBNNk > %(ProbNNk)s) "\
                "& (HASRICH)&(PIDK-PIDpi>%(Kaon_PIDKPIDpi_Min)s) "% self.config
        _ka = FilterDesktop(Code = _code )
        return _ka

    def _LambdaLLFilter( self, _name):
        _code = " (P > %(LambdaLLPMin)s) & (PT > %(LambdaLLPTMin)s)" \
                " &(ADMASS('Lambda0')<%(LambdaLLCutMass)s)"\
                " & (BPVVDCHI2> %(LambdaLLCutFDChi2)s)" \
                " & (VFASPF(VCHI2PDOF) < %(LambdaVertexChi2)s)" % self.config
        _l0LL = FilterDesktop(Code = _code)
        return _l0LL

    def _LambdaDDFilter( self , _name):
        _code = " (P> %(LambdaDDPMin)s) & (PT> %(LambdaDDPTMin)s)" \
                " & (ADMASS('Lambda0') < %(LambdaDDCutMass)s) "\
                " & (BPVVDCHI2> %(LambdaDDCutFDChi2)s)" \
                " & (VFASPF(VCHI2PDOF) < %(LambdaVertexChi2)s)" % self.config
        _l0DD = FilterDesktop(Code = _code)
        return _l0DD

    def makeLb0Line(self, line_name, prescale, Lc):
        comb_cut = "(ADAMASS('Lambda_b0') < 1.1*%(Lb_M_HalfWin)s)"\
                   "& (AP > %(Lb_AP_Min)s)"\
                   "& (APT > %(Lb_APT_Min)s)" % self.config
        vertex_cut  = "  (ADMASS('Lambda_b0') < %(Lb_M_HalfWin)s)"\
                      "& (BPVVDCHI2>%(Lb_BPVVDCHI2_Min)s)" \
                      "& (BPVDIRA>%(Lb_BPVDIRA_Min)s)" % self.config

        Lb0 = self.createCombinationSel(OutputList = "Lb02LcPi_" + line_name,
                DecayDescriptor = "[Lambda_b0 -> Lambda_c+ pi-]cc",
                DaughterLists   = [Lc, self.selPion],
                PreVertexCuts   = comb_cut,
                PostVertexCuts  = vertex_cut )
        Lb0Line = StrippingLine( line_name, 
                prescale = prescale,
                selection = Lb0, 
                EnableFlavourTagging = False )
        return Lb0Line


    ##------------------------------------------------------------------------------------------
    ##  -------------------- Begin to DpSL  ------------
    def makeLine( self ):

        _strCutCombfor = "(AMINCHILD(MIPCHI2DV(PRIMARY))>%(Lc_Daug_1of3_MIPCHI2DV_Min)s)" \
                    "& (AP>%(Lc_AP_Min)s)" \
                    "& (ADAMASS('Lambda_c+') < %(Lc_M_HalfWin)s)" \
                    "& (APT>%(Lc_APT_Min)s)" % self.config

        _strCutMothfor = "(VFASPF(VCHI2) < %(Lc_VCHI2_Max)s)" \
                      "& (ADMASS('Lambda_c+') < %(Lc_M_HalfWin)s)" \
                      "& (BPVVDCHI2>%(Lc_BPVVDCHI2_Min)s)" \
                      "& (BPVDIRA>%(Lc_BPVDIRA_Min)s)" % self.config

        _strCutComb_Mass   = "(ADAMASS('Lambda_c+') < 1.1*%(Lc_M_HalfWin)s)" % self.config
        _strCutMoth_Mass   = "(ADMASS('Lambda_c+') < %(Lc_M_HalfWin)s)" % self.config

        _strCutComb   = _strCutCombfor + '&' + _strCutComb_Mass
        _strCutMoth   = _strCutMothfor + '&' + _strCutMoth_Mass
        
        ''' Stripping Lambda_c+ -> Lambda eta pi+ '''
        LmdEtaPiLLEta2PiPiPi0 = self.createCombinationSel(OutputList = "Lc2LambdaEtaPiLLEta2PiPiPi0" + self.name,
                DecayDescriptor = "[Lambda_c+ -> Lambda0 eta pi+]cc",
                DaughterLists   = [self.selLambdaLL, self.Eta2PiPiPi0,self.selPion],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        self.registerLine(self.makeLb0Line(
            line_name=self.name + "Lb2LcPiLc2LambdaEtaPiLLEta2PiPiPi0",
            prescale=self.config['signalPrescale'],
            Lc=LmdEtaPiLLEta2PiPiPi0
            ))

        ''' Stripping Lambda_c+ -> Lambda eta pi+ '''
        LmdEtaPiLLEta2PiPiGamma = self.createCombinationSel(OutputList = "Lc2LambdaEtaPiLLEta2PiPiGamma" + self.name,
                DecayDescriptor = "[Lambda_c+ -> Lambda0 eta pi+]cc",
                DaughterLists   = [self.selLambdaLL, self.Eta2PiPiGamma,self.selPion],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        self.registerLine(self.makeLb0Line(
            line_name=self.name + "Lb2LcPiLc2LambdaEtaPiLLEta2PiPiGamma",
            prescale=self.config['signalPrescale'],
            Lc=LmdEtaPiLLEta2PiPiGamma
            ))

        ''' Stripping Lambda_c+ -> Lambda eta pi+ '''
        LmdEtaPiDDEta2PiPiPi0 = self.createCombinationSel(OutputList = "Lc2LambdaEtaPiDDEta2PiPiPi0" + self.name,
                DecayDescriptor = "[Lambda_c+ -> Lambda0 eta pi+]cc",
                DaughterLists   = [self.selLambdaDD, self.Eta2PiPiPi0,self.selPion],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        self.registerLine(self.makeLb0Line(
            line_name=self.name + "Lb2LcPiLc2LambdaEtaPiDDEta2PiPiPi0",
            prescale=self.config['signalPrescale'],
            Lc=LmdEtaPiDDEta2PiPiPi0
            ))

        ''' Stripping Lambda_c+ -> Lambda eta pi+ '''
        LmdEtaPiDDEta2PiPiGamma = self.createCombinationSel(OutputList = "Lc2LambdaEtaPiDDEta2PiPiGamma" + self.name,
                DecayDescriptor = "[Lambda_c+ -> Lambda0 eta pi+]cc",
                DaughterLists   = [self.selLambdaDD, self.Eta2PiPiGamma,self.selPion],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        self.registerLine(self.makeLb0Line(
            line_name=self.name + "Lb2LcPiLc2LambdaEtaPiDDEta2PiPiGamma",
            prescale=self.config['signalPrescale'],
            Lc=LmdEtaPiDDEta2PiPiGamma
            ))
    ##  --------------------  End of DpSL  ------------
    ##------------------------------------------------------------------------------------------
    def makeEta(self):
        #get standard containers
        self.Pi0List     = DataOnDemand(Location = "Phys/StdLoosePi02gg/Particles")
        self.GammaList   = DataOnDemand(Location = "Phys/StdLooseAllPhotons/Particles")
        self.LoosePionList    = DataOnDemand(Location = "Phys/StdLoosePions/Particles")

        #clean neutrals
        _tightpi0_cut   = "(CHILD(CL,1)>0.05) & (CHILD(CL,2)>0.05) & (PT>1*GeV)"
        _tightgamma_cut = "(CL>0.05) & (PT>500*MeV)"

        #pipi vertex cuts
        _pipix_cut      = "(BPVVDZ>0) & (VFASPF(VCHI2)<9) & (BPVDIRA>0.95) & (BPVVDCHI2>25)"

        #pipix0 cuts
        _eta_cut        = "(ADAMASS('eta')<100*MeV)       & (APT>1500*MeV)"

        #filter standard containers
        self.TightPi0List   = self.createSubSel( OutputList = self.name + '_TightPi0',   InputList  = self.Pi0List,     Cuts = _tightpi0_cut)
        self.TightGammaList = self.createSubSel( OutputList = self.name + '_TightGamma', InputList  = self.GammaList,   Cuts = _tightgamma_cut)

        #-------------

        self.Eta2PiPiPi0 = self.createCombinationSel( OutputList      = self.name + "_Eta2PiPiPi0",
                                                      DecayDescriptor = "eta -> pi+ pi- pi0",
                                                      DaughterLists   = [ self.LoosePionList, self.TightPi0List],
                                                      PreVertexCuts   = _eta_cut,
                                                      PostVertexCuts  = _pipix_cut)
        #-------------

        self.Eta2PiPiGamma = self.createCombinationSel( OutputList      = self.name + "_Eta2PiPiGamma",
                                                        DecayDescriptor = "eta -> pi+ pi- gamma",
                                                        DaughterLists   = [ self.LoosePionList, self.TightGammaList],
                                                        PreVertexCuts   = _eta_cut,
                                                        PostVertexCuts  = _pipix_cut )

    ##########################################################################
    ## Basic Function
    ##########################################################################
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                Algorithm = filter,
                RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
            DecayDescriptor,
            DaughterLists,
            DaughterCuts = {} ,
            PreVertexCuts = "ALL",
            PostVertexCuts = "ALL") :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        if type(DecayDescriptor) is list:
            combiner = CombineParticles( DecayDescriptors = DecayDescriptor,
                    DaughtersCuts = DaughterCuts,
                    MotherCut = PostVertexCuts,
                    CombinationCut = PreVertexCuts,
                    ReFitPVs = True)
            return Selection ( OutputList,
                    Algorithm = combiner,
                    RequiredSelections = DaughterLists)
        else:
            combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                    DaughtersCuts = DaughterCuts,
                    MotherCut = PostVertexCuts,
                    CombinationCut = PreVertexCuts,
                    ReFitPVs = True)
            return Selection ( OutputList,
                    Algorithm = combiner,
                    RequiredSelections = DaughterLists)
    #print "DEBUG here is :",__file__,sys._getframe().f_lineno 
