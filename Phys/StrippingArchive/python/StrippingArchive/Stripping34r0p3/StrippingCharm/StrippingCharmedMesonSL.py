###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Speculative code for charmed meson searches.
Charmed mesons are from beauty decays (Btag) along with WS lines and normalization lines:
Note: 
The mass window for B~0 and D+ are wide enough to include B_s~0 and Ds+

1. B~0 ->  D+ pi- pi- pi+, D+ ->  K+ K- mu+ nu : 

2. B~0 ->  D+ pi- pi- pi+, D+ ->   pi+ pi- mu+ nu :

3. B- ->  D0 pi- pi- pi+, D0 ->   eta pi- mu+ nu, eta -> pi+ pi- pi0(gamma):

4. B- ->  D0 pi- pi- pi+, D0 ->   pi- pi+ pi- mu+ nu:

5. B- ->  D0 pi- pi- pi+, D0 ->   pi- K+ K- mu+ nu :
'''


__author__ = ['Xiao-Rui Lyu', 'Yangjie Su']
__date__ = '2023/06/30'
__version__ = '$Revision: 0.1 $'
__all__ = ('StrippingCharmedMesonSL'
           ,'default_config')


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdLoosePions, StdNoPIDsDownPions, StdLooseKaons, StdLooseDownKaons
from StandardParticles import  StdAllLoosePions, StdAllLooseKaons, StdLooseMuons, StdAllLooseProtons, StdAllLooseMuons, StdAllVeryLooseMuons
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from PhysSelPython.Wrappers import MultiSelectionSequence
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond
import sys


default_name='CharmedMesonSL'
    #### This is the dictionary of all tunable cuts ########
default_config={
      'NAME'        : 'CharmedMesonSL',
      'WGs'         : ['Charm'],
      'BUILDERTYPE' : 'StrippingCharmedMesonSL',
      'STREAMS'     : ['Charm'],
      'CONFIG'      : {
          'GEC_nLongTrk' : 160    # adimensional
        , 'signalPrescaleViaD2KKmunu'       :   1.0
        , 'controlPrescaleViaD2KKpi'        :   1.0
        , 'signalPrescaleViaD2pipimunu'     :   1.0
        , 'controlPrescaleViaD2pipipi'      :   1.0
        , 'signalPrescaleViaD02pipipimunu'  :   1.0
        , 'controlPrescaleViaD02pipipipi'   :   1.0
        , 'signalPrescaleViaD02KKpimunu'    :   1.0
        , 'controlPrescaleViaD02KKpipi'     :   1.0
        , 'signalPrescaleViaD02etapimunu'    :   1.0
        , 'controlPrescaleViaD02etapipi'     :   1.0
        , 'TRCHI2DOFMax'            :   3.0
        , 'GhostProb'               :   0.3
        , 'TrGhostProbMax'          :   0.25 # same for all particles
        , 'MINIPCHI2'               :   4.0  # adimensiional
        , 'MuonP'                   :   3*GeV
        , 'MuonPT'                  :   250*MeV
        , 'MuonIPCHI2'              :   4.0 
        , 'MuonPIDmu'               :   0.0
        , 'PionP'                   :   3*GeV
        , 'PionPT'                  :   250*MeV
        , 'PionPIDK'                :  5.0
        , 'Pion_PIDpiPIDK_Min'      :   0.0
        , 'KaonP'                   :   3*GeV
        , 'KaonPT'                  :   250*MeV
        , 'KaonPIDK'                :  10.0
        , 'Kaon_PIDKPIDpi_Min'      :   5.0
        , 'ProbNNk'                 :   0.4
        , 'ProbNNpi'                :   0.5
        , 'ProbNNpiMax'             :   0.9
        , 'Dp_ADMASS_Min'           :   1800.0*MeV
        , 'Dp_ADMASS_Max'           :   2030.0*MeV #large enough to cover D_s+
        , 'D0_ADMASS_Min'           :   1820.0*MeV
        , 'D0_ADMASS_Max'           :   1900.0*MeV 
        , 'Dp_Daug_1of3_MIPCHI2DV_Min': 4.0
        , 'Dp_ADOCAMAX_Max'         :   0.25*mm
        , 'Dp_APT_Min'              :   1.2*GeV
        , 'Dp_AP_Min'               :   20*GeV
        , 'Dp_VCHI2_Max'            :  30.0
        , 'Dp_BPVVDCHI2_Min'        :  16.0
        , 'Dp_BPVDIRA_Min'          :   0.95
        , 'KKMu_AM_Min'             :  1100.0*MeV
        , 'PiPiMu_AM_Min'           :  400.0*MeV
        , 'PiPiPiMu_AM_Min'         :  530.0*MeV
        , 'PiKKMu_AM_Min'           :  1240.0*MeV
        , 'EtaPiMu_AM_Min'          :  800.0*MeV
        , 'DpSL_AM_Max'             :  2080.0*MeV #large enough to cover D_s+
        , 'D0SL_AM_Max'             :  1960.0*MeV 

        #For B mesons
        , 'B0SL_M_Min': 2800*MeV
        , 'B0SL_M_Max': 5600*MeV #large enough to cover B_s0
        , 'B0_M_Min': 5100*MeV
        , 'B0_M_Max': 5600*MeV #large enough to cover B_s0

        , 'BpSL_M_Min': 3000*MeV
        , 'BpSL_M_Max': 5700*MeV 
        , 'Bp_M_Min': 5100*MeV
        , 'Bp_M_Max': 5500*MeV 

        , 'B_AP_Min': 20*GeV 
        , 'B_APT_Min': 1.5*GeV 
        , 'B_VCHI2_Max': 30 
        , 'B_BPVVDCHI2_Min': 16 
        , 'B_BPVDIRA_Min': 0.95 
      } ## end of 'CONFIG' 
}  ## end of default_config

#-------------------------------------------------------------------------------------------------------------
class StrippingCharmedMesonSL(LineBuilder) : 
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config
       
        GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                     "Preambulo": ["from LoKiTracks.decorators import *"]}
        
        ##########################################################################
        ## Basic particles: mu, K, pi, eta
        ##########################################################################
        self.selMuon = Selection( "SelMufor" + name,
                Algorithm = self._muonFilter("Mufor"+name),
                RequiredSelections = [StdLooseMuons])

        self.selKaon = Selection( "SelKfor" + name,
                Algorithm = self._kaonFilter("Kfor"+name),
                RequiredSelections = [StdAllLooseKaons])

        self.selPion = Selection( "SelPifor" + name,
                Algorithm = self._pionFilter("Pifor"+name),
                RequiredSelections = [StdAllLoosePions])

        self.makeEta()
        # then use self.Eta2PiPiPi0 and self.Eta2PiPiGamma

        ##########################################################################
        ## Charmed Meson SL lines
        ##########################################################################
        self.DmesonSLList = self.makeDmesonSL()


    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    # Sub Function
    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    def _muonFilter( self , _name):
        _code = "  (TRCHI2DOF< %(TRCHI2DOFMax)s)"\
                "& (PT>%(MuonPT)s) & (P>%(MuonP)s)"\
                "& (TRGHOSTPROB< %(GhostProb)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MuonIPCHI2)s)"\
                "& (PIDmu > %(MuonPIDmu)s) & ISMUON" % self.config
        _mu = FilterDesktop(Code = _code )
        return _mu

    def _pionFilter( self , _name):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (P>%(PionP)s) & (PT > %(PionPT)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PIDK< %(PionPIDK)s) "\
                "& (HASRICH)&(PROBNNpi > %(ProbNNpi)s) " % self.config
        _pi = FilterDesktop(Code = _code )
        return _pi

    def _kaonFilter( self , _name ):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (P>%(KaonP)s) & (PT > %(KaonPT)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PIDK> %(KaonPIDK)s) & (PROBNNpi < %(ProbNNpiMax)s)"\
                "& (PROBNNk > %(ProbNNk)s) "\
                "& (HASRICH)&(PIDK-PIDpi>%(Kaon_PIDKPIDpi_Min)s) "% self.config
        _ka = FilterDesktop(Code = _code )
        return _ka

    def makeB0Line(self, line_name, prescale, charmed_meson, is_NC = False):
        comb_cut = "(ADOCA(2,3) < %(Dp_ADOCAMAX_Max)s) & (ADOCA(2,4) < %(Dp_ADOCAMAX_Max)s)"\
                   "& (AP > %(B_AP_Min)s)"\
                   "& (APT > %(B_APT_Min)s)" % self.config
        vertex_cut  = "(VFASPF(VCHI2) < %(B_VCHI2_Max)s)" \
                      "& (BPVVDCHI2>%(B_BPVVDCHI2_Min)s)" \
                      "& (BPVDIRA>%(B_BPVDIRA_Min)s)" % self.config

        if is_NC :
            comb_cut = comb_cut + "& in_range(%(B0_M_Min)s - 200*MeV, AM, %(B0_M_Max)s + 200*MeV)" % self.config
            vertex_cut = vertex_cut  + "& in_range(%(B0_M_Min)s , M, %(B0_M_Max)s )" % self.config
        else:
            comb_cut = comb_cut  + "& in_range(%(B0SL_M_Min)s - 200*MeV, AM, %(B0SL_M_Max)s + 200*MeV)" % self.config
            vertex_cut = vertex_cut  + "& in_range(%(B0SL_M_Min)s , M, %(B0SL_M_Max)s )" % self.config

        B0 = self.createCombinationSel(OutputList = "B02DPiPiPi_" + line_name,
                DecayDescriptor = "[B~0 -> D+ pi+ pi- pi-]cc",
                DaughterLists   = [charmed_meson, self.selPion],
                PreVertexCuts   = comb_cut,
                PostVertexCuts  = vertex_cut )
        B0Line = StrippingLine( line_name, 
                prescale = prescale,
                selection = B0, 
                EnableFlavourTagging = False )
        return B0Line

    def makeBpLine(self, line_name, prescale, charmed_meson, is_NC = False):
        comb_cut = "(ADOCA(2,3) < %(Dp_ADOCAMAX_Max)s) & (ADOCA(2,4) < %(Dp_ADOCAMAX_Max)s)"\
                   "& (AP > %(B_AP_Min)s)"\
                   "& (APT > %(B_APT_Min)s)" % self.config
        vertex_cut  = "(VFASPF(VCHI2) < %(B_VCHI2_Max)s)" \
                      "& (BPVVDCHI2>%(B_BPVVDCHI2_Min)s)" \
                      "& (BPVDIRA>%(B_BPVDIRA_Min)s)" % self.config

        if is_NC :
            comb_cut = comb_cut + "& in_range(%(Bp_M_Min)s - 200*MeV, AM, %(Bp_M_Max)s + 200*MeV)" % self.config
            vertex_cut = vertex_cut  + "& in_range(%(Bp_M_Min)s , M, %(Bp_M_Max)s )" % self.config
        else:
            comb_cut = comb_cut  + "& in_range(%(BpSL_M_Min)s - 200*MeV, AM, %(BpSL_M_Max)s + 200*MeV)" % self.config
            vertex_cut = vertex_cut  + "& in_range(%(BpSL_M_Min)s , M, %(BpSL_M_Max)s )" % self.config

        Bp = self.createCombinationSel(OutputList = "B2D0PiPiPi_" + line_name,
                DecayDescriptor = ["B- -> D0 pi+ pi- pi-", "B- -> D~0 pi+ pi- pi-", "B- -> D0 pi- pi+ pi+", "B- -> D~0 pi- pi+ pi+"],
                DaughterLists   = [charmed_meson, self.selPion],
                PreVertexCuts   = comb_cut,
                PostVertexCuts  = vertex_cut )
        BpLine = StrippingLine( line_name, 
                prescale = prescale,
                selection = Bp, 
                EnableFlavourTagging = False )
        return BpLine

    def makeB0WSLine(self, line_name, prescale, charmed_meson, is_NC = False):
        comb_cut = "(ADOCA(2,3) < %(Dp_ADOCAMAX_Max)s) & (ADOCA(2,4) < %(Dp_ADOCAMAX_Max)s)"\
                   "& (AP > %(B_AP_Min)s)"\
                   "& (APT > %(B_APT_Min)s)" % self.config
        vertex_cut  = "(VFASPF(VCHI2) < %(B_VCHI2_Max)s)" \
                      "& (BPVVDCHI2>%(B_BPVVDCHI2_Min)s)" \
                      "& (BPVDIRA>%(B_BPVDIRA_Min)s)" % self.config
        if is_NC :
            comb_cut = comb_cut + "& in_range(%(B0_M_Min)s - 200*MeV, AM, %(B0_M_Max)s + 200*MeV)" % self.config
            vertex_cut = vertex_cut  + "& in_range(%(B0_M_Min)s , M, %(B0_M_Max)s )" % self.config
        else:
            comb_cut = comb_cut  + "& in_range(%(B0SL_M_Min)s - 200*MeV, AM, %(B0SL_M_Max)s + 200*MeV)" % self.config
            vertex_cut = vertex_cut  + "& in_range(%(B0SL_M_Min)s , M, %(B0SL_M_Max)s )" % self.config

        WS_descriptors=[
            "B0 -> D+ pi+ pi+ pi+",
            "B0 -> D- pi- pi- pi-",
            "B0 -> D+ pi- pi- pi-",
            "B0 -> D- pi+ pi+ pi+",
            "B0 -> D+ pi+ pi+ pi-",
            "B0 -> D- pi- pi- pi+",
        ]

        B0 = self.createCombinationSel(OutputList = "B02DPiPiPi_" + line_name,
                DecayDescriptor = WS_descriptors,
                DaughterLists   = [charmed_meson, self.selPion],
                PreVertexCuts   = comb_cut,
                PostVertexCuts  = vertex_cut )
        B0Line = StrippingLine( line_name, 
                prescale = prescale,
                selection = B0, 
                EnableFlavourTagging = False )
        return B0Line

    def makeBpWSLine(self, line_name, prescale, charmed_meson, is_NC = False):
        comb_cut = "(ADOCA(2,3) < %(Dp_ADOCAMAX_Max)s) & (ADOCA(2,4) < %(Dp_ADOCAMAX_Max)s)"\
                   "& (AP > %(B_AP_Min)s)"\
                   "& (APT > %(B_APT_Min)s)" % self.config
        vertex_cut  = "(VFASPF(VCHI2) < %(B_VCHI2_Max)s)" \
                      "& (BPVVDCHI2>%(B_BPVVDCHI2_Min)s)" \
                      "& (BPVDIRA>%(B_BPVDIRA_Min)s)" % self.config
        if is_NC :
            comb_cut = comb_cut + "& in_range(%(Bp_M_Min)s - 200*MeV, AM, %(Bp_M_Max)s + 200*MeV)" % self.config
            vertex_cut = vertex_cut  + "& in_range(%(Bp_M_Min)s , M, %(Bp_M_Max)s )" % self.config
        else:
            comb_cut = comb_cut  + "& in_range(%(BpSL_M_Min)s - 200*MeV, AM, %(BpSL_M_Max)s + 200*MeV)" % self.config
            vertex_cut = vertex_cut  + "& in_range(%(BpSL_M_Min)s , M, %(BpSL_M_Max)s )" % self.config

        WS_descriptors=[
            "B- -> D0 pi+ pi+ pi+",
            "B- -> D~0 pi- pi- pi-",
            "B- -> D0 pi- pi- pi-",
            "B- -> D~0 pi+ pi+ pi+",
        ]

        Bp = self.createCombinationSel(OutputList = "B2D0PiPiPi_" + line_name,
                DecayDescriptor = WS_descriptors,
                DaughterLists   = [charmed_meson, self.selPion],
                PreVertexCuts   = comb_cut,
                PostVertexCuts  = vertex_cut )
        BpLine = StrippingLine( line_name, 
                prescale = prescale,
                selection = Bp, 
                EnableFlavourTagging = False )
        return BpLine
        

    ##------------------------------------------------------------------------------------------
    ##  -------------------- Begin to DpSL  ------------
    def makeDmesonSL( self ):

        _strCutCombfor = "(AMINCHILD(MIPCHI2DV(PRIMARY))>%(Dp_Daug_1of3_MIPCHI2DV_Min)s)" \
                    "& (ADOCAMAX('')<%(Dp_ADOCAMAX_Max)s)" \
                    "& (AP>%(Dp_AP_Min)s)" \
                    "& (APT>%(Dp_APT_Min)s)" % self.config

        _strCutMothfor = "(VFASPF(VCHI2) < %(Dp_VCHI2_Max)s)" \
                      "& (BPVVDCHI2>%(Dp_BPVVDCHI2_Min)s)" \
                      "& (BPVDIRA>%(Dp_BPVDIRA_Min)s)" % self.config

        _strCutComb_Mass_Dp   = "(AM>0.9*%(Dp_ADMASS_Min)s) & (AM<1.1*%(Dp_ADMASS_Max)s)" % self.config
        _strCutMoth_Mass_Dp   = "(M>%(Dp_ADMASS_Min)s) & (M<%(Dp_ADMASS_Max)s)" % self.config
        _strCutComb_Mass_D0   = "(AM>0.9*%(D0_ADMASS_Min)s) & (AM<1.1*%(D0_ADMASS_Max)s)" % self.config
        _strCutMoth_Mass_D0   = "(M>%(D0_ADMASS_Min)s) & (M<%(D0_ADMASS_Max)s)" % self.config

        _strCutComb_Mass_D0_with_neutral   = "(AM>0.9*%(D0_ADMASS_Min)s) & (AM<1.1*%(D0_ADMASS_Max)s)" % self.config
        _strCutMoth_Mass_D0_with_neutral   = "(M>%(D0_ADMASS_Min)s - 100*MeV) & (M<%(D0_ADMASS_Max)s + 100*MeV)" % self.config
        
        _strCutComb_Dp   = _strCutCombfor + '&' + _strCutComb_Mass_Dp
        _strCutMoth_Dp   = _strCutMothfor + '&' + _strCutMoth_Mass_Dp
        _strCutComb_D0   = _strCutCombfor + '&' + _strCutComb_Mass_D0
        _strCutMoth_D0   = _strCutMothfor + '&' + _strCutMoth_Mass_D0

        _strCutComb_D0_with_neutral   = _strCutCombfor + '&' + _strCutComb_Mass_D0_with_neutral
        _strCutMoth_D0_with_neutral   = _strCutMothfor + '&' + _strCutMoth_Mass_D0_with_neutral
        
        _strCutComb_MassKKMuSL  = "(AM>%(KKMu_AM_Min)s)  & (AM<%(DpSL_AM_Max)s)" % self.config
        _strCutComb_MassPiPiMuSL  = "(AM>%(PiPiMu_AM_Min)s)  & (AM<%(DpSL_AM_Max)s)" % self.config
        _strCutComb_MassPiPiPiMuSL  = "(AM>%(PiPiPiMu_AM_Min)s)  & (AM<%(D0SL_AM_Max)s)" % self.config
        _strCutComb_MassPiKKMuSL  = "(AM>%(PiKKMu_AM_Min)s)  & (AM<%(D0SL_AM_Max)s)" % self.config
        _strCutComb_MassEtaPiMuSL  = "(AM>%(EtaPiMu_AM_Min)s)  & (AM<%(D0SL_AM_Max)s)" % self.config

        _strCutComb_KKMuSL  = _strCutCombfor + '&' + _strCutComb_MassKKMuSL 
        _strCutComb_PiPiMuSL  = _strCutCombfor + '&' + _strCutComb_MassPiPiMuSL 
        _strCutComb_PiPiPiMuSL  = _strCutCombfor + '&' + _strCutComb_MassPiPiPiMuSL 
        _strCutComb_PiKKMuSL  = _strCutCombfor + '&' + _strCutComb_MassPiKKMuSL 
        _strCutComb_EtaPiMuSL  = _strCutCombfor + '&' + _strCutComb_MassEtaPiMuSL 
        _strCutMothSL = _strCutMothfor

        ''' Stripping D+ -> K+ K- mu+ '''
        KKMuNu = self.createCombinationSel(OutputList = "D2KKMuNu" + self.name,
                DecayDescriptor = "[D+ -> K+ K- mu+]cc",
                DaughterLists   = [self.selKaon,self.selMuon],
                PreVertexCuts   = _strCutComb_KKMuSL,
                PostVertexCuts  = _strCutMothSL )

        self.registerLine(self.makeB0Line(
            line_name=self.name + "B02DPiPiPiD2KKMuNuFromBLine",
            prescale=self.config['signalPrescaleViaD2KKmunu'],
            charmed_meson=KKMuNu
            ))
        self.registerLine(self.makeB0WSLine(
            line_name=self.name + "B02DPiPiPiD2KKMuNuFromBWSLine",
            prescale=self.config['signalPrescaleViaD2KKmunu'],
            charmed_meson=KKMuNu
            ))

        ''' Stripping D+ -> pi+ pi- mu+ '''
        PiPiMuNu = self.createCombinationSel(OutputList = "D2PiPiMuNu" + self.name,
                DecayDescriptor = "[D+ -> pi+ pi- mu+]cc",
                DaughterLists   = [self.selPion,self.selMuon],
                PreVertexCuts   = _strCutComb_PiPiMuSL,
                PostVertexCuts  = _strCutMothSL )
        self.registerLine(self.makeB0Line(
            line_name=self.name + "B02DPiPiPiD2PiPiMuNuFromBLine",
            prescale=self.config['signalPrescaleViaD2pipimunu'],
            charmed_meson=PiPiMuNu
            ))
        self.registerLine(self.makeB0WSLine(
            line_name=self.name + "B02DPiPiPiD2PiPiMuNuFromBWSLine",
            prescale=self.config['signalPrescaleViaD2pipimunu'],
            charmed_meson=PiPiMuNu
            ))

        ''' Stripping D0 -> pi- pi+ pi- mu+ '''
        PiPiPiMuNu = self.createCombinationSel(OutputList = "D02PiPiPiMuNu" + self.name,
                DecayDescriptor = "[D0 -> pi- pi- pi+ mu+]cc",
                DaughterLists   = [self.selPion, self.selMuon],
                PreVertexCuts   = _strCutComb_PiPiPiMuSL,
                PostVertexCuts  = _strCutMothSL )

        self.registerLine(self.makeBpLine(
            line_name=self.name + "B2D0PiPiPiD02PiPiPiMuNuFromBLine",
            prescale=self.config['signalPrescaleViaD02pipipimunu'],
            charmed_meson=PiPiPiMuNu
            ))
        self.registerLine(self.makeBpWSLine(
            line_name=self.name + "B2D0PiPiPiD02PiPiPiMuNuFromBWSLine",
            prescale=self.config['signalPrescaleViaD02pipipimunu'],
            charmed_meson=PiPiPiMuNu
            ))

        ''' Stripping D0 -> pi- K+ K- mu+ '''
        PiKKMuNu = self.createCombinationSel(OutputList = "D02PiKKMuNu" + self.name,
                DecayDescriptor = "[D0 -> pi- K+ K- mu+]cc",
                DaughterLists   = [self.selPion, self.selKaon, self.selMuon],
                PreVertexCuts   = _strCutComb_PiKKMuSL,
                PostVertexCuts  = _strCutMothSL )

        self.registerLine(self.makeBpLine(
            line_name=self.name + "B2D0PiPiPiD02PiKKMuNuFromBLine",
            prescale=self.config['signalPrescaleViaD02KKpimunu'],
            charmed_meson=PiKKMuNu
            ))
        self.registerLine(self.makeBpWSLine(
            line_name=self.name + "B2D0PiPiPiD02PiKKMuNuFromBWSLine",
            prescale=self.config['signalPrescaleViaD02KKpimunu'],
            charmed_meson=PiKKMuNu
            ))

        ''' Stripping D0 -> eta pi- mu+, eta -> pi+ pi- pi0  '''
        EtaPiMuNuEta2PiPiPi0 = self.createCombinationSel(OutputList = "D02EtaPiMuNu_Eta2PiPi0" + self.name,
                DecayDescriptor = "[D0 -> eta pi- mu+]cc",
                DaughterLists   = [self.Eta2PiPiPi0, self.selPion, self.selMuon],
                PreVertexCuts   = _strCutComb_EtaPiMuSL,
                PostVertexCuts  = _strCutMothSL )

        self.registerLine(self.makeBpLine(
            line_name=self.name + "B2D0PiPiPiD02EtaPiMuNuEta2PiPiPi0FromBLine",
            prescale=self.config['signalPrescaleViaD02etapimunu'],
            charmed_meson=EtaPiMuNuEta2PiPiPi0
            ))
        self.registerLine(self.makeBpWSLine(
            line_name=self.name + "B2D0PiPiPiD02EtaPiMuNuEta2PiPiPi0FromBWSLine",
            prescale=self.config['signalPrescaleViaD02etapimunu'],
            charmed_meson=EtaPiMuNuEta2PiPiPi0
            ))

        ''' Stripping D0 -> eta pi- mu+, eta -> pi+ pi- gamma '''
        EtaPiMuNuEta2PiPiGamma = self.createCombinationSel(OutputList = "D02EtaPiMuNu_Eta2PiPiGamma" + self.name,
                DecayDescriptor = "[D0 -> eta pi- mu+]cc",
                DaughterLists   = [self.Eta2PiPiGamma, self.selPion, self.selMuon],
                PreVertexCuts   = _strCutComb_EtaPiMuSL,
                PostVertexCuts  = _strCutMothSL )

        self.registerLine(self.makeBpLine(
            line_name=self.name + "B2D0PiPiPiD02EtaPiMuNuEta2PiPiGammaFromBLine",
            prescale=self.config['signalPrescaleViaD02etapimunu'],
            charmed_meson=EtaPiMuNuEta2PiPiGamma
            ))
        self.registerLine(self.makeBpWSLine(
            line_name=self.name + "B2D0PiPiPiD02EtaPiMuNuEta2PiPiGammaFromBWSLine",
            prescale=self.config['signalPrescaleViaD02etapimunu'],
            charmed_meson=EtaPiMuNuEta2PiPiGamma
            ))

        ''' ---------------------- '''
        ''' Normalization channels '''
        ''' ---------------------- '''
        ''' NC Stripping D+ -> K+ K- pi+ '''
        D2KKPiNC = self.createCombinationSel(OutputList = "D2KKPiNC" + self.name,
                DecayDescriptor = "[D+ -> K+ K- pi+]cc",
                DaughterLists   = [self.selKaon, self.selPion],
                PreVertexCuts   = _strCutComb_Dp,
                PostVertexCuts  = _strCutMoth_Dp )

        self.registerLine(self.makeB0Line(
            line_name=self.name + "B02DPiPiPiD2KKPiNCFromBLine",
            prescale=self.config['controlPrescaleViaD2KKpi'],
            charmed_meson=D2KKPiNC,
            is_NC=True
            ))

        ''' NC Stripping D+ -> pi+ pi+ pi-'''
        D2PiPiPiNC = self.createCombinationSel(OutputList = "D2PiPiPiNC" + self.name,
                DecayDescriptor = "[D+ -> pi+ pi+ pi-]cc",
                DaughterLists   = [self.selPion],
                PreVertexCuts   = _strCutComb_Dp,
                PostVertexCuts  = _strCutMoth_Dp )

        self.registerLine(self.makeB0Line(
            line_name=self.name + "B02DPiPiPiD2PiPiPiNCFromBLine",
            prescale=self.config['controlPrescaleViaD2pipipi'],
            charmed_meson=D2PiPiPiNC,
            is_NC=True
            ))

        ''' NC Stripping D0 -> pi+ pi+ pi- pi-'''
        D2PiPiPiPiNC = self.createCombinationSel(OutputList = "D02PiPiPiPiNC" + self.name,
                DecayDescriptor = "[D0 -> pi+ pi+ pi- pi-]cc",
                DaughterLists   = [self.selPion],
                PreVertexCuts   = _strCutComb_D0,
                PostVertexCuts  = _strCutMoth_D0 )

        self.registerLine(self.makeBpLine(
            line_name=self.name + "B2D0PiPiPiD02PiPiPiPiNCFromBLine",
            prescale=self.config['controlPrescaleViaD02pipipipi'],
            charmed_meson=D2PiPiPiPiNC,
            is_NC=True
            ))


        ''' NC Stripping D0 -> K+ K- pi+ pi-'''
        D2KKPiPiNC = self.createCombinationSel(OutputList = "D02KKPiPiNC" + self.name,
                DecayDescriptor = "[D0 ->  K+ K- pi+ pi-]cc",
                DaughterLists   = [self.selKaon, self.selPion],
                PreVertexCuts   = _strCutComb_D0,
                PostVertexCuts  = _strCutMoth_D0 )

        self.registerLine(self.makeBpLine(
            line_name=self.name + "B2D0PiPiPiD02PiPiKKNCFromBLine",
            prescale=self.config['controlPrescaleViaD02KKpipi'],
            charmed_meson=D2KKPiPiNC,
            is_NC=True
            ))

        '''NC Stripping D0 -> eta pi+ pi-, eta -> pi+ pi- pi0 '''
        EtaPiPiEta2PiPiPi0NC = self.createCombinationSel(OutputList = "D02EtaPiPi_Eta2PiPiPi0NC" + self.name,
                DecayDescriptor = "[D0 -> eta pi+ pi-]cc",
                DaughterLists   = [self.Eta2PiPiPi0, self.selPion],
                PreVertexCuts   = _strCutComb_D0_with_neutral,
                PostVertexCuts  = _strCutMoth_D0_with_neutral )

        self.registerLine(self.makeBpLine(
            line_name=self.name + "B2D0PiPiPiD02EtaPiPiEta2PiPiPi0NCFromBLine",
            prescale=self.config['controlPrescaleViaD02etapipi'],
            charmed_meson=EtaPiPiEta2PiPiPi0NC,
            is_NC=True
            ))

        '''NC Stripping D0 -> eta pi+ pi-, eta -> pi+ pi- gamma '''
        EtaPiPiEta2PiPiGammaNC = self.createCombinationSel(OutputList = "D02EtaPiPi_Eta2PiPiGammaNC" + self.name,
                DecayDescriptor = "[D0 -> eta pi+ pi-]cc",
                DaughterLists   = [self.Eta2PiPiGamma, self.selPion],
                PreVertexCuts   = _strCutComb_D0_with_neutral,
                PostVertexCuts  = _strCutMoth_D0_with_neutral )

        self.registerLine(self.makeBpLine(
            line_name=self.name + "B2D0PiPiPiD02EtaPiPiEta2PiPiGammaNCFromBLine",
            prescale=self.config['controlPrescaleViaD02etapipi'],
            charmed_meson=EtaPiPiEta2PiPiGammaNC,
            is_NC=True
            ))

    ##  --------------------  End of DpSL  ------------
    ##------------------------------------------------------------------------------------------
    def makeEta(self):
        #get standard containers
        self.Pi0List     = DataOnDemand(Location = "Phys/StdLoosePi02gg/Particles")
        self.GammaList   = DataOnDemand(Location = "Phys/StdLooseAllPhotons/Particles")
        self.LoosePionList    = DataOnDemand(Location = "Phys/StdLoosePions/Particles")

        #clean neutrals
        _tightpi0_cut   = "(CHILD(CL,1)>0.05) & (CHILD(CL,2)>0.05) & (PT>1*GeV)"
        _tightgamma_cut = "(CL>0.05) & (PT>500*MeV)"

        #pipi vertex cuts
        _pipix_cut      = "(BPVVDZ>0) & (VFASPF(VCHI2)<9) & (BPVDIRA>0.95) & (BPVVDCHI2>25)"

        #pipix0 cuts
        _eta_cut        = "(ADAMASS('eta')<100*MeV)       & (APT>1500*MeV)"

        #filter standard containers
        self.TightPi0List   = self.createSubSel( OutputList = self.name + '_TightPi0',   InputList  = self.Pi0List,     Cuts = _tightpi0_cut)
        self.TightGammaList = self.createSubSel( OutputList = self.name + '_TightGamma', InputList  = self.GammaList,   Cuts = _tightgamma_cut)

        #-------------

        self.Eta2PiPiPi0 = self.createCombinationSel( OutputList      = self.name + "_Eta2PiPiPi0",
                                                      DecayDescriptor = "eta -> pi+ pi- pi0",
                                                      DaughterLists   = [ self.LoosePionList, self.TightPi0List],
                                                      PreVertexCuts   = _eta_cut,
                                                      PostVertexCuts  = _pipix_cut)
        #-------------

        self.Eta2PiPiGamma = self.createCombinationSel( OutputList      = self.name + "_Eta2PiPiGamma",
                                                        DecayDescriptor = "eta -> pi+ pi- gamma",
                                                        DaughterLists   = [ self.LoosePionList, self.TightGammaList],
                                                        PreVertexCuts   = _eta_cut,
                                                        PostVertexCuts  = _pipix_cut )

    ##########################################################################
    ## Basic Function
    ##########################################################################
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                Algorithm = filter,
                RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
            DecayDescriptor,
            DaughterLists,
            DaughterCuts = {} ,
            PreVertexCuts = "ALL",
            PostVertexCuts = "ALL") :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        if type(DecayDescriptor) is list:
            combiner = CombineParticles( DecayDescriptors = DecayDescriptor,
                    DaughtersCuts = DaughterCuts,
                    MotherCut = PostVertexCuts,
                    CombinationCut = PreVertexCuts,
                    ReFitPVs = True)
            return Selection ( OutputList,
                    Algorithm = combiner,
                    RequiredSelections = DaughterLists)
        else:
            combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                    DaughtersCuts = DaughterCuts,
                    MotherCut = PostVertexCuts,
                    CombinationCut = PreVertexCuts,
                    ReFitPVs = True)
            return Selection ( OutputList,
                    Algorithm = combiner,
                    RequiredSelections = DaughterLists)
    #print "DEBUG here is :",__file__,sys._getframe().f_lineno 
