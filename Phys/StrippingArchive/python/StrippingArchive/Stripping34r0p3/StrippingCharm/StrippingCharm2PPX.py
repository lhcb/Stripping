###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
    Exotic six-quarks (hexa-quarks) with charm or strange: [c(s)u(d)][ud][ud] decaying to two same-sign protons
    - Hc++ -> pp (exch , Cabibbo-suppr.)
    - Hc++ -> ppK-pi+
    - Hc+ -> ppK- (exch)
    - Hc+ -> ppK-pi+pi-
    - Hs+ -> pppi- : LLL and DDD

 for 2023 Restripping update with following modes:
    - Hcc++/+ -> ppKs, ppKspi-, Lamppi-, Lamppi+pi-
    - Hcs+ -> ppK-K-pi+, pK-pi+Lam, LamLampi+, ppKsK-
    - Pc -> pK+pi-pi-, pKspi-
    - Ps -> ppi+pi- : LLL and DDD

    + add modes Lc->pKpi, Lam->ppi with 0.01 prescale for reference
"""


__author__ = ['Ivan Polyakov']
__date__ = ['17/11/2016']

__all__ = (
    'default_config',
    'StrippingCharm2PPXConf' ##???
)

from GaudiKernel.SystemOfUnits import MeV, GeV, mm, mrad
from Configurables import CombineParticles, FilterDesktop
#from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop, DaVinci__N3BodyDecays
from StandardParticles import (
    StdAllNoPIDsProtons,
    StdAllNoPIDsKaons,
    StdAllNoPIDsPions,
    StdNoPIDsDownProtons,
    StdNoPIDsDownPions,
    StdLooseLambdaLL,
    StdLooseLambdaDD,
    StdLooseKsLL,
    StdLooseKsDD,
)
from PhysSelPython.Wrappers import Selection
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine

default_config = {
    'NAME': 'Charm2PPX',
    'WGs': ['Charm'],
    'BUILDERTYPE': 'StrippingCharm2PPXConf',
    'STREAMS': ['Charm'],
    'CONFIG': {
        'Daug_All_CloneDist_MIN': 5000,
        'Daug_All_TrChostProb_MAX': 0.5,
        # Minimum transverse momentum all daughters must satisfy
        'Daug_All_PT_MIN': 200.0*MeV,
        # Minimum transverse momentum at least 1 daughter must satisfy
        'Daug_1ofAll_PT_MIN': 700*MeV, # 500.0*MeV, # 1000
        'HsDaug_1ofAll_PT_MIN': -500.0*MeV, # 1000
        # Minimum best primary vertex IP chi^2 all daughters must satisfy
        'Daug_All_BPVIPCHI2_MIN': 4.0,
        'Daug_All_MIPCHI2_MIN': 4.0,
        # Minimum best PV IP chi^2 at least 1 daughter must satisfy
        'Daug_1ofAll_BPVIPCHI2_MIN': 9.0, # 9.0
        'HsDaug_1ofAll_BPVIPCHI2_MIN': 9.0, # 9.0
        'HsDownDaug_1ofAll_BPVIPCHI2_MIN': -9.0, #
        # Minimum daughter momentum
        'Daug_P_MIN': 3.2*GeV,
        'Proton_P_MIN': 10.0*GeV,
        'Proton_PT_MIN': 300.0*MeV, # -250*MeV,
        # Maximum daughter momentum
        'Daug_P_MAX': 100.0*GeV,
        # Minimum daughter pseudorapidity
        'Daug_ETA_MIN': 2.0,
        # Maximum daughter pseudorapidity
        'Daug_ETA_MAX': 4.9,
        # Minimum daughter proton ProbNN
        'Proton_ProbNN_MIN': 0.1,
        'Proton_ProbNNpi_MAX': 0.55, #0.5
        'Proton_ProbNNk_MAX': 0.8, #0.8
        'Proton_1ofAll_ProbNN_MIN': 0.5,
        'HsProton_1ofAll_ProbNN_MIN': 0.5,
        # Minimum daughter kaon ProbNN
        'Kaon_ProbNN_MIN': 0.1,
        # Maximum daughter pion ProbNN
        'Pion_ProbNN_MIN': 0.1,

        # Hc maximum mass
        'Hc_MASS_MAX': 3500*MeV , # 3350*MeV,
        'Hcs_MASS_MAX': 3650*MeV , 
        'Hs_MASS_MAX': 2125*MeV,
        'Pc_MASS_MAX': 3100*MeV , 
        'Ps_MASS_MAX': 1650*MeV , 
        'Lc_MASS_MAX': 2350*MeV , 
        'Lam_MASS_MAX': 1170*MeV , 
        # Maximum distance of closest approach of daughters
        'Comb_ADOCACHI2_MAX': 16,
        'HsComb_ADOCACHI2_MAX': 16,
        # Maximum Hc vertex chi^2 per vertex fit DoF
        'Hc_VCHI2VDOF_MAX': 25.0,
        'Hs_VCHI2VDOF_MAX': 25.0,
        # Maximum angle between Hc momentum and Hc direction of flight
        #'Hc_BPVIPCHI2_MAX': 16,
        #'Hs_BPVIPCHI2_MAX': 16,
        # Primary vertex displacement requirement, that the Hc is some sigma
        # away from the PV and it has a minimum flight time
        'Hc_BPVVDCHI2_MIN': 9.0,
        'Hc_BPVCTAU_MIN': 0.05*mm, # 0.03*mm,
        'Hs_BPVVDCHI2_MIN': 9.0,
        'HsDown_BPVVDCHI2_MIN': 4.0,
        'Hs_BPVCTAU_MIN': 0.1*mm,
        # Hc minimum transverse momenta
        'Hc_PT_MIN': 1000*MeV,
        'Hs_PT_MIN': -1000*MeV,
        # HLT filters, only process events firing triggers matching the RegEx
        'Hlt1Filter': None,
        'Hlt2Filter': None,

        # Fraction of candidates to randomly throw away before stripping
        'PrescaleHc2PP': 1.0,
        'PrescaleHc2PPKPi': 1.0,
        'PrescaleHc2PPK': 1.0,
        'PrescaleHc2PPKPiPi': 1.0,
        'PrescaleHs2PPPi': 1.0,
        #
        'PrescaleHc2PPKs': 1.0,
        'PrescaleHc2PPKsPi': 1.0,
        'PrescaleHc2LamPPi': 1.0,
        'PrescaleHc2LamPPiPi': 1.0,
        'PrescaleHcs2PPKKPi': 1.0,
        'PrescaleHcs2PPKsK': 1.0,
        'PrescaleHcs2LamPKPi': 1.0,
        'PrescaleHcs2LamLamPi': 1.0,
        'PrescalePc2PKPiPi': 0.1,
        'PrescalePc2PKsPi': 0.1,
        'PrescalePs2PPiPi': 0.1,
        #
        'PrescaleLc2PKPi' : 0.01,
        'PrescaleL2PPi' : 0.01,

        # Fraction of candidates to randomly throw after before stripping
        'PostscaleHc2PP': 1.0,
        'PostscaleHc2PPKPi': 1.0,
        'PostscaleHc2PPK': 1.0,
        'PostscaleHc2PPKPiPi': 1.0,
        'PostscaleHs2PPPi': 1.0,
        #
        'PostscaleHc2PPKs': 1.0,
        'PostscaleHc2PPKsPi': 1.0,
        'PostscaleHc2LamPPi': 1.0,
        'PostscaleHc2LamPPiPi': 1.0,
        'PostscaleHcs2PPKKPi': 1.0,
        'PostscaleHcs2PPKsK': 1.0,
        'PostscaleHcs2LamPKPi': 1.0,
        'PostscaleHcs2LamLamPi': 1.0,
        'PostscalePc2PKPiPi': 1.0,
        'PostscalePc2PKsPi': 1.0,
        'PostscalePs2PPiPi': 1.0,
        #
        'PostscaleLc2PKPi' : 1.0,
        'PostscaleL2PPi' : 1.0,
    }
}

class StrippingCharm2PPXConf(LineBuilder):
    """Creates LineBuilder object containing the stripping lines."""
    # Allowed configuration keys
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        """Initialise this LineBuilder instance."""
        self.name = name
        self.config = config
        LineBuilder.__init__(self, name, config)

        # Line names
        hc2pp_name = '{0}Hc2PP'.format(name)
        hc2ppkpi_name = '{0}Hc2PPKPi'.format(name)
        hc2ppk_name = '{0}Hc2PPK'.format(name)
        hc2ppkpipi_name = '{0}Hc2PPKPiPi'.format(name)
        
        hs2pppi_name = '{0}Hs2PPPi'.format(name)
        hs2pppiDown_name = '{0}Hs2PPPiDown'.format(name)

        #
        hc2ppKs_name = '{0}Hc2PPKs'.format(name)
        hc2ppKspi_name = '{0}Hc2PPKsPi'.format(name)
        hc2Lppi_name = '{0}Hc2LamPPi'.format(name)
        hc2Lppipi_name = '{0}Hc2LamPPiPi'.format(name)

        hcs2ppKKpi_name = '{0}Hcs2PPKKPi'.format(name)
        hcs2ppKsK_name = '{0}Hcs2PPKsK'.format(name)
        hcs2LpKpi_name = '{0}Hcs2LamPKPi'.format(name)
        hcs2LLpi_name = '{0}Hcs2LamLamPi'.format(name)

        pc2pKpipi_name = '{0}Pc2PKPiPi'.format(name)
        pc2pKspi_name = '{0}Pc2PKsPi'.format(name)
        ps2ppipi_name = '{0}Ps2PPiPi'.format(name)
        ps2ppipiDown_name = '{0}Ps2PPiPiDown'.format(name)

        # reference
        lc2pKpi_name = '{0}Lc2PKPiRef'.format(name)
        l2ppi_name = '{0}L2PPiRef'.format(name)

        # Build pion, kaon, and proton cut strings
        daugCuts = (
            '(PT > {0[Daug_All_PT_MIN]})'
            ' & (CLONEDIST > {0[Daug_All_CloneDist_MIN]} )'
            ' & (TRGHOSTPROB < {0[Daug_All_TrChostProb_MAX]})'
            ' & (MIPCHI2DV() > {0[Daug_All_MIPCHI2_MIN]})'
        ).format(self.config)
        pidFiducialCuts = (
            '(in_range({0[Daug_P_MIN]}, P, {0[Daug_P_MAX]}))'
            '& (in_range({0[Daug_ETA_MIN]}, ETA, {0[Daug_ETA_MAX]}))'
            ' & HASRICH'
        ).format(self.config)
        protonPIDCuts = (
            pidFiducialCuts +
            '& (PROBNNp > {0[Proton_ProbNN_MIN]})'
            '& (PROBNNpi < {0[Proton_ProbNNpi_MAX]})'
            '& (PROBNNk < {0[Proton_ProbNNk_MAX]})'
            '& (P > {0[Proton_P_MIN]})'
            '& (PT > {0[Proton_PT_MIN]})'
        ).format(self.config)
        kaonPIDCuts = (
            pidFiducialCuts +
            '& (PROBNNk > {0[Kaon_ProbNN_MIN]})'
        ).format(self.config)
        pionPIDCuts = (
            pidFiducialCuts +
            '& (PROBNNpi > {0[Pion_ProbNN_MIN]})'
        ).format(self.config)

        pionCuts = '{0} & {1}'.format(daugCuts, pionPIDCuts)
        kaonCuts = '{0} & {1}'.format(daugCuts, kaonPIDCuts)
        protonCuts = '{0} & {1}'.format(daugCuts, protonPIDCuts)

        # Filter StdAllNoPIDs particles
        self.inPions = Selection(
            'PionsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                'FilterPionsFor{0}'.format(name),
                Code=pionCuts
            ),
            RequiredSelections=[StdAllNoPIDsPions]
        )
        self.inKaons = Selection(
            'KaonsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                'FilterKaonsFor{0}'.format(name),
                Code=kaonCuts
            ),
            RequiredSelections=[StdAllNoPIDsKaons]
        )
        self.inProtons = Selection(
            'ProtonsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                'FilterProtonsFor{0}'.format(name),
                Code=protonCuts
            ),
            RequiredSelections=[StdAllNoPIDsProtons]
        )
        self.inDownPions = Selection(
            'DownPionsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                'FilterDownPionsFor{0}'.format(name),
                Code=pionCuts
            ),
            RequiredSelections=[StdNoPIDsDownPions]
        )
        self.inDownProtons = Selection(
            'DownProtonsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                'FilterDownProtonsFor{0}'.format(name),
                Code=protonCuts
            ),
            RequiredSelections=[StdNoPIDsDownProtons]
        )

        self.selHc2PP = self.makeHc2PPX(
            name=hc2pp_name,
            inputSel=[self.inProtons],
            decDescriptors=['[Sigma_c++ -> p+ p+]cc'],
            mass_max = self.config["Hc_MASS_MAX"]
        )

        self.selHc2PPKPi = self.makeHc2PPX(
            name=hc2ppkpi_name,
            inputSel=[self.inProtons, self.inKaons, self.inPions ],
            decDescriptors=['[Sigma_c++ -> p+ p+ K- pi+]cc', 
                            '[Sigma_c++ -> p+ p+ K+ pi-]cc',  
                            '[Sigma_c++ -> p+ p+ K- pi-]cc',  
                            '[Sigma_c++ -> p+ p+ K+ pi+]cc',  
                        ],
            mass_max = self.config["Hc_MASS_MAX"]
        )

        self.selHc2PPK = self.makeHc2PPX(
            name=hc2ppk_name,
            inputSel=[self.inProtons, self.inKaons ],
            decDescriptors=['[Sigma_c+ -> p+ p+ K-]cc',
                            '[Sigma_c+ -> p+ p+ K+]cc',
                        ],
            mass_max = self.config["Hc_MASS_MAX"]
        )

        self.selHc2PPKPiPi = self.makeHc2PPX(
            name=hc2ppkpipi_name,
            inputSel=[self.inProtons, self.inKaons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> p+ p+ K- pi+ pi-]cc',
                            '[Sigma_c+ -> p+ p+ K+ pi+ pi-]cc',
                            '[Sigma_c+ -> p+ p+ K- pi+ pi+]cc',
                            '[Sigma_c+ -> p+ p+ K+ pi- pi-]cc',
                        ],
            mass_max = self.config["Hc_MASS_MAX"]
        )

        self.selHs2PPPi = self.makeHs2PPX(
            name=hs2pppi_name,
            inputSel=[self.inProtons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> p+ p+ pi-]cc',
                            '[Sigma_c+ -> p+ p+ pi+]cc',
                        ],
            mass_max = self.config["Hs_MASS_MAX"],
            mipchi2_cut = config['HsDaug_1ofAll_BPVIPCHI2_MIN'],
            vdchi2_cut = config['Hs_BPVVDCHI2_MIN'],
        )

        self.selHs2PPPiDown = self.makeHs2PPX(
            name=hs2pppiDown_name,
            inputSel=[self.inDownProtons, self.inDownPions ],
            decDescriptors=['[Sigma_c+ -> p+ p+ pi-]cc',
                            '[Sigma_c+ -> p+ p+ pi+]cc',
                        ],
            mass_max = self.config["Hs_MASS_MAX"],
            mipchi2_cut = config['HsDownDaug_1ofAll_BPVIPCHI2_MIN'],
            vdchi2_cut = config['HsDown_BPVVDCHI2_MIN'],
        )

        ##
        self.selHc2PPKs_KsLL = self.makeHc2PPX(
            name=hc2ppKs_name + "_KsLL",
            inputSel=[StdLooseKsLL, self.inProtons ],
            decDescriptors=['[Sigma_c+ -> p+ p+ KS0]cc'],
            mass_max = self.config["Hc_MASS_MAX"],
        )

        self.selHc2PPKs_KsDD = self.makeHc2PPX(
            name=hc2ppKs_name + "_KsDD",
            inputSel=[StdLooseKsDD, self.inProtons ],
            decDescriptors=['[Sigma_c+ -> p+ p+ KS0]cc'],
            mass_max = self.config["Hc_MASS_MAX"],
        )

        self.selHc2PPKsPi_KsLL = self.makeHc2PPX(
            name=hc2ppKspi_name + "_KsLL",
            inputSel=[StdLooseKsLL, self.inProtons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> p+ p+ KS0 pi-]cc',
                            '[Sigma_c+ -> p+ p+ KS0 pi+]cc' ],
            mass_max = self.config["Hc_MASS_MAX"],
        )

        self.selHc2PPKsPi_KsDD = self.makeHc2PPX(
            name=hc2ppKspi_name + "_KsDD",
            inputSel=[StdLooseKsDD, self.inProtons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> p+ p+ KS0 pi-]cc',
                            '[Sigma_c+ -> p+ p+ KS0 pi+]cc' ],
            mass_max = self.config["Hc_MASS_MAX"],
        )

        self.selHc2LamPPi_LamLL = self.makeHc2PPX(
            name=hc2Lppi_name + "_LamLL",
            inputSel=[StdLooseLambdaLL, self.inProtons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> Lambda0 p+ pi-]cc',
                            '[Sigma_c+ -> Lambda0 p+ pi+]cc' ],
            mass_max = self.config["Hc_MASS_MAX"],
        )

        self.selHc2LamPPi_LamDD = self.makeHc2PPX(
            name=hc2Lppi_name + "_LamDD",
            inputSel=[StdLooseLambdaDD, self.inProtons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> Lambda0 p+ pi-]cc',
                            '[Sigma_c+ -> Lambda0 p+ pi+]cc' ],
            mass_max = self.config["Hc_MASS_MAX"],
        )

        self.selHc2LamPPiPi_LamLL = self.makeHc2PPX(
            name=hc2Lppipi_name + "_LamLL",
            inputSel=[StdLooseLambdaLL, self.inProtons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> Lambda0 p+ pi+ pi-]cc',
                            '[Sigma_c+ -> Lambda0 p+ pi- pi-]cc',
                            '[Sigma_c+ -> Lambda0 p+ pi+ pi+]cc' ],
            mass_max = self.config["Hc_MASS_MAX"],
        )

        self.selHc2LamPPiPi_LamDD = self.makeHc2PPX(
            name=hc2Lppipi_name + "_LamDD",
            inputSel=[StdLooseLambdaDD, self.inProtons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> Lambda0 p+ pi+ pi-]cc',
                            '[Sigma_c+ -> Lambda0 p+ pi- pi-]cc',
                            '[Sigma_c+ -> Lambda0 p+ pi+ pi+]cc' ],
            mass_max = self.config["Hc_MASS_MAX"],
        )

        self.selHcs2PPKKPi = self.makeHc2PPX(
            name=hcs2ppKKpi_name,
            inputSel=[self.inProtons, self.inKaons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> p+ p+ K- K- pi+]cc',
                            '[Sigma_c+ -> p+ p+ K+ K- pi+]cc',
                            '[Sigma_c+ -> p+ p+ K+ K- pi-]cc',
                            '[Sigma_c+ -> p+ p+ K- K- pi-]cc',                            
                            '[Sigma_c+ -> p+ p+ K+ K+ pi-]cc',                            
                            '[Sigma_c+ -> p+ p+ K+ K+ pi+]cc' ],
            mass_max = self.config["Hcs_MASS_MAX"],
        )

        self.selHcs2PPKsK_KsLL = self.makeHc2PPX(
            name=hcs2ppKsK_name + "_KsLL",
            inputSel=[StdLooseKsLL, self.inProtons, self.inKaons ],
            decDescriptors=['[Sigma_c+ -> p+ p+ KS0 K-]cc',
                            '[Sigma_c+ -> p+ p+ KS0 K+]cc' ],
            mass_max = self.config["Hcs_MASS_MAX"],
        )

        self.selHcs2PPKsK_KsDD = self.makeHc2PPX(
            name=hcs2ppKsK_name + "_KsDD",
            inputSel=[StdLooseKsDD, self.inProtons, self.inKaons ],
            decDescriptors=['[Sigma_c+ -> p+ p+ KS0 K-]cc',
                            '[Sigma_c+ -> p+ p+ KS0 K+]cc' ],
            mass_max = self.config["Hcs_MASS_MAX"],
        )

        self.selHcs2LamPKPi_LamLL = self.makeHc2PPX(
            name=hcs2LpKpi_name + "_LamLL",
            inputSel=[StdLooseLambdaLL, self.inProtons, self.inKaons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> Lambda0 p+ K- pi+]cc',
                            '[Sigma_c+ -> Lambda0 p+ K- pi-]cc',
                            '[Sigma_c+ -> Lambda0 p+ K+ pi+]cc',
                            '[Sigma_c+ -> Lambda0 p+ K+ pi-]cc' ],
            mass_max = self.config["Hcs_MASS_MAX"],
        )

        self.selHcs2LamPKPi_LamDD = self.makeHc2PPX(
            name=hcs2LpKpi_name + "_LamDD",
            inputSel=[StdLooseLambdaDD, self.inProtons, self.inKaons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> Lambda0 p+ K- pi+]cc',
                            '[Sigma_c+ -> Lambda0 p+ K- pi-]cc',
                            '[Sigma_c+ -> Lambda0 p+ K+ pi+]cc',
                            '[Sigma_c+ -> Lambda0 p+ K+ pi-]cc' ],
            mass_max = self.config["Hcs_MASS_MAX"],
        )

        self.selHcs2LamLamPi_LamLLLL = self.makeHc2PPX(
            name=hcs2LLpi_name + "_LamLLLL",
            inputSel=[StdLooseLambdaLL, self.inPions ],
            decDescriptors=['[Sigma_c+ -> Lambda0 Lambda0 pi+]cc',
                            '[Sigma_c+ -> Lambda0 Lambda0 pi-]cc' ],
            mass_max = self.config["Hcs_MASS_MAX"],
        )

        self.selHcs2LamLamPi_LamDDDD = self.makeHc2PPX(
            name=hcs2LLpi_name + "_LamDDDD",
            inputSel=[StdLooseLambdaDD, self.inPions ],
            decDescriptors=['[Sigma_c+ -> Lambda0 Lambda0 pi+]cc',
                            '[Sigma_c+ -> Lambda0 Lambda0 pi-]cc' ],
            mass_max = self.config["Hcs_MASS_MAX"],
        )

        self.selHcs2LamLamPi_LamLLDD = self.makeHc2PPX(
            name=hcs2LLpi_name + "_LamLLDD",
            inputSel=[StdLooseLambdaLL, StdLooseLambdaDD, self.inPions ],
            decDescriptors=['[Sigma_c+ -> Lambda0 Lambda0 pi+]cc',
                            '[Sigma_c+ -> Lambda0 Lambda0 pi-]cc' ],
            mass_max = self.config["Hcs_MASS_MAX"],
        )

        self.selPc2PKPiPi = self.makeHc2PPX(
            name=pc2pKpipi_name,
            inputSel=[self.inProtons, self.inKaons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> p+ K+ pi- pi-]cc',
                            '[Sigma_c+ -> p+ K+ pi+ pi-]cc',
                            '[Sigma_c+ -> p+ K+ pi+ pi+]cc',
                            '[Sigma_c+ -> p+ K- pi- pi-]cc',
                            '[Sigma_c+ -> p+ K- pi+ pi-]cc',
                            '[Sigma_c+ -> p+ K- pi+ pi+]cc' ],
            mass_max = self.config["Pc_MASS_MAX"],
        )

        self.selPc2PKsPi_KsLL = self.makeHc2PPX(
            name=pc2pKspi_name + "_KsLL",
            inputSel=[StdLooseKsLL, self.inProtons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> p+ KS0 pi-]cc',
                            '[Sigma_c+ -> p+ KS0 pi+]cc' ],
            mass_max = self.config["Pc_MASS_MAX"],
        )

        self.selPc2PKsPi_KsDD = self.makeHc2PPX(
            name=pc2pKspi_name + "_KsDD",
            inputSel=[StdLooseKsDD, self.inProtons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> p+ KS0 pi-]cc',
                            '[Sigma_c+ -> p+ KS0 pi+]cc' ],
            mass_max = self.config["Pc_MASS_MAX"],
        )

        self.selPs2PPiPi = self.makeHs2PPX(
            name=ps2ppipi_name,
            inputSel=[self.inProtons, self.inPions ],
            decDescriptors=['[Sigma_c+ -> p+ pi+ pi-]cc',
                            '[Sigma_c+ -> p+ pi+ pi+]cc',
                            '[Sigma_c+ -> p+ pi- pi-]cc',
                        ],
            mass_max = self.config["Ps_MASS_MAX"],
            mipchi2_cut = config['HsDaug_1ofAll_BPVIPCHI2_MIN'],
            vdchi2_cut = config['Hs_BPVVDCHI2_MIN'],
        )

        self.selPs2PPiPiDown = self.makeHs2PPX(
            name=ps2ppipiDown_name,
            inputSel=[self.inDownProtons, self.inDownPions ],
            decDescriptors=['[Sigma_c+ -> p+ pi+ pi-]cc',
                            '[Sigma_c+ -> p+ pi+ pi+]cc',
                            '[Sigma_c+ -> p+ pi- pi-]cc',
                        ],
            mass_max = self.config["Ps_MASS_MAX"],
            mipchi2_cut = config['HsDownDaug_1ofAll_BPVIPCHI2_MIN'],
            vdchi2_cut = config['HsDown_BPVVDCHI2_MIN'],
        )

        ## ref
        self.selLc2PKPi = self.makeHc2PPX(
            name=lc2pKpi_name,
            inputSel=[self.inProtons, self.inKaons, self.inPions ],
            decDescriptors=['[Lambda_c+ -> p+ K- pi+]cc'],
            mass_max = self.config["Lc_MASS_MAX"],
        )

        self.selL2PPi = self.makeHs2PPX(
            name=l2ppi_name,
            inputSel=[self.inProtons, self.inPions ],
            decDescriptors=['[Lambda0 -> p+ pi-]cc'],
            mass_max = self.config["Lam_MASS_MAX"],
            mipchi2_cut = config['HsDaug_1ofAll_BPVIPCHI2_MIN'],
            vdchi2_cut = config['Hs_BPVVDCHI2_MIN'],
        )

        self.selL2PPiDown = self.makeHs2PPX(
            name=l2ppi_name + "Down",
            inputSel=[self.inDownProtons, self.inDownPions ],
            decDescriptors=['[Lambda0 -> p+ pi-]cc'],
            mass_max = self.config["Lam_MASS_MAX"],
            mipchi2_cut = config['HsDownDaug_1ofAll_BPVIPCHI2_MIN'],
            vdchi2_cut = config['HsDown_BPVVDCHI2_MIN'],
        )

        ### lines

        self.line_Hc2PP = self.make_line(
            name='{0}Line'.format(hc2pp_name),
            selection=self.selHc2PP,
            prescale=config['PrescaleHc2PP'],
            postscale=config['PostscaleHc2PP'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2PPKPi = self.make_line(
            name='{0}Line'.format(hc2ppkpi_name),
            selection=self.selHc2PPKPi,
            prescale=config['PrescaleHc2PPKPi'],
            postscale=config['PostscaleHc2PPKPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2PPK = self.make_line(
            name='{0}Line'.format(hc2ppk_name),
            selection=self.selHc2PPK,
            prescale=config['PrescaleHc2PPK'],
            postscale=config['PostscaleHc2PPK'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2PPKPiPi = self.make_line(
            name='{0}Line'.format(hc2ppkpipi_name),
            selection=self.selHc2PPKPiPi,
            prescale=config['PrescaleHc2PPKPiPi'],
            postscale=config['PostscaleHc2PPKPiPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hs2PPPi = self.make_line(
            name='{0}Line'.format(hs2pppi_name),
            selection=self.selHs2PPPi,
            prescale=config['PrescaleHs2PPPi'],
            postscale=config['PostscaleHs2PPPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hs2PPPiDown = self.make_line(
            name='{0}Line'.format(hs2pppiDown_name),
            selection=self.selHs2PPPiDown,
            prescale=config['PrescaleHs2PPPi'],
            postscale=config['PostscaleHs2PPPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        #
        self.line_Hc2PPKs_KsLL = self.make_line(
            name='{0}Line'.format(hc2ppKs_name + "LL"),
            selection=self.selHc2PPKs_KsLL,
            prescale=config['PrescaleHc2PPKs'],
            postscale=config['PostscaleHc2PPKs'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2PPKs_KsDD = self.make_line(
            name='{0}Line'.format(hc2ppKs_name + "DD"),
            selection=self.selHc2PPKs_KsDD,
            prescale=config['PrescaleHc2PPKs'],
            postscale=config['PostscaleHc2PPKs'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2PPKsPi_KsLL = self.make_line(
            name='{0}Line'.format(hc2ppKspi_name + "LL"),
            selection=self.selHc2PPKsPi_KsLL,
            prescale=config['PrescaleHc2PPKsPi'],
            postscale=config['PostscaleHc2PPKsPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2PPKsPi_KsDD = self.make_line(
            name='{0}Line'.format(hc2ppKspi_name + "DD"),
            selection=self.selHc2PPKsPi_KsDD,
            prescale=config['PrescaleHc2PPKsPi'],
            postscale=config['PostscaleHc2PPKsPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2LamPPi_LamLL = self.make_line(
            name='{0}Line'.format(hc2Lppi_name + "LL"),
            selection=self.selHc2LamPPi_LamLL,
            prescale=config['PrescaleHc2LamPPi'],
            postscale=config['PostscaleHc2LamPPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2LamPPi_LamDD = self.make_line(
            name='{0}Line'.format(hc2Lppi_name + "DD"),
            selection=self.selHc2LamPPi_LamDD,
            prescale=config['PrescaleHc2LamPPi'],
            postscale=config['PostscaleHc2LamPPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2LamPPiPi_LamLL = self.make_line(
            name='{0}Line'.format(hc2Lppipi_name + "LL"),
            selection=self.selHc2LamPPiPi_LamLL,
            prescale=config['PrescaleHc2LamPPiPi'],
            postscale=config['PostscaleHc2LamPPiPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2LamPPiPi_LamDD = self.make_line(
            name='{0}Line'.format(hc2Lppipi_name + "DD"),
            selection=self.selHc2LamPPiPi_LamDD,
            prescale=config['PrescaleHc2LamPPiPi'],
            postscale=config['PostscaleHc2LamPPiPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hcs2PPKKPi = self.make_line(
            name='{0}Line'.format(hcs2ppKKpi_name),
            selection=self.selHcs2PPKKPi,
            prescale=config['PrescaleHcs2PPKKPi'],
            postscale=config['PostscaleHcs2PPKKPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hcs2PPKsK_KsLL = self.make_line(
            name='{0}Line'.format(hcs2ppKsK_name + "LL"),
            selection=self.selHcs2PPKsK_KsLL,
            prescale=config['PrescaleHcs2PPKsK'],
            postscale=config['PostscaleHcs2PPKsK'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hcs2PPKsK_KsDD = self.make_line(
            name='{0}Line'.format(hcs2ppKsK_name + "DD"),
            selection=self.selHcs2PPKsK_KsDD,
            prescale=config['PrescaleHcs2PPKsK'],
            postscale=config['PostscaleHcs2PPKsK'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hcs2LamPKPi_LamLL = self.make_line(
            name='{0}Line'.format(hcs2LpKpi_name + "LL"),
            selection=self.selHcs2LamPKPi_LamLL,
            prescale=config['PrescaleHcs2LamPKPi'],
            postscale=config['PostscaleHcs2LamPKPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hcs2LamPKPi_LamDD = self.make_line(
            name='{0}Line'.format(hcs2LpKpi_name + "DD"),
            selection=self.selHcs2LamPKPi_LamDD,
            prescale=config['PrescaleHcs2LamPKPi'],
            postscale=config['PostscaleHcs2LamPKPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hcs2LamLamPi_LamLLLL = self.make_line(
            name='{0}Line'.format(hcs2LLpi_name + "LLLL"),
            selection=self.selHcs2LamLamPi_LamLLLL,
            prescale=config['PrescaleHcs2LamLamPi'],
            postscale=config['PostscaleHcs2LamLamPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hcs2LamLamPi_LamDDDD = self.make_line(
            name='{0}Line'.format(hcs2LLpi_name + "DDDD"),
            selection=self.selHcs2LamLamPi_LamDDDD,
            prescale=config['PrescaleHcs2LamLamPi'],
            postscale=config['PostscaleHcs2LamLamPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hcs2LamLamPi_LamLLDD = self.make_line(
            name='{0}Line'.format(hcs2LLpi_name + "LLDD"),
            selection=self.selHcs2LamLamPi_LamLLDD,
            prescale=config['PrescaleHcs2LamLamPi'],
            postscale=config['PostscaleHcs2LamLamPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Pc2PKPiPi = self.make_line(
            name='{0}Line'.format(pc2pKpipi_name),
            selection=self.selPc2PKPiPi,
            prescale=config['PrescalePc2PKPiPi'],
            postscale=config['PostscalePc2PKPiPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Pc2PKsPi_KsLL = self.make_line(
            name='{0}Line'.format(pc2pKspi_name + "LL"),
            selection=self.selPc2PKsPi_KsLL,
            prescale=config['PrescalePc2PKsPi'],
            postscale=config['PostscalePc2PKsPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Pc2PKsPi_KsDD = self.make_line(
            name='{0}Line'.format(pc2pKspi_name + "DD"),
            selection=self.selPc2PKsPi_KsDD,
            prescale=config['PrescalePc2PKsPi'],
            postscale=config['PostscalePc2PKsPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Ps2PPiPi = self.make_line(
            name='{0}Line'.format(ps2ppipi_name),
            selection=self.selPs2PPiPi,
            prescale=config['PrescalePs2PPiPi'],
            postscale=config['PostscalePs2PPiPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Ps2PPiPiDown = self.make_line(
            name='{0}Line'.format(ps2ppipiDown_name),
            selection=self.selPs2PPiPiDown,
            prescale=config['PrescalePs2PPiPi'],
            postscale=config['PostscalePs2PPiPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        ## reference

        self.line_Lc2PKPi = self.make_line(
            name='{0}Line'.format(lc2pKpi_name),
            selection=self.selLc2PKPi,
            prescale=config['PrescaleLc2PKPi'],
            postscale=config['PostscaleLc2PKPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_L2PPi = self.make_line(
            name='{0}Line'.format(l2ppi_name),
            selection=self.selL2PPi,
            prescale=config['PrescaleL2PPi'],
            postscale=config['PostscaleL2PPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_L2PPiDown = self.make_line(
            name='{0}Line'.format(l2ppi_name + "Down"),
            selection=self.selL2PPiDown,
            prescale=config['PrescaleL2PPi'],
            postscale=config['PostscaleL2PPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

    def make_line(self, name, selection, prescale, postscale, **kwargs):
        """Create the stripping line defined by the selection.

        Keyword arguments:
        name -- Base name for the Line
        selection -- Selection instance
        prescale -- Fraction of candidates to randomly drop before stripping
        postscale -- Fraction of candidates to randomly drop after stripping
        **kwargs -- Keyword arguments passed to StrippingLine constructor
        """
        # Only create the line with positive pre- and postscales
        # You can disable each line by setting either to a negative value
        if prescale > 0 and postscale > 0:
            line = StrippingLine(
                name,
                selection=selection,
                prescale=prescale,
                postscale=postscale,
                **kwargs
            )
            self.registerLine(line)
            return line
        else:
            return False

    def makeHc2PPX(self, name, inputSel, decDescriptors, mass_max ):
        """Return a Selection instance for a Hc -> p+ p+ X decay.

        Keyword arguments:
        name -- Name to give the Selection instance
        inputSel -- List of inputs passed to Selection.RequiredSelections
        decDescriptors -- List of decay descriptors for CombineParticles
        """

        combCuts = (
            '(AMAXCHILD(PT) > {0[Daug_1ofAll_PT_MIN]})'
            ' & (AM < {1})'
            ' & (AMAXCHILD(PROBNNp) > {0[Proton_1ofAll_ProbNN_MIN]})'
            ' & (AMAXCHILD(BPVIPCHI2()) > {0[Daug_1ofAll_BPVIPCHI2_MIN]})'
            " & (ACUTDOCACHI2({0[Comb_ADOCACHI2_MAX]}, ''))"
        ).format(self.config, mass_max + 150*MeV)

        HcCuts = (
            '(VFASPF(VCHI2/VDOF) < {0[Hc_VCHI2VDOF_MAX]})'
            ' & (BPVVDCHI2 > {0[Hc_BPVVDCHI2_MIN]})'
            ' & (BPVLTIME(9)*c_light > {0[Hc_BPVCTAU_MIN]})'
            ' & (M < {1})'
            ' & (PT > {0[Hc_PT_MIN]})'
#            ' & (BPVIPCHI2() < {0[Hc_BPVIPCHI2_MAX]})'
        ).format(self.config, mass_max)

        _Hc = CombineParticles(
            name='Combine{0}'.format(name),
            DecayDescriptors=decDescriptors,
            CombinationCut=combCuts,
            MotherCut=HcCuts
        )

        return Selection(name, Algorithm=_Hc, RequiredSelections=inputSel)

    def makeHs2PPX(self, name, inputSel, decDescriptors, mass_max, mipchi2_cut, vdchi2_cut):
        """Return a Selection instance for a Hs -> p+ p+ X decay.

        Keyword arguments:
        name -- Name to give the Selection instance
        inputSel -- List of inputs passed to Selection.RequiredSelections
        decDescriptors -- List of decay descriptors for CombineParticles
        """

        combCuts = (
            '(AMAXCHILD(PT) > {0[HsDaug_1ofAll_PT_MIN]})'
            ' & (AM < {2})'
            ' & (AMAXCHILD(PROBNNp) > {0[HsProton_1ofAll_ProbNN_MIN]})'
            ' & (AMAXCHILD(BPVIPCHI2()) > {1})'
            " & (ACUTDOCACHI2({0[HsComb_ADOCACHI2_MAX]}, ''))"
        ).format(self.config,mipchi2_cut,mass_max)

        HsCuts = (
            '(VFASPF(VCHI2/VDOF) < {0[Hs_VCHI2VDOF_MAX]})'
            ' & (BPVVDCHI2 > {1})'
            ' & (BPVLTIME(9)*c_light > {0[Hs_BPVCTAU_MIN]})'
            ' & (M < {2})'
            ' & (PT > {0[Hs_PT_MIN]})'
#            ' & (BPVIPCHI2() < {0[Hs_BPVIPCHI2_MAX]})'
        ).format(self.config,vdchi2_cut,mass_max)

        _Hs = CombineParticles(
            name='Combine{0}'.format(name),
            DecayDescriptors=decDescriptors,
            CombinationCut=combCuts,
            MotherCut=HsCuts
        )

        return Selection(name, Algorithm=_Hs, RequiredSelections=inputSel)
