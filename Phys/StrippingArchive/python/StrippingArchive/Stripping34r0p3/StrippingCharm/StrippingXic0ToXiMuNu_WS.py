###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for Xic0 -> Xi- mu+ nu
The lines are used to complement the inclusive Xi Turbo lines by adding
Velo raw banks to study if the Xi- can be reconstructed as an upstream track.
The wrong sign StrippingLine is needed to understand the background from when Xi- 
is wrongly combined with a muon from another event.
"""
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from Configurables import FilterDesktop
from PhysConf.Selections import (
    FilterSelection, CombineSelection, MergedSelection)
from PhysSelPython.Wrappers import AutomaticData
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine
__author__ = ["Patrik Adlarson", "Artur Ukleja",
              "Marian Stahl", "Laurent Dufour"]

__all__ = ("Xic0ToXiMuNu_WSConf", "default_config")

moduleName = "Xic0ToXiMuNu_WS"


# Import Packages

# Default configuration dictionary
default_config = {
    "NAME": "Xic0ToXiMuNu_WS",
    "BUILDERTYPE": "Xic0ToXiMuNu_WSConf",
    "CONFIG": {
        "descriptor_xi": ["[Xi- -> Lambda0 pi-]cc"],
        "descriptor_ximu": ["[Xi_c0 -> Xi- mu-]cc"],
        "RequiredRawEvents": ["Velo"],
        "bach_pion": {
            "tes": "Phys/StdAllNoPIDsPions/Particles",
            "filter": "(PT > 120*MeV) & (P > 1000*MeV) & (MIPDV(PRIMARY)>0.01*mm) & (MIPCHI2DV(PRIMARY) > 9.0) & (PROBNNpi>0.03)"
        },
        "down_pion": {
            "tes": "Phys/StdNoPIDsDownPions/Particles",
            "filter": "(P>1000*MeV) & (PT>150*MeV) & (PROBNNpi>0.03)"
        },
        "bach_muon": {
            "tes": "Phys/StdAllNoPIDsMuons/Particles",
            "filter": "(PT>200*MeV) & (P>3000*MeV) & (PROBNNmu>0.3) & (BPVIPCHI2()>4)"
        },
        "lambda_ll": {
            "tes": "Phys/StdVeryLooseLambdaLL/Particles",
            "filter": """(ADMASS('Lambda0')<20*MeV) & (P>6*GeV) & (PT>200*MeV) & (BPVVDZ>8*mm) & (BPVLTIME() > 2.0*ps) & (CHI2VXNDF<20) &
                    (MAXTREE('p+'==ABSID,MIPCHI2DV(PRIMARY))>9) & (MAXTREE('pi-'==ABSID,MIPCHI2DV(PRIMARY))>24) & 
                    (MAXTREE('p+'==ABSID,PT)>100*MeV) & (MAXTREE('p+'==ABSID,P)>5.0*GeV) & (VFASPF(VZ) > -100.*mm) & (VFASPF(VZ) <  640.*mm) & (BPVVDCHI2 > 100)"""
        },
        "lambda_dd": {
            "tes": "Phys/StdLooseLambdaDD/Particles",
            "filter": """(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>400*MeV) & (CHILDIP(1)<2*mm) & (MAXTREE('p+'==ABSID,P)>9*GeV) & (CHI2VXNDF<30) &
                    (MAXTREE('p+'==ABSID,PT)>200*MeV) & (MAXTREE('pi+'==ABSID,PT)>150*MeV) & (VFASPF(VZ) >  300.*mm) & (VFASPF(VZ) < 2485.*mm)"""
        },
        "xi_lll": {
            "comb_cut": "(ASUM(PT)>0.5*GeV) & (ADAMASS('Xi-')<30*MeV) & (ACHILD(2,MIPCHI2DV(PRIMARY))>9) & (ADOCA(1,2)<0.5*mm) & (ACHI2DOCA(1,2)<20)",
            "mother_cut": """(ADMASS('Xi-')<25*MeV) & (VFASPF(VCHI2PDOF) < 10.0) & (BPVVDCHI2>15) & (BPVLTIME() > 2.0*picosecond) & 
            (P>8*GeV) & (PT>400*MeV) """
        },
        "xi_ddl": {
            "comb_cut": "(ASUM(PT)>0.5*GeV) & (ADAMASS('Xi-')<30*MeV) & (ADOCA(1,2)<2.5*mm) & (ACHI2DOCA(1,2)<12) ",
            "mother_cut": """(ADMASS('Xi-')<25*MeV) & (VFASPF(VCHI2PDOF) < 10.0) & (BPVVDCHI2>20) & (BPVLTIME() > 2.0*picosecond) &
            (P>15*GeV) & (PT>600*MeV)"""
        },
        "xi_ddd": {
            "comb_cut": "(ASUM(PT)>0.5*GeV) & (ADAMASS('Xi-')<30*MeV) & (ACHILD(2,MIPCHI2DV(PRIMARY))>20) & (ADOCA(1,2)<5*mm) & (ACHI2DOCA(1,2)<12)",
            "mother_cut": """(ADMASS('Xi-')<25*MeV) & (VFASPF(VCHI2PDOF) < 10.0) &  (BPVVDCHI2>10) & (BPVLTIME() > 2.0*picosecond) &
            (P>15*GeV) & (PT>600*MeV)"""
        },
        "amass_ximu": "(in_range(1400*MeV,AMASS(),4500*MeV)) & ",
        "xc0_lll": {
            "comb_cut": """AHASCHILD( (ABSID == 'mu+') ) & (ADOCA(1,2)<1.5)""",
            "mother_cut": """(VFASPF(VCHI2/VDOF)<25) & (BPVVDCHI2>4) & (P>12*GeV) & (PT>200*MeV)"""
        },
        "xc0_ddl": {
            "comb_cut": """AHASCHILD( (ABSID == 'mu+') ) & (ADOCA(1,2)<3.5)""",
            "mother_cut": """(VFASPF(VCHI2/VDOF)<25) & (BPVVDCHI2>4) & (P>20*GeV) & (PT>200*MeV) """
        },
        "xc0_ddd": {
            "comb_cut": """AHASCHILD( (ABSID == 'mu+') ) & (ADOCA(1,2)<5.5)""",
            "mother_cut": """(VFASPF(VCHI2/VDOF)<25) & (BPVVDCHI2>4) & (P>20*GeV) & (PT>200*MeV)"""
        },
    },
    "STREAMS": {
        "CharmCompleteEvent": ["StrippingXic0ToXiMuNu_WS_{0}Line".format(tt) for tt in ["LLL", "DDL", "DDD"]]
    },
    "WGs": ["Charm"]
}


class Xic0ToXiMuNu_WSConf(LineBuilder):

    __configuration_keys__ = default_config["CONFIG"].keys()

    def __init__(self, moduleName, config):
        LineBuilder.__init__(self, moduleName, config)
        # decay products
        bach_pion = FilterSelection(moduleName+"_bach_pion", [AutomaticData(
            config["bach_pion"]["tes"])], Code=config["bach_pion"]["filter"])
        down_pion = FilterSelection(moduleName+"_down_pion", [AutomaticData(
            config["down_pion"]["tes"])], Code=config["down_pion"]["filter"])
        bach_muon = FilterSelection(moduleName+"_bach_muon", [AutomaticData(
            config["bach_muon"]["tes"])], Code=config["bach_muon"]["filter"])
        lambda_ll = FilterSelection(moduleName+"_lambda_ll", [AutomaticData(
            config["lambda_ll"]["tes"])], Code=config["lambda_ll"]["filter"])
        lambda_dd = FilterSelection(moduleName+"_lambda_dd", [AutomaticData(
            config["lambda_dd"]["tes"])], Code=config["lambda_dd"]["filter"])
        xi_lll = CombineSelection(moduleName+"_xi_lll", [lambda_ll, bach_pion], DecayDescriptors=config["descriptor_xi"],
                                  CombinationCut=config["xi_lll"]["comb_cut"], MotherCut=config["xi_lll"]["mother_cut"])
        xi_ddl = CombineSelection(moduleName+"_xi_ddl", [lambda_dd, bach_pion], DecayDescriptors=config["descriptor_xi"],
                                  CombinationCut=config["xi_ddl"]["comb_cut"], MotherCut=config["xi_ddl"]["mother_cut"])
        xi_ddd = CombineSelection(moduleName+"_xi_ddd", [lambda_dd, down_pion], DecayDescriptors=config["descriptor_xi"],
                                  CombinationCut=config["xi_ddd"]["comb_cut"], MotherCut=config["xi_ddd"]["mother_cut"])
        # charm baryon candidates; this could look a bit nicer when looping over track types and channels...

        ximu_lll = CombineSelection(moduleName+"_ximu_lll", [xi_lll, bach_muon], DecayDescriptors=config["descriptor_ximu"],
                                    CombinationCut=config["amass_ximu"]+config["xc0_lll"]["comb_cut"], MotherCut=config["xc0_lll"]["mother_cut"])
        ximu_ddl = CombineSelection(moduleName+"_ximu_ddl", [xi_ddl, bach_muon], DecayDescriptors=config["descriptor_ximu"],
                                    CombinationCut=config["amass_ximu"]+config["xc0_ddl"]["comb_cut"], MotherCut=config["xc0_ddl"]["mother_cut"])
        ximu_ddd = CombineSelection(moduleName+"_ximu_ddd", [xi_ddd, bach_muon], DecayDescriptors=config["descriptor_ximu"],
                                    CombinationCut=config["amass_ximu"]+config["xc0_ddd"]["comb_cut"], MotherCut=config["xc0_ddd"]["mother_cut"])

        # Create the stripping lines
        self.registerLine(StrippingLine(
            moduleName+"_LLLLine", RequiredRawEvents=config["RequiredRawEvents"], algos=[ximu_lll], MDSTFlag=True))
        self.registerLine(StrippingLine(
            moduleName+"_DDLLine", RequiredRawEvents=config["RequiredRawEvents"], algos=[ximu_ddl], MDSTFlag=True))
        self.registerLine(StrippingLine(
            moduleName+"_DDDLine", RequiredRawEvents=config["RequiredRawEvents"], algos=[ximu_ddd], MDSTFlag=True))
