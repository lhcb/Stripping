from __future__ import print_function
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selection of He candidates via Median DEDX
'''

__author__ = ['Hendrik Jage', 'Dan Moise']
__date__ = '14.06.2023'
__version__ = 'v0r1'

__all__ = ('He_IT_DownConf', 'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

from StandardParticles import StdNoPIDsDownPions as DownPions
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from Configurables import LoKi__VoidFilter as VoidFilter

default_config = {
    'NAME': 'He_IT_Down',
    'BUILDERTYPE': 'He_IT_DownConf',
    'WGs': ['IFT'],
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'Prescale': 1.0,
        'ITDedxMedian': 90.0,
        'NumITClusters':   2,
        'RequiredRawEvents': ["Calo", "Rich", "Velo", "Tracker", "Muon", "HC"],
    },
}


class He_IT_DownConf(LineBuilder):
    __configuration_keys__ = list(default_config['CONFIG'].keys())

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        _heliumCuts = "    (TRITCLUSTERS            > %(NumITClusters)s)" % self.config
        _heliumCuts += " & (TRITCLUSTERDEDXMEDIAN() > %(ITDedxMedian)s)" % self.config

        heliumFilter = FilterDesktop(Code=_heliumCuts)

        myHelium = Selection('Helium_IT_DownForSel',
                             Algorithm=heliumFilter,
                             RequiredSelections=[DownPions])

        HeliumITDownLine = StrippingLine(
            self.name + '_Line',
            prescale=self.config['Prescale'],
            RequiredRawEvents=self.config['RequiredRawEvents'],
            selection=myHelium)

        self.registerLine(HeliumITDownLine)
