from __future__ import print_function
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selection of He candidates via Median DEDX
'''

__author__ = ['Hendrik Jage', 'Dan Moise']
__date__ = '14.06.2023'
__version__ = 'v0r1'

__all__ = ('He_TTOT_LongConf', 'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

from StandardParticles import StdAllNoPIDsPions as LongPions
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from Configurables import LoKi__VoidFilter as VoidFilter

default_config = {
    'NAME': 'He_TTOT_Long',
    'BUILDERTYPE': 'He_TTOT_LongConf',
    'WGs': ['IFT'],
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'Prescale': 1.0,
        'OT_p0': -1.7189,
        'OT_p1':   0.486,
        'OT_p2':   13200,
        'NumTTClusters':     2,
        'NumOTTimes':        7,
        'minP':              0,
        'TTLLDCut':        1.3,
        'TTLLDVersionHe': 'v2',
        'TTLLDVersionZ1': 'v2',
        'OT_dt':             1,
        'RequiredRawEvents': ["Calo", "Rich", "Velo", "Tracker", "Muon", "HC"],
    },
}


class He_TTOT_LongConf(LineBuilder):
    __configuration_keys__ = list(default_config['CONFIG'].keys())

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        ot_preambulo = ["P_He     = 2*P*MeV",
                        "OT_t_eff = %(OT_p0)s + %(OT_p1)s/0.299792458*(sqrt((%(OT_p2)s/P_He)**2 + 1) - 1)" % self.config]

        presel_cut = "    (TRTTCLUSTERS  > %(NumTTClusters)s)" % self.config
        presel_cut += " & (TROTTIMES     > %(NumOTTimes)s)" % self.config
        presel_cut += " & (P             > %(minP)s)" % self.config

        presel_filter = FilterDesktop(Code=presel_cut)

        presel = Selection('HeliumLLDTTOT_LongPreSel',
                           Algorithm=presel_filter,
                           RequiredSelections=[LongPions])

        pid_cut = "(   DEDXPID('He', 'TT', '%(TTLLDVersionHe)s')" % self.config
        pid_cut += " - DEDXPID('Z1', 'TT', '%(TTLLDVersionZ1)s') > %(TTLLDCut)s)" % self.config
        pid_cut += " & (abs(TRTIME() - OT_t_eff)                 < %(OT_dt)s)" % self.config

        pid_filter = FilterDesktop(Code=pid_cut,
                                   Preambulo=ot_preambulo)

        helium_sel = Selection('HeliumLLDTTOT_LongForSel',
                               Algorithm=pid_filter,
                               RequiredSelections=[presel])

        He_TTOT_LongLine = StrippingLine(
            self.name + '_Line',
            prescale=self.config['Prescale'],
            RequiredRawEvents=self.config['RequiredRawEvents'],
            selection=helium_sel)

        self.registerLine(He_TTOT_LongLine)
