###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of B+ ->Lambda0 Lambda~0 K+ stripping Selections and StrippingLines.
Provides functions to build Lambda0->DD, Lambda0->LL selections.
Provides class B2LambdaLambdabarKLinesConf, which constructs the Selections and StrippingLines
given a configuration dictionary.
Exported symbols (use python help!):
   - B2LambdaLambdabarKLinesConf
"""

__author__ = ["Xingyu Tong"]
__date__ = '11/07/2023'
__version__ = '$Revision: 1.0 $'
__all__ = 'StrippingB2LambdaLambdabarKLinesConf'

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

from StandardParticles import StdLooseKaons as Kaons

'''
2016:
StrippingReport                                                INFO Event 20000, Good event 20000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |_StrippingGlobal_                                            |  0.1250|        25|       |   4.547|
 |_StrippingSequenceStreamBhadron_                             |  0.1250|        25|       |   4.540|
 |!StrippingB2LambdaLambdabarKDDDDLine                         |  0.0050|         1|  1.000|   3.240|
 |!StrippingB2LambdaLambdabarKLLLLLine                         |  0.0800|        16|  1.312|   0.585|
 |!StrippingB2LambdaLambdabarKLLDDLine                         |  0.0800|        16|  1.312|   0.032|
 |!StrippingB2LambdaLambdabarKDDDDLine_TIMING                  |  0.0050|         1|  1.000|   0.034|
 |!StrippingB2LambdaLambdabarKLLLLLine_TIMING                  |  0.0800|        16|  1.312|   0.032|
 |!StrippingB2LambdaLambdabarKLLDDLine_TIMING                  |  0.0800|        16|  1.312|   0.029|
2017:
StrippingReport                                                INFO Event 20000, Good event 20000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |_StrippingGlobal_                                            |  0.1550|        31|       |   5.439|
 |_StrippingSequenceStreamBhadron_                             |  0.1550|        31|       |   5.431|
 |!StrippingB2LambdaLambdabarKDDDDLine                         |  0.0250|         5|  1.800|   3.913|
 |!StrippingB2LambdaLambdabarKLLLLLine                         |  0.0550|        11|  1.364|   0.713|
 |!StrippingB2LambdaLambdabarKLLDDLine                         |  0.1050|        21|  1.333|   0.029|
 |!StrippingB2LambdaLambdabarKDDDDLine_TIMING                  |  0.0250|         5|  1.800|   0.030|
 |!StrippingB2LambdaLambdabarKLLLLLine_TIMING                  |  0.0550|        11|  1.364|   0.044|
 |!StrippingB2LambdaLambdabarKLLDDLine_TIMING                  |  0.1050|        21|  1.333|   0.030|
2018:
StrippingReport                                                INFO Event 20000, Good event 20000
 |                                              *Decision name*|*Rate,%*|*Accepted*| *Mult*|*ms/evt*|
 |_StrippingGlobal_                                            |  0.2200|        44|       |   5.720|
 |_StrippingSequenceStreamBhadron_                             |  0.2200|        44|       |   5.712|
 |!StrippingB2LambdaLambdabarKDDDDLine                         |  0.0150|         3|  1.000|   4.130|
 |!StrippingB2LambdaLambdabarKLLLLLine                         |  0.1100|        22|  1.318|   0.739|
 |!StrippingB2LambdaLambdabarKLLDDLine                         |  0.1500|        30|  1.467|   0.038|
 |!StrippingB2LambdaLambdabarKDDDDLine_TIMING                  |  0.0150|         3|  1.000|   0.037|
 |!StrippingB2LambdaLambdabarKLLLLLine_TIMING                  |  0.1100|        22|  1.318|   0.038|
 |!StrippingB2LambdaLambdabarKLLDDLine_TIMING                  |  0.1500|        30|  1.467|   0.033|

'''

default_config = {
    'NAME': 'B2LambdaLambdabarK',
    'WGs': ['BnoC'],
    'BUILDERTYPE': 'B2LambdaLambdabarKLinesConf',
    'CONFIG': {'Trk_Chi2': 4.0,
               'Trk_GhostProb': 0.4,
               'Lambda_DD_MassWindow': 20.0,
               'Lambda_DD_VtxChi2': 12.0,
               'Lambda_DD_FDChi2': 50.0,
               'Lambda_DD_Pmin': 5000.0,
               'Lambda_LL_MassWindow': 20.0,
               'Lambda_LL_VtxChi2': 12.0,
               'B0_Mlow': 779.0,
               'B0_Mhigh': 1921.0,
               'B0_APTmin': 900.0,
               'B0_PTmin': 1000.0,
               'B0Daug_DD_maxDocaChi2': 16.0,
               'B0Daug_LL_maxDocaChi2': 16.0,
               'B0Daug_LD_maxDocaChi2': 16.0,
               'B0Daug_DD_PTsum': 2000.0,
               'B0Daug_LL_PTsum': 2000.0,
               'B0Daug_LD_PTsum': 2000.0,
               'B0_VtxChi2': 36.0,
               'B0_Dira': 0.9990,
               'B0_DD_IPCHI2wrtPV': 25.0,
               'B0_LL_IPCHI2wrtPV': 25.0,
               'B0_LD_IPCHI2wrtPV': 25.0,
               'B0_FDwrtPV': 0.8,
               'B0_DD_FDChi2': 25,
               'B0_LL_FDChi2': 25,
               'B0_LD_FDChi2': 25,
               'GEC_MaxTracks': 250,
               'Prescale': 1.0,
               'Postscale': 1.0,
               'RelatedInfoTools': [{"Type": "RelInfoConeVariables",
                                     "ConeAngle": 1.7,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar17'
                                     },
                                    {"Type": "RelInfoConeVariables",
                                     "ConeAngle": 1.5,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar15'
                                     },
                                    {"Type": "RelInfoConeVariables",
                                     "ConeAngle": 1.0,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar10'
                                     },
                                    {"Type": "RelInfoConeVariables",
                                     "ConeAngle": 0.8,
                                     "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                     "Location": 'ConeVar08'
                                     },
                                    {"Type": "RelInfoVertexIsolation",
                                     "Location": "VtxIsolationVar"
                                     }
                                    ]
               },
    'STREAMS': ['Bhadron']
}


class B2LambdaLambdabarKLinesConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        GECCode = {'Code': "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %s)" % config['GEC_MaxTracks'],
                   'Preambulo': ["from LoKiTracks.decorators import *"]}

        self.kaons = Kaons
        self.makeLambda2DD('Lambda02DD', config)
        self.makeLambda2LL('Lambda02LL', config)

        namesSelections = [(name + 'DDDD', self.makeB2LambdaLambdabarKDDDD(name + 'DDDD', config)),
                           (name + 'LLLL',
                            self.makeB2LambdaLambdabarKLLLL(name + 'LLLL', config)),
                           (name + 'LLDD',
                            self.makeB2LambdaLambdabarKLLDD(name + 'LLDD', config)),
                           ]

        # make lines

        for selName, sel in namesSelections:

            extra = {}

            line = StrippingLine(selName + 'Line',
                                 selection=sel,
                                 prescale=config['Prescale'],
                                 postscale=config['Postscale'],
                                 RelatedInfoTools=config['RelatedInfoTools'],
                                 FILTER=GECCode,
                                 **extra)

            self.registerLine(line)

    def makeLambda2DD(self, name, config):
        # define all the cuts
        _massCut = "(ADMASS('Lambda0')<%s*MeV)" % config['Lambda_DD_MassWindow']
        _vtxCut = "(VFASPF(VCHI2)<%s)   " % config['Lambda_DD_VtxChi2']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['Lambda_DD_FDChi2']
        _momCut = "(P>%s*MeV)" % config['Lambda_DD_Pmin']

        _allCuts = _momCut
        _allCuts += '&'+_massCut
        _allCuts += '&'+_vtxCut
        _allCuts += '&'+_fdChi2Cut

        # get the Lambda0's to filter
        _stdLambdaDD = DataOnDemand(Location="Phys/StdLooseLambdaDD/Particles")

        # make the filter
        _filterLambdaDD = FilterDesktop(Code=_allCuts)

        # make and store the Selection object
        self.selLambda2DD = Selection(
            name, Algorithm=_filterLambdaDD, RequiredSelections=[_stdLambdaDD])

        return self.selLambda2DD

    def makeLambda2LL(self, name, config):
        # define all the cuts
        _massCut = "(ADMASS('Lambda0')<%s*MeV)" % config['Lambda_LL_MassWindow']
        _vtxCut = "(VFASPF(VCHI2)<%s)" % config['Lambda_LL_VtxChi2']
        _trkChi2Cut1 = "(CHILDCUT((TRCHI2DOF<%s),1))" % config['Trk_Chi2']
        _trkChi2Cut2 = "(CHILDCUT((TRCHI2DOF<%s),2))" % config['Trk_Chi2']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))" % config['Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))" % config['Trk_GhostProb']

        _allCuts = _massCut
        _allCuts += '&'+_trkChi2Cut1
        _allCuts += '&'+_trkChi2Cut2
        _allCuts += '&'+_vtxCut
        _allCuts += '&'+_trkGhostProbCut1
        _allCuts += '&'+_trkGhostProbCut2

        # get the Lambda's to filter
        _stdLambdaLL = DataOnDemand(
            Location="Phys/StdVeryLooseLambdaLL/Particles")

        # make the filter
        _filterLambdaLL = FilterDesktop(Code=_allCuts)

        # make and store the Selection object
        self.selLambda2LL = Selection(
            name, Algorithm=_filterLambdaLL, RequiredSelections=[_stdLambdaLL])

        return self.selLambda2LL

    def makeB2LambdaLambdabarKDDDD(self, name, config):
        """
        Create and store a B+ ->Lambda0 Lambda~0 (DDDD) K+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow = "(AM>(5279-%s)*MeV)" % config['B0_Mlow']
        _massCutHigh = "(AM<(5279+%s)*MeV)" % config['B0_Mhigh']
        _aptCut = "(APT>%s*MeV)" % config['B0_APTmin']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))" % config['B0Daug_DD_maxDocaChi2']
        _daugPtSumCut = "((APT1+APT2+APT3)>%s*MeV)" % config['B0Daug_DD_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut = "(PT>%s*MeV)" % config['B0_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)" % config['B0_VtxChi2']
        _diraCut = "(BPVDIRA>%s)" % config['B0_Dira']
        _ipChi2Cut = "(MIPCHI2DV(PRIMARY)<%s)" % config['B0_DD_IPCHI2wrtPV']
        _fdCut = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B0_FDwrtPV']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['B0_DD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdCut  # lookhere
        _motherCuts += '&'+_fdChi2Cut

        _B0 = CombineParticles()

        _B0.DecayDescriptors = ["[B+ -> Lambda0 Lambda~0 K+]cc"]

        _trkGhostProbCut = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut = "(TRCHI2DOF<%s)" % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _B0.DaughtersCuts = {"K+": _daughtersCuts,
                             }
        _B0.CombinationCut = _combCuts
        _B0.MotherCut = _motherCuts
        _B0.ReFitPVs = True

        _B0Conf = _B0.configurable(name + '_combined')

        self.selB2LambdaLambdabarKDDDD = Selection(
            name, Algorithm=_B0Conf, RequiredSelections=[self.selLambda2DD, self.kaons])
        return self.selB2LambdaLambdabarKDDDD

    def makeB2LambdaLambdabarKLLLL(self, name, config):
        """
        Create and store a B+ ->Lambda0 Lambda~0 (LLLL) K+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow = "(AM>(5279-%s)*MeV)" % config['B0_Mlow']
        _massCutHigh = "(AM<(5279+%s)*MeV)" % config['B0_Mhigh']
        _aptCut = "(APT>%s*MeV)" % config['B0_APTmin']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))" % config['B0Daug_LL_maxDocaChi2']
        _daugPtSumCut = "((APT1+APT2+APT3)>%s*MeV)" % config['B0Daug_LL_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut = "(PT>%s*MeV)" % config['B0_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)" % config['B0_VtxChi2']
        _diraCut = "(BPVDIRA>%s)" % config['B0_Dira']
        _ipChi2Cut = "(MIPCHI2DV(PRIMARY)<%s)" % config['B0_LL_IPCHI2wrtPV']
        _fdCut = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B0_FDwrtPV']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['B0_LL_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdChi2Cut
        _motherCuts += '&'+_fdCut

        _B0 = CombineParticles()

        _B0.DecayDescriptors = ["[B+ -> Lambda0 Lambda~0 K+]cc"]

        _trkGhostProbCut = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut = "(TRCHI2DOF<%s)" % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _B0.DaughtersCuts = {"K+": _daughtersCuts,
                             }
        _B0.CombinationCut = _combCuts
        _B0.MotherCut = _motherCuts
        _B0.ReFitPVs = True

        _B0Conf = _B0.configurable(name + '_combined')

        self.selB2LambdaLambdabarKLLLL = Selection(
            name, Algorithm=_B0Conf, RequiredSelections=[self.selLambda2LL, self.kaons])
        return self.selB2LambdaLambdabarKLLLL

    def makeB2LambdaLambdabarKLLDD(self, name, config):
        """
        Create and store a B+ ->Lambda0 Lambda~0 (LLDD) K+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow = "(AM>(5279-%s)*MeV)" % config['B0_Mlow']
        _massCutHigh = "(AM<(5279+%s)*MeV)" % config['B0_Mhigh']
        _aptCut = "(APT>%s*MeV)" % config['B0_APTmin']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))" % config['B0Daug_LD_maxDocaChi2']
        _daugPtSumCut = "((APT1+APT2+APT3)>%s*MeV)" % config['B0Daug_LD_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut = "(PT>%s*MeV)" % config['B0_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)" % config['B0_VtxChi2']
        _diraCut = "(BPVDIRA>%s)" % config['B0_Dira']
        _ipChi2Cut = "(MIPCHI2DV(PRIMARY)<%s)" % config['B0_LD_IPCHI2wrtPV']
        _fdCut = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B0_FDwrtPV']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['B0_LD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdCut
        _motherCuts += '&'+_fdChi2Cut

        _B0 = CombineParticles()

        _B0.DecayDescriptors = ["[B+ -> Lambda0 Lambda~0 K+]cc"]

        _trkGhostProbCut = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut = "(TRCHI2DOF<%s)" % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _B0.DaughtersCuts = {"K+": _daughtersCuts,
                             }
        _B0.CombinationCut = _combCuts
        _B0.MotherCut = _motherCuts
        _B0.ReFitPVs = True

        _B0Conf = _B0.configurable(name + '_combined')

        self.selB2LambdaLambdabarKLLDD = Selection(name, Algorithm=_B0Conf, RequiredSelections=[
                                                   self.selLambda2LL, self.selLambda2DD, self.kaons])
        return self.selB2LambdaLambdabarKLLDD
