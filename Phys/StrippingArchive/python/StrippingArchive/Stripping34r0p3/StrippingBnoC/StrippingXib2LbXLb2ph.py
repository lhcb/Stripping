###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping options for (pre-)selecting Xib -> Lb pi/mu, Lb -> p h

Authors: Ao Xu
"""

__author__ = ["Ao Xu"]
__date__ = "06/09/2021"
__version__ = "1.0"

__all__ = ("Xib2LbXLb2phLines",
           "default_config")

from Gaudi.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
from StandardParticles import (StdAllNoPIDsPions,
                               StdAllNoPIDsProtons,
                               StdAllLooseMuons)

from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder, checkConfig



default_config = {
    "NAME"        : "Xib2LbXLb2ph",
    "WGs"         : ["BnoC"],
    "BUILDERTYPE" : "Xib2LbXLb2phLines",
    "CONFIG"      : { "PrescaleXib2LbX"  : 1,
                      "MinPTLb2ph_h"     : 1000,
                      "MinPTLb2ph_p"     : 1000,
                      "MinPTXib2LbX_h"   : 100,
                      "MinIPChi2Lb2ph"   : 8,
                      "MinIPChi2Xib2LbX" : 4,
                      "TrChi2"           : 4,
                      "TrGhostProb"      : 0.5,
                      "MaxPTLb2ph"       : 1500,
                      "MaxIPChi2Lb2ph"   : 12,
                      "DOCA"             : 0.08,
                      "VCHI2"            : 10,
                      "XibVCHI2"         : 25,
                      "BVDCHI2"          : 100,
                      "BPT"              : 1500,
                      "XibPT"            : 1500,
                      "BTAU"             : 0.0006,
                      "LbCombMassLow"    : 4900,
                      "LbCombMassHigh"   : 6200,
                      "LbMassLow"        : 5000,
                      "LbMassHigh"       : 6100,
                      "XibCombMassLow"   : 5100,
                      "XibCombMassHigh"  : 6300,
                      "XibMassLow"       : 5200,
                      "XibMassHigh"      : 6200,
                      "ConeAngles"       : [ 1.0, 1.5, 1.7, 2.0 ] },
    "STREAMS"     : [ "Bhadron" ]
}



class Xib2LbXLb2phLines( LineBuilder ) :
    """Class defining the Xib -> Lb pi/mu, Lb -> ph stripping lines"""

    __configuration_keys__ = default_config["CONFIG"].keys()

    def __init__( self,name,config ) :

        LineBuilder.__init__(self, name, config)

        # inputs
        pion_filter = FilterDesktop( Code = require_all(
            "TRGHOSTPROB < {}".format(config["TrGhostProb"]),
            "TRCHI2DOF < {}".format(config["TrChi2"]),
            "PT > {} * MeV".format(config["MinPTLb2ph_h"]),
            "MIPCHI2DV(PRIMARY) > {}".format(config["MinIPChi2Lb2ph"])) )
        self.pions = Selection( "pions_for_"+name,
                                 Algorithm = pion_filter,
                                 RequiredSelections = [StdAllNoPIDsPions] )
        proton_filter = FilterDesktop( Code = require_all(
            "TRGHOSTPROB < {}".format(config["TrGhostProb"]),
            "TRCHI2DOF < {}".format(config["TrChi2"]),
            "PT > {} * MeV".format(config["MinPTLb2ph_p"]),
            "MIPCHI2DV(PRIMARY) > {}".format(config["MinIPChi2Lb2ph"])) )
        self.protons = Selection( "protons_for_"+name,
                                  Algorithm = proton_filter,
                                  RequiredSelections = [StdAllNoPIDsProtons] )
        soft_pion_filter = FilterDesktop( Code = require_all(
            "TRGHOSTPROB < {}".format(config["TrGhostProb"]),
            "TRCHI2DOF < {}".format(config["TrChi2"]),
            "PT > {} * MeV".format(config["MinPTXib2LbX_h"]),
            "MIPCHI2DV(PRIMARY) > {}".format(config["MinIPChi2Xib2LbX"])) )
        self.soft_pions = Selection( "soft_pions_for_"+name,
                                     Algorithm = soft_pion_filter,
                                     RequiredSelections = [StdAllNoPIDsPions] )
        muon_filter = FilterDesktop( Code = require_all(
            "TRGHOSTPROB < {}".format(config["TrGhostProb"]),
            "TRCHI2DOF < {}".format(config["TrChi2"]),
            "PT > {} * MeV".format(config["MinPTXib2LbX_h"]),
            "MIPCHI2DV(PRIMARY) > {}".format(config["MinIPChi2Xib2LbX"])) )
        self.muons = Selection( "muons_for_"+name,
                                Algorithm = muon_filter,
                                RequiredSelections = [StdAllLooseMuons] )

        # Lb -> p h
        Lb2phName = name + "Lb2ph"
        self.lb = self._make_lb(Lb2phName,config)

        # Xib- -> Lb pi-
        decay = "[Xi_b- -> Lambda_b0 pi-]cc"
        inputs = [self.lb,self.soft_pions]
        line_name = name + "Xib2LbPiLb2ph"
        xib2lbpi_rs = self._make_xib(line_name,decay,inputs,config)
        self._make_line(line_name,xib2lbpi_rs,config["PrescaleXib2LbX"])

        # (WS) Xib- -> Lb pi+
        decay = "[Xi_b- -> Lambda_b0 pi+]cc"
        inputs = [self.lb,self.soft_pions]
        line_name = name + "Xib2LbPiWSLb2ph"
        xib2lbpi_ws = self._make_xib(line_name,decay,inputs,config)
        self._make_line(line_name,xib2lbpi_ws,config["PrescaleXib2LbX"])

        # Xib- -> Lb mu- (nu)
        decay = "[Xi_b- -> Lambda_b0 mu-]cc"
        inputs = [self.lb,self.muons]
        line_name = name + "Xib2LbMuLb2ph"
        xib2lbmu_rs = self._make_xib(line_name,decay,inputs,config)
        self._make_line(line_name,xib2lbmu_rs,config["PrescaleXib2LbX"])

        # (WS) Xib- -> Lb mu+ (nu)
        decay = "[Xi_b- -> Lambda_b0 mu+]cc"
        inputs = [self.lb,self.muons]
        line_name = name + "Xib2LbMuWSLb2ph"
        xib2lbmu_ws = self._make_xib(line_name,decay,inputs,config)
        self._make_line(line_name,xib2lbmu_ws,config["PrescaleXib2LbX"])


    def _make_lb(self,name,config):
        """make Lb -> p h decay
        """
        cc = require_all(
                "AMAXDOCA('') < {}".format(config["DOCA"]),
                "AM > {} * MeV".format(config["LbCombMassLow"]),
                "AM < {} * MeV".format(config["LbCombMassHigh"]) )
        mc = require_all(
                "MAXTREE(((ABSID=='p+')|(ABSID=='pi+')),PT) > {} * MeV".format(config["MaxPTLb2ph"]),
                "MAXTREE(((ABSID=='p+')|(ABSID=='pi+')),MIPCHI2DV(PRIMARY)) > {}".format(config["MaxIPChi2Lb2ph"]),
                "PT > {} * MeV".format(config["BPT"]),
                "VFASPF(VCHI2) < {}".format(config["VCHI2"]),
                "BPVVDCHI2 > {}".format(config["BVDCHI2"]),
                "BPVLTIME() > {}".format(config["BTAU"]),
                "M > {} * MeV".format(config["LbMassLow"]),
                "M < {} * MeV".format(config["LbMassHigh"]) )
        combine_lb2ph = CombineParticles( DecayDescriptors = ["[Lambda_b0 -> p+ pi-]cc"],
                CombinationCut = cc,
                MotherCut = mc )

        return Selection( name,
                          Algorithm = combine_lb2ph,
                          RequiredSelections = [ self.protons, self.pions ] )


    def _make_xib(self,name,decay,inputs,config):
        """make Xib -> Lb X decay
        """
        cc = require_all(
                "AM > {} * MeV".format(config["XibCombMassLow"]),
                "AM < {} * MeV".format(config["XibCombMassHigh"]) )
        mc = require_all(
                "PT > {} * MeV".format(config["XibPT"]),
                "VFASPF(VCHI2) < {}".format(config["XibVCHI2"]),
                "M > {} * MeV".format(config["XibMassLow"]),
                "M < {} * MeV".format(config["XibMassHigh"]) )
        combine_xib2lbx = CombineParticles( DecayDescriptors = [decay],
                CombinationCut = cc,
                MotherCut = mc )

        return Selection( name,
                          Algorithm = combine_xib2lbx,
                          RequiredSelections = inputs )


    def _make_line(self,name,selection,prescale):
        """make the stripping line
        """
        line = StrippingLine( name+"Line",
                              prescale  = prescale,
                              selection = selection,
                              EnableFlavourTagging = False,
                              RequiredRawEvents = ["Velo"] )

        self.registerLine(line)



def require_all(*cuts):
    """
    return string of "and" of inputs
    """
    cuts = ["({})".format(c) for c in cuts]
    return " & ".join(cuts)
