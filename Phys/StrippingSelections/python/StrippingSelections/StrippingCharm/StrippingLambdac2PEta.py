###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Speculative code for charmed baryon searches.
The following two channels are used:
Lambda_b0-> pi (Lambda_c+ -> p+ eta)
Lambda_b0-> pi (Lambda_c+ -> p+ eta')
'''


__author__ = ['Xiao-Rui Lyu', 'Pei-Rong Li', 'Miroslav Saur', 'Qi-Le Niu']
__date__ = '2023/07/12'
__version__ = '$Revision: 0.1 $'
__all__ = ('StrippingLambdac2PEta'
           ,'default_config')


from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import  StdAllLoosePions, StdAllLooseProtons, StdLooseProtons, StdLoosePions
from StandardParticles import StdLooseMergedPi0, StdLooseResolvedPi0
from StandardParticles import StdLooseResolvedEta, StdLooseEta2gg
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from PhysSelPython.Wrappers import MultiSelectionSequence
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond
import sys


default_name='Lambdac2PEta'
    #### This is the dictionary of all tunable cuts ########
default_config={
      'NAME'        : 'Lambdac2PEta',
      'WGs'         : ['Charm'],
      'BUILDERTYPE' : 'StrippingLambdac2PEta',
      'STREAMS'     : ['Charm'],
      'CONFIG'      : {
          'GEC_nLongTrk' : 160    # adimensional
        , 'signalPrescale'       :   1.0
        , 'TRCHI2DOFMax'            :   3.0
        , 'TrGhostProbMax'          :   0.25 # same for all particles
        , 'MINIPCHI2'               :   3.0  # adimensiional
        , 'ProtonP'                 :   3.0*GeV
        , 'ProtonPT'                :   500*MeV
        , 'Proton_PIDpPIDpi_Min'    :   5.0
        , 'Proton_PIDpPIDK_Min'     :   0.0
        , 'PionP'                   :   3*GeV
        , 'PionPT'                  :   250*MeV
        , 'PionPIDK'                :   5.0
        , 'ProbNNp'                 :   0.4
        , 'ProbNNpi'                :   0.4
        , 'ProbNNpiMax'             :   0.9

        , 'Lc_M_HalfWin'           :   200.0*MeV
        , 'Lb_M_HalfWin'           :   200.0*MeV
        , 'Lc_Daug_1of3_MIPCHI2DV_Min': 3.0
        , 'Lc_ADOCAMAX_Max'         :   0.15*mm
        , 'Lc_APT_Min'              :   1.2*GeV
        , 'Lc_AP_Min'               :   15*GeV
        , 'Lc_VCHI2_Max'            :  30.0
        , 'Lc_BPVVDCHI2_Min'        :  16.0
        , 'Lc_BPVVDZ_Min'           :  0.5*mm
        , 'Lc_BPVDIRA_Min'          :   0.95

        #For Lb
        , 'Lb_AP_Min': 20*GeV 
        , 'Lb_APT_Min': 1.5*GeV 
        , 'Lb_VCHI2_Max': 30 
        , 'Lb_BPVVDCHI2_Min': 16 
        , 'Lb_BPVVDZ_Min': 0.7*mm 
        , 'Lb_BPVDIRA_Min': 0.95 
      } ## end of 'CONFIG' 
}  ## end of default_config

#-------------------------------------------------------------------------------------------------------------
class StrippingLambdac2PEta(LineBuilder) : 
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config
       
        GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                     "Preambulo": ["from LoKiTracks.decorators import *"]}
        
        ##########################################################################
        ## Basic particles: p+, pi
        ##########################################################################

        self.selPi = Selection( "SelPifor" + name,
                Algorithm = self._piFilter("Pifor"+name),
                RequiredSelections = [StdLoosePions])

        self.selProton = Selection( "SelPfor" + name,
                Algorithm = self._protonFilter("Pfor"+name),
                RequiredSelections = [StdLooseProtons])

        self.makeEta()
        # then use self.Eta2PiPiPi0 and self.Eta2PiPiGamma


        ##########################################################################
        self.DmesonSLList = self.makeLine()


    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    # Sub Function
    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    def _protonFilter( self, _name ):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (PT > %(ProtonPT)s) & (P>%(ProtonP)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PROBNNp > %(ProbNNp)s) & (PROBNNpi < %(ProbNNpiMax)s)"\
                "& (HASRICH)&(PIDp-PIDpi>%(Proton_PIDpPIDpi_Min)s)"\
                "& (HASRICH)&(PIDp-PIDK>%(Proton_PIDpPIDK_Min)s)"% self.config
        _pr = FilterDesktop(Code = _code)
        return _pr


    def _piFilter( self , _name):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (P>%(PionP)s) & (PT > %(PionPT)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PIDK< %(PionPIDK)s) "\
                "& (HASRICH)&(PROBNNpi > %(ProbNNpi)s) " % self.config
        _pi = FilterDesktop(Code = _code )
        return _pi


    def makeLb0Line(self, line_name, prescale, Lc):
        comb_cut = "in_range(200, AM, 200000)"
        vertex_cut  = "ALL"

        Lb0 = self.createCombinationSel(OutputList = "Lb02LcPi_" + line_name,
                DecayDescriptor = "[Lambda_b0 -> Lambda_c+ pi-]cc",
                DaughterLists   = [Lc, self.selPi],
                PreVertexCuts   = comb_cut,
                PostVertexCuts  = vertex_cut )
        Lb0Line = StrippingLine( line_name, 
                prescale = prescale,
                selection = Lb0, 
                EnableFlavourTagging = False )
        return Lb0Line


    ##------------------------------------------------------------------------------------------
    ##  -------------------- Begin to DpSL  ------------
    def makeLine( self ):

        _strCutCombfor = "(AMINCHILD(MIPCHI2DV(PRIMARY))>%(Lc_Daug_1of3_MIPCHI2DV_Min)s)" \
                    "& (AP>%(Lc_AP_Min)s)" \
                    "& (ADAMASS('Lambda_c+') < %(Lc_M_HalfWin)s)" \
                    "& (APT>%(Lc_APT_Min)s)" % self.config

        _strCutMothfor = "(VFASPF(VCHI2) < %(Lc_VCHI2_Max)s)" \
                      "& (ADMASS('Lambda_c+') < %(Lc_M_HalfWin)s)" \
                      "& (BPVVDCHI2>%(Lc_BPVVDCHI2_Min)s)" \
                      "& (BPVDIRA>%(Lc_BPVDIRA_Min)s)" % self.config

        _strCutComb_Mass   = "(ADAMASS('Lambda_c+') < 1.1*%(Lc_M_HalfWin)s)" % self.config
        _strCutMoth_Mass   = "(ADMASS('Lambda_c+') < %(Lc_M_HalfWin)s)" % self.config

        _strCutComb   = _strCutCombfor + '&' + _strCutComb_Mass
        _strCutMoth   = _strCutMothfor + '&' + _strCutMoth_Mass
        
        ''' Stripping Lambda_c+ -> p+ eta '''
        PEta2PiPiPi0 = self.createCombinationSel(OutputList = "Lc2PEta2PiPiPi0" + self.name,
                DecayDescriptor = "[Lambda_c+ -> p+ eta]cc",
                DaughterLists   = [self.selProton, self.Eta2PiPiPi0],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        self.registerLine(self.makeLb0Line(
            line_name=self.name + "Lb2LcPiLc2Eta2PiPiPi0",
            prescale=self.config['signalPrescale'],
            Lc=PEta2PiPiPi0
            ))

        ''' Stripping Lambda_c+ -> p+ eta_prime '''
        PEtap2PiPiEta = self.createCombinationSel(OutputList = "Lc2PEtap2PiPiEta" + self.name,
                DecayDescriptor = "[Lambda_c+ -> p+ eta_prime]cc",
                DaughterLists   = [self.selProton, self.Etap2PiPiEta],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        self.registerLine(self.makeLb0Line(
            line_name=self.name + "Lb2LcPiLc2Etap2PiPiEta",
            prescale=self.config['signalPrescale'],
            Lc=PEtap2PiPiEta
            ))

    ##  --------------------  End of DpSL  ------------
    ##------------------------------------------------------------------------------------------
    def makeEta(self):
        #get standard containers
        self.Pi0List     = DataOnDemand(Location = "Phys/StdLoosePi02gg/Particles")
        self.LoosePionList    = DataOnDemand(Location = "Phys/StdLoosePions/Particles")

        #clean neutrals
        _tightpi0_cut   = "(CHILD(CL,1)>0.05) & (CHILD(CL,2)>0.05) & (PT>0.0*GeV)"

        #pipi vertex cuts
        _pipix_cut      = "(BPVVDZ>0) & (VFASPF(VCHI2)<9) & (BPVDIRA>0.95) & (BPVVDCHI2>25)"

        #pipix0 cuts
        _eta_cut        = "(ADAMASS('eta')<100*MeV)       & (APT>1500*MeV)"
        _etap_cut       = "(ADAMASS('eta_prime')<200*MeV) & (APT>1500*MeV)"

        #filter standard containers
        self.TightPi0List   = self.createSubSel( OutputList = self.name + '_TightPi0',   InputList  = self.Pi0List,     Cuts = _tightpi0_cut)

        #-------------

        self.Eta2PiPiPi0 = self.createCombinationSel( OutputList      = self.name + "_Eta2PiPiPi0",
                                                      DecayDescriptor = "eta -> pi+ pi- pi0",
                                                      DaughterLists   = [ self.LoosePionList, self.TightPi0List],
                                                      PreVertexCuts   = _eta_cut,
                                                      PostVertexCuts  = _pipix_cut)
        #-------------
        self.Etap2PiPiEta = self.createCombinationSel( OutputList      = self.name + "_Etap2PiPiEta",
                                                      DecayDescriptor = "eta_prime -> pi+ pi- eta",
                                                      DaughterLists   = [ self.LoosePionList, self.Eta2PiPiPi0],
                                                      PreVertexCuts   = _etap_cut,
                                                      PostVertexCuts  = _pipix_cut)

    ##########################################################################
    ## Basic Function
    ##########################################################################
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                Algorithm = filter,
                RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
            DecayDescriptor,
            DaughterLists,
            DaughterCuts = {} ,
            PreVertexCuts = "ALL",
            PostVertexCuts = "ALL") :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        if type(DecayDescriptor) is list:
            combiner = CombineParticles( DecayDescriptors = DecayDescriptor,
                    DaughtersCuts = DaughterCuts,
                    MotherCut = PostVertexCuts,
                    CombinationCut = PreVertexCuts,
                    ReFitPVs = True)
            return Selection ( OutputList,
                    Algorithm = combiner,
                    RequiredSelections = DaughterLists)
        else:
            combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                    DaughtersCuts = DaughterCuts,
                    MotherCut = PostVertexCuts,
                    CombinationCut = PreVertexCuts,
                    ReFitPVs = True)
            return Selection ( OutputList,
                    Algorithm = combiner,
                    RequiredSelections = DaughterLists)
