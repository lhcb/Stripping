###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for Xic+ -> Xi- pi+ pi+, Lc+ -> Xi- pi+ K+ and Xic+ -> Xi- pi+ K+.
The lines are used to complement the inclusive Xi Turbo lines by adding
Velo raw banks to study if the Xi- can be reconstructed as an upstream track.
"""
__author__ = ["Marian Stahl", "Laurent Dufour"]

__all__ = ("XcpToXiPipHpConf", "default_config")

moduleName = "XcpToXiPipHp"

# Import Packages
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from PhysSelPython.Wrappers import AutomaticData
from PhysConf.Selections import (FilterSelection, CombineSelection, Combine3BodySelection, MergedSelection)
from Configurables import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import CombineParticles

# Default configuration dictionary
default_config = {
  "NAME": "XcpToXiPipHp",
  "BUILDERTYPE": "XcpToXiPipHpConf",
  "CONFIG": {
    "descriptor_xi"     : ["[Xi- -> Lambda0 pi-]cc"],
    "descriptor_xipipi" : ["[Xi_c+ -> Xi- pi+ pi+]cc"],
    "descriptor_xikpi"  : ["[Xi_c+ -> Xi- K+ pi+]cc"],
    "RequiredRawEvents" : ["Velo"],
    "bach_pion" : {
      "tes"    : "Phys/StdAllNoPIDsPions/Particles",
      "filter" : "(P>2*GeV) & (PT>100*MeV) & (MIPDV(PRIMARY)>0.01*mm) & (MIPCHI2DV(PRIMARY)>1) & (PROBNNpi>0.03)"
    },
    "down_pion" : {
      "tes"    : "Phys/StdNoPIDsDownPions/Particles",
      "filter" : "(P>2*GeV) & (PT>150*MeV) & (PROBNNpi>0.03)"
    },
    "bach_kaon" : {
      "tes"    : "Phys/StdAllNoPIDsKaons/Particles",
      "filter" : "(P>4.5*GeV) & (PT>300*MeV) & (MIPCHI2DV(PRIMARY)>1) & (PROBNNk>0.1)"
    },
    "lambda_ll" : {
      "tes"    : "Phys/StdVeryLooseLambdaLL/Particles",
      "filter" : """(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>800*MeV) & (BPVVDZ>8*mm) & (BPVVDCHI2>32) & (CHI2VXNDF<24) &
                    (DOCA(1,2)<0.6*mm) & (DOCACHI2(1,2)<16) & (MAXTREE('p+'==ABSID,PT)>600*MeV) & (MAXTREE('p+'==ABSID,P)>7.5*GeV) &
                    (MAXTREE('p+'==ABSID,MIPCHI2DV(PRIMARY))>9) & (MAXTREE('pi+'==ABSID,MIPCHI2DV(PRIMARY))>9)"""
    },
    "lambda_dd" : {
      "tes"    : "Phys/StdLooseLambdaDD/Particles",
      "filter" : """(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>1*GeV) & (CHILDIP(1)<2*mm) & (MAXTREE('p+'==ABSID,P)>9*GeV) & (CHI2VXNDF<24) &
                    (MAXTREE('p+'==ABSID,PT)>800*MeV) & (MAXTREE('pi+'==ABSID,PT)>150*MeV)"""
    },
    "xi_lll" : {
      "comb_cut"   : "(ADOCA(1,2)<0.3*mm) & (ACHI2DOCA(1,2)<16) & (ASUM(PT)>1*GeV) & (ADAMASS('Xi-')<60*MeV) & (ACHILD(2,MIPCHI2DV(PRIMARY))>9)",
      "mother_cut" : """(ADMASS('Xi-')<30*MeV) & (P>10*GeV) & (PT>700*MeV) & (BPVVDCHI2>40) & (BPVVD>12*mm) & (CHI2VXNDF<16) &
                         (CHILDIP(1)<0.16*mm) & (CHILDIP(2)<0.28*mm) & (CHILDIPCHI2(1)<8) & (CHILDIPCHI2(2)<8)"""
    },
    "xi_ddl" : {
      "comb_cut"   : "(ADOCA(1,2)<2*mm) & (ACHI2DOCA(1,2)<24) & (ASUM(PT)>1.1*GeV) & (ADAMASS('Xi-')<60*MeV) & (ACHILD(2,MIPCHI2DV(PRIMARY))>9)",
      "mother_cut" : "(ADMASS('Xi-')<30*MeV) & (P>15*GeV) & (PT>800*MeV) & (CHI2VXNDF<16) & (CHILDIP(1)<2.5*mm) & (CHILDIP(2)<0.35*mm)"
    },
    "xi_ddd" : {
      "comb_cut"   : "(ADOCA(1,2)<3*mm) & (ACHI2DOCA(1,2)<32) & (ASUM(PT)>1.2*GeV) & (ADAMASS('Xi-')<60*MeV)",
      "mother_cut" : "(ADMASS('Xi-')<30*MeV) & (P>16*GeV) & (PT>800*MeV) & (CHI2VXNDF<16) & (CHILDIP(1)<3*mm) & (CHILDIP(2)<1*mm)"
    },
    "amass_xipipi" : "(in_range(2360*MeV,AMASS(),2580*MeV)) & ",
    "amass_xikpi"  : "(in_range(2160*MeV,AMASS(),2580*MeV)) & ",
    "mass_xipipi"  : "(in_range(2400*MeV,M,2540*MeV)) & ",
    "mass_xikpi"   : "(in_range(2220*MeV,M,2540*MeV)) & ",
    "xc_lll" : {
      "comb12_cut" : "(AMASS(1,2)<2510*MeV) & (ADOCA(1,2)<0.24*mm) & (ACHI2DOCA(1,2)<12)",
      "comb_cut"   : """(APT>1.3*GeV) & (ASUM(PT)>1.75*GeV) & (ADOCA(1,3)<0.35*mm) & (ACHI2DOCA(1,3)<10) & (ADOCA(2,3)<0.35*mm) & (ACHI2DOCA(2,3)<9) &
                        (AHASCHILD(ISBASIC & HASTRACK & (PT>300*MeV) & (P>3*GeV) & (MIPCHI2DV(PRIMARY)>2)))""",
      "mother_cut" : """(P>20*GeV) & (PT>1.5*GeV) & (BPVVDZ>0.25*mm) & (BPVVDZ<200*mm) & (CHI2VXNDF<9) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>12*mm) &
                        (CHILDIP(1)<0.15*mm) & (CHILDIP(2)<0.2*mm) & (CHILDIP(3)<0.45*mm) & (CHILDIPCHI2(1)<9) & (CHILDIPCHI2(2)<6) & (CHILDIPCHI2(3)<9) &
                        ((CHILD(2,PX)*CHILD(3,PX)+CHILD(2,PY)*CHILD(3,PY)+CHILD(2,PZ)*CHILD(3,PZ))/(CHILD(2,P)*CHILD(3,P))<0.99999998)"""
    },
    "xc_ddl" : {
      "comb12_cut" : "(AMASS(1,2)<2510*MeV) & (ADOCA(1,2)<1.5*mm) & (ACHI2DOCA(1,2)<12)",
      "comb_cut"   : """(APT>1.3*GeV) & (ASUM(PT)>1.8*GeV) & (ADOCA(1,3)<1.8*mm) & (ACHI2DOCA(1,3)<12) & (ADOCA(2,3)<0.5*mm) & (ACHI2DOCA(2,3)<12) &
                        (AHASCHILD(ISBASIC & HASTRACK & (PT>300*MeV) & (P>3*GeV) & (MIPCHI2DV(PRIMARY)>2)))""",
      "mother_cut" : """(P>20*GeV) & (PT>1.5*GeV) & (BPVVDZ>0.25*mm) & (BPVVDZ<200*mm) & (CHI2VXNDF<9) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>12*mm) &
                        (CHILDIP(1)<1.4*mm) & (CHILDIP(2)<0.2*mm) & (CHILDIP(3)<0.4*mm) & (CHILDIPCHI2(1)<12) & (CHILDIPCHI2(2)<6) & (CHILDIPCHI2(3)<9) &
                        ((CHILD(2,PX)*CHILD(3,PX)+CHILD(2,PY)*CHILD(3,PY)+CHILD(2,PZ)*CHILD(3,PZ))/(CHILD(2,P)*CHILD(3,P))<0.99999998)"""
    },
    "xc_ddd" : {
      "comb12_cut" : "(AMASS(1,2)<2510*MeV) & (ADOCA(1,2)<2*mm) & (ACHI2DOCA(1,2)<16)",
      "comb_cut"   : """(APT>1.3*GeV) & (ASUM(PT)>1.9*GeV) & (ADOCA(1,3)<3*mm) & (ACHI2DOCA(1,3)<16) & (ADOCA(2,3)<1*mm) & (ACHI2DOCA(2,3)<16) &
                        (AHASCHILD(ISBASIC & HASTRACK & (PT>300*MeV) & (P>3*GeV) & (MIPCHI2DV(PRIMARY)>2)))""",
      "mother_cut" : """(P>20*GeV) & (PT>1.5*GeV) & (BPVVDZ>0.25*mm) & (BPVVDZ<200*mm) & (CHI2VXNDF<9) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>12*mm) &
                        (CHILDIP(1)<2*mm) & (CHILDIP(2)<0.3*mm) & (CHILDIP(3)<0.4*mm) & (CHILDIPCHI2(1)<16) & (CHILDIPCHI2(2)<12) & (CHILDIPCHI2(3)<12) &
                        ((CHILD(2,PX)*CHILD(3,PX)+CHILD(2,PY)*CHILD(3,PY)+CHILD(2,PZ)*CHILD(3,PZ))/(CHILD(2,P)*CHILD(3,P))<0.99999998)"""
    },
  },
  "STREAMS" : {
      "CharmCompleteEvent" : ["StrippingXcpToXiPipHp_{0}{1}Line".format(h,tt) for h in ["Pi","K"] for tt in ["LLL","DDL","DDD"]]
  },
  "WGs": ["Charm"]
}

class XcpToXiPipHpConf(LineBuilder):

  __configuration_keys__ = default_config["CONFIG"].keys()

  def __init__(self, moduleName, config):
    LineBuilder.__init__(self, moduleName, config)
    # decay products
    bach_pion = FilterSelection(moduleName+"_bach_pion", [AutomaticData(config["bach_pion"]["tes"])], Code=config["bach_pion"]["filter"])
    down_pion = FilterSelection(moduleName+"_down_pion", [AutomaticData(config["down_pion"]["tes"])], Code=config["down_pion"]["filter"])
    bach_kaon = FilterSelection(moduleName+"_bach_kaon", [AutomaticData(config["bach_kaon"]["tes"])], Code=config["bach_kaon"]["filter"])
    lambda_ll = FilterSelection(moduleName+"_lambda_ll", [AutomaticData(config["lambda_ll"]["tes"])], Code=config["lambda_ll"]["filter"])
    lambda_dd = FilterSelection(moduleName+"_lambda_dd", [AutomaticData(config["lambda_dd"]["tes"])], Code=config["lambda_dd"]["filter"])
    xi_lll    = CombineSelection(moduleName+"_xi_lll", [lambda_ll, bach_pion], DecayDescriptors=config["descriptor_xi"],
                                 CombinationCut=config["xi_lll"]["comb_cut"], MotherCut=config["xi_lll"]["mother_cut"])
    xi_ddl    = CombineSelection(moduleName+"_xi_ddl", [lambda_dd, bach_pion], DecayDescriptors=config["descriptor_xi"],
                                 CombinationCut=config["xi_ddl"]["comb_cut"], MotherCut=config["xi_ddl"]["mother_cut"])
    xi_ddd    = CombineSelection(moduleName+"_xi_ddd", [lambda_dd, down_pion], DecayDescriptors=config["descriptor_xi"],
                                 CombinationCut=config["xi_ddd"]["comb_cut"], MotherCut=config["xi_ddd"]["mother_cut"])
    # charm baryon candidates; this could look a bit nicer when looping over track types and channels...
    xipipi_lll = Combine3BodySelection(moduleName+"_xipipi_lll", [xi_lll, bach_pion], DecayDescriptors=config["descriptor_xipipi"],
                                       Combination12Cut=config["xc_lll"]["comb12_cut"], CombinationCut=config["amass_xipipi"]+config["xc_lll"]["comb_cut"],
                                       MotherCut=config["mass_xipipi"]+config["xc_lll"]["mother_cut"])
    xipipi_ddl = Combine3BodySelection(moduleName+"_xipipi_ddl", [xi_ddl, bach_pion], DecayDescriptors=config["descriptor_xipipi"],
                                       Combination12Cut=config["xc_ddl"]["comb12_cut"], CombinationCut=config["amass_xipipi"]+config["xc_ddl"]["comb_cut"],
                                       MotherCut=config["mass_xipipi"]+config["xc_ddl"]["mother_cut"])
    xipipi_ddd = Combine3BodySelection(moduleName+"_xipipi_ddd", [xi_ddd, bach_pion], DecayDescriptors=config["descriptor_xipipi"],
                                       Combination12Cut=config["xc_ddd"]["comb12_cut"], CombinationCut=config["amass_xipipi"]+config["xc_ddd"]["comb_cut"],
                                       MotherCut=config["mass_xipipi"]+config["xc_ddd"]["mother_cut"])

    xikpi_lll  = Combine3BodySelection(moduleName+"_xikpi_lll", [xi_lll, bach_pion, bach_kaon], DecayDescriptors=config["descriptor_xikpi"],
                                       Combination12Cut=config["xc_lll"]["comb12_cut"], CombinationCut=config["amass_xikpi"]+config["xc_lll"]["comb_cut"],
                                       MotherCut=config["mass_xikpi"]+config["xc_lll"]["mother_cut"])
    xikpi_ddl  = Combine3BodySelection(moduleName+"_xikpi_ddl", [xi_ddl, bach_pion, bach_kaon], DecayDescriptors=config["descriptor_xikpi"],
                                       Combination12Cut=config["xc_ddl"]["comb12_cut"], CombinationCut=config["amass_xikpi"]+config["xc_ddl"]["comb_cut"],
                                       MotherCut=config["mass_xikpi"]+config["xc_ddl"]["mother_cut"])
    xikpi_ddd  = Combine3BodySelection(moduleName+"_xikpi_ddd", [xi_ddd, bach_pion, bach_kaon], DecayDescriptors=config["descriptor_xikpi"],
                                       Combination12Cut=config["xc_ddd"]["comb12_cut"], CombinationCut=config["amass_xikpi"]+config["xc_ddd"]["comb_cut"],
                                       MotherCut=config["mass_xikpi"]+config["xc_ddd"]["mother_cut"])
    # Create the stripping lines
    self.registerLine(StrippingLine(moduleName+"_PiLLLLine", RequiredRawEvents=config["RequiredRawEvents"], algos=[xipipi_lll]))
    self.registerLine(StrippingLine(moduleName+"_PiDDLLine", RequiredRawEvents=config["RequiredRawEvents"], algos=[xipipi_ddl]))
    self.registerLine(StrippingLine(moduleName+"_PiDDDLine", RequiredRawEvents=config["RequiredRawEvents"], algos=[xipipi_ddd]))
    self.registerLine(StrippingLine(moduleName+"_KLLLLine",  RequiredRawEvents=config["RequiredRawEvents"], algos=[xikpi_lll]))
    self.registerLine(StrippingLine(moduleName+"_KDDLLine",  RequiredRawEvents=config["RequiredRawEvents"], algos=[xikpi_ddl]))
    self.registerLine(StrippingLine(moduleName+"_KDDDLine",  RequiredRawEvents=config["RequiredRawEvents"], algos=[xikpi_ddd]))
