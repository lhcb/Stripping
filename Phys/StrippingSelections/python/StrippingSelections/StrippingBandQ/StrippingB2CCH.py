###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting B2CCH
'''

__author__=['Shuqi Sheng']
__date__ = '13/07/2023'
__version__= '$Revision: 1.0$'

__all__ = ( 
    'B2CCHConf',
    'default_config'
    )

default_config =  {
    'NAME'              :  'B2CCH',
    'BUILDERTYPE'       :  'B2CCHConf',
    'CONFIG'    : {
        'ProtonCuts'    : "(PT>100*MeV) & (PROBNNp>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0) & (TRCHI2DOF<4)",
        'KaonCuts'      : "(PT>100*MeV) & (PROBNNk>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0) & (TRCHI2DOF<4)",
        'PionCuts'      : "(PT>100*MeV) & (PROBNNpi>0.1) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>4.0)& (TRCHI2DOF<4)",
        'LcComCuts'     : "(ADOCA(1,2)<0.5*mm)",
        'LcComN3Cuts'   : "(ASUM(PT)>1000*MeV) & (ADAMASS('Lambda_c+')<110*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 500*MeV) & (P > 5000*MeV))) & (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        'LcMomCuts'     : "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)",
        'XicpComCuts'   : "(ADOCA(1,2)<0.5*mm)",
        'XicpComN3Cuts' : "(ASUM(PT)>1000*MeV) & (ADAMASS('Xi_c+')<110*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 500*MeV) & (P > 5000*MeV))) & (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        'XicpMomCuts'   : "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)",
        'Xic0barComCuts': "AALL",
        'Xic0barComN4Cuts' : "(ASUM(PT)>1000*MeV) & (ADAMASS('Xi_c~0')<110*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 300*MeV) & (P > 3000*MeV) & (TRCHI2DOF<4.)))",
        'Xic0barMomCuts': "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)",
        'DspComCuts'    : "(ADOCA(1,2)<0.5*mm)",
        'DspComN3Cuts'  : "(ASUM(PT)>500*MeV) & (ADAMASS('D_s+')<110*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 300*MeV) & (P > 3000*MeV))) & (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        'DspMomCuts'    : "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)",
        'DmComCuts'     : "(ADOCA(1,2)<0.5*mm)",
        'DmComN3Cuts'   : "(ASUM(PT)>500*MeV) & (ADAMASS('D-')<110*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 300*MeV) & (P > 3000*MeV))) & (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        'DmMomCuts'     : "(VFASPF(VCHI2/VDOF)<10.) & (BPVDIRA>0.)",
        'D0ComCuts'     : "AALL",
        'D0ComN4Cuts'   : "(ADAMASS('D0')<110*MeV) & (ASUM(PT)>500*MeV) & (AHASCHILD(ISBASIC & HASTRACK & (PT > 300*MeV) & (P > 3000*MeV) & (TRCHI2DOF<4.))) & (ADOCA(1,2)<0.5*mm) &(ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm) & (ADOCA(1,4)<0.5*mm) & (ADOCA(2,4)<0.5*mm) & (ADOCA(3,4)<0.5*mm)",
        'D0MomCuts'     : "(VFASPF(VCHI2/VDOF)<10) & (ADMASS('D0')<100*MeV) & (BPVDIRA>0.)",
        'BuComCuts'     : "AALL",
        'BuComN3Cuts'   : "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)",
        'BuMomCuts'     : "(VFASPF(VCHI2/VDOF)<10.) & (BPVLTIME()>0.2*ps) & (BPVIPCHI2()<25) & (BPVDIRA>0.)",
        'B0ComCuts'     : "AALL",
        'B0ComN4Cuts'   : "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)",
        'B0MomCuts'     : "(VFASPF(VCHI2/VDOF)<10.) & (BPVLTIME()>0.2*ps) & (BPVIPCHI2()<25) & (BPVDIRA>0.)",
        'B0ComN3Cuts'   : "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)",
        'Bs0ComCuts'    : "AALL",
        'Bs0ComN3Cuts'  : "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)",
        'Bs0MomCuts'    : "(VFASPF(VCHI2/VDOF)<10.) & (BPVLTIME()>0.2*ps) & (BPVIPCHI2()<25) & (BPVDIRA>0.)",
        'Lambdab0ComCuts'    : "AALL",
        'Lambdab0ComN3Cuts'  : "(AM<7000*MeV) & (AM>4500*MeV) & (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)",
        'Lambdab0MomCuts'    : "(VFASPF(VCHI2/VDOF)<10.) & (BPVLTIME()>0.2*ps) & (BPVIPCHI2()<25) & (BPVDIRA>0.)",
        'Prescale'      : 1.
    },  
    'STREAMS'           : ['Bhadron'],
    'WGs'               : ['BandQ'],
    }

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays, DaVinci__N4BodyDecays
from PhysSelPython.Wrappers import MergedSelection

class B2CCHConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config ):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        self.SelKaons = self.createSubSel( OutputList = self.name + "SelKaons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseKaons/Particles' ),
                                           Cuts = config['KaonCuts']
                                           )

        self.SelPions = self.createSubSel( OutputList = self.name + "SelPions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLoosePions/Particles' ),
                                           Cuts = config['PionCuts']
                                           )

        self.SelProtons = self.createSubSel( OutputList = self.name + "SelProtons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseProtons/Particles' ),
                                           Cuts = config['ProtonCuts']
                                           )
        """
        Lc-> P K Pi
        """
        self.SelLc2PKPi = self.createN3BodySel( OutputList = self.name + "SelLc2PKPi",
                                                    DaughterLists = [ self.SelProtons,self.SelKaons, self.SelPions ],
                                                    DecayDescriptor = "[ Lambda_c+ -> p+ K- pi+ ]cc",
                                                    ComAMCuts      = config['LcComCuts'],
                                                    PreVertexCuts  = config['LcComN3Cuts'],
                                                    PostVertexCuts = config['LcMomCuts']
                                                    )

        """
        Xic0bar -> P K- K- Pi+
        """
        self.SelXic0bar2PKPi = self.createN4BodySel( OutputList = self.name + "SelXic0bar2PKPi",
                                                    DaughterLists = [ self.SelProtons,self.SelKaons, self.SelPions ],
                                                    DecayDescriptor = "[ Xi_c~0 -> p+ K- K- pi+ ]cc",
                                                    ComAMCuts      = config['Xic0barComCuts'],
                                                    PreVertexCuts  = config['Xic0barComN4Cuts'],
                                                    PostVertexCuts = config['Xic0barMomCuts']
                                                    )

        """
        D_s+ -> K+ K- Pi+
        """
        self.SelDsp2KKPi = self.createN3BodySel( OutputList = self.name + "SelDsp2KKPi",
                                                    DaughterLists = [ self.SelKaons, self.SelPions ],
                                                    DecayDescriptor = "[ D_s+ -> K+ K- pi+ ]cc",
                                                    ComAMCuts      = config['DspComCuts'],
                                                    PreVertexCuts  = config['DspComN3Cuts'],
                                                    PostVertexCuts = config['DspMomCuts']
                                                    )

        """
        D0 -> K- Pi+
        """
        self.SelD02KPi = self.createCombinationSel( OutputList = self.name + "SelD02KPi",
                                                    DaughterLists = [ self.SelKaons, self.SelPions ],
                                                    DecayDescriptor = "[ D0 -> K- pi+ ]cc",
                                                    PreVertexCuts  = config['D0ComCuts'],
                                                    PostVertexCuts = config['D0MomCuts']
                                                    )
        """
        D0 -> K- Pi+ Pi- Pi+
        """
        self.SelD02K3Pi = self.createN4BodySel( OutputList = self.name + "SelD02K3Pi",
                                                    DaughterLists = [ self.SelKaons, self.SelPions ],
                                                    DecayDescriptor = "[ D0 -> K- pi+ pi- pi+ ]cc",
                                                    ComAMCuts      = config['D0ComCuts'],
                                                    PreVertexCuts  = config['D0ComN4Cuts'],
                                                    PostVertexCuts = config['D0MomCuts']
                                                    )

        """
        D- -> K+ Pi- Pi-
        """
        self.SelDm2KPiPi = self.createN3BodySel( OutputList = self.name + "SelDm2KPiPi",
                                                    DaughterLists = [ self.SelKaons, self.SelPions ],
                                                    DecayDescriptor = "[ D- -> K+ pi- pi- ]cc",
                                                    ComAMCuts      = config['DmComCuts'],
                                                    PreVertexCuts  = config['DmComN3Cuts'],
                                                    PostVertexCuts = config['DmMomCuts']
                                                    )

        """
        Xic+ -> P+ K- Pi+
        """
        self.SelXicp2PKPi = self.createN3BodySel( OutputList = self.name + "SelXicp2PKPi",
                                                    DaughterLists = [ self.SelProtons,self.SelKaons, self.SelPions ],
                                                    DecayDescriptor = "[ Xi_c+ -> p+ K- pi+ ]cc",
                                                    ComAMCuts      = config['XicpComCuts'],
                                                    PreVertexCuts  = config['XicpComN3Cuts'],
                                                    PostVertexCuts = config['XicpMomCuts']
                                                    )

        """
        B+ ->Lc Lc Pi+ 
        """
        self.SelBu2LcLcPi = self.createN3BodySel( OutputList = self.name + "SelBu2LcLcPi",
                                                              DecayDescriptor = "[ B+ -> Lambda_c+ Lambda_c~- pi+ ]cc",
                                                              DaughterLists = [ self.SelLc2PKPi, self.SelPions ],
                                                              ComAMCuts      = config['BuComCuts'],
                                                              PreVertexCuts  = config['BuComN3Cuts'],
                                                              PostVertexCuts = config['BuMomCuts'] )

        self.Bu2LcLcPiLine = StrippingLine( self.name + 'Bu2LcLcPiLine',
                                                   prescale  = config['Prescale'],
                                                   algos     = [ self.SelBu2LcLcPi ],
                                                   MaxCandidates     = 1000,
                                                   )

        self.registerLine( self.Bu2LcLcPiLine )

        """
        B0 ->Lc Lc K- Pi+ 
        """
        self.SelBd2LcLcKPi = self.createN4BodySel( OutputList = self.name + "SelBd2LcLcKPi",
                                                              DecayDescriptor = "[ B0 -> Lambda_c+ Lambda_c~- K- pi+ ]cc",
                                                              DaughterLists = [ self.SelLc2PKPi, self.SelKaons, self.SelPions ],
                                                              ComAMCuts      = config['B0ComCuts'],
                                                              PreVertexCuts  = config['B0ComN4Cuts'],
                                                              PostVertexCuts = config['B0MomCuts'] )

        self.Bd2LcLcKPiLine = StrippingLine( self.name + 'Bd2LcLcKPiLine',
                                                   prescale  = config['Prescale'],
                                                   algos     = [ self.SelBd2LcLcKPi ],
                                                   MaxCandidates     = 1000,
                                                   )

        self.registerLine( self.Bd2LcLcKPiLine )

        """
        B0 ->Lc+ Xi_c~0 Pi- 
        """
        self.SelBd2LcXi0Pi = self.createN3BodySel( OutputList = self.name + "SelBd2LcXi0Pi",
                                                              DecayDescriptor = "[ B0 -> Lambda_c+ Xi_c~0 pi- ]cc",
                                                              DaughterLists = [ self.SelLc2PKPi, self.SelXic0bar2PKPi, self.SelPions ],
                                                              ComAMCuts      = config['B0ComCuts'],
                                                              PreVertexCuts  = config['B0ComN3Cuts'],
                                                              PostVertexCuts = config['B0MomCuts'] )

        self.Bd2LcXi0PiLine = StrippingLine( self.name + 'Bd2LcXi0PiLine',
                                                   prescale  = config['Prescale'],
                                                   algos     = [ self.SelBd2LcXi0Pi ],
                                                   MaxCandidates     = 1000,
                                                   )

        self.registerLine( self.Bd2LcXi0PiLine )

        """
        Bs0 ->Lc+ Xi_c~0 K- 
        """
        self.SelBs02LcXi0K = self.createN3BodySel( OutputList = self.name + "SelBs02LcXi0K",
                                                              DecayDescriptor = "[ B_s0 -> Lambda_c+ Xi_c~0 K- ]cc",
                                                              DaughterLists = [ self.SelLc2PKPi, self.SelXic0bar2PKPi, self.SelKaons ],
                                                              ComAMCuts      = config['Bs0ComCuts'],
                                                              PreVertexCuts  = config['Bs0ComN3Cuts'],
                                                              PostVertexCuts = config['Bs0MomCuts'] )

        self.Bs02LcXi0KLine = StrippingLine( self.name + 'Bs02LcXi0KLine',
                                                   prescale  = config['Prescale'],
                                                   algos     = [ self.SelBs02LcXi0K ],
                                                   MaxCandidates     = 1000,
                                                   )

        self.registerLine( self.Bs02LcXi0KLine )

        """
        Bs0 ->Ds+ D~0(KPi) K- 
        """
        self.SelBs02DsD0KD02KPi = self.createN3BodySel( OutputList = self.name + "SelBs02DsD0KD02KPi",
                                                              DecayDescriptor = "[ B_s0 -> D_s+ D~0 K- ]cc",
                                                              DaughterLists = [ self.SelDsp2KKPi, self.SelD02KPi, self.SelKaons ],
                                                              ComAMCuts      = config['Bs0ComCuts'],
                                                              PreVertexCuts  = config['Bs0ComN3Cuts'],
                                                              PostVertexCuts = config['Bs0MomCuts'] )

        self.SelBs02DsD0KD02KPiLine = StrippingLine( self.name + 'Bs02DsD0KD02KPiLine',
                                                   prescale  = config['Prescale'],
                                                   algos     = [ self.SelBs02DsD0KD02KPi ],
                                                   MaxCandidates     = 1000,
                                                   )

        self.registerLine( self.SelBs02DsD0KD02KPiLine )

        """
        Bs0 ->Ds+ D~0(K3Pi) K- 
        """
        self.SelBs02DsD0KD02K3Pi = self.createN3BodySel( OutputList = self.name + "SelBs02DsD0KD02K3Pi",
                                                              DecayDescriptor = "[ B_s0 -> D_s+ D~0 K- ]cc",
                                                              DaughterLists = [ self.SelDsp2KKPi, self.SelD02K3Pi, self.SelKaons ],
                                                              ComAMCuts      = config['Bs0ComCuts'],
                                                              PreVertexCuts  = config['Bs0ComN3Cuts'],
                                                              PostVertexCuts = config['Bs0MomCuts'] )

        self.SelBs02DsD0KD02K3PiLine = StrippingLine( self.name + 'Bs02DsD0KD02K3PiLine',
                                                   prescale  = config['Prescale'],
                                                   algos     = [ self.SelBs02DsD0KD02K3Pi ],
                                                   MaxCandidates     = 1000,
                                                   )

        self.registerLine( self.SelBs02DsD0KD02K3PiLine )


        """
        Lambda_b0 ->D- Xic0 Pi+ 
        """
        self.SelLb02DmXic0Pi = self.createN3BodySel( OutputList = self.name + "SelLb02DmXic0Pi",
                                                              DecayDescriptor = "[ Lambda_b0 -> D- Xi_c0 pi+ ]cc",
                                                              DaughterLists = [ self.SelDm2KPiPi, self.SelXic0bar2PKPi, self.SelPions ],
                                                              ComAMCuts      = config['Lambdab0ComCuts'],
                                                              PreVertexCuts  = config['Lambdab0ComN3Cuts'],
                                                              PostVertexCuts = config['Lambdab0MomCuts'] )

        self.SelLb02DmXic0PiLine = StrippingLine( self.name + 'Lb02DmXic0PiLine',
                                                   prescale  = config['Prescale'],
                                                   algos     = [ self.SelLb02DmXic0Pi ],
                                                   MaxCandidates     = 1000,
                                                   )

        self.registerLine( self.SelLb02DmXic0PiLine )

        """
        Lambda_b0 ->D0bar(KPi) Xic+ Pi- 
        """
        self.SelLb02D0XicpPiD02KPi = self.createN3BodySel( OutputList = self.name + "SelLb02D0XicpPiD02KPi",
                                                              DecayDescriptor = "[ Lambda_b0 -> D~0 Xi_c+ pi- ]cc",
                                                              DaughterLists = [ self.SelD02KPi, self.SelXicp2PKPi, self.SelPions ],
                                                              ComAMCuts      = config['Lambdab0ComCuts'],
                                                              PreVertexCuts  = config['Lambdab0ComN3Cuts'],
                                                              PostVertexCuts = config['Lambdab0MomCuts'] )

        self.SelLb02D0XicpPiD02KPiLine = StrippingLine( self.name + 'Lb02D0XicpPiD02KPiLine',
                                                   prescale  = config['Prescale'],
                                                   algos     = [ self.SelLb02D0XicpPiD02KPi ],
                                                   MaxCandidates     = 1000,
                                                   )

        self.registerLine( self.SelLb02D0XicpPiD02KPiLine )

        """
        Lambda_b0 ->D0bar(K3Pi) Xic+ Pi- 
        """
        self.SelLb02D0XicpPiD02K3Pi = self.createN3BodySel( OutputList = self.name + "SelLb02D0XicpPiD02K3Pi",
                                                              DecayDescriptor = "[ Lambda_b0 -> D~0 Xi_c+ pi- ]cc",
                                                              DaughterLists = [ self.SelD02K3Pi, self.SelXicp2PKPi, self.SelPions ],
                                                              ComAMCuts      = config['Lambdab0ComCuts'],
                                                              PreVertexCuts  = config['Lambdab0ComN3Cuts'],
                                                              PostVertexCuts = config['Lambdab0MomCuts'] )

        self.SelLb02D0XicpPiD02K3PiLine = StrippingLine( self.name + 'Lb02D0XicpPiD02K3PiLine',
                                                   prescale  = config['Prescale'],
                                                   algos     = [ self.SelLb02D0XicpPiD02K3Pi ],
                                                   MaxCandidates     = 1000,
                                                   )

        self.registerLine( self.SelLb02D0XicpPiD02K3PiLine )


    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL") :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     CombinationCut = PreVertexCuts,
                                     MotherCut = PostVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)

    def createN3BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N3BodyDecays ( DecayDescriptor = DecayDescriptor,
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts,
                                           CombinationCut = PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)

    def createN4BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) : 
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N4BodyDecays ( DecayDescriptor = DecayDescriptor,    
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts + "&" + "( ACHI2DOCA(1,2)<20 )",
                                           Combination123Cut = ComAMCuts + "&" + "( ACHI2DOCA(1,3)<20 ) & ( ACHI2DOCA(2,3)<20 )",
                                           CombinationCut = "( ACHI2DOCA(1,4)<20 ) & ( ACHI2DOCA(2,4)<20 ) & ( ACHI2DOCA(3,4)<20 )" + " & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
