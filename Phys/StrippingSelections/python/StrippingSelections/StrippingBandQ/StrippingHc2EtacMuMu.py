###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting Hc->eta_c mu mu
'''

__author__=['Roberta Cardinale', 'Youhua Yang']
__date__ = '05/07/2023'
__version__= '$Revision: 1.0$'


__all__ = (
    'Hc2EtacMuMuConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'Hc2EtacMuMu',
    'BUILDERTYPE'       :  'Hc2EtacMuMuConf',
    'CONFIG'    : {
        'KsCuts':
            "(ADMASS('KS0') < 30.*MeV) & (BPVDLS>5) & (PT > 300*MeV) & (MAXTREE('pi-'==ABSID, PROBNNpi) > 0.1) & (MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.4) & (MAXTREE('pi-'==ABSID, TRCHI2DOF) < 5) & (MAXTREE('pi-'==ABSID, MIPCHI2DV(PRIMARY)) > 4)",
        'KaonCuts'      : "(PROBNNk > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)",
        'PionCuts'      : "(PROBNNk > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)",
        'ProtonCuts'    : "(PROBNNp > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)",        
        'MuonCuts'      : "(PROBNNmu > 0.1) & (PT < 2*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)",        
        'PhiCuts'       : "(VFASPF(VCHI2/VDOF) < 9.) & (PT > 800 * MeV) & (ADMASS('phi(1020)') < 30 * MeV)", 
                          #& (INTREE( ('K+'==ID) & (PT > 650 * MeV) & (P > 3000 * MeV) & (TRCHI2DOF < 3.) ",
        'PhiComN4Cuts'  : "",
        'KKComAMCuts' : "(AM>987.354*MeV) ",
        'ppbarComAMCuts' : "(AM>1876.544*MeV) ",
        'pipiComAMCuts' : "(AM>279.14*MeV) ",
        'EtacComN4Cuts' : #"(AM>0. *GeV)",#"
                          "(in_range(2.7*GeV, AM, 3.4*GeV))",
                          #""",
        'EtacMomN4Cuts' : #"(MM>0.*GeV)",#"
                          "(VFASPF(VCHI2/VDOF) < 9.) & (in_range(2.8*GeV, MM, 3.3*GeV))",
                          #""",
        'EtacComCuts'   : "(in_range(2.85*GeV, AM, 3.25*GeV))",
        
        'HcComCuts'     : "(ADAMASS('h_c(1P)') < 500 *MeV)",
        'HcMomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 10.) 
                          """,
        'Prescale'      : 1.,
        'RelatedInfoTools': [{
                      'Type'              : 'RelInfoVertexIsolation',
                      'Location'          : 'RelInfoVertexIsolation'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                      'Location'          : 'RelInfoConeVariables_1.0'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.5,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                      'Location'          : 'RelInfoConeVariables_1.5'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 2.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                      'Location'          : 'RelInfoConeVariables_2.0'
                  }]
    },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['BandQ'],
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays
from PhysSelPython.Wrappers import MergedSelection

class Hc2EtacMuMuConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config

        self.InputKs = MergedSelection(
            self.name + "InputKs",
            RequiredSelections=[
                DataOnDemand(Location="Phys/StdLooseKsDD/Particles"),
                DataOnDemand(Location="Phys/StdVeryLooseKsLL/Particles")
            ])
        
        self.SelKaons = self.createSubSel( OutputList = self.name + "SelKaons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseKaons/Particles' ), 
                                           Cuts = config['KaonCuts']
                                           )

        self.SelKs = self.createSubSel( OutputList=self.name + "SelKs",
                                        InputList=self.InputKs,
                                        Cuts=config['KsCuts'])


        self.SelPions = self.createSubSel( OutputList = self.name + "SelPions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLoosePions/Particles' ),
                                           Cuts = config['PionCuts']
                                           )


        self.SelProtons = self.createSubSel( OutputList = self.name + "SelProtons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseProtons/Particles' ), 
                                           Cuts = config['ProtonCuts']
                                           )

        self.SelMuons = self.createSubSel( OutputList = self.name + "SelMuons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllNoPIDsMuons/Particles' ), 
                                           Cuts = config['MuonCuts']
                                           )

        self.SelPhi = self.createSubSel( OutputList = self.name + "SelPhi",
                                                 InputList =  DataOnDemand( Location = 'Phys/StdLoosePhi2KK/Particles' ),
                                                 Cuts = config['PhiCuts']
)






        """
        Etac-> K K Pi Pi
        """
        self.SelEtac2KKPiPi = self.createN4BodySel( OutputList = self.name + "SelEtac2KKPiPi",
                                                    DaughterLists = [ self.SelKaons, self.SelPions ],
                                                    DecayDescriptor = "eta_c(1S) -> K+ K- pi+ pi-",
                                                    ComAMCuts      = config['KKComAMCuts'],
                                                    PreVertexCuts  = config['EtacComN4Cuts'], 
                                                    PostVertexCuts = config['EtacMomN4Cuts']
                                                    )

        """
        Etac-> K K K K
        """
        self.SelEtac2KKKK = self.createN4BodySel( OutputList = self.name + "SelEtac2KKKK",
                                                  DaughterLists = [ self.SelKaons ],
                                                  DecayDescriptor = "eta_c(1S) -> K+ K- K+ K-",
                                                  ComAMCuts      = config['KKComAMCuts'],
                                                  PreVertexCuts  = config['EtacComN4Cuts'], 
                                                  PostVertexCuts = config['EtacMomN4Cuts']
                                                  )

        
        """
        Etac-> Pi Pi Pi Pi
        """
        self.SelEtac2PiPiPiPi = self.createN4BodySel( OutputList = self.name + "SelEtac2PiPiPiPi",
                                                      DaughterLists = [ self.SelPions ],
                                                      DecayDescriptor = "eta_c(1S) -> pi+ pi- pi+ pi-",
                                                      ComAMCuts      = config['pipiComAMCuts'],
                                                      PreVertexCuts  = config['EtacComN4Cuts'], 
                                                      PostVertexCuts = config['EtacMomN4Cuts']
                                                      )

        """
        Etac-> p pbar Pi Pi
        """
        self.SelEtac2PPbarPiPi = self.createN4BodySel( OutputList = self.name + "SelEtac2PPbarPiPi",
                                                      DaughterLists = [ self.SelPions, self.SelProtons ],
                                                      DecayDescriptor = "eta_c(1S) -> p+ p~- pi+ pi-",
                                                      ComAMCuts      = config['ppbarComAMCuts'],
                                                      PreVertexCuts  = config['EtacComN4Cuts'], 
                                                      PostVertexCuts = config['EtacMomN4Cuts']
                                                      )

        """
        Etac-> p pbar
        """
        self.SelEtac2PPbar = self.createCombinationSel( OutputList = self.name + "SelEtac2PPbar",
                                                        DecayDescriptor = "eta_c(1S) -> p+ p~-",
                                                        DaughterLists = [ self.SelProtons ],                                                        
                                                        PreVertexCuts  = config['EtacComCuts'], 
                                                        PostVertexCuts = config['EtacMomN4Cuts']
                                                        )

        """
        Etac-> phi phi
        """
        self.SelEtac2PhiPhi = self.createCombinationSel( OutputList = self.name + "SelEtac2PhiPhi",
                                                        DecayDescriptor = "eta_c(1S) -> phi(1020) phi(1020)",
                                                        DaughterLists = [ self.SelPhi ],                                                        
                                                        PreVertexCuts  = config['EtacComCuts'], 
                                                        PostVertexCuts = config['EtacMomN4Cuts']
                                                        )
  
        """
        Etac-> Ks K Pi
        """
        self.SelEtac2KsKPi = self.createCombinationSel( OutputList = self.name + "SelEtac2KsKPi",
                                                    DecayDescriptor = "eta_c(1S) -> KS0 K- pi+",
                                                    DaughterLists = [ self.SelKs, self.SelKaons, self.SelPions ],
                                                    PreVertexCuts  = config['EtacComN4Cuts'], 
                                                    PostVertexCuts = config['EtacMomN4Cuts']
                                                    )


        """
        Eta_c
        """

        self.SelEtac = MergedSelection( self.name + "SelEtac",
                                        RequiredSelections =  [ self.SelEtac2KKPiPi, 
                                                                self.SelEtac2KKKK,
                                                                self.SelEtac2PiPiPiPi,
                                                                self.SelEtac2PPbarPiPi
                                                                ])
        
        
                
        
        
        """
        Hc->Eta_c(p pbar) mu+ mu- 
        """
        self.SelHc2EtacMuMu_PPbar = self.createCombinationSel( OutputList = self.name + "SelHc2EtacMuMu_PPbar",
                                                               DecayDescriptor = "[h_c(1P) -> eta_c(1S) mu+ mu-]cc",
                                                              DaughterLists = [ self.SelMuons, self.SelEtac2PPbar ],                    
                                                              PreVertexCuts  = config['HcComCuts'],
                                                              PostVertexCuts = config['HcMomCuts'] )

        self.Hc2EtacMuMu_PPbarLine = StrippingLine( self.name + '_PPbarLine',                                                
                                                   prescale  = config['Prescale'],
                                                  # HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelHc2EtacMuMu_PPbar ],
                                                   EnableFlavourTagging = False,
                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']                                                   
                                                   )

        self.registerLine( self.Hc2EtacMuMu_PPbarLine )


        """
        Hc->Eta_c(Ks K pi) mu+ mu- 
        """
        self.SelHc2EtacMuMu_KsKpi = self.createCombinationSel( OutputList = self.name + "SelHc2EtacMuMu_KsKpi",
                                                               DecayDescriptor = "[h_c(1P) -> eta_c(1S) mu+ mu-]cc",
                                                              DaughterLists = [ self.SelMuons, self.SelEtac2KsKPi ],                    
                                                              PreVertexCuts  = config['HcComCuts'],
                                                              PostVertexCuts = config['HcMomCuts'] )

        self.Hc2EtacMuMu_KsKpiLine = StrippingLine( self.name + '_KsKpiLine',                                                
                                                   prescale  = config['Prescale'],
                                                  # HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelHc2EtacMuMu_KsKpi ],
                                                   EnableFlavourTagging = False,
                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']                                                   
                                                   )

        self.registerLine( self.Hc2EtacMuMu_KsKpiLine )



        """
        Hc->Eta_c(phi phi) mu+ mu- 
        """
        self.SelHc2EtacMuMu_PhiPhi = self.createCombinationSel( OutputList = self.name + "SelHc2EtacMuMu_PhiPhi",
                                                               DecayDescriptor = "[h_c(1P) -> eta_c(1S) mu+ mu-]cc",
                                                              DaughterLists = [ self.SelMuons, self.SelEtac2PhiPhi ],                    
                                                              PreVertexCuts  = config['HcComCuts'],
                                                              PostVertexCuts = config['HcMomCuts'] )

        self.Hc2EtacMuMu_PhiPhiLine = StrippingLine( self.name + '_PhiPhiLine',                                                
                                                   prescale  = config['Prescale'],
                                                  # HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelHc2EtacMuMu_PhiPhi ],
                                                   EnableFlavourTagging = False,
                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']                                                   
                                                   )

        self.registerLine( self.Hc2EtacMuMu_PhiPhiLine )
        

        """                                                                                                                                       
        Hc->Eta_c(4h) mu+ mu-                                                                                                                         
        """
        self.SelHc2EtacMuMu = self.createCombinationSel( OutputList = self.name + "SelHc2EtacMuMu",
                                                         DecayDescriptor = "[h_c(1P) -> eta_c(1S) mu+ mu-]cc",
                                                        DaughterLists = [ self.SelMuons, self.SelEtac ],
                                                        PreVertexCuts  = config['HcComCuts'],
                                                        PostVertexCuts = config['HcMomCuts'] )

        
        self.Hc2EtacMuMu_4hLine            = StrippingLine( self.name + '_4hLine',                                                
                                                   prescale  = config['Prescale'],
                                                 #  HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelHc2EtacMuMu ],
                                                   EnableFlavourTagging = False,
                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']                                                   
                                                   )

        self.registerLine( self.Hc2EtacMuMu_4hLine )
        


    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)


    def createN4BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N4BodyDecays ( DecayDescriptor = DecayDescriptor,      
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts + " & " + "( ACHI2DOCA(1,2)<20 )",
                                           Combination123Cut = ComAMCuts + " & " + "( ACHI2DOCA(1,3)<20 ) & ( ACHI2DOCA(2,3)<20 )",
                                           CombinationCut = "( ACHI2DOCA(1,4)<20 ) & ( ACHI2DOCA(2,4)<20 ) & ( ACHI2DOCA(3,4)<20 )" + " & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
    

    def applyMVA( self, name, 
                  SelB,
                  MVAVars,
                  MVAxmlFile,
                  MVACutValue
                  ):
        from MVADictHelpers import addTMVAclassifierValue
        from Configurables import FilterDesktop as MVAFilterDesktop

        _FilterB = MVAFilterDesktop( name + "Filter",
                                     Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )

        addTMVAclassifierValue( Component = _FilterB,
                                XMLFile   = MVAxmlFile,
                                Variables = MVAVars,
                                ToolName  = name )
        
        return Selection( name,
                          Algorithm =  _FilterB,
                          RequiredSelections = [ SelB ] )
