###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting Lb->Lc SL decays, with Lc->Lambda Pi. Lambda decay to LL, DD, TT. For EDM/MDM study of Lambda particle
'''

__author__=['Mengzhen Wang', 'Nicola Neri', 'Tianze Rong']
__date__ = '10/07/2023'
__version__= '$Revision: 0.0$'


__all__ = (
    'Lb2LcSLForEDMConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'Lb2LcSLForEDM',
    'BUILDERTYPE'       :  'Lb2LcSLForEDMConf',
    'CONFIG'    : {
        'LbPionCuts'      : "(PROBNNpi > 0.4) & (PT > 250*MeV) & (TRGHOSTPROB<0.3) & (TRCHI2DOF < 5) & (MIPCHI2DV() > 16) & (P > 2.6*GeV)",
        'LcPionCuts'      : "(PROBNNpi > 0.4) & (PT > 150*MeV) & (TRGHOSTPROB<0.3) & (TRCHI2DOF < 5) & (MIPCHI2DV() > 16) & (P > 2.6*GeV)",
        'LbMuonCuts'      : "(PROBNNmu > 0.3) & (PT > 500*MeV) & (TRGHOSTPROB<0.3) & (TRCHI2DOF < 5) & (MIPCHI2DV() > 16)",
        'LbMuonCuts_Tight'     : "(PROBNNmu > 0.5) & (PT > 1.8*GeV) & (TRGHOSTPROB<0.3) & (TRCHI2DOF < 5) & (MIPCHI2DV() > 45)",
        'LcPionCuts_Tight'     : "(PROBNNpi > 0.5) & (PT > 800*MeV) & (TRGHOSTPROB<0.3) & (TRCHI2DOF < 5) & (MIPCHI2DV() > 30) & (P > 2.6*GeV)", 
        'LambdaLLCuts'    : "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>0.8*GeV) & (BPVVDZ>10*mm) & (BPVVDCHI2>32) & (DOCA(1,2)<0.5*mm) & (DOCACHI2(1,2)<16) & (MAXTREE('p+'==ABSID,P)>7.5*GeV) & (MAXTREE('p+'==ABSID,PROBNNp)>0.15) & (MAXTREE('p+'==ABSID,MIPCHI2DV(PRIMARY))>9) & (MAXTREE('pi+'==ABSID,MIPCHI2DV(PRIMARY))>9) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.1)", 
        'LambdaDDCuts'    : "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>1*GeV) & (MAXTREE('p+'==ABSID,P)>9*GeV) & (MAXTREE('p+'==ABSID,PROBNNp)>0.15) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.1)", 
        'PiPiMuComAMCuts' : "(AM<3.45*GeV)", # Lb mass - Lc mass - neutrino mass about 3333MeV. 
        'PiPiMuCom12AMCuts' : "(AM<3.45*GeV)", # For speeding up the 12+3 combination
        'PiPiMuCom12AMCuts_RhoRegion' : "(AM<1.0*GeV)", # Focus on the Rho region 
        'PiPiMuCom12AMCuts_AboveRho' : "(AM>1.0*GeV) & (AM<3.45*GeV)", # Focus on above Rho region
        'PiPiMuComN3Cuts' : """
                          (in_range(0.3*GeV, AM, 3.45*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3) ) > 2.0 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3))>60)
                          """, # PT too tight?
        'PiPiMuComN3Cuts_Tight' : """
                          (in_range(0.3*GeV, AM, 3.45*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3) ) > 4. *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3))>80)
                          """, 
        'PiPiMuMomN3Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 9.) 
                           & (in_range(0.35*GeV, MM, 3.4*GeV)) 
                           & (MIPCHI2DV(PRIMARY) > 9.) 
                           & (BPVVDCHI2>10) 
                           & (BPVDIRA>0.)
                           """,
        'PiPiMuMomN3Cuts_Tight' : """
                           (VFASPF(VCHI2/VDOF) < 4.) 
                           & (in_range(0.35*GeV, MM, 3.4*GeV)) 
                           & (MIPCHI2DV(PRIMARY) > 36.) 
                           & (BPVVDCHI2>160) 
                           & (BPVDIRA>0.)
                           """,
        'PiPiPiComAMCuts' : "(AM<1.2*GeV)", 
        'PiPiPiComN3Cuts' : """
                          (in_range(0.3*GeV, AM, 1.2*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3) ) > 2.0 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3))>60)
                          """, # PT too too tight
        'PiPiPiComN3Cuts_Tight' : """
                          (in_range(0.3*GeV, AM, 1.2*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3) ) > 4.5 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3))>100)
                          """, # PT too too tight
        'PiPiPiMomN3Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 9.) 
                           & (in_range(0.35*GeV, MM, 1.19*GeV)) 
                           & (MIPCHI2DV(PRIMARY) > 9.) 
                           & (BPVVDCHI2>10) 
                           & (BPVDIRA>0.)
                           """,
        'PiPiPiMomN3Cuts_Tight' : """
                           (VFASPF(VCHI2/VDOF) < 4.) 
                           & (in_range(0.35*GeV, MM, 1.19*GeV)) 
                           & (MIPCHI2DV(PRIMARY) > 49.) 
                           & (BPVVDCHI2>180) 
                           & (BPVDIRA>0.)
                           """,
        'LcCombCuts_LL'   : "(ADAMASS('Lambda_c+') < 70.0)",  
        'LcMotherCuts_LL' : "(ADMASS('Lambda_c+') < 60.0) & (VFASPF(VCHI2/VDOF)<10)",  
        'LcCombCuts_DD'   : "(ADAMASS('Lambda_c+') < 110.0)",  
        'LcMotherCuts_DD' : "(ADMASS('Lambda_c+') < 100.0) & (VFASPF(VCHI2/VDOF)<10)",  
        'LbCombCuts_LL'   : "( AM < 6.0*GeV )",  
        'LbMotherCuts_LL' : "( M < 5.9*GeV ) & (VFASPF(VCHI2/VDOF)<10)",  
        'LbCombCuts_DD'   : "( AM < 6.0*GeV )",  
        'LbMotherCuts_DD' : "( M < 5.9*GeV ) & (VFASPF(VCHI2/VDOF)<10) ",
        'PiPiMu3PiMomCuts': "( M < 5.0*GeV ) & (VFASPF(VCHI2/VDOF) > 16.)",
        'Mu3PiMomCuts'    : "( M < 4.7*GeV ) & (VFASPF(VCHI2/VDOF) > 16.)",
        'PiPiMuPiMomCuts' : "( M < 4.7*GeV ) & (VFASPF(VCHI2/VDOF) > 16.)",
        'IsoMax' : "4.0 * GeV",
        },
    'STREAMS'           : ['BhadronCompleteEvent' ],
    'WGs'               : ['BandQ'],
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays

class Lb2LcSLForEDMConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config


        self.SelLbPions = self.createSubSel( OutputList = self.name + "SelLbPions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLoosePions/Particles' ), 
                                           Cuts = config['LbPionCuts']
                                           )
        
        self.SelLbMuons = self.createSubSel( OutputList = self.name + "SelLbMuons",
                                             InputList =  DataOnDemand(Location = 'Phys/StdLooseMuons/Particles' ), 
                                             Cuts = config['LbMuonCuts']
                                           )
        
        self.SelLcPions = self.createSubSel( OutputList = self.name + "SelLcPions",
                                             InputList =  DataOnDemand(Location = 'Phys/StdLoosePions/Particles' ), 
                                             Cuts = config['LcPionCuts']
                                           )

        self.SelLbMuon_Tight = self.createSubSel( OutputList = self.name + "SelLbMuon_Tight",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseMuons/Particles' ), 
                                           Cuts = config['LbMuonCuts_Tight']
                                           )

        self.SelLcPion_Tight = self.createSubSel( OutputList = self.name + "SelLcPion_Tight",
                                             InputList =  DataOnDemand(Location = 'Phys/StdLoosePions/Particles' ), 
                                             Cuts = config['LcPionCuts_Tight']
                                           )

        self.SelLambdaLL = self.createSubSel( OutputList = self.name + "SelLambdaLL",
                                              InputList =  DataOnDemand(Location = 'Phys/StdVeryLooseLambdaLL/Particles' ), 
                                              Cuts = config['LambdaLLCuts']
                                            )

        self.SelLambdaDD = self.createSubSel( OutputList = self.name + "SelLambdaDD",
                                              InputList =  DataOnDemand(Location = 'Phys/StdLooseLambdaDD/Particles' ), 
                                              Cuts = config['LambdaDDCuts']
                                            )

        """
        The PiPiMu vertex
        """
        self.SelPiPiMu = self.createN3BodySel( OutputList = self.name + "SelPiPiMu",
                                               DaughterLists = [ self.SelLbPions, self.SelLbMuons ],
                                               DecayDescriptor = "[a_0(1450)- -> pi+ pi- mu-]cc",
                                               Com12AMCuts      = config['PiPiMuCom12AMCuts'],
                                               ComAMCuts      = config['PiPiMuComAMCuts'],
                                               PreVertexCuts  = config['PiPiMuComN3Cuts'], 
                                               PostVertexCuts = config['PiPiMuMomN3Cuts']
                                               )

        self.SelPiPiMu_Tight = self.createN3BodySel( OutputList = self.name + "SelPiPiMu_Tight",
                                               DaughterLists = [ self.SelLbPions, self.SelLbMuons ],
                                               DecayDescriptor = "[a_0(1450)- -> pi+ pi- mu-]cc",
                                               Com12AMCuts      = config['PiPiMuCom12AMCuts'],
                                               ComAMCuts      = config['PiPiMuComAMCuts'],
                                               PreVertexCuts  = config['PiPiMuComN3Cuts_Tight'], 
                                               PostVertexCuts = config['PiPiMuMomN3Cuts_Tight']
                                               )

        self.SelPiPiMu_Tight_RhoRegion = self.createN3BodySel( OutputList = self.name + "SelPiPiMu_Tight_RhoRegion",
                                               DaughterLists = [ self.SelLbPions, self.SelLbMuons ],
                                               DecayDescriptor = "[a_0(1450)- -> pi+ pi- mu-]cc",
                                               Com12AMCuts      = config['PiPiMuCom12AMCuts_RhoRegion'],
                                               ComAMCuts      = config['PiPiMuComAMCuts'],
                                               PreVertexCuts  = config['PiPiMuComN3Cuts_Tight'], 
                                               PostVertexCuts = config['PiPiMuMomN3Cuts_Tight']
                                               )

        self.SelPiPiMu_Tight_AboveRho = self.createN3BodySel( OutputList = self.name + "SelPiPiMu_Tight_AboveRho",
                                               DaughterLists = [ self.SelLbPions, self.SelLbMuons ],
                                               DecayDescriptor = "[a_0(1450)- -> pi+ pi- mu-]cc",
                                               Com12AMCuts      = config['PiPiMuCom12AMCuts_AboveRho'],
                                               ComAMCuts      = config['PiPiMuComAMCuts'],
                                               PreVertexCuts  = config['PiPiMuComN3Cuts_Tight'], 
                                               PostVertexCuts = config['PiPiMuMomN3Cuts_Tight']
                                               )

        """
        The PiPiPi vertex
        """
        self.SelPiPiPi = self.createN3BodySel( OutputList = self.name + "SelPiPiPi",
                                               DaughterLists = [ self.SelLcPions ],
                                               DecayDescriptor = "[b_1(1235)+ -> pi+ pi- pi+]cc",
                                               Com12AMCuts      = config['PiPiPiComAMCuts'],
                                               ComAMCuts      = config['PiPiPiComAMCuts'],
                                               PreVertexCuts  = config['PiPiPiComN3Cuts'],
                                               PostVertexCuts = config['PiPiPiMomN3Cuts']
                                               )

        self.SelPiPiPi_Tight = self.createN3BodySel( OutputList = self.name + "SelPiPiPi_Tight",
                                               DaughterLists = [ self.SelLcPions ],
                                               DecayDescriptor = "[b_1(1235)+ -> pi+ pi- pi+]cc",
                                               Com12AMCuts      = config['PiPiPiComAMCuts'],
                                               ComAMCuts      = config['PiPiPiComAMCuts'],
                                               PreVertexCuts  = config['PiPiPiComN3Cuts_Tight'],
                                               PostVertexCuts = config['PiPiPiMomN3Cuts_Tight']
                                               )
        """
        Lc->Lambda Pi, Lambda->p pi (LL, DD)
        """
        self.SelLc2LambdaPi_LL = self.createCombinationSel( OutputList = self.name + "SelLc2LambdaPi_LL", 
                                                             DecayDescriptor = "[Lambda_c+ -> Lambda0  pi+]cc",
                                                             DaughterLists = [ self.SelLambdaLL, self.SelLcPions ],
                                                             PreVertexCuts = config['LcCombCuts_LL'], 
                                                             PostVertexCuts = config['LcMotherCuts_LL'] )


        self.SelLc2LambdaPi_DD = self.createCombinationSel( OutputList = self.name + "SelLc2LambdaPi_DD", 
                                                             DecayDescriptor = "[Lambda_c+ -> Lambda0  pi+]cc",
                                                             DaughterLists = [ self.SelLambdaDD, self.SelLcPions ],
                                                             PreVertexCuts = config['LcCombCuts_DD'], 
                                                             PostVertexCuts = config['LcMotherCuts_DD'] )


        """
        Lc->Lambda 3Pi, Lambda->p pi (LL, DD)
        """
        self.SelLc2Lambda3Pi_LL = self.createCombinationSel( OutputList = self.name + "SelLc2Lambda3Pi_LL",
                                                              DecayDescriptor = "[Lambda_c+ -> Lambda0  pi+ pi- pi+]cc",
                                                              DaughterLists = [ self.SelLambdaLL, self.SelLcPions ],
                                                              PreVertexCuts = config['LcCombCuts_LL'],
                                                              PostVertexCuts = config['LcMotherCuts_LL'] )

        self.SelLc2Lambda3Pi_DD = self.createCombinationSel( OutputList = self.name + "SelLc2Lambda3Pi_DD",
                                                              DecayDescriptor = "[Lambda_c+ -> Lambda0  pi+ pi- pi+]cc",
                                                              DaughterLists = [ self.SelLambdaDD, self.SelLcPions ],
                                                              PreVertexCuts = config['LcCombCuts_DD'],
                                                              PostVertexCuts = config['LcMotherCuts_DD'] )

        """
        PiPiMu vertex + one additional detached long track pion. For Lb->LcPiPiMuNu, Lc->Lambda Pi, TT case. PiPi mass in Rho region 
        """
        self.SelPiPiMu_Pi_RhoRegion = self.createCombinationSel( OutputList = self.name + "SelPiPiMu_Pi_RhoRegion", 
                                                       DecayDescriptor = "[Lambda_b0 -> a_0(1450)- pi+]cc",
                                                       DaughterLists = [ self.SelPiPiMu_Tight_RhoRegion, self.SelLcPion_Tight ],
                                                       PostVertexCuts = config["PiPiMuPiMomCuts"])



        self.Lb2LcPiPiMu_Lc2LambdaPi_RhoRegion_TTLine = StrippingLine( self.name + '_Lb2LcPiPiMu_Lc2LambdaPi_RhoRegion_TTLine',
                                                   prescale  = 0.25,
                                                   algos     = [ self.SelPiPiMu_Pi_RhoRegion ],
                                                   checkPV   = True,
                                                   RequiredRawEvents = [ "Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker" ],
                                                   )

        self.registerLine( self.Lb2LcPiPiMu_Lc2LambdaPi_RhoRegion_TTLine )



        """
        PiPiMu vertex + one additional detached long track pion. For Lb->LcPiPiMuNu, Lc->Lambda Pi, TT case. PiPi mass above Rho region 
        """
        self.SelPiPiMu_Pi_AboveRho = self.createCombinationSel( OutputList = self.name + "SelPiPiMu_Pi_AboveRho", 
                                                       DecayDescriptor = "[Lambda_b0 -> a_0(1450)- pi+]cc",
                                                       DaughterLists = [ self.SelPiPiMu_Tight_AboveRho, self.SelLcPion_Tight ],
                                                       PostVertexCuts = config["PiPiMuPiMomCuts"])

        self.Lb2LcPiPiMu_Lc2LambdaPi_AboveRho_TTLine = StrippingLine( self.name + '_Lb2LcPiPiMu_Lc2LambdaPi_AboveRho_TTLine',
                                                   prescale  = 1.,
                                                   algos     = [ self.SelPiPiMu_Pi_AboveRho ],
                                                   checkPV   = True,
                                                   RequiredRawEvents = [ "Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker" ],
                                                   )

        self.registerLine( self.Lb2LcPiPiMu_Lc2LambdaPi_AboveRho_TTLine )

       
        """
        PiPiMu vetex + one additional PiPiPi detached vertex. For Lb->LcPiPiMuNu, Lc-> Lambda 3pi, TT case
        """
        self.SelPiPiMu_PiPiPi = self.createCombinationSel( OutputList = self.name + "SelPiPiMu_PiPiPi",
                                                           DecayDescriptor = "[Lambda_b0 -> a_0(1450)- b_1(1235)+ ]cc",
                                                           DaughterLists = [ self.SelPiPiMu, self.SelPiPiPi ],
                                                           PostVertexCuts = config["PiPiMu3PiMomCuts"])

        self.Lb2LcPiPiMu_Lc2Lambda3Pi_TTLine = StrippingLine( self.name + '_Lb2LcPiPiMu_Lc2Lambda3Pi_TTLine',                     
                                                   prescale  = 1.,
                                                   algos     = [  self.SelPiPiMu_PiPiPi ],
                                                   checkPV   = True,
                                                   RequiredRawEvents = [ "Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker" ],
                                                   )

        self.registerLine( self.Lb2LcPiPiMu_Lc2Lambda3Pi_TTLine )


        """
        Long track muon + one additional PiPiPi detached vertex. For Lb->LcMuNu, Lc-> Lambda 3pi, TT case
        """
        self.SelMu_PiPiPi = self.createCombinationSel( OutputList = self.name + "SelMu_PiPiPi",
                                                           DecayDescriptor = "[Lambda_b0 -> mu- b_1(1235)+ ]cc",
                                                           DaughterLists = [ self.SelLbMuon_Tight, self.SelPiPiPi_Tight ],
                                                           PostVertexCuts = config["Mu3PiMomCuts"])

        self.Lb2LcMu_Lc2Lambda3Pi_TTLine = StrippingLine( self.name + '_Lb2LcMu_Lc2Lambda3Pi_TTLine',
                                                   prescale  = 1.,
                                                   algos     = [  self.SelMu_PiPiPi ],
                                                   checkPV   = True,
                                                   RequiredRawEvents = [ "Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker" ],
                                                   )

        self.registerLine( self.Lb2LcMu_Lc2Lambda3Pi_TTLine )

        
        """
        Lb->LcPiPiMuNu, Lc->LambdaPi, LL DD case
        """
        self.SelLb2LcPiPiMu_Lc2LambdaPi_LL = self.createCombinationSel( OutputList = self.name + "_Lb2LcPiPiMu_Lc2LambdaPi_LL",
                                                                        DecayDescriptor = "[Lambda_b0 -> Lambda_c+ a_0(1450)- ]cc",
                                                                        DaughterLists = [ self.SelLc2LambdaPi_LL, self.SelPiPiMu ],
                                                                        PreVertexCuts = config['LbCombCuts_LL'],
                                                                        PostVertexCuts = config['LbMotherCuts_LL'])

        self.Lb2LcPiPiMu_Lc2LambdaPi_LLLine = StrippingLine( self.name + '_Lb2LcPiPiMu_Lc2LambdaPi_LLLine',                     
                                                   prescale  = 1.,
                                                   algos     = [  self.SelLb2LcPiPiMu_Lc2LambdaPi_LL ],
                                                   checkPV   = True,
                                                   RequiredRawEvents = [ "Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker" ]
                                                   )

        self.registerLine( self.Lb2LcPiPiMu_Lc2LambdaPi_LLLine )


        
        self.SelLb2LcPiPiMu_Lc2LambdaPi_DD = self.createCombinationSel( OutputList = self.name + "_Lb2LcPiPiMu_Lc2LambdaPi_DD",
                                                                        DecayDescriptor = "[Lambda_b0 -> Lambda_c+ a_0(1450)- ]cc",
                                                                        DaughterLists = [ self.SelLc2LambdaPi_DD, self.SelPiPiMu ],
                                                                        PreVertexCuts = config['LbCombCuts_DD'],
                                                                        PostVertexCuts = config['LbMotherCuts_DD'])

        self.Lb2LcPiPiMu_Lc2LambdaPi_DDLine = StrippingLine( self.name + '_Lb2LcPiPiMu_Lc2LambdaPi_DDLine',
                                                   prescale  = 1.,
                                                   algos     = [  self.SelLb2LcPiPiMu_Lc2LambdaPi_DD ],
                                                   checkPV   = True,
                                                   RequiredRawEvents = [ "Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker" ]
                                                   )

        self.registerLine( self.Lb2LcPiPiMu_Lc2LambdaPi_DDLine )



        """
        Lb->LcPiPiMuNu, Lc->Lambda3Pi, LL DD case
        """                                    
        self.SelLb2LcPiPiMu_Lc2Lambda3Pi_LL = self.createCombinationSel( OutputList = self.name + "_Lb2LcPiPiMu_Lc2Lambda3Pi_LL",
                                                                        DecayDescriptor = "[Lambda_b0 -> Lambda_c+ a_0(1450)- ]cc",
                                                                        DaughterLists = [ self.SelLc2Lambda3Pi_LL, self.SelPiPiMu ],
                                                                        PreVertexCuts = config['LbCombCuts_LL'],
                                                                        PostVertexCuts = config['LbMotherCuts_LL'])

        self.Lb2LcPiPiMu_Lc2Lambda3Pi_LLLine = StrippingLine( self.name + '_Lb2LcPiPiMu_Lc2Lambda3Pi_LLLine',
                                                   prescale  = 1.,
                                                   algos     = [  self.SelLb2LcPiPiMu_Lc2Lambda3Pi_LL ],
                                                   checkPV   = True,
                                                   RequiredRawEvents = [ "Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker" ]
                                                   )
                                               
        self.registerLine( self.Lb2LcPiPiMu_Lc2Lambda3Pi_LLLine )
                                               

        
        self.SelLb2LcPiPiMu_Lc2Lambda3Pi_DD = self.createCombinationSel( OutputList = self.name + "_Lb2LcPiPiMu_Lc2Lambda3Pi_DD",
                                                                        DecayDescriptor = "[Lambda_b0 -> Lambda_c+ a_0(1450)- ]cc",
                                                                        DaughterLists = [ self.SelLc2Lambda3Pi_DD, self.SelPiPiMu ],
                                                                        PreVertexCuts = config['LbCombCuts_DD'],
                                                                        PostVertexCuts = config['LbMotherCuts_DD'])
                                                             
        self.Lb2LcPiPiMu_Lc2Lambda3Pi_DDLine = StrippingLine( self.name + '_Lb2LcPiPiMu_Lc2Lambda3Pi_DDLine',
                                                   prescale  = 1.,
                                                   algos     = [  self.SelLb2LcPiPiMu_Lc2Lambda3Pi_DD ],
                                                   checkPV   = True,
                                                   RequiredRawEvents = [ "Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker" ]
                                                   )         
                                                             
        self.registerLine( self.Lb2LcPiPiMu_Lc2Lambda3Pi_DDLine )



        """
        Lb->LcMuNu, Lc->Lambda3Pi, LL DD case
        """
        self.SelLb2LcMu_Lc2Lambda3Pi_LL = self.createCombinationSel( OutputList = self.name + "_Lb2LcMu_Lc2Lambda3Pi_LL",
                                                                        DecayDescriptor = "[Lambda_b0 -> Lambda_c+ mu- ]cc",
                                                                        DaughterLists = [ self.SelLc2Lambda3Pi_LL, self.SelLbMuons ],
                                                                        PreVertexCuts = config['LbCombCuts_LL'],
                                                                        PostVertexCuts = config['LbMotherCuts_LL'])
        
        self.Lb2LcMu_Lc2Lambda3Pi_LLLine = StrippingLine( self.name + '_Lb2LcMu_Lc2Lambda3Pi_LLLine',
                                                   prescale  = 1.,
                                                   algos     = [  self.SelLb2LcMu_Lc2Lambda3Pi_LL ],
                                                   checkPV   = True,
                                                   RequiredRawEvents = [ "Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker" ]
                                                   )
 
        self.registerLine( self.Lb2LcMu_Lc2Lambda3Pi_LLLine )
 

 
        self.SelLb2LcMu_Lc2Lambda3Pi_DD = self.createCombinationSel( OutputList = self.name + "_Lb2LcMu_Lc2Lambda3Pi_DD",
                                                                        DecayDescriptor = "[Lambda_b0 -> Lambda_c+ mu- ]cc",
                                                                        DaughterLists = [ self.SelLc2Lambda3Pi_DD, self.SelLbMuons ],
                                                                        PreVertexCuts = config['LbCombCuts_DD'],
                                                                        PostVertexCuts = config['LbMotherCuts_DD'])
 
        self.Lb2LcMu_Lc2Lambda3Pi_DDLine = StrippingLine( self.name + '_Lb2LcMu_Lc2Lambda3Pi_DDLine',
                                                   prescale  = 1.,
                                                   algos     = [  self.SelLb2LcMu_Lc2Lambda3Pi_DD ],
                                                   checkPV   = True,
                                                   RequiredRawEvents = [ "Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker" ]
                                                   )
 
        self.registerLine( self.Lb2LcMu_Lc2Lambda3Pi_DDLine )

        


    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "AALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)


    def createN3BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         Com12AMCuts      = "AALL",
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N3BodyDecays ( DecayDescriptor = DecayDescriptor,      
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = Com12AMCuts + "&" + "( ACHI2DOCA(1,2)<20 )",
                                           CombinationCut = ComAMCuts + "&" + "( ACHI2DOCA(1,3)<20 ) & ( ACHI2DOCA(2,3)<20 ) & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
