###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -----------------------------------------------------------------------------------------------------------------------------------------
# Stripping line for BsJpsiK* analysis: remove PID cuts in the original BetaSBs2JpsiKstarWideLine
# Line name: BetaSBs2JpsiKstarWide_NoPID
# -----------------------------------------------------------------------------------------------------------------------------------------
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllLooseMuons
from StandardParticles import StdLoosePions
from StandardParticles import StdNoPIDsKaons
from GaudiKernel.SystemOfUnits import MeV


__author__ = ['Carlos Vazquez Sierra', 'Jie Wu']
__date__ = '09/07/2023'
__version__ = '$Revision: 1.0$'
__all__ = ('B2JpsiXforBeta_s_NoPID_Conf', 'default_config')


default_config = {
    'NAME': 'BetaS_NoPID',
    'WGs': ['B2CC'],
    'BUILDERTYPE': 'B2JpsiXforBeta_s_NoPID_Conf',
    'CONFIG': {
        'JpsiMassWindow':       80,       
        'VCHI2PDOF':            10,
        #                 ,       'PIDKCuts'                  :       0.
        #                 ,       'PIDpiCuts'                 :       0.
    },
    'STREAMS': {
        'Leptonic': [
            'StrippingBetaS_NoPIDBs2JpsiKstarWideLine',
        ],
    }
}

# Lines stored in this file:
# StrippingBetaSBs2JpsiKstarWideNoPIDLine


class B2JpsiXforBeta_s_NoPID_Conf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        # Input selections:

        # ------------------------------------------------------------ J/Psi:
        self.JpsiList = self.createCombinationsSel(OutputList="NarrowJpsiListForBsJpsiKstarWide" + self.name,
                                                   DaughterLists=[
                                                       StdAllLooseMuons],
                                                   DecayDescriptors=[
                                                       "J/psi(1S) -> mu+ mu-"],
                                                   DaughterCuts={
                                                       "mu+": " (PT > 500 *MeV)"},
                                                   PreVertexCuts="(ADAMASS('J/psi(1S)') < %(JpsiMassWindow)s * MeV) & (ADOCACHI2CUT(20, ''))" % self.config,
                                                   PostVertexCuts="(VFASPF(VCHI2) < 16.) & (MFIT)",
                                                   ReFitPVs=False)

        # ------------------------------------------------------------ Kstar:
        self.KstarWideList = self.createCombinationsSel(OutputList="KstarWideListForBsJpsiKstarWide" + self.name,
                                                        # StdLoosePions do not contain PID cut
                                                        DaughterLists=[
                                                            StdNoPIDsKaons, StdLoosePions],
                                                        DecayDescriptors=[
                                                            "[K*(892)0 -> K+ pi-]cc", "[K*_0(1430)0 -> K+ pi-]cc"],
                                                        DaughterCuts={"pi-": "(PT > 500 *MeV) & (TRGHOSTPROB < 0.8)",
                                                                      "K+": "HASRICH & (PT > 500 *MeV) & (TRGHOSTPROB < 0.8)"},  # conpensate RICH
                                                        PreVertexCuts="(in_range(750,AM,1900))  & (ADOCACHI2CUT(30, ''))",
                                                        PostVertexCuts="(VFASPF(VCHI2) < 25)",
                                                        ReFitPVs=False)
        # -------------------------------------------------------------------
        self.makeBs2JpsiKstarWide()  # Making the line.
    # ---------------------------------------------------------------------------------------------------------------------------------

    def createSubSel(self, OutputList, InputList, Cuts):
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code=Cuts)
        return Selection(OutputList,
                         Algorithm=filter,
                         RequiredSelections=[InputList])
    # ---------------------------------------------------------------------------------------------------------------------------------

    def createCombinationSel(self, OutputList,
                             DecayDescriptor,
                             DaughterLists,
                             DaughterCuts={},
                             PreVertexCuts="ALL",
                             PostVertexCuts="ALL",
                             ReFitPVs=True):
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles(DecayDescriptor=DecayDescriptor,
                                    DaughtersCuts=DaughterCuts,
                                    MotherCut=PostVertexCuts,
                                    CombinationCut=PreVertexCuts,
                                    ReFitPVs=ReFitPVs)
        return Selection(OutputList,
                         Algorithm=combiner,
                         RequiredSelections=DaughterLists)
    # ---------------------------------------------------------------------------------------------------------------------------------

    def createCombinationsSel(self, OutputList,
                              DecayDescriptors,
                              DaughterLists,
                              DaughterCuts={},
                              PreVertexCuts="ALL",
                              PostVertexCuts="ALL",
                              ReFitPVs=True):
        '''For taking in multiple decay descriptors'''
        combiner = CombineParticles(DecayDescriptors=DecayDescriptors,
                                    DaughtersCuts=DaughterCuts,
                                    MotherCut=PostVertexCuts,
                                    CombinationCut=PreVertexCuts,
                                    ReFitPVs=ReFitPVs)
        return Selection(OutputList,
                         Algorithm=combiner,
                         RequiredSelections=DaughterLists)
    # ---------------------------------------------------------------------------------------------------------------------------------

    def makeBs2JpsiKstarWide(self):  # Line maker.
        Bs2JpsiKstarWide = self.createCombinationSel(OutputList="Bs2JpsiKstarWide" + self.name,
                                                     DecayDescriptor="[B_s~0 -> J/psi(1S) K*(892)0]cc",
                                                     DaughterLists=[
                                                         self.JpsiList, self.KstarWideList],
                                                     PreVertexCuts="in_range(5100,AM,5700)",
                                                     PostVertexCuts="(VFASPF(VCHI2PDOF) < %(VCHI2PDOF)s) & (BPVDIRA >0.999) & (BPVVD > 1.5 *mm)" % self.config)
        Bs2JpsiKstarWideLine = StrippingLine(self.name + "Bs2JpsiKstarWideLine", algos=[Bs2JpsiKstarWide], EnableFlavourTagging=True, RequiredRawEvents=[
                                             "Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"])  # , MDSTFlag = True )
        self.registerLine(Bs2JpsiKstarWideLine)
    # ---------------------------------------------------------------------------------------------------------------------------------
