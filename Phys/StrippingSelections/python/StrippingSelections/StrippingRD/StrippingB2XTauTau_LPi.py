###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for
    Bs to K K tau tau
    Bs to K* K* tau tau
    B0 to K pi tau tau
    Lb to p K tau tau
Contains one tau decay to pi and one tau decay to leptons 
Same-sign combinations are included.
"""

__author__ = 'H. Tilquin'
__date__ = '09/06/2023'
__version__ = '$Revision: 0.0 $'

__all__ = ('B2XTauTauLeptonicConf', 'default_config')

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop, DaVinci__N3BodyDecays

from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME': 'B2XTauTau_LPi',
    'BUILDERTYPE': 'B2XTauTau_LPiConf',
    'CONFIG':
        {
            "Bs_Comb_MassHigh": 6750.0,
            "Bd_Comb_MassHigh": 6300.0,
            "Lb_Comb_MassHigh": 7250.0,
            "Bs_VertDist": -2,  
            "Bs_VertDist_E": -1,
            "Bd_VertDist":  0,
            "Bd_VertDist_E":  0.5, 
            "Lb_VertDist": -2,
            "Lb_VertDist_E": -1,
            "B_DIRA": 0.9995,
            "Lb_DIRA":  0.9995,
            "Bs_VertexCHI2": 50.0,
            "B0_VertexCHI2": 50.0,
            "Lb_VertexCHI2": 50.0,
            "Dau_DIRA": 0.995,
            "DiMuPi_DIRA": 0.97,
            "DiEPi_DIRA": 0.97, # 0.98,
            "Hadron_MinIPCHI2": 25.0,
            "Muon_MinIPCHI2": 12.0,
            "Electron_MinIPCHI2": 16.0,
            "PionFromTau_MinIPCHI2": 16.0,
            "DiMuPiUPPERMASS": 4350.0,
            "DiEPiUPPERMASS": 4350.0,
            "Phi_FlightChi2": 36.0,
            "Phi_Comb_MassHigh": 1850.0,
            "Phi_PT": 600,
            "Phi_VertexCHI2": 4,
            "Kstar_for_B2Kstar_FlightChi2": 250.0,
            "Kstar_for_B2Kstar_Comb_MassHigh": 1750.0,
            "Kstar_for_B2Kstar_Comb_MassLow": 795.0,
            "Kstar_for_B2Kstar_PT": 1600,
            "Kstar_for_B2Kstar_VertexCHI2": 3,
            "Kstar_for_B2KstarKstar_MassWindow": 100,
            "Kstar_for_B2KstarKstar_PT": 600,
            "Kstar_for_B2KstarKstar_VertexCHI2": 4,
            "KstarKstar_DOCACHI2": 16,
            "KstarKstar_Comb_MassHigh": 2400,
            "Lambdastar_FlightChi2": 36,
            "Lambdastar_Comb_MassHigh": 2200.0,
            "Lambdastar_Comb_MassLow": 1400.0,
            "Lambdastar_PT": 1500,
            "Lambdastar_VertexCHI2": 3,
            "DiMuPi_VertexCHI2": 10,
            "DiEPi_VertexCHI2": 10,
            "MuonPID": 0.0,
            "ElectronPID": 4.0,
            "PionFromTau_PIDK_max": 0.0,
            "KaonPID": 4.0,
            "Kaon_Proton_PID": 0,
            "ProtonPID": 5.0,
            "Proton_Kaon_PID": -2,
            "Pion_ProbNN": 0.96,
            "Pion_Kaon_ProbNN":  0.96,
            "Pion_ProbNN_B2Kstar": 0.95,
            "Kaon_Pion_ProbNN_B2Kstar": 0.95,
            "Hadron_P": 3000,
            "Hadron_PT": 500,
            "Muon_PT": 250,
            "Electron_PT": 250,
            "Pion_PT": 250,
            "SpdMult": 600,
            "Track_GhostProb": 0.3,
            "Track_TRCHI2": 3,
            "UseNoPIDsHadrons": False,
            "UseNoPIDsTauDau": False,
            "HLT1_FILTER": None,
            "HLT2_FILTER": None,
            "L0DU_FILTER": None,
        },

    'WGs': ['RD'],
    'STREAMS': ['Semileptonic']
}


class B2XTauTau_LPiConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name

        self.BsCombCut = "(AM < %(Bs_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bs_VertDist)s * mm)" % config
        self.BsCombCut_E = "(AM < %(Bs_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bs_VertDist_E)s * mm)" % config        
        self.Bs_for_B2KstarKstarCombCut = "(AM < %(Bs_Comb_MassHigh)s * MeV)" % config
        self.BsCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Bs_VertexCHI2)s)" % config

        self.BdCombCut = "(AM < %(Bd_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bd_VertDist)s * mm)" % config
        self.BdCombCut_E = "(AM < %(Bd_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bd_VertDist_E)s * mm)" % config
        self.BdCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(B0_VertexCHI2)s)" % config

        self.LambdaBCombCut = "(AM < %(Lb_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Lb_VertDist)s * mm)" % config
        self.LambdaBCombCut_E = "(AM < %(Lb_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Lb_VertDist_E)s * mm)" % config
        self.LambdaBCut = "(BPVDIRA> %(Lb_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Lb_VertexCHI2)s)" % config

        DaughterCuts = "(BPVDIRA> %(Dau_DIRA)s)" % config

        self.PhiCombCut = "(AM < %(Phi_Comb_MassHigh)s * MeV)" % config
        self.PhiCut = DaughterCuts + " & (VFASPF(VCHI2/VDOF) < %(Phi_VertexCHI2)s) & (PT > %(Phi_PT)s * MeV) & " \
                                     "(BPVVDCHI2 > %(Phi_FlightChi2)s)" % config

        self.Kstar_for_B2KstarCombCut = "(AM < %(Kstar_for_B2Kstar_Comb_MassHigh)s*MeV) & (AM > %(Kstar_for_B2Kstar_Comb_MassLow)s*MeV)" % config
        self.Kstar_for_B2KstarCut = DaughterCuts + " & (VFASPF(VCHI2/VDOF) < %(Kstar_for_B2Kstar_VertexCHI2)s) & " \
                                                   "(PT > %(Kstar_for_B2Kstar_PT)s * MeV) & (BPVVDCHI2 > %(Kstar_for_B2Kstar_FlightChi2)s)" % config

        self.Kstar_for_B2KstarKstarCombCut = "(ADAMASS('K*(892)0')< %(Kstar_for_B2KstarKstar_MassWindow)s * MeV )" % config
        self.Kstar_for_B2KstarKstarCut = DaughterCuts + " &  (VFASPF(VCHI2/VDOF) < %(Kstar_for_B2KstarKstar_VertexCHI2)s) & " \
                                                        "(PT > %(Kstar_for_B2KstarKstar_PT)s * MeV)" % config

        self.LambdaStarCombCut = "(AM < %(Lambdastar_Comb_MassHigh)s * MeV) & (AM > %(Lambdastar_Comb_MassLow)s * MeV)" % config
        self.LambdaStarCut = DaughterCuts + " & (PT > %(Lambdastar_PT)s * MeV) & (VFASPF(VCHI2/VDOF) < %(Lambdastar_VertexCHI2)s)" % config

        self.DiMuPiCombCut = "(AM < %(DiMuPiUPPERMASS)s * MeV) " % config
        self.DiMuPiCut = "(BPVDIRA> %(DiMuPi_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(DiMuPi_VertexCHI2)s)" % config

        self.DiEPiCombCut = "(AM < %(DiEPiUPPERMASS)s * MeV) " % config
        self.DiEPiCut = "(BPVDIRA> %(DiEPi_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(DiEPi_VertexCHI2)s)" % config

        self.TrackCuts = "(TRGHP < %(Track_GhostProb)s) & (TRCHI2DOF < %(Track_TRCHI2)s)" % config

        self.HadronCuts = "(MIPCHI2DV(PRIMARY) > %(Hadron_MinIPCHI2)s) & (PT > %(Hadron_PT)s * MeV)" % config

        self.KaonCutBase = self.TrackCuts + " & " + self.HadronCuts + " & (P > %(Hadron_P)s * MeV)" % config
        self.KaonCut = self.KaonCutBase + " & (PIDK > %(KaonPID)s) & (~ISMUON) & (PIDK - PIDp > %(Kaon_Proton_PID)s)" % config
        self.KaonCut_B2Kstar = self.KaonCutBase + " & (PROBNNK * (1-PROBNNpi) > %(Kaon_Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.KaonCutReversePID_B2Kstar = self.KaonCutBase + " & (PROBNNK * (1-PROBNNpi) < %(Kaon_Pion_ProbNN_B2Kstar)s)" % config
        self.KaonCutReversePID = self.KaonCutBase + " & (PIDK < %(KaonPID)s)" % config

        self.PionCutBase = self.TrackCuts + " & " + self.HadronCuts
        self.PionCut_B2Kstar = self.PionCutBase + " & (PROBNNpi > %(Pion_ProbNN_B2Kstar)s) & (~ISMUON) & (PROBNNpi * (1 - PROBNNK) > %(Pion_Kaon_ProbNN)s)" % config
        self.PionCutReversePID_B2Kstar = self.PionCutBase + " & (PROBNNpi < %(Pion_ProbNN_B2Kstar)s)" % config

        self.PionFromTauCutBase = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(PionFromTau_MinIPCHI2)s) & (PT > %(Pion_PT)s * MeV)" % config
        self.PionFromTauCut = self.PionFromTauCutBase + " & (PROBNNpi > %(Pion_ProbNN)s) & (~ISMUON) & (PROBNNpi * (1 - PROBNNK) > %(Pion_Kaon_ProbNN)s)" % config
        self.PionFromTauCutReversePID = self.PionFromTauCutBase + " & (PROBNNpi < %(Pion_ProbNN)s) " % config

        self.MuonCutBase = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s) & (PT > %(Muon_PT)s * MeV)" % config
        self.MuonCut = self.MuonCutBase  + " & (PIDmu > %(MuonPID)s) & (ISMUON)" % config
        self.MuonCutReversePID = self.MuonCutBase + " & (PIDmu < %(MuonPID)s) " % config

        self.ElectronCutBase = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s) & (PT > %(Electron_PT)s * MeV)" % config
        self.ElectronCut = self.ElectronCutBase + " & (PIDe > %(ElectronPID)s) & (~ISMUON)" % config
        self.ElectronCutReversePID = self.ElectronCutBase + " & (PIDe < %(ElectronPID)s) " % config

        self.ProtonCutBase = self.TrackCuts + " & " + self.HadronCuts + " & (P > %(Hadron_P)s * MeV)" % config
        self.ProtonCut = self.ProtonCutBase + " & (PIDp > %(ProtonPID)s)  & (~ISMUON) & (PIDp - PIDK > %(Proton_Kaon_PID)s)" % config
        self.ProtonCutReversePID = self.ProtonCutBase + " & (PIDp < %(ProtonPID)s)" % config

        self.Kaons = self.__Kaons__(config)
        self.FakeKaons = self.__FakeKaons__()

        self.Kaons_Kstar = self.__Kaons__(config, sel_name="_B2Kstar")
        self.FakeKaons_Kstar = self.__FakeKaons__(sel_name="_B2Kstar")

        self.Pions_Kstar = self.__Pions__(config, sel_name="_B2Kstar")
        self.FakePions_Kstar = self.__FakePions__(sel_name="_B2Kstar")

        self.PionsFromTau = self.__PionsFromTau__(config)
        self.FakePionsFromTau = self.__FakePionsFromTau__()

        self.Muons = self.__Muons__(config)
        self.FakeMuons = self.__FakeMuons__()

        self.Electrons = self.__Electrons__(config)
        self.FakeElectrons = self.__FakeElectrons__()

        self.Protons = self.__Protons__(config)
        self.FakeProtons = self.__FakeProtons__()

        self.DiMuPi = self.__DiMuPi__(self.Muons, self.PionsFromTau)
        self.DiMuPi_fakemuon = self.__DiMuPi__(self.FakeMuons, self.PionsFromTau, pid_selection='ReversePIDMu_')
        self.DiMuPi_fakepionfromtau = self.__DiMuPi__(self.Muons, self.FakePionsFromTau, pid_selection='ReversePIDPi_')

        self.DiEPi = self.__DiEPi__(self.Electrons, self.PionsFromTau)
        self.DiEPi_fakeelectron = self.__DiEPi__(self.FakeElectrons, self.PionsFromTau, pid_selection='ReversePIDE_')
        self.DiEPi_fakepionfromtau = self.__DiEPi__(self.Electrons, self.FakePionsFromTau, pid_selection='ReversePIDPi_')

        self.Phi = self.__Phi__(self.Kaons, conf=config)
        self.FakePhi = self.__Phi__(self.Kaons, self.FakeKaons, conf=config)

        self.Kstar_for_B2KstarKstar = self.__Kstar__(self.Kaons_Kstar, self.Pions_Kstar, sel_name="_B2KstarKstar")
        self.FakePionKstar_for_B2KstarKstar = self.__Kstar__(self.Kaons_Kstar, self.FakePions_Kstar, pid_selection="ReversePIDPi_",
                                                             sel_name="_B2KstarKstar")
        self.FakeKaonKstar_for_B2KstarKstar = self.__Kstar__(self.FakeKaons_Kstar, self.Pions_Kstar, pid_selection="ReversePIDK_",
                                                             sel_name="_B2KstarKstar")

        self.Kstar_for_B2Kstar = self.__Kstar__(self.Kaons_Kstar, self.Pions_Kstar, sel_name="_B2Kstar")
        self.FakePionKstar_for_B2Kstar = self.__Kstar__(self.Kaons_Kstar, self.FakePions_Kstar,
                                                        pid_selection="ReversePIDPi_", sel_name="_B2Kstar")
        self.FakeKaonKstar_for_B2Kstar = self.__Kstar__(self.FakeKaons_Kstar, self.Pions_Kstar,
                                                        pid_selection="ReversePIDK_", sel_name="_B2Kstar")

        self.LambdaStar = self.__Lambdastar__(self.Protons, self.Kaons)
        self.FakeProtonLambdaStar = self.__Lambdastar__(self.FakeProtons, self.Kaons, pid_selection="ReversePIDp_")
        self.FakeKaonLambdaStar = self.__Lambdastar__(self.Protons, self.FakeKaons, pid_selection="ReversePIDK_")

        # K K algorithms
        self.Bs_MuPi = self.__Bs_Phi__(daughters=[self.Phi, self.DiMuPi], pid_selection="MuPi")
        self.Bs_MuPi_ReversePIDmu = self.__Bs_Phi__(daughters=[self.Phi, self.DiMuPi_fakemuon], pid_selection="MuPi_ReversePIDmu_")
        self.Bs_MuPi_ReversePIDpifromtau = self.__Bs_Phi__(daughters=[self.Phi, self.DiMuPi_fakepionfromtau], pid_selection="MuPi_ReversePIDpifromtau_")
        self.Bs_MuPi_ReversePIDK = self.__Bs_Phi__(daughters=[self.FakePhi, self.DiMuPi], pid_selection="MuPi_ReversePIDK_")
        self.Bs_EPi = self.__Bs_Phi__(daughters=[self.Phi, self.DiEPi], pid_selection="EPi", e_mode=True)
        self.Bs_EPi_ReversePIDe = self.__Bs_Phi__(daughters=[self.Phi, self.DiEPi_fakeelectron], pid_selection="EPi_ReversePIDe_", e_mode=True)
        self.Bs_EPi_ReversePIDpifromtau = self.__Bs_Phi__(daughters=[self.Phi, self.DiEPi_fakepionfromtau], pid_selection="EPi_ReversePIDpifromtau_", e_mode=True)
        self.Bs_EPi_ReversePIDK = self.__Bs_Phi__(daughters=[self.FakePhi, self.DiEPi], pid_selection="EPi_ReversePIDK_", e_mode=True)

        # Kst Kst algorithms
        self.Bs_kstarkstar_MuPi = self.__Bs_KstarKstar__([self.Kstar_for_B2KstarKstar, self.DiMuPi], conf=config, pid_selection="MuPi")
        self.Bs_kstarkstar_MuPi_reversepidK = self.__Bs_KstarKstar__(daughters=[self.FakeKaonKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.DiMuPi], conf=config, pid_selection="MuPi_ReversePIDK_")
        self.Bs_kstarkstar_MuPi_reversepidpi = self.__Bs_KstarKstar__(daughters=[self.FakePionKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.DiMuPi], conf=config, pid_selection="MuPi_ReversePIDpi_")
        self.Bs_kstarkstar_MuPi_reversepidmu = self.__Bs_KstarKstar__(daughters=[self.Kstar_for_B2KstarKstar, self.DiMuPi_fakemuon], conf=config, pid_selection="MuPi_ReversePIDmu_")
        self.Bs_kstarkstar_MuPi_reversepidpifromtau = self.__Bs_KstarKstar__(daughters=[self.Kstar_for_B2KstarKstar, self.DiMuPi_fakepionfromtau], conf=config, pid_selection="Mu_ReversePIDpifromtau_")
        self.Bs_kstarkstar_EPi = self.__Bs_KstarKstar__([self.Kstar_for_B2KstarKstar, self.DiEPi], conf=config, pid_selection="EPi")
        self.Bs_kstarkstar_EPi_reversepidK = self.__Bs_KstarKstar__(daughters=[self.FakeKaonKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.DiEPi], conf=config, pid_selection="EPi_ReversePIDK_")
        self.Bs_kstarkstar_EPi_reversepidpi = self.__Bs_KstarKstar__(daughters=[self.FakePionKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.DiEPi], conf=config, pid_selection="EPi_ReversePIDpi_")
        self.Bs_kstarkstar_EPi_reversepide = self.__Bs_KstarKstar__(daughters=[self.Kstar_for_B2KstarKstar, self.DiEPi_fakeelectron], conf=config, pid_selection="EPi_ReversePIDe_")
        self.Bs_kstarkstar_EPi_reversepidpifromtau = self.__Bs_KstarKstar__(daughters=[self.Kstar_for_B2KstarKstar, self.DiEPi_fakepionfromtau], conf=config, pid_selection="EPi_ReversePIDpifromtau_")

        # K pi algorithms
        self.Bd_kstar_MuPi = self.__B0_Kstar__(daughters= [self.Kstar_for_B2Kstar, self.DiMuPi], pid_selection="MuPi")
        self.Bd_kstar_MuPi_reversepidK = self.__B0_Kstar__(daughters=[self.FakeKaonKstar_for_B2Kstar, self.DiMuPi], pid_selection="MuPi_ReversePIDK_")
        self.Bd_kstar_MuPi_reversepidpi = self.__B0_Kstar__(daughters=[self.FakePionKstar_for_B2Kstar, self.DiMuPi], pid_selection="MuPi_ReversePIDpi_")
        self.Bd_kstar_MuPi_reversepidmu = self.__B0_Kstar__(daughters=[self.Kstar_for_B2Kstar, self.DiMuPi_fakemuon], pid_selection="MuPi_ReversePIDmu_")
        self.Bd_kstar_MuPi_reversepidpifromtau = self.__B0_Kstar__(daughters=[self.Kstar_for_B2Kstar, self.DiMuPi_fakepionfromtau], pid_selection="MuPi_ReversePIDpifromtau_")
        self.Bd_kstar_EPi = self.__B0_Kstar__(daughters= [self.Kstar_for_B2Kstar, self.DiEPi], pid_selection="EPi", e_mode=True)
        self.Bd_kstar_EPi_reversepidK = self.__B0_Kstar__(daughters=[self.FakeKaonKstar_for_B2Kstar, self.DiEPi], pid_selection="EPi_ReversePIDK_", e_mode=True)
        self.Bd_kstar_EPi_reversepidpi = self.__B0_Kstar__(daughters=[self.FakePionKstar_for_B2Kstar, self.DiEPi], pid_selection="EPi_ReversePIDpi_", e_mode=True)
        self.Bd_kstar_EPi_reversepide = self.__B0_Kstar__(daughters=[self.Kstar_for_B2Kstar, self.DiEPi_fakeelectron], pid_selection="EPi_ReversePIDe_", e_mode=True)
        self.Bd_kstar_EPi_reversepidpifromtau = self.__B0_Kstar__(daughters=[self.Kstar_for_B2Kstar, self.DiEPi_fakepionfromtau], pid_selection="EPi_ReversePIDpifromtau_", e_mode=True)

        # p K algorithms
        self.LambdaB_pk_MuPi = self.__LambdaB_pK__(daughters=[self.DiMuPi, self.LambdaStar], pid_selection="MuPi")
        self.LambdaB_pk_MuPi_reversePIDK = self.__LambdaB_pK__(daughters=[self.DiMuPi, self.FakeKaonLambdaStar], pid_selection="MuPi_ReversePIDK_")
        self.LambdaB_pk_MuPi_reversePIDp = self.__LambdaB_pK__(daughters=[self.DiMuPi, self.FakeProtonLambdaStar], pid_selection="MuPi_ReversePIDp_")
        self.LambdaB_pk_MuPi_reversePIDmu = self.__LambdaB_pK__(daughters=[self.DiMuPi_fakemuon, self.LambdaStar], pid_selection="MuPi_ReversePIDmu_")
        self.LambdaB_pk_MuPi_reversePIDpifromtau = self.__LambdaB_pK__(daughters=[self.DiMuPi_fakepionfromtau, self.LambdaStar], pid_selection="MuPi_ReversePIDpifromtau_")
        self.LambdaB_pk_EPi = self.__LambdaB_pK__(daughters=[self.DiEPi, self.LambdaStar], pid_selection="EPi", e_mode=True)
        self.LambdaB_pk_EPi_reversePIDK = self.__LambdaB_pK__(daughters=[self.DiEPi, self.FakeKaonLambdaStar], pid_selection="EPi_ReversePIDK_", e_mode=True)
        self.LambdaB_pk_EPi_reversePIDp = self.__LambdaB_pK__(daughters=[self.DiEPi, self.FakeProtonLambdaStar], pid_selection="EPi_ReversePIDp_", e_mode=True)
        self.LambdaB_pk_EPi_reversePIDe = self.__LambdaB_pK__(daughters=[self.DiEPi_fakeelectron, self.LambdaStar], pid_selection="EPi_ReversePIDe_", e_mode=True)
        self.LambdaB_pk_EPi_reversePIDpifromtau = self.__LambdaB_pK__(daughters=[self.DiEPi_fakepionfromtau, self.LambdaStar], pid_selection="EPi_ReversePIDpifromtau_", e_mode=True)

        self.FilterSPD = {'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                          'Preambulo': ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]}

        # K K lines
        self.Bs2phi_MuPi_line = StrippingLine(
            self.name + "_Bs2Phi_MuPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_MuPi], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phi_MuPi_line)

        self.Bs2Phi_MuPi_ReversePIDmu_line = StrippingLine(
            self.name + "_Bs2Phi_MuPi_ReversePIDMu_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_MuPi_ReversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_MuPi_ReversePIDmu_line)

        self.Bs2Phi_MuPi_ReversePIDpifromtau_line = StrippingLine(
            self.name + "_Bs2Phi_MuPi_ReversePIDPiFromTau_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_MuPi_ReversePIDpifromtau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_MuPi_ReversePIDpifromtau_line)

        self.Bs2Phi_MuPi_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2Phi_MuPi_ReversePIDK_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_MuPi_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_MuPi_ReversePIDK_line)

        self.Bs2phi_EPi_line = StrippingLine(
            self.name + "_Bs2Phi_EPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_EPi], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phi_EPi_line)

        self.Bs2Phi_EPi_ReversePIDe_line = StrippingLine(
            self.name + "_Bs2Phi_EPi_ReversePIDE_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_EPi_ReversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_EPi_ReversePIDe_line)
        self.Bs2Phi_EPi_ReversePIDpifromtau_line = StrippingLine(
            self.name + "_Bs2Phi_EPi_ReversePIDPiFromTau_line", prescale=0.04,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_EPi_ReversePIDpifromtau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_EPi_ReversePIDpifromtau_line)

        self.Bs2Phi_EPi_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2Phi_EPi_ReversePIDK_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_EPi_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_EPi_ReversePIDK_line)

        # Kst Kst lines
        self.Bs2KstarKstar_MuPi_line = StrippingLine(
            self.name + "_Bs2KstarKstar_MuPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_MuPi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_MuPi_line)

        self.Bs2KstarKstar_MuPi_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2KstarKstar_MuPi_ReversePIDK_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_MuPi_reversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_MuPi_ReversePIDK_line)

        self.Bs2KstarKstar_MuPi_ReversePIDpi_line = StrippingLine(
            self.name + "_Bs2KstarKstar_MuPi_ReversePIDPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_MuPi_reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_MuPi_ReversePIDpi_line)

        self.Bs2KstarKstar_MuPi_ReversePIDmu_line = StrippingLine(
            self.name + "_Bs2KstarKstar_MuPi_ReversePIDMu_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_MuPi_reversepidmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_MuPi_ReversePIDmu_line)
        self.Bs2KstarKstar_MuPi_ReversePIDpifromtau_line = StrippingLine(
            self.name + "_Bs2KstarKstar_MuPi_ReversePIDPiFromTau_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_MuPi_reversepidpifromtau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_MuPi_ReversePIDpifromtau_line)

        self.Bs2KstarKstar_EPi_line = StrippingLine(
            self.name + "_Bs2KstarKstar_EPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_EPi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_EPi_line)

        self.Bs2KstarKstar_EPi_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2KstarKstar_EPi_ReversePIDK_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_EPi_reversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_EPi_ReversePIDK_line)

        self.Bs2KstarKstar_EPi_ReversePIDpi_line = StrippingLine(
            self.name + "_Bs2KstarKstar_EPi_ReversePIDPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_EPi_reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_EPi_ReversePIDpi_line)

        self.Bs2KstarKstar_EPi_ReversePIDe_line = StrippingLine(
            self.name + "_Bs2KstarKstar_EPi_ReversePIDE_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_EPi_reversepide], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_EPi_ReversePIDe_line)
        self.Bs2KstarKstar_EPi_ReversePIDpifromtau_line = StrippingLine(
            self.name + "_Bs2KstarKstar_EPi_ReversePIDPiFromTau_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_EPi_reversepidpifromtau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_EPi_ReversePIDpifromtau_line)


        # K pi lines
        self.Bd2Kstar_MuPi_line = StrippingLine(
            self.name + "_B02Kstar_MuPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_MuPi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_MuPi_line)

        self.Bd2Kstar_MuPi_ReversePIDK_line = StrippingLine(
            self.name + "_B02Kstar_MuPi_ReversePIDK_line", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_MuPi_reversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_MuPi_ReversePIDK_line)

        self.Bd2Kstar_MuPi_ReversePIDpi_line = StrippingLine(
            self.name + "_B02Kstar_MuPi_ReversePIDPi_line", prescale=0.7,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_MuPi_reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_MuPi_ReversePIDpi_line)

        self.Bd2Kstar_MuPi_ReversePIDmu_line = StrippingLine(
            self.name + "_B02Kstar_MuPi_ReversePIDMu_line", prescale=0.06,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_MuPi_reversepidmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_MuPi_ReversePIDmu_line)
        self.Bd2Kstar_MuPi_ReversePIDpifromtau_line = StrippingLine(
            self.name + "_B02Kstar_MuPi_ReversePIDPiFromTau_line", prescale=0.06,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_MuPi_reversepidpifromtau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_MuPi_ReversePIDpifromtau_line)

        self.Bd2Kstar_EPi_line = StrippingLine(
            self.name + "_B02Kstar_EPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_EPi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_EPi_line)

        self.Bd2Kstar_EPi_ReversePIDK_line = StrippingLine(
            self.name + "_B02Kstar_EPi_ReversePIDK_line", prescale=0.15,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_EPi_reversepidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_EPi_ReversePIDK_line)

        self.Bd2Kstar_EPi_ReversePIDpi_line = StrippingLine(
            self.name + "_B02Kstar_EPi_ReversePIDPi_line", prescale=0.7,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_EPi_reversepidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_EPi_ReversePIDpi_line)

        self.Bd2Kstar_EPi_ReversePIDe_line = StrippingLine(
            self.name + "_B02Kstar_EPi_ReversePIDE_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_EPi_reversepide], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_EPi_ReversePIDe_line)
        self.Bd2Kstar_EPi_ReversePIDpifromtau_line = StrippingLine(
            self.name + "_B02Kstar_EPi_ReversePIDPiFromTau_line", prescale=0.08,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_kstar_EPi_reversepidpifromtau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_EPi_ReversePIDpifromtau_line)

        # p K lines
        self.Lb2pK_MuPi_line = StrippingLine(
            self.name + "_Lb2pK_MuPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_MuPi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_MuPi_line)

        self.Lb2pK_MuPi_ReversePIDK_line = StrippingLine(
            self.name + "_Lb2pK_MuPi_ReversePIDK_line", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_MuPi_reversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_MuPi_ReversePIDK_line)

        self.Lb2pK_MuPi_ReversePIDp_line = StrippingLine(
            self.name + "_Lb2pK_MuPi_ReversePIDp_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_MuPi_reversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_MuPi_ReversePIDp_line)

        self.Lb2pK_MuPi_ReversePIDmu_line = StrippingLine(
            self.name + "_Lb2pK_MuPi_ReversePIDmu_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_MuPi_reversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_MuPi_ReversePIDmu_line)
        self.Lb2pK_MuPi_ReversePIDpifromtau_line = StrippingLine(
            self.name + "_Lb2pK_MuPi_ReversePIDpifromtau_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_MuPi_reversePIDpifromtau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_MuPi_ReversePIDpifromtau_line)


        self.Lb2pK_EPi_line = StrippingLine(
            self.name + "_Lb2pK_EPi_line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_EPi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_EPi_line)

        self.Lb2pK_EPi_ReversePIDK_line = StrippingLine(
            self.name + "_Lb2pK_EPi_ReversePIDK_line", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_EPi_reversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_EPi_ReversePIDK_line)

        self.Lb2pK_EPi_ReversePIDp_line = StrippingLine(
            self.name + "_Lb2pK_EPi_ReversePIDp_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_EPi_reversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_EPi_ReversePIDp_line)

        self.Lb2pK_EPi_ReversePIDe_line = StrippingLine(
            self.name + "_Lb2pK_Epi_ReversePIDe_line", prescale=0.09,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_EPi_reversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_EPi_ReversePIDe_line)

        self.Lb2pK_EPi_ReversePIDpifromtau_line = StrippingLine(
            self.name + "_Lb2pK_Epi_ReversePIDpifromtau_line", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_EPi_reversePIDpifromtau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_EPi_ReversePIDpifromtau_line)


    def __PionsFromTau__(self, conf):
        from StandardParticles import StdAllLoosePions, StdAllNoPIDsPions
        pions = StdAllNoPIDsPions if conf['UseNoPIDsTauDau'] else StdAllLoosePions
        pion_cut = self.self.PionFromTauCutBase if conf['UseNoPIDsTauDau'] else self.PionFromTauCut
        _filter = FilterDesktop(Code=pion_cut)
        _sel = Selection("Selection_" + self.name + "_StdLoosePionsFromTau", RequiredSelections=[pions],
                         Algorithm=_filter)
        return _sel

    def __FakePionsFromTau__(self):
        from StandardParticles import StdAllNoPIDsPions
        _filter = FilterDesktop(Code=self.PionFromTauCutReversePID)
        _sel = Selection("Selection_" + self._name + "StdAllNoPIDsPionsFromTau", Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsPions])
        return _sel


    def __Muons__(self, conf):
        from StandardParticles import StdAllLooseMuons, StdAllNoPIDsMuons
        muons = StdAllNoPIDsMuons if conf['UseNoPIDsTauDau'] else StdAllLooseMuons
        muon_cut = self.MuonCutBase if conf['UseNoPIDsTauDau'] else self.MuonCut
        _filter = FilterDesktop(Code=muon_cut)
        _sel = Selection("Selection_" + self.name + "_StdLooseMuons", RequiredSelections=[muons],
                         Algorithm=_filter)
        return _sel

    def __FakeMuons__(self):
        from StandardParticles import StdAllNoPIDsMuons
        _filter = FilterDesktop(Code=self.MuonCutReversePID)
        _sel = Selection("Selection_" + self._name + "StdAllNoPIDsMuons", Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsMuons])
        return _sel

    def __Electrons__(self, conf):
        from StandardParticles import StdAllLooseElectrons, StdAllNoPIDsElectrons
        electrons = StdAllNoPIDsElectrons if conf['UseNoPIDsTauDau'] else StdAllLooseElectrons
        electron_cut = self.ElectronCutBase if conf['UseNoPIDsTauDau'] else self.ElectronCut
        _filter = FilterDesktop(Code=electron_cut)
        _sel = Selection("Selection_" + self.name + "_StdAllLooseElectrons", RequiredSelections=[electrons],
                         Algorithm=_filter)
        return _sel

    def __FakeElectrons__(self):
        from StandardParticles import StdAllNoPIDsElectrons
        _filter = FilterDesktop(Code=self.ElectronCutReversePID)
        _sel = Selection("Selection_" + self._name + "StdAllNoPIDsElectrons", Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsElectrons])
        return _sel

    def __Protons__(self, conf):
        from StandardParticles import StdLooseProtons, StdNoPIDsProtons
        protons = StdNoPIDsProtons if conf['UseNoPIDsHadrons'] else StdLooseProtons
        proton_cuts = self.ProtonCutBase if conf['UseNoPIDsHadrons'] else self.ProtonCut
        _filter = FilterDesktop(Code=proton_cuts)
        _sel = Selection("Selection_" + self.name + "_StdLooseProtons", RequiredSelections=[protons], Algorithm=_filter)
        return _sel

    def __FakeProtons__(self):
        from StandardParticles import StdNoPIDsProtons
        _filter = FilterDesktop(Code=self.ProtonCutReversePID)
        _sel = Selection("Selection_" + self._name + "_StdAllNoPIDsProtons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsProtons])
        return _sel

    def __Kaons__(self, conf, sel_name="_"):
        from StandardParticles import StdLooseKaons, StdNoPIDsKaons
        kaons = StdNoPIDsKaons if conf['UseNoPIDsHadrons'] else StdLooseKaons
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.KaonCutBase)
        elif "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCut_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCut)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLooseKaons", RequiredSelections=[kaons],
                         Algorithm=_filter)
        return _sel

    def __FakeKaons__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsKaons
        if "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCutReversePID_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCutReversePID)
        _sel = Selection("Selection_" + self._name + sel_name + "StdAllNoPIDsKaons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsKaons])
        return _sel

    def __Pions__(self, conf, sel_name="_"):
        from StandardParticles import StdLoosePions, StdNoPIDsPions
        pions = StdNoPIDsPions if conf['UseNoPIDsHadrons'] else StdLoosePions
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.PionCutBase)
        else:
            _filter = FilterDesktop(Code=self.PionCut_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLoosePions", RequiredSelections=[pions],
                         Algorithm=_filter)
        return _sel

    def __FakePions__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsPions
        _filter = FilterDesktop(Code=self.PionCutReversePID_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdAllReversePIDsPions", RequiredSelections=[StdNoPIDsPions],
                         Algorithm=_filter)
        return _sel

    def __DiMuPi__(self, Muons, Pions, pid_selection=''):
        CombineDiMuPi = CombineParticles()
        CombineDiMuPi.DecayDescriptors = ["D0 -> mu- pi+", "D0 -> mu+ pi-", "D0 -> mu+ pi+", " D0 -> mu- pi-"]
        CombineDiMuPi.CombinationCut = self.DiMuPiCombCut
        CombineDiMuPi.MotherCut = self.DiMuPiCut
        _sel = Selection("Selection_" + self.name + "_DiMuPi" + pid_selection, Algorithm=CombineDiMuPi, RequiredSelections=[Muons, Pions])
        return _sel


    def __DiEPi__(self, Electrons, Pions, pid_selection=''):
        CombineDiEPi = CombineParticles()
        CombineDiEPi.DecayDescriptors = ["D0 -> e- pi+", "D0 -> e+ pi-", "D0 -> e+ pi+", " D0 -> e- pi-"]
        CombineDiEPi.CombinationCut = self.DiEPiCombCut
        CombineDiEPi.MotherCut = self.DiEPiCut
        _sel = Selection("Selection_" + self.name + "_DiEPi" + pid_selection, Algorithm=CombineDiEPi, RequiredSelections=[Electrons, Pions])
        return _sel


    def __Phi__(self, Kaons, fakekaon=None, conf=None):
        _phi2kk = CombineParticles()
        _phi2kk.DecayDescriptors = ["phi(1020) -> K+ K-", "phi(1020) -> K+ K+", "phi(1020) -> K- K-"]
        _phi2kk.MotherCut = self.PhiCut
        if fakekaon is None:
            _phi2kk.CombinationCut = self.PhiCombCut
            _sel = Selection("Phi_selection_for" + self.name, Algorithm=_phi2kk, RequiredSelections=[Kaons])
        else:
            _phi2kk.CombinationCut = self.PhiCombCut + " & (AHASCHILD((PIDK < %(KaonPID)s)))" % conf
            _sel = Selection("Phi_NoPIDK_selection_for" + self.name, Algorithm=_phi2kk,
                             RequiredSelections=[Kaons, fakekaon])
        return _sel

    def __Kstar__(self, Kaons, Pions, pid_selection="_", sel_name="_B2KstarKstar"):

        _kstar2kpi = CombineParticles()
        if sel_name == "_B2KstarKstar":
            _kstar2kpi.DecayDescriptors = ["[K*(892)0 -> K+ pi-]cc"]
            _kstar2kpi.CombinationCut = self.Kstar_for_B2KstarKstarCombCut
            _kstar2kpi.MotherCut = self.Kstar_for_B2KstarKstarCut
        else:
            _kstar2kpi.DecayDescriptors = ["[K*(892)0 -> K+ pi-]cc", "K*(892)0 -> K+ pi+", "K*(892)0 -> K- pi-"]
            _kstar2kpi.CombinationCut = self.Kstar_for_B2KstarCombCut
            _kstar2kpi.MotherCut = self.Kstar_for_B2KstarCut
        _sel = Selection("Kstar_for" + sel_name + pid_selection + "selection_for" + self.name, Algorithm=_kstar2kpi,
                         RequiredSelections=[Kaons, Pions])
        return _sel

    def __Lambdastar__(self, Protons, Kaons, pid_selection="_"):
        _lstar2pk = CombineParticles()
        _lstar2pk.DecayDescriptors = ["[Lambda(1520)0 -> p+ K-]cc", "Lambda(1520)0 -> p+ K+", "Lambda(1520)0 -> p~- K-"]
        _lstar2pk.CombinationCut = self.LambdaStarCombCut
        _lstar2pk.MotherCut = self.LambdaStarCut
        _sel = Selection("Lambdastar" + pid_selection + "selection_for" + self.name, Algorithm=_lstar2pk,
                         RequiredSelections=[Protons, Kaons])
        return _sel

    def __Bs_Phi__(self, daughters, pid_selection="_", e_mode=False):
        comb_cut = self.BsCombCut if not e_mode else self.BsCombCut_E
        _b2phitautau = CombineParticles(DecayDescriptors=["B_s0 -> phi(1020) D0"],
                                        MotherCut=self.BsCut, CombinationCut=comb_cut)
        sel = Selection("Phi" + pid_selection + "for" + self.name, Algorithm=_b2phitautau, RequiredSelections=daughters)
        return sel

    def __Bs_KstarKstar__(self, daughters, conf, pid_selection="_"):
        _b2kstarkstartautau = DaVinci__N3BodyDecays()
        _b2kstarkstartautau.DecayDescriptors = ["B_s0 -> K*(892)0 K*(892)~0 D0", "B_s0 -> K*(892)0 K*(892)0 D0", "B_s0 -> K*(892)~0 K*(892)~0 D0"]
        _b2kstarkstartautau.CombinationCut = self.Bs_for_B2KstarKstarCombCut
        _b2kstarkstartautau.MotherCut = self.BsCut
        _b2kstarkstartautau.Combination12Cut = "(ACHI2DOCA(1,2) < %(KstarKstar_DOCACHI2)s) & (AM < %(KstarKstar_Comb_MassHigh)s * MeV)" % conf
        sel = Selection("KstarKstar" + pid_selection + "for" + self.name + "_daughters", Algorithm=_b2kstarkstartautau,
                        RequiredSelections=daughters)
        return sel

    def __B0_Kstar__(self, daughters, pid_selection="_", e_mode=False):
        comb_cut = self.BdCombCut if not e_mode else self.BdCombCut_E
        _b2kstartautau = CombineParticles(DecayDescriptors=["B0 -> K*(892)0 D0", "B~0 -> K*(892)~0 D0"],
                                          MotherCut=self.BdCut, CombinationCut=comb_cut)
        sel = Selection("Kstar" + pid_selection + "for" + self.name, Algorithm=_b2kstartautau,
                        RequiredSelections=daughters)
        return sel

    def __LambdaB_pK__(self, daughters, pid_selection="_", e_mode=False):
        comb_cut = self.LambdaBCombCut if not e_mode else self.LambdaBCombCut_E
        _b2lambdatautau = CombineParticles(DecayDescriptors=["Lambda_b0 -> Lambda(1520)0 D0", "Lambda_b~0 -> Lambda(1520)~0 D0"],
                                           MotherCut=self.LambdaBCut, CombinationCut=comb_cut)
        sel = Selection("Lambda" + pid_selection + "for" + self.name, Algorithm=_b2lambdatautau,
                        RequiredSelections=daughters)
        return sel
