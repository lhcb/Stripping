###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting B->Jpsi(LL (mumu, ee))K(Kstar),
RelatedInfoToos stolen from StrippingB2LLXBDT
Note:
*. CPU-intensive cuts like IPCHI2 are not re-applied
   if being identical to those in the common particles
'''

__author__=['Jibo He', 'Yanting Fan']
__date__ = '10/03/2021'
__version__= '$Revision: 1.0 $'


__all__ = (
    'B2LLXBDT_CalibConf',
    'default_config'
    )

daughter_locations = {
    # OPPOSITE SIGN
    # 3-body
    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC" : "{0}H",
    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body with a strange particle in the final state
    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
}

daughter_vtx_locations = {
    # OPPOSITE SIGN
    # 3-body
    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body
    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC" : "{0}LL"
}


default_config = {
    'NAME'              :  'B2LLXBDT_Calib',
    'BUILDERTYPE'       :  'B2LLXBDT_CalibConf',
    'CONFIG'    : {
	'EETag1Cuts'	: """
			   (CHILDCUT( (PT>1.5*GeV),1 )) & (CHILDCUT( (PIDe>5.),1 )) & (CHILDCUT( (P>6.0*GeV),1 ))
			   & (CHILDCUT( (MIPCHI2DV(PRIMARY)>25.),1 )) & (CHILDCUT( (TRGHOSTPROB<1.),1 )) & (CHILDCUT( (TRCHI2DOF<4.),1 ))
			   & (CHILDCUT( ISLONG,1 ))
			  """,
	'EETag2Cuts'	: """
			   (CHILDCUT( (PT>1.5*GeV),2 )) & (CHILDCUT( (PIDe>5.),2 )) & (CHILDCUT( (P>6.0*GeV),2 ))
			   & (CHILDCUT( (MIPCHI2DV(PRIMARY)>25.),2 )) & (CHILDCUT( (TRGHOSTPROB<1.),2 )) & (CHILDCUT( (TRCHI2DOF<4.),2 ))
			   & (CHILDCUT( ISLONG,2 ))
			  """,
	'EEProbe1Cuts'	: """
			   (CHILDCUT( (PT>200*MeV),1 )) & (CHILDCUT( (P>3.0*GeV),1 )) & (CHILDCUT( (MIPCHI2DV(PRIMARY)>1.),1 ))
			   & (CHILDCUT( ISLONG,1 )) & (CHILDCUT( (TRCHI2DOF<4.),1 ))
			  """,
	'EEProbe2Cuts'	: """
			   (CHILDCUT( (PT>200*MeV),2 )) & (CHILDCUT( (P>3.0*GeV),2 )) & (CHILDCUT( (MIPCHI2DV(PRIMARY)>1.),2 ))
			   & (CHILDCUT( ISLONG,2 )) & (CHILDCUT( (TRCHI2DOF<4.),2 ))
			  """,
	'MMTag1Cuts'	: """
			   (CHILDCUT( ISMUON,1 )) & (CHILDCUT( (P>3.0*GeV),1 )) & (CHILDCUT( (PT>1.2*GeV),1 ))
			   & (CHILDCUT( (TRCHI2DOF<4.),1 )) & (CHILDCUT( ISLONG,1 )) & (CHILDCUT( (TRGHOSTPROB<0.2),1 ))
			   & (CHILDCUT( (MIPCHI2DV(PRIMARY)>9.),1 ))
			  """,
	'MMTag2Cuts'	: """
			   (CHILDCUT( ISMUON,2 )) & (CHILDCUT( (P>3.0*GeV),2 )) & (CHILDCUT( (PT>1.2*GeV),2 ))
			   & (CHILDCUT( (TRCHI2DOF<4.),2 )) & (CHILDCUT( ISLONG,2 )) & (CHILDCUT( (TRGHOSTPROB<0.2),2 ))
			   & (CHILDCUT( (MIPCHI2DV(PRIMARY)>9.),2 ))
			  """,
	'MMProbe1Cuts'	: """
			   (CHILDCUT( (TRCHI2DOF<4.),1 )) & (CHILDCUT( ISLONG,1 )) & (CHILDCUT( (P>3.0*GeV),1 ))
			   & (CHILDCUT( (PT>200*MeV),1 )) & (CHILDCUT( (MIPCHI2DV(PRIMARY)>1.),1 ))
			  """,
	'MMProbe2Cuts'	: """
			   (CHILDCUT( (TRCHI2DOF<4.),2 )) & (CHILDCUT( ISLONG,2 )) & (CHILDCUT( (P>3.0*GeV),2 ))
			   & (CHILDCUT( (PT>200*MeV),2 )) & (CHILDCUT( (MIPCHI2DV(PRIMARY)>1.),2 ))
			  """,
	'EEBothCuts'	: """
			   (in_range(2196.0*MeV, M, 3596.0*MeV))
			  """,
	'EEComCuts'	: """
			   (in_range(2096.0*MeV, AM, 3696.0*MeV)) & (ACHI2DOCA(1,2) <18)	  
			  """,
	'MMBothCuts'	: """
			   (in_range(2896.0*MeV, M, 3246.0*MeV))
			  """,
	'MMComCuts'	: """
			   (in_range(2796.0*MeV, AM, 3346.0*MeV)) & (ACHI2DOCA(1,2) < 5)
			  """,

        'KaonCuts'      : "(PROBNNk > 0.1) & (PT>300*MeV) & (TRGHOSTPROB<0.4)",
        'KstarCuts'     : "(VFASPF(VCHI2/VDOF)<16) & (ADMASS('K*(892)0')< 300*MeV)",
        
        'BComCuts'      : "(in_range(3.7*GeV, AM, 6.8*GeV))",
        'BMomCuts'      : "(in_range(4.0*GeV,  M, 6.5*GeV)) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.999) & (BPVDLS>0) & (BPVIPCHI2()<400)",


        'Bu2JpsieeKMVACut'       :  "0.",
        'Bu2JpsimumuKMVACut'     :  "0.",
        'Bu2LLKXmlFile'		 :  '$TMVAWEIGHTSROOT/data/Bu2eeK_BDT_v1r0.xml',

        'Bd2JpsieeKstarMVACut'   :  "0.",
        'Bd2JpsimumuKstarMVACut' :  "0.",
        'Bd2LLKstarXmlFile'	 :  '$TMVAWEIGHTSROOT/data/Bd2eeKstar_BDT_v1r0.xml',
        
	'RelatedInfoTools'     : [
            {'Type'              : 'RelInfoVertexIsolation',
             'Location'          : 'VertexIsoInfo',
             'IgnoreUnmatchedDescriptors': True, 
             'DaughterLocations' : {key: val.format('VertexIsoInfo') for key, val in daughter_vtx_locations.items()}},
            {'Type'              : 'RelInfoVertexIsolationBDT',
             'Location'          : 'VertexIsoBDTInfo',
             'IgnoreUnmatchedDescriptors': True, 
             'DaughterLocations' : {key: val.format('VertexIsoBDTInfo') for key, val in daughter_vtx_locations.items()}},
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 0.5,
             'IgnoreUnmatchedDescriptors': True, 
             'Location' : 'TrackIsoInfo05',
             'DaughterLocations' : {key: val.format('TrackIsoInfo') for key, val in daughter_locations.items()}},
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'          : 0.5,
             'IgnoreUnmatchedDescriptors': True, 
             'Location' : 'ConeIsoInfo05',
             'DaughterLocations' : {key: val.format('ConeIsoInfo') for key, val in daughter_locations.items()}},
            {'Type'              : 'RelInfoBs2MuMuTrackIsolations',
             'IgnoreUnmatchedDescriptors': True,
             'Location' : 'TrackIsoBs2MMInfo',
             'DaughterLocations' : {key: val.format('TrackIsoBs2MMInfo') for key, val in daughter_locations.items()}},

            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 1.0,
             'Location'          : 'TrackIsoInfo10'
            },
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 1.5,
             'Location'          : 'TrackIsoInfo15'
            },
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 2.0,
             'Location'          : 'TrackIsoInfo20'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'         : 1.0,
             'Location'          : 'ConeIsoInfo10'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'         : 1.5,
             'Location'          : 'ConeIsoInfo15'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'         : 2.0,
             'Location'          : 'ConeIsoInfo20'
            }
            ]        
        },
    'STREAMS'           : ['Leptonic' ],
    'WGs'               : ['RD']
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays
from PhysSelPython.Wrappers import Selection, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

class B2LLXBDT_CalibConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config

        """
        Di letpons
        """
        from StandardParticles import StdDiElectronFromTracks, StdLooseDiMuon, StdAllNoPIDsElectrons, StdAllNoPIDsMuons


        self.SelDiElectron = self.createCombinationSel( OutputList = self.name + "SelDiElectron",
							DecayDescriptor = 'J/psi(1S) -> e+ e-',
							DaughterLists = [ StdAllNoPIDsElectrons ],
							PreVertexCuts = config['EEComCuts'],	
							PostVertexCuts = "( " + config['EEBothCuts'] + " & ( (" + config['EETag1Cuts'] + " & " + config['EEProbe2Cuts'] + ") | (" + config['EETag2Cuts'] + " & " + config['EEProbe1Cuts'] + ") ) )"
							)

        self.SelDiMuon = self.createCombinationSel( OutputList = self.name + "SelDiMuon",
						    DecayDescriptor = 'J/psi(1S) -> mu+ mu-',
						    DaughterLists = [ StdAllNoPIDsMuons ],
						    PreVertexCuts = config['MMComCuts'],
						    PostVertexCuts = "( " + config['MMBothCuts'] + " & ( (" + config['MMTag1Cuts'] + " & " + config['MMProbe2Cuts'] + ") | (" + config['MMTag2Cuts'] + " & " + config['MMProbe1Cuts'] + ") ) )"
						    )
        """
        Basic particles, long tracks
        """
        from StandardParticles import StdLooseANNKaons

        self.SelKaons = self.createSubSel( OutputList = self.name + "SelKaons",
                                           InputList = StdLooseANNKaons, 
                                           Cuts = config['KaonCuts']
                                           )
        
	"""
        Kstar
        """
        from StandardParticles import StdVeryLooseDetachedKst2Kpi
        
        self.SelKstar = self.createSubSel( OutputList = self.name + "SelKstar",
                                           InputList =  StdVeryLooseDetachedKst2Kpi, 
                                           Cuts = config['KstarCuts']
                                           )
                                                   
        
        #
        #  Stripping lines here 
        #
        self.B2LLHVars = {
            "sqrt(L1_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(L2_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(Jpsi_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(H_IPCHI2_OWNPV)"       : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(B_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(L1_PT)"                 : "log(CHILD(PT, 1, 1))",
            "log(L2_PT)"                 : "log(CHILD(PT, 1, 2))", 
            "log(Jpsi_PT)"               : "log(CHILD(PT, 1))",
            "log(H_PT)"                  : "log(CHILD(PT, 2))",
            "log(B_PT)"                  : "log(PT)",
            "sqrt(Jpsi_FDCHI2_OWNPV)"    : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(B_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "B_DIRA_OWNPV"               : "BPVDIRA"
            }

        
        """
        Bu2JpsieeK
        """
        self.SelBu2JpsieeK = self.createCombinationSel( OutputList = self.name + "SelBu2JpsieeK",
                                                    DecayDescriptor = "[B+ -> J/psi(1S) K+]cc",
                                                    DaughterLists = [ self.SelDiElectron, self.SelKaons ],
                                                    PreVertexCuts  = config['BComCuts'],
                                                    PostVertexCuts = config['BMomCuts'] )

        self.MvaBu2JpsieeK = self.applyMVA( self.name + "MvaBu2JpsieeK",
                                        SelB        = self.SelBu2JpsieeK,
                                        MVAVars     = self.B2LLHVars,
                                        MVACutValue = config['Bu2JpsieeKMVACut'], 
                                        MVAxmlFile  = config['Bu2LLKXmlFile']
                                        )

        self.Bu2JpsieeKLine = StrippingLine( self.name + '_Bu2JpsieeKLine',                                                
                                         RelatedInfoTools = config['RelatedInfoTools'],                                        
                                         algos     = [ self.MvaBu2JpsieeK ],
                                         MDSTFlag  = False
                                         )

        self.registerLine( self.Bu2JpsieeKLine )


        """
        Bu2JpsimumuK
        """
        self.SelBu2JpsimumuK = self.createCombinationSel( OutputList = self.name + "SelBu2JpsimumuK",
                                                      DecayDescriptor = "[B+ -> J/psi(1S) K+]cc",
                                                      DaughterLists = [ self.SelDiMuon, self.SelKaons ],                    
                                                      PreVertexCuts  = config['BComCuts'],
                                                      PostVertexCuts = config['BMomCuts'] )

        self.MvaBu2JpsimumuK = self.applyMVA( self.name + "MvaBu2JpsimumuK",
                                          SelB        = self.SelBu2JpsimumuK,
                                          MVAVars     = self.B2LLHVars,
                                          MVACutValue = config['Bu2JpsimumuKMVACut'], 
                                          MVAxmlFile  = config['Bu2LLKXmlFile']
                                          )
                
        self.Bu2JpsimumuKLine = StrippingLine( self.name + '_Bu2JpsimumuKLine',                                                
                                           RelatedInfoTools = config['RelatedInfoTools'],
                                           algos     = [ self.MvaBu2JpsimumuK ],
                                           MDSTFlag  = False
                                           )

        self.registerLine( self.Bu2JpsimumuKLine )
        
        #
        # Bd2LLKstar
        #
        """
        B2LLXVars
        """
        self.B2LLXVars = {
            "sqrt(L1_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(L2_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(Jpsi_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(H1_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 2, 1))",
            "sqrt(H2_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 2, 2))",
            "sqrt(X_IPCHI2_OWNPV)"       : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(B_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(L1_PT)"                 : "log(CHILD(PT, 1, 1))",
            "log(L2_PT)"                 : "log(CHILD(PT, 1, 2))", 
            "log(Jpsi_PT)"               : "log(CHILD(PT, 1))",
            "log(H1_PT)"                 : "log(CHILD(PT, 2, 1))",
            "log(H2_PT)"                 : "log(CHILD(PT, 2, 2))", 
            "log(X_PT)"                  : "log(CHILD(PT, 2))",
            "log(B_PT)"                  : "log(PT)",
            "sqrt(Jpsi_FDCHI2_OWNPV)"    : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(X_FDCHI2_OWNPV)"       : "sqrt(CHILD(BPVVDCHI2,2))",
            "sqrt(B_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "B_DIRA_OWNPV"               : "BPVDIRA"
            }


        """
        Bd2JpsieeKstar
        """
        self.SelBd2JpsieeKstar = self.createCombinationSel( OutputList = self.name + "SelBd2JpsieeKstar",
                                                        DecayDescriptor = "[B0 -> J/psi(1S) K*(892)0]cc",
                                                        DaughterLists = [ self.SelDiElectron, self.SelKstar ],
                                                        PreVertexCuts  = config['BComCuts'],
                                                        PostVertexCuts = config['BMomCuts'] )
        
        self.MvaBd2JpsieeKstar = self.applyMVA( self.name + "MvaBd2JpsieeKstar",
                                            SelB        = self.SelBd2JpsieeKstar,
                                            MVAVars     = self.B2LLXVars,
                                            MVACutValue = config['Bd2JpsieeKstarMVACut'], 
                                            MVAxmlFile  = config['Bd2LLKstarXmlFile']
                                            )
        
        self.Bd2JpsieeKstarLine = StrippingLine( self.name + '_Bd2JpsieeKstarLine',                                                
                                             RelatedInfoTools = config['RelatedInfoTools'], 
                                             algos     = [ self.MvaBd2JpsieeKstar ],
                                             MDSTFlag  = False
                                             )

        self.registerLine( self.Bd2JpsieeKstarLine )

        """
        Bd2JpsimumuKstar
        """
        self.SelBd2JpsimumuKstar = self.createCombinationSel( OutputList = self.name + "SelBd2JpsimumuKstar",
                                                          DecayDescriptor = "[B0 -> J/psi(1S) K*(892)0]cc",
                                                          DaughterLists = [ self.SelDiMuon, self.SelKstar ],
                                                          PreVertexCuts  = config['BComCuts'],
                                                          PostVertexCuts = config['BMomCuts'] )

        self.MvaBd2JpsimumuKstar = self.applyMVA( self.name + "MvaBd2JpsimumuKstar",
                                              SelB        = self.SelBd2JpsimumuKstar,
                                              MVAVars     = self.B2LLXVars,
                                              MVACutValue = config['Bd2JpsimumuKstarMVACut'], 
                                              MVAxmlFile  = config['Bd2LLKstarXmlFile']
                                              )
                
        self.Bd2JpsimumuKstarLine = StrippingLine( self.name + '_Bd2JpsimumuKstarLine',                                                
                                               RelatedInfoTools = config['RelatedInfoTools'],
                                               algos     = [ self.MvaBd2JpsimumuKstar ],
                                               MDSTFlag  = False
                                               )

        self.registerLine( self.Bd2JpsimumuKstarLine )

       

        

    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)

    def applyMVA( self, name, 
                  SelB,
                  MVAVars,
                  MVAxmlFile,
                  MVACutValue
                  ):
        from MVADictHelpers import addTMVAclassifierValue
        from Configurables import FilterDesktop as MVAFilterDesktop

        _FilterB = MVAFilterDesktop( name + "Filter",
                                     Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )

        addTMVAclassifierValue( Component = _FilterB,
                                XMLFile   = MVAxmlFile,
                                Variables = MVAVars,
                                ToolName  = name )
        
        return Selection( name,
                          Algorithm =  _FilterB,
                          RequiredSelections = [ SelB ] )
