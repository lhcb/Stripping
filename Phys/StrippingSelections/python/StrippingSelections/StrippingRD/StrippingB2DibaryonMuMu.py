###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__  = 'Vitalii Lisovskyi, Niladri Sahoo; based on Bu2LLK lines'
__date__    = '12/07/2023'
__version__ = '$Revision: 2 $'

__all__ = ( 'B2DibaryonMuMuConf', 'default_config' )

"""
Selections for B -> two baryons and dimuon:
B+ ->  p+ Lambda~0 mu+ mu-
B+ -> p+ p~- K+ mu+ mu-
B+ -> p+ p~- pi+ mu+ mu-
Bs -> Lambda0 Lambda~0 mu+ mu-
"""

default_config = {
    'NAME'                       : 'B2DibaryonMuMu',
    'BUILDERTYPE'                : 'B2DibaryonMuMuConf',
    'CONFIG'                     :
        {
        'BFlightCHI2'            : 36 #100
        , 'BDIRA'                : 0.999
        , 'BIPCHI2'              : 25
        , 'BVertexCHI2'          : 16
        , 'DiLeptonPT'           : 0
        , 'DiLeptonFDCHI2'       : 16
        , 'DiLeptonIPCHI2'       : 0
        , 'LeptonIPCHI2'         : 9
        , 'LeptonPT'             : 300
        , 'LeptonPTTight'        : 500
        , 'KaonIPCHI2'           : 9
        , 'KaonPT'               : 250
        , 'UpperMass'            : 5500
        , 'BMassWindow'          : 1500
        , 'Trk_Chi2'             : 3
        , 'Trk_GhostProb'        : 0.3
        , 'K1_MassWindow_Lo'     : 0
        , 'K1_MassWindow_Hi'     : 6300
        , 'K1_VtxChi2'           : 20 #25
        , 'K1_SumPTHad'          : 800
        , 'K1_SumIPChi2Had'      : 48.0
        , 'LamLam_VtxChi2'       : 36
        , 'V0TAU'               : 0.0005
        , 'Bu2mmLinePrescale'    : 1
        },
    'WGs'     : [ 'RD' ],
    'STREAMS' : [ 'Leptonic' ]
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

class B2DibaryonMuMuConf(LineBuilder) :
    """
    Builder for B->2baryons mumu measurements
    """

    # now just define keys. Default values are fixed later
    __configuration_keys__ = (
        'BFlightCHI2'
        , 'BDIRA'
        , 'BIPCHI2'
        , 'BVertexCHI2'
        , 'DiLeptonPT'
        , 'DiLeptonFDCHI2'
        , 'DiLeptonIPCHI2'
        , 'LeptonIPCHI2'
        , 'LeptonPT'
        , 'LeptonPTTight'
        , 'KaonIPCHI2'
        , 'KaonPT'
        , 'UpperMass'
        , 'BMassWindow'
        , 'Trk_Chi2'
        , 'Trk_GhostProb'
        , 'K1_MassWindow_Lo'
        , 'K1_MassWindow_Hi'
        , 'K1_VtxChi2'
        , 'K1_SumPTHad'
        , 'K1_SumIPChi2Had'
        , 'LamLam_VtxChi2'
        , 'V0TAU'
        , 'Bu2mmLinePrescale'
      )

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self._name = name
        mmXLine_name   = name
        mmXSSLine_name = name + "_SS"

        from StandardParticles import StdLoosePions as Pions
        from StandardParticles import StdLooseKaons as Kaons
        from StandardParticles import StdLooseProtons as Protons
        from StandardParticles import StdVeryLooseLambdaLL as LambdasLL
        from StandardParticles import StdLooseLambdaDD as LambdasDD
        from StandardParticles import StdLooseLambdaLD as LambdasLD

        #+++++++++++++++++++++++++++++++++++++++++
        # 1 : Make K, Ks, K*, K1, Phi and Lambdas
        #+++++++++++++++++++++++++++++++++++++++++

        SelKaons  = self._filterHadron( name   = "KaonsFor" + self._name,
                                        sel    = Kaons,
                                        params = config )

        SelPions  = self._filterHadron( name   = "PionsFor" + self._name,
                                        sel    = Pions,
                                        params = config )

        SelppK = self._makeppK( name   = "ppKFor" + self._name,
                               kaons  = Kaons,
                               protons  = Protons,
                               params = config )

        Selpppi = self._makepppi( name   = "pppiFor" + self._name,
                               pions  = Pions,
                               protons  = Protons,
                               params = config )

        SelpL = self._makepLambda( name   = "pLFor" + self._name,
                               lambdasLL  = LambdasLL,
                               lambdasDD  = LambdasDD,
                               lambdasLD  = LambdasLD,
                               protons  = Protons,
                               params = config )

        SelLamLam = self._makeLambdaLambda( name   = "LamLamFor" + self._name,
                               lambdasLL  = LambdasLL,
                               lambdasDD  = LambdasDD,
                               lambdasLD  = LambdasLD,
                               params = config )

        SelLambdasLL = self._filterLongLivedHadron( name   = "LambdasLLFor" + self._name,
                                           sel    =  LambdasLL,
                                           params = config )

        SelLambdasDD = self._filterLongLivedHadron( name   = "LambdasDDFor" + self._name,
                                           sel    =  LambdasDD,
                                           params = config )

        SelLambdasLD = self._filterLongLivedHadron( name   = "LambdasLDFor" + self._name,
                                           sel    =  LambdasLD,
                                           params = config )

        #+++++++++++++++++++++++++++++++++++++++++
        # 2 : Make Dileptons
        #+++++++++++++++++++++++++++++++++++++++++

        from StandardParticles import StdLooseDiMuon as DiMuons
        MuonID = "(HASMUON)&(ISMUON)"
        DiMuonID     = "(2 == NINTREE((ABSID==13)&(HASMUON)&(ISMUON)))"

        SelDiMuon = self._filterDiLepton( "SelDiMuonsFor" + self._name,
                                          dilepton = DiMuons,
                                          params   = config,
                                          idcut    = DiMuonID )


        MuMu_SS = self._makeMuMuSS( "MuMuSSFor" + self._name, 
                                    params = config, 
                                    muonid = MuonID )

        SelDiMuon_SS = self._filterDiLepton( "SelMuMuSSFor" + self._name,
                                                  dilepton = MuMu_SS,
                                                  params   = config,
                                                  idcut    = DiMuonID )

        #+++++++++++++++++++++++++++++++++++++++++
        # 4 : Combine Particles
        #+++++++++++++++++++++++++++++++++++++++++

        SelB2mmX = self._makeB2LLX(mmXLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelPions, SelKaons, SelLambdasLL , SelLambdasDD , SelLambdasLD, SelppK, Selpppi, SelpL, SelLamLam ],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)

        SelB2mmX_SS = self._makeB2LLX(mmXSSLine_name,
                                      dilepton = SelDiMuon_SS,
                                      hadrons  = [ SelPions, SelKaons, SelLambdasLL , SelLambdasDD , SelLambdasLD, SelppK, Selpppi, SelpL, SelLamLam ],
                                      params   = config,
                                      masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)




        #+++++++++++++++++++++++++++++++++++++++++
        # 5 : Declare Lines
        #+++++++++++++++++++++++++++++++++++++++++

        SPDFilter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 600 )" ,
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb" ]
            }


        self.B2mmXLine = StrippingLine(mmXLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False )

        self.B2mmX_SSLine = StrippingLine(mmXSSLine_name + "Line",
                                          prescale          = config['Bu2mmLinePrescale'],
                                          postscale         = 1,
                                          selection         = SelB2mmX_SS,
                                          FILTER            = SPDFilter,
                                          RequiredRawEvents = [],
                                          MDSTFlag          = False,
                                          MaxCandidates     = 300)



        #+++++++++++++++++++++++++++++++++++++++++
        # 6 : Register Lines
        #+++++++++++++++++++++++++++++++++++++++++

        self.registerLine( self.B2mmXLine )
        self.registerLine( self.B2mmX_SSLine )


#####################################################
    def _filterHadron( self, name, sel, params ):
        """
        Filter for all hadronic final states
        """

        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        # need to add the ID here
        _Code = "(PT > %(KaonPT)s *MeV) & " \
                "((ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) | " \
                "(NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))))" % params

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = [ sel ] )

#####################################################
    def _filterLongLivedHadron( self, name, sel, params ):
        """
        Filter for all hadronic final states
        """

        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        _Code = "(PT > %(KaonPT)s *MeV) & (BPVLTIME() > %(V0TAU)s * ns)" % params

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = [ sel ] )

#####################################################
    def _filterDiLepton( self, name, dilepton, params, idcut = None ) :
        """
        Handy interface for dilepton filter
        """

        _Code = "(ID=='J/psi(1S)') & "\
                "(PT > %(DiLeptonPT)s *MeV) & "\
                "(MM < %(UpperMass)s *MeV) & "\
                "(MINTREE(ABSID<14,PT) > %(LeptonPT)s *MeV) & "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "\
                "(VFASPF(VCHI2/VDOF) < 10) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "\
                "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params

        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )

        _Filter = FilterDesktop( Code = _Code )

        return Selection(name, Algorithm = _Filter, RequiredSelections = [ dilepton ] )

#####################################################
    # not used anymore for SSLines, rather use _filterDiLepton
    def _filterDiLeptonTight( self, name, dilepton, params, idcut = None ) :
        """
        Handy interface for dilepton filter
        """

        _Code = "(ID=='J/psi(1S)') & "\
            "(PT > %(DiLeptonPT)s *MeV) & "\
            "(MM < %(UpperMass)s *MeV) & "\
            "(MINTREE(ABSID<14,PT) > %(LeptonPTTight)s *MeV) & "\
            "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "\
            "(VFASPF(VCHI2/VDOF) < 10) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "\
            "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params

        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )

        _Filter = FilterDesktop( Code = _Code )

        return Selection(name, Algorithm = _Filter, RequiredSelections = [ dilepton ] )

#####################################################
    def _makeppK( self, name, kaons, protons, params ) :
        """
        Make a K1 -> K+ p+ p~-
        K1 is just a proxy to get the three-body combination
        """

        _Decays = "[K_1(1270)+ -> K+ p+ p~-]cc"

         # define all the cuts
        _K1Comb12Cuts  = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV)" % params
        _K1CombCuts    = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV) & ((APT1+APT2+APT3) > %(K1_SumPTHad)s*MeV)" % params

        _K1MotherCuts  = "(VFASPF(VCHI2PDOF) < %(K1_VtxChi2)s) & (SUMTREE(MIPCHI2DV(PRIMARY),((ABSID=='K+') | (ABSID=='K-') | (ABSID=='p+') | (ABSID=='p~-')),0.0) > %(K1_SumIPChi2Had)s)" % params
        _daughtersCutsK = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNk > 0.05) & (HASRICH)" % params
        _daughtersCutsP = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNp > 0.05) & (P > 5000) & (HASRICH)" % params

        _Combine = DaVinci__N3BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "K+"  : _daughtersCutsK,
            "p+" : _daughtersCutsP }

        _Combine.Combination12Cut = _K1Comb12Cuts
        _Combine.CombinationCut   = _K1CombCuts
        _Combine.MotherCut        = _K1MotherCuts

        # make and store the Selection object
        return Selection( name, Algorithm = _Combine, RequiredSelections = [ kaons, protons ] )

####################################################
    def _makepppi( self, name, pions, protons, params ) :
        """
        Make an a1 -> pi+ p+ p~-
        a1 is just a proxy to get the three-body combination
        """

        _Decays = "[a_1(1260)+ -> pi+ p+ p~-]cc"

         # define all the cuts
        _K1Comb12Cuts  = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV)" % params
        _K1CombCuts    = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV) & ((APT1+APT2+APT3) > %(K1_SumPTHad)s*MeV)" % params

        _K1MotherCuts  = "(VFASPF(VCHI2PDOF) < %(K1_VtxChi2)s) & (SUMTREE(MIPCHI2DV(PRIMARY),((ABSID=='K+') | (ABSID=='pi+') | (ABSID=='p+') | (ABSID=='p~-')),0.0) > %(K1_SumIPChi2Had)s)" % params
        _daughtersCutsPi = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (HASRICH)" % params
        _daughtersCutsP = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNp > 0.05) & (P > 5000) & (HASRICH)" % params

        _Combine = DaVinci__N3BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "pi+"  : _daughtersCutsPi,
            "p+" : _daughtersCutsP }

        _Combine.Combination12Cut = _K1Comb12Cuts
        _Combine.CombinationCut   = _K1CombCuts
        _Combine.MotherCut        = _K1MotherCuts

        # make and store the Selection object
        return Selection( name, Algorithm = _Combine, RequiredSelections = [ pions, protons ] )

#####################################################
    def _makepLambda( self, name, lambdasLL, lambdasDD, lambdasLD, protons, params ) :
        """
        Make an K* -> p+ Lambda~0
        K* is just a proxy to get the two-body combination
        """

        _Decays = "[K*_0(1430)+ -> p+ Lambda~0]cc"

         # define all the cuts

        _CombinationCut    = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2PDOF) < %(K1_VtxChi2)s)" % params
        _daughtersCutsP = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PROBNNp > 0.05) & (P > 5000) & (HASRICH)" % params
        _daughtersCutsL = "(M > 1105*MeV) & (M<1130*MeV)"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Lambda0"  : _daughtersCutsL,
            "p+" : _daughtersCutsP }

        #_Combine.CombinationCut   = _CombinationCut
        #_Combine.MotherCut        = _MotherCut

        #return Selection( name, Algorithm = _Combine, RequiredSelections = [ pions, protons ] )

        _sel_pLambdaLL = Selection(name+"_LL",
            RequiredSelections = [ lambdasLL, protons ],
            Algorithm = _Combine)

        _sel_pLambdaDD = Selection(name+"_DD",
            RequiredSelections = [ lambdasDD, protons ],
            Algorithm = _Combine)

        _sel_pLambdaLD = Selection(name+"_LD",
            RequiredSelections = [ lambdasLD, protons ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_pLambdaLL, _sel_pLambdaDD, _sel_pLambdaLD ])

        return _sel

#####################################################
    def _makeLambdaLambda( self, name, lambdasLL, lambdasDD, lambdasLD, params ) :
        """
        Make an f2 -> Lambda0 Lambda~0
        f2 is just a proxy to get the two-body combination
        """

        _Decays = "f_2(2300) -> Lambda0 Lambda~0"

         # define all the cuts

        _CombinationCut    = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2PDOF) < %(LamLam_VtxChi2)s)" % params
        _daughtersCutsL = "(M > 1105*MeV) & (M<1130*MeV)"

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "Lambda0"  : _daughtersCutsL,
            "Lambda~0"  : _daughtersCutsL, }

        #_Combine.CombinationCut   = _CombinationCut
        #_Combine.MotherCut        = _MotherCut

        #return Selection( name, Algorithm = _Combine, RequiredSelections = [ pions, protons ] )

        _sel_LamLamLLLL = Selection(name+"_LLLL",
            RequiredSelections = [ lambdasLL, lambdasLL ],
            Algorithm = _Combine)

        _sel_LamLamLLDD = Selection(name+"_LLDD",
            RequiredSelections = [ lambdasLL, lambdasDD ],
            Algorithm = _Combine)

        _sel_LamLamDDDD = Selection(name+"_DDDD",
            RequiredSelections = [ lambdasDD, lambdasDD ],
            Algorithm = _Combine)

        _sel_LamLamLLLD = Selection(name+"_LLLD",
            RequiredSelections = [ lambdasLL, lambdasLD ],
            Algorithm = _Combine)

        _sel_LamLamLDLD = Selection(name+"_LDLD",
            RequiredSelections = [ lambdasLD, lambdasLD ],
            Algorithm = _Combine)

        _sel_LamLamDDLD = Selection(name+"_DDLD",
            RequiredSelections = [ lambdasDD, lambdasLD ],
            Algorithm = _Combine)

        _sel = MergedSelection(name+"_all",
            RequiredSelections = [ _sel_LamLamLLLL, _sel_LamLamLLDD, _sel_LamLamDDDD, _sel_LamLamLLLD, _sel_LamLamLDLD, _sel_LamLamDDLD ])

        return _sel


####################################################
    def _makeMuMuSS( self, name, params, muonid = None):
        """
        Makes MuMu same sign combinations
        """
        from StandardParticles import StdLooseMuons as Muons

        _DecayDescriptor = "[J/psi(1S) -> mu+ mu+]cc"
        _MassCut = "(AM > 100*MeV)"
        _MotherCut = "(VFASPF(VCHI2/VDOF) < 9)"
        _DaughtersCut = "(PT > %(LeptonPT)s) & " \
                        "(MIPCHI2DV(PRIMARY) > %(LeptonIPCHI2)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _DecayDescriptor,
                                     CombinationCut  = _MassCut,
                                     MotherCut       = _MotherCut )

        _MuonCut     = _DaughtersCut

        if muonid     : _MuonCut     += ( "&" + muonid )

        _Combine.DaughtersCuts = {
            "mu+" : _MuonCut,
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ Muons ] )


#####################################################
    def _makeB2LLX( self, name, dilepton, hadrons, params, masscut = "(ADAMASS('B+')< 1500 *MeV" ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [
                    "[ B+ -> J/psi(1S) K_1(1270)+ ]cc",
                    "[ B+ -> J/psi(1S) K*_0(1430)+ ]cc",
                    "[ B+ -> J/psi(1S) a_1(1260)+ ]cc",
                    "B_s0 -> J/psi(1S) f_2(2300)",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )


#####################################################
