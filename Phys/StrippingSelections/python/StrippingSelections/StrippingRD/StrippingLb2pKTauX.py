###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = [' Yunxuan Song','A.Venkateswaran','Han Gao']
__date__ = '14/07/2023'
__version__ = '$Revision: 0.1$'

# Stripping line for B->Htautau

from GaudiKernel.SystemOfUnits import MeV
from GaudiKernel.SystemOfUnits import mm
"""
  Lb->LambdaTauTau_3pi_3pi
  Lb->pKTauTau_3pi_3pi
  Lb->pKTauTau_3pi_pi
  Lb->pKTauTau_3pi_mu
  Lb->pKTauTau_3pi_e
  Lb->pKDD for bkg check
"""
__all__ = ('Lb2pKTauXConf', 'getRelInfoLb2pKTauX', 'default_config')

default_config = {
    'NAME': 'Lb2pKTauX',
    'BUILDERTYPE': 'Lb2pKTauXConf',
    'WGs': ['RD'],
    'CONFIG': {
        'SpdMult': '600',
        #
        'FD_B_Max': 100 * mm,
        'PT_B': 4000 * MeV,
        'P_B': 30000 * MeV,
        'FDCHI2_B': 16,
        'MASS_LOW_B': 3000 * MeV,
        'MASS_HIGH_B': 7000 * MeV,
        'DLSForLongLived': 5.,
        #
        'VCHI2_pK': 9,
        'PT_kaon': 250 * MeV,
        'PT_pion': 500 * MeV,
        'PT_muon': 250 * MeV,
        'PT_elec': 250 * MeV,
        'PT_proton': 250 * MeV,
        'PT_Ls': 1000 * MeV,
        'P_kaon': 3000 * MeV,
        'P_pion': 3000 * MeV,
        'P_muon': 3000 * MeV,
        'P_elec': 3000 * MeV,
        'P_proton': 3000 * MeV,
        'PID_KaonCuts_PIDK': 0,
        'PID_PionCuts_PIDK': 0,
        'PID_PionCuts_PIDmu': 0,
        'PID_MuonCuts_PIDmu': 0,
        'PID_ElecCuts_PIDe': 0,
        'PID_ProtonCuts_PIDpk': 0,
        'PID_ProtonCuts_PIDk': 0,
        'IPCHI2_Tr': 16,
        'TRACKCHI2_Tr': 4,
        'TRGHOPROB_Tr': 0.4,
        'MASS_LOW_pK': 1400 * MeV,
        'MASS_HIGH_pK': 2400 * MeV,
        'MASS_HIGH_Tau': 1800 * MeV,
        #Tau pion
        'PT_TauPion': 250 * MeV,
        'P_TauPion': 2000 * MeV,
        'MIPCHI2DV_TauPion': 16.0,
        'TRACKCHI2_TauPion': 4,
        'TRGHOPROB_TauPion': 0.4,
        'TRCHI2DOF_TauPion': 4,
        'PROBNNpi_TauPion': 0.55,
        #Tau Kaon
        'PT_TauKaon': 250 * MeV,
        'P_TauKaon': 2000 * MeV,
        'MIPCHI2DV_TauKaon': 16.0,
        'TRACKCHI2_TauKaon': 4,
        'TRGHOPROB_TauKaon': 0.4,
        'TRCHI2DOF_TauKaon': 4,
        #Loose Tau
        'LooseTau_APT': 800.*MeV,
        'LooseTau_AM_LOW': 400.*MeV,
        'LooseTau_AM_HIGH': 2100.*MeV,
        'LooseTau_AMAXDOCA': 0.2*mm,
        #Loose Tau Combine
        'LooseTau_MOTHER_PT': 1000.*MeV,
        'LooseTau_MOTHER_M_LOW': 500.*MeV,
        'LooseTau_MOTHER_M_HIGH': 2000.*MeV,
        'LooseTau_MOTHER_BPVDIRA': 0.99,
        'LooseTau_MOTHER_VFASPF': 16,
        'LooseTau_MOTHER_BPVVDCHI2': 16,
        'LooseTau_MOTHER_BPVVDRHO_LOW': 0.1*mm,
        'LooseTau_MOTHER_BPVVDRHO_HIGH': 7.0*mm,
        'LooseTau_MOTHER_BPVVDZ': 5.0*mm,
        #M12 cut
        'M12_HIGH_LsTauTau_3pi_3pi': 6000,
        'M12_HIGH_pKTauTau_3pi_3pi': 4200,
        'M12_HIGH_pKTauTau_3pi_pi': 4200,
        'M12_HIGH_pKTauTau_3pi_mu': 4200,
        'M12_HIGH_pKTauTau_3pi_e': 4200,
        'M12_HIGH_pKDD_3pi_3pi': 4200,
        'M12_HIGH_pKDD_3pi_mu': 4200,
        'M12_HIGH_pKDD_3pi_e': 4200,
        'M12_HIGH_pKDD_3pi_3pi': 4200,
        #
        'B2HTauTau_LinePrescale': 1,
        'B2HTauTau_LinePostscale': 1,
        # X0TauTau RelInfo (cover Lambda(*) Taup->(3pi) Taum->3pi, mu ,e ,pi
        'RelInfoTools_X0DD':
        [
            {"Type": "RelInfoVertexIsolation",
             "Location": "BVars_VertexIsoInfo",
             "DaughterLocations": {"[Beauty ->  ^D+  X- X0 ]CC": "Dp_VertexIsoInfo",
                                   "[Beauty ->   D+ ^X- X0 ]CC": "Xm_VertexIsoInfo"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 0.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone05",
             "DaughterLocations": {"[Beauty ->  D+  X- ^X0]CC": "H_ConeIsoInfo_Cone05",
                                   "[Beauty -> ^D+  X-  X0]CC": "Dp_ConeIsoInfo_Cone05",
                                   "[Beauty ->  D+ ^X-  X0]CC": "Xm_ConeIsoInfo_Cone05"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone10",
             "DaughterLocations": {"[Beauty ->  D+  X- ^X0]CC": "H_ConeIsoInfo_Cone10",
                                   "[Beauty -> ^D+  X-  X0]CC": "Dp_ConeIsoInfo_Cone10",
                                   "[Beauty ->  D+ ^X-  X0]CC": "Xm_ConeIsoInfo_Cone10"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone15",
             "DaughterLocations": {"[Beauty ->  D+  X- ^X0]CC": "H_ConeIsoInfo_Cone15",
                                   "[Beauty -> ^D+  X-  X0]CC": "Dp_ConeIsoInfo_Cone15",
                                   "[Beauty ->  D+ ^X-  X0]CC": "Xm_ConeIsoInfo_Cone15"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 2.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone20",
             "DaughterLocations": {"[Beauty ->  D+  X- ^X0]CC": "H_ConeIsoInfo_Cone20",
                                   "[Beauty -> ^D+  X-  X0]CC": "Dp_ConeIsoInfo_Cone20",
                                   "[Beauty ->  D+ ^X-  X0]CC": "Xm_ConeIsoInfo_Cone20"
                                   }
             },
            {'Type': 'RelInfoVertexIsolationBDT',
             'Location': 'BVars_VertexIsoBDTInfo',
             'DaughterLocations': {"[Beauty ->  D+  X- ^X0]CC": "H_VertexIsoBDTInfo",
                                   "[Beauty -> ^D+  X-  X0]CC": "Dp_VertexIsoBDTInfo",
                                   "[Beauty ->  D+ ^X-  X0]CC": "Xm_VertexIsoBDTInfo"
                                   }
             },
            {"Type": "RelInfoBKsttautauTauIsolationBDT",
             "Location": "B2KstTauTau_TauIsolationBDT"
             },
        ],
        # X0TauTau RelInfo (cover Lambda(*) Taup->(3pi) Taum->3pi, mu ,e ,pi
        'RelInfoTools_X0TauTau':
        [
            {"Type": "RelInfoVertexIsolation",
             "Location": "BVars_VertexIsoInfo",
             "DaughterLocations": {"[Beauty ->  ^tau+  X- X0 ]CC": "Taup_VertexIsoInfo",
                                   "[Beauty ->   tau+ ^X- X0 ]CC": "Xm_VertexIsoInfo"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 0.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone05",
             "DaughterLocations": {"[Beauty ->  tau+  X- ^X0]CC": "H_ConeIsoInfo_Cone05",
                                   "[Beauty -> ^tau+  X-  X0]CC": "Taup_ConeIsoInfo_Cone05",
                                   "[Beauty ->  tau+ ^X-  X0]CC": "Xm_ConeIsoInfo_Cone05"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone10",
             "DaughterLocations": {"[Beauty ->  tau+  X- ^X0]CC": "H_ConeIsoInfo_Cone10",
                                   "[Beauty -> ^tau+  X-  X0]CC": "Taup_ConeIsoInfo_Cone10",
                                   "[Beauty ->  tau+ ^X-  X0]CC": "Xm_ConeIsoInfo_Cone10"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone15",
             "DaughterLocations": {"[Beauty ->  tau+  X- ^X0]CC": "H_ConeIsoInfo_Cone15",
                                   "[Beauty -> ^tau+  X-  X0]CC": "Taup_ConeIsoInfo_Cone15",
                                   "[Beauty ->  tau+ ^X-  X0]CC": "Xm_ConeIsoInfo_Cone15"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 2.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone20",
             "DaughterLocations": {"[Beauty ->  tau+  X- ^X0]CC": "H_ConeIsoInfo_Cone20",
                                   "[Beauty -> ^tau+  X-  X0]CC": "Taup_ConeIsoInfo_Cone20",
                                   "[Beauty ->  tau+ ^X-  X0]CC": "Xm_ConeIsoInfo_Cone20"
                                   }
             },
            {'Type': 'RelInfoVertexIsolationBDT',
             'Location': 'BVars_VertexIsoBDTInfo',
             'DaughterLocations': {"[Beauty ->  tau+  X- ^X0]CC": "H_VertexIsoBDTInfo",
                                   "[Beauty -> ^tau+  X-  X0]CC": "Taup_VertexIsoBDTInfo",
                                   "[Beauty ->  tau+ ^X-  X0]CC": "Xm_VertexIsoBDTInfo"
                                   }
             },
            {"Type": "RelInfoBKsttautauTauIsolationBDT",
             "Location": "B2KstTauTau_TauIsolationBDT"
             },
        ],
                # X0TauTau RelInfo (cover Lambda(*) Taup->(3pi) Taum->3pi, mu ,e ,pi
        'RelInfoTools_X0TauTau_SS':
        [
            {"Type": "RelInfoVertexIsolation",
             "Location": "BVars_VertexIsoInfo",
             "DaughterLocations": {"[Beauty ->  ^tau+  X+ X0 ]CC": "Taup_VertexIsoInfo",
                                   "[Beauty ->   tau+ ^X+ X0 ]CC": "Xm_VertexIsoInfo"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 0.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone05",
             "DaughterLocations": {"[Beauty ->  tau+  X+ ^X0]CC": "H_ConeIsoInfo_Cone05",
                                   "[Beauty -> ^tau+  X+  X0]CC": "Taup_ConeIsoInfo_Cone05",
                                   "[Beauty ->  tau+ ^X+  X0]CC": "Xm_ConeIsoInfo_Cone05"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone10",
             "DaughterLocations": {"[Beauty ->  tau+  X+ ^X0]CC": "H_ConeIsoInfo_Cone10",
                                   "[Beauty -> ^tau+  X+  X0]CC": "Taup_ConeIsoInfo_Cone10",
                                   "[Beauty ->  tau+ ^X+  X0]CC": "Xm_ConeIsoInfo_Cone10"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 1.5,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone15",
             "DaughterLocations": {"[Beauty ->  tau+  X+ ^X0]CC": "H_ConeIsoInfo_Cone15",
                                   "[Beauty -> ^tau+  X+  X0]CC": "Taup_ConeIsoInfo_Cone15",
                                   "[Beauty ->  tau+ ^X+  X0]CC": "Xm_ConeIsoInfo_Cone15"
                                   }
             },
            {"Type": "RelInfoConeIsolation",
             "ConeSize": 2.0,
             "Variables": [],
             "Location": "BVars_ConeIsoInfo_Cone20",
             "DaughterLocations": {"[Beauty ->  tau+  X+ ^X0]CC": "H_ConeIsoInfo_Cone20",
                                   "[Beauty -> ^tau+  X+  X0]CC": "Taup_ConeIsoInfo_Cone20",
                                   "[Beauty ->  tau+ ^X+  X0]CC": "Xm_ConeIsoInfo_Cone20"
                                   }
             },
            {'Type': 'RelInfoVertexIsolationBDT',
             'Location': 'BVars_VertexIsoBDTInfo',
             'DaughterLocations': {"[Beauty ->  tau+  X+ ^X0]CC": "H_VertexIsoBDTInfo",
                                   "[Beauty -> ^tau+  X+  X0]CC": "Taup_VertexIsoBDTInfo",
                                   "[Beauty ->  tau+ ^X+  X0]CC": "Xm_VertexIsoBDTInfo"
                                   }
             },
            {"Type": "RelInfoBKsttautauTauIsolationBDT",
             "Location": "B2KstTauTau_TauIsolationBDT"
             },
        ],
        

    },
    'STREAMS': ['Bhadron']
}

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, OfflineVertexFitter, DaVinci__N3BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdNoPIDsPions
from StandardParticles import StdNoPIDsKaons
from StandardParticles import StdNoPIDsMuons
from StandardParticles import StdNoPIDsElectrons
from StandardParticles import StdNoPIDsProtons
from StandardParticles import StdLoosePions



class Lb2pKTauXConf(LineBuilder):
    """
      Builder for Lb->pKTauTau_3pi_pi
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        trackCuts = "(MIPCHI2DV(PRIMARY) > %(IPCHI2_Tr)s) & (TRCHI2DOF < %(TRACKCHI2_Tr)s) & (TRGHOSTPROB < %(TRGHOPROB_Tr)s)" % config
        KaonCuts = trackCuts + " & (PT > %(PT_kaon)s) & (P > %(P_kaon)s) & ((PIDK)>%(PID_KaonCuts_PIDK)s)" % config
        PionCuts = trackCuts + " & (PT > %(PT_pion)s) & (P > %(P_pion)s) & ((PIDK)<%(PID_PionCuts_PIDK)s) & ((PIDmu)<%(PID_PionCuts_PIDmu)s)" % config
        MuonCuts = trackCuts + " & (PT > %(PT_muon)s) & (P > %(P_muon)s) & ((PIDmu)>%(PID_MuonCuts_PIDmu)s)" % config
        ElecCuts = trackCuts + " & (PT > %(PT_elec)s) & (P > %(P_elec)s) & ((PIDe)>%(PID_ElecCuts_PIDe)s)" % config
        ProtonCuts = trackCuts + " & (PT > %(PT_proton)s) & (P > %(P_proton)s) & ((PIDp-PIDK)>%(PID_ProtonCuts_PIDpk)s) & ((PIDp)>%(PID_ProtonCuts_PIDk)s)" % config
        TauPionCuts = "(PT>%(PT_TauPion)s) & (P>%(P_TauPion)s) & (MIPCHI2DV(PRIMARY) > %(MIPCHI2DV_TauPion)s) & (TRCHI2DOF<%(TRCHI2DOF_TauPion)s) & (TRGHOSTPROB< %(TRGHOPROB_TauPion)s) & (PROBNNpi > %(PROBNNpi_TauPion)s)" % config
        TauKaonCuts = "(PT>%(PT_TauKaon)s) & (P>%(P_TauKaon)s) & (MIPCHI2DV(PRIMARY) > %(MIPCHI2DV_TauKaon)s) & (TRCHI2DOF<%(TRCHI2DOF_TauKaon)s) & (TRGHOSTPROB< %(TRGHOPROB_TauKaon)s)" % config

        TauCuts = "(M < %(MASS_HIGH_Tau)s)" % config

        self.FilterSPD = {
            'Code':
            " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )"
            % config,
            'Preambulo': [
                "from LoKiNumbers.decorators import *",
                "from LoKiCore.basic import LHCb"
            ]
        }

        self.rawTau = DataOnDemand("Phys/StdTightDetachedTau3pi/Particles")
        self.selTau = SimpleSelection("Tau" + name, FilterDesktop, [self.rawTau], Code=TauCuts)

        self.LambdaListLoose = MergedSelection(
            "StdLooseLambdaMergedFor" + name, 
            RequiredSelections=[
                DataOnDemand(Location="Phys/StdLooseLambdaDD/Particles"),
                DataOnDemand(Location="Phys/StdLooseLambdaLL/Particles")
            ])
        self.LambdaList =  self.createSubSel(OutputList = "LambdaFor" + name,
                                             InputList = self.LambdaListLoose ,
                                             Cuts = "(MAXTREE('p+'==ABSID, PT) > 100.*MeV) "\
                                             "& (ADMASS('Lambda0') < 15.*MeV) & (VFASPF(VCHI2) < 20 )  & (BPVDLS> %(DLSForLongLived)s ) " % config)

        self.selKaon = SimpleSelection("Kaon" + name, FilterDesktop, [StdNoPIDsKaons], Code=KaonCuts)
        self.selPion = SimpleSelection("Pion" + name, FilterDesktop, [StdNoPIDsPions], Code=PionCuts)
        self.selMuon = SimpleSelection("Muon" + name, FilterDesktop, [StdNoPIDsMuons], Code=MuonCuts)
        self.selElec = SimpleSelection("Elec" + name, FilterDesktop, [StdNoPIDsElectrons], Code=ElecCuts)
        self.selProton = SimpleSelection("Proton" + name, FilterDesktop, [StdNoPIDsProtons], Code=ProtonCuts)
        #self.selTauKaon = SimpleSelection("TauKaon" + name, FilterDesktop, [StdNoPIDsKaons], Code=TauKaonCuts)
        #self.selTauPion = SimpleSelection("TauPion" + name, FilterDesktop, [StdNoPIDsPions], Code=TauPionCuts)

        self.selpK = self._makepK("pK" + name, ProtonCuts, KaonCuts, config)
        self.selLooseTau = self._makelooseTau("LooseTau" + name, TauPionCuts, TauKaonCuts, config)
        
        self.selB2LsTauTau_3pi_3pi  = self._makeLb2LsTauTau_3pi_3pi(name + "_Ls3pi3pi", self.selTau, self.LambdaList, config)
        self.selB2LsTauTauSS_3pi_3pi= self._makeLb2LsTauTau_3pi_3pi(name + "_Ls3pi3pi", self.selTau, self.LambdaList, config, SS=True)
        
        self.selB2pKTauTau_3pi_3pi  = self._makeLb2pKTauTau_3pi_3pi(name + "_pK3pi3pi", self.selTau, self.selpK, config)
        self.selB2pKTauTauSS_3pi_3pi= self._makeLb2pKTauTau_3pi_3pi(name + "_pK3pi3pi", self.selTau, self.selpK, config, SS=True)

        self.selB2pKTauTau_3pi_pi   = self._makeLb2pKTauTau_3pi_pi(name + "_pK3pipi", self.selTau, self.selpK, self.selPion, config)
        self.selB2pKTauTauSS_3pi_pi = self._makeLb2pKTauTau_3pi_pi(name + "_pK3pipi", self.selTau, self.selpK, self.selPion, config, SS=True)
        
        self.selB2pKTauTau_3pi_mu   = self._makeLb2pKTauTau_3pi_mu(name + "_pK3pimu", self.selTau, self.selpK, self.selMuon, config)
        self.selB2pKTauTauSS_3pi_mu = self._makeLb2pKTauTau_3pi_mu(name + "_pK3pimu", self.selTau, self.selpK, self.selMuon, config, SS=True)
        
        self.selB2pKTauTau_3pi_e    = self._makeLb2pKTauTau_3pi_e(name + "_pK3pie", self.selTau, self.selpK, self.selElec, config)
        self.selB2pKTauTauSS_3pi_e  = self._makeLb2pKTauTau_3pi_e(name + "_pK3pie", self.selTau, self.selpK, self.selElec, config, SS=True)

        self.selB2pKDD_3pi_3pi  = self._makeLb2pKDD_3pi_3pi(name + "_pKDD3pi3pi", self.selLooseTau, self.selpK, config)
        self.selB2pKDDSS_3pi_3pi= self._makeLb2pKDD_3pi_3pi(name + "_pKDD3pi3pi", self.selLooseTau, self.selpK, config, SS=True)

        self.selB2pKDD_3pi_mu   = self._makeLb2pKDD_3pi_mu(name + "_pKDD3pimu", self.selLooseTau, self.selpK, self.selMuon, config)
        self.selB2pKDDSS_3pi_mu = self._makeLb2pKDD_3pi_mu(name + "_pKDD3pimu", self.selLooseTau, self.selpK, self.selMuon, config, SS=True)
        
        self.selB2pKDD_3pi_e    = self._makeLb2pKDD_3pi_e(name + "_pKDD3pie", self.selLooseTau, self.selpK, self.selElec, config)
        self.selB2pKDDSS_3pi_e  = self._makeLb2pKDD_3pi_e(name + "_pKDD3pie", self.selLooseTau, self.selpK, self.selElec, config, SS=True)

        ## Finished making selections build and register lines
        self.LsTauTau3pi3pi_Line   = self._makeLine("B2LsTauTau3pi3piLine"  , self.selB2LsTauTau_3pi_3pi  , config)
        self.LsTauTau3pi3piSS_Line = self._makeLine("B2LsTauTau3pi3piSSLine", self.selB2LsTauTauSS_3pi_3pi, config)
        
        self.pKTauTau3pi3pi_Line   = self._makeLine("B2pKTauTau3pi3piLine"  , self.selB2pKTauTau_3pi_3pi  , config)
        self.pKTauTau3pi3piSS_Line = self._makeLine("B2pKTauTau3pi3piSSLine", self.selB2pKTauTauSS_3pi_3pi, config)

        self.pKTauTau3pipi_Line   = self._makeLine("B2pKTauTau3pipiLine"  , self.selB2pKTauTau_3pi_pi  , config)
        self.pKTauTau3pipiSS_Line = self._makeLine("B2pKTauTau3pipiSSLine", self.selB2pKTauTauSS_3pi_pi, config)

        self.pKTauTau3pimu_Line   = self._makeLine("B2pKTauTau3pimuLine"  , self.selB2pKTauTau_3pi_mu  , config)
        self.pKTauTau3pimuSS_Line = self._makeLine("B2pKTauTau3pimuSSLine", self.selB2pKTauTauSS_3pi_mu, config)

        self.pKTauTau3pie_Line    = self._makeLine("B2pKTauTau3pieLine"  , self.selB2pKTauTau_3pi_e  , config)
        self.pKTauTau3pieSS_Line  = self._makeLine("B2pKTauTau3pieSSLine", self.selB2pKTauTauSS_3pi_e, config)

        self.pKDD3pi3pi_Line   = self._makeLine("B2pKDD3pi3piLine"  , self.selB2pKDD_3pi_3pi  , config)
        self.pKDD3pi3piSS_Line = self._makeLine("B2pKDD3pi3piSSLine", self.selB2pKDDSS_3pi_3pi, config)

        self.pKDD3pimu_Line   = self._makeLine("B2pKDD3pimuLine"  , self.selB2pKDD_3pi_mu  , config)
        self.pKDD3pimuSS_Line = self._makeLine("B2pKDD3pimuSSLine", self.selB2pKDDSS_3pi_mu, config)

        self.pKDD3pie_Line    = self._makeLine("B2pKDD3pieLine"  , self.selB2pKDD_3pi_e  , config)
        self.pKDD3pieSS_Line  = self._makeLine("B2pKDD3pieSSLine", self.selB2pKDDSS_3pi_e, config)

    def createSubSel(self, OutputList, InputList, Cuts):
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code=Cuts)
        return Selection(
            OutputList, Algorithm=filter, RequiredSelections=[InputList])


#### Make resonances ###################################################

    def _makepK(self, name, protonSel, kaonSel, config):

        combcut = "in_range ( %(MASS_LOW_pK)s, AM, %(MASS_HIGH_pK)s )" % config
        mothercut = " (PT > %(PT_Ls)s) & (VFASPF(VCHI2) < %(VCHI2_pK)s)" % config

        daucut = {'p+': protonSel, 'K-': kaonSel, 'p~-': protonSel, 'K+': kaonSel}

        return SimpleSelection(
            name + "_pK",
            CombineParticles, [StdNoPIDsProtons, StdNoPIDsKaons],
            DecayDescriptors=["[Lambda(1520)0 -> p+ K-]cc"],
            CombinationCut=combcut,
            MotherCut=mothercut,
            DaughtersCuts=daucut)

    def _makelooseTau(self, name, pionSel, kaonSel, config):####Same as StdTightDetachedTau3pi, but wrong PID for pi

        combcut = "(APT>%(LooseTau_APT)s) & ((AM>%(LooseTau_AM_LOW)s) & (AM<%(LooseTau_AM_HIGH)s)) & (AMAXDOCA('')<%(LooseTau_AMAXDOCA)s) & (ANUM(PT > 800*MeV) >= 1)" % config
        mothercut = "(PT>%(LooseTau_MOTHER_PT)s) & (M>%(LooseTau_MOTHER_M_LOW)s) & (M<%(LooseTau_MOTHER_M_HIGH)s) & (BPVDIRA>%(LooseTau_MOTHER_BPVDIRA)s) & (VFASPF(VCHI2) < %(LooseTau_MOTHER_VFASPF)s) & (BPVVDCHI2>%(LooseTau_MOTHER_BPVVDCHI2)s) & (BPVVDRHO>%(LooseTau_MOTHER_BPVVDRHO_LOW)s) & (BPVVDRHO<%(LooseTau_MOTHER_BPVVDRHO_HIGH)s) & (BPVVDZ>%(LooseTau_MOTHER_BPVVDZ)s)" % config

        daucut = {'pi+': pionSel, 'K-': kaonSel, 'pi-': pionSel, 'K+': kaonSel}

        return SimpleSelection(
            name + "_D",
            CombineParticles, [StdLoosePions, StdNoPIDsKaons],
            DecayDescriptors=["[D+ -> K- pi+ pi+]cc"],
            CombinationCut=combcut,
            MotherCut=mothercut,
            DaughtersCuts=daucut)

#### Make B ###################################################

    def _makeLb2LsTauTau_3pi_3pi(self, name, tauSel, pKSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config

        n = name + "TauTau"

        descriptors = ["[Lambda_b0 -> tau+ tau- Lambda0]cc"]

        if SS:
            n += "SS"
            descriptors = [
                "[Lambda_b0 ->  tau+ tau+ Lambda0]cc",
                "[Lambda_b0 ->  tau- tau- Lambda0]cc"
            ]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM<%(M12_HIGH_LsTauTau_3pi_3pi)s" % config,
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            n, Algorithm=Combine, RequiredSelections=[tauSel, pKSel])

    def _makeLb2pKTauTau_3pi_3pi(self, name, tauSel, pKSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config

        n = name + "TauTau"

        descriptors = ["[Lambda_b0 -> tau+ tau- Lambda(1520)0]cc"]

        if SS:
            n += "SS"
            descriptors = [
                "[Lambda_b0 ->  tau+ tau+ Lambda(1520)0]cc",
                "[Lambda_b0 ->  tau- tau- Lambda(1520)0]cc"
            ]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM<%(M12_HIGH_pKTauTau_3pi_3pi)s" % config,
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            n, Algorithm=Combine, RequiredSelections=[tauSel, pKSel])

    def _makeLb2pKTauTau_3pi_pi(self, name, tauSel, pKSel, XSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config

        n = name + "TauTau"

        descriptors = [
                "[Lambda_b0 -> tau+ pi- Lambda(1520)0]cc",
                "[Lambda_b0 -> tau- pi+ Lambda(1520)0]cc"
            ]

        if SS:
            n += "SS"
            descriptors = [
                "[Lambda_b0 -> tau+ pi+ Lambda(1520)0]cc",
                "[Lambda_b0 -> tau- pi- Lambda(1520)0]cc"
            ]
        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM<%(M12_HIGH_pKTauTau_3pi_pi)s" % config,
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            n, Algorithm=Combine, RequiredSelections=[tauSel, pKSel, XSel])

    def _makeLb2pKTauTau_3pi_mu(self, name, tauSel, pKSel, XSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config

        n = name + "TauTau"

        descriptors = [
                "[Lambda_b0 -> tau+ mu-  Lambda(1520)0]cc",
                "[Lambda_b0 -> tau- mu+  Lambda(1520)0]cc"
            ]

        if SS:
            n += "SS"
            descriptors = [
                "[Lambda_b0 -> tau+ mu+ Lambda(1520)0]cc",
                "[Lambda_b0 -> tau- mu- Lambda(1520)0]cc"
            ]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM<%(M12_HIGH_pKTauTau_3pi_mu)s" % config,
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            n, Algorithm=Combine, RequiredSelections=[tauSel, pKSel, XSel])

    def _makeLb2pKTauTau_3pi_e(self, name, tauSel, pKSel, XSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config

        n = name + "TauTau"

        descriptors = [
                "[Lambda_b0 -> tau+ e- Lambda(1520)0]cc",
                "[Lambda_b0 -> tau- e+ Lambda(1520)0]cc"
            ]

        if SS:
            n += "SS"
            descriptors = [
                "[Lambda_b0 -> tau+ e+ Lambda(1520)0]cc",
                "[Lambda_b0 -> tau- e- Lambda(1520)0]cc"
            ]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM<%(M12_HIGH_pKTauTau_3pi_e)s" % config,
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            n, Algorithm=Combine, RequiredSelections=[tauSel, pKSel, XSel])


    def _makeLb2pKDD_3pi_3pi(self, name, tauSel, pKSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config

        n = name + "TauTau"

        descriptors = ["[Lambda_b0 -> D+ D- Lambda(1520)0]cc"]

        if SS:
            n += "SS"
            descriptors = [
                "[Lambda_b0 ->  D+ D+ Lambda(1520)0]cc",
                "[Lambda_b0 ->  D- D- Lambda(1520)0]cc"
            ]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM<%(M12_HIGH_pKDD_3pi_3pi)s" % config,
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            n, Algorithm=Combine, RequiredSelections=[tauSel, pKSel])

    def _makeLb2pKDD_3pi_mu(self, name, tauSel, pKSel, XSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config

        n = name + "DD"

        descriptors = [
                "[Lambda_b0 -> D+ mu-  Lambda(1520)0]cc",
                "[Lambda_b0 -> D- mu+  Lambda(1520)0]cc"
            ]

        if SS:
            n += "SS"
            descriptors = [
                "[Lambda_b0 -> D+ mu+ Lambda(1520)0]cc",
                "[Lambda_b0 -> D- mu- Lambda(1520)0]cc"
            ]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM<%(M12_HIGH_pKDD_3pi_mu)s" % config,
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            n, Algorithm=Combine, RequiredSelections=[tauSel, pKSel, XSel])

    def _makeLb2pKDD_3pi_e(self, name, tauSel, pKSel, XSel, config, SS=False):

        combcut = "in_range ( %(MASS_LOW_B)s, AM, %(MASS_HIGH_B)s )" % config
        mothercut = "( BPVVDCHI2 > %(FDCHI2_B)s ) & ( BPVVD < %(FD_B_Max)s ) " % config
        mothercut += " & (PT > %(PT_B)s) & (P > %(P_B)s) " % config

        n = name + "DD"

        descriptors = [
                "[Lambda_b0 -> D+ e- Lambda(1520)0]cc",
                "[Lambda_b0 -> D- e+ Lambda(1520)0]cc"
            ]

        if SS:
            n += "SS"
            descriptors = [
                "[Lambda_b0 -> D+ e+ Lambda(1520)0]cc",
                "[Lambda_b0 -> D- e- Lambda(1520)0]cc"
            ]

        Combine = DaVinci__N3BodyDecays(
            DecayDescriptors=descriptors,
            Combination12Cut="AM<%(M12_HIGH_pKDD_3pi_e)s" % config,
            CombinationCut=combcut,
            MotherCut=mothercut)

        return Selection(
            n, Algorithm=Combine, RequiredSelections=[tauSel, pKSel, XSel])


#### Helpers to make lines

    def _makeLine(self, name, sel, config):

        line = StrippingLine(
            name,
            prescale=config['B2HTauTau_LinePrescale'],
            postscale=config['B2HTauTau_LinePostscale'],
            #MDSTFlag = False,
            FILTER=self.FilterSPD,
            #RelatedInfoTools=config['RelInfoTools_X0TauTau'],#getRelInfoLb2pKTauX(),
            selection=sel,
            MaxCandidates=50)
        if 'TauTau' in name and 'SS' not in name:
            line.RelatedInfoTools=config['RelInfoTools_X0TauTau']
        elif 'TauTau' in name and 'SS' in name:
            line.RelatedInfoTools=config['RelInfoTools_X0TauTau_SS']
        elif 'DD' in name and 'SS' not in name:
            line.RelatedInfoTools=config['RelInfoTools_X0DD']
        self.registerLine(line)
        return line

