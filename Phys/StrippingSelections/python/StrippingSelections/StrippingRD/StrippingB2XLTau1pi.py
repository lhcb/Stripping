###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for
    Bs to KK l tau
    Bd to Kpi l tau
    Lb to p K l tau
    with single pion decays of tau
Same-sign combinations are included.
Extended from the stripping line S34r0p2 StrippingB2XMuTauMuonic.py by H. Tilquin
"""

__author__ = 'H. Tilquin, Lakshan Madhan'
__date__ = '26/06/2023'
__version__ = '$Revision: 1.1 $'

__all__ = ('B2XLTau1piConf', 'default_config')

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop

from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME': 'B2XLTau1pi',
    'BUILDERTYPE': 'B2XLTau1piConf',
    'CONFIG':
        {
            "Bs_Comb_MassHigh": 7250.0,
            "Bs_FlightChi2": 16,
            "Bd_Comb_MassHigh": 6300.0,
            "Bd_FlightChi2": 25,
            "Lb_Comb_MassHigh": 7750.0,
            "Lb_FlightChi2": 16,
            "B_DIRA": 0.9995,
            "Lb_DIRA": 0.999,
            "Bs_VertexCHI2": 100.0,
            "B0_VertexCHI2": 50.0,
            "Lb_VertexCHI2": 100.0,
            "Hadron_MinIPCHI2": 25.0,
            "Muon_MinIPCHI2": 25.0,
            "Electron_MinIPCHI2": 25.0, 
            "PionFromTau_MinIPCHI2": 9.0,
            "Phi_FlightChi2": 25.0,
            "Phi_Comb_MassHigh": 3650.0,
            "Phi_PT": 800,
            "Phi_VertexCHI2": 4,
            "Phi_DOCA": 0.15,
            "Phi_DOCACHI2": 6,
            "PhiMu_VertexCHI2": 6,
            "PhiMu_Comb_MassHigh": 3650.0,
            "PhiMu_DOCA": 0.8,
            "PhiMu_DOCACHI2": 9,
            "PhiE_VertexCHI2": 9,
            "PhiE_Comb_MassHigh": 3650.0,
            "PhiE_Comb_MassLow": 300,
            "PhiE_DOCA": 0.8,
            "PhiE_DOCACHI2": 9,
            "Kstar_FlightChi2": 25,
            "Kstar_Comb_MassHigh": 3550.0,
            "Kstar_Comb_MassLow": 600.0,
            "Kstar_PT": 1100,
            "Kstar_DOCA": 0.1,
            "Kstar_DOCACHI2": 1,
            "Kstar_VertexCHI2": 1,
            "KstarMu_FlightChi2": 200,
            "KstarMu_PT": 1700,
            "KstarMu_VertexCHI2": 1,
            "KstarMu_Comb_MassHigh": 3400,
            "KstarMu_Comb_MassLow": 700,
            "KstarMu_DOCA": 0.1,
            "KstarMu_DOCACHI2": 1,
            "KstarE_FlightChi2": 25,
            "KstarE_PT": 1400,
            "KstarE_VertexCHI2": 4,
            "KstarE_Comb_MassHigh": 3550.0,
            "KstarE_Comb_MassLow":550,
            "KstarE_DOCA": 0.15,
            "KstarE_DOCACHI2": 2,
            "Lambdastar_FlightChi2": 25,
            "Lambdastar_Comb_MassHigh": 5000.0,
            "Lambdastar_Comb_MassLow": 1400.0,
            "Lambdastar_PT": 1250,
            "Lambdastar_DOCA": 0.15,
            "Lambdastar_DOCACHI2": 4,
            "Lambdastar_VertexCHI2": 4,
            "LambdastarMu_Comb_MassHigh": 5000.0,
            "LambdastarMu_Comb_MassLow": 1500.0,
            "LambdastarMu_DOCA": 0.8,
            "LambdastarMu_DOCACHI2": 8,
            "LambdastarMu_VertexCHI2": 8,
            "LambdastarE_Comb_MassHigh": 4200,
            "LambdastarE_Comb_MassLow": 1250.0,
            "LambdastarE_DOCA": 0.8,
            "LambdastarE_DOCACHI2": 8,
            "LambdastarE_VertexCHI2": 8,
            "MuonPT": 500,
            "MuonPID": 1.0,
            "Muon_ProbNN": 0.1,
            "ElectronPT": 500,
            "ElectronPID": 6.0, 
            "Electron_ProbNN": 0.52, 
            # "PionFromTau_PIDK_max": 0.0,

            "PionFromTau_PT": 500, 
            "PionFromTau_PIDpiK_min": 14,  
            "PionFromTau_PIDpip_min": 14,
            "PionFromTau_PIDpie_min": 0, 
            "PionFromTau_PIDpimu_min": 4,
            "PionFromTau_ProbNN_min": 0.7,
            "KaonPID": 4.0,
            "Kaon_ProbNN": 0.2,
            "ProtonPID": 5.0,
            "Proton_ProbNN": 0.2,
            "Pion_ProbNN": 0.5,
            "Pion_ProbNN_B2Kstar": 0.88,
            "Kaon_Pion_ProbNN_B2Kstar": 0.88,
            "Muon_ProbNN_Kstar": 0.88,
            "MuonPT_Kstar": 800,
            "Hadron_P": 3000,
            "SpdMult": 600,
            "Track_GhostProb": 0.3,
            "Track_TRCHI2": 3,
            "UseNoPIDsHadrons": False,
            "UseNoPIDsMuons": False,
            "UseNoPIDsElectrons": False,
            "UseNoPIDsTauDau": False,
            "HLT1_FILTER": None,
            "HLT2_FILTER": None,
            "L0DU_FILTER": None,
        },

    'WGs': ['RD'],
    'STREAMS': ['Semileptonic']
}


class B2XLTau1piConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name

        self.BsCombCut = "(AM < %(Bs_Comb_MassHigh)s * MeV)" % config
        self.BsCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Bs_VertexCHI2)s) & (BPVVDCHI2 > %(Bs_FlightChi2)s)" % config

        self.BdCombCut = "(AM < %(Bd_Comb_MassHigh)s * MeV)" % config
        self.BdCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(B0_VertexCHI2)s) & (BPVVDCHI2 > %(Bd_FlightChi2)s)" % config

        self.LambdaBCombCut = "(AM < %(Lb_Comb_MassHigh)s * MeV)" % config
        self.LambdaBCut = "(BPVDIRA > %(Lb_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Lb_VertexCHI2)s) & (BPVVDCHI2 > %(Lb_FlightChi2)s)" % config

        self.PhiCombCut = "(AM < %(Phi_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(Phi_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(Phi_DOCACHI2)s, ''))" % config
        self.PhiCut = "(VFASPF(VCHI2/VDOF) < %(Phi_VertexCHI2)s) & (PT > %(Phi_PT)s * MeV) & (BPVVDCHI2 > %(Phi_FlightChi2)s)" % config

        self.KstarCombCut = "(AM < %(Kstar_Comb_MassHigh)s*MeV) & (AM > %(Kstar_Comb_MassLow)s * MeV) & " \
            "(ACUTDOCA(%(Kstar_DOCA)s * mm,'')) & (ACUTDOCACHI2(%(Kstar_DOCACHI2)s, ''))" % config
        self.KstarCut = "(VFASPF(VCHI2/VDOF) < %(Kstar_VertexCHI2)s) & " \
            "(PT > %(Kstar_PT)s * MeV) & (BPVVDCHI2 > %(Kstar_FlightChi2)s)" % config

        self.LambdastarCombCut = "(AM < %(Lambdastar_Comb_MassHigh)s*MeV) & (AM > %(Lambdastar_Comb_MassLow)s*MeV) & " \
            "(ACUTDOCA(%(Lambdastar_DOCA)s * mm,'')) & (ACUTDOCACHI2(%(Lambdastar_DOCACHI2)s, ''))" % config
        self.LambdastarCut = "(PT > %(Lambdastar_PT)s*MeV) & " \
            "(VFASPF(VCHI2/VDOF) < %(Lambdastar_VertexCHI2)s) & (BPVVDCHI2 > %(Lambdastar_FlightChi2)s)" % config

        self.PhiMuCut = "(VFASPF(VCHI2/VDOF) < %(PhiMu_VertexCHI2)s)" % config
        self.PhiMuCombCut = "(AM < %(PhiMu_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(PhiMu_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(PhiMu_DOCACHI2)s, ''))" % config

        self.KstarMuCut = "(PT > %(KstarMu_PT)s * MeV) & (VFASPF(VCHI2/VDOF) < %(KstarMu_VertexCHI2)s) & (BPVVDCHI2 > %(KstarMu_FlightChi2)s)" % config
        self.KstarMuCombCut = "(AM > %(KstarMu_Comb_MassLow)s * MeV) & (AM < %(KstarMu_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(KstarMu_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(KstarMu_DOCACHI2)s, ''))" % config

        self.LambdastarMuCut = "(VFASPF(VCHI2/VDOF) < %(LambdastarMu_VertexCHI2)s)" % config
        self.LambdastarMuCombCut = "(AM > %(LambdastarMu_Comb_MassLow)s * MeV) & (AM < %(LambdastarMu_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(LambdastarMu_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(LambdastarMu_DOCACHI2)s, ''))" % config


        self.PhiECut = "(VFASPF(VCHI2/VDOF) < %(PhiE_VertexCHI2)s)" % config
        self.PhiECombCut = "(AM > %(PhiE_Comb_MassLow)s * MeV) & (AM < %(PhiE_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(PhiE_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(PhiE_DOCACHI2)s, ''))" % config

        self.KstarECut = "(PT > %(KstarE_PT)s * MeV) & (VFASPF(VCHI2/VDOF) < %(KstarE_VertexCHI2)s) & (BPVVDCHI2 > %(KstarE_FlightChi2)s)" % config
        self.KstarECombCut = "(AM > %(KstarE_Comb_MassLow)s * MeV) & (AM < %(KstarE_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(KstarE_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(KstarE_DOCACHI2)s, ''))" % config

        self.LambdastarECut = "(VFASPF(VCHI2/VDOF) < %(LambdastarE_VertexCHI2)s)" % config
        self.LambdastarECombCut = "(AM > %(LambdastarE_Comb_MassLow)s * MeV) & (AM < %(LambdastarE_Comb_MassHigh)s * MeV) & (ACUTDOCA(%(LambdastarE_DOCA)s * mm, '')) & (ACUTDOCACHI2(%(LambdastarE_DOCACHI2)s, ''))" % config


        self.TrackCuts = "(TRGHP < %(Track_GhostProb)s) & (TRCHI2DOF < %(Track_TRCHI2)s)" % config

        self.HadronCuts = "(MIPCHI2DV(PRIMARY) > %(Hadron_MinIPCHI2)s)" % config

        self.KaonCutBase = self.TrackCuts + " & " + \
            self.HadronCuts + " & (P > %(Hadron_P)s*MeV)" % config
        self.KaonCut = self.KaonCutBase + \
            " & (PIDK > %(KaonPID)s) & (~ISMUON) & (PROBNNK > %(Kaon_ProbNN)s)" % config
        self.KaonCutReversePID = self.KaonCutBase + \
            " & (PIDK < %(KaonPID)s) & (PROBNNK < %(Kaon_ProbNN)s)" % config
        self.KaonCut_B2Kstar = self.KaonCutBase + \
            " & (PROBNNK * (1-PROBNNpi) > %(Kaon_Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.KaonCutReversePID_B2Kstar = self.KaonCutBase + \
            " & (PROBNNK * (1-PROBNNpi) < %(Kaon_Pion_ProbNN_B2Kstar)s)" % config

        self.PionCutBase = self.TrackCuts + " & " + self.HadronCuts
        self.PionCut_B2Kstar = self.PionCutBase + \
            " & (PROBNNpi > %(Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.PionCutReversePID_B2Kstar = self.PionCutBase + \
            " & (PROBNNpi < %(Pion_ProbNN_B2Kstar)s)" % config

        self.PionFromTauCutBase = self.TrackCuts + \
            " & (MIPCHI2DV(PRIMARY) > %(PionFromTau_MinIPCHI2)s) & (PT > %(PionFromTau_PT)s * MeV) " % config
        self.PionFromTauCut = self.PionFromTauCutBase + \
        " & (PIDpi-PIDK > %(PionFromTau_PIDpiK_min)s) & (PIDpi-PIDp > %(PionFromTau_PIDpip_min)s) & (PIDpi-PIDmu > %(PionFromTau_PIDpimu_min)s) & (PIDpi-PIDe > %(PionFromTau_PIDpie_min)s) & (PROBNNpi > %(PionFromTau_ProbNN_min)s) & (~ISMUON)" % config #

        self.PionFromTauCutReversePID = self.PionFromTauCutBase + \
            " & (PIDpi-PIDK < %(PionFromTau_PIDpiK_min)s) | (PIDpi-PIDp < %(PionFromTau_PIDpip_min)s) | (PIDpi-PIDmu < %(PionFromTau_PIDpimu_min)s) | (PIDpi-PIDe < %(PionFromTau_PIDpie_min)s) | (PROBNNpi < %(PionFromTau_ProbNN_min)s)" % config  # 

        self.ProtonCutBase = self.TrackCuts + " & " + \
            self.HadronCuts + " & (P > %(Hadron_P)s * MeV)" % config
        self.ProtonCut = self.ProtonCutBase + \
            " & (PIDp > %(ProtonPID)s)  & (~ISMUON) & (PROBNNp > %(Proton_ProbNN)s)" % config
        self.ProtonCutReversePID = self.ProtonCutBase + \
            " & (PIDp < %(ProtonPID)s) & (PROBNNp < %(Proton_ProbNN)s)" % config

        self.MuonCutBase = self.TrackCuts + \
            " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s)" % config
        self.MuonCut = self.MuonCutBase + \
            " & (PIDmu> %(MuonPID)s) & (ISMUON) & (PROBNNmu > %(Muon_ProbNN)s)" % config
        self.MuonCutReversePID = self.MuonCutBase + \
            " & (PIDmu < %(MuonPID)s)  & (PROBNNmu < %(Muon_ProbNN)s)" % config

        self.MuonFromBCutBase = self.MuonCutBase + \
            " & (PT > %(MuonPT)s * MeV)" % config
        self.MuonFromBCut = self.MuonCut + \
            " & (PT > %(MuonPT)s * MeV)" % config
        self.MuonFromBCutReversePID = self.MuonCutReversePID + \
            " & (PT > %(MuonPT)s * MeV)" % config


        self.MuonCutBase_Kstar = self.TrackCuts + \
            " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s)" % config
        self.MuonCut_Kstar = self.MuonCutBase_Kstar + \
            " & (PIDmu> %(MuonPID)s) & (ISMUON) & (PROBNNmu > %(Muon_ProbNN_Kstar)s)" % config
        self.MuonCutReversePID_Kstar = self.MuonCutBase_Kstar + \
            " & (PIDmu < %(MuonPID)s)  & (PROBNNmu < %(Muon_ProbNN_Kstar)s)" % config

        self.MuonFromBCutBase_Kstar = self.MuonCutBase_Kstar + \
            " & (PT > %(MuonPT_Kstar)s * MeV)" % config
        self.MuonFromBCut_Kstar = self.MuonCut_Kstar + \
            " & (PT > %(MuonPT_Kstar)s * MeV)" % config
        self.MuonFromBCutReversePID_Kstar = self.MuonCutReversePID_Kstar + \
            " & (PT > %(MuonPT_Kstar)s * MeV)" % config


        self.ElectronCutBase = self.TrackCuts + \
            " & (MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s) " % config
        self.ElectronCut = self.ElectronCutBase + \
            " & (PIDe> %(ElectronPID)s) & (PROBNNe > %(Electron_ProbNN)s) & (~ISMUON)" % config
        self.ElectronCutReversePID = self.ElectronCutBase + \
            " & (PIDe < %(ElectronPID)s) & (PROBNNe < %(Electron_ProbNN)s)" % config

        self.ElectronFromBCutBase = self.ElectronCutBase + \
            " & (PT > %(ElectronPT)s * MeV)" % config
        self.ElectronFromBCut = self.ElectronCut + \
            " & (PT > %(ElectronPT)s * MeV) " % config
        self.ElectronFromBCutReversePID = self.ElectronCutReversePID + \
            " & (PT > %(ElectronPT)s * MeV)" % config


        self.Kaons = self.__Kaons__(config)
        self.FakeKaons = self.__FakeKaons__()

        self.Kaons_Kstar = self.__Kaons__(config, sel_name="_B2Kstar")
        self.FakeKaons_Kstar = self.__FakeKaons__(sel_name="_B2Kstar")

        self.Pions_Kstar = self.__Pions__(config, sel_name="_B2Kstar")
        self.FakePions_Kstar = self.__FakePions__(sel_name="_B2Kstar")

        self.Protons = self.__Protons__(config)
        self.FakeProtons = self.__FakeProtons__()

        self.PionsFromTau = self.__PionsFromTau__(config)
        self.FakePionsFromTau = self.__FakePionsFromTau__()

        self.MuonsFromB = self.__Muons__(config)
        self.FakeMuonsFromB = self.__FakeMuons__()

        self.MuonsFromB_Kstar = self.__Muons__(config, Kstar=True)
        self.FakeMuonsFromB_Kstar = self.__FakeMuons__(Kstar=True)

        self.ElectronsFromB = self.__Electrons__(config)
        self.FakeElectronsFromB = self.__FakeElectrons__()

        self.Phi = self.__Phi__(self.Kaons, conf=config)
        self.FakePhi = self.__Phi__(self.Kaons, self.FakeKaons, conf=config)

        self.PhiMu = self.__PhiMu__(self.Phi, self.MuonsFromB)
        self.PhiMu_FakeKaon = self.__PhiMu__(
            self.FakePhi, self.MuonsFromB, pid_selection='ReversePIDK_')
        self.PhiMu_FakeMuon = self.__PhiMu__(
            self.Phi,  self.FakeMuonsFromB, pid_selection='ReversePIDmu_')

        self.PhiE = self.__PhiE__(self.Phi, self.ElectronsFromB)
        self.PhiE_FakeKaon = self.__PhiE__(
            self.FakePhi, self.ElectronsFromB, pid_selection='ReversePIDK_')
        self.PhiE_FakeElectron = self.__PhiE__(
            self.Phi,  self.FakeElectronsFromB, pid_selection='ReversePIDe_')

        self.Kstar = self.__Kstar__(self.Kaons_Kstar, self.Pions_Kstar)
        self.FakePionKstar = self.__Kstar__(
            self.Kaons_Kstar, self.FakePions_Kstar, pid_selection="ReversePIDPi_")
        self.FakeKaonKstar = self.__Kstar__(
            self.FakeKaons_Kstar, self.Pions_Kstar, pid_selection="ReversePIDK_")

        self.KstarMu = self.__KstarMu__(self.Kstar, self.MuonsFromB_Kstar)
        self.KstarMu_FakeKaon = self.__KstarMu__(
            self.FakeKaonKstar, self.MuonsFromB_Kstar, pid_selection='ReversePIDK_')
        self.KstarMu_FakePion = self.__KstarMu__(
            self.FakePionKstar, self.MuonsFromB_Kstar, pid_selection='ReversePIDpi_')
        self.KstarMu_FakeMuon = self.__KstarMu__(
            self.Kstar, self.FakeMuonsFromB_Kstar, pid_selection='ReversePIDmu_')

        self.KstarE = self.__KstarE__(self.Kstar, self.ElectronsFromB)
        self.KstarE_FakeKaon = self.__KstarE__(
            self.FakeKaonKstar, self.ElectronsFromB, pid_selection='ReversePIDK_')
        self.KstarE_FakePion = self.__KstarE__(
            self.FakePionKstar, self.ElectronsFromB, pid_selection='ReversePIDpi_')
        self.KstarE_FakeElectron = self.__KstarE__(
            self.Kstar, self.FakeElectronsFromB, pid_selection='ReversePIDe_')

        self.LambdaStar = self.__Lambdastar__(self.Protons, self.Kaons)
        self.FakeProtonLambdaStar = self.__Lambdastar__(
            self.FakeProtons, self.Kaons, pid_selection="ReversePIDp_")
        self.FakeKaonLambdaStar = self.__Lambdastar__(
            self.Protons, self.FakeKaons, pid_selection="ReversePIDK_")

        self.LambdastarMu = self.__LambdastarMu__(
            self.LambdaStar, self.MuonsFromB)
        self.LambdastarMu_FakeKaon = self.__LambdastarMu__(
            self.FakeKaonLambdaStar, self.MuonsFromB, pid_selection='ReversePIDK_')
        self.LambdastarMu_FakeProton = self.__LambdastarMu__(
            self.FakeProtonLambdaStar, self.MuonsFromB, pid_selection='ReversePIDp_')
        self.LambdastarMu_FakeMuon = self.__LambdastarMu__(
            self.LambdaStar, self.FakeMuonsFromB, pid_selection='ReversePIDmu_')

        self.LambdastarE = self.__LambdastarE__(
            self.LambdaStar, self.ElectronsFromB)
        self.LambdastarE_FakeKaon = self.__LambdastarE__(
            self.FakeKaonLambdaStar, self.ElectronsFromB, pid_selection='ReversePIDK_')
        self.LambdastarE_FakeProton = self.__LambdastarE__(
            self.FakeProtonLambdaStar, self.ElectronsFromB, pid_selection='ReversePIDp_')
        self.LambdastarE_FakeElectron = self.__LambdastarE__(
            self.LambdaStar, self.FakeElectronsFromB, pid_selection='ReversePIDe_')

        # Muon in final state, b->XXtaumu
        DeclaredDaughters = [self.PhiMu, self.PionsFromTau]
        self.Bs_Mu = self.__Bs_Phi__(daughters=DeclaredDaughters, lepton='mu')

        DeclaredDaughters = [self.PhiMu, self.FakePionsFromTau]
        self.Bs_Mu_ReversePIDpiFromTau = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpiFromTau_", lepton='mu')

        DeclaredDaughters = [self.PhiMu_FakeMuon, self.PionsFromTau]
        self.Bs_Mu_ReversePIDmu = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromb_", lepton='mu')

        DeclaredDaughters = [self.PhiMu_FakeKaon, self.PionsFromTau]
        self.Bs_Mu_ReversePIDK = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", lepton='mu')

        DeclaredDaughters = [self.KstarMu, self.PionsFromTau]
        self.Bd_Mu = self.__B_Kstar__(daughters=DeclaredDaughters, lepton='mu')

        DeclaredDaughters = [self.KstarMu, self.FakePionsFromTau]
        self.Bd_Mu_ReversePIDpiFromTau = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpiFromTau_", lepton='mu')

        DeclaredDaughters = [self.KstarMu_FakeMuon, self.PionsFromTau]
        self.Bd_Mu_ReversePIDmu = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromb_", lepton='mu')

        DeclaredDaughters = [self.KstarMu_FakeKaon, self.PionsFromTau]
        self.Bd_Mu_ReversePIDK = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", lepton='mu')

        DeclaredDaughters = [self.KstarMu_FakePion, self.PionsFromTau]
        self.Bd_Mu_ReversePIDpifromKstar = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpi_", lepton='mu')

        self.Bd_Mu_ReversePIDpi = Selection("Bd_ReversePIDpi_sel_for" + self.name.replace('L', 'mu'), Algorithm=FilterDesktop(Code='ALL'),
                                       RequiredSelections=[self.Bd_Mu_ReversePIDpiFromTau, self.Bd_Mu_ReversePIDpifromKstar])

        DeclaredDaughters = [self.LambdastarMu, self.PionsFromTau]
        self.Lb_Mu = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, lepton='mu')

        DeclaredDaughters = [self.LambdastarMu, self.FakePionsFromTau]
        self.Lb_Mu_ReversePIDpiFromTau = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpiFromTau_", lepton='mu')

        DeclaredDaughters = [self.LambdastarMu_FakeMuon, self.PionsFromTau]
        self.Lb_Mu_ReversePIDmu = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDmuFromb_", lepton='mu')

        DeclaredDaughters = [self.LambdastarMu_FakeKaon, self.PionsFromTau]
        self.Lb_Mu_ReversePIDK = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", lepton='mu')

        DeclaredDaughters = [self.LambdastarMu_FakeProton, self.PionsFromTau]
        self.Lb_Mu_ReversePIDp = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDp_", lepton='mu')

        # Electron in final state, b->XXtaue

        DeclaredDaughters = [self.PhiE, self.PionsFromTau]
        self.Bs_E = self.__Bs_Phi__(daughters=DeclaredDaughters, lepton='e')

        DeclaredDaughters = [self.PhiE, self.FakePionsFromTau]
        self.Bs_E_ReversePIDpiFromTau = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpiFromTau_", lepton='e')

        DeclaredDaughters = [self.PhiE_FakeElectron, self.PionsFromTau]
        self.Bs_E_ReversePIDe = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromb_", lepton='e')

        DeclaredDaughters = [self.PhiE_FakeKaon, self.PionsFromTau]
        self.Bs_E_ReversePIDK = self.__Bs_Phi__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", lepton='e')


        DeclaredDaughters = [self.KstarE, self.PionsFromTau]
        self.Bd_E = self.__B_Kstar__(daughters=DeclaredDaughters, lepton='e')

        DeclaredDaughters = [self.KstarE, self.FakePionsFromTau]
        self.Bd_E_ReversePIDpiFromTau = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpiFromTau_", lepton='e')

        DeclaredDaughters = [self.KstarE_FakeElectron, self.PionsFromTau]
        self.Bd_E_ReversePIDe = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromb_", lepton='e')

        DeclaredDaughters = [self.KstarE_FakeKaon, self.PionsFromTau]
        self.Bd_E_ReversePIDK = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", lepton='e')

        DeclaredDaughters = [self.KstarE_FakePion, self.PionsFromTau]
        self.Bd_E_ReversePIDpifromKstar = self.__B_Kstar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpi_", lepton='e')

        self.Bd_E_ReversePIDpi = Selection("Bd_ReversePIDpi_sel_for" + self.name.replace('L', 'e'), Algorithm=FilterDesktop(Code='ALL'),
                                      RequiredSelections=[self.Bd_E_ReversePIDpiFromTau, self.Bd_E_ReversePIDpifromKstar])


        DeclaredDaughters = [self.LambdastarE, self.PionsFromTau]
        self.Lb_E = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, lepton='e')

        DeclaredDaughters = [self.LambdastarE, self.FakePionsFromTau]
        self.Lb_E_ReversePIDpiFromTau = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDpiFromTau_", lepton='e')

        DeclaredDaughters = [self.LambdastarE_FakeElectron, self.PionsFromTau]
        self.Lb_E_ReversePIDe = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDeFromb_", lepton='e')

        DeclaredDaughters = [self.LambdastarE_FakeKaon, self.PionsFromTau]
        self.Lb_E_ReversePIDK = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDK_", lepton='e')

        DeclaredDaughters = [self.LambdastarE_FakeProton, self.PionsFromTau]
        self.Lb_E_ReversePIDp = self.__Lambdab_Lambdastar__(
            daughters=DeclaredDaughters, pid_selection="ReversePIDp_", lepton='e')

        self.FilterSPD = {'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                          'Preambulo': ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]}


        # muon lines
        self.Bs2phipimu_line = StrippingLine(
            self.name + "_Bs2Phipimu_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_Mu], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phipimu_line)

        self.Bs2phipimu_ReversePIDmu_line = StrippingLine(
            self.name + "_Bs2Phipimu_ReversePIDMuLine", prescale=0.08,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_Mu_ReversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phipimu_ReversePIDmu_line)

        self.Bs2phipimu_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2Phipimu_ReversePIDKLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_Mu_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phipimu_ReversePIDK_line)

        self.Bs2phipimu_ReversePIDpi_line = StrippingLine(
            self.name + "_Bs2Phipimu_ReversePIDpiLine", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_Mu_ReversePIDpiFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phipimu_ReversePIDpi_line)

        self.Bd2Kstpimu_line = StrippingLine(
            self.name + "_B2Kstarpimu_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_Mu], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bd2Kstpimu_line)

        self.Bd2Kstpimu_ReversePIDmu_line = StrippingLine(
            self.name + "_B2Kstarpimu_ReversePIDMuLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_Mu_ReversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstpimu_ReversePIDmu_line)

        self.Bd2Kstpimu_ReversePIDK_line = StrippingLine(
            self.name + "_B2Kstarpimu_ReversePIDKLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_Mu_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstpimu_ReversePIDK_line)

        self.Bd2Kstpimu_ReversePIDpi_line = StrippingLine(
            self.name + "_B2Kstarpimu_ReversePIDpiLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_Mu_ReversePIDpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstpimu_ReversePIDpi_line)

        self.Lb2Lstpimu_line = StrippingLine(
            self.name + "_Lb2Lstarpimu_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_Mu], MDSTFlag=False, MaxCandidates=1000)

        self.registerLine(self.Lb2Lstpimu_line)

        self.Lb2Lstpimu_ReversePIDmu_line = StrippingLine(
            self.name + "_Lb2Lstarpimu_ReversePIDMuLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_Mu_ReversePIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstpimu_ReversePIDmu_line)

        self.Lb2Lstpimu_ReversePIDK_line = StrippingLine(
            self.name + "_Lb2Lstarpimu_ReversePIDKLine", prescale=0.4,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_Mu_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstpimu_ReversePIDK_line)

        self.Lb2Lstpimu_ReversePIDp_line = StrippingLine(
            self.name + "_Lb2Lstarpimu_ReversePIDpLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_Mu_ReversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstpimu_ReversePIDp_line)

        self.Lb2Lstpimu_ReversePIDpi_line = StrippingLine(
            self.name + "_Lb2Lstarpimu_ReversePIDpiLine", prescale=0.3,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_Mu_ReversePIDpiFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstpimu_ReversePIDpi_line)

        
        # -----------------------------------------------------------------------------------------
        # electron lines

        self.Bs2phipie_line = StrippingLine(
            self.name + "_Bs2Phipie_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_E], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phipie_line)

        self.Bs2phipie_ReversePIDe_line = StrippingLine(
            self.name + "_Bs2Phipie_ReversePIDeLine", prescale=0.08,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_E_ReversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phipie_ReversePIDe_line)

        self.Bs2phipie_ReversePIDK_line = StrippingLine(
            self.name + "_Bs2Phipie_ReversePIDKLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_E_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phipie_ReversePIDK_line)

        self.Bs2phipie_ReversePIDpi_line = StrippingLine(
            self.name + "_Bs2Phipie_ReversePIDpiLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_E_ReversePIDpiFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2phipie_ReversePIDpi_line)

        self.Bd2Kstpie_line = StrippingLine(
            self.name + "_B2Kstarpie_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_E], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bd2Kstpie_line)

        self.Bd2Kstpie_ReversePIDe_line = StrippingLine(
            self.name + "_B2Kstarpie_ReversePIDeLine", prescale=0.08,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_E_ReversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstpie_ReversePIDe_line)

        self.Bd2Kstpie_ReversePIDK_line = StrippingLine(
            self.name + "_B2Kstarpie_ReversePIDKLine", prescale=0.5,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_E_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstpie_ReversePIDK_line)

        self.Bd2Kstpie_ReversePIDpi_line = StrippingLine(
            self.name + "_B2Kstarpie_ReversePIDpiLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bd_E_ReversePIDpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstpie_ReversePIDpi_line)

        self.Lb2Lstpie_line = StrippingLine(
            self.name + "_Lb2Lstarpie_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_E], MDSTFlag=False, MaxCandidates=1000)

        self.registerLine(self.Lb2Lstpie_line)

        self.Lb2Lstpie_ReversePIDe_line = StrippingLine(
            self.name + "_Lb2Lstarpie_ReversePIDeLine", prescale=0.08,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_E_ReversePIDe], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstpie_ReversePIDe_line)

        self.Lb2Lstpie_ReversePIDK_line = StrippingLine(
            self.name + "_Lb2Lstarpie_ReversePIDKLine", prescale=0.5,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_E_ReversePIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstpie_ReversePIDK_line)

        self.Lb2Lstpie_ReversePIDp_line = StrippingLine(
            self.name + "_Lb2Lstarpie_ReversePIDpLine", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_E_ReversePIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstpie_ReversePIDp_line)

        self.Lb2Lstpie_ReversePIDpi_line = StrippingLine(
            self.name + "_Lb2Lstarpie_ReversePIDpiLine", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Lb_E_ReversePIDpiFromTau], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2Lstpie_ReversePIDpi_line)

    def __Muons__(self, conf, Kstar=False):
        from StandardParticles import StdAllLooseMuons, StdAllNoPIDsMuons
        muons = StdAllNoPIDsMuons if conf['UseNoPIDsMuons'] else StdAllLooseMuons
        kstar = ''

        muon_cut = self.MuonFromBCutBase if conf['UseNoPIDsMuons'] else self.MuonFromBCut
        if Kstar:
            muon_cut = self.MuonFromBCutBase_Kstar if conf['UseNoPIDsMuons'] else self.MuonFromBCut_Kstar
            kstar = '_B2Kstar'
        _filter = FilterDesktop(Code=muon_cut)
        _sel = Selection("Selection_" + self.name + "_StdLooseMuons"+kstar, RequiredSelections=[muons],
                         Algorithm=_filter)
        return _sel

    def __FakeMuons__(self, Kstar=False):
        from StandardParticles import StdAllNoPIDsMuons
        code = self.MuonFromBCutReversePID if not Kstar else self.MuonFromBCutReversePID_Kstar
        _filter = FilterDesktop(Code=code)
        kstar = '' if not Kstar else '_B2Kstar'
        _sel = Selection("Selection_" + self._name + "StdAllReversePIDsMuons"+kstar, Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsMuons])
        return _sel

    def __Electrons__(self, conf):
        from StandardParticles import StdAllLooseElectrons, StdAllNoPIDsElectrons
        electrons = StdAllNoPIDsElectrons if conf['UseNoPIDsElectrons'] else StdAllLooseElectrons

        electron_cut = self.ElectronFromBCutBase if conf['UseNoPIDsElectrons'] else self.ElectronFromBCut

        _filter = FilterDesktop(Code=electron_cut)
        _sel = Selection("Selection_" + self.name + "_StdLooseElectrons", RequiredSelections=[electrons],
                         Algorithm=_filter)
        return _sel

    def __FakeElectrons__(self):
        from StandardParticles import StdAllNoPIDsElectrons

        _filter = FilterDesktop(Code=self.ElectronFromBCutReversePID)
        _sel = Selection("Selection_" + self._name + "StdAllReversePIDsElectrons",  Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsElectrons])
        return _sel

    def __Protons__(self, conf):
        from StandardParticles import StdLooseProtons, StdNoPIDsProtons
        protons = StdNoPIDsProtons if conf['UseNoPIDsHadrons'] else StdLooseProtons
        proton_cuts = self.ProtonCutBase if conf['UseNoPIDsHadrons'] else self.ProtonCut
        _filter = FilterDesktop(Code=proton_cuts)
        _sel = Selection("Selection_" + self.name + "_StdLooseProtons",
                         RequiredSelections=[protons], Algorithm=_filter)
        return _sel

    def __FakeProtons__(self):
        from StandardParticles import StdNoPIDsProtons
        _filter = FilterDesktop(Code=self.ProtonCutReversePID)
        _sel = Selection("Selection_" + self._name + "_StdAllReversePIDsProtons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsProtons])
        return _sel

    def __Kaons__(self, conf, sel_name="_"):
        from StandardParticles import StdLooseKaons, StdNoPIDsKaons
        kaons = StdNoPIDsKaons if conf['UseNoPIDsHadrons'] else StdLooseKaons
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.KaonCutBase)
        elif "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCut_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCut)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLooseKaons", RequiredSelections=[kaons],
                         Algorithm=_filter)
        return _sel

    def __FakeKaons__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsKaons
        if "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCutReversePID_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCutReversePID)
        _sel = Selection("Selection_" + self._name + sel_name + "StdAllReversePIDsKaons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsKaons])
        return _sel

    def __Pions__(self, conf, sel_name="_"):
        from StandardParticles import StdLoosePions, StdNoPIDsPions
        pions = StdNoPIDsPions if conf['UseNoPIDsHadrons'] else StdLoosePions
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.PionCutBase)
        else:
            _filter = FilterDesktop(Code=self.PionCut_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLoosePions", RequiredSelections=[pions],
                         Algorithm=_filter)
        return _sel

    def __FakePions__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsPions
        _filter = FilterDesktop(Code=self.PionCutReversePID_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdAllReversePIDsPions", RequiredSelections=[StdNoPIDsPions],
                         Algorithm=_filter)
        return _sel

    def __PionsFromTau__(self, conf):
        
        from StandardParticles import StdAllLoosePions, StdAllNoPIDsPions
        pions = StdAllNoPIDsPions if conf['UseNoPIDsTauDau'] else StdAllLoosePions
        pion_cut = self.PionFromTauCutBase if conf['UseNoPIDsTauDau'] else self.PionFromTauCut
        _filter = FilterDesktop(Code=pion_cut) 
        _sel = Selection("Selection_" + self.name + "_StdLoosePions", RequiredSelections=[pions],
                         Algorithm=_filter)
        return _sel

    def __FakePionsFromTau__(self):
        from StandardParticles import StdAllNoPIDsPions
        _filter = FilterDesktop(Code=self.PionFromTauCutReversePID)
        _sel = Selection("Selection_" + self._name + "StdAllReversePIDsPions", Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsPions])
        return _sel

    def __Phi__(self, Kaons, fakekaon=None, conf=None):
        _phi2kk = CombineParticles()
        _phi2kk.DecayDescriptors = [
            "phi(1020) -> K+ K-", "phi(1020) -> K+ K+", "phi(1020) -> K- K-"]
        _phi2kk.MotherCut = self.PhiCut
        if fakekaon is None:
            _phi2kk.CombinationCut = self.PhiCombCut
            _sel = Selection("Phi_selection_for" + self.name,
                             Algorithm=_phi2kk, RequiredSelections=[Kaons])
        else:
            _phi2kk.CombinationCut = self.PhiCombCut + \
                " & (AHASCHILD((PIDK < %(KaonPID)s)))" % conf
            _sel = Selection("Phi_ReversePIDK_selection_for" + self.name, Algorithm=_phi2kk,
                             RequiredSelections=[Kaons, fakekaon])
        return _sel

    def __Kstar__(self, Kaons, Pions, pid_selection="_"):
        _kstar2kpi = CombineParticles()
        _kstar2kpi.DecayDescriptors = [
            "[K*(892)0 -> K+ pi-]cc", "K*(892)0 -> K+ pi+", "K*(892)0 -> K- pi-"]
        _kstar2kpi.CombinationCut = self.KstarCombCut
        _kstar2kpi.MotherCut = self.KstarCut
        _sel = Selection("Kstar" + pid_selection + "selection_for" + self.name, Algorithm=_kstar2kpi,
                         RequiredSelections=[Kaons, Pions])
        return _sel

    def __Lambdastar__(self, Protons, Kaons, pid_selection="_"):
        _lstar2pk = CombineParticles()
        _lstar2pk.DecayDescriptors = [
            "[Lambda(1520)0 -> p+ K-]cc", "Lambda(1520)0 -> p+ K+", "Lambda(1520)0 -> p~- K-"]
        _lstar2pk.CombinationCut = self.LambdastarCombCut
        _lstar2pk.MotherCut = self.LambdastarCut
        _sel = Selection("Lambdastar" + pid_selection + "selection_for" + self.name, Algorithm=_lstar2pk,
                         RequiredSelections=[Protons, Kaons])
        return _sel

    def __PhiMu__(self, Phi, Muon, conf=None, pid_selection='_'):
        _b2phimu = CombineParticles()
        _b2phimu.DecayDescriptors = ["[B0 -> phi(1020) mu+]cc"]
        _b2phimu.MotherCut = self.PhiMuCut
        _b2phimu.CombinationCut = self.PhiMuCombCut
        _sel = Selection("B2PhiMu" + pid_selection + self.name,
                         Algorithm=_b2phimu, RequiredSelections=[Phi, Muon])
        return _sel

    def __KstarMu__(self, Kstar, Muon, conf=None, pid_selection='_'):
        _b2kstmu = CombineParticles()
        _b2kstmu.DecayDescriptors = [
            "[B0 -> K*(892)0 mu+]cc", "[B0 -> K*(892)0 mu-]cc"]
        _b2kstmu.MotherCut = self.KstarMuCut
        _b2kstmu.CombinationCut = self.KstarMuCombCut
        _sel = Selection("B2KstarMu" + pid_selection + self.name,
                         Algorithm=_b2kstmu, RequiredSelections=[Kstar, Muon])
        return _sel

    def __LambdastarMu__(self, Lambdastar, Muon, conf=None, pid_selection='_'):
        _b2lstmu = CombineParticles()
        _b2lstmu.DecayDescriptors = [
            "[B0 -> Lambda(1520)0 mu+]cc", "[B0 -> Lambda(1520)0 mu-]cc"]
        _b2lstmu.MotherCut = self.LambdastarMuCut
        _b2lstmu.CombinationCut = self.LambdastarMuCombCut
        _sel = Selection("B2LambdastarMu" + pid_selection + self.name,
                         Algorithm=_b2lstmu, RequiredSelections=[Lambdastar, Muon])
        return _sel

    def __PhiE__(self, Phi, Electron, conf=None, pid_selection='_'):
        _b2phie = CombineParticles()
        _b2phie.DecayDescriptors = ["[B0 -> phi(1020) e+]cc"]
        _b2phie.MotherCut = self.PhiECut
        _b2phie.CombinationCut = self.PhiECombCut
        _sel = Selection("B2PhiE" + pid_selection + self.name,
                         Algorithm=_b2phie, RequiredSelections=[Phi, Electron])
        return _sel

    def __KstarE__(self, Kstar, Electron, conf=None, pid_selection='_'):
        _b2kste = CombineParticles()
        _b2kste.DecayDescriptors = [
            "[B0 -> K*(892)0 e+]cc", "[B0 -> K*(892)0 e-]cc"]
        _b2kste.MotherCut = self.KstarECut
        _b2kste.CombinationCut = self.KstarECombCut
        _sel = Selection("B2KstarE" + pid_selection + self.name,
                         Algorithm=_b2kste, RequiredSelections=[Kstar, Electron])
        return _sel

    def __LambdastarE__(self, Lambdastar, Electron, conf=None, pid_selection='_'):
        _b2lste = CombineParticles()
        _b2lste.DecayDescriptors = [
            "[B0 -> Lambda(1520)0 e+]cc", "[B0 -> Lambda(1520)0 e-]cc"]
        _b2lste.MotherCut = self.LambdastarECut
        _b2lste.CombinationCut = self.LambdastarECombCut
        _sel = Selection("B2LambdastarE" + pid_selection + self.name,
                         Algorithm=_b2lste, RequiredSelections=[Lambdastar, Electron])
        return _sel

    def __Bs_Phi__(self, daughters, pid_selection="_", lepton='L'):
        _b2phipitau = CombineParticles(DecayDescriptors=["[B_s0 -> B0 pi+]cc", "[B_s0 -> B0 pi-]cc"],
                                       MotherCut=self.BsCut, CombinationCut=self.BsCombCut)
        sel = Selection("Phi" + pid_selection + "for" + self.name.replace('L',
                        lepton), Algorithm=_b2phipitau, RequiredSelections=daughters)
        return sel

    def __B_Kstar__(self, daughters, pid_selection="_", lepton='L'):
        _b2kstpitau = CombineParticles(DecayDescriptors=["[B_s0 -> B0 pi+]cc", "[B_s0 -> B0 pi-]cc"],
                                       MotherCut=self.BdCut, CombinationCut=self.BdCombCut)
        sel = Selection("Kstar" + pid_selection + "for" + self.name.replace('L',
                        lepton), Algorithm=_b2kstpitau, RequiredSelections=daughters)
        return sel

    def __Lambdab_Lambdastar__(self, daughters, pid_selection="_", lepton='L'):
        _b2lstpitau = CombineParticles(DecayDescriptors=["[Lambda_b0 -> B0 pi+]cc", "[Lambda_b0 -> B0 pi-]cc"],
                                       MotherCut=self.BsCut, CombinationCut=self.BsCombCut)
        sel = Selection("Lambdastar" + pid_selection + "for" + self.name.replace(
            'L', lepton), Algorithm=_b2lstpitau, RequiredSelections=daughters)
        return sel
