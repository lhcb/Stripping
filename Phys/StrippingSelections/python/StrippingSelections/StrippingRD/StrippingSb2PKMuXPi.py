###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


__author__ = "Dan Thompson; based on combination of Bu2LLK lines and Bs2st2KKMuXLine"
__date__ = "14/06/2021"
__version__ = "$Revision: 1$"

__all__= ('Sb2PKMuXPiConf','default_config')

"""
Stripping Line for studies involving missing mass with beauty baryons.
Pion from primary vertex allows reconstruction of Lb momentum.

Selections for:
    Sb2PKMuXPiLine:    Sigmab+- --> (Lb --> (Lambda(*) --> p+ K-) mu X) pi+- [4 Permutations (Sb+-, mu+-) + CC]
    Sb2PKMuXPiSSLine:    Sigmab+- --> (Lb --> (Lambda(*) --> p+ K+) mu X) pi+- [4 Permutations + CC]
    Sb2PKJpsiPiLine:  Sigmab+- --> (Lb --> (Lambda(*) --> p+ K-) (J/psi --> mu+ mu-))) pi+- [CC + SS Hadrons]

Updated for S34r0p2 Restripping: Altered RelInfoTools so they are functional (not required however as line is DST )
"""

daughter_locations = {
   #OS Hadrons
   "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l-) pi+]CC": "{0}_p", #Sb+, mu-
   "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l+) pi+]CC": "{0}_p", #Sb+, mu+
   "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l-) pi-]CC": "{0}_p", #Sb-, mu-
   "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l+) pi-]CC": "{0}_p", #Sb-, mu+

   "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l-) pi+]CC": "{0}_K",
   "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l+) pi+]CC": "{0}_K",
   "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l-) pi-]CC": "{0}_K",
   "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l+) pi-]CC": "{0}_K",

   "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l-) pi+]CC": "{0}_mu",
   "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l+) pi+]CC": "{0}_mu",
   "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l-) pi-]CC": "{0}_mu",
   "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l+) pi-]CC": "{0}_mu",

   "[Beauty -> (Beauty -> (X0 -> X+ Xs) l-) ^pi+]CC": "{0}_Pi",
   "[Beauty -> (Beauty -> (X0 -> X+ Xs) l+) ^pi+]CC": "{0}_Pi",
   "[Beauty -> (Beauty -> (X0 -> X+ Xs) l-) ^pi-]CC": "{0}_Pi",
   "[Beauty -> (Beauty -> (X0 -> X+ Xs) l+) ^pi-]CC": "{0}_Pi",

   "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l-) pi+]CC": "{0}_LStar",
   "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l+) pi+]CC": "{0}_LStar",
   "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l-) pi-]CC": "{0}_LStar",
   "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l+) pi-]CC": "{0}_LStar",

   #SS Hadrons
   "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l-) pi+]CC": "{0}_p",
   "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l+) pi+]CC": "{0}_p",
   "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l-) pi-]CC": "{0}_p",
   "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l+) pi-]CC": "{0}_p",

   "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l-) pi+]CC": "{0}_K",
   "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l+) pi+]CC": "{0}_K",
   "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l-) pi-]CC": "{0}_K",
   "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l+) pi-]CC": "{0}_K",

   "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l-) pi+]CC": "{0}_mu",
   "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l+) pi+]CC": "{0}_mu",
   "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l-) pi-]CC": "{0}_mu",
   "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l+) pi-]CC": "{0}_mu",

   "[Beauty -> (Beauty -> (X0 -> X- Xs) l-) ^pi+]CC": "{0}_Pi",
   "[Beauty -> (Beauty -> (X0 -> X- Xs) l+) ^pi+]CC": "{0}_Pi",
   "[Beauty -> (Beauty -> (X0 -> X- Xs) l-) ^pi-]CC": "{0}_Pi",
   "[Beauty -> (Beauty -> (X0 -> X- Xs) l+) ^pi-]CC": "{0}_Pi",

   "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l-) pi+]CC": "{0}_LStar",
   "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l+) pi+]CC": "{0}_LStar",
   "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l-) pi-]CC": "{0}_LStar",
   "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l+) pi-]CC": "{0}_LStar"
}

default_config = {
    'NAME' : "Sb2PKMuXPi",
    'BUILDERTYPE' : "Sb2PKMuXPiConf",
    'WGs' : ['RD'],
    'STREAMS' : ['Semileptonic'],
    'CONFIG' : {
        #prescales
        "Sb2PKMuXPiLinePrescale" : 1,
        "Sb2PKJpsiPiLinePrescale" : 1,
        'Sb2PKMuXPiSSLinePrescale' : 1,

        #track
        'Trk_GhostProb' : 0.2,

        #PIDNN for pK
        'ProbNNCut'     : 0.25,

        #Pion from Sb
        'PiPT'          : 500., #MeV
        'PiPTLoose'     : 250., #MeV
        'PiMinIPChi2'   : 9.,
        'PiPIDK'        : 16.,
        'PiPIDp'        : 16., 
        'PiPIDmu'       : 0., 

        #muon
        'MuP'           : 3000. ,# MeV
        'MuPT'          : 1000. , #MeV
        'MuMinIPChi2'   : 9.,
        'MuPIDmu'       : 0.,

        #kaon
        'KaonIPCHI2'    : 9. , 
        'KaonPT'        : 400. , #MeV
        'KaonPTLoose'   : 250. , #MeV

        #proton
        'ProtonP' : 2000. , #MeV

        #pK
        'DiHadronVtxCHI2'      : 20,         
        'DiHadronADOCACHI2'    : 30, #mm

        #Lb limit for Lambda(1520)
        'UpperLbMass' : 5620. , #MeV

        # J/psi from Lb -> Lambda(*) J/psi
        'JpsiMassWindow': 80. ,# MeV

        # Lb --> p K mu X Vertex
        'pKMuVChi2Dof'  : 4.    ,
        'pKMuFdChi2'    : 100.    , 
        'pKMuMassMin'   : 2300. ,# MeV 
        'pKMuMassMax'   : 6000. ,# MeV

        # Lb --> p K jpsi Vertex
        'pKJpsiVChi2Dof'  : 3.    ,
        'pKJpsiFdChi2'    : 25.    , 
        'pKJpsiMassMin'   : 5400. ,# MeV 
        'pKJpsiMassMax'   : 5850. ,# MeV

        # Sb --> Lb pi Vertex
        'SbPT'  : 50.0    , #MeV
        'DMpKMuPi'    : 359.57    , #difference between inv mass and inv mass of daughter 1 for Sb -> Lb pi
        # 139.57 + 220 or 600
        'DMpKJpsiPi'    : 739.57    , #same but for jpsi
        'DZBPV' : 1.0, #mm #best primary vertex displacment

        #track isolation tools
        'RelatedInfoTools' : [{'Type' : 'RelInfoBs2MuMuTrackIsolations',
                               'IgnoreUnmatchedDescriptors': True,
                               'Variables' : [
                                   'BSMUMUTRACKPLUSISO',
                                   'BSMUMUTRACKPLUSISOTWO',
                                   'ISOTWOBODYMASSISOPLUS',
                                   'ISOTWOBODYCHI2ISOPLUS',
                                   'ISOTWOBODYISO5PLUS',
                                   'BSMUMUTRACKID'
                                   ],
                               #'Location' : 'TrackIsoBs2MMInfo',
                               'DaughterLocations' : {key: val.format('TrackIsoBs2MMInfo') for key, val in daughter_locations.items()},
                              
                               'tracktype'  : 3,
                               'angle'      : 0.27,
                               'fc'         : 0.60,
                               'doca_iso'   : 0.13,
                               'ips'        : 3.0,
                               'svdis'      : -0.15,
                               'svdis_h'    : 30.,
                               'pvdis'      : 0.5,
                               'pvdis_h'    : 40.,
                               'makeTrackCuts' : False,
                               'IsoTwoBody' : True
                              },
                              {'Type'              : 'RelInfoTrackIsolationBDT',
                               'IgnoreUnmatchedDescriptors': True,
                               # Use the BDT with 9 input variables
                               # This requires that the "Variables" value is set to 2
                               'Variables' : 2,
                               'WeightsFile'  :  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
                               #'Location' : 'TrackIsoBDTInfo',
                               'DaughterLocations' : {key: val.format('TrackIsoBDTInfo') for key, val in daughter_locations.items()}
                               }
                              ]
        }

}

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

class Sb2PKMuXPiConf(LineBuilder):
    '''
    Builder for missing mass measurments with baryons
    '''

    # __configuration_keys__ = (
    #     'Sb2PKMuXPiLinePrescale',
    #     'Sb2PKJpsiPiLinePrescale',
    #     'Sb2PKMuXPiSSLinePrescale',
    #     'Trk_GhostProb',
    #     'ProbNNCut',
    #     'KaonIPCHI2',
    #     'KaonPT',
    #     'KaonPTLoose',
    #     'ProtonP',
    #     'DiHadronVtxCHI2',    
    #     'DiHadronADOCACHI2',
    #     'UpperLbMass',
    #     'PiPT',
    #     'PiPTLoose',
    #     'PiMinIPChi2',
    #     'PiPIDK',
    #     'PiPIDp',
    #     'PiPIDmu',
    #     'MuP',
    #     'MuPT',
    #     'MuMinIPChi2',
    #     'MuPIDmu',
    #     'JpsiMassWindow',
    #     'pKMuVChi2Dof',
    #     'pKMuFdChi2',
    #     'pKMuMassMin',
    #     'pKMuMassMax',
    #     'pKJpsiVChi2Dof',
    #     'pKJpsiFdChi2',
    #     'pKJpsiMassMin',
    #     'pKJpsiMassMax',
    #     'SbPT',
    #     'DMpKMuPi','DMpKJpsiPi',
    #     'DZBPV',
    #     'RelatedInfoTools'
    #     )

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self._name=name

        from StandardParticles import StdLooseMuons as Muons 
        from StandardParticles import StdAllLooseKaons as AllKaons
        from StandardParticles import StdAllLooseProtons as AllProtons
        from StandardParticles import StdAllLoosePions as AllPions
        from StandardParticles import StdMassConstrainedJpsi2MuMu as Jpsi

        self.SelpKs = self._makepK( name   = "pKsFor" + self._name,
                                        protons = AllProtons,
                                        kaons  = AllKaons,
                                        params = config )

        self.SelpKsSS = self._makepKSS( name   = "pKsSSFor" + self._name,
                                        protons = AllProtons,
                                        kaons  = AllKaons,
                                        params = config )


        self.PionCuts = "(TRCHI2DOF < 3) & (TRGHOSTPROB < 0.5) & (PT > %(PiPT)s *MeV) &"\
                         "(MIPCHI2DV(PRIMARY) < %(PiMinIPChi2)s) & "\
                         "(PIDpi-PIDK > %(PiPIDK)s) & (PIDpi-PIDp > %(PiPIDp)s) & (PIDpi-PIDmu > %(PiPIDmu)s)" %config


        self.MuonCuts  = "(TRCHI2DOF < 3) & (TRGHOSTPROB < 0.5) & "\
                         "(P > %(MuP)s *MeV) & (PT > %(MuPT)s *MeV) & "\
                         "(MIPCHI2DV(PRIMARY) > %(MuMinIPChi2)s) & "\
                         "(PIDmu-PIDK > 0) & (PIDmu-PIDp > 0) & (PIDmu-PIDpi > %(MuPIDmu)s)" %config

        self.JpsiCuts  = "(PFUNA(ADAMASS('J/psi(1S)')) < %(JpsiMassWindow)s * MeV)" %config

        self.SelPion = Selection("Pion_for"+name,
                                Algorithm = FilterDesktop(Code=self.PionCuts),
                                RequiredSelections = [AllPions])

        self.SelMuon  = Selection("Mu_for" + name,
                                  Algorithm = FilterDesktop(Code=self.MuonCuts),
                                  RequiredSelections = [Muons])

        self.SelJpsi  = Selection("Jpsi_for" + name,
                                  Algorithm = FilterDesktop(Code=self.JpsiCuts),
                                  RequiredSelections = [Jpsi])

        # Lb -> Lambda(1520) mu
        self.Lb2pKMu = CombineParticles(DecayDescriptors=["[Lambda_b0 -> Lambda(1520)0 mu+]cc","[Lambda_b0 -> Lambda(1520)0 mu-]cc"])
        self.Lb2pKMu.CombinationCut = "(AM > %(pKMuMassMin)s*MeV) & (AM < %(pKMuMassMax)s*MeV)" % config
        self.Lb2pKMu.MotherCut = "(VFASPF(VCHI2/VDOF) < %(pKMuVChi2Dof)s) & (BPVDIRA > 0.99) & "\
                                     "(BPVVDCHI2 > %(pKMuFdChi2)s)" % config
        self.Lb2pKMu.ReFitPVs = True

        self.pKMerged=MergedSelection("pKSSCombfor" + name, RequiredSelections = [self.SelpKs, self.SelpKsSS])
        #merged pK/pKSS for jpsi line


        self.SelLb2pKMu = Selection("Lb2pKMu_for" + name,
                                   Algorithm          = self.Lb2pKMu,
                                   RequiredSelections = [self.SelpKs, self.SelMuon])

        self.SelLb2pKMuSS = Selection("Lb2pKMuSS_for" + name,
                                   Algorithm          = self.Lb2pKMu,
                                   RequiredSelections = [self.SelpKsSS, self.SelMuon])
        

        # Lb -> Lambda(1520) J/psi
        self.Lb2pKJpsi = CombineParticles(DecayDescriptors = ["[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"])
        self.Lb2pKJpsi.CombinationCut = "(AM > %(pKJpsiMassMin)s*MeV) & (AM < %(pKJpsiMassMax)s*MeV)" % config
        self.Lb2pKJpsi.MotherCut      = "(VFASPF(VCHI2/VDOF) < %(pKJpsiVChi2Dof)s) & (BPVDIRA > 0.99) & "\
                                       "(BPVVDCHI2 > %(pKJpsiFdChi2)s)" % config
        self.Lb2pKJpsi.ReFitPVs       = True

        self.SelLb2pKJpsi = Selection("Lb2pKJpsi_for" + name,
                                     Algorithm          = self.Lb2pKJpsi,
                                     RequiredSelections = [self.pKMerged, self.SelJpsi])

        self.Sb2pKMuXPi = CombineParticles( DecayDescriptors = ["[Sigma_b+ -> Lambda_b0 pi+]cc","[Sigma_b- -> Lambda_b0 pi-]cc"])
        self.Sb2pKMuXPi.CombinationCut = "(AM-AM1 < %(DMpKMuPi)s*MeV) & "\
                                      "(abs(ACHILD(BPV(VZ),1)-ACHILD(BPV(VZ),2))<%(DZBPV)s*mm)" % config
        self.Sb2pKMuXPi.MotherCut      = "(PT > %(SbPT)s*MeV)" % config
        self.Sb2pKMuXPi.ReFitPVs       = False

        self.SelSb2pKMuXPi = Selection("Sb2pKMuXPi_for" + name,
                                    Algorithm          = self.Sb2pKMuXPi,
                                    RequiredSelections = [self.SelLb2pKMu, self.SelPion])

        self.SelSb2pKMuXPiSS = Selection("Sb2pKMuXPiSS_for" + name,
                                    Algorithm          = self.Sb2pKMuXPi,
                                    RequiredSelections = [self.SelLb2pKMuSS, self.SelPion])

        self.Sb2pKJpsiPi = CombineParticles( DecayDescriptors = ["[Sigma_b+ -> Lambda_b0 pi+]cc","[Sigma_b- -> Lambda_b0 pi-]cc"])
        self.Sb2pKJpsiPi.CombinationCut = "(AM-AM1 < %(DMpKJpsiPi)s*MeV) & "\
                                      "(abs(ACHILD(BPV(VZ),1)-ACHILD(BPV(VZ),2))<%(DZBPV)s*mm)" % config
        self.Sb2pKJpsiPi.MotherCut      = "(PT > %(SbPT)s*MeV)" % config
        self.Sb2pKJpsiPi.ReFitPVs       = False

        self.SelSb2pKJpsiPi = Selection("Sb2pKJpsiPi_for" + name,
                                    Algorithm          = self.Sb2pKJpsiPi,
                                    RequiredSelections = [self.SelLb2pKJpsi, self.SelPion])

        self.SPDFilter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 450 )" ,
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb", "from LoKiTracks.decorators import *" ]
            }

        self.Sb2PKMuXPiLine = StrippingLine("Sb2PKMuXPiLine",
                                       prescale          = config['Sb2PKMuXPiLinePrescale'],
                                       postscale         = 1,
                                       selection         = self.SelSb2pKMuXPi,
                                       RelatedInfoTools  = config['RelatedInfoTools'],
                                       FILTER            = self.SPDFilter,
                                       RequiredRawEvents = [], 
                                       MDSTFlag          = False
                                       #, MaxCandidates = 400,
                                       )

        self.Sb2PKMuXPiSSLine = StrippingLine("Sb2PKMuXPiSSLine",
                                       prescale          = config['Sb2PKMuXPiSSLinePrescale'],
                                       postscale         = 1,
                                       selection         = self.SelSb2pKMuXPiSS,
                                       RelatedInfoTools  = config['RelatedInfoTools'],
                                       FILTER            = self.SPDFilter,
                                       RequiredRawEvents = [], 
                                       MDSTFlag          = False
                                       #, MaxCandidates = 400,
                                       )

        self.Sb2PKJpsiPiLine = StrippingLine("Sb2PKJpsiPiLine",
                                       prescale          = config['Sb2PKJpsiPiLinePrescale'],
                                       postscale         = 1,
                                       selection         = self.SelSb2pKJpsiPi,
                                       FILTER            = self.SPDFilter, 
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False
                                       #, MaxCandidates = 400
                                       )


        self.registerLine(self.Sb2PKMuXPiLine)
        self.registerLine(self.Sb2PKMuXPiSSLine)
        self.registerLine(self.Sb2PKJpsiPiLine)




    def _makepK( self, name, protons, kaons, params):
        """
        Make a Lambda* -> p K- in entire range. 
        """

        _Decays = "[Lambda(1520)0 -> p+ K-]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(UpperLbMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params


        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_p = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNp > %(ProbNNCut)s)" % params
        _DaughterCut_K = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNk > %(ProbNNCut)s)" % params


        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut_p,
            "K-" : _DaughterCut_K
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons, kaons ] ) 


    #same variables as correct sign comb
    #####################################################
    def _makepKSS( self, name, protons, kaons, params):
        """
        Make a same-sign Lambda* -> p K+ in entire range. 
        """

        _Decays = "[Lambda(1520)0 -> p+ K+]cc"

        _CombinationCut = "(APT > %(KaonPT)s *MeV) & " \
                          "(AM < %(UpperLbMass)s *MeV) & " \
                          "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

        _MotherCut = "(VFASPF(VCHI2) < %(DiHadronVtxCHI2)s)" % params

        _DaughterCut_p = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNp > %(ProbNNCut)s)" % params
        _DaughterCut_K = "(PT > %(KaonPTLoose)s *MeV) & (P > %(ProtonP)s *MeV) & " \
                   "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)) & (PROBNNk > %(ProbNNCut)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DaughtersCuts = {
            "p+"  : _DaughterCut_p,
            "K+" : _DaughterCut_K
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ protons, kaons ] )  
