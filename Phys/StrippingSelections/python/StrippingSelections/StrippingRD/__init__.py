###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

_selections = (
    'StrippingB2DibaryonMuMu',
    'StrippingB2EtaMuMu',
    'StrippingB2K1MuMu',
    'StrippingB2KeeXInclusive',
    'StrippingB2KsLLXInclusive',
    'StrippingB2KstTauTau',
    'StrippingB2LLXBDTSS',
    'StrippingB2LambdapbargammaLines',
    'StrippingB2STauTauInclusive',
    'StrippingB2TauNu',
    'StrippingB2XLTau1pi',
    'StrippingB2XLTauLeptonic',
    'StrippingB2XTauTau',
    'StrippingB2XTauTauLeptonic',
    'StrippingB2XTauTau_LPi',
    'StrippingB2XTauTau_PiPi',
    'StrippingB2ppbargamma',
    'StrippingBc2Ds2MuMuLine',
    'StrippingBeauty2MajoLep',
    'StrippingBeauty2XGamma',
    'StrippingBeauty2XGammaExclusive',
    'StrippingBeauty2XGammaExclTDCPV',
    'StrippingBu2LLK',
    'StrippingDarkBoson',
    'StrippingElectronID',
    'StrippingHb2XGammaConverted',
    'StrippingLb2PKLTauHadronic',
    'StrippingLb2pKTauX',
    'StrippingMultiLepton',
    'StrippingRareBaryonicMuMu',
    'StrippingS2Hyperons',
    'StrippingTau2MuPhi',
    'StrippingB2MuMuMuMuLines')

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
