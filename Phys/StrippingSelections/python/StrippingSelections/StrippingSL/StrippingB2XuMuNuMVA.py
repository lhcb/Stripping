###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = ["Mark Smith"]
__date__ = "16/05/2023"
__version__ = "$Revision: 1.6 $"

"""
B->pi mu nu exclusive reconstruction
"""
# =============================================================================
##
#  B0->pi mu nu exclusive reconstruction
#
############
#
#  Stripping XX, with requirements that the
#  rate <0.05% and timing <1ms/evt.
##

"""
##
#  B->pi mu nu exclusive reconstruction
#
#  Stripping XX, with requirements that the
#  rate <0.05% and timing <1ms/evt.

"""


default_config = {
    "NAME": "B2XuMuNuMVA",
    "WGs": ["Semileptonic"],
    "BUILDERTYPE": "B2XuMuNuMVABuilder",
    "CONFIG": {
        "GEC_nLongTrk": 250.0,  # adimensional
        "TRGHOSTPROB": 0.35,  # adimensional
        # Muons
        "MuonTRCHI2": 4.0,  # adimensional
        "MuonP": 3000.0,  # MeV
        "MuonProbNNmu" : 0.6,
        "MuonMINIPCHI2": 9,  # adminensional
        # Pion channel
        "PionTRCHI2": 4.0,  # adimensional
        "PionP": 3000.0,  # MeV
        "PionPT": 200.0,  # MeV
        "PionMINIPCHI2": 9,  # adminensional
        "PionProbNNpi" : 0.2,
        # B Mother Cuts
        "BVCHI2DOF": 4.0,  # adminensional, ideally for 2-track vertices
        "BDIRA": 0.999,  # adminensional
        "BDOCA" : 0.5, # mm
        # B corrected mass limits
        "BCorrMLow": 2500.0,  # MeV
        "BCorrMHigh": 7000.0,  # MeV
        #
        "PiMuMassLow": 2000.0,  # MeV
        "PiMuMassUpper": 6000.0,  # MeV
        # Prescales
        "PiMuNu_NoPIDmu_prescale": 0.04,
        "PiMuNu_NoPIDhad_prescale": 1.,
        "PiMuNu_SameSign_prescale": 1.,        
        # MVA
        "B2PiMuNuMVAFile": "$TMVAWEIGHTSROOT/data/B2PiMuNu_2023_stripping_BDT.xml",
        "B2PiMuNuMVACut": "0.66",
    },
    "STREAMS": ["Semileptonic"],
}

from Gaudi.Configuration import *
from StrippingUtils.Utils import LineBuilder

import logging
def makeTOSFilter(name,specs):
    from Configurables import TisTosParticleTagger
    tosFilter = TisTosParticleTagger(name+'TOSFilter')
    tosFilter.TisTosSpecs = specs
    tosFilter.ProjectTracksToCalo = False
    tosFilter.CaloClustForCharged = False
    tosFilter.CaloClustForNeutral = False
    tosFilter.TOSFrac = {4:0.0, 5:0.0}
    #tosFilter.PassOnAll = True
    return tosFilter

def tosSelection(sel,specs):
    from PhysSelPython.Wrappers import Selection 
    '''Filters Selection sel to be TOS.'''
    tosFilter = makeTOSFilter(sel.name(),specs)
    return Selection(sel.name()+'TOS', Algorithm=tosFilter,
                     RequiredSelections=[sel])
default_name = "B2XuMuNu"


class B2XuMuNuMVABuilder(LineBuilder):
    """
    Definition of B->pi mu nu stripping module
    """

    __configuration_keys__ = default_config["CONFIG"].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.config = config
        from PhysSelPython.Wrappers import Selection, DataOnDemand

        self.GECs = {
            "Code": "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )"
            % config,
            "Preambulo": ["from LoKiTracks.decorators import *"],
        }

        ## Muons
        self._muonSel = None
        self._muonFilter()

        self._muonSelTOS = None
        self._muonFilterTOS()

        self._muonSelNoPID = None
        self._muonFilterNoPID()

        ## Pions
        self._pionSel = None
        self._pionFilter()

        self._pionSelNoPID = None
        self._pionFilterNoPID()

        ###########################################
        ##
        ## Register the stripping lines
        ##
        ###########################################
        self.B2PiMuNuMVAVars = {
            "Pion_PT": "CHILD(PT, 1)",
            "Muon_PT": "CHILD(PT, 2)",
            "Pion_P": "CHILD(P, 1)",
            "Muon_P": "CHILD(P, 2)",
            "B0_P": "P",
            "log(B0_FDCHI2_OWNPV)": "log(BPVVDCHI2)",
            "log(B0_IPCHI2_OWNPV)": "log(BPVIPCHI2())",
            "B0_PT": "PT",
            "log(B0_ENDVERTEX_CHI2)": "log(VFASPF(VCHI2))",
            "log(1-B0_DIRA_OWNPV)": "log(1 - BPVDIRA)",
            "log(Muon_IPCHI2_OWNPV)": "log(CHILD(MIPCHI2DV(), 2))",
            "log(Pion_IPCHI2_OWNPV)": "log(CHILD(MIPCHI2DV(), 1))",
            "Pion_LOKI_ETA-Muon_LOKI_ETA": "CHILD(ETA, 1) - CHILD(ETA, 2)",
            "Pion_LOKI_PHI-Muon_LOKI_PHI": "CHILD(PHI, 1) - CHILD(PHI, 2)",
        }
        self._B2PiMuNuMVA = self.applyMVA(
            "B2PiMuNuMVAFilter",
            self._B2PiMuNu(),
            self.B2PiMuNuMVAVars,
            self.config["B2PiMuNuMVAFile"],
            self.config["B2PiMuNuMVACut"],
        )
        self._B2PiMuNuSSMVA = self.applyMVA(
            "B2PiMuNuSSMVAFilter",
            self._B2PiMuNuSS(),
            self.B2PiMuNuMVAVars,
            self.config["B2PiMuNuMVAFile"],
            self.config["B2PiMuNuMVACut"],
        )
        self._B2PiMuNuNoPIDmuMVA = self.applyMVA(
            "B2PiMuNuNoPIDmuMVAFilter",
            self._B2PiMuNuNoPIDmu(),
            self.B2PiMuNuMVAVars,
            self.config["B2PiMuNuMVAFile"],
            self.config["B2PiMuNuMVACut"],
        )
        self._B2PiMuNuNoPIDhadMVA = self.applyMVA(
            "B2PiMuNuNoPIDhadMVAFilter",
            self._B2PiMuNuNoPIDhad(),
            self.B2PiMuNuMVAVars,
            self.config["B2PiMuNuMVAFile"],
            self.config["B2PiMuNuMVACut"],
        )
        # pi lines
        self.registerLine(self._Pi_line())
        self.registerLine(self._Pi_NoPIDmu_line())
        self.registerLine(self._Pi_NoPIDhad_line())
        self.registerLine(self._PiSS_line())

    ### Muons
    def _NominalMuSelection(self):
        return (
            "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonP)s *MeV)"
            "& ISMUON"
            "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"
            "& (PROBNNmu > %(MuonProbNNmu)s )"
            "& (MIPCHI2DV(PRIMARY)> %(MuonMINIPCHI2)s )"
        )

    def _NominalMuSelectionNoPID(self):
        return (
            "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonP)s *MeV)"
            "& ~ISMUON"
            "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"
            "& (MIPCHI2DV(PRIMARY)> %(MuonMINIPCHI2)s )"
        )


    ### Pions
    def _NominalPiSelection(self):
        return (
            "(TRCHI2DOF < %(PionTRCHI2)s )&  (P> %(PionP)s *MeV) &  (PT> %(PionPT)s *MeV)"
            "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"
            "& (PROBNNpi > %(PionProbNNpi)s )"
            "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"
        )

    def _NominalPiSelectionNoPID(self):
        return (
            "(TRCHI2DOF < %(PionTRCHI2)s )&  (P> %(PionP)s *MeV) &  (PT> %(PionPT)s *MeV)"
            "& (PROBNNpi < %(PionProbNNpi)s )"
            "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"
            "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"
        )

    #####################################

    ## Nomenclature
    ## SS = Same-sign, the same charge for the muon and the hadron (pi+ mu+, K+, mu+, etc.)
    ## WS = wrong-sign, the same chargef for the decay products of the resonance ( rho -> pi+ pi+, omega -> pi+ pi+ pi0, phi -> K+ K+)

    ##### B -> pi mu nu lines
    def _Pi_line(self):
        from StrippingConf.StrippingLine import StrippingLine

        return StrippingLine(
            self._name + "B2Pi_Line",
            FILTER=self.GECs,
            algos=[self._B2PiMuNuMVA],
        )

    def _PiSS_line(self):
        from StrippingConf.StrippingLine import StrippingLine

        return StrippingLine(
            self._name + "B2Pi_SSLine",
            prescale=self.config["PiMuNu_SameSign_prescale"],
            FILTER=self.GECs,
            algos=[self._B2PiMuNuSSMVA],
        )

    def _Pi_NoPIDmu_line(self):
        from StrippingConf.StrippingLine import StrippingLine

        return StrippingLine(
            self._name + "B2Pi_NoPIDMuLine",
            prescale=self.config["PiMuNu_NoPIDmu_prescale"],
            FILTER=self.GECs,
            algos=[self._B2PiMuNuNoPIDmuMVA],
        )

    def _Pi_NoPIDhad_line(self):
        from StrippingConf.StrippingLine import StrippingLine

        return StrippingLine(
            self._name + "B2Pi_NoPIDPiLine",
            prescale=self.config["PiMuNu_NoPIDhad_prescale"],
            FILTER=self.GECs,
            algos=[self._B2PiMuNuNoPIDhadMVA],
        )

    ###############################
    ##
    ## Filters for particles
    ##
    ##############################

    ### Muons
    def _muonFilter(self):
        if self._muonSel is not None:
            return self._muonSel

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseMuons

        _mu = FilterDesktop(Code=self._NominalMuSelection() % self._config)
        _muSel = Selection(
            "Mu_for" + self._name, Algorithm=_mu, RequiredSelections=[StdLooseMuons]
        )

        self._muonSel = _muSel

        return _muSel

    def _muonFilterTOS( self ):
        if self._muonSelTOS is not None:
            return self._muonSelTOS
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseMuons
        _muTOS = FilterDesktop( Code = self._NominalMuSelection() % self._config )
        _muSelTOS=Selection("MuL0TOS_for"+self._name,
                            Algorithm=_muTOS,
                            RequiredSelections = [StdLooseMuons])
        _muSelTOS = tosSelection(_muSelTOS,{'L0.*Muon.*Decision%TOS':0})

        self._muonSelTOS=_muSelTOS
        
        return _muSelTOS

    ######--######
    def _muonFilterNoPID(self):
        if self._muonSelNoPID is not None:
            return self._muonSelNoPID

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsMuons

        _muNoPID = FilterDesktop(Code=self._NominalMuSelectionNoPID() % self._config)
        _muSelNoPID = Selection(
            "MuNoPID_for" + self._name,
            Algorithm=_muNoPID,
            RequiredSelections=[StdNoPIDsMuons],
        )

        self._muonSelNoPID = _muSelNoPID

        return _muSelNoPID

    ## Pions
    ######--######
    def _pionFilter(self):
        if self._pionSel is not None:
            return self._pionSel

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions

        _pi = FilterDesktop(Code=self._NominalPiSelection() % self._config)
        _piSel = Selection(
            "Pi_for" + self._name, Algorithm=_pi, RequiredSelections=[StdLoosePions]
        )
        self._pionSel = _piSel
        return _piSel

    ######--######
    def _pionFilterNoPID(self):
        if self._pionSelNoPID is not None:
            return self._pionSelNoPID

        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsPions

        _piNoPID = FilterDesktop(Code=self._NominalPiSelectionNoPID() % self._config)
        _piSelNoPID = Selection(
            "PiNoPID_for" + self._name,
            Algorithm=_piNoPID,
            RequiredSelections=[StdNoPIDsPions],
        )
        self._pionSelNoPID = _piSelNoPID
        return _piSelNoPID


    ################################
    ##
    ## B -> pi mu nu definitions
    ##
    ################################

    ### B+ -> pi+ mu- nu
    def _B2PiMuNu(self):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMu = CombineParticles(
            DecayDescriptors=["[B0 -> pi+ mu-]cc"],
            CombinationCut="(AM>%(PiMuMassLow)s*MeV) & (AM<%(PiMuMassUpper)s*MeV) & (AMINDOCA('')<%(BDOCA)s*mm)"
            % self._config,
            MotherCut="(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)"
            "& (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs=True,
        )

        _KMuSel = Selection(
            "PiMu_for" + self._name,
            Algorithm=_KMu,
            RequiredSelections=[self._muonFilterTOS(), self._pionFilter()],
        )
        _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0})
        return _KMuSel


    def _B2PiMuNuNoPIDmu(self):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuNoPIDmu = CombineParticles(
            DecayDescriptors=["[B0 -> pi+ mu-]cc"],
            CombinationCut="(AM>%(PiMuMassLow)s*MeV) & (AM<%(PiMuMassUpper)s*MeV) & (AMINDOCA('')<%(BDOCA)s*mm)"
            % self._config,
            MotherCut="(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)"
            "&  (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs=True,
        )

        _KMuSelNoPIDmu = Selection(
            "PiMuNoPIDmu_for" + self._name,
            Algorithm=_KMuNoPIDmu,
            RequiredSelections=[self._muonFilterNoPID(), self._pionFilter()],
        )

        return _KMuSelNoPIDmu

    def _B2PiMuNuNoPIDhad(self):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuNoPIDhad = CombineParticles(
            DecayDescriptors=["[B0 -> pi+ mu-]cc"],
            CombinationCut="(AM>%(PiMuMassLow)s*MeV) & (AM<%(PiMuMassUpper)s*MeV) & (AMINDOCA('')<%(BDOCA)s*mm)"
            % self._config,
            MotherCut="(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)"
            "&  (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs=True,
        )

        _KMuSelNoPIDhad = Selection(
            "PiMuNoPIDhad_for" + self._name,
            Algorithm=_KMuNoPIDhad,
            RequiredSelections=[self._muonFilterTOS(), self._pionFilterNoPID()],
        )
        _KMuSelNoPIDhad = tosSelection(_KMuSelNoPIDhad,{'Hlt2.*TopoMu2Body.*Decision%TOS':0})
        return _KMuSelNoPIDhad

    def _B2PiMuNuSS(self):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KMuSS = CombineParticles(
            DecayDescriptors=["[B0 -> pi- mu-]cc"],
            CombinationCut="(AM>%(PiMuMassLow)s*MeV) & (AM<%(PiMuMassUpper)s*MeV) & (AMINDOCA('')<%(BDOCA)s*mm)"
            % self._config,
            MotherCut=" (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) "
            " & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs=True,
        )

        _KMuSelSS = Selection(
            "PiMuSS_for" + self._name,
            Algorithm=_KMuSS,
            RequiredSelections=[self._muonFilterTOS(), self._pionFilter()],
        )
        _KMuSelSS = tosSelection(_KMuSelSS,{'Hlt2.*TopoMu2Body.*Decision%TOS':0})
        return _KMuSelSS

    def applyMVA(self, name, SelB, MVAVars, MVAxmlFile, MVACutValue):
        from MVADictHelpers import addTMVAclassifierValue
        from Configurables import FilterDesktop as MVAFilterDesktop
        from PhysSelPython.Wrappers import Selection

        _FilterB = MVAFilterDesktop(
            name + "Filter",
            Code="VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue
        )

        addTMVAclassifierValue(
            Component=_FilterB, XMLFile=MVAxmlFile, Variables=MVAVars, ToolName=name
        )
        mvaSel = Selection(name, Algorithm=_FilterB, RequiredSelections=[SelB])
        return mvaSel
