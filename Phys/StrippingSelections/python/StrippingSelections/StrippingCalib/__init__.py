###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

_selections = ('StrippingDst2D0Pi', 'StrippingDstarD02hhPi0PartRecoForPhotonCalib', 'StrippingElectronRecoEff', 'StrippingK23PiForDownstreamTrackEff', 'StrippingTrackEffDownMuon', 'StrippingTrackEffMuonTT', 'StrippingTrackEffVeloMuon', 'StrippingNoPIDDstarWithD02RSKPi', 'StrippingNoPIDDstarWithD02RSKPi_Brem', "StrippingEta2MuMuGamma", #'StrippingElectronID'
)

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
