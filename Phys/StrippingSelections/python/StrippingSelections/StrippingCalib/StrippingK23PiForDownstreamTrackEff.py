###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping lines to measure K+ -> pi+ pi- pi+, to measure the downstream tracking efficiency, with
one missing pion, using the corrected mass
'''

__author__=['Michel De Cian']
__date__ = '19/12/2018'
__version__= '$Revision: 1.0 $'

default_config = {
    'NAME'        : 'K23PiForDownstreamTrackEff',
    'WGs'         : ['Calib'],
    'BUILDERTYPE' : 'K23PiForDownstreamTrackEffConf',
    'CONFIG'      : {

    ### Pion (also the probe)
    ### Pion (not the probe)
    'PionIPCHI2'                 : 16.0,
    'PionMinP'                   : 1500,
    'PionMinPT'                  : 125,
    'PionProbNNghost'            : 0.25,
    'PionProbNNpi'               : 0.6,
    'PionProbNNp'                : 0.1, # against Lambda decays
    ### Dipion
    'DipionMinPT'                : 300,
    'DipionMinP'                 : 3000,
    'DipionMinMassNarrow'        : 275,
    'DipionMaxMassNarrow'        : 500,
    'DipionMinMassWide'          : 260,
    'DipionMaxMassWide'          : 600,
    'DipionMaxMCorrNarrow'       : 600,
    'DipionMaxMCorrWide'         : 700,
    'DipionMCorrPseudoErrNarrow' : 30,
    'DipionMCorrPseudoErrWide'   : 100,
    'DipionVertexChi2PerDoF'     : 2, # for a two-track vertex, it peaks at 0
    'DipionFlightDistanceChi2'   : 100,
    'DipionMinVertexZ'           : 100.0,
    'DipionMaxVertexZ'           : 2200.0,
    'DipionDira'                 : 0.9995,
    ### Tripion
    'TripionMinMass'             : 450,
    'TripionMaxMass'             : 550,
    'TripionVertexChi2PerDoF'    : 5,
    'TripionFlightDistanceChi2'  : 100,
    'TripionMinVertexZ'          : 0,
    'TripionMaxVertexZ'          : 2200,
    
    ### Prescales
    'DipionSSNarrowDownstreamPrescale'  : 1.0,
    'DipionSSNarrowLongPrescale'        : 0.2,
    'DipionSSWidePrescale'              : 0.05,
    'DipionOSNarrowDownstreamPrescale'  : 0.4,
    'DipionOSNarrowLongPrescale'        : 0.2,
    'DipionOSWidePrescale'              : 0.05,
    'TripionSSPrescale'                 : 1.0,
    'TripionOSPrescale'                 : 1.0


    },
    'STREAMS'     :  [ 'BhadronCompleteEvent' ]
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

class K23PiForDownstreamTrackEffConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        
        self.name = name
        self.config = config

        
        
        self._downPionSel = None
        self._downPionFilter()
        
        self._longPionSel = None
        self._longPionFilter()
        
        self._dipionSelSSNarrowDownstream = None
        self._dipionSelSSNarrowLong = None
        self._dipionSelSSWide = None
        #self._dipionFilterSS()

        self._dipionSelOSNarrowDownstream = None
        self._dipionSelOSNarrowLong = None
        self._dipionSelOSWide = None
        #self._dipionFilterOS()
                
        #self._tripionSelDipionSS = None
        #self._tripionFilterDipionSS()
        
        #self._tripionSelDipionOS = None
        #self._tripionFilterDipionOS()

       

        # some calculation for the major contribution to the corrected mass error
        self.mCorrCalc =  [ "from LoKiPhys.decorators import *",
                            # This is a simplified version of the corrected mass error calculation (only taking the uncertainty on the SV into account)
                            "invSqMassPt   =  1/sqrt( M*M +  BPVPTFLIGHT*BPVPTFLIGHT )",
                            "dMcorrdPt     =  0.5 * invSqMassPt * 2 * BPVPTFLIGHT + 1",
                            "A             =  PX*(VFASPF(VX) - BPV(VX))+ PY*(VFASPF(VY) - BPV(VY)) + PZ*(VFASPF(VZ) - BPV(VZ))", 
                            "B             =  (VFASPF(VX) - BPV(VX))*(VFASPF(VX) - BPV(VX)) + (VFASPF(VY) - BPV(VY))*(VFASPF(VY) - BPV(VY)) + (VFASPF(VZ) - BPV(VZ))*(VFASPF(VZ) - BPV(VZ))",
                            "dAdx_sv       =  PX",
                            "dAdy_sv       =  PY",
                            "dAdz_sv       =  PZ",
                            "dBdx_sv       =  2*(VFASPF(VX) - BPV(VX))", 
                            "dBdy_sv       =  2*(VFASPF(VY) - BPV(VY))",
                            "dBdz_sv       =  2*(VFASPF(VZ) - BPV(VZ))",
                            "dMcdx_sv      =  dMcorrdPt * 1/BPVPTFLIGHT * -0.5 * (2*A*B*dAdx_sv - A*A*dBdx_sv)/(B*B)",
                            "dMcdy_sv      =  dMcorrdPt * 1/BPVPTFLIGHT * -0.5 * (2*A*B*dAdy_sv - A*A*dBdy_sv)/(B*B)",
                            "dMcdz_sv      =  dMcorrdPt * 1/BPVPTFLIGHT * -0.5 * (2*A*B*dAdz_sv - A*A*dBdz_sv)/(B*B)",
                            "errxx         =  VFASPF(VCOV2(0,0)) * dMcdx_sv * dMcdx_sv",
                            "erryy         =  VFASPF(VCOV2(1,1)) * dMcdy_sv * dMcdy_sv",
                            "errzz         =  VFASPF(VCOV2(2,2)) * dMcdz_sv * dMcdz_sv",
                            "errxy         =  2*VFASPF(VCOV2(0,1)) * dMcdx_sv * dMcdy_sv",
                            "errxz         =  2*VFASPF(VCOV2(0,2)) * dMcdx_sv * dMcdz_sv",
                            "erryz         =  2*VFASPF(VCOV2(1,2)) * dMcdy_sv * dMcdz_sv",
                            "PSEUDOERR     = sqrt( errxx + erryy + errzz + errxy + errxz + erryz)",
                            # corrected mass with a missing pion (instead of a neutrino)
                            "MCORRPi       = (BPVCORRM - BPVPTFLIGHT) + sqrt(BPVPTFLIGHT*BPVPTFLIGHT + 139.57*139.57)"
                            ]
        
        self.registerLine( self._KPiPi_DipionSSNarrow_Downstream_line() )
        self.registerLine( self._KPiPi_DipionSSNarrow_Long_line() )
        self.registerLine( self._KPiPi_DipionSSWide_line() )
        self.registerLine( self._KPiPi_DipionOSNarrow_Downstream_line() )
        self.registerLine( self._KPiPi_DipionOSNarrow_Long_line() )
        self.registerLine( self._KPiPi_DipionOSWide_line() )
        self.registerLine( self._KPiPiPi_DipionSS_line() )
        self.registerLine( self._KPiPiPi_DipionOS_line() )
        

    ### The lines
    def _KPiPi_DipionSSNarrow_Downstream_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'_K2PiPiDipionSSNarrow_Downstream_Line',
                             prescale =  self.config["DipionSSNarrowDownstreamPrescale"],
                             RequiredRawEvents = ["Trigger","Calo","Velo","Tracker"],
                             algos = [ self._dipionFilterSSNarrowDownstream()])

    def _KPiPi_DipionSSNarrow_Long_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'_K2PiPiDipionSSNarrow_Long_Line',
                             prescale =  self.config["DipionSSNarrowLongPrescale"],
                             RequiredRawEvents = ["Trigger","Calo","Velo","Tracker"],
                             algos = [ self._dipionFilterSSNarrowLong()])

    def _KPiPi_DipionSSWide_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'_K2PiPiDipionSSWide_Line',
                             prescale =  self.config["DipionSSWidePrescale"],
                             RequiredRawEvents = ["Trigger","Calo","Velo","Tracker"],
                             algos = [ self._dipionFilterSSWide()])
    
    def _KPiPi_DipionOSNarrow_Downstream_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'_K2PiPiDipionOSNarrow_Downstream_Line',
                             prescale =  self.config["DipionOSNarrowDownstreamPrescale"],
                             RequiredRawEvents = ["Trigger","Calo","Velo","Tracker"],
                             algos = [ self._dipionFilterOSNarrowDownstream()])

    def _KPiPi_DipionOSNarrow_Long_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'_K2PiPiDipionOSNarrow_Long_Line',
                             prescale =  self.config["DipionOSNarrowLongPrescale"],
                             RequiredRawEvents = ["Trigger","Calo","Velo","Tracker"],
                             algos = [ self._dipionFilterOSNarrowLong()])

    def _KPiPi_DipionOSWide_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'_K2PiPiDipionOSWide_Line',
                             prescale =  self.config["DipionOSWidePrescale"],
                             RequiredRawEvents = ["Trigger","Calo","Velo","Tracker"],
                             algos = [ self._dipionFilterOSWide()])

    def _KPiPiPi_DipionSS_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'_K2PiPiPiDipionSS_Line',
                             prescale =  self.config["TripionSSPrescale"],
                             RequiredRawEvents = ["Trigger","Calo","Velo","Tracker"],
                             algos = [ self._tripionFilterDipionSS()])

    def _KPiPiPi_DipionOS_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'_K2PiPiPiDipionOS_Line',
                             prescale =  self.config["TripionOSPrescale"],
                             RequiredRawEvents = ["Trigger","Calo","Velo","Tracker"],
                             algos = [ self._tripionFilterDipionOS()])
        


    ### The particle (and composite) selection criteria
    #def _pionSelection( self ):
    #    return
    
    def _tagPionSelection( self ):
        return "(P > %(PionMinP)s ) & (PT > %(PionMinPT)s ) & (PROBNNghost < %(PionProbNNghost)s ) & (PROBNNpi > %(PionProbNNpi)s ) & (MIPCHI2DV(PRIMARY) > %(PionIPCHI2)s ) & (PROBNNp < %(PionProbNNp)s )"

    def _dipionSelectionNarrow( self ):
        return "(M > %(DipionMinMassNarrow)s )  & (M < %(DipionMaxMassNarrow)s ) & (VFASPF(VCHI2/VDOF) < %(DipionVertexChi2PerDoF)s )"\
               " & (BPVVDCHI2 > %(DipionFlightDistanceChi2)s ) & (VFASPF(VZ) > %(DipionMinVertexZ)s ) & (VFASPF(VZ) < %(DipionMaxVertexZ)s )" \
               " & (PT > %(DipionMinPT)s ) & (P > %(DipionMinP)s ) & (BPVDIRA > %(DipionDira)s ) & ( MCORRPi < %(DipionMaxMCorrNarrow)s )" \
               " & (PSEUDOERR < %(DipionMCorrPseudoErrNarrow)s )"
    
    def _dipionSelectionWide( self ):
        return "(M > %(DipionMinMassWide)s )  & (M < %(DipionMaxMassWide)s ) & (VFASPF(VCHI2/VDOF) < %(DipionVertexChi2PerDoF)s )"\
               " & (BPVVDCHI2 > %(DipionFlightDistanceChi2)s ) & (VFASPF(VZ) > %(DipionMinVertexZ)s ) & (VFASPF(VZ) < %(DipionMaxVertexZ)s )" \
               " & (PT > %(DipionMinPT)s ) & (P > %(DipionMinP)s ) & (BPVDIRA > %(DipionDira)s ) & ( MCORRPi < %(DipionMaxMCorrWide)s )" \
               " & (PSEUDOERR < %(DipionMCorrPseudoErrWide)s )"
    
    def _tripionSelection( self ):
        return "(M > %(TripionMinMass)s )  & (M < %(TripionMaxMass)s ) & (VFASPF(VCHI2/VDOF) < %(TripionVertexChi2PerDoF)s )"\
               " & (BPVVDCHI2 > %(TripionFlightDistanceChi2)s ) & (VFASPF(VZ) > %(TripionMinVertexZ)s ) & (VFASPF(VZ) < %(TripionMaxVertexZ)s )"


    ### Filter the pions
    def _downPionFilter( self ):
        if self._downPionSel is not None:
            return self._downPionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsDownPions
        
        _pi = FilterDesktop( Code = 'ALL' )
        _piSel=Selection("DownPi_for"+self._name,
                         Algorithm=_pi,
                         RequiredSelections = [StdNoPIDsDownPions])
        self._downPionSel=_piSel
        return _piSel
    
    def _longPionFilter( self ):
        if self._longPionSel is not None:
            return self._longPionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsPions
        
        _pi = FilterDesktop( Code = "(MIPCHI2DV(PRIMARY) > 49.0)" )
        _piSel=Selection("LongPi_for"+self._name,
                         Algorithm=_pi,
                         RequiredSelections = [StdNoPIDsPions])
        self._longPionSel=_piSel
        return _piSel


    #### Dipion ####
    def _dipionFilterSSNarrowDownstream( self ):
        if self._dipionSelSSNarrowDownstream is not None:
           return self._dipionSelSSNarrowDownstream
       
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
        from PhysSelPython.Wrappers import Selection, DataOnDemand

        _k2pipi = CombineParticles(
            DecayDescriptors = [ "[phi(1020) -> pi+ pi+]cc" ] ,
            DaughtersCuts    = { "pi+" : self._tagPionSelection()  %self._config },
            CombinationCut = "AALL",
            MotherCut       =  self._dipionSelectionNarrow() %self._config
            )

        _k2pipi.Preambulo = self.mCorrCalc
        

        _dipionSel=Selection("Kplus2PiPiSSNarrowDownstream_for"+self._name,
                             Algorithm= _k2pipi,
                             RequiredSelections = [self._downPionFilter()])
      
        self._dipionSelSSNarrowDownstream = _dipionSel
      
        return _dipionSel

    def _dipionFilterSSNarrowLong( self ):
        if self._dipionSelSSNarrowLong is not None:
           return self._dipionSelSSNarrowLong
       
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
        from PhysSelPython.Wrappers import Selection, DataOnDemand

        _k2pipi = CombineParticles(
            DecayDescriptors = [ "[phi(1020) -> pi+ pi+]cc" ] ,
            DaughtersCuts    = { "pi+" : self._tagPionSelection()  %self._config },
            CombinationCut = "AALL",
            MotherCut       =  self._dipionSelectionNarrow() %self._config
            )

        _k2pipi.Preambulo = self.mCorrCalc
        

        _dipionSel=Selection("Kplus2PiPiSSNarrowLong_for"+self._name,
                             Algorithm= _k2pipi,
                             RequiredSelections = [self._longPionFilter()])
      
        self._dipionSelSSNarrowLong = _dipionSel
      
        return _dipionSel


    def _dipionFilterSSWide( self ):
        if self._dipionSelSSWide is not None:
           return self._dipionSelSSWide
       
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
        from PhysSelPython.Wrappers import Selection, DataOnDemand

        _k2pipi = CombineParticles(
            DecayDescriptors = [ "[phi(1020) -> pi+ pi+]cc" ] ,
            DaughtersCuts    = { "pi+" : self._tagPionSelection()  %self._config },
            CombinationCut = "AALL",
            MotherCut       =  self._dipionSelectionWide() %self._config
            )

        _k2pipi.Preambulo = self.mCorrCalc

        _dipionSel=Selection("Kplus2PiPiSSWide_for"+self._name,
                             Algorithm= _k2pipi,
                             RequiredSelections = [self._downPionFilter(), self._longPionFilter()])
      
        self._dipionSelSSWide = _dipionSel
      
        return _dipionSel
    

    #### Dipion ####
    def _dipionFilterOSNarrowDownstream( self ):
        if self._dipionSelOSNarrowDownstream is not None:
           return self._dipionSelOSNarrowDownstream
       
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
        from PhysSelPython.Wrappers import Selection, DataOnDemand

        # Cut out the Kshort
        mCut = self._dipionSelectionNarrow() + " & (ADMASS('KS0') > 40)"
        
        _k2pipi = CombineParticles(
            DecayDescriptors = [ "phi(1020) -> pi+ pi-" ] ,
            DaughtersCuts    = { "pi+" : self._tagPionSelection()  %self._config,
                                 "pi-" : self._tagPionSelection()  %self._config },
            CombinationCut = "AALL",
            MotherCut       = mCut  %self._config
            )

        _k2pipi.Preambulo = self.mCorrCalc
        
        _dipionSel=Selection("Kplus2PiPiOSNarrowDownstream_for"+self._name,
                             Algorithm= _k2pipi,
                             RequiredSelections = [self._downPionFilter()])
      
        self._dipionSelOSNarrowDownstream = _dipionSel
      
        return _dipionSel

    def _dipionFilterOSNarrowLong( self ):
        if self._dipionSelOSNarrowLong is not None:
           return self._dipionSelOSNarrowLong
       
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
        from PhysSelPython.Wrappers import Selection, DataOnDemand

        # Cut out the Kshort
        mCut = self._dipionSelectionNarrow() + " & (ADMASS('KS0') > 40)"
        
        _k2pipi = CombineParticles(
            DecayDescriptors = [ "phi(1020) -> pi+ pi-" ] ,
            DaughtersCuts    = { "pi+" : self._tagPionSelection()  %self._config,
                                 "pi-" : self._tagPionSelection()  %self._config },
            CombinationCut = "AALL",
            MotherCut       = mCut  %self._config
            )

        _k2pipi.Preambulo = self.mCorrCalc
        
        _dipionSel=Selection("Kplus2PiPiOSNarrowLong_for"+self._name,
                             Algorithm= _k2pipi,
                             RequiredSelections = [self._longPionFilter()])
      
        self._dipionSelOSNarrowLong = _dipionSel
      
        return _dipionSel

    def _dipionFilterOSWide( self ):
        if self._dipionSelOSWide is not None:
           return self._dipionSelOSWide
       
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
        from PhysSelPython.Wrappers import Selection, DataOnDemand

        # Cut out the Kshort
        mCut = self._dipionSelectionWide() + " & (ADMASS('KS0') > 40)"
        
        _k2pipi = CombineParticles(
            DecayDescriptors = [ "phi(1020) -> pi+ pi-" ] ,
            DaughtersCuts    = { "pi+" : self._tagPionSelection()  %self._config,
                                 "pi-" : self._tagPionSelection()  %self._config },
            CombinationCut = "AALL",
            MotherCut       = mCut  %self._config
            )

        _k2pipi.Preambulo = self.mCorrCalc
        
        _dipionSel=Selection("Kplus2PiPiOSWide_for"+self._name,
                             Algorithm= _k2pipi,
                             RequiredSelections = [self._downPionFilter(), self._longPionFilter()])
      
        self._dipionSelOSWide = _dipionSel
      
        return _dipionSel

    #### Tripion ####
    def _tripionFilterDipionSS( self ):
        #if self._tripionSelDipionSS is not None:
        #   return self._tripionSelDipionSS
       
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
        from PhysSelPython.Wrappers import Selection, DataOnDemand


        _k2pipipi = CombineParticles(
            DecayDescriptors = [ "[K+ -> phi(1020) pi-]cc" ] ,
            CombinationCut = "AALL",
            MotherCut       = self._tripionSelection() %self._config
            )

        _tripionSel=Selection("Kplus2PiPiPi_DipionSS_for"+self._name,
                              Algorithm= _k2pipipi,
                              RequiredSelections = [self._downPionFilter(), self._longPionFilter(), self._dipionSelSSWide])
      
        #self._tripionSelDipionSS = _tripionSel
        
        return _tripionSel

    #### Tripion ####
    def _tripionFilterDipionOS( self ):
        #if self._tripionSelDipionOS is not None:
        #   return self._tripionSelDipionOS
       
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
        from PhysSelPython.Wrappers import Selection, DataOnDemand


        _k2pipipi = CombineParticles(
            DecayDescriptors = [ "[K+ -> phi(1020) pi+]cc" ] ,
            CombinationCut = "AALL",
            MotherCut       = self._tripionSelection() %self._config
            )

        _tripionSel=Selection("Kplus2PiPiPi_DipionOS_for"+self._name,
                              Algorithm= _k2pipipi,
                              RequiredSelections = [self._downPionFilter(), self._longPionFilter(), self._dipionSelOSWide])
      
        #self._tripionSelDipionOS = _tripionSel
        
        return _tripionSel
    

