from __future__ import print_function
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selection of He candidates via Median DEDX
'''

__author__ = ['Hendrik Jage', 'Dan Moise']
__date__ = '14.06.2023'
__version__ = 'v0r1'

__all__ = ('He_TT_Up2Conf', 'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

from StandardParticles import StdNoPIDsUpPions as UpPions
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from Configurables import LoKi__VoidFilter as VoidFilter

default_config = {
    'NAME': 'He_TT_Up2',
    'BUILDERTYPE': 'He_TT_Up2Conf',
    'WGs': ['IFT'],
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'Prescale': 1.0,
        'NumTTClusters':     2,
        'TTLLDCut':        3.7,
        'TTLLDVersionHe': 'v3',
        'TTLLDVersionZ1': 'v2',
        'RequiredRawEvents': ["Calo", "Rich", "Velo", "Tracker", "Muon", "HC"],
    },
}


class He_TT_Up2Conf(LineBuilder):
    __configuration_keys__ = list(default_config['CONFIG'].keys())

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        pid_cut = "   (TRTTCLUSTERS                                > %(NumTTClusters)s)" % self.config
        pid_cut += " & (DEDXPID('He', 'TT', '%(TTLLDVersionHe)s')" % self.config
        pid_cut += " -  DEDXPID('Z1', 'TT', '%(TTLLDVersionZ1)s')  > %(TTLLDCut)s)" % self.config

        pid_filter = FilterDesktop(Code=pid_cut)

        helium_sel = Selection('HeliumLLDTT_Up2ForSel',
                               Algorithm=pid_filter,
                               RequiredSelections=[UpPions])

        He_TT_Up2Line = StrippingLine(
            self.name + '_Line',
            prescale=self.config['Prescale'],
            RequiredRawEvents=self.config['RequiredRawEvents'],
            selection=helium_sel)

        self.registerLine(He_TT_Up2Line)
