from __future__ import print_function
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selection of He candidates via Median DEDX
'''

__author__ = ['Hendrik Jage', 'Dan Moise']
__date__ = '14.06.2023'
__version__ = 'v0r1'

__all__ = ('He_VL_VeloConf', 'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

from StandardParticles import StdAllNoPIDsVeloPions as VeloPions
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from Configurables import LoKi__VoidFilter as VoidFilter

default_config = {
    'NAME': 'He_VL_Velo',
    'BUILDERTYPE': 'He_VL_VeloConf',
    'WGs': ['IFT'],
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'Prescale': 1.0,
        'NumVeloRClusters':      5,
        'NumVeloPhiClusters':    5,
        'VeloRDedxMedian':   120.0,
        'VeloPhiDedxMedian': 120.0,
        'VeloROverflows':       -1,
        'VeloPhiOverflows':     -1,
        'RequiredRawEvents': ["Calo", "Rich", "Velo", "Tracker", "Muon", "HC"],
    },
}


class He_VL_VeloConf(LineBuilder):
    __configuration_keys__ = list(default_config['CONFIG'].keys())

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        _heliumCuts = "    (TRVELORCLUSTERDEDXMEDIAN    > %(VeloRDedxMedian)s)" % self.config
        _heliumCuts += " & (TRVELOPHICLUSTERDEDXMEDIAN  > %(VeloPhiDedxMedian)s)" % self.config
        _heliumCuts += " & (TRVELORCLUSTERS             > %(NumVeloRClusters)s)" % self.config
        _heliumCuts += " & (TRVELOPHICLUSTERS           > %(NumVeloPhiClusters)s)" % self.config
        _heliumCuts += " & (TRVELORCLUSTEROVERFLOWS()   > %(VeloROverflows)s)" % self.config
        _heliumCuts += " & (TRVELOPHICLUSTEROVERFLOWS() > %(VeloPhiOverflows)s)" % self.config

        heliumFilter = FilterDesktop(Code=_heliumCuts)

        myHelium = Selection('Helium_VL_Velo_ForSel',
                             Algorithm=heliumFilter,
                             RequiredSelections=[VeloPions])

        HeliumVLVeloLine = StrippingLine(
            self.name + '_Line',
            prescale=self.config['Prescale'],
            RequiredRawEvents=self.config['RequiredRawEvents'],
            selection=myHelium)

        self.registerLine(HeliumVLVeloLine)
