###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selection of Lb -> d p pi (pi) + X 
   Lb -> dppi + X
   Lb -> dppipi + X
This line is based on the StrippingLb2dp line by Paula Alvarez.
'''

__author__ = ['Gediminas Sarpis', 'Hendrik Jage']
__date__ = '03/19/2021'
__version__ = 'v1r0'

__all__ = ('Lb2dpXConf',
           'default_config'
           )


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop

from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllNoPIDsProtons as Protons
from StandardParticles import StdAllNoPIDsKaons as Kaons
from PhysSelPython.Wrappers import Selection, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from Configurables import LoKi__VoidFilter as VoidFilter

default_config = {
    'NAME'        : 'Lb2dpX',
    'BUILDERTYPE' : 'Lb2dpXConf',
    'WGs'         : ['IFT'],
    'STREAMS'     : ['BhadronCompleteEvent'],
    'CONFIG'      : {
        'Prescale_pi'          : 0.5 ,      
        'Prescale_pipi'        : 1.0 ,

        'TrackChi2Ndof'        : 3.0,
        'TrackGhostProb'       : 0.4,
        'TrackIPChi2'          : 16.,

        'PionPT'               : 500,
        'PionP'                : 1500,
        'PionPIDKpi'           : 0,
        
        'ProtonPT'             : 500,
        'ProtonP'              : 15000,
        'ProtonPIDppi'         : 15,
        'ProtonPIDpK'          : 10,

        'DeuteronPT'           : 500,
        'DeuteronP'            : 35000,
        
        'LbMassMax'            : 7000. ,
        'LbVtxChi2'            : 5. ,
        'LbDIRA_pipi'          : 0.999,
        'LbDIRA_pi'            : 0.9999,
        'LbFDChi2'             : 150,
        'LbPT'                 : 1500,
        'LbIPChi2_pi'          : 25, 
        'LbIPChi2_pipi'        : 50, 
    },
}

class Lb2dpXConf(LineBuilder) :
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        # Tracks
        _trackCuts          =    "(TRCHI2DOF          < %s)"         % self.config['TrackChi2Ndof']
        _trackCuts         += " & (TRGHOSTPROB        < %s)"         % self.config['TrackGhostProb']
        _trackCuts         += " & (MIPCHI2DV(PRIMARY) > %s)"         % self.config['TrackIPChi2']

        # Proton
        _protonPidCuts      =    "((PIDp-PIDpi)       > %s)"         % self.config['ProtonPIDppi']
        _protonPidCuts     += " & ((PIDp-PIDK)        > %s)"         % self.config['ProtonPIDpK']

        _protonKinCuts      =    "(PT                 > %s *MeV)"    % self.config['ProtonPT']
        _protonKinCuts     += " & (P                  > %s *MeV)"    % self.config['ProtonP']
        _protonCuts         = _trackCuts + " & " + _protonPidCuts + " & " + _protonKinCuts

        # Pion
        _pionPidCuts        =    "((PIDK - PIDpi)     < %s )"        % self.config['PionPIDKpi']

        _pionKinCuts        =    "(PT                 > %s *MeV)"    % self.config['PionPT']
        _pionKinCuts       += " & (P                  > %s *MeV)"    % self.config['PionP']
        _pionCuts           = _trackCuts + " & " + _pionPidCuts + " & " + _pionKinCuts

        # Deuteron
        _deuteronKinCuts    =    "(PT                 > %s *MeV)"    % self.config['DeuteronPT']
        _deuteronKinCuts   += " & (P                  > %s *MeV)"    % self.config['DeuteronP']

        _deuteronCuts       = _trackCuts + " & " + _deuteronKinCuts


        # Substitute deuteron hypothesis
        from Configurables import SubstitutePID
        subst_K2d = SubstitutePID(
            'subst_K2d',
            Code = "DECTREE('[K+]CC')",
            # note that SubstitutePID can't handle automatic CC
            Substitutions = {
            'K+ ': 'deuteron',
            'K-' : 'deuteron~',
            },
            MaxChi2PerDoF   = -1,
            UsePVConstraint = False,
        )
        
        # create a selection using the substitution algorithm
        Deuterons = Selection(
            'subst_K2d_Sel',
            Algorithm=subst_K2d,
            RequiredSelections=[Kaons]
        )

        # Daughters
        self.ProtonForLb2dpX = SimpleSelection( "ProtonFor" + self.name, FilterDesktop,
          [ Protons ],
          Code  = _protonCuts,
        )

        self.PionForLb2dpX = SimpleSelection( "PionFor" + self.name, FilterDesktop,
          [ Pions ],
          Code  = _pionCuts,
        )

        self.DeuteronForLb2dpX = SimpleSelection( "DeuteronFor" + self.name, FilterDesktop, 
          [ Deuterons ],
          Code  = _deuteronCuts,
        )
        self.makeLb2dppi()
        self.makeLb2dppipi()

    

    def makeLb2dppi(self):

        _decayDescriptors = ["[Lambda_b0 -> p~- pi+ deuteron]cc", "[Lambda_b0 -> p~- pi- deuteron]cc"]

        _motherCut                = "   (BPVDIRA            > %s)"        % self.config['LbDIRA_pi'] 
        _motherCut               += " & (VFASPF(VCHI2/VDOF) < %s)"        % self.config['LbVtxChi2']
        _motherCut               += " & (BPVVDCHI2          > %s)"        % self.config['LbFDChi2']
        _motherCut               += " & (MIPCHI2DV(PRIMARY) < %s)"        % self.config['LbIPChi2_pi']
        _motherCut               += " & (PT                 > %s)"        % self.config['LbPT']
        _motherCut               += " & (M                  < %s)"        % self.config['LbMassMax']

        _Lb2dppi = CombineParticles( DecayDescriptors = _decayDescriptors,
                                     MotherCut = _motherCut)                            

        _Lb2dppi_sel = Selection( self.name+"pi",
                                Algorithm = _Lb2dppi,
                                RequiredSelections = [self.DeuteronForLb2dpX, self.ProtonForLb2dpX, self.PionForLb2dpX])

        Lb2dppiLine = StrippingLine( self.name + '_piLine',
                                   prescale = self.config['Prescale_pi'],
                                   selection =  _Lb2dppi_sel,
                                   RequiredRawEvents = ["Rich","Velo","Tracker","Calo"])
        
        self.registerLine(Lb2dppiLine)


        
    def makeLb2dppipi(self):

        _decayDescriptors = ["[Lambda_b0 -> p~- pi+ pi- deuteron]cc"]

        _motherCut                = "   (BPVDIRA            > %s)"        % self.config['LbDIRA_pipi'] 
        _motherCut               += " & (VFASPF(VCHI2/VDOF) < %s)"        % self.config['LbVtxChi2']
        _motherCut               += " & (BPVVDCHI2          > %s)"        % self.config['LbFDChi2']
        _motherCut               += " & (MIPCHI2DV(PRIMARY) < %s)"        % self.config['LbIPChi2_pipi']
        _motherCut               += " & (PT                 > %s)"        % self.config['LbPT']
        _motherCut               += " & (M                  < %s)"        % self.config['LbMassMax']

        _Lb2dppipi = CombineParticles( DecayDescriptors = _decayDescriptors,
                                       #CombinationCut = _combCut,
                                       MotherCut = _motherCut)                            

        _Lb2dppipi_sel = Selection( self.name+"pipi",
                                Algorithm = _Lb2dppipi,
                                RequiredSelections = [self.DeuteronForLb2dpX, self.ProtonForLb2dpX, self.PionForLb2dpX])

        Lb2dppipiLine = StrippingLine( self.name + '_pipiLine',
                                   prescale = self.config['Prescale_pipi'],
                                   selection =  _Lb2dppipi_sel,
                                   RequiredRawEvents = ["Rich","Velo","Tracker","Calo"])

        
        self.registerLine(Lb2dppipiLine)
