###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

_selections = [
  "StrippingHe_VLTT_Long",
  "StrippingHe_VLTT_Up",
  "StrippingHe_IT_Long", 
  "StrippingHe_IT_Down",  
  "StrippingHe_TTOT_Long",
  "StrippingHe_TTOT_Down",
  "StrippingHe_TT_Up",
  "StrippingHe_VL_Long",
  "StrippingHe_VL_Up",
  "StrippingHe_VL_Velo",
  "StrippingHeNOverflows",
  "StrippingHe_SV_DD",
  "StrippingHe_TTOT_Long2",
  "StrippingHe_TTOT_Down2",
  "StrippingHe_TT_Up2",
  "StrippingHe_SV_DD2",
]

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
