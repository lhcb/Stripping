from __future__ import print_function
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selection of He candidates via Median DEDX
'''

__author__ = ['Hendrik Jage', 'Dan Moise']
__date__ = '14.06.2023'
__version__ = 'v0r1'

__all__ = ('He_VLTT_UpConf', 'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

from StandardParticles import StdNoPIDsUpPions as UpPions
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from Configurables import LoKi__VoidFilter as VoidFilter

default_config = {
    'NAME': 'He_VLTT_Up',
    'BUILDERTYPE': 'He_VLTT_UpConf',
    'WGs': ['IFT'],
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'Prescale': 1.0,
        'NumVeloRClusters':     3,
        'NumVeloPhiClusters':   3,
        'NumTTClusters':        2,
        'VeloRDedxMedian':   90.0,
        'VeloPhiDedxMedian': 90.0,
        'TTDedxMedian':      80.0,
        'RequiredRawEvents': ["Calo", "Rich", "Velo", "Tracker", "Muon", "HC"],
    },
}


class He_VLTT_UpConf(LineBuilder):
    __configuration_keys__ = list(default_config['CONFIG'].keys())

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        _heliumCuts = "    (TRVELORCLUSTERDEDXMEDIAN   > %(VeloRDedxMedian)s)" % self.config
        _heliumCuts += " & (TRVELOPHICLUSTERDEDXMEDIAN > %(VeloPhiDedxMedian)s)" % self.config
        _heliumCuts += " & (TRTTCLUSTERDEDXMEDIAN      > %(TTDedxMedian)s)" % self.config
        _heliumCuts += " & (TRVELORCLUSTERS            > %(NumVeloRClusters)s)" % self.config
        _heliumCuts += " & (TRVELOPHICLUSTERS          > %(NumVeloPhiClusters)s)" % self.config
        _heliumCuts += " & (TRTTCLUSTERS               > %(NumTTClusters)s)" % self.config

        heliumFilter = FilterDesktop(Code=_heliumCuts)

        myHelium = Selection('Helium_VLTT_Up_ForSel',
                             Algorithm=heliumFilter,
                             RequiredSelections=[UpPions])

        HeliumVLTTUpLine = StrippingLine(
            self.name + '_Line',
            prescale=self.config['Prescale'],
            RequiredRawEvents=self.config['RequiredRawEvents'],
            selection=myHelium)

        self.registerLine(HeliumVLTTUpLine)
