###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of B->K/pi eta(') stripping Selections and StrippingLines.
Provides functions to build  eta', eta, Bu  selections.
Provides class B2EtaphConf, which constructs the Selections and StrippingLines
given a configuration dictionary.
"""

__author__ = ['Youhua Yang']
__date__ = '12/7/2023'
__version__ = ''
__all__ = ('B2EtaphConf',
           'default_config')

default_config = {
    'NAME'         : 'B2Etaph',
    'WGs'          : ['BnoC'],
    'BUILDERTYPE'  : 'B2EtaphConf',
    'CONFIG'       : {
                  'Trk_Chi2'                : 4.0,
                  'Trk_PT'                  : 300.0,
                  'Trk_GP'                  : 0.5,
                  'Tight_Trk_GP'            : 0.3,
                  'pK_PT'                   : 500., #1000.
                  'Tight_pK_PT'             : 1000.,
                  'pK_IPCHI2'               : 20.,
                  'Tight_pK_IPCHI2'         : 30.,
                  'ProbNNCut'               : 0.1,
                  'TightProbNNCut'          : 0.5,
                  'ba_ProbNNCut'            : 0.0,
                  'eta_PT'                  : 2000,
                  'eta_MassWindow'          : 200.0,
                  'etaforetap_MassWindow'   : 75.0,
                  'eta_vtxChi2'             : 10.,
                  'eta_DOCA'                : 10.0, #20
                  'gamma_PT'                : 200, #photons from eta
                  'eta_prime_MassWindow'    : 150.0,
                  'eta_prime_PT'            : 2000.0,
                  'eta_prime_vtxChi2'       : 10.0,
                  'eta_prime_DOCA'          : 10.0, #15
                  'BDaug_maxDocaChi2'       : 15.0,  #20
                  'B_MassWindow'            : 750.0,
                  'Eta2g_B_MassWindow'      : 250.0,
                  'B_PTmin'                 : 1500.0,
                  'B_VtxChi2'               : 15.0,
                  'B_Dira'                  : 0.9995,
                  'B_IPCHI2'                : 20.0,
                  'B_eta_IPCHI2'            : 6.0, 
                  'GEC_MaxTracks'           : 250,
                  'Prescale'                : 1.0,
                  'Postscale'               : 1.0,
                  'etaGG_Prescale'          : 1.0
                  },
    'STREAMS'     : ['Bhadron']
    }


from GaudiKernel import SystemOfUnits as Units
from PhysSelPython.Wrappers import DataOnDemand, Selection, SelectionSequence, MergedSelection
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays
#from Configurables import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


from StandardParticles import StdLoosePions as Pions
from StandardParticles import StdLooseKaons as Kaons
from StandardParticles import StdLooseProtons as Protons
from StandardParticles import StdLooseAllPhotons as Photons
from StandardParticles import StdLooseResolvedEta as Eta
from StandardParticles import StdLooseEta2gg as WideEta
from StandardParticles import StdLooseResolvedPi0 as PiZero

class B2EtaphConf(LineBuilder) :
    """
    Builder of B->XEta stripping Selection and StrippingLine.
    Constructs B -> K/pi eta/eta' Selections and StrippingLines from a configuration dictionary.
    
    Exports as instance data members:
    selEtap                : eta' Selection Object
    lines                  : List of lines, [dd_line, ll_line]

    Exports as class data member:
    B2KShhConf.__configuration_keys__ : List of required configuration parameters.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        
        LineBuilder.__init__(self,name,config)

        B2etap_K_name = name+'B2etapK'
        B2etaGG_K_name = name+'B2etaGGK'
        B2etaGG_Pi_name = name+'B2etaGGPi'
        B2eta3Pi_K_name = name+'B2eta3PiK'
        
        GECCode = {'Code' : "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %s)" % config['GEC_MaxTracks'],
                   'Preambulo' : ["from LoKiTracks.decorators import *"]}

                
        Hlt1Filter = "(HLT_PASS_RE('Hlt1(Two)?TrackMVADecision'))"
        Hlt2Filter = "(HLT_PASS_RE('Hlt2Topo[234]BodyDecision'))"

        #---------------------------------------
        # RelatedInfo

        relinfo = [ { "Type" : "RelInfoConeVariables"
                    , "ConeAngle" : 1.5
                    , "TracksLocation" : "/Event/Phys/StdNoPIDsPions"
                    , "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPXASYM', 'CONEPYASYM', 'CONEPZASYM', 'CONEPASYM', 'CONEPTASYM', 'CONEDELTAETA', 'CONEDELTAPHI']
                    , "Location"  : 'P2ConeVar15' },
                    { "Type" : "RelInfoConeVariables"
                    , "ConeAngle" : 1.0
                    , "TracksLocation" : "/Event/Phys/StdNoPIDsPions"
                    , "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPXASYM', 'CONEPYASYM', 'CONEPZASYM', 'CONEPASYM', 'CONEPTASYM', 'CONEDELTAETA', 'CONEDELTAPHI']
                    , "Location"  : 'P2ConeVar10' },
                    { "Type" : "RelInfoConeVariables"
                    , "ConeAngle" : 1.7
                    , "TracksLocation" : "/Event/Phys/StdNoPIDsPions"
                    , "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPXASYM', 'CONEPYASYM', 'CONEPZASYM', 'CONEPASYM', 'CONEPTASYM', 'CONEDELTAETA', 'CONEDELTAPHI']
                    , "Location"  : 'P2ConeVar17' },
                    { "Type" : "RelInfoConeVariables"
                    , "ConeAngle" : 2.0
                    , "TracksLocation" : "/Event/Phys/StdNoPIDsPions"
                    , "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPXASYM', 'CONEPYASYM', 'CONEPZASYM', 'CONEPASYM', 'CONEPTASYM', 'CONEDELTAETA', 'CONEDELTAPHI']
                    , "Location"  : 'P2ConeVar20' },
                    { "Type"      : "RelInfoVertexIsolation"
                    , "Location"  : 'VtxIsolationVar'}
                  ]


        _tagging=False
        self.refitPVs=True
        
        self.pions = Pions
        self.kaons = Kaons
        self.protons = Protons
        self.photons = Photons
        self.eta = Eta
        self.wideeta=WideEta
        self.pizero = PiZero

        self.daughters = MergedSelection("DaughtersFor" + name, RequiredSelections = [ self.pions, self.photons, self.pizero] )

        self.makeFilterKaons( 'KFor'+B2etap_K_name, config )
        self.makeFilterTightKaons( 'KFor'+B2etaGG_K_name, config )
        self.makeFilterTightPions( 'PiFor'+B2etaGG_Pi_name, config )
        self.makeFilterKaons( 'KFor'+B2eta3Pi_K_name, config )



        self.makeEtaGG('EtaGGfor'+name,config)
        self.makeEta3Pi('Eta3Pifor'+name,config)
        self.makeEtaforEtap('EtaforEtapfor'+name,config)
        self.makeEtap('Etapfor'+name,config)


        self.makeB2Ketap(B2etap_K_name, config)
        self.makeB2KetaGG(B2etaGG_K_name, config)
        self.makeB2PietaGG(B2etaGG_Pi_name, config)
        self.makeB2Keta3Pi(B2eta3Pi_K_name, config)


        self.B2etap_K_line = StrippingLine(B2etap_K_name+'Line',
                                             prescale = config['Prescale'],
                                             postscale = config['Postscale'],
                                             selection = self.selB2etapK,
                                             HLT1 = Hlt1Filter,
                                             HLT2 = Hlt2Filter,
                                             FILTER = GECCode,
                                             #ODIN = TCKFilters,
                                             RelatedInfoTools = relinfo,
                                             EnableFlavourTagging=_tagging
                                             )
        self.B2etaGG_K_line = StrippingLine(B2etaGG_K_name+'Line',
                                             prescale = config['etaGG_Prescale'],
                                             postscale = config['Postscale'],
                                             selection = self.selB2etaGGK,
                                             FILTER = GECCode,
                                             RelatedInfoTools = relinfo,
                                             EnableFlavourTagging=_tagging
                                             )
        self.B2etaGG_Pi_line = StrippingLine(B2etaGG_Pi_name+'Line',
                                             prescale = config['etaGG_Prescale'],
                                             postscale = config['Postscale'],
                                             selection = self.selB2etaGGPi,
                                             FILTER = GECCode,
                                             RelatedInfoTools = relinfo,
                                             EnableFlavourTagging=_tagging
                                             )
        self.B2eta3Pi_K_line = StrippingLine(B2eta3Pi_K_name+'Line',
                                               prescale = config['Prescale'],
                                               postscale = config['Postscale'],
                                               selection = self.selB2eta3PiK,
                                               HLT1 = Hlt1Filter,
                                               FILTER = GECCode,
                                             RelatedInfoTools = relinfo,
                                               EnableFlavourTagging=_tagging
                                               )
                
        
        self.registerLine(self.B2etap_K_line)
        self.registerLine(self.B2etaGG_K_line)
        self.registerLine(self.B2etaGG_Pi_line)
        self.registerLine(self.B2eta3Pi_K_line)
        
        

    def makeFilterKaons( self, name, config):

        _momCut = "(PT>%s*MeV)"           % config['pK_PT']
        _GPCut = "(TRGHOSTPROB<%s)"       % config['Trk_GP']
        _PIDCut = "(PROBNNk>%s)"          % config['ba_ProbNNCut']
        _IPChi2_Cut = "(BPVIPCHI2()>%s)"  % config['pK_IPCHI2']

        _allCuts = _momCut+'&'+_GPCut+'&'+_PIDCut+'&'+_IPChi2_Cut

        _kaonFilter=FilterDesktop(Code=_allCuts)
        self.selKaons = Selection( name, Algorithm=_kaonFilter, RequiredSelections=[self.kaons] )

    def makeFilterTightKaons( self, name, config):

        _momCut = "(PT>%s*MeV)"           % config['Tight_pK_PT']
        _GPCut = "(TRGHOSTPROB<%s)"       % config['Tight_Trk_GP']
        _PIDCut = "(PROBNNk>%s)"          % config['TightProbNNCut']
        _IPChi2_Cut = "(BPVIPCHI2()>%s)"  % config['Tight_pK_IPCHI2']

        _allCuts = _momCut+'&'+_GPCut+'&'+_PIDCut+'&'+_IPChi2_Cut

        _kaonFilter=FilterDesktop(Code=_allCuts)
        self.selTightKaons = Selection( name, Algorithm=_kaonFilter, RequiredSelections=[self.kaons] )

    def makeFilterTightPions( self, name, config):

        _momCut = "(PT>%s*MeV)"           % config['Tight_pK_PT']
        _GPCut = "(TRGHOSTPROB<%s)"       % config['Tight_Trk_GP']
        _PIDCut = "(PROBNNk>%s)"          % config['TightProbNNCut']
        _IPChi2_Cut = "(BPVIPCHI2()>%s)"  % config['Tight_pK_IPCHI2']

        _allCuts = _momCut+'&'+_GPCut+'&'+_PIDCut+'&'+_IPChi2_Cut

        _pionFilter=FilterDesktop(Code=_allCuts)
        self.selTightPions = Selection( name, Algorithm=_pionFilter, RequiredSelections=[self.pions] )


    

    def makeEtaGG( self, name, config):
        
        _etaPTCut = "(PT>%s*MeV)"                      % config['eta_PT']
        _gammaPT1 = "(CHILDCUT((PT>%s*MeV),1))"        % config['gamma_PT']
        _gammaPT2 = "(CHILDCUT((PT>%s*MeV),2))"        % config['gamma_PT']
        
        _allCuts = _etaPTCut+'&'+_gammaPT1+'&'+_gammaPT2
        
        _etaFilter = FilterDesktop(Code=_allCuts)
        self.selEtaGG=Selection(name, Algorithm=_etaFilter, RequiredSelections=[self.eta])

    def makeEtaforEtap(self, name, config):
        _etaforetapMassCut= "(ADMASS('eta')<%s*MeV)"  %config['etaforetap_MassWindow']
        
        _etaforetapFilter=FilterDesktop(Code=_etaforetapMassCut)

        self.selEtaforEtap=Selection(name, Algorithm=_etaforetapFilter,RequiredSelections=[self.wideeta])
        
    def makeEta3Pi(self,name,config):

        _PTCut = "(PT>%s*MeV)"                         % config['eta_PT']
        _massCut = "(ADAMASS('eta')<%s*MeV)"           % config['eta_MassWindow']
        _vtxCut = "(VFASPF(VCHI2/VDOF)<%s)"            % config['eta_vtxChi2']
        _docaCut = "(ACUTDOCACHI2(%s,''))"                 % config['eta_DOCA']

        _track_PT="(PT>%s*MeV)"                        % config['Trk_PT']
        _track_Chi2="(TRCHI2DOF<%s)"                   % config['Trk_Chi2']
        _track_GPCut ="(TRGHOSTPROB<%s)"               % config['Trk_GP']
        _track_PIDCut = "(PROBNNpi>%s)"                % config['ProbNNCut']
        
        _allCuts = _PTCut+'&'+_vtxCut
        _trackCuts = _track_PT+'&'+_track_Chi2+'&'+_track_GPCut+'&'+_track_PIDCut
        _combCuts=_massCut+'&'+_docaCut
        _combCut12Doca="ACHI2DOCA(1,2)<%s"                     % config['eta_DOCA']
        _combCut12Vtx="(VFASPF(VCHI2/VDOF)<%s)"            % config['eta_vtxChi2']
        _combCut12=_combCut12Doca
        
        #_eta3Pi=CombineParticles("eta3Pi",
        _eta3Pi=DaVinci__N3BodyDecays(DecayDescriptor = "eta -> pi+ pi- pi0",
                                      Combination12Cut = _combCut12,
                                      CombinationCut=_combCuts,
                                      MotherCut=_allCuts,
                                      DaughtersCuts = { "pi+" : _trackCuts, "pi-" : _trackCuts})
        
        self.selEta3Pi = Selection(name, Algorithm=_eta3Pi, RequiredSelections=[self.daughters])
        
    def makeEtap( self, name, config):
      
        _massCut = "(ADAMASS('eta_prime')<%s*MeV)"     % config['eta_prime_MassWindow']
        _PTCut = "(PT>%s*MeV)"                         % config['eta_prime_PT']
        _vtxCut = "(VFASPF(VCHI2/VDOF)<%s)"            % config['eta_prime_vtxChi2']
        _docaCut = "(ACUTDOCACHI2(%s,''))"                 % config['eta_prime_DOCA']
        _track_PT="(PT>%s*MeV)"                        % config['Trk_PT']
        _track_Chi2="(TRCHI2DOF<%s)"                   % config['Trk_Chi2']
        _track_GPCut ="(TRGHOSTPROB<%s)"               % config['Trk_GP']
        _track_PIDCut = "(PROBNNpi>%s)"                % config['ProbNNCut']

        _allCuts = _PTCut+'&'+_vtxCut
        _trackCuts = _track_PT+'&'+_track_Chi2+'&'+_track_GPCut+'&'+_track_PIDCut
        _combCuts=_massCut+'&'+_docaCut
        _combCut12Doca="ACHI2DOCA(1,2)<%s"                     % config['eta_prime_DOCA']
        _combCut12Vtx="(VFASPF(VCHI2/VDOF)<%s)"            % config['eta_prime_vtxChi2']
        _combCut12=_combCut12Doca
        
        #_etap=CombineParticles("etap",
        _etap=DaVinci__N3BodyDecays(DecayDescriptors = ["eta_prime -> pi+ pi- gamma", "eta_prime -> pi+ pi- eta"],
                                    Combination12Cut = _combCut12,
                                    CombinationCut=_combCuts,
                                    MotherCut=_allCuts,
                                    DaughtersCuts = { "pi+" : _trackCuts, "pi-" : _trackCuts})
        
        self.selEtap = Selection(name, Algorithm=_etap, RequiredSelections=[self.daughters,self.selEtaforEtap])
        #printable=PrintSelection(self.selEtap)
        
    def makeB2Ketap(self, name, config):
        _massCut = "(ADAMASS('B+')<%s*MeV)"            % config['B_MassWindow']
        _PTCut = "(PT>%s*MeV)"                         % config['B_PTmin']
        _docaCut = "(ACUTDOCACHI2(%s,''))"                 % config['BDaug_maxDocaChi2']
        _vtxCut = "(VFASPF(VCHI2/VDOF)<%s)"            % config['B_VtxChi2']
        _diraCut = "(BPVDIRA>%s)"                      % config['B_Dira']
        _IPChi2Cut = "(BPVIPCHI2()<%s)"                % config['B_IPCHI2']
        
        _combCuts = _massCut+'&'+_docaCut
        _allCuts = _PTCut+'&'+_vtxCut+'&'+_diraCut+'&'+_IPChi2Cut
                
        _b2etapKst = CombineParticles(DecayDescriptor = "[B+ ->  K+ eta_prime]cc",
                                      CombinationCut = _combCuts,
                                      MotherCut = _allCuts,
                                      ReFitPVs = self.refitPVs )
        self.selB2etapK = Selection( name, Algorithm=_b2etapKst, RequiredSelections=[self.selKaons, self.selEtap])
        
    def makeB2KetaGG(self, name, config):
        _massCut = "(ADAMASS('B+')<%s*MeV)"            % config['Eta2g_B_MassWindow']
        _PTCut = "(PT>%s*MeV)"                         % config['B_PTmin']
        _IPChi2Cut = "(BPVIPCHI2()<%s)"                % config['B_eta_IPCHI2']
        
        _combCuts = _massCut
        _allCuts = _PTCut+'&'+_IPChi2Cut
        
        _b2etaGGK = CombineParticles(DecayDescriptor = "[B+ -> K+ eta]cc",
                                       CombinationCut = _combCuts,
                                       MotherCut = _allCuts,
                                       ReFitPVs = self.refitPVs )
        self.selB2etaGGK = Selection( name, Algorithm=_b2etaGGK, RequiredSelections=[self.selTightKaons, self.selEtaGG])
        
    def makeB2PietaGG(self, name, config):
        _massCut = "(ADAMASS('B+')<%s*MeV)"            % config['Eta2g_B_MassWindow']
        _PTCut = "(PT>%s*MeV)"                         % config['B_PTmin']
        _IPChi2Cut = "(BPVIPCHI2()<%s)"                % config['B_eta_IPCHI2']
        
        _combCuts = _massCut
        _allCuts = _PTCut+'&'+_IPChi2Cut
        
        _b2etaGGPi = CombineParticles(DecayDescriptor = "[B+ -> pi+ eta]cc",
                                       CombinationCut = _combCuts,
                                       MotherCut = _allCuts,
                                       ReFitPVs = self.refitPVs )
        self.selB2etaGGPi = Selection( name, Algorithm=_b2etaGGPi, RequiredSelections=[self.selTightPions, self.selEtaGG])
        
    def makeB2Keta3Pi(self, name, config):
        _massCut = "(ADAMASS('B+')<%s*MeV)"            % config['B_MassWindow']
        _PTCut = "(PT>%s*MeV)"                         % config['B_PTmin']
        _docaCut = "(ACUTDOCACHI2(%s,''))"                 % config['BDaug_maxDocaChi2']
        _vtxCut = "(VFASPF(VCHI2/VDOF)<%s)"            % config['B_VtxChi2']
        _diraCut = "(BPVDIRA>%s)"                      % config['B_Dira']
        _IPChi2Cut = "(BPVIPCHI2()<%s)"                % config['B_IPCHI2']
        
        _combCuts = _massCut+'&'+_docaCut
        _allCuts = _PTCut+'&'+_IPChi2Cut+'&'+_vtxCut+'&'+_diraCut
                
        _b2eta3PiKst = CombineParticles(DecayDescriptor = "[B+ -> K+ eta]cc",
                                        CombinationCut = _combCuts,
                                        MotherCut = _allCuts,
                                        ReFitPVs = self.refitPVs )
        self.selB2eta3PiK = Selection( name, Algorithm=_b2eta3PiKst, RequiredSelections=[self.selKaons, self.selEta3Pi])
        

