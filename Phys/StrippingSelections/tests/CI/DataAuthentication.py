#!/#usr/bin/env python
###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import apd as apd
import xml.etree.ElementTree as ET

###########################################################
#                                                         #
# The function of this script is to add authentication    #
# tokens to the paths in the file catalog xml file.       #
# This is necessary for the CI to have access to the data #
#                                                         #
###########################################################


########################################################
# Functions to do all of the dirty authentication work #
########################################################

def append_token(pfn):
    print("Appending token for '{}' ".format(pfn))
    token = apd.auth(pfn) # Replace the pfn with the tokened pfns.
    return token

# Use this guy to process the pfn, obstaining the mdf prepend for the xrootd protocol, and then putting it back
def process_pfn(string):
    if string.startswith("mdf:"):
        temp_string = string[4:]
    else:
        temp_string = string
    new_string = append_token(temp_string)
    if string.startswith("mdf:"):
        new_string = "mdf:" + new_string
    return new_string


def replace_elements_in_xml(file_path):
    # Load XML file
    tree = ET.parse(file_path)
    root = tree.getroot()
    # Iterate over each old element and its corresponding replacement
    for element in root.iter('pfn'):
        element.set('name', process_pfn(element.get('name')))

    # Modify the pool catalog file
    xml_string = (
        '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n' +
        '<!DOCTYPE POOLFILECATALOG SYSTEM "InMemory">\n' +
        ET.tostring(root).decode("utf-8")
    )
    with open(file_path, "wt") as fh:
        fh.write(xml_string)

    print("XML file '{}' modified and saved as '{}'.".format(file_path, file_path))


def main():
    # Data
    for year in ["2016", "2017", "2018"]:
        if year == "2016":
            data_XML_file = "Phys/StrippingSelections/tests/data/Reco16_DataType2016_Run174858.xml"
        elif year == "2017":
            data_XML_file = "Phys/StrippingSelections/tests/data/Reco17_DataType2017_Run195969.xml"
        elif year == "2018":
            data_XML_file = "Phys/StrippingSelections/tests/data/Reco18_DataType2018_Run214741.xml"
        else:
            raise NotImplementedError(year)
    
        # Create a new XML file to use in the test
        replace_elements_in_xml(data_XML_file)


if __name__ =='__main__':
        main()
