###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import appendPostConfigAction
from Configurables import DaVinci, Stripping, EventTuple, TupleToolStripping

def make_tuples() :
    evttuple = EventTuple('StrippingDecsTuple')
    evttuple.ToolList = ['TupleToolStripping', 'TupleToolEventInfo']
    ttstrip = TupleToolStripping()
    linenames = set()
    for stream in Stripping().cache['streams'] :
        linenames.add('StrippingStream' + stream.name())
        for line in stream.lines :
            linenames.add(line.name())
    ttstrip.StrippingList = [name + 'Decision' for name in linenames]
    print 'Adding TupleToolStripping for:'
    print ttstrip.StrippingList
    ttstrip.StrippingReportsLocations = "Strip/Phys/DecReports"
    evttuple.addTool(ttstrip)
    DaVinci().appendToMainSequence([evttuple])

DaVinci().TupleFile = 'DaVinciTuples.root'
appendPostConfigAction(make_tuples)
