#!/user/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################################

import os
from copy import copy
from math import sqrt

corr = {}
listAlgs = []
countActiveLines = 0
countActiveLinesNoTiming = 0
reachedLastEvent = False
foundStrippingReport = False
reachedCorrelations = False
foundStrippingCorrelations = False
foundSeparator = False
nevents = 0
f = open('1k.log')
for l in f : 
  if l.find('Application Manager Stopped successfully') > 0 : 
    reachedLastEvent = True
    continue
  if reachedLastEvent : 
    if l.find('StrippingReport') == 0: 
      nevents += int(l.split()[6])
    if l.find('StrippingGlobal') > 0 : 
      foundStrippingReport = True
    elif foundStrippingReport and l.find('StrippingSequenceStream') > 0 : 
      ls = l.split('|')
      name = ls[1].strip().rstrip('_')[24:]
      curr_stream = name
      nev = int(ls[3])
    elif foundStrippingReport and l.find('Stripping') > 0 : 
      ls = l.split('|')
      name = ls[1].strip()[1:]
      nev = int(ls[3])
      if nev != 0:
        countActiveLines += 1
      if "TIMING" not in name and nev != 0:
        countActiveLinesNoTiming += 1
    elif foundStrippingReport: 
      break
for l in f:
  if l.find('SUCCESS Correlation Table') > 0:    
    reachedCorrelations = True
    continue
  if reachedCorrelations:
    if l.find('Eff') > 0:
      foundStrippingCorrelations = True
    elif foundStrippingCorrelations and l.find('---------------------------') == 0:
      foundSeparator = True
    elif foundStrippingCorrelations and l.find('Stripping') > 0:  
      ls = l.split('#####')
      name = ls[0].split()[1]
      corrs = ls[1].split()
      listAlgs.append(name)
      corr[name] = corrs
    elif foundStrippingCorrelations:
      break

f.close()

from ROOT import *
gStyle.SetOptStat(0)
c = TCanvas('c','c',2000,2000,2000,2000)
correlations = TH2D('corr','Line Correlation',len(listAlgs),0.5,len(listAlgs)+0.5,len(listAlgs),0.5,len(listAlgs)+0.5)
correlations.GetXaxis().SetTickLength(0)
correlations.GetYaxis().SetTickLength(0)
for i in range(1,len(listAlgs)+1):
  name = listAlgs[i-1].strip('Stripping')
  if 'TIMING' in name:
    newname = name.rstrip('Beauty2CharmLine_TIMING')
  else:
    newname = name.rstrip('Beauty2CharmLine')
  correlations.GetXaxis().SetBinLabel(i,newname)
  correlations.GetYaxis().SetBinLabel(i,newname)
for i in range(1,len(listAlgs)+1):
  for j in range(1,len(listAlgs)+1):
    gbin = correlations.GetBin(i,j)
    if i == j:
      correlations.SetBinContent(gbin,100)
    if j>i:
      val = float(corr[listAlgs[i-1]][j-i-1].split('%')[0])
      correlations.SetBinContent(gbin,val)
    if i>j:
      val = float(corr[listAlgs[j-1]][i-j-1].split('%')[0])
      correlations.SetBinContent(gbin,val)
correlations.Draw("COLZ")
c.SaveAs("correlations.pdf")

