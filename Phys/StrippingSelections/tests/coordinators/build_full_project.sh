#!/bin/bash
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

function set_user_release_area() {
    export CMTPROJECTPATH="${CMTPROJECTPATH/$User_release_area/$1}"
    export User_release_area="$1"
}

function parse_project_name() {
    proj=$(dirname $1)
    branch=$(basename $1)
    if [ "$proj" = "." ] ; then
	proj=$branch
	branch="master"
    fi
    label="$proj/$branch"    
}

function checkout_project() {
    if [ $# = 0 ] || [ $1 = "-h" ] ; then
	echo 'Usage :
checkout_project <project>/<version>
version can be a tag or branch name. default is master.'
	return 0 
    fi
    parse_project_name "$1"
    echo "Checking out $label"
    if [ ! -e "$proj" ] ; then 
	git clone "ssh://git@gitlab.cern.ch:7999/lhcb/${proj}.git"
	cd "$proj"
	lb-project-init
	cd ..
    fi
    
    cd "$proj"
    git fetch
    if [ ! -z "$(git tag -l | grep ${branch})" ] ; then
	tag="$branch"
	branch="tag-${branch}"
	if [ -z "$(git branch | grep $branch)" ] ; then
	    git checkout "tags/${tag}" -b "${branch}"
	else 
	    # This won't update if the tag has changed on the remote end.
	    # Not sure how to do that - maybe delete the local branch and 
	    # checkout the tag again?
	    git checkout "$branch"
	fi
    else 
	git checkout "$branch"
	git pull origin "$branch"
    fi
    cd ..
}    

function build_project() {
    if [ $# = 0 ] || [ $1 = "-h" ] ; then
	echo 'Usage :
build_project <project>/<version>
version can be a tag or branch name. default is master.'
	return 0 
    fi

    cwd=$PWD
    checkout_project "$1"
    cd "${User_release_area}/${proj}"
    echo "Configuring $label"
    make configure >& stdout-make-configure
    if [ ! 0 = $? ] ; then
	echo "'make configure' failed, output at"
	echo "$PWD/stdout-make-configure"
	cd $cwd
	return $?
    fi
    echo "Building $label"
    make install >& stdout-make-install
    if [ ! 0 = $? ] ; then
	echo "'make install' failed, output at"
	echo "$PWD/stdout-make-install"
	cd $cwd
	return $?
    fi
    cd $cwd
}

function build_projects() {
    if [ $# = 0 ] || [ $1 = "-h" ] ; then 
	echo 'Usage:
build_projects <project1>/<version1> <project2>/<version2> ...
version can be a tag or branch. default is master.'
	return 0
    fi
    for proj in $@ ; do
	build_project "$proj"
	if [ ! $? = 0 ] ; then
	    echo "Failed to build $proj"
	    return $?
	fi
    done
}
