###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys

# Pipe in arguments with : echo -n year WG | ./run gaudirun.py TestFromSettings_All.py
arguments = {"year": "2016", "Version": "Stripping34"} # Put in some default values for testing purposes
arglist = []
if not sys.stdin.isatty():
    for line in sys.stdin:
        for word in line.split():
            arglist.append(word)
# Assign these values to the needed keys.
for i in range(len(arglist)):
    if i == 0:
        arguments["year"] = arglist[i]  # set the first value to 'year'
    elif i == 1:
        arguments["Version"] = arglist[i]  # set the second value to 'WG'
# Specify the name of your configuration
year = arguments["year"]
stripping = arguments["Version"] #FOR LIAISONS

from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)
from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
from Configurables import RawEventJuggler
juggler = RawEventJuggler( DataOnDemand=True, Input=0.3, Output=4.2 )
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

conf    = strippingConfiguration(stripping)
archive = strippingArchive(stripping)
streams = buildStreams(stripping = conf, archive = archive)#, WGs = "QEE")
leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]
stripTESPrefix = 'Strip'
from Configurables import ProcStatusCheck
sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

from Configurables import RootCnvSvc
RootCnvSvc().GlobalCompression = "ZLIB:1"

'''
# Test example for the S35 case:
# MiniBias stream contains those lines that need bad events
# IFT stream contains the rest of the events

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = { 'IFT': False, 'MiniBias': True },
                    BadEventSelection = { 'IFT': ProcStatusCheck(), 'MiniBias': None },
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

'''

enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements,
                                       stripMicroDSTStreamConf,
                                       stripMicroDSTElements,
                                       stripCalibMicroDSTStreamConf )
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True)
mdstElements   = stripMicroDSTElements(pack=enablePacking)
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking),
    charmMicroDSTname       : mdstElements,
    leptonicMicroDSTname    : mdstElements,
    pidMicroDSTname         : mdstElements,
    bhadronMicroDSTname     : mdstElements
    }
SelDSTWriterConf = {
    'default'                : stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=True),
    charmMicroDSTname        : mdstStreamConf,
    leptonicMicroDSTname     : mdstStreamConf,
    bhadronMicroDSTname      : mdstStreamConf,
    pidMicroDSTname          : stripCalibMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True)
    }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams() )
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )
from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections(), OnlyPositive = False, EveryEvent = False, ReportFrequency=20000, OutputLevel=INFO)
from Configurables import AlgorithmCorrelationsAlg
ac = AlgorithmCorrelationsAlg(Algorithms = list(set(sc.selections())))
from Configurables import EventTuple
from Configurables import TupleToolStripping
DaVinci().HistogramFile = 'DV_stripping_histos.root'
DaVinci().EvtMax = 100000
DaVinci().PrintFreq = 20000
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ sr ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

# might need:
# git lb-use Analysis
# git lb-checkout Analysis/run2-patches Phys/DecayTreeTupleTrigger
# for better performance - run on all Members and use same options as in AppConfig
from Configurables import EventTuple, TupleToolEventInfo
evt = EventTuple('TIMER')
evt.ToolList = [] # empty the tool list by default
evt.addTool(TupleToolEventInfo, name="TupleToolEventInfo")
strippingAlgorithms = DaVinci().mainSeq.Members[0].allConfigurables.keys() # full Members list includes also the report
evt.TupleToolEventInfo.Algorithms += strippingAlgorithms
evt.ToolList = ["TupleToolEventInfo"] # add it to the list
DaVinci().appendToMainSequence( [ evt ] )
DaVinci().TupleFile = 'TIMER.root'

from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
TimingAuditor().OutputLevel = INFO

# Data
if year == "2016":
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco16_DataType2016_Run174858.py")
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco16_DataType2016_Run174858_DV.py")
elif year == "2017":
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco17_DataType2017_Run195969.py")
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco17_DataType2017_Run195969_DV.py")
elif year == "2018":
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco18_DataType2018_Run214741.py")
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco18_DataType2018_Run214741_DV.py")
else:
    raise NotImplementedError(year)

