###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
for wg in B2CC BandQ BnoC B2OC Calib Charm QEE Semileptonic RD
do
  #echo "qsub -q generic7 -N StrippingTest${wg} -t 1 < ../launcher_${wg}.sh"
  qsub -q generic7 -N StrippingTest${wg} -t 1 < ../launcher_${wg}.sh  
done
