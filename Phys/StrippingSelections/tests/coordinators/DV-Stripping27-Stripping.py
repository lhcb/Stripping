###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options for building Stripping27 (proton helium stripping). 
"""

#use CommonParticlesArchive
stripping='stripping27'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent (?)
#
#from Configurables import RawEventJuggler
#juggler = RawEventJuggler( DataOnDemand=True, Input=0.3, Output=4.2 )

#
#Fix for TrackEff lines (?)
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

## remove GECs for pA
from Configurables import TrackSys
TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)
streams = buildStreams(stripping = config, archive = archive)
## add HERSCHEL and VELO raw banks to some lines
for stream in streams:
  for line in stream.lines:
    if line.name() in ['StrippingHeavyIonDiMuonJpsi2MuMuLine', 'StrippingMBMicroBias', 'StrippingMBMicroBiasLowMult', 'StrippingMBNoBias', 'StrippingSingleElectron', 'StrippingHeavyIonOpenCharmD02HHLine', 'StrippingHeavyIonOpenCharmNoPVD02HHBBLine', 'StrippingHeavyIonOpenCharmNoPVD02HHBELine', 'StrippingHeavyIonOpenCharmDst2D0PiLine', 'StrippingHeavyIonOpenCharmDp2KHHLine', 'StrippingHeavyIonOpenCharmDs2KKHLine', 'StrippingHeavyIonOpenCharmLc2PKHLine']:
      if line.RequiredRawEvents: 
        line.RequiredRawEvents += ['HC']
        if 'Velo' not in line.RequiredRawEvents:
          line.RequiredRawEvents += ['Velo']
      else:
        line.RequiredRawEvents = ['HC', 'Velo']
dstStreams  = [ "IFT", "MiniBias"]
stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck, GaudiSequencer
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents, 
                    TESPrefix = stripTESPrefix ,
                    DSTStreams = dstStreams)

#
# Configure the dst writers for the output
#
enablePacking = True

from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }

SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=True) }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

from Configurables import StrippingReport 
sr = StrippingReport(Selections = sc.selections())

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x42102700) 

#
# DaVinci Configuration
#
from Configurables import DaVinci

DaVinci().DDDBtag   = "dddb-20150724" 
DaVinci().CondDBtag = "cond-20161011" 

DaVinci().EvtMax = -1 # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ sr ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco16_pHe.py")

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

