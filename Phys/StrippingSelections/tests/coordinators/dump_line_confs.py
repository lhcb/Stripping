#!/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os, sys
sys.path.insert(0, os.path.join(os.environ['STRIPPINGSELECTIONSROOT'], 'tests', 'python'))

from StrippingTests.Utils import build_streams, stripping_args

def dump_obj(fline, obj) :
    if hasattr(obj, 'Members') :
        for member in obj.Members :
            dump_obj(fline, member)
        return
    fline.write(str(obj)+'\n')
    if hasattr(obj, 'getTools') :
        for tool in obj.getTools() :
            dump_obj(fline, tool)

def main() :
    argparser = stripping_args()
    argparser.add_argument('--outputdir', default = '.', 
                           help = '''Directory in which line confs will be saved.''')
    
    args = argparser.parse_args()
    if not os.path.exists(args.outputdir) :
        os.makedirs(args.outputdir)
    if args.stripping :
        streams = build_streams(args.stripping, args.stripping)
    else :
        streams = build_streams(args.settings, args.archive)
    
    for stream in streams :
        for line in stream.lines :
            fname = os.path.join(args.outputdir, line.name() + '.txt')
            with open(fname, 'w') as fline :
                for obj in line._members :
                    dump_obj(fline, obj)

if __name__ == '__main__' :
    main()
