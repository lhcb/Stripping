#!/bin/bash
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

strippings=(Stripping28 Stripping27 Stripping24r0p1 Stripping22r0p1 Stripping25 Stripping24 Stripping22b Stripping22 Stripping20r3p1 Stripping20r2 Stripping21r0p1 Stripping21 Stripping21r1p1 Stripping21r1 Stripping1Point4TeV Stripping14)
datatypes=(2016 2016 2015 2015 2015 2015 2015 2015 2012 2012 2012 2012 2011 2011 2011 2010)
steps=(130618 130847 131135 130867 130124 129222 128793 128148 129612 125274 129290 127062 129289 127063 14138 13698)

i=0
for stripping in ${strippings[@]} ; do
    datatype=${datatypes[i]}
    stepid=${steps[i]}
    
    ./gen-pydoc-old-strippings.sh $stripping $datatype $stepid >& stdout-gendoc-$stripping
    let i+=1
done