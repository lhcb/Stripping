#!/bin/env python
###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""
Get the breakdown of lines by WG and stream for a given Stripping version,
and produce pie charts.

Can be run with:
    ./run bash --norc ; python Phys/StrippingSelections/tests/coordinators/get_wg_lines.py --settings StrippingXXX --archive --StrippingXXX
"""
import sys
import os
sys.path.insert(0, os.path.join(os.environ['STRIPPINGSELECTIONSROOT'], 'tests', 'python'))
from StrippingTests.WGLines import get_wg_lines
from StrippingTests.Utils import stripping_args
from pprint import pformat
import matplotlib.pyplot as plt

plt.ioff()

known_working_groups = [
    "B2CC",
    "B2OC",
    "BandQ",
    "BnoC",
    "Charm",
    "IFT",
    "QEE",
    "RD",
    "Semileptonic"
]

known_streams = [
    "Bhadron",
    "BhadronCompleteEvent",
    "Charm",
    "CharmCompleteEvent",
    "Dimuon",
    "EW",
    "Leptonic",
    "Semileptonic"
]


def make_plots(all_lines, wgs=known_working_groups, version="", process="WG"):
    """
    Make plots of rate and event sizes of all lines.
    It will create three stacked histograms containing distributions of all lines,
    and a pie chart showing the number of lines per WG.

    Arguments:
        all_lines: dictionary of lines by working group
        wgs: list of working groups to categorize
        version (optional): Stripping version. Defaults to "".
        process (optional): How do we want to count the lines? Defaults to "WG".
    """

    # Count number of lines and rates/evt sizes per WG
    if process == "WG":
        num_lines_per_wg = {wg: 0 for wg in wgs}
        num_lines_per_wg["Other"] = 0
        for k in all_lines.keys():
            found_wg = False
            name = k
            for wg in num_lines_per_wg.keys():
                if name.startswith(wg):
                    found_wg = True
                    count_lines = sum(len(stream)
                                      for stream in all_lines[k].values())
                    num_lines_per_wg[wg] += count_lines
            if not found_wg:
                count_lines = sum(len(stream)
                                  for stream in all_lines[k].values())
                num_lines_per_wg["Other"] += count_lines

    if process == "Stream":
        num_lines_per_wg = {wg: 0 for wg in wgs}
        num_lines_per_wg["Other"] = 0
        for sub_dict in all_lines.values():
            for sub_key, sub_value in sub_dict.items():
                if sub_key not in num_lines_per_wg:
                    num_lines_per_wg["Other"] += len(sub_value)
                else:
                    num_lines_per_wg[sub_key] += len(sub_value)

    keys_to_remove = [
        wg for wg in num_lines_per_wg.keys() if num_lines_per_wg[wg] == 0
    ]
    for key in keys_to_remove:
        num_lines_per_wg.pop(key)

    # Sort the wg in number of lines
    num_lines_per_wg = {
        k: v
        for k, v in sorted(num_lines_per_wg.items(), key=lambda x: x[1])
    }

    # Make a pie plot of lines per WG
    labels = ["%s (%d)" % (k, v) for k, v in num_lines_per_wg.items()]
    fig = plt.figure()
    plt.pie(
        num_lines_per_wg.values(),
        radius=0.80,
        labels=labels,
        wedgeprops=dict(width=0.4, edgecolor="w"))
    plt.title("Number of Stripping lines per " + process + " -- " + version)
    plt.savefig("Stripping_lines_per_" + process + version + ".png")
    plt.close(fig)


def main():
    argparser = stripping_args()
    argparser.add_argument('--outputfile', default=None,
                           help='''Name of file to which to write the dict of lines.  If None, the dict is output to the console.''')
    argparser.add_argument('--plots', default=True,
                           action='store_false', help='Do we want to make the plots')

    args = argparser.parse_args()
    if args.stripping:
        lines = get_wg_lines(args.stripping, args.stripping)
    else:
        lines = get_wg_lines(args.settings, args.archive)
    if args.outputfile:
        with open(args.outputfile, 'w') as f:
            f.write(pformat(lines))
    else:
        print pformat(lines)
    if args.plots:
        make_plots(lines, known_working_groups, args.settings, "WG")
        make_plots(lines, known_streams, args.settings, "Stream")


if __name__ == '__main__':
    main()
