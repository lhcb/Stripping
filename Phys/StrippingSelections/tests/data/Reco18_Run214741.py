###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119592_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119590_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119588_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119597_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119595_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119593_1.rdst'
], clear=True)


from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco18_Run214741.xml' ]

