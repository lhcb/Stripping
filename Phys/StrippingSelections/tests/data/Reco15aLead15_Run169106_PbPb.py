###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-- GAUDI jobOptions generated on Wed Apr  5 16:40:28 2017
#-- Contains event types : 
#--   90000000 - 307 files - 4963447 events - 262.89 GBytes


#--  Extra information about the data processing phases:

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0001/00050695_00013415_1.rdst',
'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0001/00050695_00013416_1.rdst',
'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0001/00050695_00013417_1.rdst',
'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0001/00050695_00013418_1.rdst',
'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0001/00050695_00013419_1.rdst',
'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0001/00050695_00013420_1.rdst',
], clear=True)



from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco15aLead15_Run169106_PbPb.xml' ]

