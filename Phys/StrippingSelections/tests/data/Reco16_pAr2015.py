###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-- GAUDI jobOptions generated on Wed Nov 30 23:45:15 2016
#-- Contains event types : 


#--  Extra information about the data processing phases:

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Protonargon15/FULL.DST/00051869/0000/00051869_00000400_1.full.dst',
'LFN:/lhcb/LHCb/Protonargon15/FULL.DST/00051869/0000/00051869_00001635_1.full.dst',
'LFN:/lhcb/LHCb/Protonargon15/FULL.DST/00051869/0000/00051869_00001799_1.full.dst',
'LFN:/lhcb/LHCb/Protonargon15/FULL.DST/00051869/0000/00051869_00001796_1.full.dst',
'LFN:/lhcb/LHCb/Protonargon15/FULL.DST/00051869/0000/00051869_00000390_1.full.dst',
'LFN:/lhcb/LHCb/Protonargon15/FULL.DST/00051869/0000/00051869_00002627_1.full.dst'], clear=True)





from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco16_pAr2015.xml' ]

