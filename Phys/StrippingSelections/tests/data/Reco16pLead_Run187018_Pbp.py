###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-- GAUDI jobOptions generated on Wed Apr  5 16:46:54 2017
#-- Contains event types : 
#--   90000000 - 929 files - 26387585 events - 5835.90 GBytes


#--  Extra information about the data processing phases:

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/LHCb/Ionproton16/FULL.DST/00055929/0000/00055929_00001117_1.full.dst',
'LFN:/lhcb/LHCb/Ionproton16/FULL.DST/00055929/0000/00055929_00001118_1.full.dst',
'LFN:/lhcb/LHCb/Ionproton16/FULL.DST/00055929/0000/00055929_00001119_1.full.dst',
'LFN:/lhcb/LHCb/Ionproton16/FULL.DST/00055929/0000/00055929_00001121_1.full.dst',
], clear=True)

from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco16pLead_Run187018_Pbp.xml' ]

