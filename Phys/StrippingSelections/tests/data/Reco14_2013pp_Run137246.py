###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000538_1.full.dst',
'LFN:/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000431_1.full.dst',
'LFN:/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000928_1.full.dst',
'LFN:/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000289_1.full.dst',
'LFN:/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000134_1.full.dst',
'LFN:/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000465_1.full.dst',

], clear=True)


from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco14_2013pp_Run137246.xml' ]

