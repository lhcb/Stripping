###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# $Id: $
# Test your line(s) of the stripping
# To be ran like: echo -n year WG | ./run gaudirun.py TestMyWGfromSelections.py 
# NOTE: Liaisons should not edit this file.  If your WG or year are not attributed, please open an issue in Stripping
#
import sys, os

# Pipe in arguments with : echo -n year WG | ./run gaudirun.py TestMyWGfromSelections.py
arguments = {"year": "2016", "WG": "SL"} # Put in some default values for testing purposes
arglist = []
if not sys.stdin.isatty():
    for line in sys.stdin:
        for word in line.split():
            arglist.append(word)
# Assign these values to the needed keys.
for i in range(len(arglist)):
    if i == 0:
        arguments["year"] = arglist[i]  # set the first value to 'year'
    elif i == 1:
        arguments["WG"] = arglist[i]  # set the second value to 'WG'
    elif i == 2 and len(arglist) == 4:
        os.environ[arglist[i]] = arglist[i+1]

# Do a check that we are using valid attributes before continuing:
#if not argumets["year"] in ["2016", "2018"]:
#    print("ERROR: Invalid year for this test")
#    break
#if not argumets["WG"] in ["B2CC", "BandQ", "SL", "RD", "BnoC", "QEE", "B2OC", "Calib", "Charm", "IFT", "MiniBias"]:
#    print("ERROR: Invalid WG for this test")
#    break

from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

# Specify the name of your configuration
year = arguments["year"]
my_wg = arguments["WG"] #FOR LIAISONS
print("Year is " + year + " and my_wg is " + my_wg)
from Configurables import RootCnvSvc
RootCnvSvc().GlobalCompression = "ZLIB:1"

# NOTE: this will work only if you inserted correctly the 
# default_config dictionary in the code where your LineBuilder 
# is defined.
from StrippingSelections import buildersConf
confs = buildersConf()
from StrippingSelections.Utils import lineBuilder, buildStreams
streams = buildStreams( confs, WGs=my_wg )

leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

#
# Configure the dst writers for the output
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements,
                                       stripMicroDSTStreamConf,
                                       stripMicroDSTElements,
                                       stripCalibMicroDSTStreamConf )

#
# Configuration of MicroDST
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True)
mdstElements   = stripMicroDSTElements(pack=enablePacking)

#
# Configuration of SelDSTWriter
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking),
    charmMicroDSTname       : mdstElements,
    leptonicMicroDSTname    : mdstElements,
    pidMicroDSTname         : mdstElements,
    bhadronMicroDSTname     : mdstElements
    }

SelDSTWriterConf = {
    'default'                : stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=True),
    charmMicroDSTname        : mdstStreamConf,
    leptonicMicroDSTname     : mdstStreamConf,
    bhadronMicroDSTname      : mdstStreamConf,
    pidMicroDSTname          : stripCalibMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams() )


#
# Add stripping TCK
#
#from Configurables import StrippingTCK
#stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36112100)

#
#Configure DaVinci
#

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections())

from Configurables import AlgorithmCorrelationsAlg
ac = AlgorithmCorrelationsAlg(Algorithms = list(set(sc.selections())))

DaVinci().HistogramFile = 'DV_stripping_histos.root'
DaVinci().EvtMax = 10000
DaVinci().PrintFreq = 1000
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ sr ] )
#DaVinci().appendToMainSequence( [ ac ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# change the column size of timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

# Data
if year == "2016":
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco16_DataType2016_Run174858.py")
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco16_DataType2016_Run174858_DV.py")
elif year == "2017":
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco17_DataType2017_Run195969.py")
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco17_DataType2017_Run195969_DV.py")
elif year == "2018":
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco18_DataType2018_Run214741.py")
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco18_DataType2018_Run214741_DV.py")
else:
    raise NotImplementedError(year)


# Modify the input PFNs from the TestFileDB to be LFNs
def pfn_to_lfn(pfn):
    prefix = "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest"
    if not pfn.startswith(prefix):
        raise NotImplementedError(pfn)
    lfn = pfn[len(prefix):]
    if lfn.startswith("/lhcb/data"):
        lfn = "/lhcb/LHCb" + lfn[len("/lhcb/data"):]
    return "LFN:" + lfn


from Gaudi.Configuration import EventSelector
from GaudiConf import IOHelper
IOHelper().inputFiles([
    pfn_to_lfn(IOHelper().undressFile(x))
    for x in EventSelector().Input
], clear=True)
