###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# $Id: $
# Test your line(s) of the stripping by taking the dictionaries from StrippingSettings
#  
# To be ran like: echo -n year WG | ./run gaudirun.py TestMyWGfromSettings.py 
# NOTE: Liaisons should not edit this file.  If your WG or year are not attributed, please open an issue in Stripping

from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf

# Pipe in arguments with : echo -n year WG | ./run gaudirun.py TestMyWGfromSettings.py
arguments = {"year": "2016", "WG": "BandQ"} # Put in some default values for testing purposes
arglist = []
if not sys.stdin.isatty():
    for line in sys.stdin:
        for word in line.split():
            arglist.append(word)
# Assign these values to the needed keys.
for i in range(len(arglist)):
    if i == 0:
        arguments["year"] = arglist[i]  # set the first value to 'year'
    elif i == 1:
        arguments["WG"] = arglist[i]  # set the second value to 'WG'

# Specify the name of your configuration
stripping="stripping34r0p2"
year = arguments["year"]
my_wg = arguments["WG"] #FOR LIAISONS
print("Year is " + year + " and my_wg is " + my_wg)


from Configurables import RootCnvSvc
RootCnvSvc().GlobalCompression = "ZLIB:1"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False


# This loads the static database from Phys/StrippingSettings/dbase/<stripping>
# If you've changed the python dicts but the database hasn't been updated then
# use the config_from_modules method below instead.from StrippingSettings.Utils import strippingConfiguration
# from StrippingSelections import buildersConf
# conf = strippingConfiguration(stripping)

# This loads the python dicts in Phys/StrippingSettings/python/StrippingSettings/<stripping>/*.py
from StrippingSettings.Utils import config_from_modules
conf = config_from_modules(stripping)

from StrippingSelections.Utils import lineBuilder, buildStreams
streams = buildStreams( conf, WGs=my_wg )

leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative", "IFT" ]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    #MaxCombinations = 10000000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

#
# Configure the dst writers for the output
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements,
                                       stripMicroDSTStreamConf,
                                       stripMicroDSTElements,
                                       stripCalibMicroDSTStreamConf )

#
# Configuration of MicroDST
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True)
mdstElements   = stripMicroDSTElements(pack=enablePacking)

#
# Configuration of SelDSTWriter
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking),
    charmMicroDSTname       : mdstElements,
    leptonicMicroDSTname    : mdstElements,
    pidMicroDSTname         : mdstElements,
    bhadronMicroDSTname     : mdstElements
    }

SelDSTWriterConf = {
    'default'                : stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=True),
    charmMicroDSTname        : mdstStreamConf,
    leptonicMicroDSTname     : mdstStreamConf,
    bhadronMicroDSTname      : mdstStreamConf,
    pidMicroDSTname          : stripCalibMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams() )


#
# Add stripping TCK
#
#from Configurables import StrippingTCK
#stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36112100)

#
#Configure DaVinci
#

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections())

from Configurables import AlgorithmCorrelationsAlg
ac = AlgorithmCorrelationsAlg(Algorithms = list(set(sc.selections())))

DaVinci().HistogramFile = 'DV_stripping_histos.root'
DaVinci().EvtMax = 10000
DaVinci().PrintFreq = 1000
#DaVinci().appendToMainSequence( [unpackIt] )
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ sr ] )
#DaVinci().appendToMainSequence( [ ac ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

# change the column size of timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

# SMOG
#importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco18Lead18_217952_PbNe.py")
#importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco18Lead18_217952_PbNe_DV.py")
#importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco15aLead15_Run168665_PbAr.py")
#importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco15aLead15_Run168665_PbAr_DV.py")

# Data (pp)
if year == "2016":
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco16_DataType2016_Run174858.py")
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco16_DataType2016_Run174858_DV.py")
elif year == "2017":
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco17_DataType2017_Run195969.py")
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco17_DataType2017_Run195969_DV.py")
elif year == "2018":
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco18_DataType2018_Run214741.py")
    importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco18_DataType2018_Run214741_DV.py")
else:
    raise NotImplementedError(year)


