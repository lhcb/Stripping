#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file  CommonParticles/StdNoPIDsPions.py
# configuration file for 'Standard NoPIDs Pions'
# @author Patrick Koppenburg
# @date 2011-07-18
# =============================================================================
"""
Configuration file for 'Standard NoPIDs Pions with loose cuts'
"""
from __future__ import print_function
from Configurables import FilterDesktop
from CommonParticles.Utils import defaultCuts, updateDoD, locationsDoD


__all__ = (
    'StdNoPIDsPions',
    'locations'
    )

__author__ = "Patrick Koppenburg"


# create the algorithm
algorithm = FilterDesktop('StdNoPIDsPions',
                          Inputs=["Phys/StdAllNoPIDsPions/Particles"],
                          Code=defaultCuts())

# configure Data-On-Demand service
locations = updateDoD(algorithm)

# finally: define the symbol
StdNoPIDsPions = algorithm


# ============================================================================
if '__main__' == __name__:

    print(__doc__)
    print(__author__)
    print(locationsDoD(locations))
