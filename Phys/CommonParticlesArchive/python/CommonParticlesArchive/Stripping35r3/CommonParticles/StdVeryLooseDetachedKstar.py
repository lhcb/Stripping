###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration file for Standard Very Loose Kstar2Kpi
"""

__author__ = 'P. Koppenburg'
__date__ = '22/01/2010'


from Configurables import CombineParticles
from CommonParticles.Utils import updateDoD

###########################
# StdVeryLooseDetachetKst2Kpi #
###########################

StdVeryLooseDetachedKst2Kpi = CombineParticles("StdVeryLooseDetachedKst2Kpi")
StdVeryLooseDetachedKst2Kpi.DecayDescriptor = "[K*(892)0 -> K+ pi-]cc"
StdVeryLooseDetachedKst2Kpi.Inputs = ["Phys/StdLooseKaons/Particles",
                                      "Phys/StdLoosePions/Particles"]

StdVeryLooseDetachedKst2Kpi.CombinationCut = "(ADAMASS('K*(892)0')<300*MeV) & (ADOCACHI2CUT(30, ''))"
StdVeryLooseDetachedKst2Kpi.MotherCut = "(VFASPF(VCHI2)<25)"

locations = updateDoD(StdVeryLooseDetachedKst2Kpi)
