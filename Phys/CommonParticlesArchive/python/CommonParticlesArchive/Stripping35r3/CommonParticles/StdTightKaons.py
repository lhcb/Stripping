#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file  CommonParticles/StdTightKaons.py
# configuration file for 'Standard Tight Kaons'
# @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
# @date 2009-01-14
# =============================================================================
"""
Configuration file for 'Standard Tight Kaons'
"""
from __future__ import print_function
from Configurables import ProtoParticleCALOFilter, CombinedParticleMaker
from CommonParticles.Utils import trackSelector, protoFilter, updateDoD, locationsDoD


__author__ = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"

__all__ = (
    'StdTightKaons',
    'locations'
    )

# create the algorithm
algorithm = CombinedParticleMaker('StdTightKaons',
                                  Particle='kaon')

# configure the track selector
selector = trackSelector(algorithm)

# protoparticle filter:
fltr = protoFilter(algorithm, ProtoParticleCALOFilter, 'Kaon')
fltr.Selection = ["RequiresDet='RICH' CombDLL(k-pi)>'0.0'"]

# configure Data-On-Demand service
locations = updateDoD(algorithm)

# finally: define the symbol
StdTightKaons = algorithm

# ============================================================================
if '__main__' == __name__:

    print(__doc__)
    print(__author__)
    print(locationsDoD(locations))
