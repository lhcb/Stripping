#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file  CommonParticles/StdNoPIDsElectrons.py
# configuration file for 'Standard NoPIDs Pions'
# @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
# @date 2009-01-14
# =============================================================================
"""
Configuration file for 'NoBrem NoPIDs Electrons with downstram tracks'
"""
from __future__ import print_function
from Configurables import NoPIDsParticleMaker
from CommonParticles.Utils import trackSelector, updateDoD, locationsDoD


__author__ = "Ulrich Kerzel Ulrich.Kerzel@cern.ch"

__all__ = (
    'NoBremNoPIDsDownElectrons',
    'locations'
    )
# =============================================================================

# create the algorithm
algorithm = NoPIDsParticleMaker('NoBremNoPIDsDownElectrons',
                                DecayDescriptor='Electron',
                                Particle='electron')

algorithm.AddBremPhotonTo = []

# configure the track selector
selector = trackSelector(algorithm, trackTypes=["Downstream"])

# configure Data-On-Demand service
locations = updateDoD(algorithm)

# finally: define the symbol
NoBremNoPIDsDownElectrons = algorithm

# ============================================================================
if '__main__' == __name__:

    print(__doc__)
    print(__author__)
    print(locationsDoD(locations))
