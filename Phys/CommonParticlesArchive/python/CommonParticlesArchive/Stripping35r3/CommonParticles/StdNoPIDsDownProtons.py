#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file  CommonParticles/StdNoPIDsDownProtons.py
# configuration file for 'Standard NoPIDs Downstream Protons'
# @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
# @date 2009-01-14
# =============================================================================
"""
Configuration file for 'Standard NoPIDs Downstream Protons'
"""
from __future__ import print_function
from Configurables import NoPIDsParticleMaker
from CommonParticles.Utils import trackSelector, updateDoD, locationsDoD


__author__ = "Alessio Sarti <Alessio.Sarti@lnf.infn.it>"

__all__ = (
    'StdNoPIDsDownProtons',
    'locations'
    )


# create the algorithm
algorithm = NoPIDsParticleMaker('StdNoPIDsDownProtons',
                                DecayDescriptor='Proton',
                                Particle='proton')

# configure the track selector
selector = trackSelector(algorithm,
                         trackTypes=['Downstream'],
                         cuts={"Chi2Cut": [0, 10]})

# configure Data-On-Demand service
locations = updateDoD(algorithm)

# finally: define the symbol
StdNoPIDsDownProtons = algorithm

# ============================================================================
if '__main__' == __name__:

    print(__doc__)
    print(__author__)
    print(locationsDoD(locations))
