#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file  CommonParticles/StdLooseLambda.py
# configuration file for 'Standard Loose Lambda'
# @author Patrick Koppenburg
# @date 2009-02-18
# =============================================================================
"""
Configuration file for 'Standard Loose Lambda'
"""
from __future__ import print_function
from Configurables import CombineParticles, FilterDesktop
from CommonParticles.Utils import updateDoD, locationsDoD


__author__ = "Patrick Koppenburg"

__all__ = (
    'StdLooseLambdaLL',
    'StdLooseLambdaDD',
    'StdLooseLambdaLD',
    'locations'
    )

# ============================================================================
# create the algorithm
StdLooseLambdaLL = CombineParticles('StdLooseLambdaLL')

StdLooseLambdaLL.Inputs = ["Phys/StdLoosePions/Particles",
                           "Phys/StdLooseProtons/Particles"]
StdLooseLambdaLL.DecayDescriptor = "[Lambda0 -> p+ pi-]cc"

StdLooseLambdaLL.DaughtersCuts = {
    "pi+": "(P>2*GeV) & (MIPCHI2DV(PRIMARY)>9)",
    "p+": "(P>2*GeV) & (MIPCHI2DV(PRIMARY)>9)"
    }

StdLooseLambdaLL.CombinationCut = "(ADAMASS('Lambda0')<50*MeV) & (ADOCACHI2CUT(30, ''))"
StdLooseLambdaLL.MotherCut = "(ADMASS('Lambda0')<35*MeV) & (VFASPF(VCHI2)<30)"

# configure Data-On-Demand service
locations = updateDoD(StdLooseLambdaLL)


StdLooseLambdaDD = StdLooseLambdaLL.clone('StdLooseLambdaDD')

StdLooseLambdaDD.Inputs = ["Phys/StdNoPIDsDownPions/Particles",
                           "Phys/StdNoPIDsDownProtons/Particles"]

StdLooseLambdaDD.DaughtersCuts = {
    "pi+": "(P>2*GeV) & (MIPCHI2DV(PRIMARY)>4)",
    "p+": "(P>2*GeV) & (MIPCHI2DV(PRIMARY)>4)"
    }

StdLooseLambdaDD.CombinationCut = "(ADAMASS('Lambda0')<80*MeV) & (ADOCACHI2CUT(25, ''))"
StdLooseLambdaDD.MotherCut = "(ADMASS('Lambda0')<64*MeV) & (VFASPF(VCHI2)<25)"

# configure Data-On-Demand service
locations.update(updateDoD(StdLooseLambdaDD))


# p +/- long

StdLooseLambdaLDpLong = StdLooseLambdaDD.clone('StdLooseLambdaLDpLong')

StdLooseLambdaLDpLong.Inputs = ["Phys/StdLooseProtons/Particles",
                                "Phys/StdNoPIDsDownPions/Particles"]

StdLooseLambdaLDpLong.DaughtersCuts = {"p+": "(ISLONG) & (P>2*GeV) & (MIPCHI2DV(PRIMARY)>9)",
                                       "pi-": "(ISDOWN) & (P>2*GeV) & (MIPCHI2DV(PRIMARY)>4)"}

# p +/- down

StdLooseLambdaLDpDown = StdLooseLambdaDD.clone('StdLooseLambdaLDpDown')

StdLooseLambdaLDpDown.Inputs = ["Phys/StdNoPIDsDownProtons/Particles",
                                "Phys/StdLoosePions/Particles"]

StdLooseLambdaLDpDown.DaughtersCuts = {"p+": "(ISDOWN) & (P>2*GeV) & (MIPCHI2DV(PRIMARY)>4)",
                                       "pi-": "(ISLONG) & (P>2*GeV) & (MIPCHI2DV(PRIMARY)>9)"}

# Combination

StdLooseLambdaLD = FilterDesktop("StdLooseLambdaLD", Code="ALL")

StdLooseLambdaLD.Inputs = ["Phys/StdLooseLambdaLDpLong/Particles",
                           "Phys/StdLooseLambdaLDpDown/Particles"]

locations.update(updateDoD(StdLooseLambdaLDpLong))
locations.update(updateDoD(StdLooseLambdaLDpDown))

locations.update(updateDoD(StdLooseLambdaLD))

# ============================================================================
if '__main__' == __name__:

    print(__doc__)
    print(__author__)
    print(locationsDoD(locations))
