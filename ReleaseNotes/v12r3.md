2018-12-12 - Stripping v12r3
===============================
## Prepare Stripping v12r3 (based on Phys v25r8) for Sim09e:
  - Removing IFT streams from test scripts (1d076234).
  - Prepare for Stripping34r0p1 (48fb6106).
  - DocfixStripping2018-patches (93990302).
  - Updating catalog for Reco18 (ef551104).
  - Fixing a bug in all end-of-LHCb data files (6eeb0a71).
  - End of LHCb restripping datafiles - white space errors (bc15794e).
  - End of LHCb restripping datafiles (32c57f70).
  - S35 datafiles updated (d8024694).
  - Adding correction to gen-catalog (94f565ff).
